# CALREL project README

This software is provided free of cost.  It comes with absolutely no
warranties as to its fitness for any purpose.  If you happen to use
it an publish any results using the software, then please cite the
user manual by Pei-Ling Liu, Hong-Zong Lin, and Armen der Kiureghian.


Quick install help:

(1) Open makefile.in and pick your compiler ifort or gfortran
by uncommenting the FF line you want and comment out the other one.

(2) In makefile.in uncomment the FFOPTFLAG line below Intel if you
have selected ifort.  Otherwise comment that line and uncomment one
of the ones above for gfortran.

(3) Run make archive to make the archive, run make calrel to make
calrel, running make install makes both the archive and the main
program.

(4) The executable is in the folder main.


Quick Run:

(1) Run ./calrel Incalrel Outfile, this will use Incalrel with the
default user.f and put the output in the file Outfile.

(2) See the manuals for further details.  No support is provided.

