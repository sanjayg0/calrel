c******************************************************************
      subroutine scprep(not,nset,icset,nry,alpha,ndro,ro,igfx,isys,
     *                  btdes,bvec,nind,iord,nda,amat,rvec)
c******************************************************************
      implicit   none
      real*8,allocatable::adum(:,:),ata(:,:),adum2(:,:),av1(:),av2(:)
      integer*4,allocatable::iprint(:)
      integer*4 not,nset,icset(*),nry,ndro,igfx(*),nind,iord(*),nda,isys
      real*8 alpha(nry,*),ro(ndro,*),amat(nda,*),btdes(*),bvec(*),cdot
      real*8 rvec(*)
c
      integer*4 i,ii,j,k
      real*8 ysum,coef
c$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' *** check for sub scprep ***'')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' nset.......................'',i10)') nset
      write(not,'(1h ,'' isys.......................'',i10)') isys
      write(not,'((1h ,'' icset '',5i10))') (icset(i),i=1,nset)
      write(not,'(1h ,'' nry........................'',i10)') nry
      write(not,'(1h ,'' alphas '')')
      do i=1,nset
      ii=iabs(icset(i))
      write(not,'(1h ,'' comp '',i5)') icset(i)
      write(not,'((1h ,'' alph '',5e15.5))')
     * (alpha(j,ii),j=1,nry)
      write(not,'((1h ,'' btdes '',5e15.5))') btdes(ii)
      enddo
c$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$      endif
c
      call scindvec(not,nry,nset,icset,alpha,iord,nind,isys,btdes)
      write(not,'(1h ,''number of independent comp......'',i5)') nind
      write(not,'((1h ,''independet components...........'',10i5))')
     *   (icset(iord(i)),i=1,nind)
      call scmkro(not,nset,icset,nry,alpha,ndro,ro,igfx,nind,iord)
      call scmkbt(not,nset,icset,isys,btdes,bvec,nind,iord)
c
      if(nset.le.2.or.nind.eq.1) then
        allocate(av1(nry))
        allocate(av2(nry))
        ii=iord(1)
        ii=icset(ii)
        if(isys.eq.1) then
          if(ii.gt.0) then
            do k=1,nry
              av1(k)=alpha(k,ii)
            enddo
          else
            do k=1,nry
              av1(k)=-alpha(k,-ii)
            enddo
          endif
        else
          if(ii.gt.0) then
            do k=1,nry
              av1(k)=-alpha(k,ii)
            enddo
          else
            do k=1,nry
              av1(k)=alpha(k,-ii)
            enddo
          endif
        endif
        do i=2,nset
          ii=iord(i)
          ii=icset(ii)
          if(isys.eq.1) then
            if(ii.gt.0) then
              do k=1,nry
                av2(k)=alpha(k,ii)
              enddo
            else
              do k=1,nry
                av2(k)=-alpha(k,-ii)
              enddo
            endif
          else
            if(ii.gt.0) then
              do k=1,nry
                av2(k)=-alpha(k,ii)
              enddo
            else
              do k=1,nry
                av2(k)=alpha(k,-ii)
              enddo
            endif
          endif
          rvec(i)=cdot(av1,av2,1,1,nry)
        enddo
        deallocate(av1)
        deallocate(av2)
        return
      endif
c
      allocate(adum(nset,nry))
      allocate(adum2(nset,nry))
      allocate(ata(nind,nind))
      allocate(iprint(nset))
c
      if(nind.ne.nset) then
      do i=1,nind
        ii=iord(i)
        ii=icset(ii)
        if(isys.eq.1) then
          if(ii.gt.0) then
            do k=1,nry
            adum(i,k)=alpha(k,ii)
            enddo
          else
            do k=1,nry
            adum(i,k)=-alpha(k,-ii)
            enddo
          endif
        else
          if(ii.gt.0) then
            do k=1,nry
            adum(i,k)=-alpha(k,ii)
            enddo
          else
            do k=1,nry
            adum(i,k)=alpha(k,-ii)
            enddo
          endif
        endif
      enddo
c
      do i=1,nind
      do j=1,nind
        ysum=0.0d0
        do k=1,nry
          ysum=ysum+adum(i,k)*adum(j,k)
        enddo
        ata(i,j)=ysum
      enddo
      enddo
c
      call covinv(nind,nind,ata)
      do i=1,nind
      do j=1,nry
        ysum=0.0d0
        do k=1,nind
          ysum=ysum+ata(i,k)*adum(k,j)
        enddo
        adum2(i,j)=ysum
      enddo
      enddo
c
      do i=1,nset-nind
        ii=iord(i+nind)
        ii=icset(ii)
        coef=1.0d0
        if(isys.eq.1) then
          if(ii.lt.0) then
            coef=-1.0d0
          endif
        else
          if(ii.gt.0) then
            coef=-1.0d0
          endif
        endif
        ii=abs(ii)
        do j=1,nind
          ysum=0.0d0
          do k=1,nry
            ysum=ysum+coef*alpha(k,ii)*adum2(j,k)
          enddo
          amat(i,j)=ysum
        enddo
      enddo
      endif
c
      if(nind.ne.nset) then
      do i=1,nind
        ii=iord(i)
        ii=icset(ii)
        if(ii.lt.0) then
          iprint(i)=-igfx(-ii)
        else
          iprint(i)=igfx(ii)
        endif
      enddo
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''number of dependent comp........'',i5)')
     *                            nset-nind
      write(not,'(1h ,''--------- beta and transformation matrix'',
     *                '' -------- '')')
      write(not,'(1h ,5x,11x4hbeta,10i15)') (iprint(i),i=1,nind)
      do i=1,nset-nind
      ii=iord(i+nind)
      ii=icset(ii)
      if(ii.gt.0) then
        ii=igfx(ii)
      else
        ii=-igfx(-ii)
      endif
      write(not,'(1h ,i5,1p10e15.5)')
     *       ii,bvec(i+nind),(amat(i,j),j=1,nind)
      enddo
      endif
c
      deallocate(iprint)
      deallocate(adum)
      deallocate(adum2)
      deallocate(ata)

      end subroutine scprep
