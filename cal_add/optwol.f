!*********************************************************************
      subroutine optwol(not,nry,nset,ncset,ngfc,ialpha,icomps,
     *                 alpha,btdes,ncomp,ydesw,btdesw,alphaw)
!*********************************************************************
      implicit   none

      integer*4,allocatable::iaflag(:),icor(:),iwork(:)
      real*8,allocatable::pmat(:,:),wpt(:),rwork(:),ptnr(:),coef(:)
      real*8,allocatable::wcoef(:),work(:),pmat1(:,:)
!
      integer*4 nry,ngfc,i,icomp,k,ncor,nmaj,nmin,jflag
      integer*4 nvars,j,mcon,kkk,liwrk,lwrk,icheck,ncomp
      integer*4 not,nset,ncset(*),ialpha(nry,*),icomps(2,ngfc,*)
      real*8 alpha(nry,*),btdes(*),ydesw(nry,*),btdesw(*)
      real*8 anorm,alphaw(nry,*),swork,wdist,ysum,cdot
!
      allocate(iaflag(nry))
!
      do i=1,nset
        do j=1,nry
          iaflag(j)=1
        enddo
        do j=1,ncset(i)
          icomp=icomps(1,j,i)
          if(icomp.ne.0) then
            do k=1,nry
              iaflag(k)=iaflag(k)*ialpha(k,abs(icomp))
            enddo
          endif
        enddo
!           ===== construct set data =====

        nvars=0
        do j=1,nry
          nvars=nvars+(1-iaflag(j))
        enddo
        mcon = ncset(i)
        allocate(pmat(nvars+1,mcon))
        kkk=0
        do j=1,nry
          if(iaflag(j).eq.0) then
            kkk=kkk+1
            do k=1,mcon
              icomp=icomps(1,k,i)
              if(icomp.gt.0) then
                pmat(kkk,k)=-alpha(j,abs(icomp))
              else
                pmat(kkk,k)=alpha(j,abs(icomp))
              endif
            enddo
          endif
        enddo
!       ----- set bounds -----
        do k=1,mcon
          if(icomps(1,k,i).gt.0) then
             pmat(nvars+1,k)=btdes(abs(icomps(1,k,i)))
          else
             pmat(nvars+1,k)=-btdes(abs(icomps(1,k,i)))
          endif
        enddo
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,'' solve '',i5,''th cutset'')') i
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,'' number of comstraint,,,,'',i5)') mcon
!        write(not,'(1h ,'' number of parameters, ,,'',i5)') nvars
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,'' pmat matrix  '')')
!        write(not,'(1h ,''  '')')
!        do j=1,nvars+1
!        write(not,'((1h ,10e15.5))')(pmat(j,k),k=1,mcon)
!        enddo
!       ----- qp solver -----
        liwrk=4*mcon+5*nvars+3
        lwrk=2*nvars**2+4*mcon*nvars+9*mcon+22*nvars+10
        allocate(pmat1(nvars+1,mcon))
        allocate(wpt(nvars))
        allocate(icor(nvars+1))
        allocate(rwork(nvars+1))
        allocate(ptnr(nvars+1))
        allocate(coef(mcon))
        allocate(wcoef(mcon))
        allocate(iwork(liwrk))
        allocate(work(lwrk))
        call wolfe(nvars,mcon,pmat,0,swork,ncor,icor,iwork,liwrk,
     *  work,lwrk,rwork,coef,ptnr,pmat1,nvars,mcon,wcoef,wpt,wdist,
     *  nmaj,nmin,jflag)
!
!           ----- check constraint -----
!
        icheck=0
        do j=1,mcon
          ysum=0.0d0
          do k=1,nvars
            ysum=ysum+pmat(k,j)*wpt(k)
          enddo
          ysum=ysum+pmat(nvars+1,j)
!          write(not,'(1h ,'' check condition '',i5,e15.5)')j,ysum
          if(ysum.gt.0.0d0) then
            icheck=1
          endif
        enddo
!
        kkk=0
        do j=1,nry
          if(iaflag(j).eq.0) then
            kkk=kkk+1
            ydesw(j,i)=wpt(kkk)
          else
            ydesw(j,i)=0.0d0
          endif
        enddo
        deallocate(pmat)
        deallocate(pmat1)
        deallocate(wpt)
        deallocate(icor)
        deallocate(rwork)
        deallocate(ptnr)
        deallocate(coef)
        deallocate(wcoef)
        deallocate(iwork)
        deallocate(work)
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,'' ***** solution ***** '')')
!        write(not,'(1h ,''  '')')
!        write(not,'(1h ,10x,6x9hdesign pt)')
!        write(not,'((1h ,i10,e15.5))')(j,ydesw(j,i),j=1,nry)
      enddo
      ncomp=nset
      do i=1,nset
        anorm=cdot(ydesw(1,i),ydesw(1,i),nry,nry,nry)
        btdesw(i)=dsqrt(anorm)
        if(btdes(i).lt.1.0d-8) then
        do j=1,nry
          alphaw(j,i)=1.0d0/dsqrt(dfloat(nry))
        enddo
        else
        do j=1,nry
          alphaw(j,i)=ydesw(j,i)/btdesw(i)
        enddo
        endif
      enddo

      deallocate(iaflag)

      end subroutine optwol
