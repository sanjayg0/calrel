c************************************************************
      subroutine fnmerit(nry,u1,u2,gu1,gu2,fmerit)
c************************************************************

      implicit   none

      integer (kind=4) :: nry 
      real    (kind=8) :: u1(*),u2(*),gu1,gu2,fmerit

      real    (kind=8) :: uu1,uu2,gg1, fmerit1,fmerit2
      real    (kind=8) :: cdot 

      uu1=cdot(u1,u1,1,1,nry)
      uu2=cdot(u2,u2,1,1,nry)

      if(gu1.le.0.0d0) then
        gg1=0.0d0
      else
        gg1=gu1
      endif

      fmerit1=0.5d0*uu2-0.5d0*uu1-gg1
      fmerit2=gu2-gg1

      fmerit=fmerit1
      if(fmerit2.gt.fmerit1) fmerit=fmerit2

      end subroutine fnmerit
