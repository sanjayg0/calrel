c***************************************************************
      subroutine fnpolak(not,nry,gx,y,a,sdgy,d,thetaf)
c***************************************************************

      implicit   none

      integer (kind=4) :: nry,not
      real    (kind=8) :: y(*),a(*),d(*),gx,thetaf,sdgy
c
      real    (kind=8) :: a11,a12,a22,bbb1,bbb2
      real    (kind=8) :: coefa,coefb,coefc,fend1,fend2,zzz1,zzz2,xxx
      real    (kind=8) :: cdot
      integer (kind=4) :: i
c
      a11=cdot(y,y,1,1,nry)
      a12=cdot(y,a,1,1,nry)*sdgy
      a12=-a12
      a22=cdot(a,a,1,1,nry)*sdgy*sdgy
c
      if(gx.le.0.0d0) then
        bbb1=0.0d0
      else
        bbb1=gx
      endif
      bbb2=bbb1-gx
      coefa=0.5d0*a11+0.5d0*a22-a12
      coefb=bbb1-bbb2+a12-a22
      coefc=0.5d0*a22+bbb2

c     ----- compute factor of second order equation -----

      if(coefa.lt.0.0d0) then
         write(not,'(1h ,''  error in polak-he algorithm '')')
         write(not,'(1h ,''  the quadratic term is negative '')')
         stop
      elseif(coefa.lt.1.0d-9) then
         fend1=coefc
         fend2=coefa+coefb+coefc
         if(fend1.lt.fend2) then
            zzz1=0.0d0
            zzz2=1.0d0
            thetaf=-fend1
         else
            zzz1=1.0d0
            zzz2=0.0d0
            thetaf=-fend2
         endif
      else
         xxx=-coefb/(2.0d0*coefa)
         zzz1=xxx
         zzz2=1.0d0-xxx
         thetaf=-(coefa*xxx*xxx+coefb*xxx+coefc)
      endif

c     ----- check if the minimum point is outside the 'triangle' -----

      if(zzz1.lt.0.0d0 .or. zzz1.gt.1.0d0 .or.
     *   zzz2.lt.0.0d0 .or. zzz2.gt.1.0d0) then
         fend1=coefc
         fend2=coefa+coefb+coefc
         if(fend1.lt.fend2) then
           zzz1=0.0d0
           zzz2=1.0d0
           thetaf=-fend1
         else
           zzz1=1.0d0
           zzz2=0.0d0
           thetaf=-fend2
         endif
      endif

c     ----- search direction -----

      do 50 i=1,nry
         d(i)=-zzz1*y(i)+zzz2*a(i)*sdgy
50    continue

      end subroutine fnpolak
