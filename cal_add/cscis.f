      subroutine cscis

      implicit   none

c*****************************************************************
c
c     perform sequential conditional importance sampling (scis)
c     for series of parallel systems
c
c     refer to
c     r. ambartzumian and a. der kiureghian
c     multinormal probability by sequential conditioned
c     importance sampling theory and application
c     probabilistic engineering mechanics vol. 13(4) 299-308 1998
c
c*****************************************************************

      include   'blkrel1.h'
      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'prob.h'
      include   'simu.h'

      integer (kind=4) :: ind, iboun,idum
      integer (kind=4) :: n1,n2,n3,n4,n5,n6, nc,nr

c     ===== imput data read =====

      nsm=10000
      call freei('m',nsm,1)  ! total number of trials
      call gseed(stp)        ! generate seed from time and data
      call freer('p',stp,1)  ! read seed from input
      npr=max0(nsm/20,1)     !
      call freei('r',npr,1)  ! print out controll
      cov=0.05d0             !
      call freer('v',cov,1)  ! target coefficient of varianse
      ist=0                  !
      call freei('t',ist,1)  ! restart flag
      ind=0                  !
      call freei('d',ind,1)  ! input data flag
      iboun=0                  !
      call freei('n',iboun,1)  ! input data flag
c
c     ind = 0 compute system reliability according to form and cuts data
c         < 0 input rectangular domain directly for demonstration of scis
c             # of components = -ind
c         > 0�@change cuts data for convenience
c
      call locate('mc  ',n1,nr,nc)
      call define('y1  ',n2,nry,1)
      call locate('igfx',n3,nr,nc)
c     ----- locate design point coordinate -----
      if(ind.eq.0) then
        if(flc(3)) call cerror(5,0,0.d0,' ')
        call locate('beta',n4,nr,nc)
        call locate('alph',n5,nry,ngf)
        call locate('fltf',n6,nr,nc)
        if(icl.eq.1.or.ngf.eq.1) then
        write(not,'(1h ,''  *** scis is not applicable'',
     *              '' to component problem *** '')')
        stop
        endif
      else
        n4=1
        n5=1
        n6=1
      endif
c
      if(ist.eq.0) then
        write(not,1010)
      else
        write(not,1020)
      endif
      write(not,1030) npr,nsm,cov,stp
c
      call scis(not,ngf,nry,nsm,npr,ntl,icl,nin,ind,iboun,cov,stp,
     *          ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),idum)
c
      call delete('x1  ')
      call delete('y1  ')
      call delete('z1  ')
      call delete('igx ')

 1010 format(//' >>>> scis simulation <<<<'/)
 1020 format(//' >>>> scis simulation -- restart <<<<'/)
 1030 format(
     *'  print interval ......................... npr=',i8/
     *'  number of simulations .................. nsm=',i8/
     *'  threshold for coef. of variation .... cov=',e11.4/
     *'  random seed ......................... stp=',f11.0)

      end subroutine cscis
