c******************************************************************
      subroutine lpcnsets(not,ngf,igfx,ntl,mc,ncomp,icomp,
     *                    ncset,nset,maxset,icset,fltf,iclin)
c*******************************************************************

c     count individual components

c     input    ntl  : total number of cutset data
c              mc   : cutset data

c     output   ncomp      : number of components
c              icomp      : component seq. id

c********************************************************************
      implicit   none

      integer*4,allocatable::iprint(:)

      integer*4 ntl,mc(ntl,2),ngf,not,igfx(ngf,3)
      integer*4 icomp(*),ncomp,ncset,nset(*),maxset,icset(maxset,*)
      integer*4 iclin,isys

      logical fltf(*)

      integer*4 i,iim,isq,j,nsett,k,kset,npr

      ncomp=0
      do i=1,ntl
        iim=mc(i,1)  !! iim is the name of the component
        if(iim.ne.0) then
          isq=iabs(mc(i,2))
          isq=igfx(isq,2)
          if(.not.fltf(isq)) then
            write(not,'(1h ,'' !! fatal error !! '')')
            write(not,'(1h ,'' form is not performed for '',i5)')
     *           igfx(isq,1)
            stop
          endif
          if(ncomp.eq.0) then
            ncomp=ncomp+1
            icomp(ncomp)=isq
          else
            do j=1,ncomp
              if(iabs(isq).eq.icomp(j)) goto 10
            enddo
            ncomp=ncomp+1
            icomp(ncomp)=isq
   10       continue
          endif
        endif
      enddo

      allocate(iprint(2*ncomp))
      ncset=1
      nsett=0
      do i=1,ntl
        iim=mc(i,1)  ! function id
        if(iim.ne.0) then
          nsett=nsett+1
          if(nsett.ge.maxset) then
            write(not,'(1h ,'' fatall !!'')')
            write(not,'(1h ,'' increase max in input !!'')')
            stop
          endif
          isq=iabs(mc(i,2))  ! function seq id
          isq=igfx(isq,2)
          do k=1,ncomp
            if(isq.eq.icomp(k)) goto 95
          enddo
   95     continue
          kset=k
          if(.not.fltf(isq)) then
            write(not,'(1h ,'' fatal error in lpbo '')')
            write(not,'(1h ,'' form is not performed'',
     *      '' for l.s.f. '',i5)') igfx(isq,1)
            stop
          endif
          if(iim.gt.0) then
            icset(nsett,ncset)=kset
          else
            icset(nsett,ncset)=-kset
          endif
        else
          nset(ncset)=nsett
          ncset=ncset+1
          if(ncset.ge.maxset) then
            write(not,'(1h ,'' fatall !!'')')
            write(not,'(1h ,'' increase max in input !!'')')
            stop
          endif
          nsett=0
        endif
      enddo
      ncset=ncset-1

      if(iclin.eq.2) then
        isys=1
      else
        if(ncset.eq.1) then
          isys=2
        else
          isys=3
        endif
      endif

      if(isys.eq.1) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== series system problem  ===== '')')
      write(not,'(1h ,'' '')')
      npr=0
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
          npr=npr+1
c          iprint(npr)=igfx(icset(j,i),1)
          iprint(npr)=icomp(icset(j,i))
        else
          npr=npr+1
c          iprint(npr)=-igfx(-icset(j,i),1)
          iprint(npr)=-icomp(-icset(j,i))
        endif
      enddo
      enddo
      write(not,'(1h ,''number of components.................'',i5)')
     *     npr
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,npr)
      elseif(isys.eq.2) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== parallel system problem ===== '')')
      write(not,'(1h ,'' '')')
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
c          iprint(j)=igfx(icset(j,i),1)
          iprint(j)=icomp(icset(j,i))
        else
c          iprint(j)=-igfx(-icset(j,i),1)
          iprint(j)=-icomp(-icset(j,i))
        endif
      enddo
      write(not,'(1h ,''number of components.................'',i5)')
     *     nset(i)
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,nset(i))
      enddo
      else
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== general system problem ===== '')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''number of cut sets...................'',i5)')
     * ncset
      do i=1,ncset
      do j=1,nset(i)
        if(icset(j,i).gt.0) then
c          iprint(j)=igfx(icset(j,i),1)
          iprint(j)=icomp(icset(j,i))
        else
c          iprint(j)=-igfx(-icset(j,i),1)
          iprint(j)=-icomp(-icset(j,i))
        endif
      enddo
      write(not,'(1h ,''----- cutset'',i5,''  -----'')') i
      write(not,'(1h ,''number of components.................'',i5)')
     *     nset(i)
      write(not,'(1h ,''components...........................'',10i5)')
     *   (iprint(j),j=1,nset(i))
      enddo
      endif
      deallocate(iprint)

      end subroutine lpcnsets
