      subroutine refwl(ndm,ncor,icor,pmat,pmat1,nparm,numgr,ixrct,
     *                 save,wpt)
c
      implicit   none
c
      integer (kind=4) :: ndm,ncor,nparm,numgr 
      integer (kind=4) :: icor(nparm+1)
      integer (kind=4) :: ixrct(2*nparm)
      real    (kind=8) :: pmat(nparm+1,numgr),pmat1(nparm+1,numgr)
      real    (kind=8) :: wpt(nparm+1),save(nparm)

      real    (kind=8) :: aa,amax
      real    (kind=8) :: fact
      integer (kind=4) :: i,imax,itrct,itrlm
      integer (kind=4) :: j,jmax,jstrt 
      integer (kind=4) :: k,kcol,kk,kp1
      integer (kind=4) :: l
      integer (kind=4) :: maxrs
      integer (kind=4) :: n, nresl
      real    (kind=8) :: spcmn
      real    (kind=8) :: tole
      real    (kind=8) :: wrst,wrsto
      real    (kind=8) :: d1mach
c this subroutine attempts to refine the ndm dimensional vector wpt
c produced by wolfe by directly solving the system
c summation(pmat(i,j)*wpt(i), i=1,...,ndm) = -pmat(ndm+1,j) for j =
c icor(l), l=1,...,ncor.
c nresl resolvents are chosen by total pivoting.  if nresl .lt. ndm then
c the remaining ndm-nresl elements of wpt are kept form the old wpt.
c itrlm steps of iterative refinement are attempted at the end.
c
c compute machine and precision dependent constants.
c     nwrit=i1mach(2)
      spcmn=d1mach(3)
      tole=spcmn
      itrlm=2
      itrct=0
      nresl=0
      n=ndm+1
c if ncor=0 we have nothing to do so we return.
      if(ncor)100,100,200
c
  100 return
c
c copy column icor(l) of pmat with the sign of the last element reversed
c into column l of the work matrix pmat1 for l=1,...,ncor.
  200 do 400 l=1,ncor
        j=icor(l)
        do 300 i=1,ndm
          pmat1(i,l)=pmat(i,j)
  300     continue
        pmat1(n,l)=-pmat(n,j)
  400   continue
c
c
c now column reduce pmat1.  note that pmat1 is the transpose of the usual
c augmented matrix for solving a linear system of equatons.
c there will be at most maxrs = min(ndm,ncor) resolvents.
      maxrs=ncor
      if(ndm-maxrs)430,470,470
  430 maxrs=ndm
  470 do 1900 k=1,maxrs
c
c search for the indices imax and jmax with 1 .le. imax .le. ndm, 1 .le.
c jmax .le. ncor, pmat1(imax,jmax) is not in the row or column of any
c other resolvent (i.e. pivot), and abs(pmat1(imax,jmax)) is maximized.
c we use the vector ixrct to save the resolvent positions to save space.
        jstrt=0
        do 1300 j=1,ncor
          if(nresl)700,700,500
  500     do 600 l=1,nresl
            if(j-ixrct(2*l))600,1300,600
  600       continue
c here there is no earlier resolvent in column j.
  700     do 1200 i=1,ndm
            if(nresl)1000,1000,800
  800       do 900 l=1,nresl
              if(i-ixrct(2*l-1))900,1200,900
  900         continue
c here there is no earlier resolvent in row i.
 1000       aa=abs(pmat1(i,j))
            if(jstrt)1030,1030,1070
 1030       jstrt=1
            go to 1100
 1070       if(aa-amax)1200,1200,1100
 1100       amax=aa
            imax=i
            jmax=j
 1200       continue
 1300     continue
c if the absolute value of this resolvent is very small we do not attempt
c any further column operations.
        if(amax-tole)2000,1400,1400
c increment nresl and put the location of the nreslth resolvent in
c (ixrct(2*l-1),ixrct(2*l)).
 1400   nresl=nresl+1
        ixrct(2*nresl-1)=imax
        ixrct(2*nresl)=jmax
c
c now eliminate wpt(imax) from those columns which do not contain any of
c the resolvents found so far (including the present resolvent).
        do 1800 j=1,ncor
          do 1500 l=1,nresl
            if(j-ixrct(2*l))1500,1800,1500
 1500       continue
c here column j does not contain any of the resolvents found so far, and
c we compute the factor for the column operation needed to zero out
c pmat1(imax,j) (although we do not actually write in the zero).
          fact=pmat1(imax,j)/pmat1(imax,jmax)
c now do the operation in column j for all rows not containing a
c resolvent.  the elements in this column in the rows which contain an
c earlier (or present) resolvent will not be needed later.
          do 1700 i=1,n
            do 1600 l=1,nresl
              if(i-ixrct(2*l-1))1600,1700,1600
 1600         continue
            pmat1(i,j)=pmat1(i,j)-fact*pmat1(i,jmax)
 1700       continue
 1800     continue
 1900   continue
c end of column reduction of pmat1.
c
c
c if nresl=0 then all the elements in pmat1 for 1 .le. i .le. ndm and
c 1 .le. j .le. ncor were very small in absolute value, and there is
c nothing we can do, so we return.
 2000 if(nresl)100,100,2100
c
c
c now do back substitution to compute, for k=nresl,...,1,
c wpt(ixrct(2*k-1)) = (pmat1(ndm+1,ixrct(2*k)) - summation(
c pmat1(i,ixrct(2*k))*wpt(i), for i = 1,...,ndm, i .ne. ixrct(2*l-1)
c for any l=1,...,k))/pmat1(ixrct(2*k-1),ixrct(2*k)).  if we are in an
c iterative refinement step we wish to consider wpt(i) (which is then
c just a correction to wpt(i)) = 0.0 if i corresponds to no resolvent
c (since the value of such wpt(i) in save should not change) so we omit
c the corresponding terms in the summation above.
 2100 do 2400 kk=1,nresl
        k=nresl-kk+1
        imax=ixrct(2*k-1)
        jmax=ixrct(2*k)
        wpt(imax)=pmat1(n,jmax)
        do 2300 i=1,ndm
          do 2200 l=1,k
            if(i-ixrct(2*l-1))2200,2300,2200
 2200       continue
c here row i contains no earlier (or present) resolvents.
          if(itrct)2280,2280,2220
 2220     if(k-nresl)2240,2300,2300
c here we are doing iterative refinement, k .lt. nresl, and i .ne.
c ixrct(2*l-1) for l=1,...,k.  we will use the term corresponding to
c wpt(i) iff i = ixrct(2*l-1) for some l = k+1,...,nresl.
 2240     kp1=k+1
          do 2260 l=kp1,nresl
            if(i-ixrct(2*l-1))2260,2280,2260
 2260       continue
          go to 2300
 2280     wpt(imax)=wpt(imax)-pmat1(i,jmax)*wpt(i)
 2300     continue
        wpt(imax)=wpt(imax)/pmat1(imax,jmax)
 2400   continue
c end of back substitution.
c
c
c if itrct is positive then wpt will contain only an iterative
c refinement correction in those positions corresponding to resolvents
c and we add this to save to get the true wpt.
      if(itrct)2900,2900,2500
 2500 do 2800 i=1,ndm
        do 2600 l=1,nresl
          if(i-ixrct(2*l-1))2600,2700,2600
 2600     continue
        go to 2800
 2700   wpt(i)=wpt(i)+save(i)
 2800   continue
c
c
c now compute the residual and put it into pmat1(ndm+1,.).
 2900 do 3200 k=1,ncor
c compute the column index kcol in pmat corresponding to column k in
c pmat1.
        kcol=icor(k)
        pmat1(n,k)=-pmat(n,kcol)
        do 3100 i=1,ndm
          pmat1(n,k)=pmat1(n,k)-pmat(i,kcol)*wpt(i)
 3100     continue
 3200   continue
c
c compute the worst absolute value of the residual elements.
      do 3500 k=1,ncor
        aa=abs(pmat1(n,k))
        if(k-1)3400,3400,3300
 3300   if(aa-wrst)3500,3500,3400
 3400   wrst=aa
 3500   continue
c
      if(itrct)3700,3700,3800
 3700 wrsto=wrst
      go to 4100
 3800 if(wrst-wrsto)4100,4100,3900
c here itrct .gt. 0 and wrst .gt. wrsto, so we go back to the previous
c wpt and return.
 3900 wrst=wrsto
      do 4000 i=1,ndm
        wpt(i)=save(i)
 4000   continue
      return
c
 4100 if(itrct-itrlm)4200,100,100
c here itrct .lt. itrlm and we increment itrct and set up for the itrctth
c iterative refinement step.
 4200 itrct=itrct+1
c copy wpt into save.
      do 4300 i=1,ndm
        save(i)=wpt(i)
 4300   continue
      go to 2100
      end subroutine refwl
