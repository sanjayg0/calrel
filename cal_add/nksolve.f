c**************************************************************************
      subroutine nksolv0(nrx,nry,ntf,ngf,nig,nrmx,ngfc,icl,igfx,igt,lgp,
     *                   ids,ipa,ib,izx,ixz,x1,y0,y1,alpha,beta,
     *                   tf,ex,sg,par,bnd,tp,gtor,root,z1w,dgx,gam,avec,
     *                   gdgx,isflg,rr,ir,g0,isol,neval)
c**************************************************************************
      implicit   none

      integer*4 nrx,nry,ntf,ngf,nig,nrmx,ngfc,icl,igfx(ngf,3),igt(nig)
      integer*4 lgp(3,nig),ids(nrx),ipa(4,nrx),ib(nrx),izx(nry),isol
      integer*4 ixz(nrx)
      real*8 x1(nrx),y0(nry),tf(ntf),ex(nrx),sg(nrx),par(4,nrx)
      real*8 bnd(2,nrx),tp(*),gtor(ngf),alpha(nry),y1(nry)
      real*8 z1w(nry),root,g0(ngf),beta
      real*8 dgx(nrx),gam(nrx),avec(nrx),gdgx(nrx)
c
      integer*4 ir(ngf),isflg(nrmx,ngf),ifound,ibetap,ibetan
      real*8 rr(nrmx,ngf)
      real*8 g1,r1
      integer*4 k,k2,ifunc,irr,kp,idn,idx2p,idx2n,irprep,irpren,i
      integer*4 idx1,idnp,idnn,idx1p,idx1n,iepos,ieneg,ifoundp,ifoundn
      integer*4 neval
      real*8 r1p,r1n,g1p,g1n,r2p,r2n,g2p,g2n,g2,g1p0,g1n0
      real*8 aaa,ppp,fai,dbeta,dgfun1,rrr3,ggg3,rrr2,ggg2
      real*8 error,betaa,g11,rrr1,dgg1,grlim,betap,betan,ggg1
      real*8 betap0,betan0,aaa2,aaa10,beta0,g10,drr2,dgg2

      real    (kind=8) :: roon, roop
c
      parameter(error=0.001d0)
      parameter(grlim=0.001d0)
c
      neval=0
      if(isol.eq.0) then
      roop=(root-beta)/3.0d0
      roon=(beta+5)/3.0d0
c
      do 301 k=1,ngfc
        k2=igfx(k,2)
        ifunc=igfx(k,3)
        irr=0
        kp=1
        do i=1,nry
          y1(i)=y0(i)+beta*alpha(i)
        enddo
c       ----- check for beta=beta -----
        call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
        neval=neval+1
        call ugfun(g1,x1,tp,ifunc)
        g0(k)=g1
        irprep=0
        irpren=0
        if(abs(g1).le.abs(gtor(k2))*1.1d0) then
          r1=beta
          irr=irr+1
          rr(irr,k)=r1
          isflg(irr,k)=0
          idn=0
          irprep=1
          irpren=1
        else
          idn=1
          r1=beta
          if(g1.lt.0.0d0) then
            idx1=1
          else
            idx1=0
          endif
        endif
c
        r1p=r1
        r1n=r1
        g1p=g1
        g1n=g1
        idnp=idn
        idnn=idn
        idx1p=idx1
        idx1n=idx1
        iepos=0
        ieneg=0
  500   continue
        r2p=r1p+roop
        if(iepos.ne.2.and.r2p.ge.root) then
          iepos=1
          r2p=root
        endif
        r2n=r1n-roon
        if(ieneg.ne.2.and.r2n.le.-5.0d0) then
          ieneg=1
          r2n=-5.0d0
        endif
        if(iepos.le.1) then
          call nksecant(nry,nrx,ntf,nig,nrmx,ifunc,irr,idx1p,idx2p,idnp,
     *                  igt,lgp,ids,ipa,ib,izx,tp,tf,ex,sg,par,bnd,
     *                  gtor(k2),y0,alpha,beta,x1,y1,z1w,
     *                  r1p,r2p,g1p,g2p,root,rr(1,k),0,isflg(1,k),
     *                  ifoundp,irprep,neval)
          if(ifoundp.eq.1) irprep=irr
          if(iepos.eq.1.and.idnp.eq.0) then
            do i=1,nry
              y1(i)=y0(i)+r2p*1.1d0*alpha(i)
            enddo
            call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,
     *                 igt,lgp,ids,ipa,ib,izx)
            call ugfun(g2,x1,tp,ifunc)
            neval=neval+1
            if(abs(g2).lt.gtor(k2)*1.0001d0) then
              isflg(irr,k)=0
            elseif(g2.gt.0.0d0) then
              isflg(irr,k)=-1
            else
              isflg(irr,k)= 1
            endif
          endif
          if(iepos.eq.1) iepos=2
          if(icl.le.2.and.ifoundp.eq.1) iepos=2
        endif
        if(ieneg.le.1) then
          call nksecant(nry,nrx,ntf,nig,nrmx,ifunc,irr,idx1n,idx2n,idnn,
     *                  igt,lgp,ids,ipa,ib,izx,tp,tf,ex,sg,par,bnd,
     *                  gtor(k2),y0,alpha,beta,x1,y1,z1w,
     *                  r1n,r2n,g1n,g2n,-5.0d0,rr(1,k),1,isflg(1,k),
     *                  ifoundn,irpren,neval)
          if(ifoundn.eq.1) irpren=irr
          if(ieneg.eq.1.and.idnp.eq.0) then
            do i=1,nry
              y1(i)=y0(i)+r2n*1.1d0*alpha(i)
            enddo
            call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,
     *                 igt,lgp,ids,ipa,ib,izx)
            call ugfun(g2,x1,tp,ifunc)
            neval=neval+1
            if(abs(g2).lt.gtor(k2)*1.0001d0) then
              isflg(irr,k)=0
            elseif(g2.gt.0.0d0) then
              isflg(irr,k)= 1
            else
              isflg(irr,k)=-1
            endif
          endif
          if(ieneg.eq.1) ieneg=2
          if(icl.le.2.and.ifoundn.eq.1) ieneg=2
        endif
        if(irr.eq.0.and.(iepos.le.1.or.ieneg.le.1)) goto 500
c        if(iepos.le.1.or.ieneg.le.1) goto 500
        if(irr.eq.0) then
          ir(k)=1
          if(g0(k).gt.0.d0) then
            isflg(1,k)=1
            rr(1,k)= root
          else
            isflg(1,k)=1
            rr(1,k)= -5.0d0
          endif
        else
          ir(k)=irr
        endif
 301  continue
      else
        beta0=beta
        if(ngfc.ne.1) then
          beta0=0.0d0
        endif
c       ------ secant method -----
        do 551 k=1,ngfc
          k2=igfx(k,2)
          ifunc=igfx(k,3)
c          irr=0
c       ----- check for beta=beta -----
          do i=1,nry
            y1(i)=y0(i)+beta0*alpha(i)
          enddo
          call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(g1,x1,tp,ifunc)
          g0(k)=g1
          g10=g1
          neval=neval+1
c         ----- find non zero gradient around beta -----
          betaa=beta0+beta*0.001d0
          do i=1,nry
            y1(i)=y0(i)+betaa*alpha(i)
          enddo
          call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(g11,x1,tp,ifunc)
          neval=neval+1
          aaa=(g11-g1)/(betaa-beta0)
          aaa10=aaa
          call dnorm(-beta,ppp)
          fai= exp(-0.5d0*beta*beta)/2.506628275d0
          dbeta=error*ppp/fai
c          dgfun1=dbeta*abs(aaa)
          dgfun1=1.0e-8
          rrr1=beta0
          ggg1=g1
          dgg1=aaa
c
          if(abs(g1).lt.dgfun1) then
            if(aaa.gt.grlim) then
              ir(k)=1
              rr(1,k)=beta0
              isflg(1,k)=-1
              goto 551
            elseif(aaa.lt.-grlim) then
              ir(k)=1
              rr(1,k)=beta0
              isflg(1,k)=1
              goto 551
            else
              ir(k)=1
              isflg(1,k)=1
              rr(1,k)=-root
c              ir(k)=0
              goto 551
            endif
          endif
c
          ifound=0
c
  700     continue
          if(abs(aaa).lt.grlim) then
            dbeta=(root-beta0)/3.0d0
            betap=beta0
            betan=beta0
            betap0=beta0
            betan0=beta0
            ibetap=0
            ibetan=0
            g1p0=g10
            g1n0=g10
  210       continue
            betap=betap+dbeta
            betan=betan-dbeta
            if(ibetap.eq.0.and.betap.ge.root) then
              ibetap=1
              betap=root
            endif
            if(ibetan.eq.0.and.betan.le.-root) then
              ibetan=1
              betan=-root
            endif
            if(ibetap.le.1) then
              do i=1,nry
                y1(i)=y0(i)+betap*alpha(i)
              enddo
              call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,
     *                                              ipa,ib,izx)
              call ugfun(g1p,x1,tp,ifunc)
              neval=neval+1
              aaa=(g1p-g1p0)/(betap-betap0)
              if(abs(g1p).lt.dgfun1) then
                ir(k)=1
                rr(1,k)=betap
                if(aaa.gt.grlim) then
                  isflg(1,k)=-1
                  goto 551
                elseif(aaa.lt.-grlim) then
                  isflg(1,k)=1
                  goto 551
                else
c                  ir(k)=0
                  if(g1p0.gt.0.0d0) then
                    isflg(1,k)=1
                  else
                    isflg(1,k)=1
                  endif
                  goto 551
                endif
              elseif(g1p*g1p0.lt.0.0d0) then
                ifound=1
                rrr1=betap0
                rrr3=betap
                ggg1=g1p0
                ggg3=g1p
                dgg1=aaa
                goto 220
              endif
              if(ibetap.eq.1) ibetap=2
            endif
            if(ibetan.le.1) then
              do i=1,nry
                y1(i)=y0(i)+betan*alpha(i)
              enddo
              call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,
     *                                              ipa,ib,izx)
              call ugfun(g1n,x1,tp,ifunc)
              neval=neval+1
              aaa=(g1n-g1n0)/(betan-betan0)
              if(abs(g1n).lt.dgfun1) then
                ir(k)=1
                rr(1,k)=betap
                if(aaa.gt.grlim) then
                  isflg(1,k)=-1
                  goto 551
                elseif(aaa.lt.-grlim) then
                  isflg(1,k)=1
                  goto 551
                else
c                  ir(k)=0
                  if(g1n0.gt.0.0d0) then
                    isflg(1,k)=-1
                  else
                    isflg(1,k)=1
                  endif
                  goto 551
                endif
              elseif(g1n*g1n0.lt.0.0d0) then
                ifound=-1
                rrr1=betan0
                rrr3=betan
                ggg1=g1n0
                ggg3=g1n
                dgg1=aaa
                goto 220
              endif
              if(ibetan.eq.1) ibetan=2
            endif
            if(ibetap*ibetan.ne.0) goto 220
            betap0=betap
            betan0=betan
            g1p0=g1p
            g1n0=g1n
            goto 210
  220       continue
            if(ifound.eq.0) then
              ir(k)=1
              isflg(1,k)=1
              if(g0(k).le.0.0d0) then
                rr(1,k)=-root
              else
                rr(1,k)= root
              endif
              goto 551
            endif
          endif
c
  600     continue
          rrr2=rrr1-ggg1/dgg1
          do i=1,nry
            y1(i)=y0(i)+rrr2*alpha(i)
          enddo
          call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(ggg2,x1,tp,ifunc)
          neval=neval+1
          if(abs(ggg2).lt.dgfun1) then
            ir(k)=1
            rr(1,k)=rrr2
            if(dgg1.gt.0.0d0) then
              isflg(1,k)=-1
            else
              isflg(1,k)= 1
            endif
            goto 551
          endif
c
c         ---- gradient -----
          if(abs(rrr2).gt.1.0d0) then
            drr2=rrr2*1.001d0
          else
            drr2=rrr2+0.001d0
          endif
          do i=1,nry
            y1(i)=y0(i)+drr2*alpha(i)
          enddo
          call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(dgg2,x1,tp,ifunc)
          neval=neval+1
          dgg1=(dgg2-ggg2)/(drr2-rrr2)
c         ---- gradient -----
c
          if(ggg1*ggg2.lt.0.0d0) ifound=1
c
          if(ifound.ne.0) then
            if(ggg2*ggg1.lt.0.0d0) then
c              dgg1=(ggg2-ggg1)/(rrr2-rrr1)
              rrr3=rrr1
              ggg3=ggg1
              rrr1=rrr2
              ggg1=ggg2
            else
c              dgg1=(ggg2-ggg3)/(rrr2-rrr3)
              rrr1=rrr2
              ggg1=ggg2
            endif
            goto 600
          else
            if(abs(rrr2).lt.root) then
c              dgg1=(ggg2-ggg1)/(rrr2-rrr1)
              ggg1=ggg2
              rrr1=rrr2
              goto 600
            else
              rrr2=sign(root,rrr2)
              rrr3=rrr2*1.001d0
              do i=1,nry
                y1(i)=y0(i)+rrr2*alpha(i)
              enddo
              call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *                 ib,izx)
              call ugfun(ggg2,x1,tp,ifunc)
              neval=neval+1
              do i=1,nry
                y1(i)=y0(i)+rrr3*alpha(i)
              enddo
              call cytox(x1,y1,z1w,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *                 ib,izx)
              call ugfun(ggg3,x1,tp,ifunc)
              neval=neval+1
              aaa2=(ggg3-ggg2)/(rrr3-rrr2)
              if(aaa2.lt.grlim.or.aaa2*aaa10.gt.0.0d0) then
                ir(k)=1
                isflg(1,k)=1
                if(g0(k).le.0.0d0) then
                  rr(1,k)=-root
                else
                  rr(1,k)= root
                endif
c                ir(k)=0
                goto 551
              else
                aaa=0.0d0
                goto 700
              endif
            endif
          endif
  551   continue
      endif

      end subroutine nksolv0
