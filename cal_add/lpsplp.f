      subroutine lpsplp(lpusrmat, mrelas, nvars, costs, prgopt, dattrv,
     &                  bl,bu, ind, info, primal, duals, ibasis, work,
     &                  lw, iwork, liw)

c***begin prologue  splp

c***purpose  solve linear programming problems involving at
c            most a few thousand constraints and variables.
c            takes advantage of sparsity in the constraint matrix.

c***library   slatec

c***category  g2a2

c***type      single precision (splp-s, dsplp-d)

c***keywords  linear constraints, linear optimization,

c             linear programming, lp, sparse constraints

c***author  hanson, r. j., (snla)
c           hiebert, k. l., (snla)

c***description
c
c     these are the short usage instructions; for details about
c     other features, options and methods for defining the matrix
c     a, see the extended usage instructions which are contained in
c     the long description section below.
c
c   |------------|
c   |introduction|
c   |------------|
c     The subprogram splp( ) solves a linear optimization problem.
c     The problem statement is as follows
c
c                         minimize (transpose of costs)*x
c                         subject to a*x=w.
c
c     The entries of the unknowns x and w may have simple lower or
c     upper bounds (or both), or be free to take on any value.  By
c     setting the bounds for x and w, the user is imposing the con-
c     straints of the problem.  The matrix a has mrelas rows and
c     nvars columns.  The vectors costs, x, and w respectively
c     have nvars, nvars, and mrelas number of entries.
c
c     The input for the problem includes the problem dimensions,
c     mrelas and nvars, the array costs(*), data for the matrix
c     a, and the bound information for the unknowns x and w, bl(*),
c     bu(*), and ind(*).  Only the nonzero entries of the matrix a
c     are passed to splp( ).
c
c     The output from the problem (when output flag info=1) includes
c     optimal values for x and w in primal(*), optimal values for
c     dual variables of the equations a*x=w and the simple bounds
c     on x in  duals(*), and the indices of the basic columns,
c     ibasis(*).
c
c  |------------------------------|
c  |fortran declarations required:|
c  |------------------------------|
c
c     dimension costs(nvars),prgopt(*),dattrv(*),
c    *bl(nvars+mrelas),bu(nvars+mrelas),ind(nvars+mrelas),
c    *primal(nvars+mrelas),duals(mrelas+nvars),ibasis(nvars+mrelas),
c    *work(lw),iwork(liw)
c
c     external usrmat
c
c     The dimensions of prgopt(*) and dattrv(*) must be at least 1.
c     The exact lengths will be determined by user-required options and
c     data transferred to the subprogram usrmat( ).
c
c     The values of lw and liw, the lengths of the arrays work(*)
c     and iwork(*), must satisfy the inequalities
c
c               lw .ge. 4*nvars+ 8*mrelas+lamat+  lbm
c               liw.ge.   nvars+11*mrelas+lamat+2*lbm
c
c     It is an error if they do not both satisfy these inequalities.
c     (The subprogram will inform the user of the required lengths
c     if either lw or liw is wrong.)  The values of lamat and lbm
c     nominally are
c
c               lamat=4*nvars+7
c     and       lbm  =8*mrelas
c
c     lamat determines the length of the sparse matrix storage area.
c     The value of lbm determines the amount of storage available
c     to decompose and update the active basis matrix.
c
c  |------|
c  |input:|
c  |------|
c
c     mrelas,nvars
c     ------------
c     These parameters are respectively the number of constraints (the
c     linear relations a*x=w that the unknowns x and w are to satisfy)
c     and the number of entries in the vector x.  both must be .ge. 1.
c     other values are errors.
c
c     costs(*)
c     --------
c     The nvars entries of this array are the coefficients of the
c     linear objective function.  The value costs(j) is the
c     multiplier for variable j of the unknown vector x.  Each
c     entry of this array must be defined.
c
c     usrmat
c     ------
c     This is the name of a specific subprogram in the splp( ) package
c     used to define the matrix a.  In this usage mode of splp( )
c     the user places the nonzero entries of a in the
c     array dattrv(*) as given in the description of that parameter.
c     The name usrmat must appear in a fortran external statement.
c
c     dattrv(*)
c     ---------
c     The array dattrv(*) contains data for the matrix a as follows:
c     each column (numbered j) requires (floating point) data con-
c     sisting of the value (-j) followed by pairs of values.  Each pair
c     consists of the row index immediately followed by the value
c     of the matrix at that entry.  A value of j=0 signals that there
c     are no more columns.  The required length of
c     dattrv(*) is 2*no. of nonzeros + nvars + 1.
c
c     bl(*),bu(*),ind(*)
c     ------------------
c     The values of ind(*) are input parameters that define
c     the form of the bounds for the unknowns x and w.  The values for
c     the bounds are found in the arrays bl(*) and bu(*) as follows.
c
c     For values of j between 1 and nvars,
c          if ind(j)=1, then x(j) .ge. bl(j); bu(j) is not used.
c          if ind(j)=2, then x(j) .le. bu(j); bl(j) is not used.
c          if ind(j)=3, then bl(j) .le. x(j) .le. bu(j),(bl(j)=bu(j) ok)
c          if ind(j)=4, then x(j) is free to have any value,
c          and bl(j), bu(j) are not used.
c
c     For values of i between nvars+1 and nvars+mrelas,
c          if ind(i)=1, then w(i-nvars) .ge. bl(i); bu(i) is not used.
c          if ind(i)=2, then w(i-nvars) .le. bu(i); bl(i) is not used.
c          if ind(i)=3, then bl(i) .le. w(i-nvars) .le. bu(i),
c          (bl(i)=bu(i) is ok).
c          if ind(i)=4, then w(i-nvars) is free to have any value,
c          and bl(i), bu(i) are not used.
c
c     A value of ind(*) not equal to 1,2,3 or 4 is an error.  When
c     ind(i)=3, bl(i) must be .le. bu(i).  The condition bl(i).gt.
c     bu(i) indicates infeasibility and is an error.
c
c     prgopt(*)
c     ---------
c     This array is used to redefine various parameters within splp( ).
c     Frequently, perhaps most of the time, a user will be satisfied
c     and obtain the solutions with no changes to any of these
c     parameters.  To try this, simply set prgopt(1)=1.e0.
c
c     For users with more sophisticated needs, splp( ) provides several
c     options that may be used to take advantage of more detailed
c     knowledge of the problem or satisfy other utilitarian needs.
c     The complete description of how to use this option array to
c     utilize additional subprogram features is found under the
c     heading  of splp( ) subprogram options in the extended
c     usage instructions.
c
c     Briefly, the user should note the following value of the parameter
c     key and the corresponding task or feature desired before turning
c     to that document.
c
c     value     brief statement of purpose for option
c     of key
c     ------    -------------------------------------
c     50        change from a minimization problem to a
c               maximization problem.
c     51        change the amount of printed output.
c               normally, no printed output is obtained.
c     52        redefine the line length and precision used
c               for the printed output.
c     53        redefine the values of lamat and lbm that
c               were discussed above under the heading
c               fortran declarations required.
c     54        redefine the unit number where pages of the sparse
c               data matrix a are stored.  normally, the unit
c               number is 1.
c     55        a computation, partially completed, is
c               being continued.  read the up-to-date
c               partial results from unit number 2.
c     56        redefine the unit number where the partial results
c               are stored.  normally, the unit number is 2.
c     57        save partial results on unit 2 either after
c               maximum iterations or at the optimum.
c     58        redefine the value for the maximum number of
c               iterations.  normally, the maximum number of
c               iterations is 3*(nvars+mrelas).
c     59        provide splp( ) with a starting (feasible)
c               nonsingular basis.  normally, splp( ) starts
c               with the identity matrix columns corresponding
c               to the vector w.
c     60        the user has provided scale factors for the
c               columns of a.  normally, splp( ) computes scale
c               factors that are the reciprocals of the max. norm
c               of each column.
c     61        the user has provided a scale factor
c               for the vector costs.  normally, splp( ) computes
c               a scale factor equal to the reciprocal of the
c               max. norm of the vector costs after the column
c               scaling for the data matrix has been applied.
c     62        size parameters, namely the smallest and
c               largest magnitudes of nonzero entries in
c               the matrix a, are provided.  values noted
c               outside this range are to be considered errors.
c     63        redefine the tolerance required in
c               evaluating residuals for feasibility.
c               normally, this value is set to relpr,
c               where relpr = relative precision of the arithmetic.
c     64        change the criterion for bringing new variables
c               into the basis from the steepest edge (best
c               local move) to the minimum reduced cost.
c     65        redefine the value for the number of iterations
c               between recalculating the error in the primal
c               solution.  normally, this value is equal to ten.
c     66        perform "partial pricing" on variable selection.
c               redefine the value for the number of negative
c               reduced costs to compute (at most) when finding
c               a variable to enter the basis.  normally this
c               value is set to nvars.  this implies that no
c               "partial pricing" is used.
c     67        adjust the tuning factor (normally one) to apply
c               to the primal and dual error estimates.
c     68        pass  information to the  subprogram  fulmat(),
c               provided with the splp() package, so that a fortran
c               two-dimensional array can be used as the argument
c               dattrv(*).
c     69        pass an absolute tolerance to use for the feasibility
c               test when the usual relative error test indicates
c               infeasibility.  the nominal value of this tolerance,
c               tolabs, is zero.
c
c
c  |---------------|
c  |working arrays:|
c  |---------------|
c
c     work(*),lw,
c     iwork(*),liw
c     ------------
c     The arrays work(*) and iwork(*) are respectively floating point
c     and type integer working arrays for splp( ) and its
c     subprograms.  The lengths of these arrays are respectively
c     lw and liw.  These parameters must satisfy the inequalities
c     noted above under the heading "fortran declarations required:"
c     it is an error if either value is too small.
c
c  |----------------------------|
c  |input/output files required:|
c  |----------------------------|
c
c     Fortran unit 1 is used by splp( ) to store the sparse matrix a
c     out of high-speed memory.  A crude
c     upper bound for the amount of information written on unit 1
c     is 6*nz, where nz is the number of nonzero entries in a.
c
c  |-------|
c  |output:|
c  |-------|
c
c     info,primal(*),duals(*)
c     -----------------------
c     The integer flag info indicates why splp( ) has returned to the
c     user.  If info=1 the solution has been computed.  In this case
c     x(j)=primal(j) and w(i)=primal(i+nvars).  The dual variables
c     for the equations a*x=w are in the array duals(i)=dual for
c     equation number i.  The dual value for the component x(j) that
c     has an upper or lower bound (or both) is returned in
c     duals(j+mrelas).  The only other values for info are .lt. 0.
c     The meaning of these values can be found by reading
c     the diagnostic message in the output file, or by looking for
c     error number = (-info) in the extended usage instructions
c     under the heading:
c
c          list of splp( ) error and diagnostic messages.
c
c     bl(*),bu(*),ind(*)
c     ------------------
c     These arrays are output parameters only under the (unusual)
c     circumstances where the stated problem is infeasible, has an
c     unbounded optimum value, or both.  These respective conditions
c     correspond to info=-1,-2 or -3.    See the extended
c     usage instructions for further details.
c
c     ibasis(i),i=1,...,mrelas
c     ------------------------
c     This array contains the indices of the variables that are
c     in the active basis set at the solution (info=1).  A value
c     of ibasis(i) between 1 and nvars corresponds to the variable
c     x(ibasis(i)).  A value of ibasis(i) between nvars+1 and nvars+
c     mrelas corresponds to the variable w(ibasis(i)-nvars).
c
c *long description:
c
c     subroutine lpsplp(usrmat,mrelas,nvars,costs,prgopt,dattrv,
c    *           bl,bu,ind,info,primal,duals,ibasis,work,lw,iwork,liw)
c
c     |------------|
c     |introduction|
c     |------------|
c     The subprogram splp( ) solves a linear optimization problem.
c     The problem statement is as follows
c
c                         minimize (transpose of costs)*x
c                         subject to a*x=w.
c
c     The entries of the unknowns x and w may have simple lower or
c     upper bounds (or both), or be free to take on any value.  By
c     setting the bounds for x and w, the user is imposing the con-
c     straints of the problem.
c
c     (The problem may also be stated as a maximization
c     problem.  This is done by means of input in the option array
c     prgopt(*).)  The matrix a has mrelas rows and nvars columns.  The
c     vectors costs, x, and w respectively have nvars, nvars, and
c     mrelas number of entries.
c
c     the input for the problem includes the problem dimensions,
c     mrelas and nvars, the array costs(*), data for the matrix
c     a, and the bound information for the unknowns x and w, bl(*),
c     bu(*), and ind(*).
c
c     the output from the problem (when output flag info=1) includes
c     optimal values for x and w in primal(*), optimal values for
c     dual variables of the equations a*x=w and the simple bounds
c     on x in  duals(*), and the indices of the basic columns in
c     ibasis(*).
c
c  |------------------------------|
c  |fortran declarations required:|
c  |------------------------------|
c
c     dimension costs(nvars),prgopt(*),dattrv(*),
c    *bl(nvars+mrelas),bu(nvars+mrelas),ind(nvars+mrelas),
c    *primal(nvars+mrelas),duals(mrelas+nvars),ibasis(nvars+mrelas),
c    *work(lw),iwork(liw)
c
c     external usrmat (or 'name', if user provides the subprogram)
c
c     The dimensions of prgopt(*) and dattrv(*) must be at least 1.
c     The exact lengths will be determined by user-required options and
c     data transferred to the subprogram usrmat( ) ( or 'name').
c
c     The values of lw and liw, the lengths of the arrays work(*)
c     and iwork(*), must satisfy the inequalities
c
c               lw .ge. 4*nvars+ 8*mrelas+lamat+  lbm
c               liw.ge.   nvars+11*mrelas+lamat+2*lbm
c
c     It is an error if they do not both satisfy these inequalities.
c     (The subprogram will inform the user of the required lengths
c     if either lw or liw is wrong.)  The values of lamat and lbm
c     nominally are
c
c               lamat=4*nvars+7
c     and       lbm  =8*mrelas
c
c     these values will be as shown unless the user changes them by
c     means of input in the option array prgopt(*).  The value of lamat
c     determines the length of the sparse matrix "staging" area.
c     For reasons of efficiency the user may want to increase the value
c     of lamat.  The value of lbm determines the amount of storage
c     available to decompose and update the active basis matrix.
c     Due to exhausting the working space because of fill-in,
c     it may be necessary for the user to increase the value of lbm.
c     (If this situation occurs an informative diagnostic is printed
c     and a value of info=-28 is obtained as an output parameter.)
c
c  |------|
c  |input:|
c  |------|
c
c     mrelas,nvars
c     ------------
c     These parameters are respectively the number of constraints (the
c     linear relations a*x=w that the unknowns x and w are to satisfy)
c     and the number of entries in the vector x.  Both must be .ge. 1.
c     Other values are errors.
c
c     costs(*)
c     --------
c     The nvars entries of this array are the coefficients of the
c     linear objective function.  The value costs(j) is the
c     multiplier for variable j of the unknown vector x.  Each
c     entry of this array must be defined.  This array can be changed
c     by the user between restarts.  See options with key=55,57 for
c     details of checkpointing and restarting.
c
c     usrmat
c     ------
c     This is the name of a specific subprogram in the splp( ) package
c     that is used to define the matrix entries when this data is passed
c     to splp( ) as a linear array.  In this usage mode of splp( )
c     the user gives information about the nonzero entries of a
c     in dattrv(*) as given under the description of that parameter.
c     The name usrmat must appear in a fortran external statement.
c     Users who are passing the matrix data with usrmat( ) can skip
c     directly to the description of the input parameter dattrv(*).
c     Also see option 68 for passing the constraint matrix data using
c     a standard fortran two-dimensional array.
c
c     If the user chooses to provide a subprogram 'name'( ) to
c     define the matrix a, then dattrv(*) may be used to pass floating
c     point data from the user's program unit to the subprogram
c     'name'( ). The content of dattrv(*) is not changed in any way.
c
c     The subprogram 'name'( ) can be of the user's choice
c     but it must meet fortran standards and it must appear in a
c     fortran external statement.  The first statement of the subprogram
c     has the form
c
c          subroutine lp'name'(i,j,aij, indcat, prgopt, dattrv, iflag)
c
c     The variables i,j, indcat, iflag(10) are type integer,
c          while  aij, prgopt(*),dattrv(*) are type real.
c
c     The user interacts with the contents of iflag(*) to
c     direct the appropriate action.  The algorithmic steps are
c     as follows.
c
c          test iflag(1).
c
c             if(iflag(1).eq.1) then
c
c               Initialize the necessary pointers and data
c               for defining the matrix a.  The contents
c               of iflag(k), k=2,...,10, may be used for
c               storage of the pointers.  This array remains intact
c               between calls to 'name'( ) by splp( ).
c               return
c
c             end if
c
c             if(iflag(1).eq.2) then
c
c               Define one set of values for i,j,aij, and indcat.
c               each nonzero entry of a must be defined this way.
c               These values can be defined in any convenient order.
c               (It is most efficient to define the data by
c               columns in the order 1,...,nvars; within each
c               column define the entries in the order 1,...,mrelas.)
c               if this is the last matrix value to be
c               defined or updated, then set iflag(1)=3.
c               (When i and j are positive and respectively no larger
c               than mrelas and nvars, the value of aij is used to
c               define (or update) row i and column j of a.)
c               return
c
c             end if
c
c               end
c
c     Remarks:  The values of i and j are the row and column
c               indices for the nonzero entries of the matrix a.
c               The value of this entry is aij.
c               Set indcat=0 if this value defines that entry.
c               Set indcat=1 if this entry is to be updated,
c                            new entry=old entry+aij.
c               A value of i not between 1 and mrelas, a value of j
c               not between 1 and nvars, or a value of indcat
c               not equal to 0 or 1 are each errors.
c
c               The contents of iflag(k), k=2,...,10, can be used to
c               remember the status (of the process of defining the
c               matrix entries) between calls to 'name'( ) by splp( ).
c               On entry to 'name'( ), only the values 1 or 2 will be
c               in iflag(1).  More than 2*nvars*mrelas definitions of
c               the matrix elements is considered an error because
c               it suggests an infinite loop in the user-written
c               subprogram 'name'( ).  Any matrix element not
c               provided by 'name'( ) is defined to be zero.
c
c               The real arrays prgopt(*) and dattrv(*) are passed as
c               arguments directly from splp( ) to 'name'( ).
c               The array prgopt(*) contains any user-defined program
c               options.  In this usage mode the array dattrv(*) may
c               now contain any (type real) data that the user needs
c               to define the matrix a.  Both arrays prgopt(*) and
c               dattrv(*) remain intact between calls to 'name'( )
c               by splp( ).
c     Here is a subprogram that communicates the matrix values for a,
c     as represented in dattrv(*), to splp( ).  This subprogram,
c     called usrmat( ), is included as part of the splp( ) package.
c     This subprogram 'decodes' the array dattrv(*) and defines the
c     nonzero entries of the matrix  a for splp( ) to store.  This
c     listing is presented here as a guide and example
c     for the users who find it necessary to write their own subroutine lp
c     for this purpose.  The contents of dattrv(*) are given below in
c     the description of that parameter.
c
c     subroutine lpusrmat(i,j,aij, indcat,prgopt,dattrv,iflag)
c     dimension prgopt(*),dattrv(*),iflag(10)
c
c     if(iflag(1).eq.1) then
c
c     This is the initialization step.  The values of iflag(k),k=2,3,4,
c     are respectively the column index, the row index (or the next col.
c     index), and the pointer to the matrix entry's value within
c     dattrv(*).  Also check (dattrv(1)=0.) signifying no data.
c          if(dattrv(1).eq.0.) then
c          i = 0
c          j = 0
c          iflag(1) = 3
c          else
c          iflag(2)=-dattrv(1)
c          iflag(3)= dattrv(2)
c          iflag(4)= 3
c          end if
c
c          return
c     else
c          j=iflag(2)
c          i=iflag(3)
c          l=iflag(4)
c          if(i.eq.0) then
c
c     Signal that all of the nonzero entries have been defined.
c               iflag(1)=3
c               return
c          else if(i.lt.0) then
c
c     Signal that a switch is made to a new column.
c               j=-i
c               i=dattrv(l)
c               l=l+1
c          end if
c
c          aij=dattrv(l)
c
c     Update the indices and pointers for the next entry.
c          iflag(2)=j
c          iflag(3)=dattrv(l+1)
c          iflag(4)=l+2
c
c     Indcat=0 denotes that entries of the matrix are assigned the
c     values from dattrv(*).  No accumulation is performed.
c          indcat=0
c          return
c     end if
c     end
c
c     dattrv(*)
c     ---------
c     If the user chooses to use the provided subprogram usrmat( ) then
c     the array dattrv(*) contains data for the matrix a as follows:
c     each column (numbered j) requires (floating point) data con-
c     sisting of the value (-j) followed by pairs of values.  Each pair
c     consists of the row index immediately followed by the value
c     of the matrix at that entry.  A value of j=0 signals that there
c     are no more columns.  (See "example of splp( ) usage," below.)
c     The dimension of dattrv(*) must be 2*no. of nonzeros
c     + nvars + 1 in this usage.  No checking of the array
c     length is done by the subprogram package.
c
c     If the save/restore feature is in use (see options with
c     key=55,57 for details of checkpointing and restarting)
c     usrmat( ) can be used to redefine entries of the matrix.
c     The matrix entries are redefined or overwritten.  No accum-
c     ulation is performed.
c     Any other nonzero entry of a, defined in a previous call lpto
c     splp( ), remain intact.
c
c     bl(*),bu(*),ind(*)
c     ------------------
c     The values of ind(*) are input parameters that define
c     the form of the bounds for the unknowns x and w.  The values for
c     the bounds are found in the arrays bl(*) and bu(*) as follows.
c
c     For values of j between 1 and nvars,
c          if ind(j)=1, then x(j) .ge. bl(j); bu(j) is not used.
c          if ind(j)=2, then x(j) .le. bu(j); bl(j) is not used.
c          if ind(j)=3, then bl(j) .le. x(j) .le. bu(j),(bl(j)=bu(j) ok)
c          if ind(j)=4, then x(j) is free to have any value,
c          and bl(j), bu(j) are not used.
c
c     For values of i between nvars+1 and nvars+mrelas,
c          if ind(i)=1, then w(i-nvars) .ge. bl(i); bu(i) is not used.
c          if ind(i)=2, then w(i-nvars) .le. bu(i); bl(i) is not used.
c          if ind(i)=3, then bl(i) .le. w(i-nvars) .le. bu(i),
c          (bl(i)=bu(i) is ok).
c          if ind(i)=4, then w(i-nvars) is free to have any value,
c          and bl(i), bu(i) are not used.
c
c     A value of ind(*) not equal to 1,2,3 or 4 is an error.  When
c     ind(i)=3, bl(i) must be .le. bu(i).  The condition bl(i).gt.
c     bu(i) indicates infeasibility and is an error.  These
c     arrays can be changed by the user between restarts.  See
c     options with key=55,57 for details of checkpointing and
c     restarting.
c
c     prgopt(*)
c     ---------
c     This array is used to redefine various parameters within splp( ).
c     Frequently, perhaps most of the time, a user will be satisfied
c     and obtain the solutions with no changes to any of these
c     parameters.  To try this, simply set prgopt(1)=1.e0.
c
c     For users with more sophisticated needs, splp( ) provides several
c     options that may be used to take advantage of more detailed
c     knowledge of the problem or satisfy other utilitarian needs.
c     The complete description of how to use this option array to
c     utilize additional subprogram features is found under the
c     heading "usage of splp( ) subprogram options."
c
c     Briefly, the user should note the following value of the parameter
c     key and the corresponding task or feature desired before turning
c     to that section.
c
c     Value     Brief statement of purpose for option
c     of key
c     ------    -------------------------------------
c     50        change from a minimization problem to a
c               maximization problem.
c     51        change the amount of printed output.
c               normally, no printed output is obtained.
c     52        redefine the line length and precision used
c               for the printed output.
c     53        redefine the values of lamat and lbm that
c               were discussed above under the heading
c               fortran declarations required.
c     54        redefine the unit number where pages of the sparse
c               data matrix a are stored.  normally, the unit
c               number is 1.
c     55        a computation, partially completed, is
c               being continued.  read the up-to-date
c               partial results from unit number 2.
c     56        redefine the unit number where the partial results
c               are stored.  normally, the unit number is 2.
c     57        save partial results on unit 2 either after
c               maximum iterations or at the optimum.
c     58        redefine the value for the maximum number of
c               iterations.  normally, the maximum number of
c               iterations is 3*(nvars+mrelas).
c     59        provide splp( ) with a starting (feasible)
c               nonsingular basis.  normally, splp( ) starts
c               with the identity matrix columns corresponding
c               to the vector w.
c     60        the user has provided scale factors for the
c               columns of a.  normally, splp( ) computes scale
c               factors that are the reciprocals of the max. norm
c               of each column.
c     61        the user has provided a scale factor
c               for the vector costs.  normally, splp( ) computes
c               a scale factor equal to the reciprocal of the
c               max. norm of the vector costs after the column
c               scaling for the data matrix has been applied.
c     62        size parameters, namely the smallest and
c               largest magnitudes of nonzero entries in
c               the matrix a, are provided.  values noted
c               outside this range are to be considered errors.
c     63        redefine the tolerance required in
c               evaluating residuals for feasibility.
c               normally, this value is set to the value relpr,
c               where relpr = relative precision of the arithmetic.
c     64        change the criterion for bringing new variables
c               into the basis from the steepest edge (best
c               local move) to the minimum reduced cost.
c     65        redefine the value for the number of iterations
c               between recalculating the error in the primal
c               solution.  normally, this value is equal to ten.
c     66        perform "partial pricing" on variable selection.
c               redefine the value for the number of negative
c               reduced costs to compute (at most) when finding
c               a variable to enter the basis.  normally this
c               value is set to nvars.  this implies that no
c               "partial pricing" is used.
c     67        adjust the tuning factor (normally one) to apply
c               to the primal and dual error estimates.
c     68        pass  information to the  subprogram  fulmat(),
c               provided with the splp() package, so that a fortran
c               two-dimensional array can be used as the argument
c               dattrv(*).
c     69        pass an absolute tolerance to use for the feasibility
c               test when the usual relative error test indicates
c               infeasibility.  the nominal value of this tolerance,
c               tolabs, is zero.
c
c
c  |---------------|
c  |working arrays:|
c  |---------------|
c
c     work(*),lw,
c     iwork(*),liw
c     ------------
c     the arrays work(*) and iwork(*) are respectively floating point
c     and type integer working arrays for splp( ) and its
c     subprograms.  the lengths of these arrays are respectively
c     lw and liw.  these parameters must satisfy the inequalities
c     noted above under the heading "fortran declarations required."
c     it is an error if either value is too small.
c
c  |----------------------------|
c  |input/output files required:|
c  |----------------------------|
c
c     fortran unit 1 is used by splp( ) to store the sparse matrix a
c     out of high-speed memory.  this direct access file is opened
c     within the package under the following two conditions.
c     1. when the save/restore feature is used.  2. when the
c     constraint matrix is so large that storage out of high-speed
c     memory is required.  the user may need to close unit 1
c     (with deletion from the job step) in the main program unit
c     when several calls are made to splp( ).  a crude
c     upper bound for the amount of information written on unit 1
c     is 6*nz, where nz is the number of nonzero entries in a.
c     the unit number may be redefined to any other positive value
c     by means of input in the option array prgopt(*).
c
c     fortran unit 2 is used by splp( ) only when the save/restore
c     feature is desired.  normally this feature is not used.  it is
c     activated by means of input in the option array prgopt(*).
c     on some computer systems the user may need to open unit
c     2 before executing a call lpto splp( ).  this file is type
c     sequential and is unformatted.
c
c     fortran unit=lpi1mach(2) (check local setting) is used by splp( )
c     when the printed output feature (key=51) is used.  normally
c     this feature is not used.  it is activated by input in the
c     options array prgopt(*).  for many computer systems lpi1mach(2)=6.
c
c  |-------|
c  |output:|
c  |-------|
c
c     info,primal(*),duals(*)
c     -----------------------
c     the integer flag info indicates why splp( ) has returned to the
c     user.  if info=1 the solution has been computed.  in this case
c     x(j)=primal(j) and w(i)=primal(i+nvars).  the dual variables
c     for the equations a*x=w are in the array duals(i)=dual for
c     equation number i.  the dual value for the component x(j) that
c     has an upper or lower bound (or both) is returned in
c     duals(j+mrelas).  the only other values for info are .lt. 0.
c     the meaning of these values can be found by reading
c     the diagnostic message in the output file, or by looking for
c     error number = (-info) under the heading "list of splp( ) error
c     and diagnostic messages."
c     the diagnostic messages are printed using the error processing
c     subprogram xermsg( ) with error category level=1.
c     see the document "brief instr. for using the sandia math.
c     subroutine lplibrary," sand79-2382, nov., 1980, for further inform-
c     ation about resetting the usual response to a diagnostic message.
c
c     bl(*),bu(*),ind(*)
c     ------------------
c     these arrays are output parameters only under the (unusual)
c     circumstances where the stated problem is infeasible, has an
c     unbounded optimum value, or both.  these respective conditions
c     correspond to info=-1,-2 or -3.  for info=-1 or -3 certain comp-
c     onents of the vectors x or w will not satisfy the input bounds.
c     if component j of x or component i of w does not satisfy its input
c     bound because of infeasibility, then ind(j)=-4 or ind(i+nvars)=-4,
c     respectively.  for info=-2 or -3 certain
c     components of the vector x could not be used as basic variables
c     because the objective function would have become unbounded.
c     in particular if component j of x corresponds to such a variable,
c     then ind(j)=-3.  further, if the input value of ind(j)
c                      =1, then bu(j)=bl(j);
c                      =2, then bl(j)=bu(j);
c                      =4, then bl(j)=0.,bu(j)=0.
c
c     (the j-th variable in x has been restricted to an appropriate
c     feasible value.)
c     the negative output value for ind(*) allows the user to identify
c     those constraints that are not satisfied or those variables that
c     would cause unbounded values of the objective function.  note
c     that the absolute value of ind(*), together with bl(*) and bu(*),
c     are valid input to splp( ).  in the case of infeasibility the
c     sum of magnitudes of the infeasible values is minimized.  thus
c     one could reenter splp( ) with these components of x or w now
c     fixed at their present values.  this involves setting
c     the appropriate components of ind(*) = 3, and bl(*) = bu(*).
c
c     ibasis(i),i=1,...,mrelas
c     ------------------------
c     this array contains the indices of the variables that are
c     in the active basis set at the solution (info=1).  a value
c     of ibasis(i) between 1 and nvars corresponds to the variable
c     x(ibasis(i)).  a value of ibasis(i) between nvars+1 and nvars+
c     mrelas corresponds to the variable w(ibasis(i)-nvars).
c
c     computing with the matrix a after calling splp( )
c     -------------------------------------------------
c     following the return from splp( ), nonzero entries of the mrelas
c     by nvars matrix a are available for usage by the user.  the method
c     for obtaining the next nonzero in column j with a row index
c     strictly greater than i in value, is completed by executing
c
c         call lppnnzrs(i,aij,iplace,work,iwork,j)
c
c     the value of i is also an output parameter.  if i.le.0 on output,
c     then there are no more nonzeroes in column j.  if i.gt.0, the
c     output value for component number i of column j is in aij.  the
c     parameters work(*) and iwork(*) are the same arguments as in the
c     call lpto splp( ).  the parameter iplace is a single integer
c     working variable.
c
c     the data structure used for storage of the matrix a within splp( )
c     corresponds to sequential storage by columns as defined in
c     sand78-0785.  note that the names of the subprograms lnnzrs(),
c     lchngs(),linitm(),lloc(),lrwpge(), and lrwvir() have been
c     changed to pnnzrs(),pchngs(),pinitm(),lpiploc(),prwpge(), and
c     prwvir() respectively.  the error processing subprogram lerror()
c     is no longer used; xermsg() is used instead.
c
c  |-------------------------------|
c  |subprograms required by splp( )|
c  |-------------------------------|
c     called by splp() are splpmn(),splpup(),spinit(),spopt(),
c                          splpdm(),splpce(),spincw(),splpfl(),
c                          splpfe(),splpmu().
c
c     error processing subprograms xermsg(),lpi1mach(),lpr1mach()
c
c     sparse matrix subprograms pnnzrs(),pchngs(),prwpge(),prwvir(),
c                               pinitm(),lpiploc()
c
c     mass storage file subprograms sopenm(),sclosm(),sreadp(),swritp()
c
c     basic linear algebra subprograms scopy(),lpsasum(),lpsdot()
c
c     sparse matrix basis handling subprograms la05as(),la05bs(),
c                                             la05cs(),la05ed(),mc20as()
c
c     vector output subprograms svout(),ivout()
c
c     machine-sensitive subprograms lpi1mach( ),lpr1mach( ),
c                                sopenm(),sclosm(),sreadp(),swritp().
c     common block used
c     -----------------
c     /la05ds/ small,lp,lenl,lenu,ncp,lrow,lcol
c     see the document aere-r8269 for further details.
c    |------------------------|
c    |example of splp( ) usage|
c    |------------------------|
c     program lpex
c     the optimization problem is to find x1, x2, x3 that
c     minimize x1 + x2 + x3, x1.ge.0, x2.ge.0, x3 unconstrained.
c
c     the unknowns x1,x2,x3 are to satisfy constraints
c
c        x1 -3*x2 +4*x3 = 5
c        x1 -2*x2     .le.3
c            2*x2 - x3.ge.4
c
c     we first define the dependent variables
c          w1=x1 -3*x2 +4*x3
c          w2=x1- 2*x2
c          w3=    2*x2 -x3
c
c     we now show how to use splp( ) to solve this linear optimization
c     problem.  each required step will be shown in this example.
c     dimension costs(03),prgopt(01),dattrv(18),bl(06),bu(06),ind(06),
c    *primal(06),duals(06),ibasis(06),work(079),iwork(103)
c
c     external usrmat
c     mrelas=3
c     nvars=3
c
c     define the array costs(*) for the objective function.
c     costs(01)=1.
c     costs(02)=1.
c     costs(03)=1.
c
c     place the nonzero information about the matrix in dattrv(*).
c     define col. 1:
c     dattrv(01)=-1
c     dattrv(02)=1
c     dattrv(03)=1.
c     dattrv(04)=2
c     dattrv(05)=1.
c
c     define col. 2:
c     dattrv(06)=-2
c     dattrv(07)=1
c     dattrv(08)=-3.
c     dattrv(09)=2
c     dattrv(10)=-2.
c     dattrv(11)=3
c     dattrv(12)=2.
c
c     define col. 3:
c     dattrv(13)=-3
c     dattrv(14)=1
c     dattrv(15)=4.
c     dattrv(16)=3
c     dattrv(17)=-1.
c
c     dattrv(18)=0
c
c     constrain x1,x2 to be nonnegative. let x3 have no bounds.
c     bl(1)=0.
c     ind(1)=1
c     bl(2)=0.
c     ind(2)=1
c     ind(3)=4
c
c     constrain w1=5,w2.le.3, and w3.ge.4.
c     bl(4)=5.
c     bu(4)=5.
c     ind(4)=3
c     bu(5)=3.
c     ind(5)=2
c     bl(6)=4.
c     ind(6)=1
c
c     indicate that no modifications to options are in use.
c     prgopt(01)=1
c
c     define the working array lengths.
c     lw=079
c     liw=103
c     call lpsplp(usrmat,mrelas,nvars,costs,prgopt,dattrv,
c    *bl,bu,ind,info,primal,duals,ibasis,work,lw,iwork,liw)
c
c     calculate val, the minimal value of the objective function.
c     val=lpsdot(nvars,costs,1,primal,1)
c
c     stop
c     end
c    |------------------------|
c    |end of example of usage |
c    |------------------------|
c
c    |------------------------------------|
c    |usage of splp( ) subprogram options.|
c    |------------------------------------|
c
c     users frequently have a large variety of requirements for linear
c     optimization software.  allowing for these varied requirements
c     is at cross purposes with the desire to keep the usage of splp( )
c     as simple as possible. one solution to this dilemma is as follows.
c     (1) provide a version of splp( ) that solves a wide class of
c     problems and is easy to use. (2) identify parameters within splp()
c     that certain users may want to change.  (3) provide a means
c     of changing any selected number of these parameters that does
c     not require changing all of them.
c
c     changing selected parameters is done by requiring
c     that the user provide an option array, prgopt(*), to splp( ).
c     the contents of prgopt(*) inform splp( ) of just those options
c     that are going to be modified within the total set of possible
c     parameters that can be modified.  the array prgopt(*) is a linked
c     list consisting of groups of data of the following form
c
c          link
c          key
c          switch
c          data set
c
c     that describe the desired options.  the parameters link, key and
c     switch are each one word and are always required.  the data set
c     can be comprised of several words or can be empty.  the number of
c     words in the data set for each option depends on the value of
c     the parameter key.
c
c     the value of link points to the first entry of the next group
c     of data within prgopt(*).  the exception is when there are no more
c     options to change.  in that case, link=1 and the values for key,
c     switch and data set are not referenced.  the general layout of
c     prgopt(*) is as follows:
c          ...prgopt(1)=link1 (link to first entry of next group)
c          .  prgopt(2)=key1 (key to the option change)
c          .  prgopt(3)=switch1 (on/off switch for the option)
c          .  prgopt(4)=data value
c          .       .
c          .       .
c          .       .
c          ...prgopt(link1)=link2 (link to first entry of next group)
c          .  prgopt(link1+1)=key2 (key to option change)
c          .  prgopt(link1+2)=switch2 (on/off switch for the option)
c          .  prgopt(link1+3)=data value
c          ...     .
c          .       .
c          .       .
c          ...prgopt(link)=1 (no more options to change)
c
c     a value of link that is .le.0 or .gt. 10000 is an error.
c     in this case splp( ) returns with an error message, info=-14.
c     this helps prevent using invalid but positive values of link that
c     will probably extend beyond the program limits of prgopt(*).
c     unrecognized values of key are ignored.  if the value of switch is
c     zero then the option is turned off.  for any other value of switch
c     the option is turned on.  this is used to allow easy changing of
c     options without rewriting prgopt(*).  the order of the options is
c     arbitrary and any number of options can be changed with the
c     following restriction.  to prevent cycling in processing of the
c     option array prgopt(*), a count of the number of options changed
c     is maintained.  whenever this count exceeds 1000 an error message
c     (info=-15) is printed and the subprogram returns.
c
c     in the following description of the options, the value of
c     latp indicates the amount of additional storage that a particular
c     option requires.  the sum of all of these values (plus one) is
c     the minimum dimension for the array prgopt(*).
c
c     if a user is satisfied with the nominal form of splp( ),
c     set prgopt(1)=1 (or prgopt(1)=1.e0).
c
c     options:
c
c -----key = 50.  change from a minimization problem to a maximization
c     problem.
c     if switch=0  option is off; solve minimization problem.
c              =1  option is on; solve maximization problem.
c     data set =empty
c     latp=3
c
c -----key = 51.  change the amount of printed output.  the nominal form
c     of splp( ) has no printed output.
c     the first level of output (switch=1) includes
c
c     (1) minimum dimensions for the arrays costs(*),bl(*),bu(*),ind(*),
c         primal(*),duals(*),ibasis(*), and prgopt(*).
c     (2) problem dimensions mrelas,nvars.
c     (3) the types of and values for the bounds on x and w,
c         and the values of the components of the vector costs.
c     (4) whether optimization problem is minimization or
c         maximization.
c     (5) whether steepest edge or smallest reduced cost criteria used
c         for exchanging variables in the revised simplex method.
c
c     whenever a solution has been found, (info=1),
c
c     (6) the value of the objective function,
c     (7) the values of the vectors x and w,
c     (8) the dual variables for the constraints a*x=w and the
c         bounded components of x,
c     (9) the indices of the basic variables,
c    (10) the number of revised simplex method iterations,
c    (11) the number of full decompositions of the basis matrix.
c
c     the second level of output (switch=2) includes all for switch=1
c     plus
c
c    (12) the iteration number,
c    (13) the column number to enter the basis,
c    (14) the column number to leave the basis,
c    (15) the length of the step taken.
c
c     the third level of output (switch=3) includes all for switch=2
c     plus
c    (16) critical quantities required in the revised simplex method.
c          this output is rather voluminous.  it is intended to be used
c          as a diagnostic tool in case of a failure in splp( ).
c
c     if switch=0  option is off; no printed output.
c              =1  summary output.
c              =2  lots of output.
c              =3  even more output.
c     data set =empty
c     latp=3
c
c -----key = 52.  redefine the parameter, idigit, which determines the
c     format and precision used for the printed output.  in the printed
c     output, at least abs(idigit) decimal digits per number is printed.
c     if idigit.lt.0, 72 printing columns are used.  if idigit.gt.0, 133
c     printing columns are used.
c     if switch=0  option is off; idigit=-4.
c              =1  option is on.
c     data set =idigit
c     latp=4
c
c -----key = 53.  redefine lamat and lbm, the lengths of the portions of
c     work(*) and iwork(*) that are allocated to the sparse matrix
c     storage and the sparse linear equation solver, respectively.
c     lamat must be .ge. nvars+7 and lbm must be positive.
c     if switch=0  option is off; lamat=4*nvars+7
c                                 lbm  =8*mrelas.
c              =1  option is on.
c     data set =lamat
c               lbm
c     latp=5
c
c -----key = 54. redefine ipagef, the file number where the pages of the
c     sparse data matrix are stored.  ipagef must be positive and
c     different from isave (see option 56).
c     if switch=0  option is off; ipagef=1.
c              =1  option is on.
c     data set =ipagef
c     latp=4
c
c -----key = 55.  partial results have been computed and stored on unit
c     number isave (see option 56), during a previous run of
c     splp( ).  this is a continuation from these partial results.
c     the arrays costs(*),bl(*),bu(*),ind(*) do not have to have
c     the same values as they did when the checkpointing occurred.
c     this feature makes it possible for the user to do certain
c     types of parameter studies such as changing costs and varying
c     the constraints of the problem.  this file is rewound both be-
c     fore and after reading the partial results.
c     if switch=0  option is off; start a new problem.
c              =1  option is on; continue from partial results
c                  that are stored in file isave.
c     data set = empty
c     latp=3
c
c -----key = 56.  redefine isave, the file number where the partial
c     results are stored (see option 57).  isave must be positive and
c     different from ipagef (see option 54).
c     if switch=0  option is off; isave=2.
c              =1  option is on.
c     data set =isave
c     latp=4
c
c -----key = 57.  save the partial results after maximum number of
c     iterations, maxitr, or at the optimum.  when this option is on,
c     data essential to continuing the calculation is saved on a file
c     using a fortran binary write operation.  the data saved includes
c     all the information about the sparse data matrix a.  also saved
c     is information about the current basis.  nominally the partial
c     results are saved on fortran unit 2.  this unit number can be
c     redefined (see option 56).  if the save option is on,
c     this file must be opened (or declared) by the user prior to the
c     call lpto splp( ).  a crude upper bound for the number of words
c     written to this file is 6*nz.  here nz= number of nonzeros in a.
c     if switch=0  option is off; do not save partial results.
c              =1  option is on; save partial results.
c     data set = empty
c     latp=3
c
c -----key = 58.  redefine the maximum number of iterations, maxitr, to
c     be taken before returning to the user.
c     if switch=0  option is off; maxitr=3*(nvars+mrelas).
c              =1  option is on.
c     data set =maxitr
c     latp=4
c
c -----key = 59.  provide splp( ) with exactly mrelas indices which
c     comprise a feasible, nonsingular basis.  the basis must define a
c     feasible point: values for x and w such that a*x=w and all the
c     stated bounds on x and w are satisfied.  the basis must also be
c     nonsingular.  the failure of either condition will cause an error
c     message (info=-23 or =-24, respectively).  normally, splp( ) uses
c     identity matrix columns which correspond to the components of w.
c     this option would normally not be used when restarting from
c     a previously saved run (key=57).
c     in numbering the unknowns,
c     the components of x are numbered (1-nvars) and the components
c     of w are numbered (nvars+1)-(nvars+mrelas).  a value for an
c     index .le. 0 or .gt. (nvars+mrelas) is an error (info=-16).
c     if switch=0  option is off; splp( ) chooses the initial basis.
c              =1  option is on; user provides the initial basis.
c     data set =mrelas indices of basis; order is arbitrary.
c     latp=mrelas+3
c
c -----key = 60.  provide the scale factors for the columns of the data
c     matrix a.  normally, splp( ) computes the scale factors as the
c     reciprocals of the max. norm of each column.
c     if switch=0  option is off; splp( ) computes the scale factors.
c              =1  option is on; user provides the scale factors.
c     data set =scaling for column j, j=1,nvars; order is sequential.
c     latp=nvars+3
c
c -----key = 61.  provide a scale factor, costsc, for the vector of
c     costs.  normally, splp( ) computes this scale factor to be the
c     reciprocal of the max. norm of the vector costs after the column
c     scaling has been applied.
c     if switch=0  option is off; splp( ) computes costsc.
c              =1  option is on; user provides costsc.
c     data set =costsc
c     latp=4
c
c -----key = 62.  provide size parameters, asmall and abig, the smallest
c     and largest magnitudes of nonzero entries in the data matrix a,
c     respectively.  when this option is on, splp( ) will check the
c     nonzero entries of a to see if they are in the range of asmall and
c     abig.  if an entry of a is not within this range, splp( ) returns
c     an error message, info=-22. both asmall and abig must be positive
c     with asmall .le. abig.  otherwise,  an error message is returned,
c     info=-17.
c     if switch=0  option is off; no checking of the data matrix is done
c              =1  option is on; checking is done.
c     data set =asmall
c               abig
c     latp=5
c
c -----key = 63.  redefine the relative tolerance, tolls, used in
c     checking if the residuals are feasible.  normally,
c     tolls=relpr, where relpr is the machine precision.
c     if switch=0  option is off; tolls=relpr.
c              =1  option is on.
c     data set =tolls
c     latp=4
c
c -----key = 64. use the minimum reduced cost pricing strategy to choose
c     columns to enter the basis.  normally, splp( ) uses the steepest
c     edge pricing strategy which is the best local move.  the steepest
c     edge pricing strategy generally uses fewer iterations than the
c     minimum reduced cost pricing, but each iteration costs more in the
c     number of calculations done.  the steepest edge pricing is
c     considered to be more efficient.  however, this is very problem
c     dependent.  that is why splp( ) provides the option of either
c     pricing strategy.
c     if switch=0  option is off; steepest option edge pricing is used.
c              =1  option is on; minimum reduced cost pricing is used.
c     data set =empty
c     latp=3
c
c -----key = 65.  redefine mxitbr, the number of iterations between
c     recalculating the error in the primal solution.  normally, mxitbr
c     is set to 10.  the error in the primal solution is used to monitor
c     the error in solving the linear system.  this is an expensive
c     calculation and every tenth iteration is generally often enough.
c     if switch=0  option is off; mxitbr=10.
c              =1  option is on.
c     data set =mxitbr
c     latp=4
c
c -----key = 66.  redefine npp, the number of negative reduced costs
c     (at most) to be found at each iteration of choosing
c     a variable to enter the basis.  normally npp is set
c     to nvars which implies that all of the reduced costs
c     are computed at each such step.  this "partial
c     pricing" may very well increase the total number
c     of iterations required.  however it decreases the
c     number of calculations at each iteration.
c     therefore the effect on overall efficiency is quite
c     problem-dependent.
c
c     if switch=0 option is off; npp=nvars
c              =1 option is on.
c     data set =npp
c     latp=4
c
c -----key =  67.  redefine the tuning factor (phi) used to scale the
c     error estimates for the primal and dual linear algebraic systems
c     of equations.  normally, phi = 1.e0, but in some environments it
c     may be necessary to reset phi to the range 0.001-0.01.  this is
c     particularly important for machines with short word lengths.
c
c     if switch = 0 option is off; phi=1.e0.
c               = 1 option is on.
c     data set  = phi
c     latp=4
c
c -----key = 68.  used together with the subprogram fulmat(), provided
c     with the splp() package, for passing a standard fortran two-
c     dimensional array containing the constraint matrix.  thus the sub-
c     program fulmat must be declared in a fortran external statement.
c     the two-dimensional array is passed as the argument dattrv.
c     the information about the array and problem dimensions are passed
c     in the option array prgopt(*).  it is an error if fulmat() is
c     used and this information is not passed in prgopt(*).
c
c     if switch = 0 option is off; this is an error is fulmat() is
c                                  used.
c               = 1 option is on.
c     data set  = ia = row dimension of two-dimensional array.
c                 mrelas = number of constraint equations.
c                 nvars  = number of dependent variables.
c     latp = 6
c -----key = 69.  normally a relative tolerance (tolls, see option 63)
c     is used to decide if the problem is feasible.  if this test fails
c     an absolute test will be applied using the value tolabs.
c     nominally tolabs = zero.
c     if switch = 0 option is off; tolabs = zero.
c               = 1 option is on.
c     data set  = tolabs
c     latp = 4
c
c    |-----------------------------|
c    |example of option array usage|
c    |-----------------------------|
c     to illustrate the usage of the option array, let us suppose that
c     the user has the following nonstandard requirements:
c
c          a) wants to change from minimization to maximization problem.
c          b) wants to limit the number of simplex steps to 100.
c          c) wants to save the partial results after 100 steps on
c             fortran unit 2.
c
c     after these 100 steps are completed the user wants to continue the
c     problem (until completed) using the partial results saved on
c     fortran unit 2.  here are the entries of the array prgopt(*)
c     that accomplish these tasks.  (the definitions of the other
c     required input parameters are not shown.)
c
c     change to a maximization problem; key=50.
c     prgopt(01)=4
c     prgopt(02)=50
c     prgopt(03)=1
c
c     limit the number of simplex steps to 100; key=58.
c     prgopt(04)=8
c     prgopt(05)=58
c     prgopt(06)=1
c     prgopt(07)=100
c
c     save the partial results, after 100 steps, on fortran
c     unit 2; key=57.
c     prgopt(08)=11
c     prgopt(09)=57
c     prgopt(10)=1
c
c     no more options to change.
c     prgopt(11)=1
c     the user makes the call lpstatement for splp( ) at this point.
c     now to restart, using the partial results after 100 steps, define
c     new values for the array prgopt(*):
c
c     again inform splp( ) that this is a maximization problem.
c     prgopt(01)=4
c     prgopt(02)=50
c     prgopt(03)=1
c
c     restart, using saved partial results; key=55.
c     prgopt(04)=7
c     prgopt(05)=55
c     prgopt(06)=1
c
c     no more options to change.  the subprogram splp( ) is no longer
c     limited to 100 simplex steps but will run until completion or
c     max.=3*(mrelas+nvars) iterations.
c     prgopt(07)=1
c     the user now makes a call lpto subprogram splp( ) to compute the
c     solution.
c    |-------------------------------------------|
c    |end of usage of splp( ) subprogram options.|
c    |-------------------------------------------|
c
c     |----------------------------------------------|
c     |list of splp( ) error and diagnostic messages.|
c     |----------------------------------------------|
c      this section may be required to understand the meanings of the
c     error flag =-info  that may be returned from splp( ).
c
c -----1. there is no set of values for x and w that satisfy a*x=w and
c     the stated bounds.  the problem can be made feasible by ident-
c     ifying components of w that are now infeasible and then rede-
c     signating them as free variables.  subprogram splp( ) only
c     identifies an infeasible problem; it takes no other action to
c     change this condition.  message:
c     splp( ). the problem appears to be infeasible.
c     error number =         1
c
c     2. one of the variables in either the vector x or w was con-
c     strained at a bound.  otherwise the objective function value,
c     (transpose of costs)*x, would not have a finite optimum.
c     message:
c     splp( ). the problem appears to have no finite soln.
c     error number =         2
c
c     3.  both of the conditions of 1. and 2. above have occurred.
c     message:
c     splp( ). the problem appears to be infeasible and to
c     have no finite soln.
c     error number =         3
c
c -----4.  the real and integer working arrays, work(*) and iwork(*),
c     are not long enough. the values (i1) and (i2) in the message
c     below will give you the minimum length required.  also redefine
c     lw and liw, the lengths of these arrays.  message:
c     splp( ). work or iwork is not long enough. lw must be (i1)
c     and liw must be (i2).
c               in above message, i1=         0
c               in above message, i2=         0
c     error number =        4
c
c -----5. and 6.  these error messages often mean that one or more
c     arguments were left out of the call lpstatement to splp( ) or
c     that the values of mrelas and nvars have been over-written
c     by garbage.  messages:
c     splp( ). value of mrelas must be .gt.0. now=(i1).
c               in above message, i1=         0
c     error number =         5
c
c     splp( ). value of nvars must be .gt.0. now=(i1).
c               in above message, i1=         0
c     error number =         6
c
c -----7.,8., and 9.  these error messages can occur as the data matrix
c     is being defined by either usrmat( ) or the user-supplied sub-
c     program, 'name'( ). they would indicate a mistake in the contents
c     of dattrv(*), the user-written subprogram or that data has been
c     over-written.
c     messages:
c     splp( ). more than 2*nvars*mrelas iters. defining or updating
c     matrix data.
c     error number =        7
c
c     splp( ). row index (i1) or column index (i2) is out of range.
c               in above message, i1=         1
c               in above message, i2=        12
c     error number =        8
c
c     splp( ). indication flag (i1) for matrix data must be
c     either 0 or 1.
c               in above message, i1=        12
c     error number =        9
c
c -----10. and 11.  the type of bound (even no bound) and the bounds
c     must be specified for each independent variable. if an independent
c     variable has both an upper and lower bound, the bounds must be
c     consistent.  the lower bound must be .le. the upper bound.
c     messages:
c     splp( ). independent variable (i1) is not defined.
c               in above message, i1=         1
c     error number =        10
c
c     splp( ).  lower bound (r1) and upper bound (r2) for indep.
c     variable (i1) are not consistent.
c               in above message, i1=         1
c               in above message, r1=    0.
c               in above message, r2=    -.1000000000e+01
c     error number =        11
c
c -----12. and 13.  the type of bound (even no bound) and the bounds
c     must be specified for each dependent variable.  if a dependent
c     variable has both an upper and lower bound, the bounds must be
c     consistent. the lower bound must be .le. the upper bound.
c     messages:
c     splp( ). dependent variable (i1) is not defined.
c               in above message, i1=         1
c     error number =        12
c
c     splp( ).  lower bound (r1) and upper bound (r2) for dep.
c      variable (i1) are not consistent.
c               in above message, i1=         1
c               in above message, r1=    0.
c               in above message, r2=    -.1000000000e+01
c     error number =        13
c
c -----14. - 21.  these error messages can occur when processing the
c     option array, prgopt(*), supplied by the user.  they would
c     indicate a mistake in defining prgopt(*) or that data has been
c     over-written.  see heading usage of splp( )
c     subprogram options, for details on how to define prgopt(*).
c     messages:
c     splp( ). the user option array has undefined data.
c     error number =        14
c
c     splp( ). option array processing is cycling.
c     error number =        15
c
c     splp( ). an index of user-supplied basis is out of range.
c     error number =        16
c
c     splp( ). size parameters for matrix must be smallest and largest
c     magnitudes of nonzero entries.
c     error number =        17
c
c     splp( ). the number of revised simplex steps between check-points
c     must be positive.
c     error number =        18
c
c     splp( ). file numbers for saved data and matrix pages must be
c     positive and not equal.
c     error number =        19
c
c     splp( ). user-defined value of lamat (i1)
c     must be .ge. nvars+7.
c               in above message, i1=         1
c     error number =         20
c
c     splp( ). user-defined value of lbm must be .ge. 0.
c     error number =         21
c
c -----22.  the user-option, number 62, to check the size of the matrix
c     data has been used.  an element of the matrix does not lie within
c     the range of asmall and abig, parameters provided by the user.
c     (see the heading: usage of splp( ) subprogram options,
c     for details about this feature.)  message:
c     splp( ). a matrix element's size is out of the specified range.
c     error number =        22
c
c -----23.  the user has provided an initial basis that is singular.
c     in this case, the user can remedy this problem by letting
c     subprogram splp( ) choose its own initial basis.  message:
c     splp( ). a singular initial basis was encountered.
c     error number =         23
c
c -----24.  the user has provided an initial basis which is infeasible.
c     the x and w values it defines do not satisfy a*x=w and the stated
c     bounds.  in this case, the user can let subprogram splp( )
c     choose its own initial basis.  message:
c     splp( ). an infeasible initial basis was encountered.
c     error number =        24
c
c -----25. subprogram splp( ) has completed the maximum specified number
c     of iterations.  (the nominal maximum number is 3*(mrelas+nvars).)
c     the results, necessary to continue on from
c     this point, can be saved on fortran unit 2 by activating option
c     key=57.  if the user anticipates continuing the calculation, then
c     the contents of fortran unit 2 must be retained intact.  this
c     is not done by subprogram splp( ), so the user needs to save unit
c     2 by using the appropriate system commands.  message:
c     splp( ). max. iters. (i1) taken. up-to-date results
c     saved on file (i2). if(i2)=0, no save.
c               in above message, i1=       500
c               in above message, i2=         2
c     error number =        25
c
c -----26.  this error should never happen.  message:
c     splp( ). moved to a singular point. this should not happen.
c     error number =        26
c
c -----27.  the subprogram la05a( ), which decomposes the basis matrix,
c     has returned with an error flag (r1).  (see the document,
c     "fortran subprograms for handling sparse linear programming
c     bases", aere-r8269, j.k. reid, jan., 1976, h.m. stationery office,
c     for an explanation of this error.)  message:
c     splp( ). la05a( ) returned error flag (r1) below.
c               in above message, r1=    -.5000000000e+01
c     error number =        27
c
c -----28.  the sparse linear solver package, la05*( ), requires more
c     space.  the value of lbm must be increased.  see the companion
c     document, usage of splp( ) subprogram options, for details on how
c     to increase the value of lbm.  message:
c     splp( ). short on storage for la05*( ) package. use prgopt(*)
c     to give more.
c     error number =        28
c
c -----29.  the row dimension of the two-dimensional fortran array,
c     the number of constraint equations (mrelas), and the number
c     of variables (nvars), were not passed to the subprogram
c     fulmat().  see key = 68 for details.  message:
c     fulmat() of splp() package. row dim., mrelas, nvars are
c     missing from prgopt(*).
c     error number =        29
c
c     |------------------------------------------------------|
c     |end of list of splp( ) error and diagnostic messages. |
c     |------------------------------------------------------|
c***references  r. j. hanson and k. l. hiebert, a sparse linear
c                 programming subprogram, report sand81-0297, sandia
c                 national laboratories, 1981.
c***routines called  splpmn, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   890605  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c   920501  reformatted the references section.  (wrb)
c***end prologue  splp
      implicit   none

      real (kind=4) :: bl(*),bu(*),costs(*),dattrv(*),duals(*)
      real (kind=4) :: prgopt(*),primal(*),work(*),zero
c
      integer ibasis(*),ind(*),iwork(*)
      character*8 xern1, xern2
      integer (kind=4) :: iadbig, ictmax,ictopt,iopt, info
      integer (kind=4) :: key 
      integer (kind=4) :: lmx,lrg,lrhs,lrprim, lrz,lw,lwr,lww, lwork
      integer (kind=4) :: limat,lipr,liwork,liw,liwr
      integer (kind=4) :: lcsc,lerd,lerp,libb,librc
      integer (kind=4) :: lamat,last,lbasma,lbm,lcolnr
      integer (kind=4) :: mrelas
      integer (kind=4) :: next, nerr, nvars
c
      external lpusrmat
c
c***first executable statement  splp
      zero=0.e0
      iopt=1
c
c     verify that mrelas, nvars .gt. 0.
c
      if (mrelas.le.0) then
         write (xern1, '(i8)') mrelas
         call lpxermsg ('slatec', 'splp', 'value of mrelas must be ' //
     *      '.gt. 0.  now = ' // xern1, 5, 1)
         info = -5
         return
      endif
c
      if (nvars.le.0) then
         write (xern1, '(i8)') nvars
         call lpxermsg ('slatec', 'splp', 'value of nvars must be ' //
     *      '.gt. 0.  now = ' // xern1, 6, 1)
         info = -6
         return
      endif
c
      lmx=4*nvars+7
      lbm=8*mrelas
      last = 1
      iadbig=10000
      ictmax=1000
      ictopt= 0
c
c     look in option array for changes to work array lengths.
20008 next=prgopt(last)
      if (.not.(next.le.0 .or. next.gt.iadbig)) go to 20010
c
c     the checks for small or large values of next are to prevent
c     working with undefined data.
      nerr=14
      call lpxermsg ('slatec', 'splp',
     +   'the user option array has undefined data.', nerr, iopt)
      info=-nerr
      return
20010 if (.not.(next.eq.1)) go to 10001
      go to 20009
10001 if (.not.(ictopt.gt.ictmax)) go to 10002
      nerr=15
      call lpxermsg ('slatec', 'splp',
     +   'option array processing is cycling.', nerr, iopt)
      info=-nerr
      return
10002 continue
      key = prgopt(last+1)
c
c     if key = 53, user may specify lengths of portions
c    of work(*) and iwork(*) that are allocated to the
c     sparse matrix storage and sparse linear equation
c     solving.
      if (.not.(key.eq.53)) go to 20013
      if (.not.(prgopt(last+2).ne.zero)) go to 20016
      lmx=prgopt(last+3)
      lbm=prgopt(last+4)
20016 continue
20013 ictopt = ictopt+1
      last = next
      go to 20008
c
c     check length validity of sparse matrix staging area.
c
20009 if (lmx.lt.nvars+7) then
         write (xern1, '(i8)') lmx
         call lpxermsg ('slatec', 'splp', 'user-defined value of ' //
     *      'lamat = ' // xern1 // ' must be .ge. nvars+7.', 20, 1)
         info = -20
         return
      endif
c
c     trivial check on length of la05*() matrix area.
      if (.not.(lbm.lt.0)) go to 20022
      nerr=21
      call lpxermsg ('slatec', 'splp',
     +   'user-defined value of lbm must be .ge. 0.', nerr, iopt)
      info=-nerr
      return
20022 continue
c
c     define pointers for starts of subarrays used in work(*)
c     and iwork(*) in other subprograms of the package.
      lamat=1
      lcsc=lamat+lmx
      lcolnr=lcsc+nvars
      lerd=lcolnr+nvars
      lerp=lerd+mrelas
      lbasma=lerp+mrelas
      lwr=lbasma+lbm
      lrz=lwr+mrelas
      lrg=lrz+nvars+mrelas
      lrprim=lrg+nvars+mrelas
      lrhs=lrprim+mrelas
      lww=lrhs+mrelas
      lwork=lww+mrelas-1
      limat=1
      libb=limat+lmx
      librc=libb+nvars+mrelas
      lipr=librc+2*lbm
      liwr=lipr+2*mrelas
      liwork=liwr+8*mrelas-1
c
c     check array length validity of work(*), iwork(*).
c
      if (lw.lt.lwork .or. liw.lt.liwork) then
         write (xern1, '(i8)') lwork
         write (xern2, '(i8)') liwork
        call lpxermsg ('slatec', 'splp', 'work or iwork is not long ' //
     *      'enough. lw must be = ' // xern1 // ' and liw must be = ' //
     *      xern2, 4, 1)
         info = -4
         return
      endif
c
      call lpsplpmn(lpusrmat,mrelas,nvars,costs,prgopt,dattrv,
     * bl,bu,ind,info,primal,duals,work(lamat),
     * work(lcsc),work(lcolnr),work(lerd),work(lerp),work(lbasma),
     * work(lwr),work(lrz),work(lrg),work(lrprim),work(lrhs),
     * work(lww),lmx,lbm,ibasis,iwork(libb),iwork(limat),
     * iwork(librc),iwork(lipr),iwork(liwr))
c
c     call lpsplpmn(usrmat,mrelas,nvars,costs,prgopt,dattrv,
c    1 bl,bu,ind,info,primal,duals,amat,
c    2 csc,colnrm,erd,erp,basmat,
c    3 wr,rz,rg,rprim,rhs,
c    4 ww,lmx,lbm,ibasis,ibb,imat,
c    5 ibrc,ipr,iwr)
c

      end subroutine lpsplp

      subroutine lpfdump
c***begin prologue  fdump
c***purpose  symbolic dump (should be locally written).
c***library   slatec (xerror)
c***category  r3
c***type      all (fdump-a)
c***keywords  error, xermsg
c***author  jones, r. e., (snla)
c***description
      implicit   none
c
c        ***note*** machine dependent routine
c        fdump is intended to be replaced by a locally written
c        version which produces a symbolic dump.  failing this,
c        it should be replaced by a version which prints the
c        subprogram nesting list.  note that this dump must be
c        printed on each of up to five files, as indicated by the
c        xgetua routine.  see xsetua and xgetua for details.
c
c     written by ron jones, with slatec common math library subcommittee
c
c***references  (none)
c***routines called  (none)
c***revision history  (yymmdd)
c   790801  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c***end prologue  fdump
c***first executable statement  fdump

      end subroutine lpfdump

      integer function lpi1mach (i)

      implicit    none

      integer (kind=4) :: i
c***begin prologue  lpi1mach
c***purpose  return integer machine dependent constants.
c***library   slatec
c***category  r1
c***type      integer (lpi1mach-i)
c***keywords  machine constants
c***author  fox, p. a., (bell labs)
c           hall, a. d., (bell labs)
c           schryer, n. l., (bell labs)
c***description
c
c   lpi1mach can be used to obtain machine-dependent parameters for the
c   local machine environment.  it is a function subprogram with one
c   (input) argument and can be referenced as follows:
c
c        k = lpi1mach(i)
c
c   where i=1,...,16.  the (output) value of k above is determined by
c   the (input) value of i.  the results for various values of i are
c   discussed below.
c
c   i/o unit numbers:
c     lpi1mach( 1) = the standard input unit.
c     lpi1mach( 2) = the standard output unit.
c     lpi1mach( 3) = the standard punch unit.
c     lpi1mach( 4) = the standard error message unit.
c
c   words:
c     lpi1mach( 5) = the number of bits per integer storage unit.
c     lpi1mach( 6) = the number of characters per integer storage unit.
c
c   integers:
c     assume integers are represented in the s-digit, base-a form
c
c                sign ( x(s-1)*a**(s-1) + ... + x(1)*a + x(0) )
c
c                where 0 .le. x(i) .lt. a for i=0,...,s-1.
c     lpi1mach( 7) = a, the base.
c     lpi1mach( 8) = s, the number of base-a digits.
c     lpi1mach( 9) = a**s - 1, the largest magnitude.
c
c   floating-point numbers:
c     assume floating-point numbers are represented in the t-digit,
c     base-b form
c                sign (b**e)*( (x(1)/b) + ... + (x(t)/b**t) )
c
c                where 0 .le. x(i) .lt. b for i=1,...,t,
c                0 .lt. x(1), and emin .le. e .le. emax.
c     lpi1mach(10) = b, the base.
c
c   single-precision:
c     lpi1mach(11) = t, the number of base-b digits.
c     lpi1mach(12) = emin, the smallest exponent e.
c     lpi1mach(13) = emax, the largest exponent e.
c
c   double-precision:
c     lpi1mach(14) = t, the number of base-b digits.
c     lpi1mach(15) = emin, the smallest exponent e.
c     lpi1mach(16) = emax, the largest exponent e.
c
c   to alter this function for a particular environment, the desired
c   set of data statements should be activated by removing the c from
c   column 1.  also, the values of lpi1mach(1) - lpi1mach(4) should be
c   checked for consistency with the local operating system.
c
c***references  p. a. fox, a. d. hall and n. l. schryer, framework for
c                 a portable library, acm transactions on mathematical
c                 software 4, 2 (june 1978), pp. 177-188.
c***routines called  (none)
c***revision history  (yymmdd)
c   750101  date written
c   891012  added vax g-floating constants.  (wrb)
c   891012  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900618  added dec risc constants.  (wrb)
c   900723  added ibm rs 6000 constants.  (wrb)
c   901009  correct lpi1mach(7) for ibm mainframes. should be 2 not 16.
c           (rwc)
c   910710  added hp 730 constants.  (smr)
c   911114  added convex ieee constants.  (wrb)
c   920121  added sun -r8 compiler option constants.  (wrb)
c   920229  added touchstone delta i860 constants.  (wrb)
c   920501  reformatted the references section.  (wrb)
c   920625  added convex -p8 and -pd8 compiler option constants.
c           (bks, wrb)
c   930201  added dec alpha and sgi constants.  (rwc and wrb)
c   930618  corrected lpi1mach(5) for convex -p8 and -pd8 compiler
c           options.  (dwl, rwc and wrb).
c***end prologue  lpi1mach
c
      integer imach(16),output
      save imach
      equivalence (imach(4),output)
c
c     machine constants for the amiga
c     absoft compiler
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -126 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1022 /
c     data imach(16) /       1023 /
c
c     machine constants for the apollo
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        129 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1025 /
c
c     machine constants for the burroughs 1700 system
c
c     data imach( 1) /          7 /
c     data imach( 2) /          2 /
c     data imach( 3) /          2 /
c     data imach( 4) /          2 /
c     data imach( 5) /         36 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         33 /
c     data imach( 9) / z1ffffffff /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -256 /
c     data imach(13) /        255 /
c     data imach(14) /         60 /
c     data imach(15) /       -256 /
c     data imach(16) /        255 /
c
c     machine constants for the burroughs 5700 system
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         48 /
c     data imach( 6) /          6 /
c     data imach( 7) /          2 /
c     data imach( 8) /         39 /
c     data imach( 9) / o0007777777777777 /
c     data imach(10) /          8 /
c     data imach(11) /         13 /
c     data imach(12) /        -50 /
c     data imach(13) /         76 /
c     data imach(14) /         26 /
c     data imach(15) /        -50 /
c     data imach(16) /         76 /
c
c     machine constants for the burroughs 6700/7700 systems
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         48 /
c     data imach( 6) /          6 /
c     data imach( 7) /          2 /
c     data imach( 8) /         39 /
c     data imach( 9) / o0007777777777777 /
c     data imach(10) /          8 /
c     data imach(11) /         13 /
c     data imach(12) /        -50 /
c     data imach(13) /         76 /
c     data imach(14) /         26 /
c     data imach(15) /     -32754 /
c     data imach(16) /      32780 /
c
c     machine constants for the cdc 170/180 series using nos/ve
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         64 /
c     data imach( 6) /          8 /
c     data imach( 7) /          2 /
c     data imach( 8) /         63 /
c     data imach( 9) / 9223372036854775807 /
c     data imach(10) /          2 /
c     data imach(11) /         47 /
c     data imach(12) /      -4095 /
c     data imach(13) /       4094 /
c     data imach(14) /         94 /
c     data imach(15) /      -4095 /
c     data imach(16) /       4094 /
c
c     machine constants for the cdc 6000/7000 series
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /    6loutput/
c     data imach( 5) /         60 /
c     data imach( 6) /         10 /
c     data imach( 7) /          2 /
c     data imach( 8) /         48 /
c     data imach( 9) / 00007777777777777777b /
c     data imach(10) /          2 /
c     data imach(11) /         47 /
c     data imach(12) /       -929 /
c     data imach(13) /       1070 /
c     data imach(14) /         94 /
c     data imach(15) /       -929 /
c     data imach(16) /       1069 /
c
c     machine constants for the celerity c1260
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          0 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / z'7fffffff' /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -126 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1022 /
c     data imach(16) /       1023 /
c
c     machine constants for the convex
c     using the -fn compiler option
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1023 /
c     data imach(16) /       1023 /
c
c     machine constants for the convex
c     using the -fi compiler option
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the convex
c     using the -p8 compiler option
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         64 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         63 /
c     data imach( 9) / 9223372036854775807 /
c     data imach(10) /          2 /
c     data imach(11) /         53 /
c     data imach(12) /      -1023 /
c     data imach(13) /       1023 /
c     data imach(14) /        113 /
c     data imach(15) /     -16383 /
c     data imach(16) /      16383 /
c
c     machine constants for the convex
c     using the -pd8 compiler option
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         64 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         63 /
c     data imach( 9) / 9223372036854775807 /
c     data imach(10) /          2 /
c     data imach(11) /         53 /
c     data imach(12) /      -1023 /
c     data imach(13) /       1023 /
c     data imach(14) /         53 /
c     data imach(15) /      -1023 /
c     data imach(16) /       1023 /
c
c     machine constants for the cray
c     using the 46 bit integer compiler option
c
c     data imach( 1) /        100 /
c     data imach( 2) /        101 /
c     data imach( 3) /        102 /
c     data imach( 4) /        101 /
c     data imach( 5) /         64 /
c     data imach( 6) /          8 /
c     data imach( 7) /          2 /
c     data imach( 8) /         46 /
c     data imach( 9) / 1777777777777777b /
c     data imach(10) /          2 /
c     data imach(11) /         47 /
c     data imach(12) /      -8189 /
c     data imach(13) /       8190 /
c     data imach(14) /         94 /
c     data imach(15) /      -8099 /
c     data imach(16) /       8190 /
c
c     machine constants for the cray
c     using the 64 bit integer compiler option
c
c     data imach( 1) /        100 /
c     data imach( 2) /        101 /
c     data imach( 3) /        102 /
c     data imach( 4) /        101 /
c     data imach( 5) /         64 /
c     data imach( 6) /          8 /
c     data imach( 7) /          2 /
c     data imach( 8) /         63 /
c     data imach( 9) / 777777777777777777777b /
c     data imach(10) /          2 /
c     data imach(11) /         47 /
c     data imach(12) /      -8189 /
c     data imach(13) /       8190 /
c     data imach(14) /         94 /
c     data imach(15) /      -8099 /
c     data imach(16) /       8190 /
c
c     machine constants for the data general eclipse s/200
c
c     data imach( 1) /         11 /
c     data imach( 2) /         12 /
c     data imach( 3) /          8 /
c     data imach( 4) /         10 /
c     data imach( 5) /         16 /
c     data imach( 6) /          2 /
c     data imach( 7) /          2 /
c     data imach( 8) /         15 /
c     data imach( 9) /      32767 /
c     data imach(10) /         16 /
c     data imach(11) /          6 /
c     data imach(12) /        -64 /
c     data imach(13) /         63 /
c     data imach(14) /         14 /
c     data imach(15) /        -64 /
c     data imach(16) /         63 /
c
c     machine constants for the dec alpha
c     using g_float
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1023 /
c     data imach(16) /       1023 /
c
c     machine constants for the dec alpha
c     using ieee_float
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the dec risc
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the dec vax
c     using d_floating
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         56 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c     machine constants for the dec vax
c     using g_floating
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1023 /
c     data imach(16) /       1023 /
c
c     machine constants for the elxsi 6400
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         32 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -126 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1022 /
c     data imach(16) /       1023 /
c
c     machine constants for the harris 220
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          0 /
c     data imach( 4) /          6 /
c     data imach( 5) /         24 /
c     data imach( 6) /          3 /
c     data imach( 7) /          2 /
c     data imach( 8) /         23 /
c     data imach( 9) /    8388607 /
c     data imach(10) /          2 /
c     data imach(11) /         23 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         38 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c     machine constants for the honeywell 600/6000 series
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /         43 /
c     data imach( 4) /          6 /
c     data imach( 5) /         36 /
c     data imach( 6) /          6 /
c     data imach( 7) /          2 /
c     data imach( 8) /         35 /
c     data imach( 9) / o377777777777 /
c     data imach(10) /          2 /
c     data imach(11) /         27 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         63 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c     machine constants for the hp 730
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the hp 2100
c     3 word double precision option with ftn4
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          4 /
c     data imach( 4) /          1 /
c     data imach( 5) /         16 /
c     data imach( 6) /          2 /
c     data imach( 7) /          2 /
c     data imach( 8) /         15 /
c     data imach( 9) /      32767 /
c     data imach(10) /          2 /
c     data imach(11) /         23 /
c     data imach(12) /       -128 /
c     data imach(13) /        127 /
c     data imach(14) /         39 /
c     data imach(15) /       -128 /
c     data imach(16) /        127 /
c
c     machine constants for the hp 2100
c     4 word double precision option with ftn4
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          4 /
c     data imach( 4) /          1 /
c     data imach( 5) /         16 /
c     data imach( 6) /          2 /
c     data imach( 7) /          2 /
c     data imach( 8) /         15 /
c     data imach( 9) /      32767 /
c     data imach(10) /          2 /
c     data imach(11) /         23 /
c     data imach(12) /       -128 /
c     data imach(13) /        127 /
c     data imach(14) /         55 /
c     data imach(15) /       -128 /
c     data imach(16) /        127 /
c
c     machine constants for the hp 9000
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          7 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         32 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -126 /
c     data imach(13) /        127 /
c     data imach(14) /         53 /
c     data imach(15) /      -1015 /
c     data imach(16) /       1017 /
c
c     machine constants for the ibm 360/370 series,
c     the xerox sigma 5/7/9, the sel systems 85/86, and
c     the perkin elmer (interdata) 7/32.
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          7 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) /  z7fffffff /
c     data imach(10) /         16 /
c     data imach(11) /          6 /
c     data imach(12) /        -64 /
c     data imach(13) /         63 /
c     data imach(14) /         14 /
c     data imach(15) /        -64 /
c     data imach(16) /         63 /
c
c     machine constants for the ibm pc
c
      data imach( 1) /          5 /
      data imach( 2) /          6 /
      data imach( 3) /          0 /
      data imach( 4) /          0 /
      data imach( 5) /         32 /
      data imach( 6) /          4 /
      data imach( 7) /          2 /
      data imach( 8) /         31 /
      data imach( 9) / 2147483647 /
      data imach(10) /          2 /
      data imach(11) /         24 /
      data imach(12) /       -125 /
      data imach(13) /        127 /
      data imach(14) /         53 /
      data imach(15) /      -1021 /
      data imach(16) /       1023 /
c
c     machine constants for the ibm rs 6000
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          0 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the intel i860
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the pdp-10 (ka processor)
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         36 /
c     data imach( 6) /          5 /
c     data imach( 7) /          2 /
c     data imach( 8) /         35 /
c     data imach( 9) / "377777777777 /
c     data imach(10) /          2 /
c     data imach(11) /         27 /
c     data imach(12) /       -128 /
c     data imach(13) /        127 /
c     data imach(14) /         54 /
c     data imach(15) /       -101 /
c     data imach(16) /        127 /
c
c     machine constants for the pdp-10 (ki processor)
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         36 /
c     data imach( 6) /          5 /
c     data imach( 7) /          2 /
c     data imach( 8) /         35 /
c     data imach( 9) / "377777777777 /
c     data imach(10) /          2 /
c     data imach(11) /         27 /
c     data imach(12) /       -128 /
c     data imach(13) /        127 /
c     data imach(14) /         62 /
c     data imach(15) /       -128 /
c     data imach(16) /        127 /
c
c     machine constants for pdp-11 fortran supporting
c     32-bit integer arithmetic.
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         56 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c     machine constants for pdp-11 fortran supporting
c     16-bit integer arithmetic.
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          5 /
c     data imach( 4) /          6 /
c     data imach( 5) /         16 /
c     data imach( 6) /          2 /
c     data imach( 7) /          2 /
c     data imach( 8) /         15 /
c     data imach( 9) /      32767 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         56 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c     machine constants for the silicon graphics
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the sun
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -125 /
c     data imach(13) /        128 /
c     data imach(14) /         53 /
c     data imach(15) /      -1021 /
c     data imach(16) /       1024 /
c
c     machine constants for the sun
c     using the -r8 compiler option
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          6 /
c     data imach( 4) /          6 /
c     data imach( 5) /         32 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         31 /
c     data imach( 9) / 2147483647 /
c     data imach(10) /          2 /
c     data imach(11) /         53 /
c     data imach(12) /      -1021 /
c     data imach(13) /       1024 /
c     data imach(14) /        113 /
c     data imach(15) /     -16381 /
c     data imach(16) /      16384 /
c
c     machine constants for the univac 1100 series ftn compiler
c
c     data imach( 1) /          5 /
c     data imach( 2) /          6 /
c     data imach( 3) /          1 /
c     data imach( 4) /          6 /
c     data imach( 5) /         36 /
c     data imach( 6) /          4 /
c     data imach( 7) /          2 /
c     data imach( 8) /         35 /
c     data imach( 9) / o377777777777 /
c     data imach(10) /          2 /
c     data imach(11) /         27 /
c     data imach(12) /       -128 /
c     data imach(13) /        127 /
c     data imach(14) /         60 /
c     data imach(15) /      -1024 /
c     data imach(16) /       1023 /
c
c     machine constants for the z80 microprocessor
c
c     data imach( 1) /          1 /
c     data imach( 2) /          1 /
c     data imach( 3) /          0 /
c     data imach( 4) /          1 /
c     data imach( 5) /         16 /
c     data imach( 6) /          2 /
c     data imach( 7) /          2 /
c     data imach( 8) /         15 /
c     data imach( 9) /      32767 /
c     data imach(10) /          2 /
c     data imach(11) /         24 /
c     data imach(12) /       -127 /
c     data imach(13) /        127 /
c     data imach(14) /         56 /
c     data imach(15) /       -127 /
c     data imach(16) /        127 /
c
c***first executable statement  lpi1mach
      if (i .lt. 1  .or.  i .gt. 16) go to 10
c
      lpi1mach = imach(i)
      return
c
   10 continue
      write (unit = output, fmt = 9000)
 9000 format (' error    1 in lpi1mach - i out of bounds')
c
c     call lpfdump
c
      stop
      end function lpi1mach
      integer function lpiploc (loc, sx, ix)
c***begin prologue  lpiploc
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (lpiploc-s, idloc-d)
c***keywords  relative address determination function, slatec
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c   given a "virtual" location,  lpiploc returns the relative working
c   address of the vector component stored in sx, ix.  any necessary
c   page swaps are performed automatically for the user in this
c   function subprogram.
c
c   loc       is the "virtual" address of the data to be retrieved.
c   sx ,ix    represent the matrix where the data is stored.
c
c***see also  splp
c***routines called  prwpge, xermsg
c***revision history  (yymmdd)
c   810306  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890606  restructured to match double precision version.  (wrb)
c   890606  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   910731  added code to set lpiploc to 0 if loc is non-positive.  (wrb)
c***end prologue  lpiploc
      real sx(*)
      integer ix(*)
c
      integer loc,k,lmx,lmxm1,lpg,itemp,ipage,np,key
c***first executable statement  lpiploc
      if (loc.le.0) then
         call lpxermsg ('slatec', 'lpiploc',
     +     'a value of loc, the first argument, .le. 0 was encountered',
     +     55, 1)
         lpiploc = 0
         return
      endif
c
c     two cases exist:  (1.le.loc.le.k) .or. (loc.gt.k).
c
      k = ix(3) + 4
      lmx = ix(1)
      lmxm1 = lmx - 1
      if (loc.le.k) then
         lpiploc = loc
         return
      endif
c
c     compute length of the page, starting address of the page, page
c     number and relative working address.
c
      lpg = lmx-k
      itemp = loc - k - 1
      ipage = itemp/lpg + 1
      lpiploc = mod(itemp,lpg) + k + 1
      np = abs(ix(lmxm1))
c
c     determine if a page fault has occurred.  if so, write page np
c     and read page ipage.  write the page only if it has been
c     modified.
c
      if (ipage.ne.np) then
         if (sx(lmx).eq.1.0) then
            sx(lmx) = 0.0
            key = 2
            call lpprwpge (key, np, lpg, sx, ix)
         endif
         key = 1
         call lpprwpge (key, ipage, lpg, sx, ix)
      endif

      end

      subroutine lpivout (n, ix, ifmt, idigit)
c***begin prologue  ivout
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      integer (ivout-i)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     integer vector output routine.
c
c  input..
c
c  n,ix(*) print the integer array ix(i),i=1,...,n, on output
c          unit lout. the heading in the fortran format
c          statement ifmt(*), described below, is printed as a first
c          step. the components ix(i) are indexed, on output,
c          in a pleasant format.
c  ifmt(*) a fortran format statement. this is printed on output
c          unit lout with the variable format fortran statement
c                write(lout,ifmt)
c  idigit  print up to abs(idigit) decimal digits per number.
c          the subprogram will choose that integer 4,6,10 or 14
c          which will print at least abs(idigit) number of
c          places.  if idigit.lt.0, 72 printing columns are utilized
c          to write each line of output of the array ix(*). (this
c          can be used on most time-sharing terminals). if
c          idigit.ge.0, 133 printing columns are utilized. (this can
c          be used on most line printers).
c
c  example..
c
c  print an array called (costs of purchases) of length 100 showing
c  6 decimal digits per number. the user is running on a time-sharing
c  system with a 72 column output device.
c
c     dimension icosts(100)
c     n = 100
c     idigit = -6
c     call lpivout(n,icosts,'(''1costs of purchases'')',idigit)
c
c***see also  splp
c***routines called  lpi1mach
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900402  added type section.  (wrb)
c   910403  updated author section.  (wrb)
c***end prologue  ivout
      dimension ix(*)
      character ifmt*(*)
c
      integer j,lout,lpi1mach,n,ndigit,idigit,k1,k2,ix,i
c
c     get the unit number where output will be written.
c***first executable statement  ivout
      j=2
      lout=lpi1mach(j)
      write(lout,ifmt)
      if(n.le.0) return
      ndigit = idigit
      if(idigit.eq.0) ndigit = 4
      if(idigit.ge.0) go to 80
c
      ndigit = -idigit
      if(ndigit.gt.4) go to 20
c
      do 10 k1=1,n,10
      k2 = min(n,k1+9)
      write(lout,1000) k1,k2,(ix(i),i=k1,k2)
   10 continue
      return
c
   20 continue
      if(ndigit.gt.6) go to 40
c
      do 30 k1=1,n,7
      k2 = min(n,k1+6)
      write(lout,1001) k1,k2,(ix(i),i=k1,k2)
   30 continue
      return
c
   40 continue
      if(ndigit.gt.10) go to 60
c
      do 50 k1=1,n,5
      k2=min(n,k1+4)
      write(lout,1002) k1,k2,(ix(i),i=k1,k2)
   50 continue
      return
c
   60 continue
      do 70 k1=1,n,3
      k2 = min(n,k1+2)
      write(lout,1003) k1,k2,(ix(i),i=k1,k2)
   70 continue
      return
c
   80 continue
      if(ndigit.gt.4) go to 100
c
      do 90 k1=1,n,20
      k2 = min(n,k1+19)
      write(lout,1000) k1,k2,(ix(i),i=k1,k2)
   90 continue
      return
c
  100 continue
      if(ndigit.gt.6) go to 120
c
      do 110 k1=1,n,15
      k2 = min(n,k1+14)
      write(lout,1001) k1,k2,(ix(i),i=k1,k2)
  110 continue
      return
c
  120 continue
      if(ndigit.gt.10) go to 140
c
      do 130 k1=1,n,10
      k2 = min(n,k1+9)
      write(lout,1002) k1,k2,(ix(i),i=k1,k2)
  130 continue
      return
c
  140 continue
      do 150 k1=1,n,7
      k2 = min(n,k1+6)
      write(lout,1003) k1,k2,(ix(i),i=k1,k2)
  150 continue
      return
 1000 format(1x,i4,' - ',i4,20(1x,i5))
 1001 format(1x,i4,' - ',i4,15(1x,i7))
 1002 format(1x,i4,' - ',i4,10(1x,i11))
 1003 format(1x,i4,' - ',i4,7(1x,i15))
      end
      integer function lpj4save (iwhich, ivalue, iset)
      integer iwhich,ivalue
c***begin prologue  lpj4save
c***subsidiary
c***purpose  save or recall lpglobal variables needed by error
c            handling routines.
c***library   slatec (xerror)
c***type      integer (lpj4save-i)
c***keywords  error messages, error number, recall, save, xerror
c***author  jones, r. e., (snla)
c***description
c
c     abstract
c        lpj4save saves and recalls several global variables needed
c        by the library error handling routines.
c
c     description of parameters
c      --input--
c        iwhich - index of item desired.
c                = 1 refers to current error number.
c                = 2 refers to current error control flag.
c                = 3 refers to current unit number to which error
c                    messages are to be sent.  (0 means use standard.)
c                = 4 refers to the maximum number of times any
c                     message is to be printed (as set by xermax).
c                = 5 refers to the total number of units to which
c                     each error message is to be written.
c                = 6 refers to the 2nd unit for error messages
c                = 7 refers to the 3rd unit for error messages
c                = 8 refers to the 4th unit for error messages
c                = 9 refers to the 5th unit for error messages
c        ivalue - the value to be set for the iwhich-th parameter,
c                 if iset is .true. .
c        iset   - if iset=.true., the iwhich-th parameter will be
c                 given the value, ivalue.  if iset=.false., the
c                 iwhich-th parameter will be unchanged, and ivalue
c                 is a dummy parameter.
c      --output--
c        the (old) value of the iwhich-th parameter will be returned
c        in the function value, lpj4save.
c
c***see also  xermsg
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  (none)
c***revision history  (yymmdd)
c   790801  date written
c   891214  prologue converted to version 4.0 format.  (bab)
c   900205  minor modifications to prologue.  (wrb)
c   900402  added type section.  (wrb)
c   910411  added keywords section.  (wrb)
c   920501  reformatted the references section.  (wrb)
c***end prologue  lpj4save
      logical iset
      integer iparam(9)
      save iparam
      data iparam(1),iparam(2),iparam(3),iparam(4)/0,2,0,10/
      data iparam(5)/1/
      data iparam(6),iparam(7),iparam(8),iparam(9)/0,0,0,0/
c***first executable statement  lpj4save
      lpj4save = iparam(iwhich)
      if (iset) iparam(iwhich) = ivalue

      end

      subroutine lpla05as (a, ind, nz, ia, n, ip, iw, w, g, u)
c***begin prologue  la05as
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (la05as-s, la05ad-d)
c***author  (unknown)
c***description
c
c     this subprogram is a slight modification of a subprogram
c     from the c. 1979 aere harwell library.  the name of the
c     corresponding harwell code can be obtained by deleting
c     the final letter =s= in the names used here.
c     revisions made by r j hanson, snla, august, 1979.
c     revised sep. 13, 1979.
c
c     royalties have been paid to aere-uk for use of their codes
c     in the package given here.  any primary usage of the harwell
c     subroutines requires a royalty agreement and payment between
c     the user and aere-uk.  any usage of the sandia written codes
c     splp( ) (which uses the harwell subroutines) is permitted.
c
c ip(i,1),ip(i,2) point to the start of row/col i.
c iw(i,1),iw(i,2) hold the number of non-zeros in row/col i.
c during the main body of this subroutine lpthe vectors iw(.,3),iw(.,5),
c     iw(.,7) are used to hold doubly linked lists of rows that have
c     not been pivotal and have equal numbers of non-zeros.
c iw(.,4),iw(.,6),iw(.,8) hold similar lists for the columns.
c iw(i,3),iw(i,4) hold first row/column to have i non-zeros
c     or zero if there are none.
c iw(i,5), iw(i,6) hold row/col number of row/col prior to row/col i
c     in its list, or zero if none.
c iw(i,7), iw(i,8) hold row/col number of row/col after row/col i
c     in its list, or zero if none.
c for rows/cols that have been pivotal iw(i,5),iw(i,6) hold negation of
c     position of row/col i in the pivotal ordering.
c
c***see also  splp
c***routines called  la05es, mc20as, lpr1mach, xermsg, xsetun
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  corrected references to xerrwv.  (wrb)
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900402  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  la05as
      integer ip(n,2)
      integer ind(ia,2), iw(n,8)
      real a(*), amax, au, am, g, u, small, w(*),lpr1mach
      logical first
      character*8 xern0, xern1, xern2
c
      common /la05ds/ small, lp, lenl, lenu, ncp, lrow, lcol
c eps is the relative accuracy of floating-point computation
      save eps, first
      data first /.true./
c***first executable statement  la05as
      if (first) then
         eps = 2.0e0 * lpr1mach(4)
      endif
      first = .false.
c
c     set the output unit number for the error processor.
c     the usage of this error processor is documented in the
c     sandia labs. tech. rept. sand78-1189, by r e jones.
      call lpxsetun(lp)
      if (u.gt.1.0e0) u = 1.0e0
      if (u.lt.eps) u = eps
      if (n.lt.1) go to 670
      g = 0.
      do 50 i=1,n
         w(i) = 0.
         do 40 j=1,5
            iw(i,j) = 0
   40    continue
   50 continue
c
c flush out small entries, count elements in rows and columns
      l = 1
      lenu = nz
      do 80 idummy=1,nz
         if (l.gt.lenu) go to 90
         do 60 k=l,lenu
            if (abs(a(k)).le.small) go to 70
            i = ind(k,1)
            j = ind(k,2)
            g = max(abs(a(k)),g)
            if (i.lt.1 .or. i.gt.n) go to 680
            if (j.lt.1 .or. j.gt.n) go to 680
            iw(i,1) = iw(i,1) + 1
            iw(j,2) = iw(j,2) + 1
   60    continue
         go to 90
   70    l = k
         a(l) = a(lenu)
         ind(l,1) = ind(lenu,1)
         ind(l,2) = ind(lenu,2)
         lenu = lenu - 1
   80 continue
c
   90 lenl = 0
      lrow = lenu
      lcol = lrow
c mcp is the maximum number of compresses permitted before an
c     error return results.
      mcp = max(n/10,20)
      ncp = 0
c check for null row or column and initialize ip(i,2) to point
c     just beyond where the last component of column i of a will
c     be stored.
      k = 1
      do 110 ir=1,n
         k = k + iw(ir,2)
         ip(ir,2) = k
         do 100 l=1,2
            if (iw(ir,l).le.0) go to 700
  100    continue
  110 continue
c reorder by rows
c check for double entries while using the newly constructed
c     row file to construct the column file. note that by putting
c    the entries in backwards and decreasing ip(j,2) each time it
c     is used we automatically leave it pointing to the first element.
      call lpmc20as(n, lenu, a, ind(1,2), ip, ind(1,1), 0)
      kl = lenu
      do 130 ii=1,n
         ir = n + 1 - ii
         kp = ip(ir,1)
         do 120 k=kp,kl
            j = ind(k,2)
            if (iw(j,5).eq.ir) go to 660
            iw(j,5) = ir
            kr = ip(j,2) - 1
            ip(j,2) = kr
            ind(kr,1) = ir
  120    continue
         kl = kp - 1
  130 continue
c
c set up linked lists of rows and cols with equal numbers of non-zeros.
      do 150 l=1,2
         do 140 i=1,n
            nz = iw(i,l)
            in = iw(nz,l+2)
            iw(nz,l+2) = i
            iw(i,l+6) = in
            iw(i,l+4) = 0
            if (in.ne.0) iw(in,l+4) = i
  140    continue
  150 continue
c
c
c start of main elimination loop.
      do 590 ipv=1,n
c find pivot. jcost is markowitz cost of cheapest pivot found so far,
c     which is in row ipp and column jp.
         jcost = n*n
c loop on length of column to be searched
         do 240 nz=1,n
            if (jcost.le.(nz-1)**2) go to 250
            j = iw(nz,4)
c search columns with nz non-zeros.
            do 190 idummy=1,n
               if (j.le.0) go to 200
               kp = ip(j,2)
               kl = kp + iw(j,2) - 1
               do 180 k=kp,kl
                  i = ind(k,1)
                  kcost = (nz-1)*(iw(i,1)-1)
                  if (kcost.ge.jcost) go to 180
                  if (nz.eq.1) go to 170
c find largest element in row of potential pivot.
                  amax = 0.
                  k1 = ip(i,1)
                  k2 = iw(i,1) + k1 - 1
                  do 160 kk=k1,k2
                     amax = max(amax,abs(a(kk)))
                     if (ind(kk,2).eq.j) kj = kk
  160             continue
c perform stability test.
                  if (abs(a(kj)).lt.amax*u) go to 180
  170             jcost = kcost
                  ipp = i
                  jp = j
                  if (jcost.le.(nz-1)**2) go to 250
  180          continue
               j = iw(j,8)
  190       continue
c search rows with nz non-zeros.
  200       i = iw(nz,3)
            do 230 idummy=1,n
               if (i.le.0) go to 240
               amax = 0.
               kp = ip(i,1)
               kl = kp + iw(i,1) - 1
c find largest element in the row
               do 210 k=kp,kl
                  amax = max(abs(a(k)),amax)
  210          continue
               au = amax*u
               do 220 k=kp,kl
c perform stability test.
                  if (abs(a(k)).lt.au) go to 220
                  j = ind(k,2)
                  kcost = (nz-1)*(iw(j,2)-1)
                  if (kcost.ge.jcost) go to 220
                  jcost = kcost
                  ipp = i
                  jp = j
                  if (jcost.le.(nz-1)**2) go to 250
  220          continue
               i = iw(i,7)
  230       continue
  240    continue
c
c pivot found.
c remove rows and columns involved in elimination from ordering vectors.
  250    kp = ip(jp,2)
         kl = iw(jp,2) + kp - 1
         do 290 l=1,2
            do 280 k=kp,kl
               i = ind(k,l)
               il = iw(i,l+4)
               in = iw(i,l+6)
               if (il.eq.0) go to 260
               iw(il,l+6) = in
               go to 270
  260          nz = iw(i,l)
               iw(nz,l+2) = in
  270          if (in.gt.0) iw(in,l+4) = il
  280       continue
            kp = ip(ipp,1)
            kl = kp + iw(ipp,1) - 1
  290    continue
c store pivot
         iw(ipp,5) = -ipv
         iw(jp,6) = -ipv
c eliminate pivotal row from column file and find pivot in row file.
         do 320 k=kp,kl
            j = ind(k,2)
            kpc = ip(j,2)
            iw(j,2) = iw(j,2) - 1
            klc = kpc + iw(j,2)
            do 300 kc=kpc,klc
               if (ipp.eq.ind(kc,1)) go to 310
  300       continue
  310       ind(kc,1) = ind(klc,1)
            ind(klc,1) = 0
            if (j.eq.jp) kr = k
  320    continue
c bring pivot to front of pivotal row.
         au = a(kr)
         a(kr) = a(kp)
         a(kp) = au
         ind(kr,2) = ind(kp,2)
         ind(kp,2) = jp
c
c perform elimination itself, looping on non-zeros in pivot column.
         nzc = iw(jp,2)
         if (nzc.eq.0) go to 550
         do 540 nc=1,nzc
            kc = ip(jp,2) + nc - 1
            ir = ind(kc,1)
c search non-pivot row for element to be eliminated.
            kr = ip(ir,1)
            krl = kr + iw(ir,1) - 1
            do 330 knp=kr,krl
               if (jp.eq.ind(knp,2)) go to 340
  330       continue
c bring element to be eliminated to front of its row.
  340       am = a(knp)
            a(knp) = a(kr)
            a(kr) = am
            ind(knp,2) = ind(kr,2)
            ind(kr,2) = jp
            am = -a(kr)/a(kp)
c compress row file unless it is certain that there is room for new row.
            if (lrow+iw(ir,1)+iw(ipp,1)+lenl.le.ia) go to 350
            if (ncp.ge.mcp .or. lenu+iw(ir,1)+iw(ipp,1)+lenl.gt.ia) go
     *       to 710
            call lpla05es(a, ind(1,2), ip, n, iw, ia, .true.)
            kp = ip(ipp,1)
            kr = ip(ir,1)
  350       krl = kr + iw(ir,1) - 1
            kq = kp + 1
            kpl = kp + iw(ipp,1) - 1
c place pivot row (excluding pivot itself) in w.
            if (kq.gt.kpl) go to 370
            do 360 k=kq,kpl
               j = ind(k,2)
               w(j) = a(k)
  360       continue
  370       ip(ir,1) = lrow + 1
c
c transfer modified elements.
            ind(kr,2) = 0
            kr = kr + 1
            if (kr.gt.krl) go to 430
            do 420 ks=kr,krl
               j = ind(ks,2)
               au = a(ks) + am*w(j)
               ind(ks,2) = 0
c if element is very small remove it from u.
               if (abs(au).le.small) go to 380
               g = max(g,abs(au))
               lrow = lrow + 1
               a(lrow) = au
               ind(lrow,2) = j
               go to 410
  380          lenu = lenu - 1
c remove element from col file.
               k = ip(j,2)
               kl = k + iw(j,2) - 1
               iw(j,2) = kl - k
               do 390 kk=k,kl
                  if (ind(kk,1).eq.ir) go to 400
  390          continue
  400          ind(kk,1) = ind(kl,1)
               ind(kl,1) = 0
  410          w(j) = 0.
  420       continue
c
c scan pivot row for fills.
  430       if (kq.gt.kpl) go to 520
            do 510 ks=kq,kpl
               j = ind(ks,2)
               au = am*w(j)
               if (abs(au).le.small) go to 500
               lrow = lrow + 1
               a(lrow) = au
               ind(lrow,2) = j
               lenu = lenu + 1
c
c create fill in column file.
               nz = iw(j,2)
               k = ip(j,2)
               kl = k + nz - 1
               if (nz .eq. 0) go to 460
c if possible place new element at end of present entry.
               if (kl.ne.lcol) go to 440
               if (lcol+lenl.ge.ia) go to 460
               lcol = lcol + 1
               go to 450
  440          if (ind(kl+1,1).ne.0) go to 460
  450          ind(kl+1,1) = ir
               go to 490
c new entry has to be created.
  460          if (lcol+lenl+nz+1.lt.ia) go to 470
c compress column file if there is not room for new entry.
               if (ncp.ge.mcp .or. lenu+lenl+nz+1.ge.ia) go to 710
               call lpla05es(a, ind, ip(1,2), n, iw(1,2), ia, .false.)
               k = ip(j,2)
               kl = k + nz - 1
c transfer old entry into new.
  470          ip(j,2) = lcol + 1
               if (kl .lt. k) go to 485
               do 480 kk=k,kl
                  lcol = lcol + 1
                  ind(lcol,1) = ind(kk,1)
                  ind(kk,1) = 0
  480          continue
  485          continue
c add new element.
               lcol = lcol + 1
               ind(lcol,1) = ir
  490          g = max(g,abs(au))
               iw(j,2) = nz + 1
  500          w(j) = 0.
  510       continue
  520       iw(ir,1) = lrow + 1 - ip(ir,1)
c
c store multiplier
            if (lenl+lcol+1.le.ia) go to 530
c compress col file if necessary.
            if (ncp.ge.mcp) go to 710
            call lpla05es(a, ind, ip(1,2), n, iw(1,2), ia, .false.)
  530       k = ia - lenl
            lenl = lenl + 1
            a(k) = am
            ind(k,1) = ipp
            ind(k,2) = ir
            lenu = lenu - 1
  540    continue
c
c insert rows and columns involved in elimination in linked lists
c     of equal numbers of non-zeros.
  550    k1 = ip(jp,2)
         k2 = iw(jp,2) + k1 - 1
         iw(jp,2) = 0
         do 580 l=1,2
            if (k2.lt.k1) go to 570
            do 560 k=k1,k2
               ir = ind(k,l)
               if (l.eq.1) ind(k,l) = 0
               nz = iw(ir,l)
               if (nz.le.0) go to 720
               in = iw(nz,l+2)
               iw(ir,l+6) = in
               iw(ir,l+4) = 0
               iw(nz,l+2) = ir
               if (in.ne.0) iw(in,l+4) = ir
  560       continue
  570       k1 = ip(ipp,1) + 1
            k2 = iw(ipp,1) + k1 - 2
  580    continue
  590 continue
c
c reset column file to refer to u and store row/col numbers in
c     pivotal order in iw(.,3),iw(.,4)
      do 600 i=1,n
         j = -iw(i,5)
         iw(j,3) = i
         j = -iw(i,6)
         iw(j,4) = i
         iw(i,2) = 0
  600 continue
      do 620 i=1,n
         kp = ip(i,1)
         kl = iw(i,1) + kp - 1
         do 610 k=kp,kl
            j = ind(k,2)
            iw(j,2) = iw(j,2) + 1
  610    continue
  620 continue
      k = 1
      do 630 i=1,n
         k = k + iw(i,2)
         ip(i,2) = k
  630 continue
      lcol = k - 1
      do 650 ii=1,n
         i = iw(ii,3)
         kp = ip(i,1)
         kl = iw(i,1) + kp - 1
         do 640 k=kp,kl
            j = ind(k,2)
            kn = ip(j,2) - 1
            ip(j,2) = kn
            ind(kn,1) = i
  640    continue
  650 continue
      return
c
c     the following instructions implement the failure exits.
c
  660 if (lp.gt.0) then
         write (xern1, '(i8)') ir
         write (xern2, '(i8)') j
         call lpxermsg ('slatec', 'la05as', 'more than one matrix ' //
     *      'entry.  here row = ' // xern1 // ' and col = ' // xern2,
     *      -4, 1)
      endif
      g = -4.
      return
c
  670 if (lp.gt.0) call lpxermsg ('slatec', 'la05as',
     *   'the order of the system, n, is not positive.', -1, 1)
      g = -1.0e0
      return
c
  680 if (lp.gt.0) then
         write (xern0, '(i8)') k
         write (xern1, '(i8)') i
         write (xern2, '(i8)') j
         call lpxermsg ('slatec', 'la05as', 'element k = ' // xern0 //
     *      ' is out of bounds.$$here row = ' // xern1 //
     *      ' and col = ' // xern2, -3, 1)
      endif
      g = -3.
      return
c
  700 if (lp.gt.0) then
         write (xern1, '(i8)') l
         call lpxermsg ('slatec', 'la05as', 'row or column has no ' //
     *      'elements.  here index = ' // xern1, -2, 1)
      endif
      g = -2.
      return
c
  710 if (lp.gt.0) call lpxermsg ('slatec', 'la05as',
     *   'lengths of arrays a(*) and ind(*,2) are too small.', -7, 1)
      g = -7.
      return
c
  720 ipv = ipv + 1
      iw(ipv,1) = ir
      do 730 i=1,n
         ii = -iw(i,l+4)
         if (ii.gt.0) iw(ii,1) = i
  730 continue
c
      if (lp.gt.0) then
         xern1 = 'rows'
         if (l.eq.2) xern1 = 'columns'
        call lpxermsg ('slatec', 'la05as', 'dependant ' // xern1, -5, 1)
c
  740    write (xern1, '(i8)') iw(i,1)
         xern2 = ' '
         if (i+1.le.ipv) write (xern2, '(i8)') iw(i+1,1)
         call lpxermsg ('slatec', 'la05as',
     *      'dependent vector indices are ' // xern1 // ' and ' //
     *      xern2, -5, 1)
         i = i + 2
         if (i.le.ipv) go to 740
      endif
      g = -5.
      return
      end subroutine lpla05as
      subroutine lpla05bs (a, ind, ia, n, ip, iw, w, g, b, trans)
c***begin prologue  la05bs
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (la05bs-s, la05bd-d)
c***author  (unknown)
c***description
c
c     this subprogram is a slight modification of a subprogram
c     from the c. 1979 aere harwell library.  the name of the
c     corresponding harwell code can be obtained by deleting
c     the final letter =s= in the names used here.
c     revised sep. 13, 1979.
c
c     royalties have been paid to aere-uk for use of their codes
c     in the package given here.  any primary usage of the harwell
c     subroutines requires a royalty agreement and payment between
c     the user and aere-uk.  any usage of the sandia written codes
c     splp( ) (which uses the harwell subroutines) is permitted.
c
c ip(i,1),ip(i,2) point to start of row/column i of u.
c iw(i,1),iw(i,2) are lengths of row/col i of u.
c iw(.,3),iw(.,4) hold row/col numbers in pivotal order.
c
c***see also  splp
c***routines called  xermsg, xsetun
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900402  added type section.  (wrb)
c   920410  corrected second dimension on iw declaration.  (wrb)
c***end prologue  la05bs
      real a(ia), b(*), am, w(*), g, small
      logical trans
      integer ind(ia,2), iw(n,8)
      integer ip(n,2)
      common /la05ds/ small, lp, lenl, lenu, ncp, lrow, lcol
c***first executable statement  la05bs
      if (g.lt.0.) go to 130
      kll = ia - lenl + 1
      if (trans) go to 80
c
c     multiply vector by inverse of l
      if (lenl.le.0) go to 20
      l1 = ia + 1
      do 10 kk=1,lenl
         k = l1 - kk
         i = ind(k,1)
         if (b(i).eq.0.) go to 10
         j = ind(k,2)
         b(j) = b(j) + a(k)*b(i)
   10 continue
   20 do 30 i=1,n
         w(i) = b(i)
         b(i) = 0.
   30 continue
c
c     multiply vector by inverse of u
      n1 = n + 1
      do 70 ii=1,n
         i = n1 - ii
         i = iw(i,3)
         am = w(i)
         kp = ip(i,1)
         if (kp.gt.0) go to 50
         kp = -kp
         ip(i,1) = kp
         nz = iw(i,1)
         kl = kp - 1 + nz
         k2 = kp + 1
         do 40 k=k2,kl
            j = ind(k,2)
            am = am - a(k)*b(j)
   40    continue
   50    if (am.eq.0.) go to 70
         j = ind(kp,2)
         b(j) = am/a(kp)
         kpc = ip(j,2)
         kl = iw(j,2) + kpc - 1
         if (kl.eq.kpc) go to 70
         k2 = kpc + 1
         do 60 k=k2,kl
            i = ind(k,1)
            ip(i,1) = -abs(ip(i,1))
   60    continue
   70 continue
      go to 140
c
c     multiply vector by inverse of transpose of u
   80 do 90 i=1,n
         w(i) = b(i)
         b(i) = 0.
   90 continue
      do 110 ii=1,n
         i = iw(ii,4)
         am = w(i)
         if (am.eq.0.) go to 110
         j = iw(ii,3)
         kp = ip(j,1)
         am = am/a(kp)
         b(j) = am
         kl = iw(j,1) + kp - 1
         if (kp.eq.kl) go to 110
         k2 = kp + 1
         do 100 k=k2,kl
            i = ind(k,2)
            w(i) = w(i) - am*a(k)
  100    continue
  110 continue
c
c     multiply vector by inverse of transpose of l
      if (kll.gt.ia) return
      do 120 k=kll,ia
         j = ind(k,2)
         if (b(j).eq.0.) go to 120
         i = ind(k,1)
         b(i) = b(i) + a(k)*b(j)
  120 continue
      go to 140
c
  130 call lpxsetun(lp)
      if (lp .gt. 0) call lpxermsg ('slatec', 'la05bs',
     +   'earlier entry gave error return.', -8, 2)
  140 return
      end
      subroutine lpla05cs (a, ind, ia, n, ip, iw, w, g, u, mm)
c***begin prologue  la05cs
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (la05cs-s, la05cd-d)
c***author  (unknown)
c***description
c
c     this subprogram is a slight modification of a subprogram
c     from the c. 1979 aere harwell library.  the name of the
c     corresponding harwell code can be obtained by deleting
c     the final letter =s= in the names used here.
c     revised sep. 13, 1979.
c
c     royalties have been paid to aere-uk for use of their codes
c     in the package given here.  any primary usage of the harwell
c     subroutines requires a royalty agreement and payment between
c     the user and aere-uk.  any usage of the sandia written codes
c     splp( ) (which uses the harwell subroutines) is permitted.
c
c***see also  splp
c***routines called  la05es, xermsg, xsetun
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  corrected references to xerrwv.  (wrb)
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900402  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c   920410  corrected second dimension on iw declaration.  (wrb)
c   920422  changed upper limit on do from last to last-1.  (wrb)
c***end prologue  la05cs
      real a(*), g, u, am, w(*), small, au
      integer ind(ia,2), iw(n,8)
      integer ip(n,2)
      character*8 xern1
c
      common /la05ds/ small, lp, lenl, lenu, ncp, lrow, lcol
c***first executable statement  la05cs
      call lpxsetun(lp)
      if (g.lt.0.0e0) go to 620
      jm = mm
c mcp limits the value of ncp permitted before an error return results.
      mcp = ncp + 20
c remove old column
      lenu = lenu - iw(jm,2)
      kp = ip(jm,2)
      im = ind(kp,1)
      kl = kp + iw(jm,2) - 1
      iw(jm,2) = 0
      do 30 k=kp,kl
         i = ind(k,1)
         ind(k,1) = 0
         kr = ip(i,1)
         nz = iw(i,1) - 1
         iw(i,1) = nz
         krl = kr + nz
         do 10 km=kr,krl
            if (ind(km,2).eq.jm) go to 20
   10    continue
   20    a(km) = a(krl)
         ind(km,2) = ind(krl,2)
         ind(krl,2) = 0
   30 continue
c
c insert new column
      do 110 ii=1,n
         i = iw(ii,3)
         if (i.eq.im) m = ii
         if (abs(w(i)).le.small) go to 100
         lenu = lenu + 1
         last = ii
         if (lcol+lenl.lt.ia) go to 40
c compress column file if necessary.
         if (ncp.ge.mcp .or. lenl+lenu.ge.ia) go to 610
         call lpla05es(a, ind, ip(1,2), n, iw(1,2), ia, .false.)
   40    lcol = lcol + 1
         nz = iw(jm,2)
         if (nz.eq.0) ip(jm,2) = lcol
         iw(jm,2) = nz + 1
         ind(lcol,1) = i
         nz = iw(i,1)
         kpl = ip(i,1) + nz
         if (kpl.gt.lrow) go to 50
         if (ind(kpl,2).eq.0) go to 90
c new entry has to be created.
   50    if (lenl+lrow+nz.lt.ia) go to 60
         if (ncp.ge.mcp .or. lenl+lenu+nz.ge.ia) go to 610
c compress row file if necessary.
         call lpla05es(a, ind(1,2), ip, n, iw, ia, .true.)
   60    kp = ip(i,1)
         ip(i,1) = lrow + 1
         if (nz.eq.0) go to 80
         kpl = kp + nz - 1
         do 70 k=kp,kpl
            lrow = lrow + 1
            a(lrow) = a(k)
            ind(lrow,2) = ind(k,2)
            ind(k,2) = 0
   70    continue
   80    lrow = lrow + 1
         kpl = lrow
c place new element at end of row.
   90    iw(i,1) = nz + 1
         a(kpl) = w(i)
         ind(kpl,2) = jm
  100    w(i) = 0.0e0
  110 continue
      if (iw(im,1).eq.0 .or. iw(jm,2).eq.0 .or. m.gt.last) go to 590
c
c find column singletons, other than the spike. non-singletons are
c     marked with w(j)=1. only iw(.,3) is revised and iw(.,4) is used
c     for workspace.
      ins = m
      m1 = m
      w(jm) = 1.0e0
      do 140 ii=m,last
         i = iw(ii,3)
         j = iw(ii,4)
         if (w(j).eq.0.0e0) go to 130
         kp = ip(i,1)
         kl = kp + iw(i,1) - 1
         do 120 k=kp,kl
            j = ind(k,2)
            w(j) = 1.0e0
  120    continue
         iw(ins,4) = i
         ins = ins + 1
         go to 140
c place singletons in new position.
  130    iw(m1,3) = i
         m1 = m1 + 1
  140 continue
c place non-singletons in new position.
      ij = m + 1
      do 150 ii=m1,last-1
         iw(ii,3) = iw(ij,4)
         ij = ij + 1
  150 continue
c place spike at end.
      iw(last,3) = im
c
c find row singletons, apart from spike row. non-singletons are marked
c     with w(i)=2. again only iw(.,3) is revised and iw(.,4) is used
c     for workspace.
      last1 = last
      jns = last
      w(im) = 2.0e0
      j = jm
      do 180 ij=m1,last
         ii = last + m1 - ij
         i = iw(ii,3)
         if (w(i).ne.2.0e0) go to 170
         k = ip(i,1)
         if (ii.ne.last) j = ind(k,2)
         kp = ip(j,2)
         kl = kp + iw(j,2) - 1
         iw(jns,4) = i
         jns = jns - 1
         do 160 k=kp,kl
            i = ind(k,1)
            w(i) = 2.0e0
  160    continue
         go to 180
  170    iw(last1,3) = i
         last1 = last1 - 1
  180 continue
      do 190 ii=m1,last1
         jns = jns + 1
         i = iw(jns,4)
         w(i) = 3.0e0
         iw(ii,3) = i
  190 continue
c
c deal with singleton spike column. note that bump rows are marked by
c    w(i)=3.0e0
      do 230 ii=m1,last1
         kp = ip(jm,2)
         kl = kp + iw(jm,2) - 1
         is = 0
         do 200 k=kp,kl
            l = ind(k,1)
            if (w(l).ne.3.0e0) go to 200
            if (is.ne.0) go to 240
            i = l
            knp = k
            is = 1
  200    continue
         if (is.eq.0) go to 590
c make a(i,jm) a pivot.
         ind(knp,1) = ind(kp,1)
         ind(kp,1) = i
         kp = ip(i,1)
         do 210 k=kp,ia
            if (ind(k,2).eq.jm) go to 220
  210    continue
  220    am = a(kp)
         a(kp) = a(k)
         a(k) = am
         ind(k,2) = ind(kp,2)
         ind(kp,2) = jm
         jm = ind(k,2)
         iw(ii,4) = i
         w(i) = 2.0e0
  230 continue
      ii = last1
      go to 260
  240 in = m1
      do 250 ij=ii,last1
         iw(ij,4) = iw(in,3)
         in = in + 1
  250 continue
  260 last2 = last1 - 1
      if (m1.eq.last1) go to 570
      do 270 i=m1,last2
         iw(i,3) = iw(i,4)
  270 continue
      m1 = ii
      if (m1.eq.last1) go to 570
c
c clear w
      do 280 i=1,n
         w(i) = 0.0e0
  280 continue
c
c perform elimination
      ir = iw(last1,3)
      do 560 ii=m1,last1
         ipp = iw(ii,3)
         kp = ip(ipp,1)
         kr = ip(ir,1)
         jp = ind(kp,2)
         if (ii.eq.last1) jp = jm
c search non-pivot row for element to be eliminated.
c  and bring it to front of its row
         krl = kr + iw(ir,1) - 1
         do 290 knp=kr,krl
            if (jp.eq.ind(knp,2)) go to 300
  290    continue
         if (ii-last1) 560, 590, 560
c bring element to be eliminated to front of its row.
  300    am = a(knp)
         a(knp) = a(kr)
         a(kr) = am
         ind(knp,2) = ind(kr,2)
         ind(kr,2) = jp
         if (ii.eq.last1) go to 310
         if (abs(a(kp)).lt.u*abs(am)) go to 310
         if (abs(am).lt.u*abs(a(kp))) go to 340
         if (iw(ipp,1).le.iw(ir,1)) go to 340
c perform interchange
  310    iw(last1,3) = ipp
         iw(ii,3) = ir
         ir = ipp
         ipp = iw(ii,3)
         k = kr
         kr = kp
         kp = k
         kj = ip(jp,2)
         do 320 k=kj,ia
            if (ind(k,1).eq.ipp) go to 330
  320    continue
  330    ind(k,1) = ind(kj,1)
         ind(kj,1) = ipp
  340    if (a(kp).eq.0.0e0) go to 590
         if (ii.eq.last1) go to 560
         am = -a(kr)/a(kp)
c compress row file unless it is certain that there is room for new row.
         if (lrow+iw(ir,1)+iw(ipp,1)+lenl.le.ia) go to 350
         if (ncp.ge.mcp .or. lenu+iw(ir,1)+iw(ipp,1)+lenl.gt.ia) go to
     *    610
         call lpla05es(a, ind(1,2), ip, n, iw, ia, .true.)
         kp = ip(ipp,1)
         kr = ip(ir,1)
  350    krl = kr + iw(ir,1) - 1
         kq = kp + 1
         kpl = kp + iw(ipp,1) - 1
c place pivot row (excluding pivot itself) in w.
         if (kq.gt.kpl) go to 370
         do 360 k=kq,kpl
            j = ind(k,2)
            w(j) = a(k)
  360    continue
  370    ip(ir,1) = lrow + 1
c
c transfer modified elements.
         ind(kr,2) = 0
         kr = kr + 1
         if (kr.gt.krl) go to 430
         do 420 ks=kr,krl
            j = ind(ks,2)
            au = a(ks) + am*w(j)
            ind(ks,2) = 0
c if element is very small remove it from u.
            if (abs(au).le.small) go to 380
            g = max(g,abs(au))
            lrow = lrow + 1
            a(lrow) = au
            ind(lrow,2) = j
            go to 410
  380       lenu = lenu - 1
c remove element from col file.
            k = ip(j,2)
            kl = k + iw(j,2) - 1
            iw(j,2) = kl - k
            do 390 kk=k,kl
               if (ind(kk,1).eq.ir) go to 400
  390       continue
  400       ind(kk,1) = ind(kl,1)
            ind(kl,1) = 0
  410       w(j) = 0.0e0
  420    continue
c
c scan pivot row for fills.
  430    if (kq.gt.kpl) go to 520
         do 510 ks=kq,kpl
            j = ind(ks,2)
            au = am*w(j)
            if (abs(au).le.small) go to 500
            lrow = lrow + 1
            a(lrow) = au
            ind(lrow,2) = j
            lenu = lenu + 1
c
c create fill in column file.
            nz = iw(j,2)
            k = ip(j,2)
            kl = k + nz - 1
c if possible place new element at end of present entry.
            if (kl.ne.lcol) go to 440
            if (lcol+lenl.ge.ia) go to 460
            lcol = lcol + 1
            go to 450
  440       if (ind(kl+1,1).ne.0) go to 460
  450       ind(kl+1,1) = ir
            go to 490
c new entry has to be created.
  460       if (lcol+lenl+nz+1.lt.ia) go to 470
c compress column file if there is not room for new entry.
            if (ncp.ge.mcp .or. lenu+lenl+nz+1.ge.ia) go to 610
            call lpla05es(a, ind, ip(1,2), n, iw(1,2), ia, .false.)
            k = ip(j,2)
            kl = k + nz - 1
c transfer old entry into new.
  470       ip(j,2) = lcol + 1
            do 480 kk=k,kl
               lcol = lcol + 1
               ind(lcol,1) = ind(kk,1)
               ind(kk,1) = 0
  480       continue
c add new element.
            lcol = lcol + 1
            ind(lcol,1) = ir
  490       g = max(g,abs(au))
            iw(j,2) = nz + 1
  500       w(j) = 0.0e0
  510    continue
  520    iw(ir,1) = lrow + 1 - ip(ir,1)
c
c store multiplier
         if (lenl+lcol+1.le.ia) go to 530
c compress col file if necessary.
         if (ncp.ge.mcp) go to 610
         call lpla05es(a, ind, ip(1,2), n, iw(1,2), ia, .false.)
  530    k = ia - lenl
         lenl = lenl + 1
         a(k) = am
         ind(k,1) = ipp
         ind(k,2) = ir
c create blank in pivotal column.
         kp = ip(jp,2)
         nz = iw(jp,2) - 1
         kl = kp + nz
         do 540 k=kp,kl
            if (ind(k,1).eq.ir) go to 550
  540    continue
  550    ind(k,1) = ind(kl,1)
         iw(jp,2) = nz
         ind(kl,1) = 0
         lenu = lenu - 1
  560 continue
c
c construct column permutation and store it in iw(.,4)
  570 do 580 ii=m,last
         i = iw(ii,3)
         k = ip(i,1)
         j = ind(k,2)
         iw(ii,4) = j
  580 continue
      return
c
c     the following instructions implement the failure exits.
c
  590 if (lp.gt.0) then
         write (xern1, '(i8)') mm
         call lpxermsg ('slatec', 'la05cs', 'singular matrix after ' //
     *      'replacement of column.  index = ' // xern1, -6, 1)
      endif
      g = -6.0e0
      return
c
  610 if (lp.gt.0) call lpxermsg ('slatec', 'la05cs',
     *   'lengths of arrays a(*) and ind(*,2) are too small.', -7, 1)
      g = -7.0e0
      return
c
  620 if (lp.gt.0) call lpxermsg ('slatec', 'la05cs',
     *   'earlier entry gave error return.', -8, 2)
      g = -8.0e0
      return
      end subroutine lpla05cs
      subroutine lpla05es (a, irn, ip, n, iw, ia, reals)
c***begin prologue  la05es
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (la05es-s, la05ed-d)
c***author  (unknown)
c***description
c
c     this subprogram is a slight modification of a subprogram
c     from the c. 1979 aere harwell library.  the name of the
c     corresponding harwell code can be obtained by deleting
c     the final letter =s= in the names used here.
c     revised sep. 13, 1979.
c
c     royalties have been paid to aere-uk for use of their codes
c     in the package given here.  any primary usage of the harwell
c     subroutines requires a royalty agreement and payment between
c     the user and aere-uk.  any usage of the sandia written codes
c     splp( ) (which uses the harwell subroutines) is permitted.
c
c***see also  splp
c***routines called  (none)
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900402  added type section.  (wrb)
c***end prologue  la05es
      logical reals
      real a(*)
      integer irn(*), iw(*)
      integer ip(*)
      common /la05ds/ small, lp, lenl, lenu, ncp, lrow, lcol
c***first executable statement  la05es
      ncp = ncp + 1
c     compress file of positive integers. entry j starts at irn(ip(j))
c  and contains iw(j) integers,j=1,n. other components of irn are zero.
c  length of compressed file placed in lrow if reals is .true. or lcol
c  otherwise.
c  if reals is .true. array a contains a real file associated with irn
c  and this is compressed too.
c  a,irn,ip,iw,ia are input/output variables.
c  n,reals are input/unchanged variables.
c
      do 10 j=1,n
c store the last element of entry j in iw(j) then overwrite it by -j.
         nz = iw(j)
         if (nz.le.0) go to 10
         k = ip(j) + nz - 1
         iw(j) = irn(k)
         irn(k) = -j
   10 continue
c kn is the position of next entry in compressed file.
      kn = 0
      ipi = 0
      kl = lcol
      if (reals) kl = lrow
c loop through the old file skipping zero (dummy) elements and
c     moving genuine elements forward. the entry number becomes
c     known only when its end is detected by the presence of a negative
c     integer.
      do 30 k=1,kl
         if (irn(k).eq.0) go to 30
         kn = kn + 1
         if (reals) a(kn) = a(k)
         if (irn(k).ge.0) go to 20
c end of entry. restore irn(k), set pointer to start of entry and
c     store current kn in ipi ready for use when next last entry
c     is detected.
         j = -irn(k)
         irn(k) = iw(j)
         ip(j) = ipi + 1
         iw(j) = kn - ipi
         ipi = kn
   20    irn(kn) = irn(k)
   30 continue
      if (reals) lrow = kn
      if (.not.reals) lcol = kn
      return
      end subroutine lpla05es
      subroutine lpmc20as (nc, maxa, a, inum, jptr, jnum, jdisp)
c***begin prologue  mc20as
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (mc20as-s, mc20ad-d)
c***author  (unknown)
c***description
c
c     this subprogram is a slight modification of a subprogram
c     from the c. 1979 aere harwell library.  the name of the
c     corresponding harwell code can be obtained by deleting
c     the final letter =s= in the names used here.
c     revised sep. 13, 1979.
c
c     royalties have been paid to aere-uk for use of their codes
c     in the package given here.  any primary usage of the harwell
c     subroutines requires a royalty agreement and payment between
c     the user and aere-uk.  any usage of the sandia written codes
c     splp( ) (which uses the harwell subroutines) is permitted.
c
c***see also  splp
c***routines called  (none)
c***revision history  (yymmdd)
c   811215  date written
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900402  added type section.  (wrb)
c***end prologue  mc20as
      integer inum(*), jnum(*)
      real a(*)
      dimension jptr(nc)
c***first executable statement  mc20as
      null = -jdisp
c**      clear jptr
      do 10 j=1,nc
         jptr(j) = 0
   10 continue
c**      count the number of elements in each column.
      do 20 k=1,maxa
         j = jnum(k) + jdisp
         jptr(j) = jptr(j) + 1
   20 continue
c**      set the jptr array
      k = 1
      do 30 j=1,nc
         kr = k + jptr(j)
         jptr(j) = k
         k = kr
   30 continue
c
c**      reorder the elements into column order.  the algorithm is an
c        in-place sort and is of order maxa.
      do 50 i=1,maxa
c        establish the current entry.
         jce = jnum(i) + jdisp
         if (jce.eq.0) go to 50
         ace = a(i)
         ice = inum(i)
c        clear the location vacated.
         jnum(i) = null
c        chain from current entry to store items.
         do 40 j=1,maxa
c        current entry not in correct position.  determine correct
c        position to store entry.
            loc = jptr(jce)
            jptr(jce) = jptr(jce) + 1
c        save contents of that location.
            acep = a(loc)
            icep = inum(loc)
            jcep = jnum(loc)
c        store current entry.
            a(loc) = ace
            inum(loc) = ice
            jnum(loc) = null
c        check if next current entry needs to be processed.
            if (jcep.eq.null) go to 50
c        it does.  copy into current entry.
            ace = acep
            ice = icep
            jce = jcep + jdisp
   40    continue
c
   50 continue
c
c**      reset jptr vector.
      ja = 1
      do 60 j=1,nc
         jb = jptr(j)
         jptr(j) = ja
         ja = jb
   60 continue
      return
      end subroutine lpmc20as
      subroutine lppchngs (ii, xval, iplace, sx, ix, ircx)
c***begin prologue  pchngs
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (pchngs-s, dpchng-d)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     pchngs limits the type of storage to a sequential scheme.
c     sparse matrix element alteration subroutine.
c
c     subroutine lppchngs() changes element ii in vector +/- ircx to the
c     value xval.
c
c            ii the absolute value of this integer is the subscript for
c               the element to be changed.
c          xval new value of the matrix element being changed.
c     iplace pointer information which is maintained by the package.
c   sx(*),ix(*) the work arrays which are used to store the sparse
c               matrix. these arrays are automatically maintained by the
c               package for the user.
c          ircx points to the vector of the matrix being updated.
c               a negative value of ircx indicates that row -ircx is
c               being updated.  a positive value of ircx indicates that
c               column ircx is being updated.  a zero value of ircx is
c               an error.
c
c     since data items are kept sorted in the sequential data structure,
c     changing a matrix element can require the movement of all the data
c     items in the matrix. for this reason, it is suggested that data
c     items be added a col. at a time, in ascending col. sequence.
c     furthermore, since deleting items from the data structure may also
c     require moving large amounts of data, zero elements are explicitly
c     stored in the matrix.
c
c     this subroutine lpis a modification of the subroutine lplchngs,
c     sandia labs. rept. sand78-0785.
c     modifications by k.l. hiebert and r.j. hanson
c     revised 811130-1000
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  lpiploc, prwpge, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   910403  updated author and description sections.  (wrb)
c***end prologue  pchngs
      dimension ix(*)
      integer lpiploc
      real sx(*),xval,zero,one,sxlast,sxval
      save zero, one
      data zero,one /0.e0,1.e0/
c***first executable statement  pchngs
      iopt=1
c
c     determine null-cases..
      if(ii.eq.0) return
c
c     check validity of row/col. index.
c
      if (.not.(ircx.eq.0)) go to 20002
      nerr=55
      call lpxermsg ('slatec', 'pchngs', 'ircx=0.', nerr, iopt)
20002 lmx = ix(1)
c
c     lmx is the length of the in-memory storage area.
c
      if (.not.(ircx.lt.0)) go to 20005
c
c     check subscripts of the row. the row number must be .le. m and
c     the index must be .le. n.
c
      if (.not.(ix(2).lt.-ircx .or. ix(3).lt.abs(ii))) go to 20008
      nerr=55
      call lpxermsg ('slatec', 'pchngs',
     +   'subscripts for array element to be accessed were out of ' //
     +   'bounds.', nerr, iopt)
20008 go to 20006
c
c     check subscripts of the column. the col. number must be .le. n and
c     the index must be .le. m.
c
20005 if (.not.(ix(3).lt.ircx .or. ix(2).lt.abs(ii))) go to 20011
      nerr=55
      call lpxermsg ('slatec', 'pchngs',
     +   'subscripts for array element to be accessed were out of ' //
     +   'bounds.', nerr, iopt)
20011 continue
c
c     set i to be the element of row/column j to be changed.
c
20006 if (.not.(ircx.gt.0)) go to 20014
      i = abs(ii)
      j = abs(ircx)
      go to 20015
20014 i = abs(ircx)
      j = abs(ii)
c
c     the integer ll points to the start of the matrix element data.
c
20015 ll=ix(3)+4
      ii = abs(ii)
      lpg = lmx - ll
c
c     set iplace to start our scan for the element at the beginning
c     of the vector.
c
      if (.not.(j.eq.1)) go to 20017
      iplace=ll+1
      go to 20018
20017 iplace=ix(j+3)+1
c
c     iend points to the last element of the vector to be scanned.
c
20018 iend = ix(j+4)
c
c     scan through several pages, if necessary, to find matrix element.
c
      ipl = lpiploc(iplace,sx,ix)
      np = abs(ix(lmx-1))
      go to 20021
20020 if (ilast.eq.iend) go to 20022
c
c     the virtual end of data for this page is ilast.
c
20021 ilast = min(iend,np*lpg+ll-2)
c
c     the relative end of data for this page is il.
c     search for a matrix value with an index .ge. i on the present
c     page.
c
      il = lpiploc(ilast,sx,ix)
      il = min(il,lmx-2)
20023 if (.not.(.not.(ipl.ge.il .or. ix(ipl).ge.i))) go to 20024
      ipl=ipl+1
      go to 20023
c
c     set iplace and store data item if found.
c
20024 if (.not.(ix(ipl).eq.i .and. ipl.le.il)) go to 20025
      sx(ipl) = xval
      sx(lmx) = one
      return
c
c     exit from loop if item was found.
c
20025 if(ix(ipl).gt.i .and. ipl.le.il) ilast = iend
      if (.not.(ilast.ne.iend)) go to 20028
      ipl = ll + 1
      np = np + 1
20028 go to 20020
c
c     insert new data item into location at iplace(ipl).
c
20022 if (.not.(ipl.gt.il.or.(ipl.eq.il.and.i.gt.ix(ipl)))) go to 20031
      ipl = il + 1
      if(ipl.eq.lmx-1) ipl = ipl + 2
20031 iplace = (np-1)*lpg + ipl
c
c     go to a new page, if necessary, to insert the item.
c
      if (.not.(ipl.le.lmx .or. ix(lmx-1).ge.0)) go to 20034
      ipl=lpiploc(iplace,sx,ix)
20034 iend = ix(ll)
      np = abs(ix(lmx-1))
      sxval = xval
c
c     loop through all subsequent pages of the matrix moving data down.
c     this is necessary to make room for the new matrix element and
c     keep the entries sorted.
c
      go to 20038
20037 if (ix(lmx-1).le.0) go to 20039
20038 ilast = min(iend,np*lpg+ll-2)
      il = lpiploc(ilast,sx,ix)
      il = min(il,lmx-2)
      sxlast = sx(il)
      ixlast = ix(il)
      istart = ipl + 1
      if (.not.(istart.le.il)) go to 20040
      k = istart + il
      do 50 jj=istart,il
      sx(k-jj) = sx(k-jj-1)
      ix(k-jj) = ix(k-jj-1)
50    continue
      sx(lmx) = one
20040 if (.not.(ipl.le.lmx)) go to 20043
      sx(ipl) = sxval
      ix(ipl) = i
      sxval = sxlast
      i = ixlast
      sx(lmx) = one
      if (.not.(ix(lmx-1).gt.0)) go to 20046
      ipl = ll + 1
      np = np + 1
20046 continue
20043 go to 20037
20039 np = abs(ix(lmx-1))
c
c     determine if a new page is to be created for the last element
c     moved down.
c
      il = il + 1
      if (.not.(il.eq.lmx-1)) go to 20049
c
c     create a new page.
c
      ix(lmx-1) = np
c
c     write the old page.
c
      sx(lmx) = zero
      key = 2
      call lpprwpge(key,np,lpg,sx,ix)
      sx(lmx) = one
c
c     store last element moved down in a new page.
c
      ipl = ll + 1
      np = np + 1
      ix(lmx-1) = -np
      sx(ipl) = sxval
      ix(ipl) = i
      go to 20050
c
c     last element moved remained on the old page.
c
20049 if (.not.(ipl.ne.il)) go to 20052
      sx(il) = sxval
      ix(il) = i
      sx(lmx) = one
20052 continue
c
c     increment pointers to last element in vectors j,j+1,... .
c
20050 jstart = j + 4
      jj=jstart
      n20055=ll
      go to 20056
20055 jj=jj+1
20056 if ((n20055-jj).lt.0) go to 20057
      ix(jj) = ix(jj) + 1
      if(mod(ix(jj)-ll,lpg).eq.lpg-1) ix(jj) = ix(jj) + 2
      go to 20055
c
c     iplace points to the inserted data item.
c
20057 ipl=lpiploc(iplace,sx,ix)
      return
      end subroutine lppchngs
      subroutine lppinitm (m, n, sx, ix, lmx, ipagef)
c***begin prologue  pinitm
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (pinitm-s, dpintm-d)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     pinitm limits the type of storage to a sequential scheme.
c     the matrix is stored by columns.
c     sparse matrix initialization subroutine.
c
c            m=number of rows of the matrix.
c            n=number of columns of the matrix.
c  sx(*),ix(*)=the work arrays which are used to store the sparse
c              matrix.  these arrays are automatically maintained by
c              the package for the user.
c          lmx=length of the work array sx(*).
c              lmx must be at least n+7 where
c              for greatest efficiency lmx should be at least n+nz+6
c              where nz is the maximum number of nonzeroes to be
c              stored in the matrix.  values of lmx between n+7 and
c              n+nz+6 will cause demand paging to occur.
c              this is implemented by the package.
c              ix(*) must be dimensioned at least lmx
c      ipagef=unit number where demand pages will be stored.
c
c     this subroutine lpis a modification of the subroutine lplinitm,
c     sandia labs. rept. sand78-0785.
c     modifications by k.l. hiebert and r.j. hanson
c     revised 811130-1000
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890831  modified array declarations.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   910403  updated author and description sections.  (wrb)
c***end prologue  pinitm
      real sx(lmx),zero,one
      dimension ix(*)
      save zero, one
      data zero,one /0.e0,1.e0/
c***first executable statement  pinitm
      iopt=1
c
c     check for input errors.
c
      if (.not.(m.le.0 .or. n.le.0)) go to 20002
      nerr=55
      call lpxermsg ('slatec', 'pinitm',
     +   'matrix dimension m or n .le. 0.', nerr, iopt)
c
c     verify if value of lmx is large enough.
c
20002 if (.not.(lmx.lt.n+7)) go to 20005
      nerr=55
      call lpxermsg ('slatec', 'pinitm',
     +   'the value of lmx is too small.', nerr, iopt)
c
c     initialize data structure independent values.
c
20005 sx(1)=zero
      sx(2)=zero
      sx(3)=ipagef
      ix(1)=lmx
      ix(2)=m
      ix(3)=n
      ix(4)=0
      sx(lmx-1)=zero
      sx(lmx)=-one
      ix(lmx-1)=-1
      lp4=n+4
c
c     initialize data structure dependent values.
c
      i=4
      n20008=lp4
      go to 20009
20008 i=i+1
20009 if ((n20008-i).lt.0) go to 20010
      sx(i)=zero
      go to 20008
20010 i=5
      n20012=lp4
      go to 20013
20012 i=i+1
20013 if ((n20012-i).lt.0) go to 20014
      ix(i)=lp4
      go to 20012
20014 sx(n+5)=zero
      ix(n+5)=0
      ix(lmx)=0
c
c     initialization complete.
c
      return
      end subroutine lppinitm
      subroutine lppnnzrs (i, xval, iplace, sx, ix, ircx)
c***begin prologue  pnnzrs
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (pnnzrs-s, dpnnzr-d)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     pnnzrs limits the type of storage to a sequential scheme.
c     sparse matrix non zero retrieval subroutine.
c
c     subroutine lppnnzrs() gets the next nonzero value in row or column
c     +/- ircx with an index greater than the value of i.
c
c             i absolute value of this subscript is to be exceeded
c               in the search for the next nonzero value. a negative
c               or zero value of i causes the search to start at
c               the beginning of the vector.  a positive value
c               of i causes the search to continue from the last place
c               accessed. on output, the argument i
c               contains the value of the subscript found.  an output
c               value of i equal to zero indicates that all components
c               with an index greater than the input value of i are
c               zero.
c          xval value of the nonzero element found.  on output,
c               xval=0. whenever i=0.
c     iplace pointer information which is maintained by the package.
c   sx(*),ix(*) the work arrays which are used to store the sparse
c               matrix.  these array contents are automatically
c               maintained by the package for the user.
c          ircx points to the vector of the matrix being scanned.  a
c               negative value of ircx indicates that row -ircx is to be
c               scanned.  a positive value of ircx indicates that
c               column ircx is to be scanned.  a zero value of ircx is
c               an error.
c
c     this subroutine lpis a modification of the subroutine lplnnzrs,
c     sandia labs. rept. sand78-0785.
c     modifications by k.l. hiebert and r.j. hanson
c     revised 811130-1000
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  lpiploc, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   910403  updated author and description sections.  (wrb)
c***end prologue  pnnzrs
      dimension ix(*)
      real xval,sx(*),zero
      save zero
      data zero /0.e0/
c***first executable statement  pnnzrs
      iopt=1
c
c     check validity of row/col. index.
c
      if (.not.(ircx .eq.0)) go to 20002
      nerr=55
      call lpxermsg ('slatec', 'pnnzrs', 'ircx=0.', nerr, iopt)
c
c     lmx is the length of the in-memory storage area.
c
20002 lmx = ix(1)
      if (.not.(ircx.lt.0)) go to 20005
c
c     check subscripts of the row. the row number must be .le. m and
c     the index must be .le. n.
c
      if (.not.(ix(2).lt.-ircx .or. ix(3).lt.abs(i))) go to 20008
      nerr=55
      call lpxermsg ('slatec', 'pnnzrs',
     +   'subscripts for array element to be accessed were out of ' //
     +   'bounds.', nerr, iopt)
20008 l=ix(3)
      go to 20006
c
c     check subscripts of the column. the col. number must be .le. n and
c     the index must be .le. m.
c
20005 if (.not.(ircx.gt.ix(3) .or. abs(i).gt.ix(2))) go to 20011
      nerr=55
      call lpxermsg ('slatec', 'pnnzrs',
     +   'subscripts for array element to be accessed were out of ' //
     +   'bounds.', nerr, iopt)
20011 l=ix(2)
c
c     here l is the largest possible subscript within the vector.
c
20006 j=abs(ircx)
      ll=ix(3)+4
      lpg = lmx - ll
      if (.not.(ircx.gt.0)) go to 20014
c
c     searching for the next nonzero in a column.
c
c     initialize starting locations..
      if (.not.(i.le.0)) go to 20017
      if (.not.(j.eq.1)) go to 20020
      iplace=ll+1
      go to 20021
20020 iplace=ix(j+3)+1
20021 continue
c
c     the case i.le.0 signals that the scan for the entry
c     is to begin at the start of the vector.
c
20017 i = abs(i)
      if (.not.(j.eq.1)) go to 20023
      istart = ll+1
      go to 20024
20023 istart=ix(j+3)+1
20024 iend = ix(j+4)
c
c     validate iplace. set to start of vector if out of range.
c
      if (.not.(istart.gt.iplace .or. iplace.gt.iend)) go to 20026
      if (.not.(j.eq.1)) go to 20029
      iplace=ll+1
      go to 20030
20029 iplace=ix(j+3)+1
20030 continue
c
c     scan through several pages, if necessary, to find matrix entry.
c
20026 ipl = lpiploc(iplace,sx,ix)
c
c     fix up iplace and ipl if they point to paging data.
c     this is necessary because there is control information at the
c     end of each page.
c
      idiff = lmx - ipl
      if (.not.(idiff.le.1.and.ix(lmx-1).gt.0)) go to 20032
c
c     update the relative address in a new page.
c
      iplace = iplace + idiff + 1
      ipl = lpiploc(iplace,sx,ix)
20032 np = abs(ix(lmx-1))
      go to 20036
20035 if (ilast.eq.iend) go to 20037
20036 ilast = min(iend,np*lpg+ll-2)
c
c     the virtual end of the data for this page is ilast.
c
      il = lpiploc(ilast,sx,ix)
      il = min(il,lmx-2)
c
c     the relative end of data for this page is il.
c     search for a nonzero value with an index .gt. i on the present
c     page.
c
20038 if (.not.(.not.(ipl.ge.il.or.(ix(ipl).gt.i.and.sx(ipl).ne.zero))))
     * go to 20039
      ipl=ipl+1
      go to 20038
c
c     test if we have found the next nonzero.
c
20039 if (.not.(ix(ipl).gt.i .and. sx(ipl).ne.zero .and. ipl.le.il)) go
     *to 20040
      i = ix(ipl)
      xval = sx(ipl)
      iplace = (np-1)*lpg + ipl
      return
c
c     update to scan the next page.
20040 ipl = ll + 1
      np = np + 1
      go to 20035
c
c     no data was found. end of vector encountered.
c
20037 i = 0
      xval = zero
      il = il + 1
      if(il.eq.lmx-1) il = il + 2
c
c     if a new item would be inserted, iplace points to the place
c     to put it.
c
      iplace = (np-1)*lpg + il
      return
c
c     search a row for the next nonzero.
c     find element j=abs(ircx) in rows abs(i)+1,...,l.
c
20014 i=abs(i)
c
c     check for end of vector.
c
      if (.not.(i.eq.l)) go to 20043
      i=0
      xval=zero
      return
20043 i1 = i+1
      ii=i1
      n20046=l
      go to 20047
20046 ii=ii+1
20047 if ((n20046-ii).lt.0) go to 20048
c
c     initialize ipploc for orthogonal scan.
c     look for j as a subscript in rows ii, ii=i+1,...,l.
c
      if (.not.(ii.eq.1)) go to 20050
      ipploc = ll + 1
      go to 20051
20050 ipploc = ix(ii+3) + 1
20051 iend = ix(ii+4)
c
c     scan through several pages, if necessary, to find matrix entry.
c
      ipl = lpiploc(ipploc,sx,ix)
c
c     fix up ipploc and ipl to point to matrix data.
c
      idiff = lmx - ipl
      if (.not.(idiff.le.1.and.ix(lmx-1).gt.0)) go to 20053
      ipploc = ipploc + idiff + 1
      ipl = lpiploc(ipploc,sx,ix)
20053 np = abs(ix(lmx-1))
      go to 20057
20056 if (ilast.eq.iend) go to 20058
20057 ilast = min(iend,np*lpg+ll-2)
      il = lpiploc(ilast,sx,ix)
      il = min(il,lmx-2)
20059 if (.not.(.not.(ipl.ge.il .or. ix(ipl).ge.j))) go to 20060
      ipl=ipl+1
      go to 20059
c
c     test if we have found the next nonzero.
c
20060 if (.not.(ix(ipl).eq.j .and. sx(ipl).ne.zero .and. ipl.le.il)) go
     *to 20061
      i = ii
      xval = sx(ipl)
      return
20061 if(ix(ipl).ge.j) ilast = iend
      ipl = ll + 1
      np = np + 1
      go to 20056
20058 go to 20046
c
c     orthogonal scan failed. the value j was not a subscript
c     in any row.
c
20048 i=0
      xval=zero
      return
      end subroutine lppnnzrs
      subroutine lpprwpge (key, ipage, lpg, sx, ix)
c***begin prologue  prwpge
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (prwpge-s, dprwpg-d)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     prwpge limits the type of storage to a sequential scheme.
c     virtual memory page read/write subroutine.
c
c     depending on the value of key, subroutine lpprwpge() performs a page
c     read or write of page ipage. the page has length lpg.
c
c     key       is a flag indicating whether a page read or write is
c               to be performed.
c               if key = 1 data is read.
c               if key = 2 data is written.
c     ipage     is the page number of the matrix to be accessed.
c     lpg       is the length of the page of the matrix to be accessed.
c   sx(*),ix(*) is the matrix to be accessed.
c
c     this subroutine lpis a modification of the subroutine lplrwpge,
c     sandia labs. rept. sand78-0785.
c     modifications by k.l. hiebert and r.j. hanson
c     revised 811130-1000
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  prwvir, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   900510  fixed error messages and replaced gotos with
c           if-then-else.  (rwc)
c   910403  updated author and description sections.  (wrb)
c***end prologue  prwpge
      real sx(*)
      dimension ix(*)
c***first executable statement  prwpge
c
c     check if ipage is in range.
c
      if (ipage.lt.1) then
         call lpxermsg ('slatec', 'prwpge',
     +      'the value of ipage (page number) was not in the range' //
     +      '1.le.ipage.le.maxpge.', 55, 1)
      endif
c
c     check if lpg is positive.
c
      if (lpg.le.0) then
         call lpxermsg ('slatec', 'prwpge',
     +      'the value of lpg (page length) was nonpositive.', 55, 1)
      endif
c
c     decide if we are reading or writing.
c
      if (key.eq.1) then
c
c        code to do a page read.
c
         call lpprwvir(key,ipage,lpg,sx,ix)
      else if (key.eq.2) then
c
c        code to do a page write.
c
         call lpprwvir(key,ipage,lpg,sx,ix)
      else
         call lpxermsg ('slatec', 'prwpge',
     +      'the value of key (read-write flag) was not 1 or 2.', 55, 1)
      endif
      return
      end subroutine lpprwpge
      subroutine lpprwvir (key, ipage, lpg, sx, ix)
c***begin prologue  prwvir
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (prwvir-s, dprwvr-d)
c***author  hanson, r. j., (snla)
c           wisniewski, j. a., (snla)
c***description
c
c     prwvir limits the type of storage to a sequential sparse matrix
c     storage scheme.  the page storage is on random access disk.
c     prwvir is part of the sparse lp package, splp.
c
c     key       is a flag which indicates whether a read or write
c               operation is to be performed. a value of key=1 indicates
c               a read. a value of key=2 indicates a write.
c     ipage     is the page of matrix mn we are accessing.
c     lpg       is the length of the page.
c   sx(*),ix(*) is the matrix data.
c
c     this subroutine lpis a modification of the subroutine lplrwvir,
c     sandia labs. rept. sand78-0785.
c     modifications by k.l. hiebert and r.j. hanson
c
c***see also  splp
c***routines called  sopenm, sreadp, swritp
c***revision history  (yymmdd)
c   811215  date written
c   891009  removed unreferenced variables.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c   910403  updated author and description sections.  (wrb)
c***end prologue  prwvir
      dimension ix(*)
      real sx(*),zero,one
      logical first
      save zero, one
      data zero,one/0.e0,1.e0/
c***first executable statement  prwvir
c
c     compute starting address of page.
c
      ipagef=sx(3)
      istart = ix(3) + 5
c
c     open random access file number ipagef, if first page write.
c
      first=sx(4).eq.zero
      if (.not.(first)) go to 20002
      call lpsopenm(ipagef,lpg)
      sx(4)=one
c
c     perform either a read or a write.
c
20002 iaddr = 2*ipage - 1
      if (.not.(key.eq.1)) go to 20005
      call lpsreadp(ipagef,ix(istart),sx(istart),lpg,iaddr)
      go to 20006
20005 if (.not.(key.eq.2)) go to 10001
      call lpswritp(ipagef,ix(istart),sx(istart),lpg,iaddr)
10001 continue
20006 return
      end
      real function lpr1mach (i)
c***begin prologue  lpr1mach
c***purpose  return floating point machine dependent constants.
c***library   slatec
c***category  r1
c***type      single precision (lpr1mach-s, d1mach-d)
c***keywords  machine constants
c***author  fox, p. a., (bell labs)
c           hall, a. d., (bell labs)
c           schryer, n. l., (bell labs)
c***description
c
c   lpr1mach can be used to obtain machine-dependent parameters for the
c   local machine environment.  it is a function subprogram with one
c   (input) argument, and can be referenced as follows:
c
c        a = lpr1mach(i)
c
c   where i=1,...,5.  the (output) value of a above is determined by
c   the (input) value of i.  the results for various values of i are
c   discussed below.
c
c   lpr1mach(1) = b**(emin-1), the smallest positive magnitude.
c   lpr1mach(2) = b**emax*(1 - b**(-t)), the largest magnitude.
c   lpr1mach(3) = b**(-t), the smallest relative spacing.
c   lpr1mach(4) = b**(1-t), the largest relative spacing.
c   lpr1mach(5) = log10(b)
c
c   assume single precision numbers are represented in the t-digit,
c   base-b form
c
c              sign (b**e)*( (x(1)/b) + ... + (x(t)/b**t) )
c
c   where 0 .le. x(i) .lt. b for i=1,...,t, 0 .lt. x(1), and
c   emin .le. e .le. emax.
c
c   the values of b, t, emin and emax are provided in lpi1mach as
c   follows:
c   lpi1mach(10) = b, the base.
c   lpi1mach(11) = t, the number of base-b digits.
c   lpi1mach(12) = emin, the smallest exponent e.
c   lpi1mach(13) = emax, the largest exponent e.
c
c   to alter this function for a particular environment, the desired
c   set of data statements should be activated by removing the c from
c   column 1.  also, the values of lpr1mach(1) - lpr1mach(4) should be
c   checked for consistency with the local operating system.
c
c***references  p. a. fox, a. d. hall and n. l. schryer, framework for
c                 a portable library, acm transactions on mathematical
c                 software 4, 2 (june 1978), pp. 177-188.
c***routines called  xermsg
c***revision history  (yymmdd)
c   790101  date written
c   890213  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900618  added dec risc constants.  (wrb)
c   900723  added ibm rs 6000 constants.  (wrb)
c   910710  added hp 730 constants.  (smr)
c   911114  added convex ieee constants.  (wrb)
c   920121  added sun -r8 compiler option constants.  (wrb)
c   920229  added touchstone delta i860 constants.  (wrb)
c   920501  reformatted the references section.  (wrb)
c   920625  added convex -p8 and -pd8 compiler option constants.
c           (bks, wrb)
c   930201  added dec alpha and sgi constants.  (rwc and wrb)
c***end prologue  lpr1mach
c
      integer small(2)
      integer large(2)
      integer right(2)
      integer diver(2)
      integer log10(2)
c
      real rmach(5)
      save rmach
c
      equivalence (rmach(1),small(1))
      equivalence (rmach(2),large(1))
      equivalence (rmach(3),right(1))
      equivalence (rmach(4),diver(1))
      equivalence (rmach(5),log10(1))
c
c     machine constants for the amiga
c     absoft fortran compiler using the 68020/68881 compiler option
c
c     data small(1) / z'00800000' /
c     data large(1) / z'7f7fffff' /
c     data right(1) / z'33800000' /
c     data diver(1) / z'34000000' /
c     data log10(1) / z'3e9a209b' /
c
c     machine constants for the amiga
c     absoft fortran compiler using software floating point
c
c     data small(1) / z'00800000' /
c     data large(1) / z'7effffff' /
c     data right(1) / z'33800000' /
c     data diver(1) / z'34000000' /
c     data log10(1) / z'3e9a209b' /
c
c     machine constants for the apollo
c
c     data small(1) / 16#00800000 /
c     data large(1) / 16#7fffffff /
c     data right(1) / 16#33800000 /
c     data diver(1) / 16#34000000 /
c     data log10(1) / 16#3e9a209b /
c
c     machine constants for the burroughs 1700 system
c
c     data rmach(1) / z400800000 /
c     data rmach(2) / z5ffffffff /
c     data rmach(3) / z4e9800000 /
c     data rmach(4) / z4ea800000 /
c     data rmach(5) / z500e730e8 /
c
c     machine constants for the burroughs 5700/6700/7700 systems
c
c     data rmach(1) / o1771000000000000 /
c     data rmach(2) / o0777777777777777 /
c     data rmach(3) / o1311000000000000 /
c     data rmach(4) / o1301000000000000 /
c     data rmach(5) / o1157163034761675 /
c
c     machine constants for the cdc 170/180 series using nos/ve
c
c     data rmach(1) / z"3001800000000000" /
c     data rmach(2) / z"4ffefffffffffffe" /
c     data rmach(3) / z"3fd2800000000000" /
c     data rmach(4) / z"3fd3800000000000" /
c     data rmach(5) / z"3fff9a209a84fbcf" /
c
c     machine constants for the cdc 6000/7000 series
c
c     data rmach(1) / 00564000000000000000b /
c     data rmach(2) / 37767777777777777776b /
c     data rmach(3) / 16414000000000000000b /
c     data rmach(4) / 16424000000000000000b /
c     data rmach(5) / 17164642023241175720b /
c
c     machine constants for the celerity c1260
c
c     data small(1) / z'00800000' /
c     data large(1) / z'7f7fffff' /
c     data right(1) / z'33800000' /
c     data diver(1) / z'34000000' /
c     data log10(1) / z'3e9a209b' /
c
c     machine constants for the convex
c     using the -fn compiler option
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7fffffff' /
c     data rmach(3) / z'34800000' /
c     data rmach(4) / z'35000000' /
c     data rmach(5) / z'3f9a209b' /
c
c     machine constants for the convex
c     using the -fi compiler option
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the convex
c     using the -p8 or -pd8 compiler option
c
c     data rmach(1) / z'0010000000000000' /
c     data rmach(2) / z'7fffffffffffffff' /
c     data rmach(3) / z'3cc0000000000000' /
c     data rmach(4) / z'3cd0000000000000' /
c     data rmach(5) / z'3ff34413509f79ff' /
c
c     machine constants for the cray
c
c     data rmach(1) / 200034000000000000000b /
c     data rmach(2) / 577767777777777777776b /
c     data rmach(3) / 377224000000000000000b /
c     data rmach(4) / 377234000000000000000b /
c     data rmach(5) / 377774642023241175720b /
c
c     machine constants for the data general eclipse s/200
c     note - it may be appropriate to include the following card -
c     static rmach(5)
c
c     data small /    20k,       0 /
c     data large / 77777k, 177777k /
c     data right / 35420k,       0 /
c     data diver / 36020k,       0 /
c     data log10 / 40423k,  42023k /
c
c     machine constants for the dec alpha
c     using g_float
c
c     data rmach(1) / '00000080'x /
c     data rmach(2) / 'ffff7fff'x /
c     data rmach(3) / '00003480'x /
c     data rmach(4) / '00003500'x /
c     data rmach(5) / '209b3f9a'x /
c
c     machine constants for the dec alpha
c     using ieee_float
c
c     data rmach(1) / '00800000'x /
c     data rmach(2) / '7f7fffff'x /
c     data rmach(3) / '33800000'x /
c     data rmach(4) / '34000000'x /
c     data rmach(5) / '3e9a209b'x /
c
c     machine constants for the dec risc
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the dec vax
c     (expressed in integer and hexadecimal)
c     the hex format below may not be suitable for unix systems
c     the integer format should be ok for unix systems
c
c     data small(1) /       128 /
c     data large(1) /    -32769 /
c     data right(1) /     13440 /
c     data diver(1) /     13568 /
c     data log10(1) / 547045274 /
c
c     data small(1) / z00000080 /
c     data large(1) / zffff7fff /
c     data right(1) / z00003480 /
c     data diver(1) / z00003500 /
c     data log10(1) / z209b3f9a /
c
c     machine constants for the elxsi 6400
c     (assuming real*4 is the default real)
c
c     data small(1) / '00800000'x /
c     data large(1) / '7f7fffff'x /
c     data right(1) / '33800000'x /
c     data diver(1) / '34000000'x /
c     data log10(1) / '3e9a209b'x /
c
c     machine constants for the harris 220
c
c     data small(1), small(2) / '20000000, '00000201 /
c     data large(1), large(2) / '37777777, '00000177 /
c     data right(1), right(2) / '20000000, '00000352 /
c     data diver(1), diver(2) / '20000000, '00000353 /
c     data log10(1), log10(2) / '23210115, '00000377 /
c
c     machine constants for the honeywell 600/6000 series
c
c     data rmach(1) / o402400000000 /
c     data rmach(2) / o376777777777 /
c     data rmach(3) / o714400000000 /
c     data rmach(4) / o716400000000 /
c     data rmach(5) / o776464202324 /
c
c     machine constants for the hp 730
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the hp 2100
c     3 word double precision with ftn4
c
c     data small(1), small(2) / 40000b,       1 /
c     data large(1), large(2) / 77777b, 177776b /
c     data right(1), right(2) / 40000b,    325b /
c     data diver(1), diver(2) / 40000b,    327b /
c     data log10(1), log10(2) / 46420b,  46777b /
c
c     machine constants for the hp 2100
c     4 word double precision with ftn4
c
c     data small(1), small(2) / 40000b,       1 /
c     data large(1), large(2) / 77777b, 177776b /
c     data right(1), right(2) / 40000b,    325b /
c     data diver(1), diver(2) / 40000b,    327b /
c     data log10(1), log10(2) / 46420b,  46777b /
c
c     machine constants for the hp 9000
c
c     data small(1) / 00004000000b /
c     data large(1) / 17677777777b /
c     data right(1) / 06340000000b /
c     data diver(1) / 06400000000b /
c     data log10(1) / 07646420233b /
c
c     machine constants for the ibm 360/370 series,
c     the xerox sigma 5/7/9, the sel systems 85/86  and
c     the perkin elmer (interdata) 7/32.
c
c     data rmach(1) / z00100000 /
c     data rmach(2) / z7fffffff /
c     data rmach(3) / z3b100000 /
c     data rmach(4) / z3c100000 /
c     data rmach(5) / z41134413 /
c
c     machine constants for the ibm pc
c
c      data small(1) / 1.18e-38      /
c      data large(1) / 3.40e+38      /
c      data right(1) / 0.595e-07     /
c      data diver(1) / 1.19e-07      /
c      data log10(1) / 0.30102999566 /
      data rmach(1) / 1.18e-38      /
      data rmach(2) / 3.40e+38      /
      data rmach(3) / 0.595e-07     /
      data rmach(4) / 1.19e-07      /
      data rmach(5) / 0.30102999566 /
c
c     machine constants for the ibm rs 6000
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the intel i860
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the pdp-10 (ka or ki processor)
c
c     data rmach(1) / "000400000000 /
c     data rmach(2) / "377777777777 /
c     data rmach(3) / "146400000000 /
c     data rmach(4) / "147400000000 /
c     data rmach(5) / "177464202324 /
c
c     machine constants for pdp-11 fortran supporting
c     32-bit integers (expressed in integer and octal).
c
c     data small(1) /    8388608 /
c     data large(1) / 2147483647 /
c     data right(1) /  880803840 /
c     data diver(1) /  889192448 /
c     data log10(1) / 1067065499 /
c
c     data rmach(1) / o00040000000 /
c     data rmach(2) / o17777777777 /
c     data rmach(3) / o06440000000 /
c     data rmach(4) / o06500000000 /
c     data rmach(5) / o07746420233 /
c
c     machine constants for pdp-11 fortran supporting
c     16-bit integers  (expressed in integer and octal).
c
c     data small(1), small(2) /   128,     0 /
c     data large(1), large(2) / 32767,    -1 /
c     data right(1), right(2) / 13440,     0 /
c     data diver(1), diver(2) / 13568,     0 /
c     data log10(1), log10(2) / 16282,  8347 /
c
c     data small(1), small(2) / o000200, o000000 /
c     data large(1), large(2) / o077777, o177777 /
c     data right(1), right(2) / o032200, o000000 /
c     data diver(1), diver(2) / o032400, o000000 /
c     data log10(1), log10(2) / o037632, o020233 /
c
c     machine constants for the silicon graphics
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the sun
c
c     data rmach(1) / z'00800000' /
c     data rmach(2) / z'7f7fffff' /
c     data rmach(3) / z'33800000' /
c     data rmach(4) / z'34000000' /
c     data rmach(5) / z'3e9a209b' /
c
c     machine constants for the sun
c     using the -r8 compiler option
c
c     data rmach(1) / z'0010000000000000' /
c     data rmach(2) / z'7fefffffffffffff' /
c     data rmach(3) / z'3ca0000000000000' /
c     data rmach(4) / z'3cb0000000000000' /
c     data rmach(5) / z'3fd34413509f79ff' /
c
c     machine constants for the univac 1100 series
c
c     data rmach(1) / o000400000000 /
c     data rmach(2) / o377777777777 /
c     data rmach(3) / o146400000000 /
c     data rmach(4) / o147400000000 /
c     data rmach(5) / o177464202324 /
c
c     machine constants for the z80 microprocessor
c
c     data small(1), small(2) /     0,    256/
c     data large(1), large(2) /    -1,   -129/
c     data right(1), right(2) /     0,  26880/
c     data diver(1), diver(2) /     0,  27136/
c     data log10(1), log10(2) /  8347,  32538/
c
c***first executable statement  lpr1mach
      if (i .lt. 1 .or. i .gt. 5) call lpxermsg ('slatec', 'lpr1mach',
     +   'i out of bounds', 1, 2)
c
      lpr1mach = rmach(i)
      return
c
      end
      real function lpsasum (n, sx, incx)
c***begin prologue  lpsasum
c***purpose  compute the sum of the magnitudes of the elements of a
c            vector.
c***library   slatec (blas)
c***category  d1a3a
c***type      single precision (lpsasum-s, dasum-d, scasum-c)
c***keywords  blas, linear algebra, sum of magnitudes of a vector
c***author  lawson, c. l., (jpl)
c           hanson, r. j., (snla)
c           kincaid, d. r., (u. of texas)
c           krogh, f. t., (jpl)
c***description
c
c                b l a s  subprogram
c    description of parameters
c
c     --input--
c        n  number of elements in input vector(s)
c       sx  single precision vector with n elements
c     incx  storage spacing between elements of sx
c
c     --output--
c    lpsasum  single precision result (zero if n .le. 0)
c
c     returns sum of magnitudes of single precision sx.
c     lpsasum = sum from 0 to n-1 of abs(sx(ix+i*incx)),
c     where ix = 1 if incx .ge. 0, else ix = 1+(1-n)*incx.
c
c***references  c. l. lawson, r. j. hanson, d. r. kincaid and f. t.
c                 krogh, basic linear algebra subprograms for fortran
c                 usage, algorithm no. 539, transactions on mathematical
c                 software 5, 3 (september 1979), pp. 308-323.
c***routines called  (none)
c***revision history  (yymmdd)
c   791001  date written
c   890831  modified array declarations.  (wrb)
c   890831  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900821  modified to correct problem with a negative increment.
c           (wrb)
c   920501  reformatted the references section.  (wrb)
c***end prologue  lpsasum
      real sx(*)
      integer i, incx, ix, m, mp1, n
c***first executable statement  lpsasum
      lpsasum = 0.0e0
      if (n .le. 0) return
c
      if (incx .eq. 1) goto 20
c
c     code for increment not equal to 1.
c
      ix = 1
      if (incx .lt. 0) ix = (-n+1)*incx + 1
      do 10 i = 1,n
        lpsasum = lpsasum + abs(sx(ix))
        ix = ix + incx
   10 continue
      return
c
c     code for increment equal to 1.
c
c     clean-up loop so remaining vector length is a multiple of 6.
c
   20 m = mod(n,6)
      if (m .eq. 0) goto 40
      do 30 i = 1,m
        lpsasum = lpsasum + abs(sx(i))
   30 continue
      if (n .lt. 6) return
   40 mp1 = m + 1
      do 50 i = mp1,n,6
        lpsasum = lpsasum + abs(sx(i)) + abs(sx(i+1)) + abs(sx(i+2)) +
     1          abs(sx(i+3)) + abs(sx(i+4)) + abs(sx(i+5))
   50 continue
      return
      end
      subroutine lpsclosm (ipage)
c***begin prologue  sclosm
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      all (sclosm-a)
c***author  (unknown)
c***description
c
c     1. unload, release, or close unit number ipagef.
c
c***see also  splp
c***routines called  xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900402  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  sclosm
      character*8 xern1
c
c***first executable statement  sclosm
      ipagef=ipage
      close(unit=ipagef,iostat=ios,err=100,status='keep')
      return
c
  100 write (xern1, '(i8)') ios
      call lpxermsg ('slatec', 'sclosm',
     *   'in splp, close has error flag = ' // xern1, 100, 1)
      return
      end subroutine lpsclosm
      subroutine lpscopy (n, sx, incx, sy, incy)
c***begin prologue  scopy
c***purpose  copy a vector.
c***library   slatec (blas)
c***category  d1a5
c***type      single precision (scopy-s, dcopy-d, ccopy-c, icopy-i)
c***keywords  blas, copy, linear algebra, vector
c***author  lawson, c. l., (jpl)
c           hanson, r. j., (snla)
c           kincaid, d. r., (u. of texas)
c           krogh, f. t., (jpl)
c***description
c
c                b l a s  subprogram
c    description of parameters
c
c     --input--
c        n  number of elements in input vector(s)
c       sx  single precision vector with n elements
c     incx  storage spacing between elements of sx
c       sy  single precision vector with n elements
c     incy  storage spacing between elements of sy
c
c     --output--
c       sy  copy of vector sx (unchanged if n .le. 0)
c
c     copy single precision sx to single precision sy.
c     for i = 0 to n-1, copy  sx(lx+i*incx) to sy(ly+i*incy),
c     where lx = 1 if incx .ge. 0, else lx = 1+(1-n)*incx, and ly is
c     defined in a similar way using incy.
c
c***references  c. l. lawson, r. j. hanson, d. r. kincaid and f. t.
c                 krogh, basic linear algebra subprograms for fortran
c                 usage, algorithm no. 539, transactions on mathematical
c                 software 5, 3 (september 1979), pp. 308-323.
c***routines called  (none)
c***revision history  (yymmdd)
c   791001  date written
c   890831  modified array declarations.  (wrb)
c   890831  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   920310  corrected definition of lx in description.  (wrb)
c   920501  reformatted the references section.  (wrb)
c***end prologue  scopy
      real sx(*), sy(*)
c***first executable statement  scopy
      if (n .le. 0) return
      if (incx .eq. incy) if (incx-1) 5,20,60
c
c     code for unequal or nonpositive increments.
c
    5 ix = 1
      iy = 1
      if (incx .lt. 0) ix = (-n+1)*incx + 1
      if (incy .lt. 0) iy = (-n+1)*incy + 1
      do 10 i = 1,n
        sy(iy) = sx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
c
c     code for both increments equal to 1.
c
c     clean-up loop so remaining vector length is a multiple of 7.
c
   20 m = mod(n,7)
      if (m .eq. 0) go to 40
      do 30 i = 1,m
        sy(i) = sx(i)
   30 continue
      if (n .lt. 7) return
   40 mp1 = m + 1
      do 50 i = mp1,n,7
        sy(i) = sx(i)
        sy(i+1) = sx(i+1)
        sy(i+2) = sx(i+2)
        sy(i+3) = sx(i+3)
        sy(i+4) = sx(i+4)
        sy(i+5) = sx(i+5)
        sy(i+6) = sx(i+6)
   50 continue
      return
c
c     code for equal, positive, non-unit increments.
c
   60 ns = n*incx
      do 70 i = 1,ns,incx
        sy(i) = sx(i)
   70 continue
      return
      end subroutine lpscopy
      real function lpsdot (n, sx, incx, sy, incy)
c***begin prologue  lpsdot
c***purpose  compute the inner product of two vectors.
c***library   slatec (blas)
c***category  d1a4
c***type      single precision (lpsdot-s, ddot-d, cdotu-c)
c***keywords  blas, inner product, linear algebra, vector
c***author  lawson, c. l., (jpl)
c           hanson, r. j., (snla)
c           kincaid, d. r., (u. of texas)
c           krogh, f. t., (jpl)
c***description
c
c                b l a s  subprogram
c    description of parameters
c
c     --input--
c        n  number of elements in input vector(s)
c       sx  single precision vector with n elements
c     incx  storage spacing between elements of sx
c       sy  single precision vector with n elements
c     incy  storage spacing between elements of sy
c
c     --output--
c     lpsdot  single precision dot product (zero if n .le. 0)
c
c     returns the dot product of single precision sx and sy.
c     lpsdot = sum for i = 0 to n-1 of  sx(lx+i*incx) * sy(ly+i*incy),
c     where lx = 1 if incx .ge. 0, else lx = 1+(1-n)*incx, and ly is
c     defined in a similar way using incy.
c
c***references  c. l. lawson, r. j. hanson, d. r. kincaid and f. t.
c                 krogh, basic linear algebra subprograms for fortran
c                 usage, algorithm no. 539, transactions on mathematical
c                 software 5, 3 (september 1979), pp. 308-323.
c***routines called  (none)
c***revision history  (yymmdd)
c   791001  date written
c   890831  modified array declarations.  (wrb)
c   890831  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   920310  corrected definition of lx in description.  (wrb)
c   920501  reformatted the references section.  (wrb)
c***end prologue  lpsdot
      real sx(*), sy(*)
c***first executable statement  lpsdot
      lpsdot = 0.0e0
      if (n .le. 0) return
      if (incx .eq. incy) if (incx-1) 5,20,60
c
c     code for unequal or nonpositive increments.
c
    5 ix = 1
      iy = 1
      if (incx .lt. 0) ix = (-n+1)*incx + 1
      if (incy .lt. 0) iy = (-n+1)*incy + 1
      do 10 i = 1,n
        lpsdot = lpsdot + sx(ix)*sy(iy)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
c
c     code for both increments equal to 1.
c
c     clean-up loop so remaining vector length is a multiple of 5.
c
   20 m = mod(n,5)
      if (m .eq. 0) go to 40
      do 30 i = 1,m
        lpsdot = lpsdot + sx(i)*sy(i)
   30 continue
      if (n .lt. 5) return
   40 mp1 = m + 1
      do 50 i = mp1,n,5
      lpsdot = lpsdot + sx(i)*sy(i) + sx(i+1)*sy(i+1) + sx(i+2)*sy(i+2)+
     1              sx(i+3)*sy(i+3) + sx(i+4)*sy(i+4)
   50 continue
      return
c
c     code for equal, positive, non-unit increments.
c
   60 ns = n*incx
      do 70 i = 1,ns,incx
        lpsdot = lpsdot + sx(i)*sy(i)
   70 continue

      end function lpsdot

      subroutine lpsopenm (ipage, lpage)

c***begin prologue  sopenm
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      all (sopenm-a)
c***author  (unknown)
c***description
c
c     1. open unit number ipagef as a random access file.
c
c     2. the record length is constant=lpg.
c
c***see also  splp
c***routines called  xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900402  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  sopenm
      character*8 xern1
c
c***first executable statement  sopenm

      ipagef=ipage
      lpg   =lpage
      open(unit=ipagef,iostat=ios,err=100,status='unknown',
     *     access='direct',form='unformatted',recl=lpg)
      return

 100  write (xern1, '(i8)') ios
      call lpxermsg ('slatec', 'sopenm',
     *   'in splp, open has error flag = ' // xern1, 100, 1)
      return
      end subroutine lpsopenm
      subroutine lpspincw (mrelas, nvars, lmx, lbm, npp, jstrt, ibasis,
     +   imat, ibrc, ipr, iwr, ind, ibb, costsc, gg, erdnrm, dulnrm,
     +   amat, basmat, csc, wr, ww, rz, rg, costs, colnrm, duals,
     +   stpedg)
c***begin prologue  spincw
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (spincw-s, dpincw-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/,
c     real (12 blanks)/double precision/,/scopy/dcopy/,/lpsdot/ddot/.
c
c     this subprogram is part of the splp( ) package.
c     it implements the procedure (initialize reduced costs and
c     steepest edge weights).
c
c***see also  splp
c***routines called  lpiploc, la05bs, prwpge, scopy, lpsdot
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  spincw
      integer ibasis(*),imat(*),ibrc(lbm,2),ipr(*),iwr(*),ind(*),ibb(*)
      real             amat(*),basmat(*),csc(*),wr(*),ww(*),rz(*),rg(*),
     * costs(*),colnrm(*),duals(*),costsc,erdnrm,dulnrm,gg,one,rzj,
     * scalr,zero,rcost,lpsdot
      logical stpedg,pagepl,trans
c***first executable statement  spincw
      lpg=lmx-(nvars+4)
      zero=0.
      one=1.
c
c     form reduced costs, rz(*), and steepest edge weights, rg(*).
      pagepl=.true.
      rz(1)=zero
      call lpscopy(nvars+mrelas,rz,0,rz,1)
      rg(1)=one
      call lpscopy(nvars+mrelas,rg,0,rg,1)
      nnegrc=0
      j=jstrt
20002 if (.not.(ibb(j).le.0)) go to 20004
      pagepl=.true.
      go to 20005
c
c     these are nonbasic independent variables. the cols. are in sparse
c     matrix format.
20004 if (.not.(j.le.nvars)) go to 20007
      rzj=costsc*costs(j)
      ww(1)=zero
      call lpscopy(mrelas,ww,0,ww,1)
      if (.not.(j.eq.1)) go to 20010
      ilow=nvars+5
      go to 20011
20010 ilow=imat(j+3)+1
20011 continue
      if (.not.(pagepl)) go to 20013
      il1=lpiploc(ilow,amat,imat)
      if (.not.(il1.ge.lmx-1)) go to 20016
      ilow=ilow+2
      il1=lpiploc(ilow,amat,imat)
20016 continue
      ipage=abs(imat(lmx-1))
      go to 20014
20013 il1=ihi+1
20014 continue
      ihi=imat(j+4)-(ilow-il1)
20019 iu1=min(lmx-2,ihi)
      if (.not.(il1.gt.iu1)) go to 20021
      go to 20020
20021 continue
      do 60 i=il1,iu1
      rzj=rzj-amat(i)*duals(imat(i))
      ww(imat(i))=amat(i)*csc(j)
60    continue
      if (.not.(ihi.le.lmx-2)) go to 20024
      go to 20020
20024 continue
      ipage=ipage+1
      key=1
      call lpprwpge(key,ipage,lpg,amat,imat)
      il1=nvars+5
      ihi=ihi-lpg
      go to 20019
20020 pagepl=ihi.eq.(lmx-2)
      rz(j)=rzj*csc(j)
      if (.not.(stpedg)) go to 20027
      trans=.false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      rg(j)=lpsdot(mrelas,ww,1,ww,1)+one
20027 continue
c
c     these are nonbasic dependent variables. the cols. are implicitly
c     defined.
      go to 20008
20007 pagepl=.true.
      ww(1)=zero
      call lpscopy(mrelas,ww,0,ww,1)
      scalr=-one
      if (ind(j).eq.2) scalr=one
      i=j-nvars
      rz(j)=-scalr*duals(i)
      ww(i)=scalr
      if (.not.(stpedg)) go to 20030
      trans=.false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      rg(j)=lpsdot(mrelas,ww,1,ww,1)+one
20030 continue
      continue
20008 continue
c
20005 rcost=rz(j)
      if (mod(ibb(j),2).eq.0) rcost=-rcost
      if (ind(j).eq.4) rcost=-abs(rcost)
      cnorm=one
      if (j.le.nvars) cnorm=colnrm(j)
      if (rcost+erdnrm*dulnrm*cnorm.lt.zero) nnegrc=nnegrc+1
      j=mod(j,mrelas+nvars)+1
      if (.not.(nnegrc.ge.npp .or. j.eq.jstrt)) go to 20033
      go to 20003
20033 go to 20002
20003 jstrt=j

      end subroutine lpspincw
      subroutine lpspinit (mrelas, nvars, costs, bl, bu, ind, primal,
     +   info, amat, csc, costsc, colnrm, xlamda, anorm, rhs, rhsnrm,
     +   ibasis, ibb, imat, lopt)
c
      include   'file.h'

      real,allocatable::apr(:,:)
c***begin prologue  spinit
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (spinit-s, dpinit-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/,/scopy/dcopy/
c     revised 810519-0900
c     revised yymmdd-hhmm
c
c     initialization subroutine lpfor splp(*) package.
c
c***see also  splp
c***routines called  pnnzrs, lpsasum, scopy
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  spinit
      real             aij,amat(*),anorm,bl(*),bu(*),cmax,
     * colnrm(*),costs(*),costsc,csc(*),csum,one,primal(*),
     * rhs(*),rhsnrm,scalr,testsc,xlamda,zero,lpsasum
      integer ibasis(*),ibb(*),imat(*),ind(*)
      logical contin,usrbas,colscp,cstscp,minprb,lopt(8)
c
      allocate(apr(mrelas,nvars))
      do i=1,nvars
      do j=1,mrelas
        apr(j,i)=0.0e0
      enddo
      enddo
c
c***first executable statement  spinit
      zero=0.
      one=1.
      contin=lopt(1)
      usrbas=lopt(2)
      colscp=lopt(5)
      cstscp=lopt(6)
      minprb=lopt(7)
c
c     scale data. normalize bounds. form column check sums.
      go to 30001
c
c     initialize active basis matrix.
20002 continue
      go to 30002
20003 return
c
c     procedure (scale data. normalize bounds. form column check sums)
c
c     do column scaling if not provided by the user.
c
30001 if (.not.(.not. colscp)) go to 20004
      j=1
      n20007=nvars
      go to 20008
20007 j=j+1
20008 if ((n20007-j).lt.0) go to 20009
      cmax=zero
      i=0
20011 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.eq.0)) go to 20013
      go to 20012
20013 continue
      apr(i,j)=aij
      cmax=max(cmax,abs(aij))
      go to 20011
20012 if (.not.(cmax.eq.zero)) go to 20016
      csc(j)=one
      go to 20017
20016 csc(j)=one/cmax
20017 continue
      go to 20007
20009 continue
c
      do i=1,mrelas
        write(not,'(1x,100i1)')(int(apr(i,j)),j=1,nvars)
      enddo

c     form check sums of columns. compute matrix norm of scaled matrix.
20004 anorm = zero
      j=1
      n20019=nvars
      go to 20020
20019 j=j+1
20020 if ((n20019-j).lt.0) go to 20021
      primal(j)=zero
      csum = zero
      i=0
20023 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.le.0)) go to 20025
      go to 20024
20025 continue
      primal(j)=primal(j)+aij
      csum = csum+abs(aij)
      go to 20023
20024 if (ind(j).eq.2) csc(j)=-csc(j)
      primal(j)=primal(j)*csc(j)
      colnrm(j)=abs(csc(j)*csum)
      anorm = max(anorm,colnrm(j))
      go to 20019
c
c     if the user has not provided cost vector scaling then scale it
c     using the max. norm of the transformed cost vector, if nonzero.
20021 testsc=zero
      j=1
      n20028=nvars
      go to 20029
20028 j=j+1
20029 if ((n20028-j).lt.0) go to 20030
      testsc=max(testsc,abs(csc(j)*costs(j)))
      go to 20028
20030 if (.not.(.not.cstscp)) go to 20032
      if (.not.(testsc.gt.zero)) go to 20035
      costsc=one/testsc
      go to 20036
20035 costsc=one
20036 continue
      continue
20032 xlamda=(costsc+costsc)*testsc
      if (xlamda.eq.zero) xlamda=one
c
c     if maximization problem, then change sign of costsc and lamda
c     =weight for penalty-feasibility method.
      if (.not.(.not.minprb)) go to 20038
      costsc=-costsc
20038 go to 20002
c:ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (initialize rhs(*),ibasis(*), and ibb(*))
c
c     initially set right-hand side vector to zero.
30002 call lpscopy(mrelas,zero,0,rhs,1)
c
c     translate rhs according to classification of independent variables
      j=1
      n20041=nvars
      go to 20042
20041 j=j+1
20042 if ((n20041-j).lt.0) go to 20043
      if (.not.(ind(j).eq.1)) go to 20045
      scalr=-bl(j)
      go to 20046
20045 if (.not.(ind(j).eq.2)) go to 10001
      scalr=-bu(j)
      go to 20046
10001 if (.not.(ind(j).eq.3)) go to 10002
      scalr=-bl(j)
      go to 20046
10002 if (.not.(ind(j).eq.4)) go to 10003
      scalr=zero
10003 continue
20046 continue
      if (.not.(scalr.ne.zero)) go to 20048
      i=0
20051 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.le.0)) go to 20053
      go to 20052
20053 continue
      rhs(i)=scalr*aij+rhs(i)
      go to 20051
20052 continue
20048 continue
      go to 20041
c
c     translate rhs according to classification of dependent variables.
20043 i=nvars+1
      n20056=nvars+mrelas
      go to 20057
20056 i=i+1
20057 if ((n20056-i).lt.0) go to 20058
      if (.not.(ind(i).eq.1)) go to 20060
      scalr=bl(i)
      go to 20061
20060 if (.not.(ind(i).eq.2)) go to 10004
      scalr=bu(i)
      go to 20061
10004 if (.not.(ind(i).eq.3)) go to 10005
      scalr=bl(i)
      go to 20061
10005 if (.not.(ind(i).eq.4)) go to 10006
      scalr=zero
10006 continue
20061 continue
      rhs(i-nvars)=rhs(i-nvars)+scalr
      go to 20056
20058 rhsnrm=lpsasum(mrelas,rhs,1)
c
c     if this is not a continuation or the user has not provided the
c     initial basis, then the initial basis is comprised of the
c     dependent variables.
      if (.not.(.not.(contin .or. usrbas))) go to 20063
      j=1
      n20066=mrelas
      go to 20067
20066 j=j+1
20067 if ((n20066-j).lt.0) go to 20068
      ibasis(j)=nvars+j
      go to 20066
20068 continue
c
c     define the array ibb(*)
20063 j=1
      n20070=nvars+mrelas
      go to 20071
20070 j=j+1
20071 if ((n20070-j).lt.0) go to 20072
      ibb(j)=1
      go to 20070
20072 j=1
      n20074=mrelas
      go to 20075
20074 j=j+1
20075 if ((n20074-j).lt.0) go to 20076
      ibb(ibasis(j))=-1
      go to 20074
c
c     define the rest of ibasis(*)
20076 ip=mrelas
      j=1
      n20078=nvars+mrelas
      go to 20079
20078 j=j+1
20079 if ((n20078-j).lt.0) go to 20080
      if (.not.(ibb(j).gt.0)) go to 20082
      ip=ip+1
      ibasis(ip)=j
20082 go to 20078
20080 go to 20003
      end subroutine lpspinit
      subroutine lpsplpce (mrelas, nvars, lmx, lbm, itlp, itbrc, ibasis,
     +   imat, ibrc, ipr, iwr, ind, ibb, erdnrm, eps, tune, gg, amat,
     +   basmat, csc, wr, ww, primal, erd, erp, singlr, redbas)
c***begin prologue  splpce
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpce-s, dplpce-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/,
c     /lpsasum/dasum/,/scopy/,dcopy/.
c
c     revised 811219-1630
c     revised yymmdd-hhmm
c
c     this subprogram is from the splp( ) package.  it calculates
c     the approximate error in the primal and dual systems.  it is
c     the main part of the procedure (compute error in dual and primal
c     systems).
c
c***see also  splp
c***routines called  lpiploc, la05bs, prwpge, lpsasum, scopy
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  splpce
      integer ibasis(*),imat(*),ibrc(lbm,2),ipr(*),iwr(*),ind(*),ibb(*)
      real             amat(*),basmat(*),csc(*),wr(*),ww(*),primal(*),
     * erd(*),erp(*),eps,erdnrm,factor,gg,one,zero,ten,tune,lpsasum
      logical singlr,redbas,trans,pagepl
c***first executable statement  splpce
      zero=0.e0
      one=1.e0
      ten=10.e0
      lpg=lmx-(nvars+4)
      singlr=.false.
      factor=0.01
c
c     copy colsums in ww(*), and solve transposed system.
      i=1
      n20002=mrelas
      go to 20003
20002 i=i+1
20003 if ((n20002-i).lt.0) go to 20004
      j=ibasis(i)
      if (.not.(j.le.nvars)) go to 20006
      ww(i) = primal(j)
      go to 20007
20006 if (.not.(ind(j).eq.2)) go to 20009
      ww(i)=one
      go to 20010
20009 ww(i)=-one
20010 continue
20007 continue
      go to 20002
c
c     perturb right-side in units of last bits to better reflect
c     errors in the check sum solns.
20004 i=1
      n20012=mrelas
      go to 20013
20012 i=i+1
20013 if ((n20012-i).lt.0) go to 20014
      ww(i)=ww(i)+ten*eps*ww(i)
      go to 20012
20014 trans = .true.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      i=1
      n20016=mrelas
      go to 20017
20016 i=i+1
20017 if ((n20016-i).lt.0) go to 20018
      erd(i)=max(abs(ww(i)-one),eps)*tune
c
c     system becomes singular when accuracy of solution is .gt. factor.
c     this value (factor) might need to be changed.
      singlr=singlr.or.(erd(i).ge.factor)
      go to 20016
20018 erdnrm=lpsasum(mrelas,erd,1)
c
c     recalculate row check sums every itbrc iterations or when
c     a redecomposition has occurred.
      if (.not.(mod(itlp,itbrc).eq.0 .or. redbas)) go to 20020
c
c     compute row sums, store in ww(*), solve primal system.
      ww(1)=zero
      call lpscopy(mrelas,ww,0,ww,1)
      pagepl=.true.
      j=1
      n20023=nvars
      go to 20024
20023 j=j+1
20024 if ((n20023-j).lt.0) go to 20025
      if (.not.(ibb(j).ge.zero)) go to 20027
c
c     the variable is non-basic.
      pagepl=.true.
      go to 20023
20027 if (.not.(j.eq.1)) go to 20030
      ilow=nvars+5
      go to 20031
20030 ilow=imat(j+3)+1
20031 if (.not.(pagepl)) go to 20033
      il1=lpiploc(ilow,amat,imat)
      if (.not.(il1.ge.lmx-1)) go to 20036
      ilow=ilow+2
      il1=lpiploc(ilow,amat,imat)
20036 continue
      ipage=abs(imat(lmx-1))
      go to 20034
20033 il1=ihi+1
20034 ihi=imat(j+4)-(ilow-il1)
20039 iu1=min(lmx-2,ihi)
      if (.not.(il1.gt.iu1)) go to 20041
      go to 20040
20041 continue
      do 20 i=il1,iu1
      ww(imat(i))=ww(imat(i))+amat(i)*csc(j)
20    continue
      if (.not.(ihi.le.lmx-2)) go to 20044
      go to 20040
20044 continue
      ipage=ipage+1
      key=1
      call lpprwpge(key,ipage,lpg,amat,imat)
      il1=nvars+5
      ihi=ihi-lpg
      go to 20039
20040 pagepl=ihi.eq.(lmx-2)
      go to 20023
20025 l=1
      n20047=mrelas
      go to 20048
20047 l=l+1
20048 if ((n20047-l).lt.0) go to 20049
      j=ibasis(l)
      if (.not.(j.gt.nvars)) go to 20051
      i=j-nvars
      if (.not.(ind(j).eq.2)) go to 20054
      ww(i)=ww(i)+one
      go to 20055
20054 ww(i)=ww(i)-one
20055 continue
20051 continue
      go to 20047
c
c     perturb right-side in units of last bit positions.
20049 i=1
      n20057=mrelas
      go to 20058
20057 i=i+1
20058 if ((n20057-i).lt.0) go to 20059
      ww(i)=ww(i)+ten*eps*ww(i)
      go to 20057
20059 trans = .false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      i=1
      n20061=mrelas
      go to 20062
20061 i=i+1
20062 if ((n20061-i).lt.0) go to 20063
      erp(i)=max(abs(ww(i)-one),eps)*tune
c
c     system becomes singular when accuracy of solution is .gt. factor.
c     this value (factor) might need to be changed.
      singlr=singlr.or.(erp(i).ge.factor)
      go to 20061
20063 continue
c
20020 return
      end subroutine lpsplpce
      subroutine lpsplpdm (mrelas, nvars, lmx, lbm, nredc, info, iopt,
     +   ibasis, imat, ibrc, ipr, iwr, ind, ibb, anorm, eps, uu, gg,
     +   amat, basmat, csc, wr, singlr, redbas)
c***begin prologue  splpdm
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpdm-s, dplpdm-d)
c***author  (unknown)
c***description
c
c     this subprogram is from the splp( ) package.  it performs the
c     task of defining the entries of the basis matrix and
c     decomposing it using the la05 package.
c     it is the main part of the procedure (decompose basis matrix).
c
c***see also  splp
c***routines called  la05as, pnnzrs, lpsasum, xermsg
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls, changed do-it-yourself
c           do loops to do loops.  (rwc)
c***end prologue  splpdm
      integer ibasis(*),imat(*),ibrc(lbm,2),ipr(*),iwr(*),ind(*),ibb(*)
      real             amat(*),basmat(*),csc(*),wr(*),anorm,eps,gg,
     * one,small,uu,zero,lpsasum
      logical singlr,redbas
      character*16 xern3
c
c     common block used by la05 () package..
      common /la05ds/ small,lp,lenl,lenu,ncp,lrow,lcol
c
c***first executable statement  splpdm
      zero = 0.e0
      one = 1.e0
c
c     define basis matrix by columns for sparse matrix equation solver.
c     the la05as() subprogram requires the nonzero entries of the matrix
c     together with the row and column indices.
c
      nzbm = 0
c
c     define dependent variable columns. these are
c     cols. of the identity matrix and implicitly generated.
c
      do 20 k = 1,mrelas
         j = ibasis(k)
         if (j.gt.nvars) then
            nzbm = nzbm+1
            if (ind(j).eq.2) then
               basmat(nzbm) = one
            else
               basmat(nzbm) = -one
            endif
            ibrc(nzbm,1) = j-nvars
            ibrc(nzbm,2) = k
         else
c
c           define the indep. variable cols.  this requires retrieving
c           the cols. from the sparse matrix data structure.
c
            i = 0
   10       call lppnnzrs(i,aij,iplace,amat,imat,j)
            if (i.gt.0) then
               nzbm = nzbm+1
               basmat(nzbm) = aij*csc(j)
               ibrc(nzbm,1) = i
               ibrc(nzbm,2) = k
               go to 10
            endif
         endif
   20 continue
c
      singlr = .false.
c
c     recompute matrix norm using crude norm  =  sum of magnitudes.
c
      anorm = lpsasum(nzbm,basmat,1)
      small = eps*anorm
c
c     get an l-u factorization of the basis matrix.
c
      nredc = nredc+1
      redbas = .true.
      call lpla05as(basmat,ibrc,nzbm,lbm,mrelas,ipr,iwr,wr,gg,uu)
c
c     check return value of error flag, gg.
c
      if (gg.ge.zero) return
      if (gg.eq.(-7.)) then
         call lpxermsg ('slatec', 'splpdm',
     *      'in splp, short on storage for la05as.  ' //
     *      'use prgopt(*) to give more.', 28, iopt)
         info = -28
      elseif (gg.eq.(-5.)) then
         singlr = .true.
      else
         write (xern3, '(1pe15.6)') gg
         call lpxermsg ('slatec', 'splpdm',
     *      'in splp, la05as returned error flag = ' // xern3,
     *      27, iopt)
         info = -27
      endif
      return
      end subroutine lpsplpdm
      subroutine lpsplpfe (mrelas, nvars, lmx, lbm, ienter, ibasis, imat
     +   ,ibrc, ipr, iwr, ind, ibb, erdnrm, eps, gg, dulnrm, dirnrm,
     +   amat, basmat, csc, wr, ww, bl, bu, rz, rg, colnrm, duals,
     +   found)
c***begin prologue  splpfe
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpfe-s, dplpfe-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/,/lpsasum/dasum/,
c     /scopy/dcopy/.
c
c     this subprogram is part of the splp( ) package.
c     it implements the procedure (find variable to enter basis
c     and get search direction).
c     revised 811130-1100
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  lpiploc, la05bs, prwpge, lpsasum, scopy
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  splpfe
      integer ibasis(*),imat(*),ibrc(lbm,2),ipr(*),iwr(*),ind(*),ibb(*)
      real             amat(*),basmat(*),csc(*),wr(*),ww(*),bl(*),bu(*),
     * rz(*),rg(*),colnrm(*),duals(*),cnorm,dirnrm,dulnrm,eps,erdnrm,gg,
     * one,ratio,rcost,rmax,zero,lpsasum
      logical found,trans
c***first executable statement  splpfe
      lpg=lmx-(nvars+4)
      zero=0.e0
      one=1.e0
      rmax=zero
      found=.false.
      i=mrelas+1
      n20002=mrelas+nvars
      go to 20003
20002 i=i+1
20003 if ((n20002-i).lt.0) go to 20004
      j=ibasis(i)
c
c     if j=ibasis(i) .lt. 0 then the variable left at a zero level
c     and is not considered as a candidate to enter.
      if (.not.(j.gt.0)) go to 20006
c
c     do not consider variables corresponding to unbounded step lengths.
      if (.not.(ibb(j).eq.0)) go to 20009
      go to 20002
20009 continue
c
c     if a variable corresponds to an equation(ind=3 and bl=bu),
c     then do not consider it as a candidate to enter.
      if (.not.(ind(j).eq.3)) go to 20012
      if (.not.((bu(j)-bl(j)).le.eps*(abs(bl(j))+abs(bu(j))))) go to 200
     *15
      go to 20002
20015 continue
20012 continue
      rcost=rz(j)
c
c     if variable is at upper bound it can only decrease.  this
c     accounts for the possible change of sign.
      if(mod(ibb(j),2).eq.0) rcost=-rcost
c
c     if the variable is free, use the negative magnitude of the
c     reduced cost for that variable.
      if(ind(j).eq.4) rcost=-abs(rcost)
      cnorm=one
      if(j.le.nvars)cnorm=colnrm(j)
c
c     test for negativity of reduced costs.
      if (.not.(rcost+erdnrm*dulnrm*cnorm.lt.zero)) go to 20018
      found=.true.
      ratio=rcost**2/rg(j)
      if (.not.(ratio.gt.rmax)) go to 20021
      rmax=ratio
      ienter=i
20021 continue
20018 continue
20006 go to 20002
c
c     use col. chosen to compute search direction.
20004 if (.not.(found)) go to 20024
      j=ibasis(ienter)
      ww(1)=zero
      call lpscopy(mrelas,ww,0,ww,1)
      if (.not.(j.le.nvars)) go to 20027
      if (.not.(j.eq.1)) go to 20030
      ilow=nvars+5
      go to 20031
20030 ilow=imat(j+3)+1
20031 continue
      il1=lpiploc(ilow,amat,imat)
      if (.not.(il1.ge.lmx-1)) go to 20033
      ilow=ilow+2
      il1=lpiploc(ilow,amat,imat)
20033 continue
      ipage=abs(imat(lmx-1))
      ihi=imat(j+4)-(ilow-il1)
20036 iu1=min(lmx-2,ihi)
      if (.not.(il1.gt.iu1)) go to 20038
      go to 20037
20038 continue
      do 30 i=il1,iu1
      ww(imat(i))=amat(i)*csc(j)
30    continue
      if (.not.(ihi.le.lmx-2)) go to 20041
      go to 20037
20041 continue
      ipage=ipage+1
      key=1
      call lpprwpge(key,ipage,lpg,amat,imat)
      il1=nvars+5
      ihi=ihi-lpg
      go to 20036
20037 go to 20028
20027 if (.not.(ind(j).eq.2)) go to 20044
      ww(j-nvars)=one
      go to 20045
20044 ww(j-nvars)=-one
20045 continue
      continue
c
c     compute search direction.
20028 trans=.false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
c
c     the search direction requires the following sign change if either
c     variable entering is at its upper bound or is free and has
c     positive reduced cost.
      if (.not.(mod(ibb(j),2).eq.0.or.(ind(j).eq.4 .and. rz(j).gt.zero))
     *) go to 20047
      i=1
      n20050=mrelas
      go to 20051
20050 i=i+1
20051 if ((n20050-i).lt.0) go to 20052
      ww(i)=-ww(i)
      go to 20050
20052 continue
20047 dirnrm=lpsasum(mrelas,ww,1)
c
c     copy contents of wr(*) to duals(*) for use in
c     add-drop (exchange) step, la05cs( ).
      call lpscopy(mrelas,wr,1,duals,1)
20024 return
      end subroutine lpsplpfe
      subroutine lpsplpfl (mrelas, nvars, ienter, ileave, ibasis, ind,
     +   ibb, theta, dirnrm, rprnrm, csc, ww, bl, bu, erp, rprim,
     +   primal, finite, zerolv)
c***begin prologue  splpfl
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpfl-s, dplpfl-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/.
c
c     this subprogram is part of the splp( ) package.
c     it implements the procedure (choose variable to leave basis).
c     revised 811130-1045
c     revised yymmdd-hhmm
c
c***see also  splp
c***routines called  (none)
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  splpfl
      integer ibasis(*),ind(*),ibb(*)
      real             csc(*),ww(*),bl(*),bu(*),erp(*),rprim(*),
     * primal(*),bound,dirnrm,ratio,rprnrm,theta,zero
      logical finite,zerolv
c***first executable statement  splpfl
      zero=0.e0
c
c     see if the entering variable is restricting the step length
c     because of an upper bound.
      finite=.false.
      j=ibasis(ienter)
      if (.not.(ind(j).eq.3)) go to 20002
      theta=bu(j)-bl(j)
      if(j.le.nvars)theta=theta/csc(j)
      finite=.true.
      ileave=ienter
c
c     now use the basic variables to possibly restrict the step
c     length even further.
20002 i=1
      n20005=mrelas
      go to 20006
20005 i=i+1
20006 if ((n20005-i).lt.0) go to 20007
      j=ibasis(i)
c
c     if this is a free variable, do not use it to
c     restrict the step length.
      if (.not.(ind(j).eq.4)) go to 20009
      go to 20005
c
c     if direction component is about zero, ignore it for computing
c     the step length.
20009 if (.not.(abs(ww(i)).le.dirnrm*erp(i))) go to 20012
      go to 20005
20012 if (.not.(ww(i).gt.zero)) go to 20015
c
c     if rprim(i) is essentially zero, set ratio to zero and exit loop.
      if (.not.(abs(rprim(i)).le.rprnrm*erp(i))) go to 20018
      theta=zero
      ileave=i
      finite=.true.
      go to 20008
c
c     the value of rprim(i) will decrease only to its lower bound or
c     only to its upper bound.  if it decreases to its
c     upper bound, then rprim(i) has already been translated
c     to its upper bound and nothing needs to be done to ibb(j).
20018 if (.not.(rprim(i).gt.zero)) go to 10001
      ratio=rprim(i)/ww(i)
      if (.not.(.not.finite)) go to 20021
      ileave=i
      theta=ratio
      finite=.true.
      go to 20022
20021 if (.not.(ratio.lt.theta)) go to 10002
      ileave=i
      theta=ratio
10002 continue
20022 continue
      go to 20019
c
c     the value rprim(i).lt.zero will not restrict the step.
10001 continue
c
c     the direction component is negative, therefore the variable will
c     increase.
20019 go to 20016
c
c     if the variable is less than its lower bound, it can
c     increase only to its lower bound.
20015 if (.not.(primal(i+nvars).lt.zero)) go to 20024
      ratio=rprim(i)/ww(i)
      if (ratio.lt.zero) ratio=zero
      if (.not.(.not.finite)) go to 20027
      ileave=i
      theta=ratio
      finite=.true.
      go to 20028
20027 if (.not.(ratio.lt.theta)) go to 10003
      ileave=i
      theta=ratio
10003 continue
20028 continue
c
c     if the basic variable is feasible and is not at its upper bound,
c     then it can increase to its upper bound.
      go to 20025
20024 if (.not.(ind(j).eq.3 .and. primal(i+nvars).eq.zero)) go to 10004
      bound=bu(j)-bl(j)
      if(j.le.nvars) bound=bound/csc(j)
      ratio=(bound-rprim(i))/(-ww(i))
      if (.not.(.not.finite)) go to 20030
      ileave=-i
      theta=ratio
      finite=.true.
      go to 20031
20030 if (.not.(ratio.lt.theta)) go to 10005
      ileave=-i
      theta=ratio
10005 continue
20031 continue
      continue
10004 continue
20025 continue
20016 go to 20005
20007 continue
c
c     if step length is finite, see if step length is about zero.
20008 if (.not.(finite)) go to 20033
      zerolv=.true.
      i=1
      n20036=mrelas
      go to 20037
20036 i=i+1
20037 if ((n20036-i).lt.0) go to 20038
      zerolv=zerolv.and. abs(theta*ww(i)).le.erp(i)*rprnrm
      if (.not.(.not. zerolv)) go to 20040
      go to 20039
20040 go to 20036
20038 continue
20039 continue
20033 continue
      return
      end subroutine lpsplpfl
      subroutine lpsplpmn (lpusrmat, mrelas, nvars, costs, prgopt,
     *   dattrv,
     +   bl, bu, ind, info, primal, duals, amat, csc, colnrm, erd, erp,
     +   basmat, wr, rz, rg, rprim, rhs, ww, lmx, lbm, ibasis, ibb,
     +   imat, ibrc, ipr, iwr)
c***begin prologue  splpmn
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpmn-s, dplpmn-d)
c***author  (unknown)
c***description
c
c     marvel option(s).. output=yes/no to eliminate printed output.
c     this does not apply to the calls to the error processor.
c
c     main subroutine lpfor splp package.
c
c***see also  splp
c***routines called  ivout, la05bs, pinitm, pnnzrs, prwpge, lpsasum,
c                    sclosm, scopy, lpsdot, spincw, spinit, splpce,
c                    splpdm, splpfe, splpfl, splpmu, splpup, spopt,
c                    svout, xermsg
c***common blocks    la05ds
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  corrected references to xerrwv.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891009  removed unreferenced variable.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  splpmn
      real             abig,aij,amat(*),anorm,asmall,basmat(*),
     * bl(*),bu(*),colnrm(*),costs(*),costsc,csc(*),dattrv(*),
     * dirnrm,duals(*),dulnrm,eps,tune,erd(*),erdnrm,erp(*),factor,gg,
     * one,prgopt(*),primal(*),resnrm,rg(*),rhs(*),rhsnrm,ropt(07),
     * rprim(*),rprnrm,rz(*),rzj,scalr,scosts,size,small,theta,lpsasum,
     * tolls,upbnd,uu,wr(*),ww(*),xlamda,xval,zero,rdum(01),tolabs,
     * lpsdot
c
      integer ibasis(*),ibb(*),ibrc(lbm,2),imat(*),ind(*),
     * ipr(*),iwr(*),intopt(08),idum(01)
c
c     array local variables
c     name(length)          description
c
c     costs(nvars)          cost coefficients
c     prgopt( )             option vector
c     dattrv( )             data transfer vector
c     primal(nvars+mrelas)  as output it is primal solution of lp.
c                           internally, the first nvars positions hold
c                           the column check sums.  the next mrelas
c                           positions hold the classification for the
c                           basic variables  -1 violates lower
c                           bound, 0 feasible, +1 violates upper bound
c     duals(mrelas+nvars)   dual solution. internally holds r.h. side
c                           as first mrelas entries.
c     amat(lmx)             sparse form of data matrix
c     imat(lmx)             sparse form of data matrix
c     bl(nvars+mrelas)      lower bounds for variables
c     bu(nvars+mrelas)      upper bounds for variables
c     ind(nvars+mrelas)     indicator for variables
c     csc(nvars)            column scaling
c     ibasis(nvars+mrelas)  cols. 1-mrelas are basic, rest are non-basic
c     ibb(nvars+mrelas)     indicator for non-basic vars., polarity of
c                           vars., and potentially infinite vars.
c                           if ibb(j).lt.0, variable j is basic
c                           if ibb(j).gt.0, variable j is non-basic
c                           if ibb(j).eq.0, variable j has to be ignored
c                           because it would cause unbounded soln.
c                           when mod(ibb(j),2).eq.0, variable is at its
c                           upper bound, otherwise it is at its lower
c                           bound
c     colnrm(nvars)         norm of columns
c     erd(mrelas)           errors in dual variables
c     erp(mrelas)           errors in primal variables
c     basmat(lbm)           basis matrix for harwell sparse code
c     ibrc(lbm,2)           row and column pointers for basmat(*)
c     ipr(2*mrelas)         work array for harwell sparse code
c     iwr(8*mrelas)         work array for harwell sparse code
c     wr(mrelas)            work array for harwell sparse code
c     rz(nvars+mrelas)      reduced costs
c     rprim(mrelas)         internal primal solution
c     rg(nvars+mrelas)      column weights
c     ww(mrelas)            work array
c     rhs(mrelas)           holds translated right hand side
c
c     scalar local variables
c     name       type         description
c
c     lmx        integer      length of amat(*)
c     lpg        integer      length of page for amat(*)
c     eps        real         machine precision
c     tune       real         parameter to scale error estimates
c     tolls      real         relative tolerance for small residuals
c     tolabs     real         absolute tolerance for small residuals.
c                             used if relative error test fails.
c                             in constraint equations
c     factor     real         .01--determines if basis is singular
c                             or component is feasible.  may need to
c                             be increased to 1.e0 on short word
c                             length machines.
c     asmall     real         lower bound for non-zero magn. in amat(*)
c     abig       real         upper bound for non-zero magn. in amat(*)
c     mxitlp     integer      maximum number of iterations for lp
c     itlp       integer      iteration counter for total lp iters
c     costsc     real         costs(*) scaling
c     scosts     real         temp loc. for costsc.
c     xlamda     real         weight parameter for pen. method.
c     anorm      real         norm of data matrix amat(*)
c     rprnrm     real         norm of the solution
c     dulnrm     real         norm of the duals
c     erdnrm     real         norm of error in dual variables
c     dirnrm     real         norm of the direction vector
c     rhsnrm     real         norm of translated right hand side vector
c     resnrm     real         norm of residual vector for checking
c                             feasibility
c     nzbm       integer      number of non-zeros in basmat(*)
c     lbm        integer      length of basmat(*)
c     small      real         eps*anorm  used in harwell sparse code
c     lp         integer      used in harwell la05*() pack as output
c                             file number. set=lpi1mach(4) now.
c     uu         real         0.1--used in harwell sparse code
c                             for relative pivoting tolerance.
c     gg         real         output info flag in harwell sparse code
c     iplace     integer      integer used by sparse matrix codes
c     ienter     integer      next column to enter basis
c     nredc      integer      no. of full redecompositions
c     kprint     integer      level of output, =0-3
c     idg        integer      format and precision of output
c     itbrc      integer      no. of iters. between recalculating
c                             the error in the primal solution.
c     npp        integer      no. of negative reduced costs required
c                             in partial pricing
c     jstrt      integer      starting place for partial pricing.
c
      logical colscp,savedt,contin,cstscp,unbnd,
     *        feas,finite,found,minprb,redbas,
     *        singlr,sizeup,stpedg,trans,usrbas,zerolv,lopt(08)
      character*8 xern1, xern2
      equivalence (contin,lopt(1)),(usrbas,lopt(2)),
     *  (sizeup,lopt(3)),(savedt,lopt(4)),(colscp,lopt(5)),
     *  (cstscp,lopt(6)),(minprb,lopt(7)),(stpedg,lopt(8)),
     *  (idg,intopt(1)),(ipagef,intopt(2)),(isave,intopt(3)),
     *  (mxitlp,intopt(4)),(kprint,intopt(5)),(itbrc,intopt(6)),
     *  (npp,intopt(7)),(lprg,intopt(8)),(eps,ropt(1)),(asmall,ropt(2)),
     *  (abig,ropt(3)),(costsc,ropt(4)),(tolls,ropt(5)),(tune,ropt(6)),
     *   (tolabs,ropt(7))
c
c     common block used by la05 () package..
      common /la05ds/ small,lp,lenl,lenu,ncp,lrow,lcol
      external lpusrmat
c
c     set lp=0 so no error messages will print within la05 () package.
c***first executable statement  splpmn
      lp=0
c
c     the values zero and one.
      zero=0.e0
      one=1.e0
      factor=0.01e0
      lpg=lmx-(nvars+4)
      iopt=1
      info=0
      unbnd=.false.
      jstrt=1
c
c     process user options in prgopt(*).
c     check that any user-given changes are well-defined.
      call lpspopt(prgopt,mrelas,nvars,info,csc,ibasis,ropt,intopt,lopt)
      if (.not.(info.lt.0)) go to 20002
      go to 30001
20002 if (.not.(contin)) go to 20003
      go to 30002
20006 go to 20004
c
c     initialize sparse data matrix, amat(*) and imat(*).
20003 call lppinitm(mrelas,nvars,amat,imat,lmx,ipagef)
c
c     update matrix data and check bounds for consistency.
20004 call lpsplpup(lpusrmat,mrelas,nvars,prgopt,dattrv,
     *     bl,bu,ind,info,amat,imat,sizeup,asmall,abig)
      if (.not.(info.lt.0)) go to 20007
      go to 30001
c
c++  code for output=yes is active
20007 if (.not.(kprint.ge.1)) go to 20008
      go to 30003
20011 continue
c++  code for output=no is inactive
c++  end
c
c     initialization. scale data, normalize bounds, form column
c     check sums, and form initial basis matrix.
20008 call lpspinit(mrelas,nvars,costs,bl,bu,ind,primal,info,
     * amat,csc,costsc,colnrm,xlamda,anorm,rhs,rhsnrm,
     * ibasis,ibb,imat,lopt)
      if (.not.(info.lt.0)) go to 20012
      go to 30001
c
20012 nredc=0
      assign 20013 to npr004
      go to 30004
20013 if (.not.(singlr)) go to 20014
      nerr=23
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp,  a singular initial basis was encountered.', nerr,
     +   iopt)
      info=-nerr
      go to 30001
20014 assign 20018 to npr005
      go to 30005
20018 assign 20019 to npr006
      go to 30006
20019 assign 20020 to npr007
      go to 30007
20020 if (.not.(usrbas)) go to 20021
      assign 20024 to npr008
      go to 30008
20024 if (.not.(.not.feas)) go to 20025
      nerr=24
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp, an infeasible initial basis was encountered.', nerr,
     +   iopt)
      info=-nerr
      go to 30001
20025 continue
20021 itlp=0
c
c     lamda has been set to a constant, perform penalty method.
      assign 20029 to npr009
      go to 30009
20029 assign 20030 to npr010
      go to 30010
20030 assign 20031 to npr006
      go to 30006
20031 assign 20032 to npr008
      go to 30008
20032 if (.not.(.not.feas)) go to 20033
c
c     set lamda to infinity by setting costsc to zero (save the value of
c     costsc) and perform standard phase-1.
      if(kprint.ge.2)call lpivout(0,idum,'('' enter standard phase-1'')'
     *,idg)
      scosts=costsc
      costsc=zero
      assign 20036 to npr007
      go to 30007
20036 assign 20037 to npr009
      go to 30009
20037 assign 20038 to npr010
      go to 30010
20038 assign 20039 to npr006
      go to 30006
20039 assign 20040 to npr008
      go to 30008
20040 if (.not.(feas)) go to 20041
c
c     set lamda to zero, costsc=scosts, perform standard phase-2.
      if(kprint.gt.1)call lpivout(0,idum,'('' enter standard phase-2'')'
     *,idg)
      xlamda=zero
      costsc=scosts
      assign 20044 to npr009
      go to 30009
20044 continue
20041 go to 20034
c     check if any basic variables are still classified as
c     infeasible.  if any are, then this may not yet be an
c     optimal point.  therefore set lamda to zero and try
c     to perform more simplex steps.
20033 i=1
      n20046=mrelas
      go to 20047
20046 i=i+1
20047 if ((n20046-i).lt.0) go to 20048
      if (primal(i+nvars).ne.zero) go to 20045
      go to 20046
20048 go to 20035
20045 xlamda=zero
      assign 20050 to npr009
      go to 30009
20050 continue
20034 continue
c
20035 assign 20051 to npr011
      go to 30011
20051 if (.not.(feas.and.(.not.unbnd))) go to 20052
      info=1
      go to 20053
20052 if (.not.((.not.feas).and.(.not.unbnd))) go to 10001
      nerr=1
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp, the problem appears to be infeasible', nerr, iopt)
      info=-nerr
      go to 20053
10001 if (.not.(feas .and. unbnd)) go to 10002
      nerr=2
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp, the problem appears to have no finite solution.',
     +   nerr, iopt)
      info=-nerr
      go to 20053
10002 if (.not.((.not.feas).and.unbnd)) go to 10003
      nerr=3
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp, the problem appears to be infeasible and to have ' //
     +   'no finite solution.', nerr, iopt)
      info=-nerr
10003 continue
20053 continue
c
      if (.not.(info.eq.(-1) .or. info.eq.(-3))) go to 20055
      size=lpsasum(nvars,primal,1)*anorm
      size=size/lpsasum(nvars,csc,1)
      size=size+lpsasum(mrelas,primal(nvars+1),1)
      i=1
      n20058=nvars+mrelas
      go to 20059
20058 i=i+1
20059 if ((n20058-i).lt.0) go to 20060
      nx0066=ind(i)
      if (nx0066.lt.1.or.nx0066.gt.4) go to 20066
      go to (20062,20063,20064,20065), nx0066
20062 if (.not.(size+abs(primal(i)-bl(i))*factor.eq.size)) go to 20068
      go to 20058
20068 if (.not.(primal(i).gt.bl(i))) go to 10004
      go to 20058
10004 ind(i)=-4
      go to 20067
20063 if (.not.(size+abs(primal(i)-bu(i))*factor.eq.size)) go to 20071
      go to 20058
20071 if (.not.(primal(i).lt.bu(i))) go to 10005
      go to 20058
10005 ind(i)=-4
      go to 20067
20064 if (.not.(size+abs(primal(i)-bl(i))*factor.eq.size)) go to 20074
      go to 20058
20074 if (.not.(primal(i).lt.bl(i))) go to 10006
      ind(i)=-4
      go to 20075
10006 if (.not.(size+abs(primal(i)-bu(i))*factor.eq.size)) go to 10007
      go to 20058
10007 if (.not.(primal(i).gt.bu(i))) go to 10008
      ind(i)=-4
      go to 20075
10008 go to 20058
20075 go to 20067
20065 go to 20058
20066 continue
20067 go to 20058
20060 continue
20055 continue
c
      if (.not.(info.eq.(-2) .or. info.eq.(-3))) go to 20077
      j=1
      n20080=nvars
      go to 20081
20080 j=j+1
20081 if ((n20080-j).lt.0) go to 20082
      if (.not.(ibb(j).eq.0)) go to 20084
      nx0091=ind(j)
      if (nx0091.lt.1.or.nx0091.gt.4) go to 20091
      go to (20087,20088,20089,20090), nx0091
20087 bu(j)=bl(j)
      ind(j)=-3
      go to 20092
20088 bl(j)=bu(j)
      ind(j)=-3
      go to 20092
20089 go to 20080
20090 bl(j)=zero
      bu(j)=zero
      ind(j)=-3
20091 continue
20092 continue
20084 go to 20080
20082 continue
20077 continue
c++  code for output=yes is active
      if (.not.(kprint.ge.1)) go to 20093
      assign 20096 to npr012
      go to 30012
20096 continue
20093 continue
c++  code for output=no is inactive
c++  end
      go to 30001
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (compute right hand side)
30010 rhs(1)=zero
      call lpscopy(mrelas,rhs,0,rhs,1)
      j=1
      n20098=nvars+mrelas
      go to 20099
20098 j=j+1
20099 if ((n20098-j).lt.0) go to 20100
      nx0106=ind(j)
      if (nx0106.lt.1.or.nx0106.gt.4) go to 20106
      go to (20102,20103,20104,20105), nx0106
20102 scalr=-bl(j)
      go to 20107
20103 scalr=-bu(j)
      go to 20107
20104 scalr=-bl(j)
      go to 20107
20105 scalr=zero
20106 continue
20107 if (.not.(scalr.ne.zero)) go to 20108
      if (.not.(j.le.nvars)) go to 20111
      i=0
20114 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.le.0)) go to 20116
      go to 20115
20116 rhs(i)=rhs(i)+aij*scalr
      go to 20114
20115 go to 20112
20111 rhs(j-nvars)=rhs(j-nvars)-scalr
20112 continue
20108 go to 20098
20100 j=1
      n20119=nvars+mrelas
      go to 20120
20119 j=j+1
20120 if ((n20119-j).lt.0) go to 20121
      scalr=zero
      if(ind(j).eq.3.and.mod(ibb(j),2).eq.0) scalr=bu(j)-bl(j)
      if (.not.(scalr.ne.zero)) go to 20123
      if (.not.(j.le.nvars)) go to 20126
      i=0
20129 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.le.0)) go to 20131
      go to 20130
20131 rhs(i)=rhs(i)-aij*scalr
      go to 20129
20130 go to 20127
20126 rhs(j-nvars)=rhs(j-nvars)+scalr
20127 continue
20123 go to 20119
20121 continue
      go to npr010, (20030,20038)
c     procedure (perform simplex steps)
30009 assign 20134 to npr013
      go to 30013
20134 assign 20135 to npr014
      go to 30014
20135 if (.not.(kprint.gt.2)) go to 20136
      call lpsvout(mrelas,duals,'(''basic (internal) dual soln.'')',idg)
      call lpsvout(nvars+mrelas,rz,'('' reduced costs'')',idg)
20136 continue
20139 assign 20141 to npr015
      go to 30015
20141 if (.not.(.not. found)) go to 20142
      go to 30016
20145 continue
20142 if (.not.(found)) go to 20146
      if (kprint.ge.3) call lpsvout(mrelas,ww,'('' search direction'')',
     *idg)
      go to 30017
20149 if (.not.(finite)) go to 20150
      go to 30018
20153 assign 20154 to npr005
      go to 30005
20154 go to 20151
20150 unbnd=.true.
      ibb(ibasis(ienter))=0
20151 go to 20147
20146 go to 20140
20147 itlp=itlp+1
      go to 30019
20155 go to 20139
20140 continue
      go to npr009, (20029,20037,20044,20050)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (retrieve saved data from file isave)
30002 lpr=nvars+4
      rewind isave
      read(isave) (amat(i),i=1,lpr),(imat(i),i=1,lpr)
      key=2
      ipage=1
      go to 20157
20156 if (np.lt.0) go to 20158
20157 lpr1=lpr+1
      read(isave) (amat(i),i=lpr1,lmx),(imat(i),i=lpr1,lmx)
      call lpprwpge(key,ipage,lpg,amat,imat)
      np=imat(lmx-1)
      ipage=ipage+1
      go to 20156
20158 nparm=nvars+mrelas
      read(isave) (ibasis(i),i=1,nparm)
      rewind isave
      go to 20006
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (save data on file isave)
c
c     some pages may not be written yet.
30020 if (.not.(amat(lmx).eq.one)) go to 20159
      amat(lmx)=zero
      key=2
      ipage=abs(imat(lmx-1))
      call lpprwpge(key,ipage,lpg,amat,imat)
c
c     force page file to be opened on restarts.
20159 key=amat(4)
      amat(4)=zero
      lpr=nvars+4
      write(isave) (amat(i),i=1,lpr),(imat(i),i=1,lpr)
      amat(4)=key
      ipage=1
      key=1
      go to 20163
20162 if (np.lt.0) go to 20164
20163 call lpprwpge(key,ipage,lpg,amat,imat)
      lpr1=lpr+1
      write(isave) (amat(i),i=lpr1,lmx),(imat(i),i=lpr1,lmx)
      np=imat(lmx-1)
      ipage=ipage+1
      go to 20162
20164 nparm=nvars+mrelas
      write(isave) (ibasis(i),i=1,nparm)
      endfile isave
c
c     close file, ipagef, where pages are stored. this is needed so that
c     the pages may be restored at a continuation of splp().
      go to 20317
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (decompose basis matrix)
c++  code for output=yes is active
30004 if (.not.(kprint.ge.2)) go to 20165
      call lpivout(mrelas,ibasis,
     *'('' subscripts of basic variables during redecomposition'')',
     *idg)
c++  code for output=no is inactive
c++  end
c
c     set relative pivoting factor for use in la05 () package.
20165 uu=0.1
      call lpsplpdm(
     *mrelas,nvars,lmx,lbm,nredc,info,iopt,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *anorm,eps,uu,gg,
     *amat,basmat,csc,wr,
     *singlr,redbas)
      if (.not.(info.lt.0)) go to 20168
      go to 30001
20168 continue
      go to npr004, (20013,20204,20242)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (classify variables)
c
c     define the classification of the basic variables
c     -1 violates lower bound, 0 feasible, +1 violates upper bound.
c     (this info is stored in primal(nvars+1)-primal(nvars+mrelas))
c     translate variable to its upper bound, if .gt. upper bound
30007 primal(nvars+1)=zero
      call lpscopy(mrelas,primal(nvars+1),0,primal(nvars+1),1)
      i=1
      n20172=mrelas
      go to 20173
20172 i=i+1
20173 if ((n20172-i).lt.0) go to 20174
      j=ibasis(i)
      if (.not.(ind(j).ne.4)) go to 20176
      if (.not.(rprim(i).lt.zero)) go to 20179
      primal(i+nvars)=-one
      go to 20180
20179 if (.not.(ind(j).eq.3)) go to 10009
      upbnd=bu(j)-bl(j)
      if (j.le.nvars) upbnd=upbnd/csc(j)
      if (.not.(rprim(i).gt.upbnd)) go to 20182
      rprim(i)=rprim(i)-upbnd
      if (.not.(j.le.nvars)) go to 20185
      k=0
20188 call lppnnzrs(k,aij,iplace,amat,imat,j)
      if (.not.(k.le.0)) go to 20190
      go to 20189
20190 rhs(k)=rhs(k)-upbnd*aij*csc(j)
      go to 20188
20189 go to 20186
20185 rhs(j-nvars)=rhs(j-nvars)+upbnd
20186 primal(i+nvars)=one
20182 continue
      continue
10009 continue
20180 continue
20176 go to 20172
20174 continue
      go to npr007, (20020,20036)
c cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (compute error in dual and primal systems)
30005 ntries=1
      go to 20195
20194 ntries=ntries+1
20195 if ((2-ntries).lt.0) go to 20196
      call lpsplpce(
     *mrelas,nvars,lmx,lbm,itlp,itbrc,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *erdnrm,eps,tune,gg,
     *amat,basmat,csc,wr,ww,primal,erd,erp,
     *singlr,redbas)
      if (.not.(.not. singlr)) go to 20198
c++  code for output=yes is active
      if (.not.(kprint.ge.3)) go to 20201
      call lpsvout(mrelas,erp,'('' est. error in primal comps.'')',idg)
      call lpsvout(mrelas,erd,'('' est. error in dual comps.'')',idg)
20201 continue
c++  code for output=no is inactive
c++  end
      go to 20193
20198 if (ntries.eq.2) go to 20197
      assign 20204 to npr004
      go to 30004
20204 continue
      go to 20194
20196 continue
20197 nerr=26
      call lpxermsg ('slatec', 'splpmn',
     +   'in splp, moved to a singular point.  this should not happen.',
     +   nerr, iopt)
      info=-nerr
      go to 30001
20193 continue
      go to npr005, (20018,20154,20243)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (check feasibility)
c
c     see if nearby feasible point satisfies the constraint
c     equations.
c
c     copy rhs into ww(*), then update ww(*).
30008 call lpscopy(mrelas,rhs,1,ww,1)
      j=1
      n20206=mrelas
      go to 20207
20206 j=j+1
20207 if ((n20206-j).lt.0) go to 20208
      ibas=ibasis(j)
      xval=rprim(j)
c
c     all variables bounded below have zero as that bound.
      if (ind(ibas).le.3) xval=max(zero,xval)
c
c     if the variable has an upper bound, compute that bound.
      if (.not.(ind(ibas).eq.3)) go to 20210
      upbnd=bu(ibas)-bl(ibas)
      if (ibas.le.nvars) upbnd=upbnd/csc(ibas)
      xval=min(upbnd,xval)
20210 continue
c
c     subtract xval times column vector from right-hand side in ww(*)
      if (.not.(xval.ne.zero)) go to 20213
      if (.not.(ibas.le.nvars)) go to 20216
      i=0
20219 call lppnnzrs(i,aij,iplace,amat,imat,ibas)
      if (.not.(i.le.0)) go to 20221
      go to 20220
20221 ww(i)=ww(i)-xval*aij*csc(ibas)
      go to 20219
20220 go to 20217
20216 if (.not.(ind(ibas).eq.2)) go to 20224
      ww(ibas-nvars)=ww(ibas-nvars)-xval
      go to 20225
20224 ww(ibas-nvars)=ww(ibas-nvars)+xval
20225 continue
20217 continue
20213 continue
      go to 20206
c
c   compute norm of difference and check for feasibility.
20208 resnrm=lpsasum(mrelas,ww,1)
      feas=resnrm.le.tolls*(rprnrm*anorm+rhsnrm)
c
c     try an absolute error test if the relative test fails.
      if(.not. feas)feas=resnrm.le.tolabs
      if (.not.(feas)) go to 20227
      primal(nvars+1)=zero
      call lpscopy(mrelas,primal(nvars+1),0,primal(nvars+1),1)
20227 continue
      go to npr008, (20024,20032,20040)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (initialize reduced costs and steepest edge weights)
30014 call lpspincw(
     *mrelas,nvars,lmx,lbm,npp,jstrt,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *costsc,gg,erdnrm,dulnrm,
     *amat,basmat,csc,wr,ww,rz,rg,costs,colnrm,duals,
     *stpedg)
c
      go to npr014, (20135,20246)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (check and return with excess iterations)
30019 if (.not.(itlp.gt.mxitlp)) go to 20230
      nerr=25
      assign 20233 to npr011
      go to 30011
c++  code for output=yes is active
20233 if (.not.(kprint.ge.1)) go to 20234
      assign 20237 to npr012
      go to 30012
20237 continue
20234 continue
c++  code for output=no is inactive
c++  end
      idum(1)=0
      if(savedt) idum(1)=isave
      write (xern1, '(i8)') mxitlp
      write (xern2, '(i8)') idum(1)
      call lpxermsg ('slatec', 'splpmn',
     *   'in splp, max iterations = ' // xern1 //
     *   ' taken.  up-to-date results saved on file no. ' // xern2 //
     *   '.  if file no. = 0, no save.', nerr, iopt)
      info=-nerr
      go to 30001
20230 continue
      go to 20155
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (redecompose basis matrix and try again)
30016 if (.not.(.not.redbas)) go to 20239
      assign 20242 to npr004
      go to 30004
20242 assign 20243 to npr005
      go to 30005
20243 assign 20244 to npr006
      go to 30006
20244 assign 20245 to npr013
      go to 30013
20245 assign 20246 to npr014
      go to 30014
20246 continue
c
c     erase non-cycling markers near completion.
20239 i=mrelas+1
      n20247=mrelas+nvars
      go to 20248
20247 i=i+1
20248 if ((n20247-i).lt.0) go to 20249
      ibasis(i)=abs(ibasis(i))
      go to 20247
20249 assign 20251 to npr015
      go to 30015
20251 continue
      go to 20145
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (compute new primal)
c
c     copy rhs into ww(*), solve system.
30006 call lpscopy(mrelas,rhs,1,ww,1)
      trans = .false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      call lpscopy(mrelas,ww,1,rprim,1)
      rprnrm=lpsasum(mrelas,rprim,1)
      go to npr006, (20019,20031,20039,20244,20275)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (compute new duals)
c
c     solve for dual variables. first copy costs into duals(*).
30013 i=1
      n20252=mrelas
      go to 20253
20252 i=i+1
20253 if ((n20252-i).lt.0) go to 20254
      j=ibasis(i)
      if (.not.(j.le.nvars)) go to 20256
      duals(i)=costsc*costs(j)*csc(j) + xlamda*primal(i+nvars)
      go to 20257
20256 duals(i)=xlamda*primal(i+nvars)
20257 continue
      go to 20252
c
20254 trans=.true.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,duals,trans)
      dulnrm=lpsasum(mrelas,duals,1)
      go to npr013, (20134,20245,20267)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (find variable to enter basis and get search direction)
30015 call lpsplpfe(
     *mrelas,nvars,lmx,lbm,ienter,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *erdnrm,eps,gg,dulnrm,dirnrm,
     *amat,basmat,csc,wr,ww,bl,bu,rz,rg,colnrm,duals,
     *found)
      go to npr015, (20141,20251)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (choose variable to leave basis)
30017 call lpsplpfl(
     *mrelas,nvars,ienter,ileave,
     *ibasis,ind,ibb,
     *theta,dirnrm,rprnrm,
     *csc,ww,bl,bu,erp,rprim,primal,
     *finite,zerolv)
      go to 20149
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (make move and update)
30018 call lpsplpmu(
     *mrelas,nvars,lmx,lbm,nredc,info,ienter,ileave,iopt,npp,jstrt,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *anorm,eps,uu,gg,rprnrm,erdnrm,dulnrm,theta,costsc,xlamda,rhsnrm,
     *amat,basmat,csc,wr,rprim,ww,bu,bl,rhs,erd,erp,rz,rg,colnrm,costs,
     *primal,duals,singlr,redbas,zerolv,stpedg)
      if (.not.(info.eq.(-26))) go to 20259
      go to 30001
c++  code for output=yes is active
20259 if (.not.(kprint.ge.2)) go to 20263
      go to 30021
20266 continue
c++  code for output=no is inactive
c++  end
20263 continue
      go to 20153
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure(rescale and rearrange variables)
c
c     rescale the dual variables.
30011 assign 20267 to npr013
      go to 30013
20267 if (.not.(costsc.ne.zero)) go to 20268
      i=1
      n20271=mrelas
      go to 20272
20271 i=i+1
20272 if ((n20271-i).lt.0) go to 20273
      duals(i)=duals(i)/costsc
      go to 20271
20273 continue
20268 assign 20275 to npr006
      go to 30006
c
c     reapply column scaling to primal.
20275 i=1
      n20276=mrelas
      go to 20277
20276 i=i+1
20277 if ((n20276-i).lt.0) go to 20278
      j=ibasis(i)
      if (.not.(j.le.nvars)) go to 20280
      scalr=csc(j)
      if(ind(j).eq.2)scalr=-scalr
      rprim(i)=rprim(i)*scalr
20280 go to 20276
c
c     replace translated basic variables into array primal(*)
20278 primal(1)=zero
      call lpscopy(nvars+mrelas,primal,0,primal,1)
      j=1
      n20283=nvars+mrelas
      go to 20284
20283 j=j+1
20284 if ((n20283-j).lt.0) go to 20285
      ibas=abs(ibasis(j))
      xval=zero
      if (j.le.mrelas) xval=rprim(j)
      if (ind(ibas).eq.1) xval=xval+bl(ibas)
      if (ind(ibas).eq.2) xval=bu(ibas)-xval
      if (.not.(ind(ibas).eq.3)) go to 20287
      if (mod(ibb(ibas),2).eq.0) xval=bu(ibas)-bl(ibas)-xval
      xval = xval+bl(ibas)
20287 primal(ibas)=xval
      go to 20283
c
c     compute duals for independent variables with bounds.
c     other entries are zero.
20285 j=1
      n20290=nvars
      go to 20291
20290 j=j+1
20291 if ((n20290-j).lt.0) go to 20292
      rzj=zero
      if (.not.(ibb(j).gt.zero .and. ind(j).ne.4)) go to 20294
      rzj=costs(j)
      i=0
20297 call lppnnzrs(i,aij,iplace,amat,imat,j)
      if (.not.(i.le.0)) go to 20299
      go to 20298
20299 continue
      rzj=rzj-aij*duals(i)
      go to 20297
20298 continue
20294 duals(mrelas+j)=rzj
      go to 20290
20292 continue
      go to npr011, (20051,20233)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c++  code for output=yes is active
c     procedure (print prologue)
30003 idum(1)=mrelas
      call lpivout(1,idum,'(''1num. of dependent vars., mrelas'')',idg)
      idum(1)=nvars
      call lpivout(1,idum,'('' num. of independent vars., nvars'')',idg)
      call lpivout(1,idum,'('' dimension of costs(*)='')',idg)
      idum(1)=nvars+mrelas
      call lpivout(1,idum, '('' dimensions of bl(*),bu(*),ind(*)''
     */'' primal(*),duals(*) ='')',idg)
      call lpivout(1,idum,'('' dimension of ibasis(*)='')',idg)
      idum(1)=lprg+1
      call lpivout(1,idum,'('' dimension of prgopt(*)='')',idg)
      call lpivout(0,idum,
     * '('' 1-nvars=independent variable indices.''/
     * '' (nvars+1)-(nvars+mrelas)=dependent variable indices.''/
     * '' constraint indicators are 1-4 and mean'')',idg)
      call lpivout(0,idum,
     * '('' 1=variable has only lower bound.''/
     * '' 2=variable has only upper bound.''/
     * '' 3=variable has both bounds.''/
     * '' 4=variable has no bounds, it is free.'')',idg)
      call lpsvout(nvars,costs,'('' array of costs'')',idg)
      call lpivout(nvars+mrelas,ind,
     * '('' constraint indicators'')',idg)
      call lpsvout(nvars+mrelas,bl,
     *'('' lower bounds for variables  (ignore unused entries.)'')',idg)
      call lpsvout(nvars+mrelas,bu,
     *'('' upper bounds for variables  (ignore unused entries.)'')',idg)
      if (.not.(kprint.ge.2)) go to 20302
      call lpivout(0,idum,
     * '(''0non-basic indices that are negative show variables''
     * '' exchanged at a zero''/'' step length'')',idg)
      call lpivout(0,idum,
     * '('' when col. no. leaving=col. no. entering, the entering ''
     * ''variable moved''/'' to its bound.  it remains non-basic.''/
     * '' when col. no. of basis exchanged is negative, the leaving''/
     * '' variable is at its upper bound.'')',idg)
20302 continue
      go to 20011
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (print summary)
30012 idum(1)=info
      call lpivout(1,idum,'('' the output value of info is'')',idg)
      if (.not.(minprb)) go to 20305
      call lpivout(0,idum,'('' this is a minimization problem.'')',idg)
      go to 20306
20305 call lpivout(0,idum,'('' this is a maximization problem.'')',idg)
20306 if (.not.(stpedg)) go to 20308
      call lpivout(0,idum,'('' steepest edge pricing was used.'')',idg)
      go to 20309
20308 call lpivout(0,idum,'('' minimum reduced cost pricing was used.''
     *)',idg)
20309 rdum(1)=lpsdot(nvars,costs,1,primal,1)
      call lpsvout(1,rdum,
     * '('' output value of the objective function'')',idg)
      call lpsvout(nvars+mrelas,primal,
     * '('' the output independent and dependent variables'')',idg)
      call lpsvout(mrelas+nvars,duals,
     * '('' the output dual variables'')',idg)
      call lpivout(nvars+mrelas,ibasis,
     * '('' variable indices in positions 1-mrelas are basic.'')',idg)
      idum(1)=itlp
      call lpivout(1,idum,'('' no. of iterations'')',idg)
      idum(1)=nredc
      call lpivout(1,idum,'('' no. of full redecomps'')',idg)
      go to npr012, (20096,20237)
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (print iteration summary)
30021 idum(1)=itlp+1
      call lpivout(1,idum,'(''0iteration number'')',idg)
      idum(1)=ibasis(abs(ileave))
      call lpivout(1,idum,
     * '('' index of variable entering the basis'')',idg)
      idum(1)=ileave
      call lpivout(1,idum,'('' column of the basis exchanged'')',idg)
      idum(1)=ibasis(ienter)
      call lpivout(1,idum,
     * '('' index of variable leaving the basis'')',idg)
      rdum(1)=theta
      call lpsvout(1,rdum,'('' length of the exchange step'')',idg)
      if (.not.(kprint.ge.3)) go to 20311
      call lpsvout(mrelas,rprim,'('' basic (internal) primal soln.'')',
     * idg)
      call lpivout(nvars+mrelas,ibasis,
     * '('' variable indices in positions 1-mrelas are basic.'')',idg)
      call lpivout(nvars+mrelas,ibb,'('' ibb array'')',idg)
      call lpsvout(mrelas,rhs,'('' translated rhs'')',idg)
      call lpsvout(mrelas,duals,'('' basic (internal) dual soln.'')'
     *,idg)
20311 continue
      go to 20266
c++  code for output=no is inactive
c++  end
c ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     procedure (return to user)
30001 if (.not.(savedt)) go to 20314
      go to 30020
20317 continue
20314 if(imat(lmx-1).ne.(-1)) call lpsclosm(ipagef)
c
c     this test is there only to avoid diagnostics on some fortran
c     compilers.
      return
      end subroutine lpsplpmn
      subroutine lpsplpmu (mrelas, nvars, lmx, lbm, nredc, info, ienter,
     +   ileave, iopt, npp, jstrt, ibasis, imat, ibrc, ipr, iwr, ind,
     +   ibb, anorm, eps, uu, gg, rprnrm, erdnrm, dulnrm, theta, costsc,
     +   xlamda, rhsnrm, amat, basmat, csc, wr, rprim, ww, bu, bl, rhs,
     +   erd, erp, rz, rg, colnrm, costs, primal, duals, singlr, redbas,
     +   zerolv, stpedg)
c***begin prologue  splpmu
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpmu-s, dplpmu-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/,
c     /lpsasum/dasum/,/scopy/dcopy/,/lpsdot/ddot/,
c     /.e0/.d0/
c
c     this subprogram is from the splp( ) package.  it performs the
c     tasks of updating the primal solution, edge weights, reduced
c     costs, and matrix decomposition.
c     it is the main part of the procedure (make move and update).
c
c     revised 821122-1100
c     revised yymmdd
c
c***see also  splp
c***routines called  lpiploc, la05bs, la05cs, pnnzrs, prwpge, lpsasum,
c                    scopy, lpsdot, splpdm, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   890606  removed unused common block la05ds.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c***end prologue  splpmu
      integer ibasis(*),imat(*),ibrc(lbm,2),ipr(*),iwr(*),ind(*),ibb(*)
      real             aij,alpha,anorm,costsc,erdnrm,dulnrm,eps,gamma,
     * gg,gq,one,rprnrm,rzj,scalr,theta,two,uu,wp,xlamda,rhsnrm,
     * zero,amat(*),basmat(*),csc(*),wr(*),rprim(*),ww(*),bu(*),bl(*),
     * rhs(*),erd(*),erp(*),rz(*),rg(*),costs(*),primal(*),duals(*),
     * colnrm(*),rcost,lpsasum,lpsdot
      logical singlr,redbas,pagepl,trans,zerolv,stpedg
c
c***first executable statement  splpmu
      zero=0.e0
      one=1.e0
      two=2.e0
      lpg=lmx-(nvars+4)
c
c     update the primal solution with a multiple of the search
c     direction.
      i=1
      n20002=mrelas
      go to 20003
20002 i=i+1
20003 if ((n20002-i).lt.0) go to 20004
      rprim(i)=rprim(i)-theta*ww(i)
      go to 20002
c
c     if ejected variable is leaving at an upper bound,  then
c     translate right hand side.
20004 if (.not.(ileave.lt.0)) go to 20006
      ibas=ibasis(abs(ileave))
      scalr=rprim(abs(ileave))
      assign 20009 to npr001
      go to 30001
20009 ibb(ibas)=abs(ibb(ibas))+1
c
c     if entering variable is restricted to its upper bound, translate
c     right hand side.  if the variable decreased from its upper
c     bound, a sign change is required in the translation.
20006 if (.not.(ienter.eq.ileave)) go to 20010
      ibas=ibasis(ienter)
      scalr=theta
      if (mod(ibb(ibas),2).eq.0) scalr=-scalr
      assign 20013 to npr001
      go to 30001
20013 ibb(ibas)=ibb(ibas)+1
      go to 20011
20010 ibas=ibasis(ienter)
c
c     if entering variable is decreasing from its upper bound,
c     complement its primal value.
      if (.not.(ind(ibas).eq.3.and.mod(ibb(ibas),2).eq.0)) go to 20014
      scalr=-(bu(ibas)-bl(ibas))
      if (ibas.le.nvars) scalr=scalr/csc(ibas)
      assign 20017 to npr001
      go to 30001
20017 theta=-scalr-theta
      ibb(ibas)=ibb(ibas)+1
20014 continue
      rprim(abs(ileave))=theta
      ibb(ibas)=-abs(ibb(ibas))
      i=ibasis(abs(ileave))
      ibb(i)=abs(ibb(i))
      if(primal(abs(ileave)+nvars).gt.zero) ibb(i)=ibb(i)+1
c
c     interchange column pointers to note exchange of columns.
20011 ibas=ibasis(ienter)
      ibasis(ienter)=ibasis(abs(ileave))
      ibasis(abs(ileave))=ibas
c
c     if variable was exchanged at a zero level, mark it so that
c     it can't be brought back in.  this is to help prevent cycling.
      if(zerolv) ibasis(ienter)=-abs(ibasis(ienter))
      rprnrm=max(rprnrm,lpsasum(mrelas,rprim,1))
      k=1
      n20018=mrelas
      go to 20019
20018 k=k+1
20019 if ((n20018-k).lt.0) go to 20020
c
c     see if variables that were classified as infeasible have now
c     become feasible.  this may required translating upper bounded
c     variables.
      if (.not.(primal(k+nvars).ne.zero .and.
     *          abs(rprim(k)).le.rprnrm*erp(k))) go to 20022
      if (.not.(primal(k+nvars).gt.zero)) go to 20025
      ibas=ibasis(k)
      scalr=-(bu(ibas)-bl(ibas))
      if(ibas.le.nvars)scalr=scalr/csc(ibas)
      assign 20028 to npr001
      go to 30001
20028 rprim(k)=-scalr
      rprnrm=rprnrm-scalr
20025 primal(k+nvars)=zero
20022 continue
      go to 20018
c
c     update reduced costs, edge weights, and matrix decomposition.
20020 if (.not.(ienter.ne.ileave)) go to 20029
c
c     the incoming variable is always classified as feasible.
      primal(abs(ileave)+nvars)=zero
c
      wp=ww(abs(ileave))
      gq=lpsdot(mrelas,ww,1,ww,1)+one
c
c     compute inverse (transpose) times search direction.
      trans=.true.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
c
c     update the matrix decomposition.  col. abs(ileave) is leaving.
c     the array duals(*) contains intermediate results for the
c     incoming column.
      call lpla05cs(basmat,ibrc,lbm,mrelas,ipr,iwr,duals,gg,uu,
     * abs(ileave))
      redbas=.false.
      if (.not.(gg.lt.zero)) go to 20032
c
c     redecompose basis matrix when an error return from
c     la05cs( ) is noted.  this will probably be due to
c     space being exhausted, gg=-7.
      call lpsplpdm(
     *mrelas,nvars,lmx,lbm,nredc,info,iopt,
     *ibasis,imat,ibrc,ipr,iwr,ind,ibb,
     *anorm,eps,uu,gg,
     *amat,basmat,csc,wr,
     *singlr,redbas)
      if (.not.(singlr)) go to 20035
      nerr=26
      call lpxermsg ('slatec', 'splpmu',
     +   'in splp, moved to a singular point.  this should not happen.',
     +   nerr, iopt)
      info=-nerr
      return
20035 continue
      go to 30002
20038 continue
20032 continue
c
c     if steepest edge pricing is used, update reduced costs
c     and edge weights.
      if (.not.(stpedg)) go to 20039
c
c     compute col. abs(ileave) of the new inverse (transpose) matrix
c     here abs(ileave) points to the ejected column.
c     use erd(*) for temp. storage.
      call lpscopy(mrelas,zero,0,erd,1)
      erd(abs(ileave))=one
      trans=.true.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,erd,trans)
c
c     compute updated dual variables in duals(*).
      assign 20042 to npr003
      go to 30003
c
c     compute the dot product of col. j of the new inverse (transpose)
c     with each non-basic column.  also compute the dot product of the
c     inverse (transpose) of non-updated matrix (times) the
c     search direction with each non-basic column.
c     recompute reduced costs.
20042 pagepl=.true.
      call lpscopy(nvars+mrelas,zero,0,rz,1)
      nnegrc=0
      j=jstrt
20043 if (.not.(ibb(j).le.0)) go to 20045
      pagepl=.true.
      rg(j)=one
      go to 20046
c
c     nonbasic independent variables (column in sparse matrix storage)
20045 if (.not.(j.le.nvars)) go to 20048
      rzj=costs(j)*costsc
      alpha=zero
      gamma=zero
c
c     compute the dot product of the sparse matrix nonbasic columns
c     with three vectors involved in the updating step.
      if (.not.(j.eq.1)) go to 20051
      ilow=nvars+5
      go to 20052
20051 ilow=imat(j+3)+1
20052 if (.not.(pagepl)) go to 20054
      il1=lpiploc(ilow,amat,imat)
      if (.not.(il1.ge.lmx-1)) go to 20057
      ilow=ilow+2
      il1=lpiploc(ilow,amat,imat)
20057 continue
      ipage=abs(imat(lmx-1))
      go to 20055
20054 il1=ihi+1
20055 ihi=imat(j+4)-(ilow-il1)
20060 iu1=min(lmx-2,ihi)
      if (.not.(il1.gt.iu1)) go to 20062
      go to 20061
20062 continue
      do 10 i=il1,iu1
      rzj=rzj-amat(i)*duals(imat(i))
      alpha=alpha+amat(i)*erd(imat(i))
      gamma=gamma+amat(i)*ww(imat(i))
10    continue
      if (.not.(ihi.le.lmx-2)) go to 20065
      go to 20061
20065 continue
      ipage=ipage+1
      key=1
      call lpprwpge(key,ipage,lpg,amat,imat)
      il1=nvars+5
      ihi=ihi-lpg
      go to 20060
20061 pagepl=ihi.eq.(lmx-2)
      rz(j)=rzj*csc(j)
      alpha=alpha*csc(j)
      gamma=gamma*csc(j)
      rg(j)=max(rg(j)-two*alpha*gamma+alpha**2*gq,one+alpha**2)
c
c     nonbasic dependent variables (columns defined implicitly)
      go to 20049
20048 pagepl=.true.
      scalr=-one
      if(ind(j).eq.2) scalr=one
      i=j-nvars
      alpha=scalr*erd(i)
      rz(j)=-scalr*duals(i)
      gamma=scalr*ww(i)
      rg(j)=max(rg(j)-two*alpha*gamma+alpha**2*gq,one+alpha**2)
20049 continue
20046 continue
c
      rcost=rz(j)
      if (mod(ibb(j),2).eq.0) rcost=-rcost
      if (.not.(ind(j).eq.3)) go to 20068
      if(bu(j).eq.bl(j)) rcost=zero
20068 continue
      if (ind(j).eq.4) rcost=-abs(rcost)
      cnorm=one
      if (j.le.nvars) cnorm=colnrm(j)
      if (rcost+erdnrm*dulnrm*cnorm.lt.zero) nnegrc=nnegrc+1
      j=mod(j,mrelas+nvars)+1
      if (.not.(nnegrc.ge.npp .or. j.eq.jstrt)) go to 20071
      go to 20044
20071 continue
      go to 20043
20044 jstrt=j
c
c     update the edge weight for the ejected variable.
      rg(abs(ibasis(ienter)))= gq/wp**2
c
c     if minimum reduced cost (dantzig) pricing is used,
c     calculate the new reduced costs.
      go to 20040
c
c     compute the updated duals in duals(*).
20039 assign 20074 to npr003
      go to 30003
20074 call lpscopy(nvars+mrelas,zero,0,rz,1)
      nnegrc=0
      j=jstrt
      pagepl=.true.
c
20075 if (.not.(ibb(j).le.0)) go to 20077
      pagepl=.true.
      go to 20078
c
c     nonbasic independent variable (column in sparse matrix storage)
20077 if (.not.(j.le.nvars)) go to 20080
      rz(j)=costs(j)*costsc
      if (.not.(j.eq.1)) go to 20083
      ilow=nvars+5
      go to 20084
20083 ilow=imat(j+3)+1
20084 continue
      if (.not.(pagepl)) go to 20086
      il1=lpiploc(ilow,amat,imat)
      if (.not.(il1.ge.lmx-1)) go to 20089
      ilow=ilow+2
      il1=lpiploc(ilow,amat,imat)
20089 continue
      ipage=abs(imat(lmx-1))
      go to 20087
20086 il1=ihi+1
20087 continue
      ihi=imat(j+4)-(ilow-il1)
20092 iu1=min(lmx-2,ihi)
      if (.not.(iu1.ge.il1 .and.mod(iu1-il1,2).eq.0)) go to 20094
      rz(j)=rz(j)-amat(il1)*duals(imat(il1))
      il1=il1+1
20094 continue
      if (.not.(il1.gt.iu1)) go to 20097
      go to 20093
20097 continue
c
c     unroll the dot product loop to a depth of two.  (this is done
c     for increased efficiency).
      do 40 i=il1,iu1,2
      rz(j)=rz(j)-amat(i)*duals(imat(i))-amat(i+1)*duals(imat(i+1))
40    continue
      if (.not.(ihi.le.lmx-2)) go to 20100
      go to 20093
20100 continue
      ipage=ipage+1
      key=1
      call lpprwpge(key,ipage,lpg,amat,imat)
      il1=nvars+5
      ihi=ihi-lpg
      go to 20092
20093 pagepl=ihi.eq.(lmx-2)
      rz(j)=rz(j)*csc(j)
c
c     nonbasic dependent variables (columns defined implicitly)
      go to 20081
20080 pagepl=.true.
      scalr=-one
      if(ind(j).eq.2) scalr=one
      i=j-nvars
      rz(j)=-scalr*duals(i)
20081 continue
20078 continue
c
      rcost=rz(j)
      if (mod(ibb(j),2).eq.0) rcost=-rcost
      if (.not.(ind(j).eq.3)) go to 20103
      if(bu(j).eq.bl(j)) rcost=zero
20103 continue
      if (ind(j).eq.4) rcost=-abs(rcost)
      cnorm=one
      if (j.le.nvars) cnorm=colnrm(j)
      if (rcost+erdnrm*dulnrm*cnorm.lt.zero) nnegrc=nnegrc+1
      j=mod(j,mrelas+nvars)+1
      if (.not.(nnegrc.ge.npp .or. j.eq.jstrt)) go to 20106
      go to 20076
20106 continue
      go to 20075
20076 jstrt=j
20040 continue
      go to 20030
c
c     this is necessary only for printing of intermediate results.
20029 assign 20109 to npr003
      go to 30003
20109 continue
20030 return
c     procedure (translate right hand side)
c
c     perform the translation on the right-hand side.
30001 if (.not.(ibas.le.nvars)) go to 20110
      i=0
20113 call lppnnzrs(i,aij,iplace,amat,imat,ibas)
      if (.not.(i.le.0)) go to 20115
      go to 20114
20115 continue
      rhs(i)=rhs(i)-scalr*aij*csc(ibas)
      go to 20113
20114 go to 20111
20110 i=ibas-nvars
      if (.not.(ind(ibas).eq.2)) go to 20118
      rhs(i)=rhs(i)-scalr
      go to 20119
20118 rhs(i)=rhs(i)+scalr
20119 continue
20111 continue
      rhsnrm=max(rhsnrm,lpsasum(mrelas,rhs,1))
      go to npr001, (20009,20013,20017,20028)
c     procedure (compute new primal)
c
c     copy rhs into ww(*), solve system.
30002 call lpscopy(mrelas,rhs,1,ww,1)
      trans = .false.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,ww,trans)
      call lpscopy(mrelas,ww,1,rprim,1)
      rprnrm=lpsasum(mrelas,rprim,1)
      go to 20038
c     procedure (compute new duals)
c
c     solve for dual variables. first copy costs into duals(*).
30003 i=1
      n20121=mrelas
      go to 20122
20121 i=i+1
20122 if ((n20121-i).lt.0) go to 20123
      j=ibasis(i)
      if (.not.(j.le.nvars)) go to 20125
      duals(i)=costsc*costs(j)*csc(j) + xlamda*primal(i+nvars)
      go to 20126
20125 duals(i)=xlamda*primal(i+nvars)
20126 continue
      go to 20121
c
20123 trans=.true.
      call lpla05bs(basmat,ibrc,lbm,mrelas,ipr,iwr,wr,gg,duals,trans)
      dulnrm=lpsasum(mrelas,duals,1)
      go to npr003, (20042,20074,20109)
      end subroutine lpsplpmu
      subroutine lpsplpup (lpusrmat, mrelas, nvars, prgopt, dattrv,
     *    bl, bu,
     +   ind, info, amat, imat, sizeup, asmall, abig)
c***begin prologue  splpup
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (splpup-s, dplpup-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/.
c
c     revised 810613-1130
c     revised yymmdd-hhmm
c
c     this subroutine lpcollects information about the bounds and matrix
c     from the user.  it is part of the splp( ) package.
c
c***see also  splp
c***routines called  pchngs, pnnzrs, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  corrected references to xerrwv.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891009  removed unreferenced variables.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls, changed do-it-yourself
c           do loops to do loops.  (rwc)
c   900602  get rid of assigned gotos.  (rwc)
c***end prologue  splpup
      real             abig,aij,amat(*),amn,amx,asmall,bl(*),
     * bu(*),dattrv(*),prgopt(*),xval,zero
      integer iflag(10),imat(*),ind(*)
      logical sizeup,first
      character*8 xern1, xern2
      character*16 xern3, xern4
c
c***first executable statement  splpup
      zero = 0.e0
c
c     check user-supplied bounds
c
c     check that ind(*) values are 1,2,3 or 4.
c     also check consistency of upper and lower bounds.
c
      do 10 j=1,nvars
         if (ind(j).lt.1 .or. ind(j).gt.4) then
            write (xern1, '(i8)') j
            call lpxermsg ('slatec', 'splpup',
     *         'in splp, independent variable = ' // xern1 //
     *         ' is not defined.', 10, 1)
            info = -10
            return
         endif
c
         if (ind(j).eq.3) then
            if (bl(j).gt.bu(j)) then
               write (xern1, '(i8)') j
               write (xern3, '(1pe15.6)') bl(j)
               write (xern4, '(1pe15.6)') bu(j)
               call lpxermsg ('slatec', 'splpup',
     *            'in splp, lower bound = ' // xern3 //
     *            ' and upper bound = ' // xern4 //
     *            ' for independent variable = ' // xern1 //
     *            ' are not consistent.', 11, 1)
               return
            endif
         endif
   10 continue
c
      do 20 i=nvars+1,nvars+mrelas
         if (ind(i).lt.1 .or. ind(i).gt.4) then
            write (xern1, '(i8)') i-nvars
            call lpxermsg ('slatec', 'splpup',
     *         'in splp, dependent variable = ' // xern1 //
     *         ' is not defined.', 12, 1)
            info = -12
            return
         endif
c
         if (ind(i).eq.3) then
            if (bl(i).gt.bu(i)) then
               write (xern1, '(i8)') i
               write (xern3, '(1pe15.6)') bl(i)
               write (xern4, '(1pe15.6)') bu(i)
               call lpxermsg ('slatec', 'splpup',
     *            'in splp, lower bound = ' // xern3 //
     *            ' and upper bound = ' // xern4 //
     *            ' for dependant variable = ' // xern1 //
     *            ' are not consistent.',13,1)
               info = -13
               return
            endif
         endif
   20 continue
c
c     get updates or data for matrix from the user
c
c     get the elements of the matrix from the user.  it will be stored
c     by columns using the sparse storage codes of rj hanson and
c     ja wisniewski.
c
      iflag(1) = 1
c
c     keep accepting elements until the user is finished giving them.
c     limit this loop to 2*nvars*mrelas iterations.
c
      itmax = 2*nvars*mrelas+1
      itcnt = 0
      first = .true.
c
c     check on the iteration count.
c
   30 itcnt = itcnt+1
      if (itcnt.gt.itmax) then
         call lpxermsg ('slatec', 'splpup',
     +      'in splp, more than 2*nvars*mrelas iterations defining ' //
     +      'or updating matrix data.', 7, 1)
         info = -7
         return
      endif
c
      aij = zero
      call lpusrmat(i,j,aij,indcat,prgopt,dattrv,iflag)
      if (iflag(1).eq.1) then
         iflag(1) = 2
         go to 30
      endif
c
c     check to see that the subscripts i and j are valid.
c
      if (i.lt.1 .or. i.gt.mrelas .or. j.lt.1 .or. j.gt.nvars) then
c
c        check on size of matrix data
c        record the largest and smallest(in magnitude) nonzero elements.
c
         if (iflag(1).eq.3) then
            if (sizeup .and. abs(aij).ne.zero) then
               if (first) then
                  amx = abs(aij)
                  amn = abs(aij)
                  first = .false.
               elseif (abs(aij).gt.amx) then
                  amx = abs(aij)
               elseif (abs(aij).lt.amn) then
                  amn = abs(aij)
               endif
            endif
            go to 40
         endif
c
         write (xern1, '(i8)') i
         write (xern2, '(i8)') j
         call lpxermsg ('slatec', 'splpup',
     *      'in splp, row index = ' // xern1 // ' or column index = '
     *      // xern2 // ' is out of range.', 8, 1)
         info = -8
         return
      endif
c
c     if indcat=0 then set a(i,j)=aij.
c     if indcat=1 then accumulate element, a(i,j)=a(i,j)+aij.
c
      if (indcat.eq.0) then
         call lppchngs(i,aij,iplace,amat,imat,j)
      elseif (indcat.eq.1) then
         index = -(i-1)
         call lppnnzrs(index,xval,iplace,amat,imat,j)
         if (index.eq.i) aij=aij+xval
         call lppchngs(i,aij,iplace,amat,imat,j)
      else
         write (xern1, '(i8)') indcat
         call lpxermsg ('slatec', 'splpup',
     *      'in splp, indication flag = ' // xern1 //
     *      ' for matrix data must be either 0 or 1.', 9, 1)
         info = -9
         return
      endif
c
c     check on size of matrix data
c     record the largest and smallest(in magnitude) nonzero elements.
c
      if (sizeup .and. abs(aij).ne.zero) then
         if (first) then
            amx = abs(aij)
            amn = abs(aij)
            first = .false.
         elseif (abs(aij).gt.amx) then
            amx = abs(aij)
         elseif (abs(aij).lt.amn) then
            amn = abs(aij)
         endif
      endif
      if (iflag(1).ne.3) go to 30
c
   40 if (sizeup .and. .not. first) then
         if (amn.lt.asmall .or. amx.gt.abig) then
            call lpxermsg ('slatec', 'splpup',
     +         'in splp, a matrix element''s size is out of the ' //
     +         'specified range.', 22, 1)
            info = -22
            return
         endif
      endif
      return
      end subroutine lpsplpup
      subroutine lpspopt (prgopt, mrelas, nvars, info, csc, ibasis, ropt
     +   ,intopt, lopt)
c***begin prologue  spopt
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (spopt-s, dpopt-d)
c***author  (unknown)
c***description
c
c     the editing required to convert this subroutine lpfrom single to
c     double precision involves the following character string changes.
c
c     use an editing command (change) /string-1/(to)string-2/.
c     /real (12 blanks)/double precision/,
c     /lpr1mach/d1mach/,/e0/d0/
c
c     revised 821122-1045
c     revised yymmdd-hhmm
c
c     this subroutine lpprocesses the option vector, prgopt(*),
c     and validates any modified data.
c
c***see also  splp
c***routines called  lpr1mach, xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   890605  removed unreferenced labels.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900315  calls to xerror changed to calls to xermsg.  (thj)
c   900328  added type section.  (wrb)
c***end prologue  spopt
      real             abig,asmall,costsc,csc(*),eps,one,prgopt(*),
     * ropt(07),tolls,tune,zero,lpr1mach,tolabs
      integer ibasis(*),intopt(08)
      logical contin,usrbas,sizeup,savedt,colscp,cstscp,minprb,
     * stpedg,lopt(8)
c
c***first executable statement  spopt
      iopt=1
      zero=0.e0
      one=1.e0
      go to 30001
20002 continue
      go to 30002
c
20003 lopt(1)=contin
      lopt(2)=usrbas
      lopt(3)=sizeup
      lopt(4)=savedt
      lopt(5)=colscp
      lopt(6)=cstscp
      lopt(7)=minprb
      lopt(8)=stpedg
c
      intopt(1)=idg
      intopt(2)=ipagef
      intopt(3)=isave
      intopt(4)=mxitlp
      intopt(5)=kprint
      intopt(6)=itbrc
      intopt(7)=npp
      intopt(8)=lprg
c
      ropt(1)=eps
      ropt(2)=asmall
      ropt(3)=abig
      ropt(4)=costsc
      ropt(5)=tolls
c      ropt(5)=1.0e-3
      ropt(6)=tune
      ropt(7)=tolabs
      return
c
c
c     procedure (initialize parameters and process user options)
30001 contin = .false.
      usrbas = .false.
      sizeup = .false.
      savedt = .false.
      colscp = .false.
      cstscp = .false.
      minprb = .true.
      stpedg = .true.
c
c     get the machine rel. floating point accuracy value from the
c     library subprogram, lpr1mach( ).
      eps=lpr1mach(4)
      tolls=lpr1mach(4)
c      tolls=1.0e-3
      tune=one
      tolabs=zero
c
c     define nominal file numbers for matrix pages and data saving.
      ipagef=1
      isave=2
      itbrc=10
      mxitlp=3*(nvars+mrelas)
      kprint=0
      idg=-4
      npp=nvars
      lprg=0
c
      last = 1
      iadbig=10000
      ictmax=1000
      ictopt= 0
20004 next=prgopt(last)
      if (.not.(next.le.0 .or. next.gt.iadbig)) go to 20006
c
c     the checks for small or large values of next are to prevent
c     working with undefined data.
      nerr=14
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, the user option array has undefined data.', nerr,
     +   iopt)
      info=-nerr
      return
20006 if (.not.(next.eq.1)) go to 10001
      go to 20005
10001 if (.not.(ictopt.gt.ictmax)) go to 10002
      nerr=15
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, option array processing is cycling.', nerr, iopt)
      info=-nerr
      return
10002 continue
      key = prgopt(last+1)
c
c     if key = 50, this is to be a maximization problem
c     instead of a minimization problem.
      if (.not.(key.eq.50)) go to 20010
      minprb = prgopt(last+2).eq.zero
      lds=3
      go to 20009
20010 continue
c
c     if key = 51, the level of output is being modified.
c     kprint = 0, no output
c            = 1, summary output
c            = 2, lots of output
c            = 3, even more output
      if (.not.(key.eq.51)) go to 20013
      kprint=prgopt(last+2)
      lds=3
      go to 20009
20013 continue
c
c     if key = 52, redefine the format and precision used
c     in the output.
      if (.not.(key.eq.52)) go to 20016
      if (prgopt(last+2).ne.zero) idg=prgopt(last+3)
      lds=4
      go to 20009
20016 continue
c
c     if key = 53, the allotted space for the sparse matrix
c     storage and/or sparse equation solving has been changed.
c     (processed in splp(). this is to compute the length of prgopt(*).)
      if (.not.(key.eq.53)) go to 20019
      lds=5
      go to 20009
20019 continue
c
c     if key = 54, redefine the file number where the pages
c     for the sparse matrix are stored.
      if (.not.(key.eq.54)) go to 20022
      if(prgopt(last+2).ne.zero) ipagef = prgopt(last+3)
      lds=4
      go to 20009
20022 continue
c
c     if key = 55,  a continuation for a problem may be requested.
      if (.not.(key .eq. 55)) go to 20025
      contin = prgopt(last+2).ne.zero
      lds=3
      go to 20009
20025 continue
c
c     if key = 56, redefine the file number where the saved data
c     will be stored.
      if (.not.(key.eq.56)) go to 20028
      if(prgopt(last+2).ne.zero) isave = prgopt(last+3)
      lds=4
      go to 20009
20028 continue
c
c     if key = 57, save data (on external file)  at mxitlp iterations or
c     the optimum, whichever comes first.
      if (.not.(key.eq.57)) go to 20031
      savedt=prgopt(last+2).ne.zero
      lds=3
      go to 20009
20031 continue
c
c     if key = 58,  see if problem is to run only a given
c     number of iterations.
      if (.not.(key.eq.58)) go to 20034
      if (prgopt(last+2).ne.zero) mxitlp = prgopt(last+3)
      lds=4
      go to 20009
20034 continue
c
c     if key = 59,  see if user provides the basis indices.
      if (.not.(key .eq. 59)) go to 20037
      usrbas = prgopt(last+2) .ne. zero
      if (.not.(usrbas)) go to 20040
      i=1
      n20043=mrelas
      go to 20044
20043 i=i+1
20044 if ((n20043-i).lt.0) go to 20045
      ibasis(i) = prgopt(last+2+i)
      go to 20043
20045 continue
20040 continue
      lds=mrelas+3
      go to 20009
20037 continue
c
c     if key = 60,  see if user has provided scaling of columns.
      if (.not.(key .eq. 60)) go to 20047
      colscp = prgopt(last+2).ne.zero
      if (.not.(colscp)) go to 20050
      j=1
      n20053=nvars
      go to 20054
20053 j=j+1
20054 if ((n20053-j).lt.0) go to 20055
      csc(j)=abs(prgopt(last+2+j))
      go to 20053
20055 continue
20050 continue
      lds=nvars+3
      go to 20009
20047 continue
c
c     if key = 61,  see if user has provided scaling of costs.
      if (.not.(key .eq. 61)) go to 20057
      cstscp = prgopt(last+2).ne.zero
      if (cstscp) costsc = prgopt(last+3)
      lds=4
      go to 20009
20057 continue
c
c     if key = 62,  see if size parameters are provided with the data.
c     these will be checked against the matrix element sizes later.
      if (.not.(key .eq. 62)) go to 20060
      sizeup = prgopt(last+2).ne.zero
      if (.not.(sizeup)) go to 20063
      asmall = prgopt(last+3)
      abig = prgopt(last+4)
20063 continue
      lds=5
      go to 20009
20060 continue
c
c     if key = 63, see if tolerance for linear system residual error is
c     provided.
      if (.not.(key .eq. 63)) go to 20066
      if (prgopt(last+2).ne.zero) tolls = max(eps,prgopt(last+3))
      lds=4
      go to 20009
20066 continue
c
c     if key = 64,  see if minimum reduced cost or steepest edge
c     descent is to be used for selecting variables to enter basis.
      if (.not.(key.eq.64)) go to 20069
      stpedg = prgopt(last+2).eq.zero
      lds=3
      go to 20009
20069 continue
c
c     if key = 65, set the number of iterations between recalculating
c     the error in the primal solution.
      if (.not.(key.eq.65)) go to 20072
      if (prgopt(last+2).ne.zero) itbrc=max(one,prgopt(last+3))
      lds=4
      go to 20009
20072 continue
c
c     if key = 66, set the number of negative reduced costs to be found
c     in the partial pricing strategy.
      if (.not.(key.eq.66)) go to 20075
      if (.not.(prgopt(last+2).ne.zero)) go to 20078
      npp=max(prgopt(last+3),one)
      npp=min(npp,nvars)
20078 continue
      lds=4
      go to 20009
20075 continue
c     if key = 67, change the tuning parameter to apply to the error
c     estimates for the primal and dual systems.
      if (.not.(key.eq.67)) go to 20081
      if (.not.(prgopt(last+2).ne.zero)) go to 20084
      tune=abs(prgopt(last+3))
20084 continue
      lds=4
      go to 20009
20081 continue
      if (.not.(key.eq.68)) go to 20087
      lds=6
      go to 20009
20087 continue
c
c     reset the absolute tolerance to be used on the feasibility
c     decision provided the relative error test failed.
      if (.not.(key.eq.69)) go to 20090
      if(prgopt(last+2).ne.zero)tolabs=prgopt(last+3)
      lds=4
      go to 20009
20090 continue
      continue
c
20009 ictopt = ictopt+1
      last = next
      lprg=lprg+lds
      go to 20004
20005 continue
      go to 20002
c
c     procedure (validate optionally modified data)
c
c     if user has defined the basis, check for validity of indices.
30002 if (.not.(usrbas)) go to 20093
      i=1
      n20096=mrelas
      go to 20097
20096 i=i+1
20097 if ((n20096-i).lt.0) go to 20098
      itest=ibasis(i)
      if (.not.(itest.le.0 .or.itest.gt.(nvars+mrelas))) go to 20100
      nerr=16
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, an index of user-supplied basis is out of range.',
     +   nerr, iopt)
      info=-nerr
      return
20100 continue
      go to 20096
20098 continue
20093 continue
c
c     if user has provided size parameters, make sure they are ordered
c     and positive.
      if (.not.(sizeup)) go to 20103
      if (.not.(asmall.le.zero .or. abig.lt.asmall)) go to 20106
      nerr=17
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, size parameters for matrix must be smallest and ' //
     +   'largest magnitudes of nonzero entries.', nerr, iopt)
      info=-nerr
      return
20106 continue
20103 continue
c
c     the number of iterations of rev. simplex steps must be positive.
      if (.not.(mxitlp.le.0)) go to 20109
      nerr=18
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, the number of revised simplex steps between ' //
     +   'check-points must be positive.', nerr, iopt)
      info=-nerr
      return
20109 continue
c
c     check that save and page file numbers are defined and not equal.
      if (.not.(isave.le.0.or.ipagef.le.0.or.(isave.eq.ipagef))) go to 2
     *0112
      nerr=19
      call lpxermsg ('slatec', 'spopt',
     +   'in splp, file numbers for saved data and matrix pages ' //
     +   'must be positive and not equal.', nerr, iopt)
      info=-nerr
      return
20112 continue
      continue
      go to 20003
      end subroutine lpspopt
      subroutine lpsreadp (ipage, list, rlist, lpage, irec)
c***begin prologue  sreadp
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (sreadp-s, dreadp-d)
c***author  (unknown)
c***description
c
c     read record number irecn, of length lpg, from unit
c     number ipagef into the storage array, list(*).
c     read record  irecn+1, of length lpg, from unit number
c     ipagef into the storage array rlist(*).
c
c     to convert this program unit to double precision change
c     /real (12 blanks)/ to /double precision/.
c
c***see also  splp
c***routines called  xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  sreadp
      integer list(*)
      real             rlist(*)
      character*8 xern1, xern2
c***first executable statement  sreadp
      ipagef=ipage
      lpg   =lpage
      irecn=irec
      read(ipagef,rec=irecn,err=100)(list(i),i=1,lpg)
      read(ipagef,rec=irecn+1,err=100)(rlist(i),i=1,lpg)
      return
c
  100 write (xern1, '(i8)') lpg
      write (xern2, '(i8)') irecn
      call lpxermsg ('slatec', 'sreadp', 'in splp, lpg = ' // xern1 //
     *   ' irecn = ' // xern2, 100, 1)
      return
      end subroutine lpsreadp
      subroutine lpsvout (n, sx, ifmt, idigit)
c***begin prologue  svout
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (svout-s, dvout-d)
c***author  (unknown)
c***description
c
c     single precision vector output routine.
c
c  input..
c
c  n,sx(*) print the single precision array sx(i),i=1,...,n, on
c          output unit lout. the heading in the fortran format
c          statement ifmt(*), described below, is printed as a first
c          step. the components sx(i) are indexed, on output,
c          in a pleasant format.
c  ifmt(*) a fortran format statement. this is printed on output
c          unit lout with the variable format fortran statement
c                write(lout,ifmt)
c  idigit  print at least abs(idigit) decimal digits per number.
c          the subprogram will choose that integer 4,6,10 or 14
c          which will print at least abs(idigit) number of
c          places.  if idigit.lt.0, 72 printing columns are utilized
c          to write each line of output of the array sx(*). (this
c          can be used on most time-sharing terminals). if
c          idigit.ge.0, 133 printing columns are utilized. (this can
c          be used on most line printers).
c
c  example..
c
c  print an array called (costs of purchases) of length 100 showing
c  6 decimal digits per number. the user is running on a time-sharing
c  system with a 72 column output device.
c
c     dimension costs(100)
c     n = 100
c     idigit = -6
c     call lpsvout(n,costs,'(''1costs of purchases'')',idigit)
c
c***see also  splp
c***routines called  lpi1mach
c***revision history  (yymmdd)
c   811215  date written
c   890531  changed all specific intrinsics to generic.  (wrb)
c   891107  added comma after 1p edit descriptor in format
c           statements.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  svout
      dimension sx(*)
      character ifmt*(*)
c
c     get the unit number where output will be written.
c***first executable statement  svout
      j=2
      lout=lpi1mach(j)
      write(lout,ifmt)
      if(n.le.0) return
      ndigit = idigit
      if(idigit.eq.0) ndigit = 4
      if(idigit.ge.0) go to 80
c
      ndigit = -idigit
      if(ndigit.gt.4) go to 20
c
      do 10 k1=1,n,5
      k2 = min(n,k1+4)
      write(lout,1000) k1,k2,(sx(i),i=k1,k2)
   10 continue
      return
c
   20 continue
      if(ndigit.gt.6) go to 40
c
      do 30 k1=1,n,4
      k2 = min(n,k1+3)
      write(lout,1001) k1,k2,(sx(i),i=k1,k2)
   30 continue
      return
c
   40 continue
      if(ndigit.gt.10) go to 60
c
      do 50 k1=1,n,3
      k2=min(n,k1+2)
      write(lout,1002) k1,k2,(sx(i),i=k1,k2)
   50 continue
      return
c
   60 continue
      do 70 k1=1,n,2
      k2 = min(n,k1+1)
      write(lout,1003) k1,k2,(sx(i),i=k1,k2)
   70 continue
      return
c
   80 continue
      if(ndigit.gt.4) go to 100
c
      do 90 k1=1,n,10
      k2 = min(n,k1+9)
      write(lout,1000) k1,k2,(sx(i),i=k1,k2)
   90 continue
      return
c
  100 continue
      if(ndigit.gt.6) go to 120
c
      do 110 k1=1,n,8
      k2 = min(n,k1+7)
      write(lout,1001) k1,k2,(sx(i),i=k1,k2)
  110 continue
      return
c
  120 continue
      if(ndigit.gt.10) go to 140
c
      do 130 k1=1,n,6
      k2 = min(n,k1+5)
      write(lout,1002) k1,k2,(sx(i),i=k1,k2)
  130 continue
      return
c
  140 continue
      do 150 k1=1,n,5
      k2 = min(n,k1+4)
      write(lout,1003) k1,k2,(sx(i),i=k1,k2)
  150 continue
      return
 1000 format(1x,i4,' - ',i4,1p,10e12.3)
 1001 format(1x,i4,' - ',i4,1x,1p,8e14.5)
 1002 format(1x,i4,' - ',i4,1x,1p,6e18.9)
 1003 format(1x,i4,' - ',i4,1x,1p,5e24.13)
      end subroutine lpsvout
      subroutine lpswritp (ipage, list, rlist, lpage, irec)
c***begin prologue  swritp
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (swritp-s, dwritp-d)
c***author  (unknown)
c***description
c
c     write record number irecn, of length lpg, from storage
c     array list(*) onto unit number ipagef.
c     write record number irecn+1, of length lpg, onto unit
c     number ipagef from the storage array rlist(*).
c
c     to change this program unit to double precision change
c     /real (12 blanks)/ to /double precision/.
c
c***see also  splp
c***routines called  xermsg
c***revision history  (yymmdd)
c   811215  date written
c   890605  corrected references to xerrwv.  (wrb)
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c   900510  convert xerrwv calls to xermsg calls.  (rwc)
c***end prologue  swritp
      integer list(*)
      real             rlist(*)
      character*8 xern1, xern2
c***first executable statement  swritp
      ipagef=ipage
      lpg   =lpage
      irecn =irec
      write(ipagef,rec=irecn,err=100)(list(i),i=1,lpg)
      write(ipagef,rec=irecn+1,err=100)(rlist(i),i=1,lpg)
      return
c
  100 write (xern1, '(i8)') lpg
      write (xern2, '(i8)') irecn
      call lpxermsg ('slatec', 'swritp', 'in splp, lgp = ' // xern1 //
     *   ' irecn = ' // xern2, 100, 1)
      return
      end subroutine lpswritp
      subroutine lpxercnt (librar, subrou, messg, nerr, level, kontrl)
c***begin prologue  xercnt
c***subsidiary
c***purpose  allow user control over handling of errors.
c***library   slatec (xerror)
c***category  r3c
c***type      all (xercnt-a)
c***keywords  error, xerror
c***author  jones, r. e., (snla)
c***description
c
c     abstract
c        allows user control over handling of individual errors.
c        just after each message is recorded, but before it is
c        processed any further (i.e., before it is printed or
c        a decision to abort is made), a call lpis made to xercnt.
c        if the user has provided his own version of xercnt, he
c        can then override the value of kontrol used in processing
c        this message by redefining its value.
c        kontrl may be set to any value from -2 to 2.
c        the meanings for kontrl are the same as in xsetf, except
c        that the value of kontrl changes only for this message.
c        if kontrl is set to a value outside the range from -2 to 2,
c        it will be moved back into that range.
c
c     description of parameters
c
c      --input--
c        librar - the library that the routine is in.
c        subrou - the subroutine lpthat xermsg is being called from
c        messg  - the first 20 characters of the error message.
c        nerr   - same as in the call lpto xermsg.
c        level  - same as in the call lpto xermsg.
c        kontrl - the current value of the control flag as set
c                 by a call lpto xsetf.
c
c      --output--
c        kontrl - the new value of kontrl.  if kontrl is not
c                 defined, it will remain at its original value.
c                 this changed value of control affects only
c                 the current occurrence of the current message.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  (none)
c***revision history  (yymmdd)
c   790801  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900206  routine changed from user-callable to subsidiary.  (wrb)
c   900510  changed calling sequence to include library and subroutine lp
c           names, changed routine name from xerctl to xercnt.  (rwc)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xercnt
      character*(*) librar, subrou, messg
c***first executable statement  xercnt
      return
      end subroutine lpxercnt
      subroutine lpxerhlt (messg)
c***begin prologue  xerhlt
c***subsidiary
c***purpose  abort program execution and print error message.
c***library   slatec (xerror)
c***category  r3c
c***type      all (xerhlt-a)
c***keywords  abort program execution, error, xerror
c***author  jones, r. e., (snla)
c***description
c
c     abstract
c        ***note*** machine dependent routine
c        xerhlt aborts the execution of the program.
c        the error message causing the abort is given in the calling
c        sequence, in case one needs it for printing on a dayfile,
c        for example.
c
c     description of parameters
c        messg is as in xermsg.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  (none)
c***revision history  (yymmdd)
c   790801  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900206  routine changed from user-callable to subsidiary.  (wrb)
c   900510  changed calling sequence to delete length of character
c           and changed routine name from xerabt to xerhlt.  (rwc)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xerhlt
      character*(*) messg
c***first executable statement  xerhlt
      stop
      end subroutine
      subroutine lpxermsg (librar, subrou, messg, nerr, level)
c***begin prologue  xermsg
c***purpose  process error messages for slatec and other libraries.
c***library   slatec (xerror)
c***category  r3c
c***type      all (xermsg-a)
c***keywords  error message, xerror
c***author  fong, kirby, (nmfecc at llnl)
c***description
c
c   xermsg processes a diagnostic message in a manner determined by the
c   value of level and the current value of the library error control
c   flag, kontrl.  see subroutine lpxsetf for details.
c
c    librar   a character constant (or character variable) with the name
c             of the library.  this will be 'slatec' for the slatec
c             common math library.  the error handling package is
c             general enough to be used by many libraries
c             simultaneously, so it is desirable for the routine that
c             detects and reports an error to identify the library name
c             as well as the routine name.
c
c    subrou   a character constant (or character variable) with the name
c             of the routine that detected the error.  usually it is the
c             name of the routine that is calling xermsg.  there are
c             some instances where a user callable library routine calls
c             lower level subsidiary routines where the error is
c             detected.  in such cases it may be more informative to
c             supply the name of the routine the user called rather than
c             the name of the subsidiary routine that detected the
c             error.
c
c    messg    a character constant (or character variable) with the text
c             of the error or warning message.  in the example below,
c             the message is a character constant that contains a
c             generic message.
c
c                   call lpxermsg ('slatec', 'mmpy',
c                  *'the order of the matrix exceeds the row dimension',
c                  *3, 1)
c
c             it is possible (and is sometimes desirable) to generate a
c             specific message--e.g., one that contains actual numeric
c             values.  specific numeric values can be converted into
c             character strings using formatted write statements into
c             character variables.  this is called standard fortran
c             internal file i/o and is exemplified in the first three
c             lines of the following example.  you can also catenate
c             substrings of characters to construct the error message.
c             here is an example showing the use of both writing to
c             an internal file and catenating character strings.
c
c                   character*5 charn, charl
c                   write (charn,10) n
c                   write (charl,10) lda
c                10 format(i5)
c                   call lpxermsg ('slatec', 'mmpy', 'the order'//charn//
c                  *   ' of the matrix exceeds its row dimension of'//
c                  *   charl, 3, 1)
c
c             there are two subtleties worth mentioning.  one is that
c             the // for character catenation is used to construct the
c             error message so that no single character constant is
c             continued to the next line.  this avoids confusion as to
c             whether there are trailing blanks at the end of the line.
c             the second is that by catenating the parts of the message
c             as an actual argument rather than encoding the entire
c             message into one large character variable, we avoid
c             having to know how long the message will be in order to
c             declare an adequate length for that large character
c             variable.  xermsg calls xerprn to print the message using
c             multiple lines if necessary.  if the message is very long,
c             xerprn will break it into pieces of 72 characters (as
c             requested by xermsg) for printing on multiple lines.
c             also, xermsg asks xerprn to prefix each line with ' *  '
c             so that the total line length could be 76 characters.
c             note also that xerprn scans the error message backwards
c             to ignore trailing blanks.  another feature is that
c             the substring '$$' is treated as a new line sentinel
c             by xerprn.  if you want to construct a multiline
c             message without having to count out multiples of 72
c             characters, just use '$$' as a separator.  '$$'
c             obviously must occur within 72 characters of the
c             start of each line to have its intended effect since
c             xerprn is asked to wrap around at 72 characters in
c             addition to looking for '$$'.
c
c    nerr     an integer value that is chosen by the library routine's
c             author.  it must be in the range -99 to 999 (three
c             printable digits).  each distinct error should have its
c             own error number.  these error numbers should be described
c             in the machine readable documentation for the routine.
c             the error numbers need be unique only within each routine,
c             so it is reasonable for each routine to start enumerating
c             errors from 1 and proceeding to the next integer.
c
c    level    an integer value in the range 0 to 2 that indicates the
c             level (severity) of the error.  their meanings are
c
c            -1  a warning message.  this is used if it is not clear
c                that there really is an error, but the user's attention
c                may be needed.  an attempt is made to only print this
c                message once.
c
c             0  a warning message.  this is used if it is not clear
c                that there really is an error, but the user's attention
c                may be needed.
c
c             1  a recoverable error.  this is used even if the error is
c                so serious that the routine cannot return any useful
c                answer.  if the user has told the error package to
c                return after recoverable errors, then xermsg will
c                return to the library routine which can then return to
c                the user's routine.  the user may also permit the error
c                package to terminate the program upon encountering a
c                recoverable error.
c
c             2  a fatal error.  xermsg will not return to its caller
c                after it receives a fatal error.  this level should
c                hardly ever be used; it is much better to allow the
c                user a chance to recover.  an example of one of the few
c                cases in which it is permissible to declare a level 2
c                error is a reverse communication library routine that
c                is likely to be called repeatedly until it integrates
c                across some interval.  if there is a serious error in
c                the input such that another step cannot be taken and
c                the library routine is called again without the input
c                error having been corrected by the caller, the library
c                routine will probably be called forever with improper
c                input.  in this case, it is reasonable to declare the
c                error to be fatal.
c
c    each of the arguments to xermsg is input; none will be modified by
c    xermsg.  a routine may make multiple calls to xermsg with warning
c    level messages; however, after a call lpto xermsg with a recoverable
c    error, the routine should return to the user.  do not try to call lp
c    xermsg with a second recoverable error after the first recoverable
c    error because the error package saves the error number.  the user
c    can retrieve this error number by calling another entry point in
c    the error handling package and then clear the error number when
c    recovering from the error.  calling xermsg in succession causes the
c    old error number to be overwritten by the latest error number.
c    this is considered harmless for error numbers associated with
c    warning messages but must not be done for error numbers of serious
c    errors.  after a call lpto xermsg with a recoverable error, the user
c    must be given a chance to call lpnumxer or xerclr to retrieve or
c    clear the error number.
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  fdump, lpj4save, xercnt, xerhlt, xerprn, xersve
c***revision history  (yymmdd)
c   880101  date written
c   880621  revised as directed at slatec cml meeting of february 1988.
c           there are two basic changes.
c           1.  a new routine, xerprn, is used instead of xerprt to
c               print messages.  this routine will break long messages
c               into pieces for printing on multiple lines.  '$$' is
c               accepted as a new line sentinel.  a prefix can be
c               added to each line to be printed.  xermsg uses either
c               ' ***' or ' *  ' and long messages are broken every
c               72 characters (at most) so that the maximum line
c               length output can now be as great as 76.
c           2.  the text of all messages is now in upper case since the
c               fortran standard document does not admit the existence
c               of lower case.
c   880708  revised after the slatec cml meeting of june 29 and 30.
c           the principal changes are
c           1.  clarify comments in the prologues
c           2.  rename xrprnt to xerprn
c           3.  rework handling of '$$' in xerprn to handle blank lines
c               similar to the way format statements handle the /
c               character for new records.
c   890706  revised with the help of fred fritsch and reg clemens to
c           clean up the coding.
c   890721  revised to use new feature in xerprn to count characters in
c           prefix.
c   891013  revised to correct comments.
c   891214  prologue converted to version 4.0 format.  (wrb)
c   900510  changed test on nerr to be -9999999 < nerr < 99999999, but
c           nerr .ne. 0, and on level to be -2 < level < 3.  added
c           level=-1 logic, changed calls to xersav to xersve, and
c           xerctl to xercnt.  (rwc)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xermsg
      character*(*) librar, subrou, messg
      character*8 xlibr, xsubr
      character*72  temp
      character*20  lfirst
c***first executable statement  xermsg
      lkntrl = lpj4save (2, 0, .false.)
      maxmes = lpj4save (4, 0, .false.)
c
c       lkntrl is a local copy of the control flag kontrl.
c       maxmes is the maximum number of times any particular message
c          should be printed.
c
c       we print a fatal error message and terminate for an error in
c          calling xermsg.  the error number should be positive,
c          and the level should be between 0 and 2.
c
      if (nerr.lt.-9999999 .or. nerr.gt.99999999 .or. nerr.eq.0 .or.
     *   level.lt.-1 .or. level.gt.2) then
         call lpxerprn (' ***', -1, 'fatal error in...$$ ' //
     *      'xermsg -- invalid error number or level$$ '//
     *      'job abort due to fatal error.', 72)
         call lpxersve (' ', ' ', ' ', 0, 0, 0, kdummy)
         call lpxerhlt (' ***xermsg -- invalid input')
         return
      endif
c
c       record the message.
c
      i = lpj4save (1, nerr, .true.)
      call lpxersve (librar, subrou, messg, 1, nerr, level, kount)
c
c       handle print-once warning messages.
c
      if (level.eq.-1 .and. kount.gt.1) return
c
c       allow temporary user override of the control flag.
c
      xlibr  = librar
      xsubr  = subrou
      lfirst = messg
      lerr   = nerr
      llevel = level
      call lpxercnt (xlibr, xsubr, lfirst, lerr, llevel, lkntrl)
c
      lkntrl = max(-2, min(2,lkntrl))
      mkntrl = abs(lkntrl)
c
c       skip printing if the control flag value as reset in xercnt is
c       zero and the error is not fatal.
c
      if (level.lt.2 .and. lkntrl.eq.0) go to 30
      if (level.eq.0 .and. kount.gt.maxmes) go to 30
      if (level.eq.1 .and. kount.gt.maxmes .and. mkntrl.eq.1) go to 30
      if (level.eq.2 .and. kount.gt.max(1,maxmes)) go to 30
c
c       announce the names of the library and subroutine lpby building a
c       message in character variable temp (not exceeding 66 characters)
c       and sending it out via xerprn.  print only if control flag
c       is not zero.
c
      if (lkntrl .ne. 0) then
         temp(1:21) = 'message from routine '
         i = min(len(subrou), 16)
         temp(22:21+i) = subrou(1:i)
         temp(22+i:33+i) = ' in library '
         ltemp = 33 + i
         i = min(len(librar), 16)
         temp(ltemp+1:ltemp+i) = librar (1:i)
         temp(ltemp+i+1:ltemp+i+1) = '.'
         ltemp = ltemp + i + 1
         call lpxerprn (' ***', -1, temp(1:ltemp), 72)
      endif
c
c       if lkntrl is positive, print an introductory line before
c       printing the message.  the introductory line tells the choice
c       from each of the following three options.
c       1.  level of the message
c              'informative message'
c              'potentially recoverable error'
c              'fatal error'
c       2.  whether control flag will allow program to continue
c              'prog continues'
c              'prog aborted'
c       3.  whether or not a traceback was requested.  (the traceback
c           may not be implemented at some sites, so this only tells
c           what was requested, not what was delivered.)
c              'traceback requested'
c              'traceback not requested'
c       notice that the line including four prefix characters will not
c       exceed 74 characters.
c       we skip the next block if the introductory line is not needed.
c
      if (lkntrl .gt. 0) then
c
c       the first part of the message tells about the level.
c
         if (level .le. 0) then
            temp(1:20) = 'informative message,'
            ltemp = 20
         elseif (level .eq. 1) then
            temp(1:30) = 'potentially recoverable error,'
            ltemp = 30
         else
            temp(1:12) = 'fatal error,'
            ltemp = 12
         endif
c
c       then whether the program will continue.
c
         if ((mkntrl.eq.2 .and. level.ge.1) .or.
     *       (mkntrl.eq.1 .and. level.eq.2)) then
            temp(ltemp+1:ltemp+14) = ' prog aborted,'
            ltemp = ltemp + 14
         else
            temp(ltemp+1:ltemp+16) = ' prog continues,'
            ltemp = ltemp + 16
         endif
c
c       finally tell whether there should be a traceback.
c
         if (lkntrl .gt. 0) then
            temp(ltemp+1:ltemp+20) = ' traceback requested'
            ltemp = ltemp + 20
         else
            temp(ltemp+1:ltemp+24) = ' traceback not requested'
            ltemp = ltemp + 24
         endif
         call lpxerprn (' ***', -1, temp(1:ltemp), 72)
      endif
c
c       now send out the message.
c
      call lpxerprn (' *  ', -1, messg, 72)
c
c       if lkntrl is positive, write the error number and request a
c          traceback.
c
      if (lkntrl .gt. 0) then
         write (temp, '(''error number = '', i8)') nerr
         do 10 i=16,22
            if (temp(i:i) .ne. ' ') go to 20
   10    continue
c
   20    call lpxerprn (' *  ', -1, temp(1:15) // temp(i:23), 72)
         call lpfdump
      endif
c
c       if lkntrl is not zero, print a blank line and an end of message.
c
      if (lkntrl .ne. 0) then
         call lpxerprn (' *  ', -1, ' ', 72)
         call lpxerprn (' ***', -1, 'end of message', 72)
         call lpxerprn ('    ',  0, ' ', 72)
      endif
c
c       if the error is not fatal or the error is recoverable and the
c       control flag is set for recovery, then return.
c
   30 if (level.le.0 .or. (level.eq.1 .and. mkntrl.le.1)) return
c
c       the program will be stopped due to an unrecovered error or a
c       fatal error.  print the reason for the abort and the error
c       summary if the control flag and the maximum error count permit.
c
      if (lkntrl.gt.0 .and. kount.lt.max(1,maxmes)) then
         if (level .eq. 1) then
            call lpxerprn
     *         (' ***', -1, 'job abort due to unrecovered error.', 72)
         else
          call lpxerprn(' ***', -1, 'job abort due to fatal error.', 72)
         endif
         call lpxersve (' ', ' ', ' ', -1, 0, 0, kdummy)
         call lpxerhlt (' ')
      else
         call lpxerhlt (messg)
      endif
      return
      end subroutine lpxermsg
      subroutine lpxerprn (prefix, npref, messg, nwrap)
c***begin prologue  xerprn
c***subsidiary
c***purpose  print error messages processed by xermsg.
c***library   slatec (xerror)
c***category  r3c
c***type      all (xerprn-a)
c***keywords  error messages, printing, xerror
c***author  fong, kirby, (nmfecc at llnl)
c***description
c
c this routine sends one or more lines to each of the (up to five)
c logical units to which error messages are to be sent.  this routine
c is called several times by xermsg, sometimes with a single line to
c print and sometimes with a (potentially very long) message that may
c wrap around into multiple lines.
c
c prefix  input argument of type character.  this argument contains
c         characters to be put at the beginning of each line before
c         the body of the message.  no more than 16 characters of
c         prefix will be used.
c
c npref   input argument of type integer.  this argument is the number
c         of characters to use from prefix.  if it is negative, the
c         intrinsic function len is used to determine its length.  if
c         it is zero, prefix is not used.  if it exceeds 16 or if
c         len(prefix) exceeds 16, only the first 16 characters will be
c         used.  if npref is positive and the length of prefix is less
c         than npref, a copy of prefix extended with blanks to length
c         npref will be used.
c
c messg   input argument of type character.  this is the text of a
c         message to be printed.  if it is a long message, it will be
c         broken into pieces for printing on multiple lines.  each line
c         will start with the appropriate prefix and be followed by a
c         piece of the message.  nwrap is the number of characters per
c         piece; that is, after each nwrap characters, we break and
c         start a new line.  in addition the characters '$$' embedded
c         in messg are a sentinel for a new line.  the counting of
c         characters up to nwrap starts over for each new line.  the
c         value of nwrap typically used by xermsg is 72 since many
c         older error messages in the slatec library are laid out to
c         rely on wrap-around every 72 characters.
c
c nwrap   input argument of type integer.  this gives the maximum size
c         piece into which to break messg for printing on multiple
c         lines.  an embedded '$$' ends a line, and the count restarts
c         at the following character.  if a line break does not occur
c         on a blank (it would split a word) that word is moved to the
c         next line.  values of nwrap less than 16 will be treated as
c         16.  values of nwrap greater than 132 will be treated as 132.
c         the actual line length will be npref + nwrap after npref has
c         been adjusted to fall between 0 and 16 and nwrap has been
c         adjusted to fall between 16 and 132.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  lpi1mach, xgetua
c***revision history  (yymmdd)
c   880621  date written
c   880708  revised after the slatec cml subcommittee meeting of
c           june 29 and 30 to change the name to xerprn and to rework
c           the handling of the new line sentinel to behave like the
c           slash character in format statements.
c   890706  revised with the help of fred fritsch and reg clemens to
c           streamline the coding and fix a bug that caused extra blank
c           lines to be printed.
c   890721  revised to add a new feature.  a negative value of npref
c           causes len(prefix) to be used as the length.
c   891013  revised to correct error in calculating prefix length.
c   891214  prologue converted to version 4.0 format.  (wrb)
c   900510  added code to break messages between words.  (rwc)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xerprn
      character*(*) prefix, messg
      integer npref, nwrap
      character*148 cbuff
      integer iu(5), nunit
      character*2 newlin
      parameter (newlin = '$$')
c***first executable statement  xerprn
      call lpxgetua(iu,nunit)
c
c       a zero value for a logical unit number means to use the standard
c       error message unit instead.  lpi1mach(4) retrieves the standard
c       error message unit.
c
      n = lpi1mach(4)
      do 10 i=1,nunit
         if (iu(i) .eq. 0) iu(i) = n
   10 continue
c
c       lpref is the length of the prefix.  the prefix is placed at the
c       beginning of cbuff, the character buffer, and kept there during
c       the rest of this routine.
c
      if ( npref .lt. 0 ) then
         lpref = len(prefix)
      else
         lpref = npref
      endif
      lpref = min(16, lpref)
      if (lpref .ne. 0) cbuff(1:lpref) = prefix
c
c       lwrap is the maximum number of characters we want to take at one
c       time from messg to print on one line.
c
      lwrap = max(16, min(132, nwrap))
c
c       set lenmsg to the length of messg, ignore any trailing blanks.
c
      lenmsg = len(messg)
      n = lenmsg
      do 20 i=1,n
         if (messg(lenmsg:lenmsg) .ne. ' ') go to 30
         lenmsg = lenmsg - 1
   20 continue
   30 continue
c
c       if the message is all blanks, then print one blank line.
c
      if (lenmsg .eq. 0) then
         cbuff(lpref+1:lpref+1) = ' '
         do 40 i=1,nunit
            write(iu(i), '(a)') cbuff(1:lpref+1)
   40    continue
         return
      endif
c
c       set nextc to the position in messg where the next substring
c       starts.  from this position we scan for the new line sentinel.
c       when nextc exceeds lenmsg, there is no more to print.
c       we loop back to label 50 until all pieces have been printed.
c
c       we look for the next occurrence of the new line sentinel.  the
c       index intrinsic function returns zero if there is no occurrence
c       or if the length of the first argument is less than the length
c       of the second argument.
c
c       there are several cases which should be checked for in the
c       following order.  we are attempting to set lpiece to the number
c       of characters that should be taken from messg starting at
c       position nextc.
c
c       lpiece .eq. 0   the new line sentinel does not occur in the
c                       remainder of the character string.  lpiece
c                       should be set to lwrap or lenmsg+1-nextc,
c                       whichever is less.
c
c       lpiece .eq. 1   the new line sentinel starts at messg(nextc:
c                       nextc).  lpiece is effectively zero, and we
c                       print nothing to avoid producing unnecessary
c                       blank lines.  this takes care of the situation
c                       where the library routine has a message of
c                       exactly 72 characters followed by a new line
c                       sentinel followed by more characters.  nextc
c                       should be incremented by 2.
c
c       lpiece .gt. lwrap+1  reduce lpiece to lwrap.
c
c       else            this last case means 2 .le. lpiece .le. lwrap+1
c                       reset lpiece = lpiece-1.  note that this
c                       properly handles the end case where lpiece .eq.
c                       lwrap+1.  that is, the sentinel falls exactly
c                       at the end of a line.
c
      nextc = 1
   50 lpiece = index(messg(nextc:lenmsg), newlin)
      if (lpiece .eq. 0) then
c
c       there was no new line sentinel found.
c
         idelta = 0
         lpiece = min(lwrap, lenmsg+1-nextc)
         if (lpiece .lt. lenmsg+1-nextc) then
            do 52 i=lpiece+1,2,-1
               if (messg(nextc+i-1:nextc+i-1) .eq. ' ') then
                  lpiece = i-1
                  idelta = 1
                  goto 54
               endif
   52       continue
         endif
   54    cbuff(lpref+1:lpref+lpiece) = messg(nextc:nextc+lpiece-1)
         nextc = nextc + lpiece + idelta
      elseif (lpiece .eq. 1) then
c
c       we have a new line sentinel at messg(nextc:nextc+1).
c       don't print a blank line.
c
         nextc = nextc + 2
         go to 50
      elseif (lpiece .gt. lwrap+1) then
c
c       lpiece should be set down to lwrap.
c
         idelta = 0
         lpiece = lwrap
         do 56 i=lpiece+1,2,-1
            if (messg(nextc+i-1:nextc+i-1) .eq. ' ') then
               lpiece = i-1
               idelta = 1
               goto 58
            endif
   56    continue
   58    cbuff(lpref+1:lpref+lpiece) = messg(nextc:nextc+lpiece-1)
         nextc = nextc + lpiece + idelta
      else
c
c       if we arrive here, it means 2 .le. lpiece .le. lwrap+1.
c       we should decrement lpiece by one.
c
         lpiece = lpiece - 1
         cbuff(lpref+1:lpref+lpiece) = messg(nextc:nextc+lpiece-1)
         nextc  = nextc + lpiece + 2
      endif
c
c       print
c
      do 60 i=1,nunit
         write(iu(i), '(a)') cbuff(1:lpref+lpiece)
   60 continue
c
      if (nextc .le. lenmsg) go to 50
      return
      end subroutine lpxerprn
      subroutine lpxersve (librar, subrou, messg, kflag, nerr, level,
     +   icount)
c***begin prologue  xersve
c***subsidiary
c***purpose  record that an error has occurred.
c***library   slatec (xerror)
c***category  r3
c***type      all (xersve-a)
c***keywords  error, xerror
c***author  jones, r. e., (snla)
c***description
c
c *usage:
c
c        integer  kflag, nerr, level, icount
c        character * (len) librar, subrou, messg
c
c        call lpxersve (librar, subrou, messg, kflag, nerr, level, icount)
c
c *arguments:
c
c        librar :in    is the library that the message is from.
c        subrou :in    is the subroutine lpthat the message is from.
c        messg  :in    is the message to be saved.
c        kflag  :in    indicates the action to be performed.
c                      when kflag > 0, the message in messg is saved.
c                      when kflag=0 the tables will be dumped and
c                      cleared.
c                      when kflag < 0, the tables will be dumped and
c                      not cleared.
c        nerr   :in    is the error number.
c        level  :in    is the error severity.
c        icount :out   the number of times this message has been seen,
c                      or zero if the table has overflowed and does not
c                      contain this message specifically.  when kflag=0,
c                      icount will not be altered.
c
c *description:
c
c   record that this error occurred and possibly dump and clear the
c   tables.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  lpi1mach, xgetua
c***revision history  (yymmdd)
c   800319  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   900413  routine modified to remove reference to kflag.  (wrb)
c   900510  changed to add library name and subroutine lpto calling
c           sequence, use if-then-else, make number of saved entries
c           easily changeable, changed routine name from xersav to
c           xersve.  (rwc)
c   910626  added libtab and subtab to save statement.  (bks)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xersve
      integer lentab
      parameter (lentab=10)
      integer lun(5)
      character*(*) librar, subrou, messg
      character*8  libtab(lentab), subtab(lentab), lib, sub
      character*20 mestab(lentab), mes
      dimension nertab(lentab), levtab(lentab), kount(lentab)
      integer kountx,nmsg,kflag,nunit,kunit,iunit,lpi1mach,i
      save libtab, subtab, mestab, nertab, levtab, kount, kountx, nmsg
      data kountx/0/, nmsg/0/
c***first executable statement  xersve
c
      if (kflag.le.0) then
c
c        dump the table.
c
         if (nmsg.eq.0) return
c
c        print to each unit.
c
         call lpxgetua (lun, nunit)
         do 20 kunit = 1,nunit
            iunit = lun(kunit)
            if (iunit.eq.0) iunit = lpi1mach(4)
c
c           print the table header.
c
            write (iunit,9000)
c
c           print body of table.
c
            do 10 i = 1,nmsg
               write (iunit,9010) libtab(i), subtab(i), mestab(i),
     *            nertab(i),levtab(i),kount(i)
   10       continue
c
c           print number of other errors.
c
            if (kountx.ne.0) write (iunit,9020) kountx
            write (iunit,9030)
   20    continue
c
c        clear the error tables.
c
         if (kflag.eq.0) then
            nmsg = 0
            kountx = 0
         endif
      else
c
c        process a message...
c        search for this messg, or else an empty slot for this messg,
c        or else determine that the error table is full.
c
         lib = librar
         sub = subrou
         mes = messg
         do 30 i = 1,nmsg
            if (lib.eq.libtab(i) .and. sub.eq.subtab(i) .and.
     *         mes.eq.mestab(i) .and. nerr.eq.nertab(i) .and.
     *         level.eq.levtab(i)) then
                  kount(i) = kount(i) + 1
                  icount = kount(i)
                  return
            endif
   30    continue
c
         if (nmsg.lt.lentab) then
c
c           empty slot found for new message.
c
            nmsg = nmsg + 1
            libtab(i) = lib
            subtab(i) = sub
            mestab(i) = mes
            nertab(i) = nerr
            levtab(i) = level
            kount (i) = 1
            icount    = 1
         else
c
c           table is full.
c
            kountx = kountx+1
            icount = 0
         endif
      endif
      return
c
c     formats.
c
 9000 format ('0          error message summary' /
     +   ' library    subroutine lpmessage start             nerr',
     +   '     level     count')
 9010 format (1x,a,3x,a,3x,a,3i10)
 9020 format ('0other errors not individually tabulated = ', i10)
 9030 format (1x)
      end subroutine lpxersve
      subroutine lpxgetua (iunita, n)
c***begin prologue  xgetua
c***purpose  return unit number(s) to which error messages are being
c            sent.
c***library   slatec (xerror)
c***category  r3c
c***type      all (xgetua-a)
c***keywords  error, xerror
c***author  jones, r. e., (snla)
c***description
c
c     abstract
c        xgetua may be called to determine the unit number or numbers
c        to which error messages are being sent.
c        these unit numbers may have been set by a call lpto xsetun,
c        or a call lpto xsetua, or may be a default value.
c
c     description of parameters
c      --output--
c        iunit - an array of one to five unit numbers, depending
c                on the value of n.  a value of zero refers to the
c                default unit, as defined by the lpi1mach machine
c                constant routine.  only iunit(1),...,iunit(n) are
c                defined by xgetua.  the values of iunit(n+1),...,
c                iunit(5) are not defined (for n .lt. 5) or altered
c                in any way by xgetua.
c        n     - the number of units to which copies of the
c                error messages are being sent.  n will be in the
c                range from 1 to 5.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  lpj4save
c***revision history  (yymmdd)
c   790801  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xgetua
      integer iunita
      dimension iunita(5)
      integer n,lpj4save,i,index
c***first executable statement  xgetua
      n = lpj4save(5,0,.false.)
      do 30 i=1,n
         index = i+4
         if (i.eq.1) index = 3
         iunita(i) = lpj4save(index,0,.false.)
   30 continue
      return
      end subroutine lpxgetua
      subroutine lpxsetun (iunit)
      integer junk,iunit,lpj4save
c***begin prologue  xsetun
c***purpose  set output file to which error messages are to be sent.
c***library   slatec (xerror)
c***category  r3b
c***type      all (xsetun-a)
c***keywords  error, xerror
c***author  jones, r. e., (snla)
c***description
c
c     abstract
c        xsetun sets the output file to which error messages are to
c        be sent.  only one file will be used.  see xsetua for
c        how to declare more than one file.
c
c     description of parameter
c      --input--
c        iunit - an input parameter giving the logical unit number
c                to which error messages are to be sent.
c
c***references  r. e. jones and d. k. kahaner, xerror, the slatec
c                 error-handling package, sand82-0800, sandia
c                 laboratories, 1982.
c***routines called  lpj4save
c***revision history  (yymmdd)
c   790801  date written
c   861211  revision date from version 3.2
c   891214  prologue converted to version 4.0 format.  (bab)
c   920501  reformatted the references section.  (wrb)
c***end prologue  xsetun
c***first executable statement  xsetun
      junk = lpj4save(3,iunit,.true.)
      junk = lpj4save(5,1,.true.)
      return
      end subroutine lpxsetun
      subroutine lpusrmat (i, j, aij, indcat, prgopt, dattrv, iflag)
c***begin prologue  usrmat
c***subsidiary
c***purpose  subsidiary to splp
c***library   slatec
c***type      single precision (usrmat-s, dusrmt-d)
c***author  (unknown)
c***description
c
c   the user may supply this code
c
c***see also  splp
c***routines called  (none)
c***revision history  (yymmdd)
c   811215  date written
c   891214  prologue converted to version 4.0 format.  (bab)
c   900328  added type section.  (wrb)
c***end prologue  usrmat
      dimension prgopt(*),dattrv(*),iflag(10)
c
c***first executable statement  usrmat
      if(iflag(1).eq.1) then
c
c     this is the initialization step.  the values of iflag(k),k=2,3,4,
c     are respectively the column index, the row index (or the next col.
c     index), and the pointer to the matrix entry's value within
c     dattrv(*).  also check (dattrv(1)=0.) signifying no data.
           if(dattrv(1).eq.0.) then
           i = 0
           j = 0
           iflag(1) = 3
           else
           iflag(2)=-dattrv(1)
           iflag(3)= dattrv(2)
           iflag(4)= 3
           endif
c
           return
      else
           j=iflag(2)
           i=iflag(3)
           l=iflag(4)
           if(i.eq.0) then
c
c     signal that all of the nonzero entries have been defined.
                iflag(1)=3
                return
           else if(i.lt.0) then
c
c     signal that a switch is made to a new column.
                j=-i
                i=dattrv(l)
                l=l+1
           endif
c
           aij=dattrv(l)
c
c     update the indices and pointers for the next entry.
           iflag(2)=j
           iflag(3)=dattrv(l+1)
           iflag(4)=l+2
c
c     indcat=0 denotes that entries of the matrix are assigned the
c     values from dattrv(*).  no accumulation is performed.
           indcat=0
           return
      endif

      end subroutine lpusrmat
