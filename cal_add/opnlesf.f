      subroutine esf(x,fx)

      implicit   none

      include 'o8fuco.h'
      include 'o8fint.h'
      double precision x(*),fx
      integer i
      save
      if ( bloc ) then
        if ( valid ) then
          fx=fu(0)
          icf=icf+1
        else
          stop 'donlp2: bloc-call, function info invalid'
        endif
      else
        do i=1,n
          xtr(i)=x(i)*xsc(i)
        enddo
        call ef(xtr,fx)
      endif

      end subroutine
