!**************************************************************************
      subroutine kmont(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,
     *           mc,x1,y1,z,ir,sg,ex,igfx,xdes,ydes,btdes,gtor,ixz,
     *           alph,fltf,rdev,dist,isol,ndesign,isampl)
!**************************************************************************
!
!     orthognal plane sampling(koo heonsong)
!
!     coede by k.f 07/2002
!
!     note- only for component problem
!**************************************************************************
      implicit   none

      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      real*8,allocatable::y0(:),g0(:),rr(:,:),troot(:),wlim(:),avec(:)
      real*8,allocatable::alpha(:,:),weight(:),dgx(:),gam(:)
      real*8,allocatable::gdgx(:)
      integer*4,allocatable::isflg(:,:)
      real*8,allocatable::ycent(:,:),sgcent(:),btcent(:),alcent(:,:)
!
      integer*4 ids(nrx),ib(nrx),ipa(4,nrx),izx(nrx),lgp(3,nig),igt(nig)
      integer*4 mc(ntl,2),igfx(ngf,3),ir(ngfc),nc,ixz(nrx),isampl
      integer*4 ndesign
      real*8 x1(nrx),y1(nry),z(nry),bnd(2,nrx),tf(ntf),par(4,nrx)
      real*8 sg(nrx),ex(nrx),xdes(nrx,ngf),btdes(ngf),rdev,dist
      real*8 ydes(nry,ngf),gtor(ngf),tp(ntp),alph(nry,ngf)
      logical fltf(ngf)
!
      integer*4 nrmx
      parameter( nrmx = 5 )
!
      integer*4 nexact,iexact,neval0
      integer*4 mcst,mcend,igtp,i,i1,n1,n,iopt,jjj,itst,ifunc,ier,netot
      real*8 btlim,cum,sbet,cum2,dum,amag,xpp,rnm,sp,pff,dev,cvar,btg
      real*8 cdot,pf,exact

      integer (kind=4) :: isol, nevall
      real    (kind=8) :: r
!
      allocate(isflg(nrmx,ngf))
      allocate(y0(nry))
      allocate(dgx(nrx))
      allocate(gam(nrx))
      allocate(avec(nrx))
      allocate(gdgx(nrx))
      allocate(g0(ngf))
      allocate(weight(ngf))
      allocate(rr(nrmx,ngf))
      allocate(troot(nrmx*ngf))
      allocate(wlim(ngf))
      allocate(alpha(nry,ngf))
      allocate(ycent(nry,ngf))
      allocate(sgcent(ngf))
      allocate(btcent(ngf))
      allocate(alcent(nry,ngf))
!
      nevall=0
!
      write(not,'(1h ,''  '')')
      write(not,'(1h ,''  ***** orthogonal plane sampling ******'')')
      write(not,'(1h ,''  '')')
!
      netot=0
      igtp=0
      do 299 i=1,nig
 299  if(igt(i).ne.1) igtp=1
!
!     ----- define deterministic value for x if ids=0 -----
!
      do 120 i=1,nrx
        if(ids(i).gt.50) then
          if(ib(i).eq.0) x1(i)=0.0d0
          if(ib(i).eq.1) x1(i)=bnd(1,i)+sg(i)
          if(ib(i).eq.2) x1(i)=bnd(2,i)-sg(i)
          if(ib(i).eq.3) x1(i)=(bnd(1,i)+bnd(2,i))/2.0
         else
          x1(i)=ex(i)
        endif
120   continue
!
!     ----- adjust cut set data for component problem -----
!
      mcst=1
      mcend=ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend=2*ngfc
      endif
      if(icl.eq.1) then
!       ----- for component problem -----
        call finim(igfx,igf)
        mcst=1
        mcend=2
        mc(1,1)=igf
        mc(1,2)=1
        mc(2,1)=0
        mc(2,2)=0
!      else
!        write(not,'(1h ,'' fatal error '')')
!        write(not,'(1h ,'' orthognal plane sampling cant be used'')')
!        write(not,'(1h ,'' for a system problem '')')
!        stop
      endif
!
!     ------ make design point ( should be analyzed previously by form ) -----
!
      call ncent(not,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,
     *           ids,ipa,izx,ixz,ib,lgp,igt,
     *           xdes,btdes,z,bnd,tf,tp,par,sg,ex,ydes,alpha,
     *           g0,wlim,rdev,dist,
     *           nc,ycent,alcent,btcent,sgcent,0.0d0,ndesign,
     *           alph,fltf,igfx(1,1),isampl)
      do i=1,nc
        weight(i)=g0(i)
      enddo
!
!     ----- make threshold for gfunc -----
!
      call nkroot(ngf,ntl,nry,ngfc,icl,igfx,mcst,mcend,mc,
     *            btdes,alpha,z,y1,ir,btlim)
      btlim=10.0d0
!
!     ----- for continuing problem -----
!
      if(ist.ne.0) then
        read(ns0,rec=1,err=10)cum,sbet,i1
        n1=i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif
!
!
!     ------ analysis start -----
!
      i1=0
      n1=0
      nexact=0
      iexact=0
      exact=0.0d0
      cum=0.0d0
      cum2=0.0d0
   30 n=npr
      n1=n+n1
      i1=i1+1
      write(not,2010)
!
!.... perform monte carlo simulation
!
      open(25,file='info.dat',form='formatted')
      iopt = 1
      call gguw(stp,1,iopt,r)
      iopt=0
      do i=1,100000
        call gguw(stp,1,iopt,r)
      enddo
!
      do 50 jjj=i1,nsm
        itst=jjj
        if(jjj.eq.112) then
          continue
        endif
!
!       ----- selection of design point -----
!
        if(nc.gt.1) then
          call nselect(nc,wlim,stp,iopt,ifunc)
        else
          ifunc=1
        endif
!        ifunc=1
!
!       ------- generation of y ------
!
        call ngenorm(nry,dum,dum,y1,stp,iopt,1)
!
!       ------ make y0 -----
!
        amag=cdot(y1,alcent(1,ifunc),nry,nry,nry)
        do i=1,nry
          y0(i)=y1(i)-amag*alcent(i,ifunc)
        enddo
!
!       ------ make beta solution ---------
!
        call nksolv0(nrx,nry,ntf,ngf,nig,nrmx,ngfc,icl,igfx,igt,lgp,
     *               ids,ipa,ib,izx,ixz,x1,y0,y1,
     *               alcent(1,ifunc),btcent(ifunc),
     *               tf,ex,sg,par,bnd,tp,gtor,btlim,z,dgx,gam,avec,
     *               gdgx,isflg,rr,ir,g0,isol,neval)
        netot=netot+neval
!
!       ----- compute failure probability -----
!
        xpp=0.0d0
        if(icl.eq.1) then
!       ----- for component problem -----
          call nkchkg(nrmx,ir(1),isflg(1,1),rr(1,1),g0(1),gtor(1),xpp)
        elseif(icl.eq.2) then
!       ----- for seriese system problem -----
          call nksers(nrmx,ngf,ngfc,ir,isflg,rr,xpp)
        else
!       ----- for general system problem -----
          call nkgens(nrx,nry,ntf,ngf,nig,ntl,nrmx,ngfc,igfx,igt,
     *                lgp,ids,ipa,ib,izx,mcst,mcend,mc,
     *                x1,y1,z,tf,ex,sg,par,
     *                bnd,tp,alcent(1,ifunc),y0,ir,rr,troot,xpp,btlim)
        endif
!
!        xpp=xpp*weight(ifunc)
        cum=cum+xpp
        cum2=cum2+xpp*xpp

        if(iexact.ne.0) then
          nexact=nexact+1
          exact=exact+cum/real(jjj)
        endif
        if(jjj.lt.n1.or.jjj.eq.1) goto 50
        rnm=real(jjj)
        pf=cum/rnm
        sp=cum2-cum*cum/rnm
        sp=dabs(sp/(rnm*(rnm-1)))
!        if(sp.lt.0.0d0.and.sp.gt.-1.0d-10) sp=0.0d0
        pff=1.0d0-pf
        if(sp.ge.0.0d0) then
          if(iexact.ne.0) goto 60
          dev=dsqrt(sp)
          cvar=dev/pf
          call dnormi(btg,pff,ier)
!          write(not,2020) jjj,pf,btg,cvar,netot,ir(1),rr(1,1),xpp
          write(not,2020) jjj,pf,btg,cvar,netot
!          write(50,2020) jjj,pf,cvar,btg,netot,ir(1),rr(1,1)
          if(cvar.le.cov) then
            neval0=nevall
            iexact=1
!            n1=n1+int(rnm/10)
            n1=n1+n
          endif
        else
          write(not,2020) jjj,pf,btg,cvar
!          write(not,2030) jjj
        endif
        n1=n+n1
  50  continue
!
  60  write(ns0,rec=1) cum,sbet,min(jjj,nsm)
!      write(50,'(1h ,''  '')')
!      write(not,'(1h ,''  '')')
!      write(not,'(1h ,'' exact='',1e15.5)') exact/float(nexact)
!      write(not,'(1h ,'' neval='',i15)') neval0
!
!.... output formats
!
 2010 format('     trials',8x,'pf-mean',5x,'betag-mean',4x,
     1       'coef of var',4x,'g-func eval')
 2020 format(i11,1p,e15.5,e15.5,e15.5,i15)
!2030 format(i12,5x,'  -----',9x,'  ------',9x,'   ------')

      deallocate(isflg)
      deallocate(y0)
      deallocate(g0)
      deallocate(rr)
      deallocate(troot)
      deallocate(wlim)
      deallocate(alpha)
      deallocate(ycent)
      deallocate(sgcent)
      deallocate(btcent)
      deallocate(alcent)
      deallocate(weight)
      deallocate(dgx)
      deallocate(gam)
      deallocate(avec)
      deallocate(gdgx)

      end subroutine kmont
