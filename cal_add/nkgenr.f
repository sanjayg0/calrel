c**********************************************************************
      subroutine nkgenr(nrx,nry,ntf,ngf,nig,ntl,ngfc,igfx,igt,
     *                  lgp,ids,ipa,ib,izx,ir,mcst,mcend,mc,
     *                  x1,y0,tf,ex,sg,par,bnd,tp,alpha,y1,z,
     *                  r,idg)
c**********************************************************************
      implicit   none

      integer*4 nrx,nry,ntf,ngf,nig,ntl,ngfc,igfx(ngf,3),igt(nig)
      integer*4 lgp(3,nig),ids(nrx),ipa(4,nrx),ib(nrx),izx(nry),ir(ngf)
      real*8 x1(nrx),y0(nry),tf(ntf),ex(nrx),sg(nrx),par(4,nrx)
      real*8 bnd(2,nrx),tp(*),alpha(nry),y1(nry)
      real*8 z(nry),r
      integer*4 mcst,mcend,mc(ntl,2),idg
c
      integer*4 j,ifunc,jfunc,imm,ics,is,ic
      real*8 gg
c
      do j=1,nry
        y1(j)=y0(j)+r*alpha(j)
      enddo
      call cytox(x1,y1,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      do jfunc=1,ngfc
        ifunc=igfx(jfunc,3)
        call ugfun(gg,x1,tp,ifunc)
        if(gg.le.0.0d0) then
          ir(jfunc)=0
        else
          ir(jfunc)=1
        endif
      enddo
      imm=1
      idg=1
      do 100 is=mcst,mcend
        if(mc(is,1).ne.0) then
          ic=mc(is,2)
          if(mc(is,1).lt.0) then
            imm=imm*ir(ic)
          else
            imm=imm*(1-ir(ic))
          endif
        else
          ics=1-imm
          idg=idg*ics
          imm=1
        endif
  100 continue
      idg=1-idg

      end subroutine nkgenr
