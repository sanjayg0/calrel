c********************************************************************
      subroutine o8eval(sigact,sigres,reject,error,
     *            ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd,
     *            gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)

      implicit   none

      include 'o8comm.h'
      include 'o8cons.h'
      include 'o8fint.h'
      double precision sigact,sigres
      logical reject,error
      integer*4 ifunc1(*),ifunc2(*),ids(*),ipa(4,*),ib(*)
      integer*4 izx(*),lgp(3,*),igt(*),ixz(*)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 gam(*),dgx(*),gdgx(*),alp(*)
c***  local
      integer i,j
      double precision term
      save
c**** evaluate the functions at the new point
      sig=sigact
      do i =1,n
        x1(i)=x(i)+sig*(d(i)+sig*dd(i))
c****** project with respect to the box-constraints
        if ( llow(i) ) x1(i)=max(x1(i),ug(i))
        if ( lup(i) ) x1(i)=min(x1(i),og(i))
      enddo
      reject=.false.
      error=.false.
      sigres=sig
      upsi1=zero
      psi1=zero
c      if ( bloc ) call user_eval(x1,-1)
c***** only function values, from xtr=x1*xsc
      do j = 1,nres
        i=sort(j)
        if ( i .le. nh ) then
          cfuerr(i)=.false.
          call esh(i,x1,res1(i))
          if ( cfuerr(i) ) then
            error=.true.
            return
          endif
          term=abs(res1(i))
        else
          cfuerr(i)=.false.
c          call esg(i-nh,x1,res1(i))
          call esg(i-nh,x1,res1(i),ifunc1,ifunc2,yyy,zzz,tf,tp,ex,
     *     sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          if ( cfuerr(i) ) then
            error=.true.
            return
          endif
          term=-min(zero,res1(i))
          if ( res1(i) .lt. -delmin .and. bind(i) .eq. 0) then
            violis(0)=violis(0)+1
            violis(violis(0))=i
          endif
        endif
c***** violis is the list of inequality-contraints currently
c***** not binding which have been hit during unidimensional search
c***** sigres is the smallest zero of secants through constraints
c***** which change sign along [x,x+d]
        upsi1=upsi1+term
        if ( upsi1 .gt. tau0 .and. phase .ne. -1  ) then
          reject=.true.
          return
        endif
        psi1=psi1+term*w(i)
        if ( res1(i)*res(i) .lt. zero .and. sig .le. one
     f       .and. ( bind(i) .eq. 0 .or.
     f            (bind(i) .eq. 1 .and. (abs(res(i))/gresn(i)
     f             .ge. tp3*epsmac .or. abs(res1(i))/gresn(i)
     f             .ge. tp3*epsmac ) ) )
     f     )
     f      sigres=min(sigres,sig*res(i)/(res(i)-res1(i)))
      enddo
      if ( phase .ne. -1 ) then
        ffuerr=.false.
        call esf(x1,fx1)
        if ( ffuerr ) then
          error=.true.
          return
        endif
      else
        fx1=zero
      endif
      phi1=scf*fx1+psi1

      end subroutine o8eval
