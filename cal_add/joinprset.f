c***************************************************************
      subroutine joinprset(not,mcount,ncount,mc2,nry,alphnew,btnew)
c***************************************************************
      implicit   none

      integer (kind=4),allocatable :: iprint(:)

      integer (kind=4) :: not,mcount,ncount,mc2(mcount,*),nry
      real    (kind=8) :: alphnew(nry,*),btnew(*)

      integer (kind=4) :: i,j,k,ncomp,iset,iim

      write(not,'(1h ,''total number of linearized comps'',i10)')ncount

      do i=1,ncount
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' === component'',i5,'' ==='')') i
      write(not,'(1h ,'' alpha vector '')')
      write(not,'(1h ,1p5e15.5)') (alphnew(k,i),k=1,nry)
      write(not,'(1h ,'' beta...'',1pe15.5)') btnew(i)
      enddo

      allocate(iprint(1000))
      write(not,'(1h ,'' cut set data '')')
      ncomp=0
      iset=1
      do i=1,mcount
        iim=mc2(i,1)
        if(iim.ne.0) then
          ncomp=ncomp+1
          iprint(ncomp)=iim
        else
          write(not,'(1h ,'' cutset '',i5)') iset
          write(not,'(1h ,'' num of comps'',i5)') ncomp
          write(not,'(1h ,'' comps  '',10i5)') (iprint(j),j=1,ncomp)
          ncomp=0
          iset=iset+1
        endif
      enddo
      deallocate(iprint)

      end subroutine joinprset
