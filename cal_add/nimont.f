!***********************************************************************
      subroutine imont(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,
     *                 mc,x1,y1,z,igx,sg,ex,igfx,xdes,ydes,btdes,
     *                 ixz,alph,fltf,beta,rdev,dist,ndesign,isampl)
!***********************************************************************
      implicit   none

      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      real*8,allocatable::ycent(:,:),sgcent(:),alcent(:,:),alpha(:,:)
      real*8,allocatable::btcent(:),weight(:),wlim(:)
      integer*4 isampl

      integer*4 ids(nrx),ib(nrx),ipa(4,nrx),izx(nrx),lgp(3,nig),igt(nig)
      integer*4 mc(ntl,2),igfx(ngf,3),igx(ngfc),ixz(*),ndesign
      real*8 tf(ntf),x1(nrx),y1(nry),z(nry),bnd(2,nrx)
      real*8 par(4,nrx),tp(ntp),sg(nrx),ex(nrx)
      real*8 xdes(nrx,ngf),ydes(nry,ngf),btdes(ngf)
      real*8 beta,rdev,dist,alph(nry,ngf)
      logical fltf(ngf)

      integer*4 i,igtp,inkk,mcst,mcend,i1,n,n1,iopt,jjj
      integer*4 itst,m,idg,kip,ier,nc,icent,iexact,nexact,neval0
      real*8 sbet,cum,cum2,p,rbt,xpp,smpl,rnm,sp,pff,exact
      real*8 dev,cvar,btg,coef,pf,smpll,smp,ffunc,hfunc

      real    (kind=8) :: r 

      allocate(ycent(nry,ngf))
      allocate(sgcent(ngf))
      allocate(weight(ngf))
      allocate(wlim(ngf))
      allocate(btcent(ngf))
      allocate(alcent(nry,ngf))
      allocate(alpha(nry,ngf))

!     ----- check whether all subgroups are type 1. -----

      neval=0
      igtp=0
      do 299 i=1,nig
 299  if(igt(i).ne.1) igtp=1

!    ----- define deterministic value for x if ids=0 -----

      do 120 i=1,nrx
        if(ids(i).gt.50) then
          if(ib(i).eq.0) x1(i)=0.0d0
          if(ib(i).eq.1) x1(i)=bnd(1,i)+sg(i)
          if(ib(i).eq.2) x1(i)=bnd(2,i)-sg(i)
          if(ib(i).eq.3) x1(i)=(bnd(1,i)+bnd(2,i))/2.0
         else
          x1(i)=ex(i)
        endif
120   continue

!     ----- check beta restaraint -----

      inkk=0
      if(beta.gt.0.1e-9) then
      write(not,398) beta
      sbet=beta*beta
      inkk=1
      endif

!     ----- search for cutset data -----

      exact=0.0d0
      mcst=1
      mcend=ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend=2*ngfc
      endif
      if(icl.eq.1) then
!       ----- for component problem -----
        call finim(igfx,igf)
        mcst=1
        mcend=2
        mc(1,1)=igf
        mc(1,2)=1
        mc(2,1)=0
        mc(2,2)=0
      endif

      write(not,'(1h ,''  '')')
      write(not,'(1h ,''  ***** ordinary importance sampling ******'')')
      write(not,'(1h ,''  '')')
!      write(not,'(1h ,''  rdev ,,,,,,,,,,,,,,,,,,,,'',1e15.5)') rdev
!      write(not,'(1h ,''  dist,,,,,,,,,,,,,,,,,,,,,'',1e15.5)') dist
!      write(not,'(1h ,''  '')')

!      write(not,'(1h ,''  '')')
!      write(not,'(1h ,''  number of components,,,,,'',i10)') ngfc
!      do i=1,ngfc
!      write(not,'(1h ,''  '')')
!      write(not,'(1h ,''  functiond id ,,,,,,,,,,,,'',i10)') igfx(i,3)
!      write(not,'(1h ,''  coordinates of design point in x '')')
!      write(not,'(1h ,''  '')')
!      write(not,'((1h ,5e15.5))') (xdes(j,1),j=1,nrx)
!      write(not,'(1h ,''  '')')
!      enddo

!     ------ design point ( should be analyzed previously ) -----

      call ncent(not,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,
     *           ids,ipa,izx,ixz,ib,lgp,igt,
     *           xdes,btdes,z,bnd,tf,tp,par,sg,ex,ydes,alpha,
     *           weight,wlim,rdev,dist,
     *           nc,ycent,alcent,btcent,sgcent,0.0d0,ndesign,
     *           alph,fltf,igfx(1,1),isampl)

!     ----- for restart problem -----

      if(ist.ne.0) then
        read(ns0,rec=1,err=10)cum,sbet,i1
        n1=i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif

      i1=0
      n1=0
      iexact=0
      exact=0.0d0
      nexact=0
      cum=0.0d0
      cum2=0.0d0
      smpl=0.0d0
   30 n=npr
      n1=n+n1
      i1=i1+1
      write(not,2010)

!.... perform monte carlo simulation

!     ----- test for presentation -----

!x      sgcent(1)=1.0d0
!x      ycent(1,1)=btcent(1)
!      ycent(2,1)=btcent(2)
!      weight(1)=1.0d0
!      nc=1

      iopt = 1
      call gguw(stp,1,iopt,r)
      iopt=0
      do i=1,100000
        call gguw(stp,1,iopt,r)
      enddo

      do 50 jjj=i1,nsm
        if(jjj.eq.112) then
          continue
        endif
        itst=jjj

!       ----- selection of design point -----

        if(nc.eq.1) then
          icent=1
        else
          call nselect(nc,wlim,stp,iopt,icent)
        endif

!       ------- generation of y ------

        call ngenorm(nry,sgcent(icent),ycent(1,icent),y1,stp,iopt,0)

!       ------ compute pdf -------

        call nhfunc(nc,nry,weight,y1,sgcent,ycent,
     *              ffunc,hfunc,coef)

!       ------ transform into x -----

        if(igtp.eq.0) then
!       ----- all are s.i. variables ------
          do 46 m=1,nry
            call dnorm(y1(m),p)
            y1(m) = p
   46     continue
          call crtox(x1,y1,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          go to 499
        endif
        if(inkk.eq.1) then
!       ----- correlated variables ------
          rbt=0.d0
          do 999 kip=1,nry
            rbt=rbt+y1(kip)*y1(kip)
  999     continue
          if(rbt.le.sbet) go to 55
        endif
        call cytox(x1,y1,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)

!       ----- check fail or safe -----

  499   continue
        call nsumpt(tp,x1,mc,igx,igfx,idg,mcst,mcend)
        if(idg.eq.1) then
          xpp=1.d0*coef
          smp=1.0d0
        else
          xpp=0.d0
          smp=0.0d0
        endif
        cum=cum+xpp
        smpl=smpl+smp
        cum2=cum2+xpp*xpp
        if(iexact.ne.0) then
          nexact=nexact+1
          exact=exact+cum/real(jjj)
        endif
 55     if(jjj.lt.n1) go to 50
        rnm=real(jjj)
        pf=cum/rnm
        smpll=smpl/rnm
        sp=cum2-cum*cum/rnm
        sp=sp/(rnm*(rnm-1))
        pff=1.d0-pf
        if(sp.gt.0.d0) then
          dev=dsqrt(sp)
          if(iexact.eq.1) goto 60
          cvar=dev/pf
          call dnormi(btg,pff,ier)
          write(not,2020) jjj,pf,btg,cvar,smpll
!          write(50,2020) jjj,pf,cvar,btg,smpll,ffunc,hfunc
          if(cvar.le.cov) then
            neval0=neval
            iexact=1
            n1=n1+n
            goto 50
          endif
        else
!          write(not,2020) jjj,pf,btg,cvar,smpll,ffunc,hfunc
          write(not,2030) jjj
        endif
        n1=n+n1
  50  continue

  60  write(ns0,rec=1) cum,sbet,min(jjj,nsm)
!      write(50,'(1h ,''  '')')
!      write(not,'(1h ,''  '')')
!      write(not,'(1h ,'' exact='',1e15.5)') exact/float(nexact)
      write(not,'(1h ,'' neval='',i15)') neval0

!.... output formats

 2010 format('     trials',8x,'pf-mean ',4x,'betag-mean',4x,
     1       'coef of var',4x,'sampl. eff.')
 2020 format(i11,1p,e15.5,e15.5,e15.5,4e15.5)
 2030 format(i12,5x,'  -----',9x,'  ------',9x,'   ------')
 398  format(
     *'  Radius of beta sphere ................rad=',1p,1e11.3)

      deallocate(ycent)
      deallocate(sgcent)
      deallocate(btcent)
      deallocate(alcent)
      deallocate(alpha)
      deallocate(weight)
      deallocate(wlim)

      end subroutine imont
