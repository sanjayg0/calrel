c**************************************************************
      subroutine sciboun(ncset,ndp,ptable,stable,
     *                   min,next,pl3,pu3,cvl,cvu,
     *                   maxcvli,maxcvlj,maxcvui,maxcvuj)
c**************************************************************
      implicit   none

      integer*4 ncset,ndp,min,next(*)
      real*8 ptable(ndp,*),stable(ndp,*),pl3,pu3,cvl,cvu
c
      integer*4 ii,i,im1,jj,j,maxcvli,maxcvlj,maxcvui,maxcvuj
      integer*4 maxcvlli,maxcvllj
      real*8 pp,px,spl3,ss,sx,spu3,ccmax,cccmax
c
c     ---  compute unimodeal bounds -----
c
c      pl1=prob(min,min)
c      pp=1.d0
c      do 110 i=1,ncset
c  110 pp=pp*(1.d0-prob(i,i))
c      pu1=1.d0-pp
c      pu1n=0.
c      do 120 i=1,ncset
c  120 pu1n=pu1n+prob(i,i)
c      if(id.eq.1) pu1=pu1n
c      if(ibt.eq.1) go to 999
c
c     ----- compute bi-modal bounds -----
c
c     ----- lower bound calculation -----
c
      ii=min
      pl3=ptable(ii,ii)
      spl3=stable(ii,ii)
      maxcvli=ii
      maxcvlj=ii
      ccmax=spl3
      do 340 i=2,ncset
      ii=next(ii)
      im1=i-1
      pp=0.d0
      ss=0.0d0
      jj=min
      cccmax=ss
      maxcvlli=0
      maxcvllj=0
      do 330 j=1,im1
      px=ptable(jj,ii)
      sx=stable(jj,ii)
      pp=pp+px
      ss=ss+sx
      if(sx.gt.cccmax) then
        maxcvlli=jj
        maxcvllj=ii
        cccmax=sx
      endif
  330 jj=next(jj)
      pp=ptable(ii,ii)-pp
      if(stable(ii,ii).gt.cccmax) then
        maxcvlli=ii
        maxcvllj=ii
        cccmax=stable(ii,ii)
      endif
      ss=stable(ii,ii)+ss
      if(pp.lt.0.d0) then
        pp=0.d0
        ss=0.0d0
      else
        if(cccmax.gt.ccmax) then
          ccmax=cccmax
          maxcvli=maxcvlli
          maxcvlj=maxcvllj
        endif
      endif
      spl3=spl3+ss
  340 pl3=pl3+pp
c
      cvl=dsqrt(spl3)/pl3
c
c     -------- upper bound calculation -----
c
      ii=min
      pu3=ptable(ii,ii)
      spu3=stable(ii,ii)
      maxcvui=ii
      maxcvuj=ii
      ccmax=spu3
      do 360 i=2,ncset
      ii=next(ii)
      pp=0.d0
      ss=0.0d0
      im1=i-1
      jj=min
      do 350 j=1,im1
      px=ptable(ii,jj)
      sx=stable(ii,jj)
      if(px.gt.pp) then
        pp=px
        ss=sx
        if(ss.gt.ccmax) then
          maxcvui=jj
          maxcvuj=ii
          ccmax=ss
        endif
      endif
  350 jj=next(jj)
      if(stable(ii,ii).gt.ccmax) then
        maxcvui=ii
        maxcvuj=ii
        ccmax=stable(ii,ii)
      endif
      spu3=spu3+stable(ii,ii)+ss
  360 pu3=pu3+ptable(ii,ii)-pp
c
      cvu=dsqrt(spu3)/pu3

      end subroutine sciboun
