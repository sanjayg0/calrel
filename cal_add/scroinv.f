c********************************************
      subroutine scroinv(nset,ndro,ro,rovec)
c********************************************
      implicit   none

      real*8,allocatable::rodum(:,:)
      integer*4 nset,ndro
      real*8 ro(ndro,*),rovec(*)
c
      integer*4 il,k,i,j
c
      allocate(rodum(nset,nset))
c
      il=0
      do k=2,nset
        do i=1,k
        do j=1,k
          rodum(i,j)=ro(i,j)
        enddo
        enddo
        call covinv(nset,k,rodum)
        do i=1,k-1
        il=il+1
        rovec(il)=rodum(k,i)
        enddo
        il=il+1
        rovec(il)=rodum(k,k)
      enddo
c
      deallocate(rodum)

      end subroutine scroinv

