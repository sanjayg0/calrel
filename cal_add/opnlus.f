      subroutine setup0(nry,mconst)

      implicit   none

      include 'o8comm.h'
      integer i,j,nry,mconst
c   name is ident of the example/user and can be set at users will
c   the first character must be alphabetic.  40 characters maximum
       name='userexample'
c   x is initial guess and also holds the current solution
c   problem dimension n=dim(x), nh=dim(h), ng=dim(g)
      n=nry       ! number of unknowns
      nh=0        ! number of equal constraint
      ng=mconst   ! number of inquality constraint
      analyt=.true.  ! if use finite difference .false.
      cold=.true.   ! if hot start .false.
      silent=.false.  ! if not want to have any print .true.
      epsdif=1.0d0-14  !  the precision of finite diffrence
      prou=60    ! output unit id !
      open(prou,file='fort.60',form='formatted')
      meu=70     ! meesage output !
      open(meu,file='fort.70',form='formatted')
c   del0 and tau0: see below
      del0=1.d0
      tau0=1.d0
      do i=1,n
        x(i)=0.0d0
      enddo
c    gunit-array, see donlp2doc.txt
      do j=0,ng
        gunit(1,j)=-1
        gunit(2,j)=0
        gunit(3,j)=0
      enddo
c  gconst-array:
c      do j=...
c        gconst(j)=.true.
c  if the j-th function is affine linear
c      enddo

      end subroutine setup0
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine ef(x,fx)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit   none

      include 'o8fuco.h'
      double precision x(*),fx,ysum
      integer*4 i
      icf=icf+1

      ysum=0.0d0
      do i=1,n
        ysum=ysum+x(i)**2
      enddo

      fx=0.5d0*ysum

      end subroutine ef
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine egradf(x,gradf)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit   none

      include 'o8fuco.h'
      double precision x(*),gradf(*)
c     user declarations, if any ,follow
      integer j
      icgf=icgf+1
      do j=1,n
        gradf(j)=x(j)
      enddo

      end subroutine egradf
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine eh(i,x,hxi)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit   none

      include 'o8fuco.h'
      double precision x(*),hxi
      integer i
      cres(i)=cres(i)+1
      hxi=0.0d0

      end subroutine eh
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine egradh(i,x,gradhi)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit   none

      include 'o8fuco.h'

      double precision x(*),gradhi(*)

      integer i,j

      if ( gunit(1,i) .ne. 1 ) cgres(i)=cgres(i)+1
      do  j=1,nx
        gradhi(j)=0.d0
      enddo

      end subroutine egradh

c compute the i-th inequality constaint, bounds included

      subroutine setup

      implicit   none

      include 'o8comm.h'

c     change termination criterion
c      epsx=..
c     change i/o-control
c      te0=.true.
c*** now you get for every step a one-line-output on stdout
c      ......

      end subroutine setup
