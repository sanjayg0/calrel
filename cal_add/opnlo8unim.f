c********************************************************************
c     determination of stepsize by an armijo-like test for
c     descent
c********************************************************************
c      subroutine o8unim(sig1th)
      subroutine o8unim(sig1th,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd
     *           ,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)

      implicit   none

      include 'o8comm.h'
      include 'o8cons.h'
      double precision sig1th
      integer*4 ifunc1(*),ifunc2(*),ids(*),ipa(4,*)
      integer*4 izx(*),lgp(3,*),igt(*),ixz(*),ib(*)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 gam(*),dgx(*),gdgx(*),alp(*)
c**** sig1th the first proposed stepsize for searching on the arc
c**** if sig=one did'nt work
c
c   n = number of variables
c   x = current point
c   d = direction of descent. dd = second order correction
c   x,d = input
c   x0,d0 etc. information from previous step
c
c   xnorm,dnorm = euclidean length of x and d
c   stptrm = 1 on success , =-1 or =-2 otherwise
c   sig = computed stepsize
c   it is assumed that one is asymptotically optimal
c   sigsm = smallest acceptable stepsize
c   sigla = largest acceptable stepsize
c   alpha = smallest feasible reduction factor for stepsize
c   delta = multiplier for derivative should be smaller than .25
c   beta = maximum feasible increase of x-norm for sig=1
c   theta = bound for cos(angle(current direction, previous direction))
c           if overridden, stepsize larger than one is tried
c
c*********************************************************************
c*** local
      integer i,l,j
      double precision term,maxphi
      double precision sigres ,diff
      double precision step(nstep)
      logical desc,descre,sminfe,lainfe,reject,error
      save
      data step/p5,twom2,38*tm1/
c*** projection of d, rescaling and computing dirder has been done
c*** already
      l=0
      error=.false.
      phi=scf*fx+psi
      sig=sig1th
      violis(0)=0
      if ( .not. silent ) call o8info(8)
100   continue
      l=l+1
      if ( l .gt. nstep ) then
        if ( error .and. .not. silent ) call o8msg(24)
        stptrm=-one
        sig=zero
        return
      endif
c*****  compute a new x and test for descent
      call o8eval(sig,sigres,reject,error,
     *            ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd,
     *            gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
      if ( error ) then
        if ( sig .gt. one ) then
          call o8rest
          goto 200
        else
          if ( .not. silent ) call o8msg(25)
          sig=step(l)*sig
          goto 100
        endif
      endif
      if ( reject ) then
        if ( sig .gt. one ) then
          call o8rest
          goto 200
        else
          sig=step(l)*sig
          goto 100
        endif
      endif
c
c
      if ( .not. silent ) call o8info(9)
c*****  new function value
      if ( sig .gt. one ) then
        if ( phi1 .ge. phimin ) then
c*****      phi does'nt decrease further
          call o8rest
          goto 200
        else
          if ( sig .lt. stmaxl ) then
            call o8save
            sig=min(stmaxl,sig+sig)
            goto 100
          else
            goto 200
          endif
        endif
      endif
      if ( lastch .ge. itstep-3 .or. phase .ne. 2
     f      .or. singul ) then
c***** require monotonic behaviour
        diff=phi-phi1
      else
        maxphi=phi
        do j=1,3
          maxphi=
     +    max(scf*accinf(itstep-j,2)+accinf(itstep-j,4),maxphi)
        enddo
        diff=maxphi-phi1
      endif
      desc = diff .ge. min(-sig*delta*dirder,level)
      descre = upsi - upsi1 .ge. sig*delta**2*upsi/tauqp
      sminfe = upsi .le. tau0*p5 .and. upsi1 .le. tau0
      lainfe = upsi .gt. tau0*p5
      if ( desc .and. ( sminfe .or. ( lainfe .and. descre ) ) ) then
c*****  goldstein-armijo descent test satisfied
        if ( sig .eq. one .and. ( (cosphi .ge. theta .and. sig0
     f        .ge. one .and. (phase+1)*(phase-2) .ne. 0
     f         .and. .not. singul) .or.
     f         diff .ge. -sig*delta1*dirder )
c*****   1>=delta1  >> delta > 0
     f         .and. stmaxl .gt. one .and. upsi .lt. tau0*p5 )  then
c*****    try stepsize larger than one
c*****    save the current point as the best one
          call o8save
          sig=min(stmaxl,sig+sig)
          goto 100
        endif
        if ( sig .le. one .and. upsi .gt. tau0*p5 .and.
     f         upsi1 .gt. upsi ) goto 300
        goto 200
      else
        goto 300
      endif
200   continue
c******   accept new x, save old values
      fx0 = fx
      fx = fx1
      upsi0=upsi
      upsi=upsi1
      psi0=psi
      psi=psi1
      stptrm = one
      sig0 = sig
      do i = 1,n
        x0(i)=x(i)
        d0(i)=d(i)
        x(i)=x1(i)
        difx(i)=x(i)-x0(i)
      enddo
      d0norm=dnorm
      x0norm=xnorm
      do i=1,nres
        res(i)=res1(i)
      enddo
      return
c****** continue reducing sig
300   continue
      if ( sigres .lt. sig ) then
        sig=min(p5*sig,max(step(l)*sig,sigres))
      else
        term=(diff-dirder*sig)*two
        if ( term .gt. epsmac*(scf*abs(fx)+psi) ) then
          sig=min(p5*sig,max(step(l)*sig,-dirder*sig**2/term))
        else
          sig=step(l)*sig
        endif
      endif
ctest      do i=1,n
ctest        if ( abs(x1(i)-x(i)) .ge. sigsm*(abs(x(i))+tm2)) then
ctest          goto 100
ctest        endif
ctest      enddo
      if ( sig*max(one,dnorm) .ge. sigsm  ) goto 100
      stptrm = -one
      sig=zero

      end subroutine o8unim
