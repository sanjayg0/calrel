!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine dirs1(beta,alph,mc,x,y,rr,a2,rot,ind,id,ig)

!     functions:
!       compute the failure probability by the directional simulation
!       (simulate a line) method for the first order system relhability
!       problem.

!     input arguments:
!       beta   : reliability indexes of all modes.
!       alph: unit vectors of design points.
!       mc   : elements of minimun cut sets.
!       nry   : no. of dimension in y space.
!       ngf   : no. of performance functions.
!       ncs  : no. of minimun cut sets.
!       nsm   : no. of directional simulations.
!       ntl : total no. of elements in the minimun cut sets + ncs.
!       cov : criteria on coefficient of variance of pf.
!       ist  : index for the option of restart in directional simulation.

!     working aray:
!       x,y,rr,a2,rot,ind,id,ig,

!     calls: dnormi,drand,dsolv1,dscum.

!     called by: main.

!     last version :  dec. 2 1989, by h. -z. lin

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dirsi1(beta,alph,mc,x,y,rr,a2,rot,ind,id,ig,igfx,
     *                 xdes,ydes,qqq,ndesign,isampl)

      implicit   none

      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      integer (kind=4) :: isampl, ndesign
      real    (kind=8) :: qqq

      real    (kind=8),allocatable :: ycent(:,:)
      real    (kind=8),allocatable :: sgcent(:)
      real    (kind=8),allocatable :: alcent(:,:)
      real    (kind=8),allocatable :: alpha(:,:)
      real    (kind=8),allocatable :: btcent(:)
      real    (kind=8),allocatable :: weight(:)
      real    (kind=8),allocatable :: wlim(:)
      real    (kind=8),allocatable :: y0(:)
      real    (kind=8),allocatable :: btdes(:)

      real    (kind=8) :: random
      real    (kind=8) :: x(nry),y(nry),beta(ngf),alph(nry,ngf),rr(ngfc)
      real    (kind=8) :: a2(ngfc),rot(2+ngfc)
      integer (kind=4) :: ind(ngfc),id(ngfc+2),ig(ngfc),mc(ntl,2)
      integer (kind=4) :: igfx(ngf,3)

      integer (kind=4) :: i,i1,ic,ii,idg,id0,ier, icent,iexact, imm,iopt
      integer (kind=4) :: jfunc,jjj
      integer (kind=4) :: l,l2
      integer (kind=4) :: m,m1,mcend,mcst,mm
      integer (kind=4) :: n,n1,nc, neval0,nexact, ngfcc
!WIP nc is never defined but is used
      real    (kind=8) :: amag, axp
      real    (kind=8) :: btg, bunbo
      real    (kind=8) :: cs, coef, cum, cvar
      real    (kind=8) :: dev
      real    (kind=8) :: exact
      real    (kind=8) :: gx
      real    (kind=8) :: p2, pf,pff
      real    (kind=8) :: r,r1,rn,rnm,rrr, root
      real    (kind=8) :: sp,sy, xp,xp1, yyy

      real    (kind=8) :: cdot

      save

!.....set up the criterio on the max. root in root's finding procedure.

      real*8 xdes(nrx,ngf),ydes(nry,ngf)
      allocate(ycent(nry,ngf))
      allocate(sgcent(ngf))
      allocate(weight(ngf))
      allocate(wlim(ngf))
      allocate(btcent(ngf))
      allocate(btdes(ngf))
      allocate(alcent(nry,ngf))
      allocate(alpha(nry,ngf))
      allocate(y0(nry))

       rn=real(nry)
      mcst=1
      mcend=ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend=2*ngfc
      endif
      if(icl.eq.1) then
        call finim(igfx,igf)
        mcst=1
        mcend=2
        mc(1,1)=igf
        mc(1,2)=1
        mc(2,1)=0
        mc(2,2)=0
      endif
      if(ist.ne.0) then
        read(ns0,rec=2,err=10) x,cum,xp1,root,id0,i1
        id(1)=id0
        n1=i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif
      call droot(beta,alph,mc,x,y,ig,root,a2,igfx,1,mcst,mcend)
      i1=0
      xp1=0.d0
      cum=0.0d0
      n1=0
!.....check the origin is in the failure domain or not.
      do 1102 l=1,ngfc
      l2=igfx(l,2)
      gx=beta(l2)
         if(gx.le.1e-10) then
            ig(l)=0
         else
            ig(l)=1
         endif
!.....ig(l)=0 means failure.
 1102 continue
      idg=1
      imm=1
      do 910 i=mcst,mcend
          if(mc(i,1).ne.0) then
                  ic=mc(i,2)
              if(mc(i,1).lt.0) then
                  imm=imm*ig(ic)
              else
                  imm=imm*(1-ig(ic))
              endif
          else
              idg=idg*(1-imm)
              imm=1
          endif
 910  continue
!c.....idg=0 means the segment is in the failure domain.
          if(idg.eq.0) then
              id(1)=1
          else
              id(1)=0
          endif
   30 n=npr
      i1=i1+1
      n1=n+n1
      write(not,2010)
      xp=0.d0

!.... perform monte carlo simulation

      exact=0.0d0
      iexact=0
      nexact=0
      i1=0
      cum=0.d0
      xp1=0.d0
      n1=0
      ngfcc=ngfc

!      call ncent(6,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,ids,
!     *           ipa,izx,lgp,igt,
!     *           xdes,beta,z1,bnd,tf,par,sg,ex,ydes,alpha,
!     *           weight,wlim,rdev,dist,
!     *           nc,ycent,alcent,btcent,sgcent,qqq,ndesign,isampl)
!      subroutine ncent(not,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,
!     *                 ids,ipa,izx,ixz,ib,lgp,igt,
!     *                 xdes,btdes,z,bnd,tf,par,sg,ex,ydes,alpha,
!     *                 weight,wlim,rdev,dist,
!     *                 nc,ycent,alcent,btcent,sgcent,p0,ndesign,
!     *                 isampl)

      iopt = 1
      do 130 jjj=i1,nsm
        axp=0.d0
        sy=0.d0
!---------- select the design point ----------------
        call gguw(stp,1,iopt,rrr)
        iopt = 0
        if(rrr.le.qqq) then
          icent=0
        else
          do jfunc=1,ngfc-1
            if(rrr.le.wlim(jfunc)) goto 17
          enddo
          icent=ngfc
          goto 27
   17    icent=jfunc
   27    continue
        endif
!---------- select the design point ----------------
        do 40 m=1,nry
          call gguw(stp,1,iopt,random)
          iopt = 0
          r = random
          call dnormi(y(m),r,ier)
 40     continue

        if(icent.ne.0) then
          amag=cdot(y,alcent(1,icent),nry,nry,nry)
          do i=1,nry
            y0(i)=y(i)-amag*alcent(i,icent)
          enddo
          call dnorm(-btcent(icent),pf)
          call gguw(stp,1,iopt,random)
          iopt=0
          r=1.0d0-(1.0d0-random)*pf
          call dnormi(yyy,r,ier)
          do i=1,nry
            y(i)=y0(i)+yyy*alpha(i,icent)
          enddo
        endif

        do 50 m=1,nry
          sy=sy+y(m)*y(m)
   50     continue
        sy=dsqrt(sy)
        do 60 m=1,nry
   60     y(m)=y(m)/sy

!        make coeff

!WIP    nc not ever defined
        write(*,*) 'ERROR in DIRIMP1:NC undefined'
        bunbo=qqq
        do jfunc=1,nc
          call dnorm(-btcent(jfunc),pf)
          amag=cdot(y,alcent(1,jfunc),nry,nry,nry)
          if(amag.gt.0.0d0) then
            amag=(btcent(jfunc)/amag)**2
            call dchis(amag,rn,p2,ier)
            bunbo=bunbo+weight(jfunc)*(1.0d0-p2)/pf
          endif
        enddo
        coef=1.0d0/bunbo



!.....use performance function to determine r
        call dsolv1(beta,alph,y,rr,ind,a2,igfx,root)
!.....count total number of roots.
        rot(1)=0.d0
!        do 138 kj=1,2
!        if(kj.eq.2) then
!        do 129 ky=1,ngfc
!        a2(ky)=-a2(ky)
!  129   rr(ky)=-rr(ky)
!        endif
        m1=1
        do 70 ii=1,ngfc
          if(ind(ii).eq.0.or.rr(ii).le.0.) go to 70
              m1=m1+1
              rot(m1)=rr(ii)
   70     continue
!.....arrange m1 roots in orderings.
        if(m1.le.1) then
        if(id(1).eq.1) axp=axp+1.d0
        go to 138
        endif
        mm=m1-1
   80   do 90 ii=1,mm
         if(rot(ii).gt.rot(ii+1)) then
             r1=rot(ii)
             rot(ii)=rot(ii+1)
             rot(ii+1)=r1
         endif
   90   continue
        if(mm.gt.1) then
          mm=mm-1
          go to 80
        endif
!.....determine the ranges of the roots in the failure sets.
        if(icl.le.2) then
          if(id(1).eq.0) then
          cs=rot(2)*rot(2)
          call dchis(cs,rn,p2,ier)
          axp=axp+1.d0-p2
          else
          cs=rot(m1)*rot(m1)
          call dchis(cs,rn,p2,ier)
          axp=axp+p2
          endif
        go to 138
        endif
        rot(m1+1)=root
        call dscum(m1,rot,rr,a2,beta,ig,id,igfx,xp,mc,mcst,mcend,1)
        axp=axp+xp
 138    continue
!        axp=axp/2.d0
!        cum=cum+axp
        cum=cum+axp*coef
        xp1=xp1+axp*axp*coef*coef
        if(iexact.ne.0) then
          nexact=nexact+1
          exact=exact+cum/real(jjj)
        endif
        if(jjj.ge.n1) then
          rnm=real(jjj)
          pf=cum/rnm
          sp=xp1-2.d0*pf*cum+rnm*pf*pf
          rnm=rnm*(rnm-1.d0)
          pff=1.d0-pf
          if(sp.gt.0.1e-12) then
            if(iexact.ne.0) goto 140
            dev=dsqrt(sp/rnm)
            cvar=dev/pf
            call dnormi(btg,pff,ier)
            write(not,2020) jjj,pf,btg,cvar
            if(cvar.le.cov) then
              iexact=1
              neval0=neval
              n1=n1+npr
              go to 130
            endif
          else
            write(not,2030) jjj
          endif
          n1=n+n1
        endif
  130 continue
 140  id0=id(1)
      write(ns0,rec=2)x,cum,xp1,root,id0,min(jjj,nsm)
!      write(not,'(1h ,'' exact='',e15.5)') exact/float(nexact)
      write(not,'(1h ,'' neval='',i15)') neval0
!---  output formats
 2010 format(/'     trials',8x,'pf-mean',5x,'betag-mean',4x,
     *       'coef of var')
 2020 format(i7,1p,e15.5,e15.5,e15.5)
 2030 format(i7,3x,'    ----',9x,'    ----',9x,'    ----')
      return

!     ------ test -----

!      ngfcc=1
!      do i=1,nry
!      ydes(i,1)=0.0d0
!      enddo
!      do i=1,5
!      ydes(i,1)=1.281551d0
!      enddo
!      do i=1,10
!      ydes(i,1)=0.478274d0
!      enddo
!     ------

      end subroutine dirsi1
