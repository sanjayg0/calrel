c**************************************************
c suite of function interfaces, for performing scaling and
c external evaluations
c if bloc=.true. then it is asumed that prior to calls to es<xyz>
c valid function information is computed (via a call of user_eval)
c and stored in fu and fugrad , setting valid=.true. afterwards
c**************************************************
c     objective function
c     gradient of objective function
      subroutine esgradf(x,gradf)

      implicit   none

      include 'o8fuco.h'
      include 'o8fint.h'
      include 'o8cons.h'
      double precision x(*),gradf(*)
c     user declarations, if any ,follow
      integer j

c     double precision d1,d2,d3,sd1,sd2,sd3,fhelp,fhelp1,fhelp2,
c    *   fhelp3,fhelp4,fhelp5,fhelp6,xincr,xhelp,floc

      save
      if ( gunit(1,0) .eq. 1 ) then
        do j=1,n
          gradf(j)=zero
        enddo
        gradf(gunit(2,0))=gunit(3,0)*xsc(gunit(2,0))
        return
      endif
      if ( bloc ) then
        if ( valid ) then
          icgf=icgf+1
          do           j=1,n
            gradf(j)=xsc(j)*fugrad(j,0)
          enddo
          return
        else
          stop 'donlp2: bloc call with function info invalid'
        endif
      else
        do j=1,n
          xtr(j)=xsc(j)*x(j)
        enddo
        if ( analyt ) then
           call egradf(xtr,gradf)
        else
c        if ( difftype .eq. 1 ) then
c          deldif=min(tm1*sqrt(epsfcn),tm2)
c          call ef(xtr,floc)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
c            if ( xhelp .ge. zero ) then
c              xtr(j)=xhelp+xincr
c            else
c              xtr(j)=xhelp-xincr
c            endif
c            call ef(xtr,fhelp)
c            gradf(j)=(fhelp-floc)/(xtr(j)-xhelp)
c            xtr(j)=xhelp
c          enddo
c        elseif ( difftype .eq. 2 ) then
c          deldif=min(tm1*epsfcn**(one/three),tm2)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd)
c            xtr(j)=xhelp+xincr
c            call ef(xtr,fhelp1)
c            xtr(j)=xhelp-xincr
c            call ef(xtr,fhelp2)
c            gradf(j)=(fhelp1-fhelp2)/(xincr+xincr)
c            xtr(j)=xhelp
c          enddo
c        else
c          deldif=min(tm1*epsfcn**(one/seven),tm2)
c          do j=1,n
c            xhelp=xtr(j)
c            xincr=min(tm2,deldif*abs(xhelp)+deldif,taubnd/four)
c            xtr(j)=xhelp-xincr
c            call ef(xtr,fhelp1)
c            xtr(j)=xhelp+xincr
c            call ef(xtr,fhelp2)
c            xincr=xincr+xincr
c            d1=xincr
c            xtr(j)=xhelp-xincr
c            call ef(xtr,fhelp3)
c            xtr(j)=xhelp+xincr
c            call ef(xtr,fhelp4)
c            xincr=xincr+xincr
c            d2=xincr
c            xtr(j)=xhelp-xincr
c            call ef(xtr,fhelp5)
c            xtr(j)=xhelp+xincr
c            call ef(xtr,fhelp6)
c            xtr(j)=xhelp
c            d3=xincr+xincr
c            sd1=(fhelp2-fhelp1)/d1
c            sd2=(fhelp4-fhelp3)/d2
c            sd3=(fhelp6-fhelp5)/d3
c            sd3=sd2-sd3
c            sd2=sd1-sd2
c            sd3=sd2-sd3
c            gradf(j)=sd1+p4*sd2+sd3/c45
c          enddo
c        endif
        endif
        do j=1,n
          gradf(j)=xsc(j)*gradf(j)
        enddo
      endif

      end subroutine esgradf
