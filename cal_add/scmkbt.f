c*********************************************************
      subroutine scmkbt(not,nset,iset,isys,btdes,bvec,nind,iord)
c*********************************************************
c
c     construct beta vector
c
c     input   nset  : number of components
c             iset  : seq id. of components
c             isys  : sysytem flag  1 system
c                                   2or3 paralell
c             btdes : beta of components
c
c     output  bvec  : beta-vector
c
c*********************************************************
      implicit   none

      real*8,allocatable::bprint(:)
      integer*4 not,nset,iset(*),isys,nind,iord(*)
      real*8 btdes(*),bvec(*)
c
      integer*4 i,ii
c
      allocate(bprint(nset))
      do i=1,nset
c        ii=iset(i)
        ii=iset(iord(i))
        bprint(i)=btdes(iabs(ii))
        if(isys.eq.1) then
c       ----- series problem -----
          if(ii.gt.0) then
             bvec(i)=btdes(ii)
          else
             bvec(i)=-btdes(-ii)
          endif
        else
c       ----- prallel problem -----
          if(ii.gt.0) then
             bvec(i)=-btdes(ii)
          else
             bvec(i)=btdes(-ii)
          endif
        endif
      enddo
c
      if(not.ne.0) then
      write(not,'(1h ,''------------------ beta values '',
     *                ''-------------------'')')
      write(not,'(1h ,5x,1p20e15.5)') (bprint(i),i=1,nind)
      endif
c
      deallocate(bprint)

      end subroutine scmkbt
