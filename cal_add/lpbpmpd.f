c primal-dual method with supernodal cholesky factorization
c               version 2.11 (1996 december)                              lpmpso
c  written by cs. meszaros, mta sztaki, budapest, hungary
c        questions, remarks to the e-mail address:
c               meszaros@lutra.sztaki.hu

c  all rights reserved ! free for academic and research use only !
c  commercial users are required to purchase a software license.

c related publications:

c    meszaros, cs.: fast cholesky factorization for interior point methods
c       of linear programming. computers & mathematics with applications,
c       vol. 31. no.4/5 (1996) pp. 49-51.

c    meszaros, cs.: the "inexact" minimum local fill-in ordering algorithm.
c       working paper wp 95-7, computer and automation institute, hungarian
c       academy of sciences

c    maros i., meszaros cs.: the role of the augmented system in interior
c       point methods.  european journal of operations researches
c       (submitted)


c it is the main of the code.
c here you can change the ammount of memory used by the program
c by setting the parameter relmxx. the total amount of memory
c used is then (14*relmxx) bytes. for example, in  64 mbyte
c systems set relmxx=4500000.

c ===========================================================================
      subroutine lpbpmain(filname,code,opt,minmax,relmxx,intmxx,rhsin,
     *                    costfunc,mtjh2)

      implicit   none

      integer (kind=4) :: n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpdims/      n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

c      common/lpbigarray/ realmem,intmem

      real*8           tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens

      common/lpsprnod/ psupn,ssupn,maxsnz
      integer*4      psupn,ssupn,maxsnz

      real*8          tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x                pinfs,dinfs,inftol
      integer*4                          maxiter
      common/lptoler/ tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x                pinfs,dinfs,inftol,maxiter

      real*8          tplus,tzer
      common/lpnumer/ tplus,tzer

      real*8          tresx,tresy
      integer*4                   maxref
      common/lpitref/ tresx,tresy,maxref

      real*8          objnor,rhsnor,scdiff
      integer*4                            scpass,scalmet
      common/lpascal/ objnor,rhsnor,scdiff,scpass,scalmet

      real*8          ccstop,barset,bargrw,barmin
      integer*4                                   mincor,maxcor,inibar
      common/lppredp/ ccstop,barset,bargrw,barmin,mincor,maxcor,inibar

      real*8        target,tsmall,tlarge,center,corstp
      integer*4     mincc,maxcc
      common/lppredc/ target,tsmall,tlarge,center,corstp,mincc,maxcc

      real*8        palpha,dalpha
      common/lpparam/ palpha,dalpha

      real*8        tfixvar,tfixslack,slklim
      common/lpdrop/  tfixvar,tfixslack,slklim

      real*8        prmin,upmax,dumin
      integer*4     stamet,safmet,premet,regul
      common/lpinitv/ prmin,upmax,dumin,stamet,safmet,premet,regul

      real*8        varadd,slkadd,scfree
      common/lpmscal/ varadd,slkadd,scfree

      real*8         maxdense,densgap
      integer*4      setlam,denslen
      common/lpsetden/ maxdense,densgap,setlam,denslen

      real*8        climit,ccorr
      common/lpcompl/ climit,ccorr

      integer*4     act
      common/lptimex/ act

      integer*4      loglog,lfile
      common/lplogprt/ loglog,lfile

      real    (kind=8),allocatable::realmem(:)
      integer (kind=4),allocatable::intmem(:)

      integer (kind=4) :: relmxx,intmxx,mtjh2

      character (len=80) :: filname
      real    (kind=8) :: rhsin(*),costfunc(*)

      integer (kind=4) :: intmax,relmax,fixn,dropn
      integer (kind=4) :: intmap(25),relmap(25)
      integer (kind=4) :: code,iter,corect
c      real*8    realmem(relmxx),opt,addobj
      real    (kind=8) :: opt,addobj,dbtime
c      integer*4 intmem(intmxx)
      integer (kind=4) :: infile,oufile,sfile
      integer (kind=4) :: outlev,fnzmax,fnzmin,nn,minmax
      character (len=99) :: buff
      real    (kind=8) :: bigbou,big
      character (len=8 ) :: objnam
      integer (kind=4) :: t1,t2,tf


      allocate(realmem(relmxx))
      allocate(intmem(intmxx))
      act=0
      big=1.0d+30

c toleranciak es parameterek

      maxdense  = 0.15d+0
      densgap   = 3.00d+0
      setlam    = 0
      supdens   = 250.0d+0
      denslen   = 10

      varadd    = 1.0d-12
      slkadd    = 1.0d+16
      scfree    = 1.1d-06

      tpiv1     = 1.0d-03
      tpiv2     = 1.0d-08
      tabs      = 1.0d-12
      trabs     = 1.0d-15
      lam       = 1.0d-05
      tfind     = 25.0d+0
      order     = 2.0d+0

      psupn     =  3
      ssupn     =  4
      maxsnz    = 9999999

      tsdir     = 1.0d-16
      topt1     = 1.0d-08
c      topt1     = 1.0d-05
      topt2     = 1.0d-16
      tfeas1    = 1.0d-07
      tfeas2    = 1.0d-07
      feas1     = 1.0d-02
      feas2     = 1.0d-02
      pinfs     = 1.0d-06
      dinfs     = 1.0d-06
      inftol    = 1.0d+04
      maxiter   = 99

      tplus     = 1.0d-11
      tzer      = 1.0d-35

      tresx     = 1.0d-09
      tresy     = 1.0d-09
      maxref    = 5

      objnor    = 1.0d+2
      rhsnor    = 0.0d+0
      scdiff    = 1.0d+0
      scalmet   = 2
      scpass    = 5

      ccstop    = 1.01d+00
      barset    = 2.50d-01
      bargrw    = 1.00d+02
      barmin    = 1.00d-10
      mincor    = 1
      maxcor    = 1
      inibar    = 0

      target    = 9.0d-02
      tsmall    = 2.0d-01
      tlarge    = 2.0d+01
      center    = 5.0d+00
      corstp    = 1.01d+00
      mincc     = 0
      maxcc     = 9

      palpha    = 0.999d+0
      dalpha    = 0.999d+0

      tfixvar   = 1.0d-16
      tfixslack = 1.0d-16
      slklim    = 1.0d-16

      prmin    =   100.0d+0
      upmax    = 50000.0d+0
      dumin    =   100.0d+0
      stamet   = 2
      safmet   =-3
      premet   = 511
      regul    = 0

      climit   = 1.0d-00
      ccorr    = 1.0d-05

      intmax=intmxx
      relmax=relmxx
      infile=25
      oufile=26
      sfile =27
      lfile =29
      loglog= 1
      outlev= 1

c      write(*,'(1x)')
c      write(*,'(30x,a)')'bpmpd version 2.11'
c      write(*,'(17x,a)')'written by cs. meszaros, mta sztaki, budapest'
c      write(*,'(1x)')

      call lpfinput (intmap,relmap,realmem,intmem,infile,oufile,intmax,
     x relmax,addobj,outlev,bigbou,big,nn,sfile,minmax,objnam,
     * filname,rhsin)
      n1=n+1

      call lptimer(t1)
      call lpsolver(
     x realmem(relmap( 1)),realmem(relmap( 2)),realmem(relmap( 3)),
     x realmem(relmap( 4)),realmem(relmap( 5)),realmem(relmap( 6)),
     x realmem(relmap( 7)),realmem(relmap( 8)),realmem(relmap( 9)),
     x realmem(relmap(10)),realmem(relmap(11)),realmem(relmap(12)),
     x realmem(relmap(13)),realmem(relmap(14)),realmem(relmap(15)),
     x realmem(relmap(16)),realmem(relmap(17)),realmem(relmap(18)),
     x realmem(relmap(19)),realmem(relmap(20)),realmem(relmap(21)),
     x realmem(relmap(22)),realmem(relmap(23)),realmem(relmap(24)),
     x intmem(intmap( 1)),intmem(intmap( 2)),intmem(intmap( 3)),
     x intmem(intmap( 4)),intmem(intmap( 5)),intmem(intmap( 6)),
     x intmem(intmap( 7)),intmem(intmap( 8)),intmem(intmap( 9)),
     x intmem(intmap(10)),intmem(intmap(11)),intmem(intmap(12)),
     x intmem(intmap(13)),intmem(intmap(14)),
     x code,opt,iter,corect,fixn,dropn,fnzmax,fnzmin,addobj,
     x bigbou,big,tf)
      call lptimer(t2)
      t1=t2-t1



      call lpmpsout(m,nn,nz,oufile,sfile,code,opt,
     x realmem(relmap(5)),realmem(relmap(6)),realmem(relmap(4)+nn),
     x realmem(relmap(3)),realmem(relmap(4)),realmem(relmap(2)),
     x intmem(intmap(2)),realmem(relmap(7)),
     x intmem(intmap(6)),intmem(intmap(6)+n),realmem(relmap(20)),
     x intmem(intmap(3)),intmem(intmap(13)),realmem(relmap(24)),
     x intmem(intmap(11)),intmem(intmap(11)+n),
     x iter,corect,outlev,minmax,big,objnam,t1,tf,
     * realmem(relmap(17)),realmem(relmap(18)),realmem(relmap(19)),
     * costfunc,mtjh2)



      write(buff,9)
      call lpmprnt(buff)
      if(code.le.1)then
         write(buff,5)
         call lpmprnt(buff)
      else if(code.eq.2)then
         write(buff,6)opt
         call lpmprnt(buff)
      else if(code.eq.3)then
         write(buff,7)
         call lpmprnt(buff)
      else if(code.eq.4)then
         write(buff,8)
         call lpmprnt(buff)
      endif

    5 format(1x,'execution stopped.')
    6 format(1x,'optimal solution.       objective  =',d18.10)
    7 format(1x,'problem is dual infeasibile (or badly scaled).')
    8 format(1x,'problem is primal infeasibile (or badly scaled).')
    9 format(1x)
   10 format(1x,'total solution time  =',f12.2,' sec.')
      write(buff,9)
      dbtime=dble(t1)
      write(buff,10)(dbtime/100.0d+0)
      call lpmprnt(buff)
c      close(oufile)
      deallocate(realmem)
      deallocate(intmem)
      return
      end
c primal-dual method with supernodal cholesky factorization
c               version 2.11 (1996 december)
c  written by cs. meszaros, mta sztaki, budapest, hungary
c           e-mail: meszaros@lutra.sztaki.hu
c                      see "bpmain.f"

c code=-2 general memory limit (no solution)
c code=-1 memory limit during iterations
c code= 0
c code= 1 no optimum
c code= 2 otimal solution
c code= 3 primal infeasible
c code= 4 dual infeasible

c ===========================================================================

      subroutine lpphas12(
     x obj,rhs,bounds,diag,odiag,xs,dxs,dxsn,up,dspr,ddspr,
     x ddsprn,dsup,ddsup,ddsupn,dv,ddv,ddvn,nonzeros,prinf,upinf,duinf,
     x vartyp,slktyp,colpnt,ecolpnt,count,vcstat,pivots,invprm,
     x snhead,nodtyp,inta1,rowidx,rindex,
     x rwork1,iwork1,iwork2,iwork3,iwork4,iwork5,
     x code,opt,iter,corect,fixn,dropn,active,fnzmax,fnzmin,addobj,
     x scobj,factim,mn2)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpmscal/ varadd,slkadd,scfree
      real*8        varadd,slkadd,scfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      common/lpparam/ palpha,dalpha
      real*8        palpha,dalpha

      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens

      common/lptoler/ tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol,maxiter
      real*8        tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol
      integer*4     maxiter

      common/lpinitv/ prmin,upmax,dumin,stamet,safmet,premet,regul
      real*8        prmin,upmax,dumin
      integer*4     stamet,safmet,premet,regul

      integer*4 fixn,dropn,active,code,iter,corect,fnzmin,fnzmax,mn2
      real*8  addobj,scobj,opt

      common/lppredp/ ccstop,barset,bargrw,barmin,mincor,maxcor,inibar
      real*8        ccstop,barset,bargrw,barmin
      integer*4     mincor,maxcor,inibar

      common/lppredc/ target,tsmall,tlarge,center,corstp,mincc,maxcc
      real*8        target,tsmall,tlarge,center,corstp
      integer*4     mincc,maxcc

      common/lpitref/ tresx,tresy,maxref
      real*8        tresx,tresy
      integer*4     maxref

      real*8 obj(n),rhs(m),bounds(mn),diag(mn),odiag(mn),xs(mn),
     x dxs(mn),dxsn(mn),up(mn),dspr(mn),ddspr(mn),ddsprn(mn),dsup(mn),
     x ddsup(mn),ddsupn(mn),dv(m),ddv(m),ddvn(m),nonzeros(cfree),
     x prinf(m),upinf(mn),duinf(mn),rwork1(mn)

      integer*4 vartyp(n),slktyp(m),colpnt(n1),ecolpnt(mn),count(mn),
     x vcstat(mn),pivots(mn),invprm(mn),snhead(mn),nodtyp(mn),
     x inta1(mn),rowidx(cfree),rindex(rfree),factim,
     x iwork1(mn2),iwork2(mn2),iwork3(mn2),iwork4(mn2),iwork5(mn2)

c ---------------------------------------------------------------------------

      integer*4 i,j,err,factyp,pphase,dphase,t1,t2,opphas,odphas
      real*8 pinf,dinf,uinf,prelinf,drelinf,popt,dopt,cgap,
     x prstpl,dustpl,barpar,oper,maxstp,pinfrd,dinfrd,objerr,nonopt,
     x oprelinf,odrelinf,opinf,odinf,ocgap
      integer*4 corr,corrc,barn,fxp,fxd,fxu,nropt
      character*99 buff,sbuff
      character*1 wmark

c to save parameters

      integer*4 maxcco,mxrefo
      real*8 lamo,spdeno,bargro,topto

c --------------------------------------------------------------------------

 101  format(1x,' ')
 102  format(1x,'it-pc   p.inf   d.inf  u.inf   actions           ',
     x 'p.obj           d.obj  barpar')
 103  format(1x,'------------------------------------------------',
     x '------------------------------')
 104  format(1x,i2,a1,i1,i1,' ',0p,f7.1,'  ',0p,f7.1,'  ',0p,f6.0,
     x ' ',i2,' ',i3,' ',i3,' ',1p,d15.8,' ',1p,d15.7,' ',0p,f6.0)

c saving parameters

      maxcco=maxcc
      mxrefo=maxref
      lamo=lam
      spdeno=supdens
      bargro=bargrw
      topto=topt1

c include dummy ranges if requested

      if(regul.gt.0)then
        do i=1,m
          if(slktyp(i).eq.0)then
            slktyp(i)=-1
            bounds(i+n)=0.0d+0
          endif
        enddo
      endif

c other initialization

      nropt=0
      factim=0
      wmark='-'
      fxp=0
      fxd=0
      fxu=0

      call lpstlamb(colpnt,vcstat,rowidx,inta1,fixn,dropn,factyp)
      call lptimer(t1)
      j=0
      do i=1,n
        if((vcstat(i).gt.-2).and.(vartyp(i).eq.0))j=j+1
      enddo
      if((j.gt.0).and.(scfree.lt.tzer))factyp=1

c initial scaling matrix (diagonal)

      call lpfscale (vcstat,diag,odiag,vartyp,slktyp)
      do i=1,m
        dv(i)=0.0d+0
      enddo

ccc      i=2*rfree
ccc      j=400
ccc      call lppaintmat(m,n,nz,i,rowidx,colpnt,rindex,j,'matrix01.pic')


c initial factorization

      fnzmax=0
      if(factyp.eq.1)then
        call lpffactor(ecolpnt,vcstat,colpnt,rowidx,
     x  iwork4,pivots,count,nonzeros,diag,
     x  iwork1,iwork1(mn+1),iwork2,iwork2(mn+1),inta1,iwork5,
     x  iwork5(mn+1),iwork3,iwork3(mn+1),iwork4(mn+1),rindex,
     x  rwork1,fixn,dropn,fnzmax,fnzmin,active,oper,xs,slktyp,code)
        if(code.ne.0)goto 999
        call lpsupnode(ecolpnt,count,rowidx,vcstat,pivots,snhead,
     x  invprm,nodtyp)
      else

c minimum local fill-in ordering

        i=int(tfind)
        if(order.lt.1.5)i=0
        if(order.lt.0.5)i=-1
        call lpsymmfo(inta1,pivots,ecolpnt,vcstat,
     x  colpnt,rowidx,nodtyp,rindex,iwork3,invprm,
     x  count,snhead,iwork1,iwork1(mn+1),iwork2,iwork2(mn+1),
     x  iwork4,iwork4(mn+1),iwork3(mn+1),iwork5,iwork5(mn+1),
     x  nonzeros,fnzmax,oper,i,rwork1,code)
        if(code.ne.0)goto 999
        call lpsupnode(ecolpnt,count,rowidx,vcstat,pivots,snhead,
     x  invprm,nodtyp)
        popt=trabs
        trabs=tabs
        call lpnfactor(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,err,rwork1,iwork2,iwork2(mn+1),dropn,slktyp,
     x  snhead,iwork3,invprm,nodtyp,dv,odiag)
        trabs=popt
      endif
      fnzmin=fnzmax

c compute centrality and iterative refinement power

      if(fnzmin.eq.0)fnzmin=1
      cgap=oper/fnzmin/10.0d+0
      j=0
  78  if(cgap.ge.1.0d+0)then
        cgap=cgap/2
        j=j+1
        goto 78
      endif
      if(j.eq.0)j=1
      if(maxcc.le.0d+0)then
        maxcc=-maxcc
      else
        if(j.le.maxcc)maxcc=j
      endif
      if(mincc.gt.maxcc)maxcc=mincc
      cgap=log(1.0d+0+oper/fnzmin/5.0d+0)/log(2.0d+00)
      if(maxref.le.0)then
        maxref=-maxref
      else
        maxref=int(cgap*maxref)
      endif
      if(maxref.le.0)maxref=0
      write(buff,'(1x,a,i2)')'centrality correction power:',maxcc
      call lpmprnt(buff)
      write(buff,'(1x,a,i2)')'iterative refinement  power:',maxref
      call lpmprnt(buff)

c starting point

      call lpinitsol(xs,up,dv,dspr,dsup,rhs,obj,bounds,vartyp,slktyp,
     x vcstat,colpnt,ecolpnt,pivots,rowidx,nonzeros,diag,rwork1,
     x count)
      call lptimer(t2)

      write(buff,'(1x,a,f12.2,a)')'firstfactor time :',
     x (dble(t2-t1)*0.01d+0),' sec'
      call lpmprnt(buff)

      maxstp=1.0d+0
      iter=0
      corect=0
      corr=0
      corrc=0
      barn=0
      cgap=0.0d+0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            cgap=cgap+xs(i)*dspr(i)
            barn=barn+1
          endif
          if(j.lt.0)then
            cgap=cgap+up(i)*dsup(i)
            barn=barn+1
          endif
        endif
      enddo
      if(barn.lt.1)barn=1

ccc      i=2*rfree
ccc      j=350
ccc      call lppaintaat(mn,nz,pivotn,i,rowidx,ecolpnt,count,rindex,
ccc     x j,pivots,iwork1,iwork1(mn+1),iwork2,iwork2(mn+1),iwork3,
ccc     x iwork3(mn+1),'normal01.pic')

ccc      i=2*rfree
ccc      j=400
ccc      call lppaintata(mn,nz,pivotn,i,rowidx,ecolpnt,count,rindex,
ccc     x j,pivots,iwork1,iwork1(mn+1),iwork2,iwork2(mn+1),iwork3,
ccc     x 'atapat01.pic')


ccc      i=2*rfree
ccc      j=350
ccc      err=nz
ccc      call lppaintfct(mn,cfree,pivotn,i,rowidx,ecolpnt,count,rindex,
ccc     x j,pivots,iwork2,err,'factor01.pic')

c initialize for the iteration loop

      do i=1,n
        if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
          if(xs(i).gt.dspr(i))then
            vcstat(i)=1
          else
            vcstat(i)=0
          endif
        endif
      enddo
      do i=1,m
        if((vcstat(i+n).gt.-2).and.(slktyp(i).ne.0))then
          if(xs(i+n).gt.dspr(i+n))then
            vcstat(i+n)=1
          else
            vcstat(i+n)=0
          endif
        endif
      enddo
      opphas=0
      odphas=0
      pinfrd=1.0d+0
      dinfrd=1.0d+0
      barpar=0.0d+0

c main iteration loop

  10  if(mod(iter,20).eq.0)then
        write(buff,101)
        call lpmprnt(buff)
        write(buff,102)
        call lpmprnt(buff)
        write(buff,103)
        call lpmprnt(buff)
      endif

c infeasibilities

      call lpcprinf(xs,prinf,slktyp,colpnt,rowidx,nonzeros,
     x rhs,vcstat,pinf)
      call lpcduinf(dv,dspr,dsup,duinf,vartyp,slktyp,colpnt,rowidx,
     x nonzeros,obj,vcstat,dinf)
      call lpcupinf(xs,up,upinf,bounds,vartyp,slktyp,vcstat,
     x uinf)

c objectives

      call lpcpdobj(popt,dopt,obj,rhs,bounds,xs,dv,dsup,
     x vcstat,vartyp,slktyp)
      popt=scobj*popt+addobj
      dopt=scobj*dopt+addobj

c stopping criteria

      call lpstpcrt(prelinf,drelinf,popt,dopt,cgap,iter,
     x code,pphase,dphase,maxstp,pinf,uinf,dinf,
     x prinf,upinf,duinf,nonopt,pinfrd,dinfrd,
     x prstpl,dustpl,obj,rhs,bounds,xs,dxs,dspr,ddspr,dsup,ddsup,dv,ddv,
     x up,addobj,scobj,vcstat,vartyp,slktyp,
     x oprelinf,odrelinf,opinf,odinf,ocgap,opphas,odphas,sbuff)

      write(buff,104)iter,wmark,corr,corrc,pinf,dinf,uinf,fxp,fxd,fxu,
     x popt,dopt,barpar
      call lpmprnt(buff)
      if(code.ne.0)then
        write(buff,'(1x)')
        call lpmprnt(buff)
        call lpmprnt(sbuff)
        goto 90
      endif

c p-d solution modification

      call lppdmodi(xs,dspr,vcstat,vartyp,slktyp,cgap,popt,
     x dopt,prinf,duinf,upinf,colpnt,rowidx,nonzeros,pinf,uinf,dinf)

c fixing variables / dropping rows / handling dual slacks

      i=fixn
      call lpvarfix(vartyp,slktyp,rhs,colpnt,rowidx,nonzeros,
     x xs,up,dspr,dsup,vcstat,fixn,dropn,addobj,scobj,obj,bounds,
     x duinf,dinf,fxp,fxd,fxu)
      if(fixn.ne.i)then
        call lpsupupd(pivots,invprm,snhead,nodtyp,vcstat,ecolpnt)
        call lpcprinf(xs,prinf,slktyp,colpnt,rowidx,nonzeros,
     x  rhs,vcstat,pinf)
        call lpcupinf(xs,up,upinf,bounds,vartyp,slktyp,vcstat,
     x  uinf)
      endif

c compute gap

      cgap=0.0d+0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            cgap=cgap+xs(i)*dspr(i)
            if(j.lt.0)then
              cgap=cgap+up(i)*dsup(i)
            endif
          endif
        endif
      enddo

c computation of the scaling matrix

      objerr=abs(dopt-popt)/(abs(popt)+1.0d+0)
      call lpcdiag(xs,up,dspr,dsup,vartyp,slktyp,vcstat,diag,odiag)
      pinfrd=pinf
      dinfrd=dinf

c the actual factorization

  50  err=0
      call lptimer(t1)
      if (factyp.eq.1) then
        call lpmfactor(ecolpnt,vcstat,colpnt,rowidx,pivots,
     x  count,iwork4,nonzeros,diag,err,rwork1,iwork2,iwork2(mn+1),
     x  dropn,slktyp,snhead,iwork3,invprm,nodtyp,dv,odiag)
      else
        call lpnfactor(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,err,rwork1,iwork2,iwork2(mn+1),dropn,slktyp,
     x  snhead,iwork3,invprm,nodtyp,dv,odiag)
      endif
      call lptimer(t2)
      if(err.gt.0)then
        do i=1,mn
          diag(i)=odiag(i)
        enddo
        call lpnewsmf(colpnt,pivots,rowidx,nonzeros,ecolpnt,count,
     x  vcstat,invprm,snhead,nodtyp,iwork1,rwork1,iwork2,iwork3,
     x  iwork4,code)
        if(code.lt.0)then
          write(buff,'(1x)')
          call lpmprnt(buff)
          goto 90
        endif
        goto 50
      endif
      factim=factim+t2-t1

c we are in the finish ?

      wmark(1:1)='-'
      if(objerr.gt.1.0d+0)objerr=1.0d+0
      if(objerr.lt.topt1)objerr=topt1
      if((objerr.le.topt1*10.0d+0).and.(pphase+dphase.eq.4))then
         if(bargrw.gt.0.1d+0)bargrw=0.1d+0
         nropt=nropt+1
         if(nropt.eq.5)then
           nropt=0
           topt1=topt1*sqrt(10.d+0)
           write(buff,'(1x,a)')'near otptimal but slow convergence.'
           call lpmprnt(buff)
         endif
         wmark(1:1)='+'
      endif

c primal-dual predictor-corrector direction

      call lp cpdpcd(xs,up,dspr,dsup,prinf,duinf,upinf,
     x dxsn,ddvn,ddsprn,ddsupn,dxs,ddv,ddspr,ddsup,bounds,
     x ecolpnt,count,pivots,vcstat,diag,odiag,rowidx,nonzeros,
     x colpnt,vartyp,slktyp,barpar,corr,prstpl,dustpl,barn,cgap)
      corect=corect+corr

c primal-dual centality-correction

      call lp cpdccd(xs,up,dspr,dsup,upinf,
     x dxsn,ddvn,ddsprn,ddsupn,dxs,ddv,ddspr,ddsup,bounds,
     x ecolpnt,count,pivots,vcstat,diag,odiag,rowidx,nonzeros,
     x colpnt,vartyp,slktyp,barpar,corrc,prstpl,dustpl)
      corect=corect+corrc

c compute steplengths

      iter=iter+1
      prstpl=prstpl*palpha
      dustpl=dustpl*dalpha

c compute the new primal-dual solution

      call lpcnewpd(prstpl,xs,dxs,up,upinf,dustpl,dv,ddv,dspr,
     x ddspr,dsup,ddsup,vartyp,slktyp,vcstat,maxstp)

c end main loop

      goto 10

 90   opt=(dopt-popt)/(abs(popt)+1.0d+0)
      write(buff,'(1x,a,1pd11.4,a,1pd18.10)')
     x 'absolute infeas.   primal  :',pinf,   '    dual         :',dinf
      call lpmprnt(buff)
      write(buff,'(1x,a,1pd11.4,a,1pd18.10)')
     x 'primal :  relative infeas. :',prelinf,'    objective    :',popt
      call lpmprnt(buff)
      write(buff,'(1x,a,1pd11.4,a,1pd18.10)')
     x 'dual   :  relative infeas. :',drelinf,'    objective    :',dopt
      call lpmprnt(buff)
      write(buff,'(1x,a,1pd11.4,a,1pd18.10)')
     x 'complementarity gap        :',cgap,'    duality gap  :',opt
      call lpmprnt(buff)
      opt=popt

c restoring parameters

 999  maxcc=maxcco
      maxref=mxrefo
      lam=lamo
      supdens=spdeno
      bargrw=bargro
      topt1=topto
      return
      end

c ===========================================================================
c ========================================================================

      subroutine lpaggreg(colpnt,colidx,colnzs,rowidx,
     x colsta,rowsta,colbeg,colend,rowbeg,rowend,
     x rhs,obj,prehis,prelen,mrk,vartyp,slktyp,iwrk1,iwrk2,
     x iwrk3,pivcol,pivrow,rwork,addobj,prelev,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      integer*4 colpnt(n1),colidx(cfree),rowidx(rfree),
     x colbeg(n),colend(n),rowbeg(m),rowend(m),slktyp(m),
     x colsta(n),rowsta(m),prehis(mn),prelen,prelev,code,mrk(mn),
     x iwrk1(mn),iwrk2(mn),iwrk3(mn),pivcol(n),pivrow(m),vartyp(n)
      real*8  colnzs(cfree),addobj,rhs(m),obj(n),
     x rwork(m)

      real*8 reltol,abstol,redtol,filtol
      integer*4 i,j,k,pnt1,pnt2,nfill,pnt,pnto,procn,fpnt
      character*99 buff

      reltol=1.0d-3
      abstol=1.0d-5
      redtol=1.0d-4
      filtol=4.0d+0

      if(iand(prelev,128).gt.0)then
        procn=8
        pnto=colpnt(1)
        pnt=nz+1
        do i=1,n
          if(colsta(i).gt.-2)then
            pnt1=colbeg(i)
            pnt2=colend(i)
            colbeg(i)=pnt
            do j=pnt1,pnt2
              colidx(pnt)=colidx(j)
              colnzs(pnt)=colnzs(j)
              pnt=pnt+1
            enddo
            colend(i)=pnt-1
            pnt1=pnt2+1
          else
            pnt1=colpnt(i)
          endif
          pnt2=colpnt(i+1)-1
          colpnt(i)=pnto
          do j=pnt1,pnt2
            colidx(pnto)=colidx(j)
            colnzs(pnto)=colnzs(j)
            pnto=pnto+1
          enddo
        enddo
        colpnt(n+1)=pnto
        call lpelimin(m,n,nz,cfree,rfree,
     x  colbeg,colend,rowbeg,rowend,colidx,rowidx,colnzs,colsta,rowsta,
     x  obj,rhs,vartyp,slktyp,
     x  iwrk1,iwrk2,iwrk1(n+1),iwrk2(n+1),mrk,mrk(n+1),
     x  iwrk3,iwrk3(n+1),rwork,pivcol,pivrow,abstol,reltol,filtol,
     x  pivotn,nfill,addobj,fpnt,code)
        if(code.ne.0)goto 999

c compute new column lengths

        do i=1,n
          iwrk1(i)=colpnt(i+1)-colpnt(i)
          if(colsta(i).gt.-2)iwrk1(i)=iwrk1(i)+colend(i)-colbeg(i)+1
        enddo
        do j=1,pivotn
          i=pivrow(j)
          pnt1=rowbeg(i)
          pnt2=rowend(i)
          do k=pnt1,pnt2
            iwrk1(colidx(k))=iwrk1(colidx(k))+1
          enddo
        enddo

c generate new pointers for columns

        pnt=1
        do i=1,n
          iwrk2(i)=pnt
          pnt=pnt+iwrk1(i)
        enddo
        if(pnt.gt.fpnt)then
          code=-2
          write(buff,'(1x,a)')'ran out of reral memory'
          call lpmprnt(buff)
          goto 999
        endif

c assemble the transformed matrix

        do i=n,1,-1
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          pnt=iwrk2(i)+iwrk1(i)-1
          do j=pnt2,pnt1,-1
            colidx(pnt)=colidx(j)
            colnzs(pnt)=colnzs(j)
            pnt=pnt-1
          enddo
          if(colsta(i).gt.-2)then
            fpnt=iwrk2(i)
            pnt1=colbeg(i)
            pnt2=colend(i)
            do j=pnt1,pnt2
              colidx(fpnt)=colidx(j)
              colnzs(fpnt)=colnzs(j)
              fpnt=fpnt+1
            enddo
            iwrk3(i)=fpnt
          endif
        enddo
        colpnt(1)=1
        do i=1,n
          colbeg(i)=iwrk2(i)
          colend(i)=iwrk3(i)-1
          colpnt(i+1)=colpnt(i)+iwrk1(i)
        enddo
        do j=1,pivotn
          i=pivrow(j)
          pnt1=rowbeg(i)
          pnt2=rowend(i)
          do k=pnt1,pnt2
            pnt=iwrk3(colidx(k))
            colidx(pnt)=i
            colnzs(pnt)=colnzs(k)
            iwrk3(colidx(k))=pnt+1
          enddo
        enddo

        do i=1,pivotn
          prelen=prelen+1
          prehis(prelen)=pivcol(i)
          colsta(pivcol(i))=-2-procn
          prelen=prelen+1
          prehis(prelen)=pivrow(i)+n
          rowsta(pivrow(i))=-2-procn
        enddo
        write(buff,'(1x,i5,a,i5,a)')pivotn,' row/cols eliminated, ',
     x  nfill,' fill-in created.'
        call lpmprnt(buff)
        nz=colpnt(n+1)-1
        if(cfree.lt.nz*2)then
          code=-2
          write(buff,'(1x,a)')'ran out of reral memory'
          call lpmprnt(buff)
        endif
      endif

      if(iand(prelev,256).gt.0)then
        do i=1,m
          rowend(i)=rowbeg(i)-1
        enddo
        do i=1,n
          if(colsta(i).gt.-2)then
            pnt1=colbeg(i)
            pnt2=colend(i)
            do j=pnt1,pnt2
              rowend(colidx(j))=rowend(colidx(j))+1
              rowidx(rowend(colidx(j)))=i
              colnzs(nz+rowend(colidx(j)))=colnzs(j)
            enddo
          endif
        enddo
        call lpsparser(n,n1,m,nz,colpnt,colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,colnzs(nz+1),colsta,rowsta,rhs,slktyp,
     x  mrk,mrk(n+1),tplus,tzer,redtol,reltol,abstol)
      endif

 999  return
      end

c ============================================================================
c ===========================================================================

      subroutine lpbndchk(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x cnum,list,mrk,procn,oldlob,oldupb,
     x ppbig,pmaxr,pmbig,pminr,chglob,chgupb,
     x big,lbig,tfeas,search,chgmax,bigbou,code)

c this subroutine checks bounds on variables
c note : this subroutine destroys min and max row activity counters !

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,list(n),mrk(n),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x ppbig(m),pmbig(m),chglob(n),chgupb(n),search,chgmax,code

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),addobj,big,lbig,bigbou,
     x tfeas,oldlob(n),oldupb(n)

      integer*4 i,j,up,row,col,dir,pnt1,pnt2,p1,p2,crem,rrem,up3,lstcnt
      real*8  sol,toler,up1,up2
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      crem=0
      rrem=0
      cnum=0
      do i=1,n
        chglob(i)=0
        chgupb(i)=0
        oldupb(i)=upb(i)
        oldlob(i)=lob(i)
      enddo
      do i=1,m
        if(ups(i).gt.lbig)then
          pmbig(i)=pmbig(i)+1
        else
          pminr(i)=pminr(i)-ups(i)
        endif
        if(los(i).lt.-lbig)then
          ppbig(i)=ppbig(i)+1
        else
          pmaxr(i)=pmaxr(i)-los(i)
        endif
        if(rowsta(i).gt.-2)then
          cnum=cnum+1
          mrk(i)=1
          list(cnum)=i
         else
          mrk(i)=-2
        endif
      enddo

      lstcnt=0
      do while (cnum.ne.lstcnt)
        lstcnt=lstcnt+1
        if(lstcnt.gt.m)then
          lstcnt=1
          search=search-1
          if(search.eq.0)goto 100
        endif
        row=list(lstcnt)
        mrk(row)=-1
        pnt1=rowbeg(row)
        pnt2=rowend(row)

        do i=pnt1,pnt2
          col=rowidx(i)

c compute new upper bound: lo1+(rhs-up2)/nzs

          if(rownzs(i).gt.0.0d+0)then
            up2=pminr(row)
            up3=pmbig(row)
          else
            up2=pmaxr(row)
            up3=ppbig(row)
          endif
          if(lob(col).lt.-lbig)then
            up1=0.0d+0
            up=1
          else
            up1=lob(col)
            up=0
          endif
          if(up.eq.up3)then
            sol=up1+(rhs(row)-up2)/rownzs(i)
            toler=(abs(sol)+1.0d+0)*tfeas
             if(abs(sol).lt.bigbou)then
              if(upb(col)-sol.gt.toler)then
                chgupb(col)=chgupb(col)+1
                p1=colbeg(col)
                p2=colend(col)
                dir=1
                if(lob(col)-sol.gt.toler)then
                  cnum=-col
                  code=4
                  goto 999
                endif
                if(sol-lob(col).lt.toler)then
                  sol=lob(col)
                endif
                call lpmodmxm(nz,p1,p2,upb(col),sol,colidx,colnzs,
     x          ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
                upb(col)=sol
                if(chgupb(col).lt.chgmax)then
                  do j=p1,p2
                    if(mrk(colidx(j)).eq.-1)then
                      cnum=cnum+1
                      if(cnum.gt.m)cnum=1
                      list(cnum)=colidx(j)
                      mrk(colidx(j))=1
                    endif
                  enddo
                endif
              endif
            endif
          endif

c compute new lower bound: up1+(rhs-up2)/nzs

          if(rownzs(i).gt.0.0d+0)then
            up2=pmaxr(row)
            up3=ppbig(row)
          else
            up2=pminr(row)
            up3=pmbig(row)
          endif
          if(upb(col).gt.lbig)then
            up1=0.0d+0
            up=1
          else
            up1=upb(col)
            up=0
          endif
          if(up.eq.up3)then
            sol=up1+(rhs(row)-up2)/rownzs(i)
            toler=(abs(sol)+1.0d+0)*tfeas
             if(abs(sol).lt.bigbou)then
              if(sol-lob(col).gt.(abs(sol)+1.0d+0)*tfeas)then
                chglob(col)=chglob(col)+1
                p1=colbeg(col)
                p2=colend(col)
                dir=-1
                if((sol-upb(col)).gt.toler)then
                  cnum=-col
                  code=4
                  goto 999
                endif
                if(upb(col)-sol.lt.toler)then
                  sol=upb(col)
                endif
                call lpmodmxm(nz,p1,p2,lob(col),sol,colidx,colnzs,
     x          ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
                lob(col)=sol
                if(chglob(col).lt.chgmax)then
                  do j=p1,p2
                    if(mrk(colidx(j)).eq.-1)then
                      cnum=cnum+1
                      if(cnum.gt.m)cnum=1
                      list(cnum)=colidx(j)
                      mrk(colidx(j))=1
                    endif
                  enddo
                endif
              endif
            endif
          endif
        enddo
      enddo

c checking row feasibility

  100 cnum=0
      do row=1,m
        if(rowsta(row).gt.-2)then
          sol=(abs(rhs(row))+1.0d+0)*tfeas
          if((ppbig(row).eq.0).and.(pmaxr(row)-rhs(row).lt.-sol))then
            code=3
            cnum=-row-n
            goto 999
          endif
          if((pmbig(row).eq.0).and.(rhs(row)-pminr(row).lt.-sol))then
            code=3
            cnum=-row-n
            goto 999
          endif
        endif
      enddo

c bound check and modification

      do col=1,n
        if(colsta(col).gt.-2)then
          if((lob(col)-upb(col)).gt.(abs(lob(col))+1.0d+0)*tfeas)then
            code=4
            cnum=-col
            goto 999
          else if(upb(col)-lob(col).lt.abs(lob(col)+1.0d+0)*tfeas)then
            sol=(upb(col)+lob(col))/2.0d+0
            prelen=prelen+1
            prehis(prelen)=col
            colsta(col)=-2-procn
            traf=.true.
            call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x      colbeg,colend,rowbeg,rowend,rhs,sol,traf)
            crem=crem+1
            addobj=addobj+obj(col)*sol
            upb(col)=sol
            lob(col)=sol
          else
            if(chglob(col).gt.0)then
              if(oldlob(col).gt.-lbig)rrem=rrem+1
              lob(col)=-big
            endif
            if(chgupb(col).gt.0)then
              if(oldupb(col).lt.lbig)rrem=rrem+1
              upb(col)=big
            endif
          endif
        endif
      enddo

 999  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'bndchk:',crem,' columns,',rrem,' bounds removed'
        call lpmprnt(buff)
      endif
      return
      end

c ===========================================================================
c scaling of free variables : "average" of basics * scfree
c correcting : "average" of basics * varadd

c ===========================================================================

      subroutine lpcdiag(xs,up,dspr,dsup,vartyp,slktyp,vcstat,diag,
     x odiag)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpmscal/ varadd,slkadd,scfree
      real*8        varadd,slkadd,scfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      integer*4 vartyp(n),slktyp(m),vcstat(mn)
      real*8 xs(mn),up(mn),dspr(mn),dsup(mn),diag(mn),odiag(mn)

      integer*4 i,j
      real*8 sol,sn,sm,mins

c ---------------------------------------------------------------------------

      sn=0.0d+0
      mins=1.0d+0
      j=0
      do i=1,n
        sol=0.0d+0
        if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
          if(vartyp(i).lt.0)then
            sol=dspr(i)/xs(i)+dsup(i)/up(i)
          else
            sol=dspr(i)/xs(i)
          endif

c compute average on "basic" variables

          if(mins.gt.sol)mins=sol
          if(vcstat(i).gt.0)then
            j=j+1
            sn=sn+log(sol)
          endif
        endif
        diag(i)=sol
        odiag(i)=sol
      enddo

c compute geometric mean of the "basics"

      if(j.eq.0)j=1
      sol=exp(sn/dble(j))

c set scale parameter for free variables

      if(abs(scfree).lt.tzer)then
        sn=0.0d+0
      else if(scfree.lt.0.0d+0)then
        sn=-scfree
      else
        sn=max(sol*scfree,mins)
      endif

c set regularization parameter

      if(abs(varadd).lt.tzer)then
        sm=0.0d+0
      else if(varadd.lt.0.0d+0)then
        sm=-varadd
      else
        sm=sol*varadd
      endif

c second pass: set free variables and regularize

      do i=1,n
        if(vcstat(i).gt.-2)then
          if(vartyp(i).eq.0)then
            sol=sn
          else
            sol=diag(i)
          endif
ccc          if(sol.lt.sm*sm)sol=sm*sqrt(sol)
          if(sol.lt.sm)sol=sm*sqrt(sol/sm)
          diag(i)=-sol
          odiag(i)=-sol
        endif
      enddo



      j=0
      sn=0.0d+0
      do i=1,m
        sol=0.0d+0
        if(vcstat(i+n).gt.-2)then
          if(slktyp(i).eq.0)then
            sol=0.0d+0
          else
            if(slktyp(i).lt.0)then
              sol=1.0d+0/(dspr(i+n)/xs(i+n)+dsup(i+n)/up(i+n))+0.0d+0
            else
              sol=xs(i+n)/dspr(i+n)
            endif
            if(vcstat(i+n).gt.0)then
              j=j+1
              sn=sn+log(sol)
            endif
          endif
        endif
        diag(i+n)=sol
        odiag(i+n)=sol
      enddo

      if(j.eq.0)j=1
      if(abs(slkadd).lt.tzer)then
        sm=0.0d+0
      else if(slkadd.lt.0.0d+0)then
        sm=-slkadd
      else
        sm=exp(sn/dble(j))*slkadd
      endif

      if(sm.gt.tzer)then
        do i=1,m
          if(vcstat(i+n).gt.-2)then
            sol=diag(i+n)
ccc            if(sol.gt.sm*sm)sol=sm*sqrt(sol)
            if(sol.gt.sm)sol=sm*sqrt(sol/sm)
            diag(i+n)=sol
            odiag(i+n)=sol
          endif
        enddo
      endif
      return
      end

c ===========================================================================
c multi-centrality corrections

c ===========================================================================

      subroutine lpcpdccd(xs,up,dspr,dsup,upinf,
     x dxsn,ddvn,ddsprn,ddsupn,dxs,ddv,ddspr,ddsup,bounds,
     x ecolpnt,count,pivots,vcstat,diag,odiag,rowidx,nonzeros,
     x colpnt,vartyp,slktyp,barpar,corr,prstpl,dustpl)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      common/lppredc/ target,tsmall,tlarge,center,corstp,mincc,maxcc
      real*8        target,tsmall,tlarge,center,corstp
      integer*4     mincc,maxcc

      integer*4 ecolpnt(mn),count(mn),vcstat(mn),rowidx(cfree),
     x pivots(mn),colpnt(n1),vartyp(n),slktyp(m),corr
      real*8 xs(mn),up(mn),dspr(mn),dsup(mn),
     x upinf(mn),dxsn(mn),ddvn(m),ddsprn(mn),ddsupn(mn),
     x dxs(mn),ddv(m),ddspr(mn),ddsup(mn),bounds(mn),
     x diag(mn),odiag(mn),nonzeros(cfree),barpar,prstpl,dustpl

      integer*4 i,j,cr,maxccx
      real*8 s,ss,ostp,ostd,prs,dus,dp

c ---------------------------------------------------------------------------
      maxccx=maxcc
      cr=0
      ostp=prstpl
      ostd=dustpl
      if(maxcc.le.0)goto 999
      cr=1

c define target

   1  prs=prstpl*(target+1.0d+0)+target
      dus=dustpl*(target+1.0d+0)+target
      if (prs.ge.1.0d+0)prs=1.0d+0
      if (dus.ge.1.0d+0)dus=1.0d+0

      do 10 j=1,n
        if(vcstat(j).le.-2)then
          dxsn(j)=0.0d+0
          goto 10
        endif
        if(vartyp(j).eq.0)then
          dxsn(j)=0.0d+0
          goto 10
        endif
        dp=(xs(j)+prs*dxs(j))*(dspr(j)+dus*ddspr(j))
        if (dp.le.tsmall*barpar)then
          s=barpar-dp
        else if(dp.ge.tlarge*barpar)then
          s=-center*barpar
        else
          s=0.0d+0
        endif

        if(vartyp(j).gt.0)then
          dxsn(j)=-s/xs(j)
          goto 10
        endif

        dp=(up(j)+prs*(upinf(j)-dxs(j)))*(dsup(j)+dus*ddsup(j))
        if (dp.le.tsmall*barpar)then
          ss=barpar-dp
        else if(dp.ge.tlarge*barpar)then
          ss=-center*barpar
        else
          ss=0.0d+0
        endif
        dxsn(j)=-s/xs(j)+ss/up(j)
  10  continue

      do 20 i=1,m
        j=i+n
        if(vcstat(j).le.-2)then
          dxsn(j)=0.0d+0
          goto 20
        endif
        if(slktyp(i).eq.0)then
          dxsn(j)=0.0d+0
          goto 20
        endif

c bounded variable

        dp=(xs(j)+prs*dxs(j))*(dspr(j)+dus*ddspr(j))
        if (dp.le.tsmall*barpar)then
          s=barpar-dp
        else if (dp.ge.tlarge*barpar)then
          s=-center*barpar
        else
          s=0.0d+0
        endif
        if(slktyp(i).gt.0)then
          dxsn(j)=s/xs(j)*odiag(j)
          goto 20
        endif

c upper bounded variable

        dp=(up(j)+prs*(upinf(j)-dxs(j)))*(dsup(j)+dus*ddsup(j))
        if (dp.le.tsmall*barpar)then
          ss=barpar-dp
        else if(dp.ge.tlarge*barpar)then
          ss=-center*barpar
        else
          ss=0.0d+0
        endif
        dxsn(j)=(s/xs(j)-ss/up(j))*odiag(j)
  20  continue

c solve the augmented system

      call lpcitref(diag,odiag,pivots,rowidx,nonzeros,colpnt,
     x ecolpnt,count,vcstat,dxsn,ddsprn,ddsupn,upinf,
     x bounds,xs,up,vartyp,slktyp)

c primal and dual variables

      do 30 i=1,m
        j=i+n
        if(vcstat(j).le.-2)goto 30
        ddvn(i)=ddv(i)+dxsn(j)
        if(slktyp(i).eq.0)goto 30
        dp=(xs(j)+prs*dxs(j))*(dspr(j)+dus*ddspr(j))
        if (dp.le.tsmall*barpar)then
          s=barpar-dp
        else if (dp.ge.tlarge*barpar)then
          s=-center*barpar
        else
          s=0.0d+0
        endif
        if(slktyp(i).gt.0)then
          dxsn(j)=-odiag(j)*(dxsn(j)-s/xs(j))
          goto 30
        endif
        dp=(up(j)+prs*(upinf(j)-dxs(j)))*(dsup(j)+dus*ddsup(j))
        if (dp.le.tsmall*barpar)then
          ss=barpar-dp
        else if(dp.ge.tlarge*barpar)then
          ss=-center*barpar
        else
          ss=0.0d+0
        endif
        dxsn(j)=-odiag(j)*(dxsn(j)-s/xs(j)+ss/up(j))
  30  continue

c primal upper bounds, dual slacks

      do 40 i=1,mn
        if(vcstat(i).le.-2)goto 40
        if(i.le.n)then
          j=vartyp(i)
        else
          j=slktyp(i-n)
        endif
        if(j.eq.0)then
          if(i.le.n)then
            ddsprn(i)=ddsprn(i)+ddspr(i)
          endif
          goto 45
        endif
        dp=(xs(i)+prs*dxs(i))*(dspr(i)+dus*ddspr(i))
        if (dp.le.tsmall*barpar)then
          s=barpar-dp
        else if(dp.ge.tlarge*barpar)then
          s=-center*barpar
        else
          s=0.0d+0
        endif
        ddsprn(i)=(s-dspr(i)*dxsn(i))/xs(i)+ddspr(i)
        if(j.lt.0)then
          dp=(up(i)+prs*(upinf(i)-dxs(i)))*(dsup(i)+dus*ddsup(i))
          if (dp.le.tsmall*barpar)then
            ss=barpar-dp
          else if(dp.ge.tlarge*barpar)then
            ss=-center*barpar
          else
            ss=0.0d+0
          endif
          ddsupn(i)=(ss+dsup(i)*dxsn(i))/up(i)+ddsup(i)
        endif
  45    dxsn(i)=dxsn(i)+dxs(i)
  40  continue

c compute primal and dual steplengths

      call lpcstpln(prstpl,xs,dxsn,up,upinf,
     x dustpl,dspr,ddsprn,dsup,ddsupn,vartyp,slktyp,vcstat)

c check corrections criteria

      if(cr.gt.mincc)then
        if(min(prstpl,dustpl).lt.corstp*min(ostp,ostd))then
          if(min(prstpl,dustpl).lt.min(ostp,ostd))then
            prstpl=ostp
            dustpl=ostd
            cr=cr-1
            goto 999
          else
            maxccx=cr
          endif
        endif
      endif

c continue correcting, change the actual search direction

      ostp=prstpl
      ostd=dustpl
      do i=1,mn
        dxs(i)=dxsn(i)
        ddspr(i)=ddsprn(i)
        ddsup(i)=ddsupn(i)
      enddo
      do i=1,m
        ddv(i)=ddvn(i)
      enddo
      if(cr.lt.maxccx)then
        cr=cr+1
        goto 1
      endif

c end of the correction loop, save the number of the corrections

 999  corr=cr
      return
      end

c ============================================================================
c multi predictor-corrector direction (merothra)

c ===========================================================================

      subroutine lpcpdpcd(xs,up,dspr,dsup,prinf,duinf,upinf,
     x dxsn,ddvn,ddsprn,ddsupn,dxs,ddv,ddspr,ddsup,bounds,
     x ecolpnt,count,pivots,vcstat,diag,odiag,rowidx,nonzeros,
     x colpnt,vartyp,slktyp,barpar,corr,prstpl,dustpl,barn,cgap)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      common/lppredp/ ccstop,barset,bargrw,barmin,mincor,maxcor,inibar
      real*8        ccstop,barset,bargrw,barmin
      integer*4     mincor,maxcor,inibar

      integer*4 ecolpnt(mn),count(mn),vcstat(mn),rowidx(cfree),
     x pivots(mn),colpnt(n1),vartyp(n),slktyp(m),corr,barn
      real*8 xs(mn),up(mn),dspr(mn),dsup(mn),prinf(m),duinf(mn),
     x upinf(mn),dxsn(mn),ddvn(m),ddsprn(mn),ddsupn(mn),
     x dxs(mn),ddv(m),ddspr(mn),ddsup(mn),bounds(mn),
     x diag(mn),odiag(mn),nonzeros(cfree),barpar,prstpl,dustpl,cgap

      integer*4 i,j,cr,mxcor
      real*8 sol,sb,ogap,ngap,obpar,ostp,ostd

c ---------------------------------------------------------------------------

c compute ogap

      ogap=cgap
      if(barpar.lt.tzer)barpar=ogap/dble(barn)*barset
      obpar=barpar
      if(inibar.le.0)then
        barpar=0.0d+0
      else
        barpar=ogap/dble(barn)*barset
        if(barpar.gt.obpar*bargrw)barpar=obpar*bargrw
      endif

      cr=0
      mxcor=maxcor

c initialize : reset

      do i=1,m
        ddv(i)=0.0d+0
      enddo
      do i=1,mn
        dxs(i)=0.0d+0
        ddspr(i)=0.0d+0
        ddsup(i)=0.0d+0
      enddo

c affine scaling / primal-dual direction

      do i=1,n
        sol=0.0d+0
        if(vcstat(i).gt.-2)then
          if(vartyp(i))10,11,12
  10      sol=duinf(i)+dspr(i)-barpar/xs(i)
     x    -dsup(i)+(barpar-dsup(i)*upinf(i))/up(i)
          goto 15
  11      sol=duinf(i)
          goto 15
  12      sol=duinf(i)+dspr(i)-barpar/xs(i)
        endif
  15    dxsn(i)=sol
      enddo

      do i=1,m
       j=i+n
       sol=0.0d+0
       if(vcstat(j).gt.-2)then
         if(slktyp(i))20,21,22
  20     sol=-(duinf(j)+dspr(j)-barpar/xs(j)
     x   -dsup(j)+(barpar-dsup(j)*upinf(j))/up(j))*odiag(j)
         goto 25
  21     sol=0.0d+0
         goto 25
  22     sol=-(duinf(j)+dspr(j)-barpar/xs(j))*odiag(j)
       endif
  25   dxsn(j)=prinf(i)+sol
      enddo

c solve the augmented system

      if(cr.lt.mincor)then
        call lpaugftr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,dxsn)
        call lpaugbtr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,dxsn)
      else
        call lpcitref(diag,odiag,pivots,rowidx,nonzeros,colpnt,
     x  ecolpnt,count,vcstat,dxsn,ddsprn,ddsupn,upinf,
     x  bounds,xs,up,vartyp,slktyp)
      endif

c primal and dual variables
c primal slacks : ds=d_s^{-1}*(b_s+dy)

      do i=1,m
        j=i+n
        if(vcstat(j).gt.-2)then
          ddvn(i)=dxsn(j)
          if(slktyp(i).ne.0)then
            if(slktyp(i).gt.0)then
              sb=duinf(j)+dspr(j)-barpar/xs(j)
            else
              sb=duinf(j)+dspr(j)-barpar/xs(j)
     x        -dsup(j)+(barpar-dsup(j)*upinf(j))/up(j)
            endif
            dxsn(j)=-odiag(j)*(ddvn(i)+sb)
          endif
        endif
      enddo

c primal upper bounds, dual slacks
c dz=-z+x^{-1}(mu -dx*dz -z*dx)

      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.lt.0)then
            ddsupn(i)=-dsup(i)+(barpar-dsup(i)*(upinf(i)-dxsn(i)))/up(i)
          endif
          if(j.ne.0)then
            ddsprn(i)=-dspr(i)+(barpar-dspr(i)*dxsn(i))/xs(i)
          else if(i.le.n)then
            ddsprn(i)=-dspr(i)
          endif
        endif
      enddo

c compute primal and dual steplengths

      call lpcstpln(prstpl,xs,dxsn,up,upinf,
     x dustpl,dspr,ddsprn,dsup,ddsupn,vartyp,slktyp,vcstat)

c estimate basic variables vcstat(i)=1 for basic, 0 for nonbasic

      do i=1,n
        if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
          if(abs(ddsprn(i))*xs(i).gt.abs(dxsn(i))*dspr(i))then
            vcstat(i)=1
          else
            vcstat(i)=0
          endif
        endif
      enddo
      do i=1,m
        if((vcstat(i+n).gt.-2).and.(slktyp(i).ne.0))then
          if(abs(ddsprn(i+n))*xs(i+n).gt.abs(dxsn(i+n))*dspr(i+n))then
            vcstat(i+n)=1
          else
            vcstat(i+n)=0
          endif
        endif
      enddo

c compute ngap

      ngap=0.0d+0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            ngap=ngap+(xs(i)+prstpl*dxsn(i))*(dspr(i)+dustpl*ddsprn(i))
            if(j.lt.0)then
              ngap=ngap+(up(i)+prstpl*(upinf(i)-dxsn(i)))*
     x        (dsup(i)+dustpl*ddsupn(i))
            endif
          endif
        endif
      enddo
      cgap=ngap/dble(barn)
      ostp=prstpl
      ostd=dustpl
      do i=1,mn
        dxs(i)=dxsn(i)
        ddspr(i)=ddsprn(i)
        ddsup(i)=ddsupn(i)
      enddo
      do i=1,m
        ddv(i)=ddvn(i)
      enddo

c compute barrier

      barpar=ngap*ngap*ngap/(ogap*ogap*dble(barn))
      if(barpar.gt.ogap/dble(barn)*barset)barpar=ogap/dble(barn)*barset
      if(barpar.gt.obpar*bargrw)barpar=obpar*bargrw
      if(barpar.lt.barmin)barpar=0.0d+0
      if(mxcor.le.0)goto 999

c higher order predictor-corrector direction

  50  cr=cr+1
      do i=1,n
        sol=0.0d+0
        if(vcstat(i).gt.-2)then
          if(vartyp(i))30,31,32
  30      sol=duinf(i)+dspr(i)+(ddspr(i)*dxs(i)-barpar)/xs(i)
     x    -dsup(i)-(ddsup(i)*(upinf(i)-dxs(i))-barpar+dsup(i)*
     x    upinf(i))/up(i)
          goto 35
  31      sol=duinf(i)
          goto 35
  32      sol=duinf(i)+dspr(i)+(ddspr(i)*dxs(i)-barpar)/xs(i)
        endif
  35    dxsn(i)=sol
      enddo

      do i=1,m
       j=i+n
       sol=0.0d+0
       if(vcstat(j).gt.-2)then
         if(slktyp(i))40,41,42
  40     sol=-(duinf(j)+dspr(j)+(ddspr(j)*dxs(j)-barpar)/xs(j)
     x   -dsup(j)-(ddsup(j)*(upinf(j)-dxs(j))-barpar+dsup(j)*
     x   upinf(j))/up(j))*odiag(j)
         goto 45
  41     sol=0.0d+0
         goto 45
  42     sol=-(duinf(j)+dspr(j)+(ddspr(j)*dxs(j)-barpar)/xs(j))*odiag(j)
       endif
  45   dxsn(j)=prinf(i)+sol
      enddo

c solve the augmented system

      if(cr.lt.mincor)then
        call lpaugftr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,dxsn)
        call lpaugbtr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,dxsn)
      else
        call lpcitref(diag,odiag,pivots,rowidx,nonzeros,colpnt,
     x  ecolpnt,count,vcstat,dxsn,ddsprn,ddsupn,upinf,
     x  bounds,xs,up,vartyp,slktyp)
      endif

c primal and dual variables
c primal slacks : ds=d_s^{-1}*(b_s+dy)

      do i=1,m
        j=i+n
        if(vcstat(j).gt.-2)then
          ddvn(i)=dxsn(j)
          if(slktyp(i).ne.0)then
            if(slktyp(i).gt.0)then
              sb=duinf(j)+dspr(j)+(ddspr(j)*dxs(j)-barpar)/xs(j)
            else
              sb=duinf(j)+dspr(j)+(ddspr(j)*dxs(j)-barpar)/xs(j)
     x        -dsup(j)-(ddsup(j)*(upinf(j)-dxs(j))-barpar+dsup(j)*
     x         upinf(j))/up(j)
            endif
            dxsn(j)=-odiag(j)*(ddvn(i)+sb)
          endif
        endif
      enddo

c primal upper bounds, dual slacks
c dz=-z+x^{-1}(mu -dx*dz -z*dx)

      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.lt.0)then
            ddsupn(i)=
     x       -dsup(i)+(barpar-ddsup(i)*(upinf(i)-dxs(i))
     x       -dsup(i)*(upinf(i)-dxsn(i)))/up(i)
          endif
          if(j.ne.0)then
            ddsprn(i)=
     x       -dspr(i)+(barpar-ddspr(i)*dxs(i)-dspr(i)*dxsn(i))/xs(i)
          else if(i.le.n)then
            ddsprn(i)=-dspr(i)
          endif
        endif
      enddo

c compute primal and dual steplengths

      call lpcstpln(prstpl,xs,dxsn,up,upinf,dustpl,dspr,
     x ddsprn,dsup,ddsupn,vartyp,slktyp,vcstat)

c compute ngap

      ngap=0.0d+0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            ngap=ngap+(xs(i)+prstpl*dxsn(i))*(dspr(i)+dustpl*ddsprn(i))
            if(j.lt.0)then
              ngap=ngap+(up(i)+prstpl*(upinf(i)-dxsn(i)))*
     x        (dsup(i)+dustpl*ddsupn(i))
            endif
          endif
        endif
      enddo

c check corrections criteria

      if(cr.gt.mincor)then
        if(min(prstpl,dustpl).lt.ccstop*min(ostp,ostd))then
          if(min(prstpl,dustpl).lt.min(ostp,ostd))then
            prstpl=ostp
            dustpl=ostd
            cr=cr-1
            goto 999
          else
            mxcor=cr
          endif
        endif
      endif

c continue correcting, change the actual search direction

      cgap=ngap/dble(barn)
      ostp=prstpl
      ostd=dustpl
      do i=1,mn
        dxs(i)=dxsn(i)
        ddspr(i)=ddsprn(i)
        ddsup(i)=ddsupn(i)
      enddo
      do i=1,m
        ddv(i)=ddvn(i)
      enddo
      if(cr.ge.mxcor)goto 999
      goto 50

c end of the correction loop, save the number of the corrections

 999  corr=cr
      return
      end

c ============================================================================
c fixing variables and dropping rows
c ===========================================================================

      subroutine lpvarfix(vartyp,slktyp,rhs,colpnt,rowidx,nonzeros,
     x xs,up,dspr,dsup,vcstat,fixn,dropn,addobj,scobj,obj,bounds,
     x duinf,dinf,fxp,fxd,fxu)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpdrop/  tfixvar,tfixslack,slklim
      real*8        tfixvar,tfixslack,slklim

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      common/lpmscal/ varadd,slkadd,scfree
      real*8        varadd,slkadd,scfree

      integer*4 colpnt(n1),vartyp(n),slktyp(m),rowidx(nz),
     x vcstat(mn),fixn,dropn,fxp,fxd,fxu
      real*8 rhs(m),nonzeros(nz),xs(mn),up(mn),addobj,scobj,obj(n),
     x dspr(mn),dsup(mn),bounds(mn),duinf(mn),dinf

      integer*4 i,j,pnt1,pnt2
      real*8    sol

c ---------------------------------------------------------------------------

      fxp=0
      fxd=0
      fxu=0
      do i=1,n
        if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
          if((xs(i).lt.tfixvar).or.
     x       ((vartyp(i).lt.0).and.(up(i).lt.tfixvar)))then
            fixn=fixn+1
            fxp=fxp+1
            vcstat(i)=-2
            if(xs(i).lt.tfixvar)then
             xs(i)=0.0d+0
             up(i)=bounds(i)
            else
             xs(i)=bounds(i)
             up(i)=0.0d+0
            endif
            sol=xs(i)
            pnt1=colpnt(i)
            pnt2=colpnt(i+1)-1
            do j=pnt1,pnt2
              rhs(rowidx(j)-n)=rhs(rowidx(j)-n)-sol*nonzeros(j)
            enddo
            addobj=addobj+scobj*obj(i)*sol
          endif
          if (dspr(i).lt.tfixslack)then
            fxd=fxd+1
            duinf(i)=duinf(i)-slklim+dspr(i)
            dspr(i)=slklim
          endif
        endif
      enddo

c release upper bounds

      do i=1,mn
        if(i.le.n)then
          j=vartyp(i)
        else
          j=slktyp(i-n)
        endif
        if((vcstat(i).gt.-2).and.(j.lt.0))then
           if(dsup(i).lt.slklim)then
             fxu=fxu+1
             duinf(i)=duinf(i)-dsup(i)
             dsup(i)=0
             if(i.le.n)then
               vartyp(i)=-j
             else
               slktyp(i-n)=-j
             endif
           endif
        endif
      enddo

c relax rows

      do i=1,m
        j=i+n
        if((vcstat(j).gt.-2).and.(slktyp(i).gt.0))then
          if(dspr(j).lt.tfixslack)then
            fxd=fxd+1
            dropn=dropn+1
            vcstat(j)=-2
          endif
        endif
      enddo

c compute new dual infeasibility

      if((fxd.gt.0).or.(fxu.gt.0))then
         dinf=0.0d+0
         do i=1,mn
           if(vcstat(i).gt.-2)then
             if(abs(duinf(i)).gt.dinf)dinf=abs(duinf(i))
           endif
         enddo
      endif

      return
      end

c ===========================================================================
c ===========================================================================

      subroutine lpchepdu(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x coln,collst,colmrk,rown,rowlst,rowmrk,
     x cnum,list,mrk,procn,
     x ppbig,pmaxr,pmbig,pminr,
     x lbig,tzer,code)

c this subroutine performs the "cheap" dual tests

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,list(n),mrk(n),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x coln,rown,collst(n),rowlst(n),colmrk(n),rowmrk(m),
     x ppbig(m),pmbig(m),code

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),addobj,lbig,tzer

      integer*4 i,j,k,l,row,col,dir,p,p1,p2,mode,crem,rrem
      real*8  pivot,sol
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      crem=0
      rrem=0
  10  if(cnum.ge.1)then
        col=list(1)
        mrk(col)=-1
        list(1)=list(cnum)
        cnum=cnum-1

        p1=colbeg(col)
        p2=colend(col)
        mode=0
        do i=p1,p2
          if (abs(colnzs(i)).gt.tzer)then
            row=colidx(i)
            if(ups(row).gt.lbig)then
              k=1
            else if(los(row).lt.-lbig)then
              k=-1
            else
              goto 10
            endif
            if(colnzs(i).gt.0.0d+0)then
              j=1
            else
              j=-1
            endif
            if(mode.eq.0)then
              mode=j*k
              if((obj(col)*dble(mode)).gt.0.0d+0)goto 10
            else
              if(j*k*mode.lt.0)goto 10
            endif
          endif
        enddo

c check the column

        if(mode.gt.0)then
          sol=upb(col)
        else if(mode.lt.0)then
          sol=lob(col)
        else
          if(obj(col).lt.0.0d+0)then
            sol=upb(col)
          else if(obj(col).gt.0.0) then
            sol=lob(col)
          else
            sol=lob(col)
            if(upb(col).ge.lbig)sol=upb(col)
          endif
        endif

c adminisztracio

        dir=-1
        call lpchgmxm(p1,p2,upb(col),lob(col),colidx,colnzs,
     x  ppbig,pmaxr,pmbig,pminr,lbig,dir,m)

        prelen=prelen+1
        prehis(prelen)=col
        colsta(col)=-2-procn
        traf=.true.
        if(abs(sol).gt.lbig)then
          pivot=0.0d+0
        else
          pivot=sol
        endif
        call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x  colbeg,colend,rowbeg,rowend,rhs,pivot,traf)
        crem=crem+1

        if(abs(sol).gt.lbig)then
          if(abs(obj(col)).gt.tzer)then
            cnum=-col
            code=3
            goto 999
          endif

c row redundacncy with the column

          do i=p1,p2
            row=colidx(i)
            if(abs(colnzs(i)).gt.tzer)then
              prelen=prelen+1
              prehis(prelen)=row+n
              rowsta(row)=-2-procn
              traf=.false.
              call lpremove(n,m,nz,row,rowidx,rownzs,colidx,colnzs,
     x        rowbeg,rowend,colbeg,colend,obj,sol,traf)
              rrem=rrem+1
              j=rowbeg(row)
              k=rowend(row)
              do p=j,k
                l=rowidx(p)
                if(colmrk(l).lt.0)then
                  coln=coln+1
                  collst(coln)=l
                endif
                colmrk(l)=procn
                if(mrk(l).lt.0)then
                  mrk(l)=procn
                  cnum=cnum+1
                  list(cnum)=l
                endif
              enddo
            endif
          enddo
        else

c column is fixed to one bound

          do i=p1,p2
            row=colidx(i)
            if(rowmrk(row).lt.0)then
              rown=rown+1
              rowlst(rown)=row
            endif
            rowmrk(row)=procn
          enddo
          addobj=addobj+obj(col)*sol
          lob(col)=pivot
          upb(col)=pivot
        endif

        goto 10
      endif
 999  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'chepdu:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif
      return
      end

c ===========================================================================
c compute primal, upper, dual infeasibilities
c ===========================================================================

       subroutine lpcprinf(xs,prinf,slktyp,colpnt,rowidx,nonzeros,
     x  rhs,vcstat,pinf)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 slktyp(m),colpnt(n1),rowidx(nz),vcstat(mn)
      real*8    xs(mn),prinf(m),rhs(m),nonzeros(nz),pinf

      integer*4 i,j,pnt1,pnt2
      real*8 sol

c ---------------------------------------------------------------------------

      do i=1,m
        prinf(i)=rhs(i)
      enddo
      pinf=0.0d+0

      do i=1,n
        if(vcstat(i).gt.-2)then
          sol=xs(i)
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            prinf(rowidx(j)-n)=prinf(rowidx(j)-n)-sol*nonzeros(j)
          enddo
        endif
      enddo
      do i=1,m
        if(vcstat(i+n).gt.-2)then
          if(slktyp(i).ne.0)then
            sol=prinf(i)+xs(i+n)
          else
            sol=prinf(i)
          endif
        else
          sol=0.0d+0
        endif
        prinf(i)=sol
        if(pinf.lt.abs(sol))pinf=abs(sol)
      enddo
      return
      end

c ===========================================================================

      subroutine lpcupinf(xs,up,upinf,bounds,vartyp,slktyp,vcstat,
     x uinf)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 vartyp(n),slktyp(m),vcstat(mn)
      real*8 xs(mn),up(mn),upinf(mn),bounds(mn),uinf

      integer*4 i

      do i=1,mn
        upinf(i)=0.0d+0
      enddo
      uinf=0.0d+0
      do i=1,n
        if((vcstat(i).gt.-2).and.(vartyp(i).lt.0))then
          upinf(i)=bounds(i)-xs(i)-up(i)
          if(uinf.lt.abs(upinf(i)))uinf=abs(upinf(i))
        endif
      enddo
      do i=1,m
        if((vcstat(i+n).gt.-2).and.(slktyp(i).lt.0))then
          upinf(i+n)=bounds(i+n)-xs(i+n)-up(i+n)
          if(uinf.lt.abs(upinf(i+n)))uinf=abs(upinf(i+n))
        endif
      enddo
      return
      end

c ============================================================================

      subroutine lpcduinf(dv,dspr,dsup,duinf,vartyp,slktyp,colpnt,
     x rowidx,nonzeros,obj,vcstat,dinf)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 vartyp(n),slktyp(m),colpnt(n1),rowidx(nz),
     x vcstat(mn)
      real*8 dv(m),dspr(mn),dsup(mn),duinf(mn),nonzeros(nz),obj(n),
     x dinf

      integer*4 i,j,pnt1,pnt2
      real*8 sol

c ------------------------------------------------------------------------------

      dinf=0.0d+0

      do i=1,m
        sol=0.0d+0
        if(vcstat(i+n).gt.-2)then
          if(slktyp(i).gt.0)then
            sol=dv(i)-dspr(i+n)
          else if(slktyp(i).lt.0)then
            sol=dv(i)-dspr(i+n)+dsup(i+n)
          endif
        endif
        duinf(i+n)=sol
      enddo

      do i=1,n
        sol=0.0d+0
        if(vcstat(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if(vcstat(rowidx(j)).gt.-2)then
              sol=sol+dv(rowidx(j)-n)*nonzeros(j)
            endif
          enddo
          if(vartyp(i))10,11,12

c upper bounded variable

  10      sol=obj(i)-sol-dspr(i)+dsup(i)
          goto 15

c free variable

  11      sol=obj(i)-sol
          goto 15

c standard variable

  12      sol=obj(i)-sol-dspr(i)
        endif
  15    duinf(i)=sol
      enddo

c compute absolute and relative infeasibility

      do i=1,mn
        sol=abs(duinf(i))
        if(dinf.lt.sol)dinf=sol
      enddo

      return
      end

c ==============================================================================

      subroutine lpcpdobj(popt,dopt,obj,rhs,bounds,xs,dv,
     x dsup,vcstat,vartyp,slktyp)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 vcstat(mn),vartyp(n),slktyp(m)
      real*8  popt,dopt,obj(n),rhs(m),bounds(mn),xs(mn),dv(m),dsup(mn)

      integer*4 i

      popt=0.0d+0
      dopt=0.0d+0
      do i=1,n
        if(vcstat(i).gt.-2)then
          popt=popt+obj(i)*xs(i)
          if(vartyp(i).lt.0)then
            dopt=dopt-bounds(i)*dsup(i)
          endif
        endif
      enddo
      do i=1,m
        if(vcstat(i+n).gt.-2)then
          dopt=dopt+rhs(i)*dv(i)
          if(slktyp(i).lt.0)then
            dopt=dopt-bounds(i+n)*dsup(i+n)
          endif
        endif
      enddo
      return
      end

c ===========================================================================
c compute the new primal and dual solution

c ===========================================================================

      subroutine lpcnewpd(prstpl,xs,dxs,up,upinf,dustpl,dv,ddv,
     x dspr,ddspr,dsup,ddsup,vartyp,slktyp,vcstat,maxd)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 vartyp(n),slktyp(m),vcstat(mn)
      real*8 prstpl,xs(mn),dxs(mn),up(mn),upinf(mn),dustpl,dv(m),
     x ddv(m),dspr(mn),ddspr(mn),dsup(mn),ddsup(mn),maxd

      integer*4 i,j
      real*8 maxdd,maxdp

      maxdp=0.0d+0
      maxdd=0.0d+0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
            dv(i-n)=dv(i-n)+dustpl*ddv(i-n)
            if(maxdd.lt.abs(ddv(i-n)))maxdd=abs(ddv(i-n))
          endif
          if((i.le.n).or.(j.ne.0))then
            xs(i)=xs(i)+prstpl*dxs(i)
            if(maxdp.lt.abs(dxs(i)))maxdp=abs(dxs(i))
            dspr(i)=dspr(i)+dustpl*ddspr(i)
            if(maxdd.lt.abs(ddspr(i)))maxdd=abs(ddspr(i))
          endif
          if (j.lt.0)then
            up(i)=up(i)+prstpl*(upinf(i)-dxs(i))
            if(maxdp.lt.abs(upinf(i)-dxs(i)))maxdp=abs(upinf(i)-dxs(i))
            dsup(i)=dsup(i)+dustpl*ddsup(i)
            if(maxdd.lt.abs(ddsup(i)))maxdd=abs(ddsup(i))
          endif
        endif
      enddo
      maxd=max(maxdp*prstpl,maxdd*dustpl)
      return
      end

c ===========================================================================
c ============================================================================

      subroutine lp coldbl(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,obj,colsta,
     x prelen,prehis,procn,mark,valc,vartyp,
     x big,lbig,tfeas,tzer)

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),colsta(n),
     x prelen,prehis(mn),procn,mark(m),vartyp(n)
      real*8 obj(n),lob(n),upb(n),colnzs(nz),rownzs(nz),valc(m),
     x big,lbig,tfeas,tzer

      integer*4 i,j,k,l,col,row,pcol,pnt1,pnt2,ppnt1,ppnt2,pntt1,pntt2,
     x crem,rrem,collen
      real*8 sd,toler,obj1,obj2,lo1,lo2,up1,up2,sol
      logical traf
      character*99 buff

c ============================================================================

      crem=0
      rrem=0
      do i=1,m
        mark(i)=0
      enddo

c start search

      do 25 col=1,n
        if((colsta(col).gt.-2).and.(colend(col).ge.colbeg(col)))then
          pnt1=colbeg(col)
          pnt2=colend(col)
          collen=pnt2-pnt1
          do i=pnt1,pnt2
            mark(colidx(i))=col
            valc(colidx(i))=colnzs(i)
          enddo

c select row

          row=0
          l=n+1
          do j=pnt1,pnt2
            k=colidx(j)
            if(rowend(k)-rowbeg(k).lt.l)then
              l=rowend(k)-rowbeg(k)
              row=k
            endif
          enddo

c start search in the row

          if(row.ne.0)then
            pntt1=rowbeg(row)
            pntt2=rowend(row)
            do 15 l=pntt1,pntt2
              pcol=rowidx(l)
              ppnt1=colbeg(pcol)
              ppnt2=colend(pcol)
              if((pcol.le.col).or.(collen.ne.ppnt2-ppnt1))goto 15
              do i=ppnt1,ppnt2
                if(mark(colidx(i)).ne.col)goto 15
              enddo

c nonzero structure is o.k.

              sd=valc(colidx(ppnt1))/colnzs(ppnt1)
              toler=(abs(sd)+1.0d+0)*tzer
              do i=ppnt1,ppnt2
                if(abs(valc(colidx(i))/colnzs(i)-sd).gt.toler)goto 15
              enddo

c nonzeros are dependent, factor : sd, columns: col,pcol

              obj1=obj(col)
              obj2=obj(pcol)*sd

c identical columns found

              if(abs(obj1-obj2).le.(abs(obj1)+1.0d+0)*tfeas)then
                lo1=lob(pcol)
                up1=upb(pcol)
                if(lob(col).lt.-lbig)then
                  if(sd.gt.0.0d+0)then
                    lo2=lob(col)
                  else
                    lo2=-lob(col)
                  endif
                else
                  lo2=lob(col)/sd
                endif
                if(upb(col).gt.lbig)then
                  if(sd.gt.0.0d+0)then
                    up2=upb(col)
                  else
                    up2=-upb(col)
                  endif
                else
                  up2=upb(col)/sd
                endif
                if(sd.lt.0.0d+0)then
                  sol=up2
                  up2=lo2
                  lo2=sol
                endif

c store factor and old bound info

                obj(col)=sd
                vartyp(col)=0
                if(lo2.lt.-lbig)then
                  vartyp(col)=4
                  lob(col)=lo1
                else
                  lob(col)=lo2
                endif
                if(up2.gt.lbig)then
                  vartyp(col)=vartyp(col)+8
                  upb(col)=up1
                else
                  upb(col)=up2
                endif
                if((lo1.gt.-lbig).and.(lo2.gt.-lbig))then
                  lob(pcol)=lo1+lo2
                else
                  lob(pcol)=-big
                endif
                if((up1.lt.lbig).and.(up2.lt.lbig))then
                  upb(pcol)=up1+up2
                else
                  upb(pcol)=big
                endif
                prelen=prelen+1
                prehis(prelen)=col
                colsta(col)=-2-procn-pcol-10
                traf=.false.
                call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x          colbeg,colend,rowbeg,rowend,obj,sol,traf)
                crem=crem+1
                goto 25
              endif
  15        continue
          endif
        endif
  25  continue
      if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'coldbl:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif
      return
      end
c ============================================================================
c ===========================================================================

      subroutine lpcolsng(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x coln,collst,colmrk,
     x cnum,list,mrk,procn,
     x ppbig,pmaxr,pmbig,pminr,
     x lbig,tfeas,tzer,code)

c this subroutine cheks singleton columns

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,list(n),mrk(n),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x coln,collst(n),colmrk(n),ppbig(m),pmbig(m),code

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),addobj,lbig,tfeas,tzer

      integer*4 i,j,k,l,row,col,crem,rrem
      real*8 ub,lb,upper,lower,sol,pivot
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      rrem=0
      crem=0
  10  if(cnum.ge.1)then
        col=list(1)
        mrk(col)=-1
        list(1)=list(cnum)
        cnum=cnum-1
        if(colbeg(col).eq.colend(col))then
          row=colidx(colbeg(col))
          pivot=colnzs(colbeg(col))
          if(pivot.gt.0.0d+0)then
            lb=lob(col)
            ub=upb(col)
            sol=obj(col)
          else
            ub=-lob(col)
            lb=-upb(col)
            pivot=-pivot
            sol=-obj(col)
          endif
          if((lb.gt.-lbig).or.(ub.lt.lbig))then

c compute lower bound of the lp constraint

            if(lb.le.-lbig)then
              l=pmbig(row)-1
              lower=pminr(row)
            else
              l=pmbig(row)
              lower=pminr(row)-lb*pivot
            endif
            if(ups(row).gt.lbig)then
              l=l+1
            else
              lower=lower-ups(row)
            endif
            if(l.gt.0)lower=-lbig

c compute upper bound of the lp constraint

            if(ub.gt.lbig)then
              l=ppbig(row)-1
              upper=pmaxr(row)
            else
              l=ppbig(row)
              upper=pmaxr(row)-ub*pivot
            endif
            if(los(row).lt.-lbig)then
              l=l+1
            else
              upper=upper-los(row)
            endif
            if(l.gt.0)upper=lbig

c check new upper and lower bound

            if(lb.gt.-lbig)then
              upper=(rhs(row)-upper)/pivot
              if((lb-upper).gt.(abs(lb)+1.0d+0)*tfeas)goto 10
            endif
            if(ub.lt.lbig)then
              lower=(rhs(row)-lower)/pivot
              if((lower-ub).gt.(abs(ub)+1.0d+0)*tfeas)goto 10
            endif
          endif

c ( hidden ) free singleton column found, check slacks

          pivot=sol/pivot
          if(pivot.gt.tzer)then
            if(los(row).lt.-lbig)then
              cnum=-col
              code=3
              goto 999
            endif
            rhs(row)=rhs(row)+los(row)
          else if(pivot.lt.-tzer)then
            if(ups(row).gt.lbig)then
              cnum=-col
              code=3
              goto 999
            endif
            rhs(row)=rhs(row)+ups(row)
          endif

c column administration

          prelen=prelen+1
          prehis(prelen)=col
          colsta(col)=-2-procn
          traf=.false.
          call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x    colbeg,colend,rowbeg,rowend,rhs,pivot,traf)
          crem=crem+1
          addobj=addobj+rhs(row)*pivot

c row administration

          prelen=prelen+1
          prehis(prelen)=row+n
          rowsta(row)=-2-procn
          traf=.true.
          call lpremove(n,m,nz,row,rowidx,rownzs,colidx,colnzs,
     x    rowbeg,rowend,colbeg,colend,obj,pivot,traf)
          rrem=rrem+1
          j=rowbeg(row)
          k=rowend(row)
          do i=j,k
            l=rowidx(i)
            if(colmrk(l).lt.0)then
              coln=coln+1
              collst(coln)=l
            endif
            colmrk(l)=procn
            if((mrk(l).lt.0).and.(colbeg(l).eq.colend(l)))then
              mrk(l)=procn
              cnum=cnum+1
              list(cnum)=l
            endif
          enddo
        endif
        goto 10
      endif
 999  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'colsng:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif
      return
      end

c ===========================================================================
c convert the lp problem to the standard form : ax-s=b, u>=x,s>=l,
c ===========================================================================

      subroutine  lpconvert(range,lbound,ubound,rowtyp,big)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 rowtyp(m)
      real*8    range(m),lbound(mn),ubound(mn),big

      integer*4    i

c --------------------------------------------------------------------------

      do i=1,m
        if(rowtyp(i).eq.3)then
          lbound(i+n)=-range(i)
          ubound(i+n)=0.0d+0
        else if(rowtyp(i).eq.2)then
          lbound(i+n)=0.0d+0
          ubound(i+n)=range(i)
        else if(rowtyp(i).eq.1)then
          lbound(i+n)=0.0d+0
          ubound(i+n)=0.0d+0
        else
          lbound(i+n)=-big
          ubound(i+n)= big
        endif
      enddo
      return
      end

c ============================================================================
c computing the starting point  xs,up in the primal space,
c                       dv, dspr,dsup in the dual   space.

c ===========================================================================

      subroutine lpinitsol(xs,up,dv,dspr,dsup,rhs,obj,bounds,vartyp,
     x slktyp,vcstat,colpnt,ecolpnt,pivots,rowidx,nonzeros,diag,
     x updat1,count)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpinitv/ prmin,upmax,dumin,stamet,safmet,premet,regul
      real*8        prmin,upmax,dumin
      integer*4     stamet,safmet,premet,regul

      common/lpmscal/ varadd,slkadd,scfree
      real*8        varadd,slkadd,scfree

      common/lpnumer/  tplus,tzer
      real*8         tplus,tzer

      integer*4 ecolpnt(mn),vcstat(mn),colpnt(n1),rowidx(cfree),
     x pivots(mn),vartyp(n),slktyp(m),count(mn)
      real*8  xs(mn),up(mn),dv(m),dspr(mn),dsup(mn),rhs(m),obj(n),
     x bounds(mn),diag(mn),updat1(mn),nonzeros(cfree)

      integer*4 i,j,pnt1,pnt2
      real*8    sol,sb,spr,sdu,prlo,dulo,ngap
      logical   addall

c ---------------------------------------------------------------------------

c reset all values

      do i=1,mn
        xs(i)=0.0d+0
        up(i)=0.0d+0
        dspr(i)=0.0d+0
        dsup(i)=0.0d+0
        if(i.le.m)dv(i)=0.0d+0
      enddo

c rhs for xs ans up

      do i=1,m
        if(slktyp(i).lt.0)then
          if(bounds(i+n).gt.upmax)then
            sol=upmax/2
          else
            sol=bounds(i+n)/2
          endif
        else
          sol=0.0d+0
        endif
        updat1(i+n)=rhs(i)+sol
      enddo
      do i=1,n
        if(vartyp(i).lt.0)then
          if(bounds(i).gt.upmax)then
            sol=-upmax
          else
            sol=-bounds(i)
          endif
        else
          sol=0.0d+0
        endif
        updat1(i)=sol
      enddo

      call lpaugftr(ecolpnt,
     x vcstat,rowidx,pivots,count,nonzeros,diag,updat1)
      call lpaugbtr(ecolpnt,
     x vcstat,rowidx,pivots,count,nonzeros,diag,updat1)

c initial values for xs, up

      do i=1,n
        if(vcstat(i).gt.-2)then
          xs(i)=updat1(i)
          if(vartyp(i).lt.0)then
            up(i)=bounds(i)-xs(i)
          endif
        endif
      enddo
      do i=1,m
        j=i+n
        if((vcstat(j).gt.-2).and.(slktyp(i).ne.0))then
          xs(j)=-updat1(j)
          if(slktyp(i).lt.0)then
            xs(j)=(bounds(j)-updat1(j))/2
            up(j)=bounds(j)-xs(j)
          endif
        endif
      enddo

c initial dual variables, stamet=2

      if(stamet.eq.1)then
        do i=1,m
          dv(i)=0
          dspr(i+n)=0
          dsup(i+n)=0
        enddo
        do i=1,n
          if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
            if(vartyp(i).lt.0)then
              dspr(i)=obj(i)/2
              dsup(i)=-obj(i)/2
            else
              dspr(i)=obj(i)
            endif
          endif
        enddo
      else if(stamet.eq.2)then
        do i=1,m
          updat1(i+n)=0.0d+0
        enddo
        do i=1,n
          updat1(i)=obj(i)
        enddo
        call lpaugftr(ecolpnt,
     x  vcstat,rowidx,pivots,count,nonzeros,diag,updat1)
        call lpaugbtr(ecolpnt,
     x  vcstat,rowidx,pivots,count,nonzeros,diag,updat1)
        do i=1,m
          if(vcstat(i+n).gt.-2)then
            dv(i)=updat1(i+n)
          else
            dv(i)=0.0d+0
          endif
          if(slktyp(i).ne.0)then
            dspr(i+n)=-dv(i)
            if(slktyp(i).lt.0)then
              dspr(i+n)=-dv(i)/2
              dsup(i+n)=dv(i)/2
            endif
          endif
        enddo
        do i=1,n
          if((vcstat(i).gt.-2).and.(vartyp(i).ne.0))then
            if(vartyp(i).lt.0)then
              dspr(i)=-updat1(i)
              dsup(i)=updat1(i)
            else
              dspr(i)=-updat1(i)
            endif
          endif
        enddo
      endif

c compute prmin,dumin

      if(safmet.lt.0)then
        safmet=-safmet
        addall=.true.
      else
        addall=.false.
      endif

c marsten et al.

      if(safmet.eq.2)then
        do i=1,m
          updat1(i)=0
        enddo
        do i=1,n
          if(vcstat(i).gt.-2)then
            pnt1=colpnt(i)
            pnt2=colpnt(i+1)-1
            sol=0.0d+0
            sb=obj(i)
            do j=pnt1,pnt2
              if(vcstat(rowidx(j)).gt.-2)then
                sol=sol+rhs(rowidx(j)-n)*nonzeros(j)
                updat1(rowidx(j)-n)=updat1(rowidx(j)-n)+nonzeros(j)*sb
              endif
            enddo
            if(prmin.lt.sol)prmin=sol
          endif
        enddo
        do i=1,m
          if(dumin.lt.abs(updat1(i)))dumin=abs(updat1(i))
        enddo
      endif

c mehrotra

      if(safmet.eq.3)then
        spr=1.0d+0/tzer
        sdu=1.0d+0/tzer
        do i=1,mn
          if(i.le.n)then
             j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if((vcstat(i).gt.-2).and.(j.ne.0))then
            if(spr.gt.xs(i))spr=xs(i)
            if(sdu.gt.dspr(i))sdu=dspr(i)
            if(j.lt.0)then
              if(spr.gt.up(i))spr=up(i)
              if(sdu.gt.dsup(i))sdu=dsup(i)
            endif
          endif
        enddo
        spr=-1.5d+0*spr
        sdu=-1.5d+0*sdu
        if(spr.lt.0.001d+0)spr=0.001d+0
        if(sdu.lt.0.001d+0)sdu=0.001d+0
        prlo=0.0d+0
        dulo=0.0d+0
        ngap=0.0d+0
        do i=1,mn
          if(i.le.n)then
             j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if((vcstat(i).gt.-2).and.(j.ne.0))then
             sol=xs(i)+spr
             sb=dspr(i)+sdu
             ngap=ngap+sol*sb
             prlo=prlo+sol
             dulo=dulo+sb
             if(j.lt.0)then
               sol=up(i)+spr
               sb=dsup(i)+sdu
               ngap=ngap+sol*sb
               prlo=prlo+sol
               dulo=dulo+sb
             endif
          endif
        enddo
        prmin=spr+0.5d+0*ngap/dulo
        dumin=sdu+0.5d+0*ngap/prlo
      endif
      if(addall.and.(safmet.lt.3))then
        sol=1.0d+0/tzer
        sb=1.0d+0/tzer
        do i=1,mn
          if(vcstat(i).gt.-2)then
            if(i.le.n)then
              j=vartyp(i)
            else
              j=slktyp(i-n)
            endif
            if(j.ne.0)then
              if(sol.gt.xs(i))sol=xs(i)
              if(sb.gt.dspr(i))sb=dspr(i)
            endif
            if(j.lt.0)then
              if(sol.gt.up(i))sol=up(i)
              if(sb.gt.dsup(i))sb=dsup(i)
            endif
          endif
        enddo
        if(sol.lt.0)prmin=prmin-sol
        if(sb.lt.0)dumin=dumin-sb
      endif

c correcting

      if(addall)then
        spr=1.0d+0/tzer
        sdu=1.0d+0/tzer
        sol=1.0d+0
      else
        spr=prmin
        sdu=dumin
        sol=0.0d+0
      endif
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            if(xs(i).lt.spr)then
              xs(i)=sol*xs(i)+prmin
            endif
            if(dspr(i).lt.sdu)then
              dspr(i)=sol*dspr(i)+dumin
            endif
            if(j.lt.0)then
              if(up(i).lt.spr)then
                up(i)=sol*up(i)+prmin
              endif
              if(dsup(i).lt.sdu)then
                dsup(i)=sol*dsup(i)+dumin
              endif
            endif
          endif
        endif
      enddo

      return
      end

c ===========================================================================

c     set up the initial scaling matrix
c     (for the computation of the initial solution)

      subroutine lpfscale(vcstat,diag,odiag,vartyp,slktyp)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpmscal/ varadd,slkadd,scfree
      real*8        varadd,slkadd,scfree

      integer*4 vcstat(mn),vartyp(n),slktyp(m)
      real*8 diag(mn),odiag(mn)

      integer*4 i,j
      real*8 sol

      do i=1,mn
        sol=0.0d+0
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
            if(j.gt.0)then
              sol=-1.0d0
            else if(j.lt.0)then
              sol=-2.0d0
            else
              sol=-scfree
            endif
          else
            j=slktyp(i-n)
            if(j.gt.0)then
              sol=1.0d0
            else if(j.lt.0)then
              sol=0.5d+0
            else
              sol=0.0d+0
            endif
          endif
        endif
        diag(i)=sol
        odiag(i)=sol
      enddo
      return
      end

c ============================================================================
c ===========================================================================

      subroutine lpstpcrt(prelinf,drelinf,popt,dopt,cgap,
     x iter,code,pphase,dphase,maxstp,pinf,uinf,dinf,
     x prinf,upinf,duinf,oldmp,pb,db,
     x prstpl,dustpl,obj,rhs,bounds,xs,dxs,dspr,ddspr,dsup,
     x ddsup,dv,ddv,up,addobj,scobj,vcstat,vartyp,slktyp,
     x oprelinf,odrelinf,opinf,odinf,ocgap,opphas,odphas,buff)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 prelinf,drelinf,popt,dopt,cgap,maxstp,
     x pinf,uinf,oldmp,dinf,pb,db,oprelinf,odrelinf,opinf,odinf,ocgap
      integer*4 iter,code,pphase,dphase,opphas,odphas

      real*8 prstpl,dustpl,obj(n),rhs(m),bounds(mn),xs(mn),dxs(mn),
     x dspr(mn),ddspr(mn),dsup(mn),ddsup(mn),dv(m),ddv(m),upinf(mn),
     x up(mn),prinf(m),duinf(mn),addobj,scobj
      integer*4 vcstat(mn),vartyp(n),slktyp(m)
      character*99 buff

      common/lptoler/ tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol,maxiter
      real*8        tsdir,topt1,topt2,tfeas1,tfeas2,feas1,feas2,
     x              pinfs,dinfs,inftol
      integer*4     maxiter
      real*8 check

      real*8 oldpopt,olddopt,objnrm,rhsnrm,bndnrm,urelinf,mp
      integer*4 i

      prelinf=0.0d+0
      urelinf=0.0d+0
      drelinf=0.0d+0
      objnrm =0.0d+0
      rhsnrm =0.0d+0
      bndnrm =0.0d+0

      do i=1,n
        if(vcstat(i).gt.-2)then
          objnrm=objnrm+obj(i)*obj(i)
          drelinf=drelinf+duinf(i)*duinf(i)
          if(vartyp(i).lt.0)then
            bndnrm=bndnrm+bounds(i)*bounds(i)
            urelinf=urelinf+upinf(i)*upinf(i)
          endif
        endif
      enddo
      do i=1,m
        if(vcstat(i+n).gt.-2)then
          rhsnrm=rhsnrm+rhs(i)*rhs(i)
          prelinf=prelinf+prinf(i)*prinf(i)
          drelinf=drelinf+duinf(i+n)*duinf(i+n)
          if(slktyp(i).lt.0)then
            bndnrm=bndnrm+bounds(i+n)*bounds(i+n)
            urelinf=urelinf+upinf(i+n)*upinf(i+n)
          endif
        endif
      enddo

      prelinf=sqrt(prelinf+urelinf)/(1.0d+0+sqrt(bndnrm+rhsnrm))
      drelinf=sqrt(drelinf)/(1.0d+0+sqrt(objnrm))
      if(drelinf.gt.dinf)drelinf=dinf
      if(prelinf.gt.max(pinf,uinf))prelinf=max(pinf,uinf)

      mp=prelinf+drelinf+
     x abs(popt-dopt)/scobj/(1.0d+0+sqrt(rhsnrm+bndnrm)+sqrt(objnrm))
      if(iter.le.1)oldmp=mp

      code=0
      if((prelinf.lt.tfeas1).and.
     x  (pinf.lt.feas1).and.(uinf.lt.feas1))then
        pphase=2
      else
        pphase=1
        pb=abs(pb-pinf)/(abs(pinf))
      endif
      if((drelinf.lt.tfeas2).and.(dinf.lt.feas2))then
        dphase=2
      else
        dphase=1
        db=abs(db-dinf)/(abs(dinf))
      endif

c     check topt1
      check=abs(popt-dopt)/(abs(popt)+1.0d+0)
      if((abs(popt-dopt)/(abs(popt)+1.0d+0).le.topt1)
     x. and.(pphase.eq.2).and.(dphase.eq.2))then
        code=2
        write(buff,'(1x,a)')
     x  'stopping criterion : small infeasibility and duality gap'
      else if((popt.lt.dopt).and.(pphase.eq.2).and.(dphase.eq.2))then
        code=0
        if(iter.gt.0)then
          call lpcpdobj(oldpopt,olddopt,obj,rhs,bounds,dxs,ddv,ddsup,
     x    vcstat,vartyp,slktyp)
          oldpopt=popt-oldpopt*scobj*prstpl
          olddopt=dopt-olddopt*scobj*dustpl
          if(oldpopt.ge.olddopt)then
            code=2
            maxstp=1.0d+0-(oldpopt-olddopt)/(dopt-olddopt-popt+oldpopt)
            dustpl=-maxstp*dustpl
            prstpl=-maxstp*prstpl
            call lpcnewpd(prstpl,xs,dxs,up,upinf,dustpl,dv,ddv,dspr,
     x      ddspr,dsup,ddsup,vartyp,slktyp,vcstat,maxstp)
            call lpcpdobj(popt,dopt,obj,rhs,bounds,xs,dv,dsup,
     x      vcstat,vartyp,slktyp)
            popt=popt*scobj+addobj
            dopt=dopt*scobj+addobj
          endif
        endif
        if(code.gt.0)then
          write(buff,'(1x,a)')
     x    'stopping criterion : small infeasibility and duality gap'
        else
          write(buff,'(1x,a)')
     x    'stopping criterion : negative gap (wrong tolerances ?)'
          code=1
        endif
      else if((mp.gt.topt1).and.(mp.gt.inftol*oldmp))then
        if(pphase+dphase.eq.4)then
          code=1
          write(buff,'(1x,a)')
     x    'stopping criterion: possible numerical problems'
        else if (opphas+odphas.eq.4)then
          code=1
          write(buff,'(1x,a)')
     x    'stopping criterion: instability, suboptimal solution'
          dustpl=-dustpl
          prstpl=-prstpl
          call lpcnewpd(prstpl,xs,dxs,up,upinf,dustpl,dv,ddv,dspr,
     x    ddspr,dsup,ddsup,vartyp,slktyp,vcstat,maxstp)
          call lpcpdobj(popt,dopt,obj,rhs,bounds,xs,dv,dsup,
     x    vcstat,vartyp,slktyp)
          call lpcpdobj(popt,dopt,obj,rhs,bounds,xs,dv,dsup,
     x    vcstat,vartyp,slktyp)
          popt=popt*scobj+addobj
          dopt=dopt*scobj+addobj
          prelinf=oprelinf
          drelinf=odrelinf
          pinf=opinf
          dinf=odinf
          pphase=opphas
          dphase=odphas
          cgap=ocgap
        else
          write(buff,'(1x,a)')
     x    'stopping criterion: problem infeasibile'
          code=4
          if(pphase.eq.2)code=3
        endif
      else if(abs(cgap).lt.topt2)then
        code=1
        if((pphase.eq.2).and.(dphase.eq.2))code=2
        write(buff,'(1x,a)')
     x  'stopping criterion : small complementarity gap'
      else if(iter.ge.maxiter)then
        code=1
        write(buff,'(1x,a)')
     x  'stopping criterion : iteration limit is exeeded'
      else if(maxstp.lt.tsdir)then
        code=1
        write(buff,'(1x,a)')
     x  'stopping criterion : very small step'
      else if((iter.gt.0).and.(pphase.eq.1).and.(pb.lt.pinfs))then
        code=4
        write(buff,'(1x,a)')
     x  'stopping criterion: pinfs limit. problem primal infeasibile'
      else if((iter.gt.0).and.(dphase.eq.1).and.(db.lt.dinfs))then
        code=3
        write(buff,'(1x,a)')
     x  'stopping criterion: dinfs limit. problem dual infeasibile'
      endif
      if(oldmp.gt.mp)oldmp=mp
      oprelinf=prelinf
      odrelinf=drelinf
      opinf=pinf
      odinf=dinf
      opphas=pphase
      odphas=dphase
      ocgap=cgap
      return
      end

c ===========================================================================
c compute the primal and dual steplengts

c ===========================================================================

      subroutine lpcstpln(prstpl,xs,dxs,up,upinf,
     x  dustpl,dspr,ddspr,dsup,ddsup,vartyp,slktyp,vcstat)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpparam/ palpha,dalpha
      real*8        palpha,dalpha

      integer*4 vartyp(n),slktyp(m),vcstat(mn)
      real*8 prstpl,xs(mn),dxs(mn),up(mn),upinf(mn),
     x dustpl,dspr(mn),ddspr(mn),dsup(mn),ddsup(mn)

      integer*4 i,j
      real*8 sol,dup

      prstpl=1.0d0/palpha
      dustpl=1.0d0/dalpha
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            if(dxs(i).lt.0.0d+0)then
              sol=-xs(i)/dxs(i)
              if(sol.lt.prstpl)prstpl=sol
            endif
            if(ddspr(i).lt.0.0d+0)then
              sol=-dspr(i)/ddspr(i)
              if(sol.lt.dustpl)dustpl=sol
            endif
            if (j.lt.0)then
              dup=upinf(i)-dxs(i)
              if(dup.lt.0.0d+0)then
                sol=-up(i)/dup
                if(sol.lt.prstpl)prstpl=sol
              endif
              if(ddsup(i).lt.0.0d+0)then
                sol=-dsup(i)/ddsup(i)
                if(sol.lt.dustpl)dustpl=sol
              endif
            endif
          endif
        endif
      enddo
      return
      end

c ===========================================================================
c ===========================================================================

      subroutine lpduchek(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x coln,collst,colmrk,rown,rowlst,rowmrk,
     x cnum,clist,cmrk,rnum,rlist,rmrk,procn,
     x ppbig,pmaxr,pmbig,pminr,
     x p,q,pbig,mbig,maxc,minc,
     x big,lbig,tfeas,tzer,bigbou,search,code,prelev)

c this subroutine removes singleton rows and may fixes variables

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,clist(n),cmrk(n),
     x rnum,rlist(m),rmrk(m),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x coln,rown,collst(n),rowlst(n),colmrk(n),rowmrk(m),
     x ppbig(m),pmbig(m),pbig(n),mbig(n),search,code,prelev

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),p(m),q(m),maxc(n),minc(n),
     x addobj,big,lbig,tfeas,tzer,bigbou

      integer*4 i,j,up,row,col,dir,pnt1,pnt2,p1,p2,crem,rrem,up3,
     x lstcnt
      real*8  sol,toler,up1,up2
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      crem=0
      rrem=0
      do while (rnum.ge.1)
        row=rlist(1)
        if (ups(row).gt.lbig)then
          p(row)=0.0d+0
        else
          p(row)=-big
        endif
        if(los(row).lt.-lbig)then
          q(row)=0.0d+0
        else
          q(row)=big
        endif
        rmrk(row)=-1
        rlist(1)=rlist(rnum)
        rnum=rnum-1
      enddo

      cnum=0
      do i=1,n
        if(upb(i).lt.lbig)then
          mbig(i)=1
        else
          mbig(i)=0
        endif
        if(lob(i).gt.-lbig)then
          pbig(i)=1
        else
          pbig(i)=0
        endif
        maxc(i)=0.0d+0
        minc(i)=0.0d+0
        if((colsta(i).gt.-2).and.(upb(i)-lob(i).gt.lbig))then
          cnum=cnum+1
          cmrk(i)=1
          clist(cnum)=i
         else
          cmrk(i)=-2
        endif
      enddo
      dir=1
      do i=1,m
        if(rowsta(i).gt.-2)then
          call lpchgmxm(rowbeg(i),rowend(i),q(i),p(i),rowidx,rownzs,
     x    pbig,maxc,mbig,minc,lbig,dir,n)
        endif
      enddo

      lstcnt=0
      do while (cnum.ne.lstcnt)
        lstcnt=lstcnt+1
        if(lstcnt.gt.n)then
          lstcnt=1
          search=search-1
          if(search.eq.0)goto 100
        endif
        col=clist(lstcnt)
        cmrk(col)=-1
        pnt1=colbeg(col)
        pnt2=colend(col)
        do i=pnt1,pnt2
          row=colidx(i)

c compute new upper bound: up1+(obj-up2)/nzs

          if(colnzs(i).gt.0.0d+0)then
            up2=minc(col)
            up3=mbig(col)
          else
            up2=maxc(col)
            up3=pbig(col)
          endif
          if(p(row).lt.-lbig)then
            up1=0.0d+0
            up=1
          else
            up1=p(row)
            up=0
          endif
          if(up.eq.up3)then
            sol=up1+(obj(col)-up2)/colnzs(i)
            if(abs(sol).lt.bigbou)then
              if(q(row)-sol.gt.(abs(sol)+1.0d+0)*tfeas)then
                p1=rowbeg(row)
                p2=rowend(row)
                dir=1
                call lpmodmxm(nz,p1,p2,q(row),sol,rowidx,rownzs,
     x          pbig,maxc,mbig,minc,lbig,dir,n)
                q(row)=sol
                do j=p1,p2
                  if(cmrk(rowidx(j)).eq.-1)then
                    if(upb(rowidx(j))-lob(rowidx(j)).gt.lbig)then
                      cnum=cnum+1
                      if(cnum.gt.n)cnum=1
                      clist(cnum)=rowidx(j)
                      cmrk(rowidx(j))=1
                    endif
                  endif
                enddo
              endif
            endif
          endif

c compute new lower bound: up1+(obj-up2)/nzs

          if(colnzs(i).gt.0.0d+0)then
            up2=maxc(col)
            up3=pbig(col)
          else
            up2=minc(col)
            up3=mbig(col)
          endif
          if(q(row).gt.lbig)then
            up1=0.0d+0
            up=1
          else
            up1=q(row)
            up=0
          endif
          if(up.eq.up3)then
            sol=up1+(obj(col)-up2)/colnzs(i)
            if(abs(sol).lt.bigbou)then
              if(sol-p(row).gt.(abs(sol)+1.0d+0)*tfeas)then
                p1=rowbeg(row)
                p2=rowend(row)
                dir=-1
                call lpmodmxm(nz,p1,p2,p(row),sol,rowidx,rownzs,
     x          pbig,maxc,mbig,minc,lbig,dir,n)
                p(row)=sol
                do j=p1,p2
                  if(cmrk(rowidx(j)).eq.-1)then
                    if(upb(rowidx(j))-lob(rowidx(j)).gt.lbig)then
                      cnum=cnum+1
                      if(cnum.gt.n)cnum=1
                      clist(cnum)=rowidx(j)
                      cmrk(rowidx(j))=1
                    endif
                  endif
                enddo
              endif
            endif
          endif
        enddo
      enddo

c dual feasibility check

  100 do while (cnum.ne.lstcnt)
        lstcnt=lstcnt+1
        if(lstcnt.gt.n)lstcnt=1
        cmrk(clist(lstcnt))=-1
      enddo
      cnum=0
      do row=1,m
        if(rowsta(row).gt.-2)then
          if((p(row)-q(row)).gt.(abs(p(row))+1.0d+0)*tfeas)then
            code=3
            cnum=-row-n
            goto 999
          else if (iand(prelev,512).gt.0)then
            if(q(row)-p(row).lt.(abs(p(row))+1.0d+0)*tfeas)then
              sol=(p(row)+q(row))/2.0d+0
              prelen=prelen+1
              prehis(prelen)=row
              rowsta(row)=-2-procn
              traf=.true.
              call lpremove(n,m,nz,row,rowidx,rownzs,colidx,colnzs,
     x        rowbeg,rowend,colbeg,colend,obj,sol,traf)
              addobj=addobj+rhs(row)*sol
              do i=rowbeg(row),rowend(row)
                col=rowidx(i)
                if(colmrk(col).lt.0)then
                  coln=coln+1
                  collst(coln)=col
                endif
                colmrk(col)=procn
              enddo
              rrem=rrem+1
            endif
          endif
        endif
      enddo

c checking variables

      do 10 col=1,n
        if(colsta(col).le.-2)goto 10
        toler=(abs(obj(col))+1.0d+0)*tfeas
        if(upb(col).lt.lbig)then
          i=1
        else
          i=0
        endif
        if(lob(col).gt.-lbig)then
          j=1
        else
          j=0
        endif
        if((mbig(col).eq.i).and.(obj(col)-minc(col).lt.-toler))then
          sol=upb(col)
        else if((pbig(col).eq.j).and.(obj(col)-maxc(col).ge.toler))then
          sol=lob(col)
        else
          goto 10
        endif

c variable is set to a bound

        if(abs(sol).gt.lbig)then
          if(abs(obj(col)).gt.tzer)then
            cnum=-col
            code=3
            goto 999
          endif
        endif
        prelen=prelen+1
        prehis(prelen)=col
        colsta(col)=-2-procn
        traf=.true.
        call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x  colbeg,colend,rowbeg,rowend,rhs,sol,traf)
        crem=crem+1
        addobj=addobj+obj(col)*sol
        do i=colbeg(col),colend(col)
          j=colidx(i)
          if(rowmrk(j).lt.0)then
            rown=rown+1
            rowlst(rown)=j
          endif
          rowmrk(j)=procn
        enddo
        dir=-1
        call lpchgmxm(colbeg(col),colend(col),upb(col),lob(col),colidx,
     x  colnzs,ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
        upb(col)=sol
        lob(col)=sol
  10  continue

 999  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'duchek:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif
      return
      end

c ===========================================================================
c ===========================================================================

      subroutine lpelimin(m,n,nz,cfre,rfre,
     x colbeg,ccol,rowbeg,crow,colidx,rowidx,colnzs,colsta,rowsta,
     x obj,rhs,vartyp,slktyp,cpermf,cpermb,rpermf,rpermb,colcan,
     x mark,cfill,rfill,workr,pivcol,pivrow,abstol,reltol,filtol,
     x pivotn,nfill,addobj,pnt,code)

      integer*4 m,n,nz,cfre,rfre,colbeg(n),ccol(n),rowbeg(m),
     x crow(m),colidx(cfre),rowidx(rfre),colsta(n),rowsta(m),
     x cpermf(n),cpermb(n),rpermf(m),rpermb(m),colcan(n),mark(m),
     x cfill(n),rfill(m),pivcol(n),pivrow(m),pivotn,code,vartyp(n),
     x slktyp(m),nfill,pnt
      real*8 workr(m),obj(n),rhs(m),colnzs(cfre),abstol,reltol,
     x filtol,addobj

      integer*4 i,j,k,l,p,pnt1,pnt2,ppnt1,ppnt2,pcol,prow,
     x fren,cfirst,rfirst,clast,rlast,endmem,prewcol,mn,fill,
     x ccfre,rcfre,rpnt1,rpnt2,ii
      real*8 pivot,s

c ---------------------------------------------------------------------------

c     cpermf       oszloplista elore lancolasa, fejmutato cfirst
c     cpermb       oszloplista hatra lancolasa, fejmutato clast
c     rpermf       sorlista    elore lancolase, fejmutato rfirst
c     rpermb       sorlista    hatra lancolasa, fejmutato rlast
c     colcan       lehetseges pivot oszlopok
c     ccol         oszlopszamlalok
c     crow         sorszamlalok (vcstat)
c     colbeg       oszlopmutatok
c     rowbeg       sormutatok
c     mark         eliminacios integer segedtomb
c     workr        eliminacios real    segedtomb
c     cfill        a sorfolytonos tarolas update-elesehez segedtomb
c     rfill        a sorfolytonos tarolas update-elesehez segedtomb
c     pivcol
c     pivrow

c --------------------------------------------------------------------------

c initialization

      nfill=0
      mn=m+n
      endmem=cfre
      fren =0
      pivotn=0
      cfirst=0
      clast =0
      rfirst=0
      rlast =0
      do i=1,n
        if(colsta(i).gt.-2)then
          if(cfirst.eq.0)then
            cfirst=i
          else
            cpermf(clast)=i
          endif
          cpermb(i)=clast
          clast=i
          ccol(i)=ccol(i)-colbeg(i)+1
          if(vartyp(i).eq.0)then
            fren=fren+1
            colcan(fren)=i
          endif
        endif
      enddo
      cpermf(clast)=0
      do i=1,m
        mark(i)=0
        if(rowsta(i).gt.-2)then
          if(rfirst.eq.0)then
            rfirst=i
          else
            rpermf(rlast)=i
          endif
          rpermb(i)=rlast
          rlast=i
          crow(i)=crow(i)-rowbeg(i)+1
        endif
      enddo
      rpermf(rlast)=0

c elimination loop

  50  pcol=0
      prow=0
      i=-1

c find pivot

      do ii=1,fren
        p=colcan(ii)
        pnt1=colbeg(p)
        pnt2=pnt1+ccol(p)-1
        s=0.0d+0
        do j=pnt1,pnt2
          if(s.lt.abs(colnzs(j)))s=abs(colnzs(j))
        enddo
        s=s*reltol
        do j=pnt1,pnt2
          if(slktyp(colidx(j)).eq.0)then
            if(abs(colnzs(j)).gt.abstol)then
              k=(ccol(p)-1)*(crow(colidx(j))-1)
              if(dble(k).lt.filtol*dble(ccol(p)+crow(colidx(j))-1))then
                if((i.lt.0).or.(k.lt.i))then
                  if(abs(colnzs(j)).gt.s)then
                    i=k
                    pcol=p
                    prow=colidx(j)
                    pivot=colnzs(j)
                    ppnt1=ii
                  endif
                else if((k.eq.i).and.(abs(pivot).lt.abs(colnzs(j))))then
                  pcol=p
                  prow=colidx(j)
                  pivot=colnzs(j)
                  ppnt1=ii
                endif
              endif
            endif
          endif
        enddo
      enddo
      if (pcol.eq.0)goto 900
      colcan(ppnt1)=colcan(fren)
      fren=fren-1
      pivot=1.0d+0/pivot
      rcfre=rfre-rowbeg(rlast)-crow(rlast)
      ccfre=endmem-colbeg(clast)-ccol(clast)

c compress column file

      if(ccfre.lt.mn)then
        call lpccomprs(mn,cfre,ccfre,endmem,nz,
     x  colbeg,ccol,cfirst,cpermf,colidx,colnzs,code)
        if(code.lt.0)goto 999
      endif

c remove pcol from the cpermf lists

      j=cpermb(pcol)
      i=cpermf(pcol)
      if(j.ne.0)then
        cpermf(j)=i
      else
        cfirst=i
      endif
      if(i.eq.0)then
        clast=j
      else
        cpermb(i)=j
      endif

c remove prow from the rpermf lists

      j=rpermb(prow)
      i=rpermf(prow)
      if(j.ne.0)then
        rpermf(j)=i
      else
        rfirst=i
      endif
      if(i.eq.0)then
        rlast=j
      else
        rpermb(i)=j
      endif

c administration

      pivotn=pivotn+1
      pivcol(pivotn)=pcol
      pivrow(pivotn)=prow
      addobj=addobj+obj(pcol)*rhs(prow)*pivot

c create pivot column

      pnt1=colbeg(pcol)
      pnt2=pnt1+ccol(pcol)-1
      ppnt1=endmem-ccol(pcol)
      ppnt2=ppnt1+ccol(pcol)-1
      pnt=ppnt1
      do j=pnt1,pnt2
        k=colidx(j)
        mark(k)=1
        colidx(pnt)=k
        colnzs(pnt)=colnzs(j)
        if(k.eq.prow)then
          p=pnt
          workr(k)=pivot
        else
          workr(k)=-colnzs(j)*pivot
          rhs(k)=rhs(k)+rhs(prow)*workr(k)
        endif
        pnt=pnt+1

        i=rowbeg(k)
        do while(rowidx(i).ne.pcol)
          i=i+1
        enddo
        rowidx(i)=rowidx(rowbeg(k)+crow(k)-1)
        rfill(k)=-1
      enddo

      colbeg(pcol)=ppnt1
      j=colidx(ppnt1)
      s=colnzs(ppnt1)
      colidx(ppnt1)=colidx(p)
      colnzs(ppnt1)=colnzs(p)
      colidx(p)=j
      colnzs(p)=s
      ppnt1=ppnt1+1

c create pivot row

      pnt1=rowbeg(prow)
      crow(prow)=crow(prow)-1
      pnt2=pnt1+crow(prow)-1
      rowbeg(prow)=colbeg(pcol)-crow(prow)
      pnt=rowbeg(prow)
      do i=pnt1,pnt2
        k=rowidx(i)
        j=colbeg(k)
        do while(colidx(j).ne.prow)
          j=j+1
        enddo
        colidx(pnt)=k
        colnzs(pnt)=colnzs(j)
        pnt=pnt+1
        colidx(j)=colidx(colbeg(k)+ccol(k)-1)
        colnzs(j)=colnzs(colbeg(k)+ccol(k)-1)
        cfill(k)=ccol(k)-1
      enddo
      endmem=endmem-ccol(pcol)-crow(prow)
      ccfre=ccfre-ccol(pcol)-crow(prow)

c elimination loop

      rpnt1=rowbeg(prow)
      rpnt2=rpnt1+crow(prow)-1
      do p=rpnt1,rpnt2
        i=colidx(p)
        s=colnzs(p)
        obj(i)=obj(i)-s*obj(pcol)*pivot
        fill=ccol(pcol)-1
        pnt1=colbeg(i)
        pnt2=pnt1+cfill(i)-1
        do j=pnt1,pnt2
          k=colidx(j)
          if(mark(k).ne.0)then
            colnzs(j)=colnzs(j)+s*workr(k)
            fill=fill-1
            mark(k)=0
          endif
        enddo

c compute the free space

        j=cpermf(i)
        if(j.eq.0)then
          k=endmem-pnt2-1
        else
          k=colbeg(j)-pnt2-1
        endif

c move column to the end of the column file

        if(fill.gt.k)then
          if (ccfre.lt.m)then
            call lpccomprs(mn,cfre,ccfre,endmem,nz,
     x      colbeg,ccol,cfirst,cpermf,colidx,colnzs,code)
            if(code.lt.0)goto 999
            pnt1=colbeg(i)
            pnt2=pnt1+cfill(i)-1
          endif
          if(i.ne.clast)then
            j=colbeg(clast)+ccol(clast)
            colbeg(i)=j
            do k=pnt1,pnt2
              colidx(j)=colidx(k)
              colnzs(j)=colnzs(k)
              j=j+1
            enddo
            pnt1=colbeg(i)
            pnt2=j-1
            k=cpermf(i)
            j=cpermb(i)
            if(j.eq.0)then
              cfirst=k
            else
              cpermf(j)=k
            endif
            cpermb(k)=j
            cpermf(clast)=i
            cpermb(i)=clast
            clast=i
            cpermf(clast)=0
          endif
        endif

c create fill-in

        do k=ppnt1,ppnt2
          j=colidx(k)
          if(mark(j).eq.0)then
            mark(j)=1
          else
            pnt2=pnt2+1
            colnzs(pnt2)=s*workr(j)
            colidx(pnt2)=j
            rfill(j)=rfill(j)+1
          endif
        enddo
        ccol(i)=pnt2-pnt1+1
        if(i.eq.clast)then
          ccfre=endmem-pnt2
        endif
      enddo

c make space for fills in the row file

      do j=ppnt1,ppnt2
        i=colidx(j)
        mark(i)=0

c compute the free space

        pnt2=rowbeg(i)+crow(i)-1
        p=rpermf(i)
        if(p.eq.0)then
          k=rfre-pnt2-1
        else
          k=rowbeg(p)-pnt2-1
        endif

c move row to the end of the row file

        if(k.lt.rfill(i))then
          if(rcfre.lt.n)then
            call lprcomprs(mn,rfre,
     x      rcfre,rowbeg,crow,rfirst,rpermf,rowidx,code)
            if(code.lt.0)goto 999
          endif
          if(p.ne.0)then
            pnt1=rowbeg(i)
            pnt2=pnt1+crow(i)-1
            pnt=rowbeg(rlast)+crow(rlast)
            rowbeg(i)=pnt
            do l=pnt1,pnt2
              rowidx(pnt)=rowidx(l)
              pnt=pnt+1
            enddo
            prewcol=rpermb(i)
            if(prewcol.eq.0)then
              rfirst=p
            else
              rpermf(prewcol)=p
            endif
            rpermb(p)=prewcol
            rpermf(rlast)=i
            rpermb(i)=rlast
            rlast=i
            rpermf(rlast)=0
          endif
        endif
        crow(i)=crow(i)+rfill(i)
        if(i.eq.rlast)rcfre=rfre-crow(i)-rowbeg(i)
        nfill=nfill+rfill(i)+1
      enddo

c make pointers to the end of the filled rows

      do j=ppnt1,ppnt2
        rfill(colidx(j))=rowbeg(colidx(j))+crow(colidx(j))-1
      enddo

c generate fill-in the row file

      do j=rpnt1,rpnt2
        i=colidx(j)
        pnt1=colbeg(i)+cfill(i)
        pnt2=colbeg(i)+ccol(i)-1
        do k=pnt1,pnt2
          rowidx(rfill(colidx(k)))=i
          rfill(colidx(k))=rfill(colidx(k))-1
        enddo
      enddo
      goto 50

c end of the elimination, compress arrays

 900  call lprcomprs(mn,rfre,rcfre,rowbeg,crow,rfirst,rpermf,rowidx,
     *               code)
      pnt=endmem
      i=clast
      do while(i.ne.0)
        pnt1=colbeg(i)
        pnt2=pnt1+ccol(i)-1
        do j=pnt2,pnt1,-1
          pnt=pnt-1
          colidx(pnt)=colidx(j)
          colnzs(pnt)=colnzs(j)
        enddo
        colbeg(i)=pnt
        i=cpermb(i)
      enddo

c make pointers form counters

      do i=1,n
        ccol(i)=colbeg(i)+ccol(i)-1
      enddo
      do i=1,m
        crow(i)=rowbeg(i)+crow(i)-1
      enddo
 999  return
      end

c ===========================================================================
c supernodal left looking, primer supernode loop (cache),
c supernode update with indirect addressing
c relative pivot tolerance
c ==========================================================================

      subroutine lpmfactor(ecolpnt,
     x vcstat,colpnt,rowidx,pivots,count,mut,nonzeros,
     x diag,err,updat,list,index,dropn,slktyp,
     x snhead,fpnt,invperm,nodtyp,dv,odiag)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 err,list(mn),mut(mn),dropn
      integer*4 ecolpnt(mn),vcstat(mn),colpnt(n1),rowidx(cfree)
      integer*4 pivots(mn),count(mn),index(mn),slktyp(m)
      integer*4 snhead(mn),fpnt(mn),invperm(mn),nodtyp(mn)
      real*8 nonzeros(cfree),diag(mn),updat(mn),dv(m),odiag(mn)

      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c --------------------------------------------------------------------------
      integer*4 i,j,k,l,o,p,pnt1,pnt2,ppnt1,ppnt2,mk,col,kprew,rb,
     x ppnode,prewnode,w1
      real*8 s,diap,diam
      character*99 buff
c---------------------------------------------------------------------------
      err=0
      w1=0

c  initialization

      do 10 i=1,mn
        list(i)=0
        index(i)=0
        updat(i)=0.0d+0
        fpnt(i)=0
  10  continue

c initialize dll

      do 15 i=1,n
        if(vcstat(i).le.-2)goto 15
        k=ecolpnt(i)
        if(k.le.nz)goto 15
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        if(pnt1.le.pnt2)then
          o=rowidx(pnt1)
          fpnt(i)=index(o)
          index(o)=i
          list(i)=pnt1
        endif
  15  continue

c  set the extra part of the matrix using a dll

      do 20 col=1,pivotn
        i=pivots(col)
        pnt1=ecolpnt(i)
        if(pnt1.le.nz)goto 20
        pnt2=count(i)
        o=0
        if(i.le.n)then
          if(vcstat(i).le.-2)goto 20
          ppnt1=list(i)
          ppnt2=colpnt(i+1)-1
          do 18 j=ppnt1,ppnt2
            k=rowidx(j)
            updat(k)=nonzeros(j)
            o=o+1
            mut(o)=k
  18      continue
          list(i)=ppnt2+1
        else
          kprew=index(i)
          if(kprew.eq.0)goto 25
          if(vcstat(i).le.-2)then
  21        mk=fpnt(kprew)
            ppnt1=list(kprew)+1
            if(ppnt1.lt.colpnt(kprew+1))then
              list(kprew)=ppnt1
              k=rowidx(ppnt1)
              fpnt(kprew)=index(k)
              index(k)=kprew
            endif
            kprew=mk
            if(kprew.ne.0)goto 21
          else
  22        mk=fpnt(kprew)
            ppnt1=list(kprew)+1
            if(ppnt1-colpnt(kprew+1))11,12,13
  11        updat(kprew)=nonzeros(ppnt1-1)
            list(kprew)=ppnt1
            k=rowidx(ppnt1)
            fpnt(kprew)=index(k)
            index(k)=kprew
            o=o+1
            mut(o)=kprew
            goto 13
  12        updat(kprew)=nonzeros(ppnt1-1)
            list(kprew)=ppnt1
            o=o+1
            mut(o)=kprew
  13        kprew=mk
            if(kprew.ne.0)goto 22
          endif
        endif

c set column i and delete updat

  25    do 23 j=pnt1,pnt2
           nonzeros(j)=updat(rowidx(j))
  23    continue
        do 26 j=1,o
          updat(mut(j))=0
  26    continue
  20  continue

c  initialize for the computation

      do 30 i=1,mn
        mut(i)=0
        fpnt(i)=ecolpnt(i)
        list(i)=0
        index(i)=0
        updat(i)=0.0
  30  continue
      ppnode=0
      prewnode=0
      i=0

c  loop for pivot columns

 100  i=i+1
      if(i.gt.pivotn)goto 60
      col=pivots(i)
      ppnt1=ecolpnt(col)
      ppnt2=count(col)

c  step vcstat if relaxed

      if(vcstat(col).le.-2)then
        call lpcolremv(i,col,mut,index,fpnt,count,pivots,invperm,
     x  snhead,nodtyp,rowidx,nonzeros,ppnode,prewnode)
        do 75 j=ppnt1,ppnt2
          k=rowidx(j)
          if((k.gt.n).or.(ecolpnt(k).le.nz))goto 75
          l=colpnt(k)
          o=colpnt(k+1)-1
          do p=l,o
            if(rowidx(p).eq.col)then
              call lpmove(p,o,rowidx,nonzeros)
              goto 75
            endif
          enddo
  75    continue
        i=i-1
        if((ppnode.gt.0).and.(prewnode.eq.i))goto 110
        goto 100
      endif

      if(ppnt1.le.nz)then
        diag(col)=1.0d00/diag(col)
        goto 180
      endif
      kprew=index(col)

c repack a column

      do k=ppnt1,ppnt2
        updat(rowidx(k))=nonzeros(k)
      enddo
      if(col.le.n)then
        diam=diag(col)
        diap=0.0d+0
      else
        diap=diag(col)
        diam=0.0d+0
      endif
 130  if(kprew)129,150,131

c standard transformation

 131  k=mut(kprew)
      pnt1=fpnt(kprew)
      pnt2=count(kprew)
      if(pnt1.lt.pnt2)then
        o=rowidx(pnt1+1)
        mut(kprew)=index(o)
        index(o)=kprew
      endif
      pnt1=pnt1+1
      fpnt(kprew)=pnt1
      s=-nonzeros(pnt1-1)*diag(kprew)
      if(kprew.le.n)then
        diap=diap+s*nonzeros(pnt1-1)
      else
        diam=diam+s*nonzeros(pnt1-1)
      endif
      do 170 o=pnt1,pnt2
        updat(rowidx(o))=updat(rowidx(o))+s*nonzeros(o)
 170  continue
      kprew=k
      goto 130

c supernodal transformation

 129  kprew=-kprew
      k=mut(kprew)
      p=invperm(kprew)
      pnt1=fpnt(kprew)+1
      if(pnt1.le.count(kprew))then
        o=rowidx(pnt1)
        mut(kprew)=index(o)
        index(o)=-kprew
      endif
      if(kprew.le.n)then
        call lpcspnd(p,snhead(p),diag,nonzeros,
     x  fpnt,count,pivots,updat,diap,rowidx(pnt1))
      else
        call lpcspnd(p,snhead(p),diag,nonzeros,
     x  fpnt,count,pivots,updat,diam,rowidx(pnt1))
      endif
      kprew=k
      goto 130

c  pack a column

 150  do k=ppnt1,ppnt2
        nonzeros(k)=updat(rowidx(k))
      enddo

c  set up diag

      if((ppnode.le.0).or.(prewnode.ne.snhead(i)))then
        diap=diap+diam
        diam=max(trabs,abs(diam*trabs))
        if(abs(diap).lt.diam)then
          call lprngchk(rowidx,nonzeros,ecolpnt(col),count(col),
     x    vcstat,rb,diag,slktyp,dropn,col,dv,diap,w1,odiag(col))
          if(rb.ne.0)err=1
          diag(col)=diap
          if(vcstat(col).le.-2)goto 100
        else
          diag(col)=1.0d00/diap
        endif
      else
        diag(col)=diam
        updat(col)=diap
      endif

c transformation in (primer) supernode

 110  if(prewnode.eq.i)then
        if(ppnode.gt.0)then
          do j=ppnode+1,i
            o=j-1
            p=pivots(j)
            call lpcspnode(ppnode,o,diag,nonzeros,fpnt,count,pivots,
     x      nonzeros(ecolpnt(p)),diag(p))
            diam=max(trabs,abs(diag(p)*trabs))
            diag(p)=diag(p)+updat(p)
            if(abs(diag(p)).lt.diam)then
              call lprngchk(rowidx,nonzeros,ecolpnt(p),count(p),
     x        vcstat,rb,diag,slktyp,dropn,p,dv,diag(p),w1,odiag(p))
              if(rb.ne.0)err=1
            else
              diag(p)=1.0d+0/diag(p)
            endif
          enddo
        endif
        ppnode=0
      endif

c update the linked list

 180  if(snhead(i).eq.0)then
        ppnode=0
        if(ppnt1.le.ppnt2)then
          j=rowidx(ppnt1)
          mut(col)=index(j)
          index(j)=col
        endif
        prewnode=0
      else
        if(prewnode.ne.snhead(i))then
          prewnode=snhead(i)
          if(nodtyp(i).gt.0)then
            ppnode=i
          else
            ppnode=-i
          endif
          if(ecolpnt(pivots(prewnode)).le.count(pivots(prewnode)))then
            j=rowidx(ecolpnt(pivots(prewnode)))
            mut(col)=index(j)
            index(j)=-col
          endif
        endif
      endif

c  end of the main loop

      goto 100

c  end of mfactor

  60  if(w1.gt.0)then
        write(buff,'(1x,a,i6)')'total warnings of row dependencies:',w1
        call lpmprnt(buff)
      endif
      return
      end

c =============================================================================
c     nem relativ nullazassal
c ==========================================================================

      subroutine lpaugftr(ecolpnt,
     x vcstat,rowidx,pivots,count,nonzeros,diag,vector)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 ecolpnt(mn),vcstat(mn),rowidx(cfree)
      integer*4 pivots(mn),count(mn)
      real*8 nonzeros(cfree),diag(mn),vector(mn)

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c --------------------------------------------------------------------------
      integer*4 i,j,pnt1,pnt2,col,o
      real*8 val
c---------------------------------------------------------------------------
      do i=1,pivotn
        col=pivots(i)
        if (vcstat(col).gt.-2)then
          val=vector(col)*diag(col)
          if(abs(val).gt.tzer)then
            pnt1=ecolpnt(col)
            pnt2=count(col)
            do j=pnt1,pnt2
              o=rowidx(j)
              vector(o)=vector(o)-val*nonzeros(j)
            enddo
          endif
        endif
      enddo
      do i=1,mn
        if(vcstat(i).le.-2)vector(i)=0
      enddo
      return
      end

c ==========================================================================

      subroutine lpaugbtr(ecolpnt,
     x vcstat,rowidx,pivots,count,nonzeros,diag,vector)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 ecolpnt(mn),vcstat(mn),rowidx(cfree)
      integer*4 pivots(mn),count(mn)
      real*8 nonzeros(cfree),diag(mn),vector(mn)
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c --------------------------------------------------------------------------
      integer*4 i,j,col,pnt1,pnt2
      real*8 sol
c---------------------------------------------------------------------------

      do i=1,pivotn
        col=pivots(pivotn+1-i)
        if(vcstat(col).gt.-2)then
          sol=vector(col)
          pnt1=ecolpnt(col)
          pnt2=count(col)
          do j=pnt1,pnt2
            sol=sol-nonzeros(j)*vector(rowidx(j))
          enddo
          vector(col)=sol*diag(col)
        endif
      enddo
      return
      end
c ==========================================================================
c find pivot in the augmented system
c prefer the pivot for expanding the supernodes
c method=0  minimum count
c method=1  minimum local fill in
c ===========================================================================

      subroutine lpfndpiv(cpnt,cnext,pntc,ccol,crow,rowidx,nonzeros,
     x diag,pivcol,pivot,md,method,inds,mark,rindex,pntr)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpfactor/   tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      real*8           tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens

      integer*4 cpnt(mn),cnext(mn),pntc(mn),ccol(mn),crow(mn),pivcol,
     x rowidx(cfree),md,method,inds(mn),mark(mn),rindex(rfree),
     x pntr(mn)
      real*8    nonzeros(cfree),diag(mn),pivot

c --------------------------------------------------------------------------
      integer*4 j,k,l,o,nnz,ffind,oldpcol,oldlen,p1,p2,srcmod
      integer*4 fill,mfill,q,oo,kk
      real*8    sol,stab,stab1,d,toler,ss
c --------------------------------------------------------------------------

c find pivot in sparse columns

      mfill=-1
      toler=tpiv1
      if(md.gt.0)then
        srcmod=1
        goto 101
      endif
  10  pivcol=pivcol+1
      if (pivcol.ge.n)goto 100
      if(crow(pivcol).ne.0)goto 10
      if(ccol(pivcol).gt.lam)goto 10
      pivot=diag(pivcol)
      if(abs(pivot).lt.tpiv2)goto 10
      goto 200

c find pivot in the another columns

 100  md=1
      srcmod=0
 101  oldpcol=pivcol
      pivcol=0
      stab1=0
      pivot=0
      nnz=md-1
      ffind=0
      if(nnz.lt.1)nnz=1
      md=md-1
      if(md.le.1)md=1

c find supernodal pivot (srcmode=1)

 115  if(oldpcol.eq.0)goto 112
      p1=pntc(oldpcol)
      p2=p1+ccol(oldpcol)-1
      oldlen=ccol(oldpcol)
 125  if(p1.gt.p2)goto 114
      j=rowidx(p1)
      if((crow(j)+ccol(j)).lt.oldlen)goto 121
 145  p1=p1+1
      goto 125
 114  if(pivcol.gt.0)goto 200

c find another pivot

 112  srcmod=0
      md=0
 110  j=cpnt(nnz)
      if((j.gt.0).and.(md.eq.0))md=nnz
 120  if(j.le.0)goto 150

c compute fill in

      if(method.ne.0)then
        q=0
        k=pntc(j)
        l=k+ccol(j)-1
        do o=k,l
          q=q+1
          inds(q)=rowidx(o)
          mark(rowidx(o))=1
        enddo
        k=pntr(j)
        l=k+crow(j)-1
        do o=k,l
          q=q+1
          inds(q)=rindex(o)
          mark(rindex(o))=1
        enddo
        fill=(q*(q-1))/2
        do kk=1,q
          o=inds(kk)
          k=pntc(o)
          l=k+ccol(o)-1
          do oo=k,l
            fill=fill-mark(rowidx(oo))
          enddo
        enddo
        do o=1,q
          mark(inds(o))=0
        enddo
      else
        fill=crow(j)
      endif
      ffind=ffind+1
      if((mfill.ge.0).and.(fill.ge.mfill))goto 130
 121  d=diag(j)
      sol=abs(d)
      if(sol.lt.tabs)goto 130
      k=pntc(j)

c stability test

      stab=sol
      l=k+ccol(j)-1
      do 32 o=k,l
        ss=abs(nonzeros(o))
        if(stab.lt.ss)stab=ss
  32  continue
      stab=sol/stab
      if(stab.lt.toler)goto 130
      if(mfill.lt.0)mfill=fill+1
      if((fill.lt.mfill).or.((fill.eq.mfill).and.(stab.gt.stab1)))then
        pivot=d
        pivcol=j
        stab1=stab
        mfill=fill
        goto 130
      endif
 130  if((srcmod.gt.0).and.(pivcol.ne.0))then
cccc          md=md-1
cccc          if(md.lt.1)md=1
         goto 200
      endif
      if((ffind.gt.tfind).and.(pivcol.ne.0))goto 200
      if(srcmod.gt.0)goto 145
      j=cnext(j)
      goto 120
 150  if((pivcol.eq.0).or.(method.ne.0))then
        nnz=nnz+1
        if(nnz.le.mn)goto 110
        if(pivcol.gt.0)goto 200
        toler=toler/10
        nnz=md
        if((toler.ge.tpiv2).and.(nnz.gt.0))goto 115
        md=1
      endif
 200  return
      end

c ==========================================================================
c multi predictor-corrector direction
c l2 norm
c ===========================================================================

      subroutine lpcitref(diag,odiag,pivots,rowidx,nonzeros,colpnt,
     x ecolpnt,count,vcstat,xrhs,rwork1,rwork2,rwork3,
     x bounds,xs,up,vartyp,slktyp)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
      common/lpitref/ tresx,tresy,maxref
      real*8        tresx,tresy
      integer*4     maxref

      integer*4 ecolpnt(mn),count(mn),rowidx(cfree),
     x pivots(mn),colpnt(n1),vcstat(mn),vartyp(n),slktyp(m)
      real*8 diag(mn),odiag(mn),nonzeros(cfree),xrhs(mn),
     x rwork1(mn),rwork2(mn),rwork3(mn),bounds(mn),xs(mn),up(mn)

c ---------------------------------------------------------------------------

      integer*4 i,j,pnt1,pnt2,refn
      real*8 maxrx,maxry,sx,sol,l2,ol2

c ---------------------------------------------------------------------------

c simple case : no refinement

      if(maxref.le.0)then
        call lpaugftr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,xrhs)
        call lpaugbtr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x  diag,xrhs)
        goto 999
      endif
      do i=1,mn
        rwork1(i)=xrhs(i)
      enddo
      ol2=1.0d+0/tzer
      do i=1,mn
        rwork3(i)=0.0d+0
      enddo
      refn=-1

c main loop

  10  refn=refn+1
      call lpaugftr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x diag,xrhs)
      call lpaugbtr(ecolpnt,vcstat,rowidx,pivots,count,nonzeros,
     x diag,xrhs)
      do i=1,mn
        xrhs(i)=xrhs(i)+rwork3(i)
      enddo

c compute the residuals

      l2=0.0d+0
      maxrx=0.0d+0
      maxry=0.0d+0
      do i=1,mn
        rwork2(i)=rwork1(i)-odiag(i)*xrhs(i)
      enddo
      do i=1,n
        if(vcstat(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          sx=xrhs(i)
          sol=rwork2(i)
          do j=pnt1,pnt2
            rwork2(rowidx(j))=rwork2(rowidx(j))-nonzeros(j)*sx
            sol=sol-nonzeros(j)*xrhs(rowidx(j))
          enddo
          rwork2(i)=sol
          if(maxry.lt.abs(sol))maxry=abs(sol)
          l2=l2+sol*sol
        endif
      enddo
      do i=1,m
        if(vcstat(i+n).gt.-2)then
           if(maxrx.lt.abs(rwork2(i+n)))maxrx=abs(rwork2(i+n))
           l2=l2+rwork2(i+n)*rwork2(i+n)
        endif
      enddo
      l2=sqrt(l2)
      if(l2.ge.ol2)then
        do i=1,mn
          xrhs(i)=rwork3(i)
        enddo
      else
        if((maxrx.gt.tresx).or.(maxry.gt.tresy))then
          if(refn.lt.maxref)then
            ol2=l2
            do i=1,mn
              rwork3(i)=xrhs(i)
              xrhs(i)=rwork2(i)
            enddo
            goto 10
          endif
        endif
      endif

c end of the main loop, reset work3 (upinf)=bounds-xs-up

      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.lt.0)then
            sol=bounds(i)-xs(i)-up(i)
          else
            sol=0.0d+0
          endif
        else
          sol=0.0d+0
        endif
        rwork3(i)=sol
      enddo

c return

 999  return
      end

c ============================================================================
c minimum local fill-in ordering

c ===========================================================================

      subroutine lpgenmfo(m,mn,nz,cfree,rfree,pivotn,
     x pntc,ccol,permut,pntr,crow,rowidx,
     x mark,cpermf,cpermb,rpermf,rpermb,cfill,rfill,cpnt,
     x cnext,cprew,suplst,fillin,colidx,tfind,noddeg,supdeg,code)

      integer*4 m,mn,nz,cfree,rfree,pivotn,rowidx(cfree),colidx(rfree),
     x permut(m),cpermf(m),cpermb(m),rpermf(m),rpermb(m),
     x ccol(m),crow(m),pntc(m),pntr(m),mark(m),cfill(m),cpnt(m),
     x cnext(m),cprew(m),rfill(m),suplst(m),fillin(m),tfind,
     x noddeg(m),supdeg(m),code
      character*99 buff

c ---------------------------------------------------------------------------
c input parameters

c  m       number of rows
c  mn      an number greather than m
c  nz      last used position of the column file
c  cfree   length of the column file (column file is used from nz+1 to cfree)
c  rfree   length of the row file (row file is used from 1 to rfree)
c  rowidx  column file (containing the lower tiriangular part of aat)
c  colidx  row file
c  pntc    pointer to the columns of the lower diagonal of aat
c  ccol    column lengths of aat
c  crow    if crow(i)<-1 row i is removed from the ordering
c  tfind   search loop,  tfind=0 gives the minimum degree ordering
c          suggested value tfind=25


c output parameters
c permut   the ordering
c pivotn   number of ordered nodes


c others: integer working arrays of size m


c --------------------------------------------------------------------------
      integer*4 pnt,pnt1,pnt2,i,j,k,l,o,p,endmem,ccfree,rcfree,pmode,
     x rfirst,rlast,cfirst,clast,pcol,pcnt,ppnt1,ppnt2,fill,prewcol,
     x ii,mm,mfill,supnd,hsupnd,oo,nnz,fnd,oldpcol,q,fl
c---------------------------------------------------------------------------

   1  format(' not enough memory in the row    file ')
   2  format(' not enough memory in the column file ')
   3  format(' analyse for supernodes in aat    :',i9,' col')
   4  format(' final supernodal columns disabled:',i9,' col')
   5  format(' hidden supernodal columns        :',i9,' col')


c initialization

      code=0
      endmem=cfree
      pivotn=0
      pmode =0
      do i=1,m
        permut(i)=0
        suplst(i)=0
        fillin(i)=-1
        supdeg(i)=1
        if(crow(i).gt.-2)then
          crow(i)=0
        endif
      enddo

c compute crow

      do 10 i=1,m
        if(crow(i).le.-2)goto 10
        pnt1=pntc(i)
        pnt2=pnt1+ccol(i)-1
        do j=pnt1,pnt2
          crow(rowidx(j))=crow(rowidx(j))+1
        enddo
        clast=i
  10  continue
      cpermf(clast)=0
      ccfree=cfree-pntc(clast)-ccol(clast)
      if(ccfree.lt.mn)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif

c create pointers to colidx

      do i=1,m
        cprew(i)=0
      enddo
      pnt=1
      do i=1,m
        if(crow(i).ge.0)then
          pntr(i)=pnt
          rfill(i)=pnt
          pnt=pnt+crow(i)
        endif
      enddo
      rcfree=rfree-pnt
      if(rcfree.lt.mn)then
        write(buff,1)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif

c create the row file : symbolical transps the matrix, set up noddeg

      do i=1,m
        noddeg(i)=ccol(i)+crow(i)
        if(crow(i).ge.0)then
          pnt1=pntc(i)
          pnt2=pnt1+ccol(i)-1
          do j=pnt1,pnt2
            k=rowidx(j)
            colidx(rfill(k))=i
            rfill(k)=rfill(k)+1
          enddo
        endif
      enddo

c search supernodes

      hsupnd=0
      supnd=0
      do i=1,m
        if(crow(i).ge.0)then
          pnt1=pntr(i)
          pnt2=pnt1+crow(i)-1
          do j=pnt1,pnt2
            mark(colidx(j))=i
          enddo
          mark(i)=i
          pnt1=pntc(i)
          pnt2=pnt1+ccol(i)-1
          do j=pnt1,pnt2
            mark(rowidx(j))=i
          enddo
          p=ccol(i)+crow(i)
 118      if (pnt1.le.pnt2)then
            o=rowidx(pnt1)
            call lpchknod(m,cfree,rfree,i,o,p,ccol,crow,mark,pntc,
     x      pntr,rowidx,colidx,supdeg,suplst,ii)
            supnd=supnd+ii
            pnt1=pnt1-ii
            pnt2=pnt2-ii
            pnt1=pnt1+1
            goto 118
          endif
        endif
      enddo
      write(buff,3)supnd
      call lpmprnt(buff)

c set up lists

      do i=1,m
        mark(i)=0
        cpnt(i)=0
        cnext(i)=0
      enddo
      cfirst=0
      clast=0
      rfirst=0
      rlast=0
      mm=0
      do i=1,m
        if(crow(i).ge.0)then
          mm=mm+1
          if(cfirst.eq.0)then
            cfirst=i
          else
            cpermf(clast)=i
          endif
          cpermb(i)=clast
          clast=i

          if(rfirst.eq.0)then
            rfirst=i
          else
            rpermf(rlast)=i
          endif
          rpermb(i)=rlast
          rlast=i

          j=noddeg(i)-supdeg(i)+2
          if(j.gt.0)then
            o=cpnt(j)
            cnext(i)=o
            cpnt(j)=i
            if(o.ne.0)cprew(o)=i
          endif
          cprew(i)=0
        endif
      enddo
      cpermf(clast)=0
      rpermf(rlast)=0
      pcol=0

c loop for pivots

  50  oldpcol=pcol
      pcol=0
      nnz=1
      if(oldpcol.eq.0)goto 9114

c find supernodal pivot

      mfill=0
      k=pntc(oldpcol)
      l=k+ccol(oldpcol)-1
      oo=ccol(oldpcol)-1
9125  if(k.gt.l)goto 9114
      j=rowidx(k)
      if(crow(j)+ccol(j).eq.oo)then
        hsupnd=hsupnd+1
        pcol=j
        goto 9200
      endif
      k=k+1
      goto 9125

c find another pivot

9114  pmode=0
      fnd=0
      mfill=-1
9110  j=cpnt(nnz)
      if((j.gt.0).and.(pmode.eq.0))then
        pmode=nnz
        if(tfind.eq.0)then
          pcol=j
          mfill=1
          goto 9200
        endif
      endif
9120  if(j.le.0)goto 9150
      if(fillin(j).ge.0)then
        fill=fillin(j)
        goto 9175
      endif

c set up mark and cfill

      q=0
      fill=0
      k=pntc(j)
      l=k+ccol(j)-1
      p=0
      do o=k,l
        q=q+1
        cfill(q)=rowidx(o)
        mark(rowidx(o))=supdeg(rowidx(o))
        fill=fill-(supdeg(rowidx(o))*(supdeg(rowidx(o))-1))/2
      enddo
      k=pntr(j)
      l=k+crow(j)-1
      do o=k,l
        q=q+1
        cfill(q)=colidx(o)
        mark(colidx(o))=supdeg(colidx(o))
        fill=fill-(supdeg(colidx(o))*(supdeg(colidx(o))-1))/2
      enddo

c compute fill-in

      fill=fill+((noddeg(j)-supdeg(j))*(noddeg(j)-supdeg(j)+1))/2
      do p=1,q
        fl=0
        o=cfill(p)
        k=pntc(o)
        l=k+ccol(o)-1
        do oo=k,l
          fl=fl+mark(rowidx(oo))
        enddo
        fill=fill-supdeg(o)*fl
      enddo

c administration

      do o=1,q
        mark(cfill(o))=0
      enddo

c test

      fillin(j)=fill
9175  if(mfill.lt.0)mfill=fill+1
      if(fill.lt.mfill)then
        mfill=fill
        pcol=j
      endif
      fnd=fnd+1
      if((fnd.gt.tfind).or.(mfill.eq.0))goto 9200
      j=cnext(j)
      goto 9120

c next bunch

9150  nnz=nnz+1
      if(nnz.le.m)goto 9110
9200  if (pcol.eq.0)goto 900
      endmem=cfree
      ccfree=cfree-pntc(clast)-ccol(clast)
      rcfree=rfree-pntr(rlast)-crow(rlast)

c compress column file

      if(ccfree.lt.mn)then
       call lpmccmpr(mn,cfree,ccfree,endmem,nz,
     x  pntc,ccol,cfirst,cpermf,rowidx,code)
       if(code.lt.0)goto 999
      endif

c remove pcol from the cpermf lists

      prewcol=cpermb(pcol)
      o=cpermf(pcol)
      if(prewcol.ne.0)then
        cpermf(prewcol)=o
      else
        cfirst=o
      endif
      if(o.eq.0)then
        clast=prewcol
      else
        cpermb(o)=prewcol
      endif

c remove pcol from the rpermf lists

      prewcol=rpermb(pcol)
      o=rpermf(pcol)
      if(prewcol.ne.0)then
        rpermf(prewcol)=o
      else
        rfirst=o
      endif
      if(o.eq.0)then
        rlast=prewcol
      else
        rpermb(o)=prewcol
      endif

c administration

      pivotn=pivotn+1
      permut(pivotn)=pcol
      pcnt=ccol(pcol)+crow(pcol)

c remove pcol from the counter lists

      o=cnext(pcol)
      ii=cprew(pcol)
      if(ii.eq.0)then
        cpnt(noddeg(pcol)-supdeg(pcol)+2)=o
      else
        cnext(ii)=o
      endif
      if(o.ne.0)cprew(o)=ii

      ppnt1=endmem-pcnt
      ppnt2=ppnt1+pcnt-1
      endmem=endmem-pcnt
      ccfree=ccfree-pcnt
      pnt=ppnt1

c create pivot column from the row file

      pnt1=pntr(pcol)
      pnt2=pnt1+crow(pcol)-1
      do 70 i=pnt1,pnt2
        o=colidx(i)
        l=pntc(o)
        p=l+ccol(o)-1

c find element and move in the column o

        cfill(o)=ccol(o)-1
        rfill(o)= 0
        do 75 k=l,p
          if(rowidx(k).eq.pcol)then
            mark(o)=1
            rowidx(pnt)=o
            pnt=pnt+1
            rowidx(k)=rowidx(p)
            goto 70
          endif
  75    continue
  70  continue
      mm=pnt

c extend pivot column from the column file

      pnt1=pntc(pcol)
      pnt2=pnt1+ccol(pcol)-1
      do 60 j=pnt1,pnt2
        o=rowidx(j)
        mark(o)=1
        rowidx(pnt)=o
        pnt=pnt+1

c remove pcol from the row file

        rfill(o)=-1
        cfill(o)=ccol(o)
        l=pntr(o)
        p=l+crow(o)-2
        do 55 k=l,p
          if(colidx(k).eq.pcol)then
            colidx(k)=colidx(p+1)
            goto 60
          endif
  55    continue
  60  continue
      pntc(pcol)=ppnt1
      ccol(pcol)=pcnt

c remove columns from the counter lists

      do 77 j=ppnt1,ppnt2
        i=rowidx(j)
        o=cnext(i)
        ii=cprew(i)
        if(ii.eq.0)then
          cpnt(noddeg(i)-supdeg(i)+2)=o
        else
          cnext(ii)=o
        endif
        if(o.ne.0)cprew(o)=ii
  77  continue

c elimination loop

      if(mfill.gt.0)then

        if(ppnt1.lt.mm)call lphpsort((mm-ppnt1),rowidx(ppnt1))
        if(mm.lt.ppnt2)call lphpsort((ppnt2-mm+1),rowidx(mm))

        do 80 p=ppnt1,ppnt2
          i=rowidx(p)

c delete element from mark

          mark(i)=0
          pcnt=pcnt-1

c transformation on the column i

          fill=pcnt
          pnt1=pntc(i)
          pnt2=pnt1+cfill(i)-1
          do 90 k=pnt1,pnt2
             o=rowidx(k)
             if(mark(o).ne.0)then
               fill=fill-1
               mark(o)=0
             endif
  90      continue

c compute the free space

          ii=cpermf(i)
          if(ii.eq.0)then
            k=endmem-pnt2-1
          else
            k=pntc(ii)-pnt2-1
          endif

c move column to the end of the column file

          if(fill.gt.k)then
            if (ccfree.lt.mn)then
              call lpmccmpr(mn,cfree,ccfree,endmem,nz,
     x        pntc,ccol,cfirst,cpermf,rowidx,code)
              if(code.lt.0)goto 999
              pnt1=pntc(i)
              pnt2=pnt1+cfill(i)-1
            endif
            if(i.ne.clast)then
              l=pntc(clast)+ccol(clast)
              pntc(i)=l
              do 95 k=pnt1,pnt2
                rowidx(l)=rowidx(k)
                l=l+1
  95          continue
              pnt1=pntc(i)
              pnt2=l-1
              prewcol=cpermb(i)
              if(prewcol.eq.0)then
                cfirst=ii
              else
                cpermf(prewcol)=ii
              endif
              cpermb(ii)=prewcol
              cpermf(clast)=i
              cpermb(i)=clast
              clast=i
              cpermf(clast)=0
            endif
          endif

c create fill in

          do 97 k=p+1,ppnt2
            o=rowidx(k)
            if(mark(o).eq.0)then
              mark(o)=1
            else
              pnt2=pnt2+1
              rowidx(pnt2)=o
              rfill(o)=rfill(o)+1
            endif
   97     continue
          pnt2=pnt2+1
          ccol(i)=pnt2-pnt1
          if(i.eq.clast)then
            ccfree=endmem-pnt2-1
          endif
  80    continue
      else
        do p=ppnt1,ppnt2
          i=rowidx(p)
          ccol(i)=ccol(i)-1-rfill(i)
          mark(i)=0
        enddo
      endif

c make space for fills in the row file

      do 100 j=ppnt1,ppnt2
        i=rowidx(j)
        if(mfill.eq.0)goto 135
        pnt2=pntr(i)+crow(i)-1

c compute the free space

        ii=rpermf(i)
        if(ii.eq.0)then
          k=rfree-pnt2-1
        else
          k=pntr(ii)-pnt2-1
        endif

c move row to the end of the row file

        if(k.lt.rfill(i))then
          if(rcfree.lt.mn)then
            call lprcomprs(mn,rfree,
     x      rcfree,pntr,crow,rfirst,rpermf,colidx,code)
            if(code.lt.0)goto 999
          endif
          if(ii.ne.0)then
            pnt1=pntr(i)
            pnt2=pnt1+crow(i)-1
            pnt=pntr(rlast)+crow(rlast)
            pntr(i)=pnt
            do 110 l=pnt1,pnt2
              colidx(pnt)=colidx(l)
              pnt=pnt+1
 110        continue

c update the rperm lists

            prewcol=rpermb(i)
            if(prewcol.eq.0)then
              rfirst=ii
            else
              rpermf(prewcol)=ii
            endif
            rpermb(ii)=prewcol
            rpermf(rlast)=i
            rpermb(i)=rlast
            rlast=i
            rpermf(rlast)=0
          endif
        endif
 135    crow(i)=crow(i)+rfill(i)
        if(i.eq.rlast)rcfree=rfree-crow(i)-pntr(i)
        noddeg(i)=noddeg(i)-supdeg(pcol)
 100  continue
      if(mfill.eq.0)goto 150

c make pointers to the end of the filled rows

      do 120 j=ppnt1,ppnt2
        rfill(rowidx(j))=pntr(rowidx(j))+crow(rowidx(j))-1
 120  continue

c generate fill-in in the row file, update noddeg

      do j=ppnt1,ppnt2
        o=rowidx(j)
        pnt1=pntc(o)+cfill(o)
        pnt2=pntc(o)+ccol(o)-1
        do k=pnt1,pnt2
          colidx(rfill(rowidx(k)))=o
          rfill(rowidx(k))=rfill(rowidx(k))-1
          noddeg(o)=noddeg(o)+supdeg(rowidx(k))
          noddeg(rowidx(k))=noddeg(rowidx(k))+supdeg(o)
        enddo
      enddo

c indicate new fill-in computation

      if(tfind.gt.0)then
        do j=ppnt1,ppnt2
          i=rowidx(j)
          fillin(i)=-1
          pnt1=pntc(i)+cfill(i)
          pnt2=pntc(i)+ccol(i)-1
          do pnt=pnt1,pnt2
            ii=rowidx(pnt)
            if(rfill(ii).ge.0)then
              k=pntc(ii)
              l=k+ccol(ii)-1
              do o=k,l
                fillin(rowidx(o))=-1
              enddo
              k=pntr(ii)
              l=k+crow(ii)-1
              do o=k,l
                fillin(colidx(o))=-1
              enddo
              rfill(ii)=-1
            endif
          enddo
        enddo
      endif

c searching for new supernodes

 150  l=0
      j=ppnt1
 151  if(j.le.ppnt2)then
        i=rowidx(j)
        p=ccol(i)+crow(i)

        pnt1=pntc(i)
        pnt2=pnt1+ccol(i)-1
        do k=pnt1,pnt2
          if(mark(rowidx(k)).eq.0)then
            l=l+1
            cfill(l)=rowidx(k)
          endif
          mark(rowidx(k))=i
        enddo

        if(mark(i).eq.0)then
          l=l+1
          cfill(l)=i
        endif
        mark(i)=i

        pnt1=pntr(i)
        pnt2=pnt1+crow(i)-1
        do k=pnt1,pnt2
          if(mark(colidx(k)).eq.0)then
            l=l+1
            cfill(l)=colidx(k)
          endif
          mark(colidx(k))=i
        enddo

        k=j+1
  152   if(k.le.ppnt2)then
          o=rowidx(k)
          call lpchknod(m,cfree,rfree,i,o,p,ccol,crow,mark,pntc,
     x    pntr,rowidx,colidx,supdeg,suplst,ii)
          if(ii.gt.0)then
            supnd=supnd+1

            prewcol=cpermb(o)
            oo=cpermf(o)
            if(prewcol.ne.0)then
              cpermf(prewcol)=oo
            else
              cfirst=oo
            endif
            if(oo.eq.0)then
              clast=prewcol
            else
              cpermb(oo)=prewcol
            endif

            prewcol=rpermb(o)
            oo=rpermf(o)
            if(prewcol.ne.0)then
              rpermf(prewcol)=oo
            else
              rfirst=oo
            endif
            if(oo.eq.0)then
              rlast=prewcol
            else
              rpermb(oo)=prewcol
            endif

            rowidx(k)=rowidx(ppnt2)
            k=k-1
            ppnt2=ppnt2-1
            ccol(pcol)=ccol(pcol)-1
          endif
          k=k+1
          goto 152
        endif
        j=j+1
        goto 151
      endif
      do i=1,l
        mark(cfill(i))=0
      enddo

c update the counter lists

      do j=ppnt1,ppnt2
        i=rowidx(j)
        fill=noddeg(i)-supdeg(i)+2
        o=cpnt(fill)
        cnext(i)=o
        cpnt(fill)=i
        if(o.ne.0)cprew(o)=i
        cprew(i)=0
      enddo

c augment the permutation with the supernodes

      i=suplst(pcol)
 155  if(i.gt.0)then
        pivotn=pivotn+1
        permut(pivotn)=i
        i=suplst(i)
        goto 155
      endif
      goto 50

c augment the permutation with the disabled rows

 900  do i=1,m
        if(crow(i).le.-2)then
          pivotn=pivotn+1
          permut(pivotn)=i
        endif
      enddo
      write(buff,4)supnd
      call lpmprnt(buff)
      write(buff,5)hsupnd
      call lpmprnt(buff)

c ready

 999  return
      end

c ===========================================================================

      subroutine lpmccmpr(mn,cfree,ccfree,endmem,nz,
     x pnt,count,cfirst,cpermf,rowidx,code)
      integer*4 mn,cfree,ccfree,endmem,nz,pnt(mn),rowidx(cfree),
     x count(mn),cpermf(mn),cfirst,code

      integer*4 i,j,pnt1,pnt2,pnt0
      character*99 buff
c ---------------------------------------------------------------------------
   2  format(' not enough memory detected in subroutine ccompress')
      pnt0=nz+1
      i=cfirst
  40  if(i.le.0)goto 30
        pnt1=pnt(i)
        if(pnt1.lt.pnt0)goto 10
        if(pnt1.eq.pnt0)then
          pnt0=pnt0+count(i)
          goto 10
        endif
        pnt(i)=pnt0
        pnt2=pnt1+count(i)-1
        do 20 j=pnt1,pnt2
          rowidx(pnt0)=rowidx(j)
          pnt0=pnt0+1
  20    continue
  10    i=cpermf(i)
      goto 40
  30  ccfree=endmem-pnt0-1
      if(ccfree.lt.mn)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
      endif
      return
      end

c ===========================================================================

      subroutine lpchknod(m,cfree,rfree,i,o,p,ccol,crow,mark,pntc,
     x pntr,rowidx,colidx,supdeg,suplst,fnd)

      integer*4 m,cfree,rfree,i,o,p,ccol(m),crow(m),mark(m),pntc(m),
     x pntr(m),rowidx(cfree),colidx(rfree),supdeg(m),suplst(m),fnd

      integer*4 ppnt1,ppnt2,k,l,pnt,ii,pnod

      fnd=0
      if(ccol(o)+crow(o).ne.p)goto 120
      ppnt1=pntr(o)
      ppnt2=ppnt1+crow(o)-1
 111  if(ppnt1.le.ppnt2)then
        if(mark(colidx(ppnt1)).ne.i)goto 119
          ppnt1=ppnt1+1
          goto 111
        endif
      ppnt1=pntc(o)
      ppnt2=ppnt1+ccol(o)-1
 112  if(ppnt1.le.ppnt2)then
        if(mark(rowidx(ppnt1)).ne.i)goto 119
        ppnt1=ppnt1+1
        goto 112
      endif

c include column o (and its list) in to the list of column i

      pnod=o
 211  if(suplst(pnod).ne.0)then
        pnod=suplst(pnod)
        goto 211
      endif
      suplst(pnod)=suplst(i)
      suplst(i)=o
      supdeg(i)=supdeg(i)+supdeg(o)

c remove column/row o from the row and column files

      ppnt1=pntr(o)
      ppnt2=ppnt1+crow(o)-1
      do 124 k=ppnt1,ppnt2
        l=colidx(k)
        pnt=pntc(l)
        ii=pnt+ccol(l)-1
        ccol(l)=ccol(l)-1
 123    if(pnt.le.ii)then
          if(rowidx(pnt).eq.o)then
            rowidx(pnt)=rowidx(ii)
            goto 124
          endif
          pnt=pnt+1
          goto 123
        endif
 124  continue
      ppnt1=pntc(o)
      ppnt2=ppnt1+ccol(o)-1
      do 127 k=ppnt1,ppnt2
        l=rowidx(k)
        pnt=pntr(l)
        ii=pnt+crow(l)-1
        crow(l)=crow(l)-1
 126    if(pnt.le.ii)then
          if(colidx(pnt).eq.o)then
            colidx(pnt)=colidx(ii)
            goto 127
          endif
          pnt=pnt+1
          goto 126
        endif
 127  continue
      crow(o)=-1
      p=p-1
      fnd=1
      goto 120
 119  fnd=0
 120  return
      end

c ===========================================================================

      subroutine lphpsort(n,iarr)

      integer*4 n,iarr(n)

      integer*4 i,j,l,ir,rra

c ---------------------------------------------------------------------------

      l=n/2+1
      ir=n
  10  if(l.gt.1)then
         l=l-1
         rra=iarr(l)
      else
        rra=iarr(ir)
        iarr(ir)=iarr(1)
        ir=ir-1
        if(ir.le.1)then
          iarr(1)=rra
          goto 999
        endif
      endif
      i=l
      j=l+l
  20  if(j-ir)40,50,60
  40  if(iarr(j).lt.iarr(j+1))j=j+1
  50  if(rra.lt.iarr(j))then
        iarr(i)=iarr(j)
        i=j
        j=j+j
      else
        j=ir+1
      endif
      goto 20
  60  iarr(i)=rra
      goto 10
 999  return
      end

c ===========================================================================
c ===========================================================================

      subroutine lpfinput(intmap,relmap,realmem,intmem,fin,fout,intmax,
     x relmax,addobj,outlev,bigbou,big,nn,sfile,minmax,objnam,filname,
     * rhsin)

      character*80 filname
      real*8 rhsin(*)

      integer*4      n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8          tplus,tzer
      common/lpnumer/ tplus,tzer

      integer*4        loglog,lfile
      common/lplogprt/ loglog,lfile

      integer*4 intmax,relmax,outlev,intmap(25),relmap(25),nn,sfile
      logical fileok
      real*8 realmem(relmax),addobj
      integer*4 intmem(intmax)
      integer*4 fin,fout
c ---------------------------------------------------------------------------
      integer*4    minmax,maxmn,t1,t2,compr(25),iok,rdloop
      character    blank
c     character    ch
      character    namarr(40),filarr(40)
      character*4  chmps,chout,chlog
      character*40 namstr,blnk40
      character*40 filstr,instr,outstr
      character*12 defbp
      character*8  objnam,rhsnam,bndnam,rngnam
      character*20 blank20
      equivalence  (namarr(1),namstr)
      equivalence  (filarr(1),filstr)
c     logical      havch
c     integer*4    k,namlen
      integer*4    i,j,iosout,inslck
      real*8       big,bigbou
      character*99 buff

      data blank   /' '/
      data chmps   /'.mps'/
      data chout   /'.out'/
      data chlog   /'.log'/
      data defbp   /'bpmpd.par   '/
      data blnk40  /'                                        '/
c ---------------------------------------------------------------------------
      maxmn=relmax/50
      if(maxmn.lt.1000)maxmn=1000
      bigbou=1.0d+15
   45 format (1x,'allocation error ! ',' new m+n limit is :',i8)
   46 format (1x,'space in the    row file  :',i8)
   47 format (1x,'space in the column file  :',i8)
   48 format (1x,'not enough real memory, maxmn reduced...')
   49 format (1x,'not enough integer memory, maxmn reduced...')
   50 format (1x,'maxmn is to low, increase realmaxx !')

c parameterfile name

      open(fout,file='lp.out',status='unknown', iostat=iosout, err=99)
      open(lfile,file='lp.log',status='unknown', iostat=iosout, err=99)
      namstr=blnk40
      outstr(1:12)=defbp(1:12)
      inslck=0
c      minmax=1
      blank20='                    '
      objnam(1:8)=blank20(1:8)
      rhsnam(1:8)=blank20(1:8)
      bndnam(1:8)=blank20(1:8)
      rngnam(1:8)=blank20(1:8)
c      call lpreadpar(outstr,outlev,minmax,maxmn,namstr,
c     x objnam,rhsnam,bndnam,rngnam,bigbou,inslck)

c   1  if (namstr.eq.blnk40)then
c        write(*,2)
c   2    format(1x,'enter name of input file [*.mps] : ')
c        read(*,'(a)')namstr
c      endif
c      if(namstr(36:40).ne.'    ')then
c        write(*,'(1x,a24,a40)')'filename is too long !  ',namstr
c        namlen=0
c        goto 20
c      endif
c      havch=.false.
c      namlen=0
c      filstr=blnk40
c      do 10 k=1,36
c      ch=namarr(k)
c      if(havch)then
c        if(ch. ne. blank)then
c          namlen=namlen+1
c          filarr(namlen)=ch
c        else
c          go to 20
c        end if
c      else if(ch. ne. blank)then
c        havch=.true.
c        namlen=1
c        filarr(namlen)=ch
c      end if
c   10 continue
c   20 if(namlen.eq.0)then
c        write(*,30)
c   30   format(1x,'error in entering input file name.')
c        namstr=blnk40
c        goto 1
c      endif

c input file name.

c      instr=filstr
c      instr(namlen+1:namlen+4)=chmps(1:4)
c      inquire(file=instr,exist=fileok)
      inquire(file=filname,exist=fileok)
      if(fileok)then
      else
c        if(namlen.eq.0)goto 20
        write(buff,'(1x,a,a)')'file does not exists:',instr
        call lpmprnt(buff)
c       write(*,'(1x,a,a)')'file does not exists:',instr
        stop
      endif

c output and log file name.

c      outstr=filstr
c      outstr(namlen+1:namlen+4)=chout(1:4)
c      open(fout,file='lp.out',status='unknown', iostat=iosout, err=99)
c      if((loglog.eq.2).or.(loglog.eq.3))then
c        outstr(namlen+1:namlen+4)=chlog(1:4)
c       open(lfile,file='lp.log',status='unknown', iostat=iosout, err=99)
c      endif
c      write(buff,'(1x,a24,a40)')'mps file name [*.mps] : ',namstr
c      call lpmprnt(buff)

      call lptimer(t1)
      rdloop=0
 11   mn=maxmn+1

c set real memory maps

      relmap( 1)=1
      relmap( 2)=relmap( 1)+mn
      relmap( 3)=relmap( 2)+mn
      relmap( 4)=relmap( 3)+mn
      relmap( 5)=relmap( 4)+mn
      relmap( 6)=relmap( 5)+mn
      relmap( 7)=relmap( 6)+mn
      relmap( 8)=relmap( 7)+mn
      relmap( 9)=relmap( 8)+mn
      relmap(10)=relmap( 9)+mn
      relmap(11)=relmap(10)+mn
      relmap(12)=relmap(11)+mn
      relmap(13)=relmap(12)+mn
      relmap(14)=relmap(13)+mn
      relmap(15)=relmap(14)+mn
      relmap(16)=relmap(15)+mn
      relmap(17)=relmap(16)+mn
      relmap(18)=relmap(17)+mn
      relmap(19)=relmap(18)+mn
      relmap(20)=relmap(19)+mn
      relmap(21)=relmap(20)+mn
      relmap(22)=relmap(21)+mn
      relmap(23)=relmap(22)+mn
      relmap(24)=relmap(23)+mn

      if(relmap(24)+mn.ge.relmax)then
        write(buff,48)
        call lpmprnt(buff)
        maxmn=maxmn/2
        if(maxmn.lt.100)then
          write(buff,50)
          call lpmprnt(buff)
          stop
        endif
        goto 11
      endif

c set integer maps

      intmap( 1)=1
      intmap( 2)=intmap( 1)+mn
      intmap( 3)=intmap( 2)+mn+1
      intmap( 4)=intmap( 3)+mn
      intmap( 5)=intmap( 4)+mn
      intmap( 6)=intmap( 5)+mn
      intmap( 7)=intmap( 6)+mn
      intmap( 8)=intmap( 7)+mn
      intmap( 9)=intmap( 8)+mn
      intmap(10)=intmap( 9)+mn
      intmap(11)=intmap(10)+mn
      intmap(12)=intmap(11)+mn
      intmap(13)=intmap(12)+mn

      if(intmap(13)+mn.ge.intmax)then
        write(buff,49)
        call lpmprnt(buff)
        maxmn=maxmn/2
        if(maxmn.lt.100)then
          write(buff,50)
          call lpmprnt(buff)
          stop
        endif
        goto 11
      endif
      i=intmax-intmap(13)-1
      j=relmax-relmap(24)-1
      if(j.lt.i)i=j

      rdloop=rdloop+1
      if(rdloop.ge.3)then
        write(buff,'(a)')'too few memory, cannot continue'
        call lpmprnt(buff)
        stop
      endif

      call lpmpsinput(instr,fin,objnam,rhsnam,bndnam,rngnam,
     x                m,n,nz,addobj,minmax,
     x                intmem(intmap(13)),
     x                realmem(relmap(24)),
     x                intmem(relmap(8)),  !WIP Changed from real
     x                intmem(relmap(9)),  !WIP Changed from real
     x                realmem(relmap(2)),
     x                realmem(relmap(10)),
     x                realmem(relmap(3)),
     x                realmem(relmap(4)),
     x                intmem(intmap(4)),
     x                intmem(intmap(5)),
     x                intmem(intmap(12)),
     x                intmem(intmap(3)),
     x                big,maxmn,maxmn,i,
     x                realmem(relmap(1)),
     x                intmem(intmap(8)),
     x                intmem(intmap(9)),
     x                intmem(intmap(10)),
     x                iok,tzer,bigbou,inslck,nn,
     x                sfile,filname,rhsin)
!     call lpmpsinput(instr,fin,objnam,rhsnam,bndnam,rngnam,
!    x m,n,nz,addobj,minmax,
!    x intmem(intmap(13)),realmem(relmap(24)),realmem(relmap(8)),
!    x realmem(relmap(9)),realmem(relmap(2)),realmem(relmap(10)),
!    x realmem(relmap(3)),realmem(relmap(4)),intmem(intmap(4)),
!    x intmem(intmap(5)),intmem(intmap(12)),intmem(intmap(3)),
!    x big,maxmn,maxmn,i,realmem(relmap(1)),intmem(intmap(8)),
!    x intmem(intmap(9)),intmem(intmap(10)),iok,tzer,bigbou,inslck,nn,
!    x sfile,filname,rhsin)



      if(m+n.ge.mn)then
        maxmn=m+n
        write(buff,45)maxmn
        call lpmprnt(buff)
        goto 11
      endif
      if(iok.eq.3)then
        maxmn=m+n
        if(mn.lt.(maxmn+100))then
          write(buff,'(a)')' too few memory, cannot continue'
          call lpmprnt(buff)
          stop
        else
          write(buff,'(a)')
     x    ' try to reload the problem with new memory configuration'
          call lpmprnt(buff)
          goto 11
        endif
      endif

c convert to the standard form

      mn=m+n
      n1=n+1
      call lpconvert(realmem(relmap(10)),realmem(relmap(3)),
     x realmem(relmap(4)),intmem(intmap(4)),big)

c compress integer memory

       compr(1)=n
       compr(2)=m
       compr(3)=n1
       do i=4,12
         compr(i)=mn
       enddo
       compr(13)=nz
       j=1
       do i=1,13
         if((i.eq.3).or.(i.eq.13))then
           call lpmovint(compr(i),intmem(j),intmem(intmap(i)))
         endif
         intmap(i)=j
         j=j+compr(i)
       enddo

c compress real memory

       compr(1)=n
       compr(2)=m
       do i=3,23
         compr(i)=mn
       enddo
       compr(24)=nz
       compr(17)=m
       compr(18)=m
       compr(19)=m
       compr(20)=m
       j=1
       do i=1,24
         if((i.le.5).or.(i.eq.24))then
           call lpmovdble(compr(i),realmem(j),realmem(relmap(i)))
         endif
         relmap(i)=j
         j=j+compr(i)
       enddo

c timer

      call lptimer(t2)
      if(iok.eq.0)then
        write(buff,'(1x,a,f12.2,a)')
     x   'mps file is read in  ',dble(t2-t1)*0.01d+0,' sec.'
        call lpmprnt(buff)
      endif

c set maps, set up cfree

      nz=nz+1
      i=intmax-intmap(13)-nz
      j=relmax-relmap(24)-nz
      cfree=j+nz
      rfree=i-j
      intmap(14)=intmap(13)+cfree
      write(buff,46)rfree
      call lpmprnt(buff)
      write(buff,47)(cfree-nz)
      call lpmprnt(buff)
      if(rfree.lt.nz)then
        write(buff,49)
        call lpmprnt(buff)
        stop
      endif
      return

c file error

   99 write(buff,1040)iosout
      call lpmprnt(buff)
      write(buff,1020)outstr
      call lpmprnt(buff)
 1020 format(1x,' unable to open file :  ',a40)
 1040 format(1x,' output file status  : ',i6)
      stop
      end

c ===========================================================================

      subroutine lpmovint(n,array1,array2)
      integer*4  n,array1(n),array2(n)
      integer*4  i
      do 10 i=1,n
        array1(i)=array2(i)
  10  continue
      return
      end

c ===========================================================================

      subroutine lpmovdble(n,array1,array2)
      integer*4  n
      real*8     array1(n),array2(n)
      integer*4  i
      do 10 i=1,n
        array1(i)=array2(i)
  10  continue
      return
      end

c ===========================================================================
      subroutine lpmprnt(buff)
      character*99 buff
      common/lplogprt/ loglog,lfile
      integer*4      loglog,lfile

    1 format(a79)
      if((loglog.eq.1).or.(loglog.eq.3))then
c        write(*,1)buff
        write(lfile,1)buff
      endif
      if((loglog.eq.2).or.(loglog.eq.3))then
        write(lfile,1)buff
      endif

      return
      end
c ==========================================================================

c           mps input formatum feldolgozasa  character*8 tombok nelkul
c         --------------------------------------------------------------
c                       egyszeru  hashing technikaval


c                       comment ( '*' ) figyelessel
c c sor,oszlop,ranges,rhs es bounds nevek elso karaktere nem lehet '*' (mert
c   ez esetben megjegyzesnek szamit), egyebkent mashol szerepelhet bennuk.
c     oszlop es sornevkent az ures string ('        ') nem szerepelhet,
c  rhs,bounds,ranges nevkent pedig csak ugy, ha ez az egyetlen ilyen nev.
c     az oszlopnevek rendezve vannak, binaris kereses a bounds-ban.
c  az oszlopnevek ismetlodesenek es a numerikus formatum helyessegenek a
c                          vizsgalata beepitve.
c         az mps file-ban levo nulla elemek nem kerulnek tarolasra !

c utolso modositas : 1994 januar 31.
c ==========================================================================


      subroutine lpmpsinput(filnam,fin,objname,rhsname,bouname,ranname,
     x                      m,n,nz,addobj,minmax,
     x                      rowidx,     ! I
     x                      nonzeros,   ! R
     x                      rownam,     ! I
     x                      colnam,     ! I
     x                      rhs,        ! R
     x                      range,      ! R
     x                      lbound,     ! R
     x                      ubound,     ! R
     x                      rowtyp,     ! I
     x                      index1,     ! I
     x                      index2,     ! I
     x                      colpnt,     ! I
     x                      big,maxm,maxn,maxnz,
     x                      obj,        ! R
     x                      colind,     ! I
     x                      rhdr,       ! I
     x                      chdr,       ! I
     X                      iok,tzer,bigbou,inslck,nn,
     x                      sfile,filname,rhsin)

      integer*4    m,n,nz,maxm,maxn,maxnz,rowidx(maxnz),rowtyp(maxm),
     x             index1(maxm),colpnt(maxn+1),colind(maxn),
     x             index2(maxn),fin,minmax,rhdr(maxm),chdr(maxn),iok,
     x             inslck,nn,sfile
      real*8       nonzeros(maxnz),rhs(maxm),range(maxm),lbound(maxn),
     x             ubound(maxn),obj(maxn),big,addobj,tzer,bigbou
      character*8  objname,rhsname,bouname,ranname
      integer*4    rownam(2*maxm),colnam(2*maxn)

      character*8  name0,name1,name2,c1,oldname
      character*3  sense
      character*4  sen4
      integer*4    i,pnt,ix,in,lcol,icol,hsval,ni,mi,freen
      real*8       val1,val2
      character*40 filnam
      character*99 buff
      integer*4 wignore,w1,w2,w3,w4,w5,w6,w7,w8,w9,w0
      character*80 filname
      real*8 rhsin(*)

c --------------------------------------------------------------------------

    2 format(1x,'wrong bounds for the variable ',a8)
    3 format(1x,'wrong filename : ',a40)
    5 format(1x,'column hashing       : ',i8,'   row    hashing :',i8)
    9 format(1x,'rhs       name       : ',a8,'   rhs    records :',i8)
   10 format(1x,'ranges    name       : ',a8,'   ranges records :',i8)
   11 format(1x,'bounds    name       : ',a8,'   bounds records :',i8)
   12 format(1x,a22,i8)
   13 format(1x,a22,i8,'    column name    : ',a8)

      w1=0
      w2=0
      w3=0
      w4=0
      w5=0
      w6=0
      w7=0
      w8=0
      w9=0
      w0=0
      wignore=5
      freen=0
      iok=0
c      open(fin,file=filnam,status='old',err=15)
      open(fin,file=filname,status='old',err=15)
      goto 20
  15  write(buff,3)filnam
      call lpmprnt(buff)
      stop
  20  do 1 i=1,maxm
        index1(i)=0
        rhdr(i)=0
   1  continue
      do 4 i=1,maxn
        index2(i)=0
        chdr(i)=0
   4  continue
      m=0
      n=0
      mi=0
      ni=0
      addobj=0.0d+0
      write(buff,'(1x)')
      call lpmprnt(buff)
      pnt=1
      call lpfreadnam(fin)
      call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
      if(sense.ne.'row')then
        write(buff,'(1x,a)')'fatal : rows not found'
        call lpmprnt(buff)
        stop
      endif

c default value for the objective function

      do 22 i=1,maxn
        obj(i)=0.0d+0
  22  continue

c rows section

      ix=0
  25  call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
      if(sense.ne.'col')then
        if(((sense.eq.' n ').or.(sense.eq.'  n')).and.(ix.eq.0))then
c          if(objname.eq.'        ')objname=name0
          objname=name0
          if(objname.eq.name0)then
            ix=1
            goto 25
          endif
        endif
        m=m+1
        colind(m)=0
        if(m.gt.maxm)then
          write(buff,'(1x,a,i8)')'to many rows. current maximum :',maxm
          call lpmprnt(buff)
          iok=1
          goto 925
        endif
        call lpctob(rownam(m),rownam(m+maxm),name0,maxm,hsval)
        if(rhdr(hsval).eq.0)mi=mi+1
        index1(m)=rhdr(hsval)
        rhdr(hsval)=m
        if(sense.eq.' eq')sense=' e '
        if((sense.eq.' n ').or.(sense.eq.'  n'))then
          rowtyp(m)=0
        else if ((sense.eq.' e ').or.(sense.eq.'  e'))then
          rowtyp(m)=1
        else if ((sense.eq.' g ').or.(sense.eq.'  g'))then
          rowtyp(m)=2
        else if ((sense.eq.' l ').or.(sense.eq.'  l'))then
          rowtyp(m)=3
        else
          w1=w1+1
          if(w1.le.wignore)then
            write(buff,'(1x,a,a,a)')'wrong rowtype : ',sense,
     x      '  replaced with   g'
            call lpmprnt(buff)
          endif
          rowtyp(m)=2
        endif
        goto 25
      endif
      if(ix.le.0)then
        write(buff,'(1x,a)')'no objective function !'
        call lpmprnt(buff)
        write(buff,'(1x,a)')'please check the rowtypes !'
        call lpmprnt(buff)
        stop
      endif
      w1=w1-wignore
      if(w1.gt.0)then
        write(buff,'(1x,a,i8)')'rowtype warnings ignored:',w1
        call lpmprnt(buff)
      endif
      write(buff,'(1x,a,a)')'objective function   :',objname
      call lpmprnt(buff)

c check for duplicated rownames

c      ix=0
c      do i=2,m
c        call lpbtoc(rownam(8*i-7),c1)
c        call lpbtoc(rownam(8*(i-1)-7),c2)
c        if(c1.le.c2)then
c          print*,'wrong rownames:  ',c1,'  index:',index1(i-1)
c          print*,'                 ',c2,'  index:',index1(i)
c          ix=1
c        endif
c      enddo
c      if(ix.ne.0)stop

c columns section

      oldname='        '
  30  call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
      if(sense.ne.'rhs')then
        if(oldname.ne.name0)then
          n=n+1
          if(n.gt.maxn)then
            write(buff,'(1x,a,i8)')'to many columns. current maximum :',
     x      maxn
            call lpmprnt(buff)
            iok=2
            goto 935
          endif
          call lpctob(colnam(n),colnam(n+maxn),name0,maxn,hsval)
          if(chdr(hsval).eq.0)ni=ni+1
          index2(n)=chdr(hsval)
          chdr(hsval)=n
          colpnt(n)=pnt
          oldname=name0
          if(n.gt.1)then
            do 32 i=colpnt(n-1),pnt-1
              colind(rowidx(i))=0
  32        continue
          endif
        endif
        if((name1.ne.'        ').and.(abs(val1).gt.tzer))then
          if(name1.eq.objname)then
            if(abs(obj(n)).gt.tzer)then
              w3=w3+1
              if(w3.le.wignore)then
                write(buff,'(1x,a,a,a,a)')'warning : row ',name1,
     x          ' is duplicated in column ',name0
                call lpmprnt(buff)
              endif
            else
              obj(n)=val1*dble(minmax)
            endif
          else
            call lpfndnam(rhdr,index1,name1,rownam,maxm,ix)
            if(ix.gt.0)then
              if(pnt.gt.maxnz)then
                write(buff,'(1x,a,i8)')
     x          'to many nonzeros. current maximum :',maxnz
                call lpmprnt(buff)
                iok=3
                goto 935
              endif
              if(colind(ix).ne.0)then
                w2=w2+1
                if(w2.le.wignore)then
                  write(buff,'(1x,a,a,a,a)')'warning : row ',name1,
     x            ' is duplicated in column ',name0
                  call lpmprnt(buff)
                endif
              else
                nonzeros(pnt)=val1
                rowidx(pnt)=ix
                colind(ix)=1
                pnt=pnt+1
              endif
            else
              w2=w2+1
              if(w2.le.wignore)then
                write(buff,'(1x,a,a,a)')
     x          'warning : wrong rowname : ',name1,' ignored'
                call lpmprnt(buff)
              endif
            endif
          endif
        endif
        if((name2.ne.'        ').and.(abs(val2).gt.tzer))then
          if(name2.eq.objname)then
            if(abs(obj(n)).gt.tzer)then
              w3=w3+1
              if(w3.le.wignore)then
                write(buff,'(1x,a,a,a,a)')'warning : row ',name2,
     x          ' is duplicated in column ',name0
                call lpmprnt(buff)
              endif
            else
              obj(n)=val2*dble(minmax)
            endif
          else
            call lpfndnam(rhdr,index1,name2,rownam,maxm,ix)
            if(ix.gt.0)then
              if(pnt.gt.maxnz)then
                write(buff,'(1x,a,i8)')
     x          'to many nonzeros. current maximum :',maxnz
                call lpmprnt(buff)
                iok=3
                goto 935
              endif
              if(colind(ix).ne.0)then
                w3=w3+1
                if(w3.le.wignore)then
                  write(buff,'(1x,a,a,a,a)')'warning : row ',name2,
     x            ' is duplicated in column ',name0
                  call lpmprnt(buff)
                 endif
              else
                nonzeros(pnt)=val2
                rowidx(pnt)=ix
                colind(ix)=1
                pnt=pnt+1
              endif
            else
              w2=w2+1
              if(w2.le.wignore)then
                write(buff,'(1x,a,a,a)')'warning : wrong rowname : ',
     x          name2,' ignored'
                call lpmprnt(buff)
              endif
            endif
          endif
        endif
        goto 30
      endif
      colpnt(n+1)=pnt
      w2=w2-wignore
      if(w2.gt.0)then
        write(buff,'(1x,a,i8)')'rowname warnings ignored:',w2
        call lpmprnt(buff)
      endif
      w3=w3-wignore
      if(w3.gt.0)then
        write(buff,'(1x,a,i8)')'duplicat row warnings ignored:',w3
        call lpmprnt(buff)
      endif

c check for duplicated colnames

c      ix=0
c      do 33 i=2,n
c        call lpbtoc(colnam(8*i-7),c1)
c        call lpbtoc(colnam(8*(i-1)-7),c2)
c        if(c1.le.c2)then
c          print*,'wrong colnames: ',c1,'  index:',index2(i-1)
c          print*,'                ',c2,'  index:',index2(i)
c          ix=1
c        endif
c  33  continue
c      if(ix.ne.0)stop

c default values for rhs, range, lo-hi bounds

      do 35 i=1,m
        if(rowtyp(i).eq.1)then
          range(i)=0.0d+0
        else
          range(i)=big
        endif
c        rhs(i)=0.0d+0
        rhs(i)=rhsin(i)
  35  continue
      do 40 i=1,n
        lbound(i)=0
        ubound(i)=big
  40  continue

c rhs section

      write(buff,5)ni,mi
      call lpmprnt(buff)
      if(rhsname.eq.'        ')then
        in=-1
      else
        in=0
      endif
cccccccc start to read right hand side ccccc
  45  call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
      if((sense.ne.'ran').and.(sense.ne.'bou').and.(sense.ne.'end'))then
        if(in.lt.0)then
          rhsname=name0
          in=0
        endif
        if(rhsname.eq.name0)then
          if(name1.ne.'        ')then
            if(name1.eq.objname)then
              addobj=val1
            else
              call lpfndnam(rhdr,index1,name1,rownam,maxm,ix)
              if(ix.gt.0)then
                in=in+1
c                rhs(ix)=val1
              else
                w4=w4+1
                if(w4.le.wignore)then
                  write(buff,'(1x,a,a,a)')
     x           'warning : wrong rowname in the rhs: ',name1,' ignored'
                  call lpmprnt(buff)
                endif
              endif
            endif
          endif
          if(name2.ne.'        ')then
            if(name2.eq.objname)then
              addobj=val2
            else
              call lpfndnam(rhdr,index1,name2,rownam,maxm,ix)
              if(ix.gt.0)then
                in=in+1
c                rhs(ix)=val2
              else
                w4=w4+1
                if(w4.le.wignore)then
                  write(buff,'(1x,a,a,a)')
     x            'warning : wrong rowname in the rhs:',name2,' ignored'
                  call lpmprnt(buff)
                endif
              endif
            endif
          endif
        endif
        goto 45
      endif
      w4=w4-wignore
      if(w4.gt.0)then
        write(buff,'(1x,a,i8)')'rowname warnings ignored:',w4
        call lpmprnt(buff)
      endif
      if(in.le.0)then
        write(buff,'(1x,a)')'warning : no rhs  value found'
        call lpmprnt(buff)
      else
        write(buff,9)rhsname,in
        call lpmprnt(buff)
      endif

c ranges

      if(sense.eq.'ran')then
        if(ranname.eq.'        ')then
          in=-1
        else
          in=0
        endif
  50    call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
        if((sense.ne.'bou').and.(sense.ne.'end'))then
          if(in.lt.0)then
            ranname=name0
            in=0
          endif
          if(ranname.eq.name0)then
            if(name1.ne.'        ')then
              call lpfndnam(rhdr,index1,name1,rownam,maxm,ix)
              if(ix.gt.0)then
                if(abs(val1).ge.bigbou)then
                  w9=w9+1
                  if(w9.le.wignore)then
                    write(buff,'(1x,a,1p,d12.5,a)')
     x              'warning :  range  ',val1,' exceed bigbound'
                    call lpmprnt(buff)
                  endif
                  val1=big
                endif
                in=in+1
                range(ix)=val1
              else
              w5=w5+1
              if(w5.le.wignore)then
                 write(buff,'(1x,a,a,a)')
     x           'warning : wrong rowname in the ranges :',name1,
     x           ' ignored'
                 call lpmprnt(buff)
               endif
              endif
            endif
            if(name2.ne.'        ')then
              call lpfndnam(rhdr,index1,name2,rownam,maxm,ix)
              if(ix.gt.0)then
                if(abs(val2).ge.bigbou)then
                  w9=w9+1
                  if(w9.le.wignore)then
                    write(buff,'(1x,a,1p,d12.5,a)')
     x              'warning :  range  ',val1,' exceed bigbound'
                    call lpmprnt(buff)
                  endif
                  val2=big
                endif
                in=in+1
                range(ix)=val2
              else
                write(buff,'(1x,a,a,a)')
     x          'wrong rowname in the ranges :',name2,' ignored'
                call lpmprnt(buff)
              endif
            endif
          endif
          goto 50
        endif
        w5=w5-wignore
        if(w5.gt.0)then
          write(buff,'(1x,a,i8)')'rowname warnings ignored:',w5
          call lpmprnt(buff)
        endif
        if(in.le.0)then
          write(buff,'(1x,a)')'warning : no range value found'
          call lpmprnt(buff)
        else
          write(buff,10)ranname,in
          call lpmprnt(buff)
        endif
      endif

c bounds

      if(sense.eq.'bou')then
        if(bouname.eq.'        ')then
          in=-1
        else
          in=0
        endif
  55    call lpfread(fin,sense,name0,name1,val1,name2,val2,w8,wignore)
        if(sense.ne.'end')then
          if(in.lt.0)then
            bouname=name0
            in=0
          endif
          if(bouname.eq.name0)then
            call lpfndnam(chdr,index2,name1,colnam,maxn,ix)
            if(ix.gt.0)then
              if(sense.eq.' bv')then
                w9=w9+1
                if(w9.le.wignore)then
                  write(buff,'(1x,a)')
     x            'warning :  bv  replaced with  up'
                  call lpmprnt(buff)
                endif
                sense=' up'
              endif
              if(sense.eq.' lo')then
                if(val1.le.-bigbou)then
                  w9=w9+1
                  if(w9.le.wignore)then
                    write(buff,'(1x,a,1p,d12.5,a)')
     x              'warning :  lower bound ',val1,' exceed bigbound'
                    call lpmprnt(buff)
                  endif
                  val1=-big
                endif
                lbound(ix)=val1
                in=in+1
              else if(sense.eq.' up')then
                if(val1.ge.bigbou)then
                  w6=w6+1
                  if(w6.le.wignore)then
                    write(buff,'(1x,a,1p,d12.5,a)')
     x               'warning :  upper bound ',val1,' exceed bigbound'
                    call lpmprnt(buff)
                  endif
                  val1=big
                endif
                ubound(ix)=val1
                in=in+1
              else if(sense.eq.' fx')then
                lbound(ix)=val1
                ubound(ix)=val1
                in=in+1
              else if(sense.eq.' mi')then
                lbound(ix)=-big
                ubound(ix)=0
                in=in+1
              else if(sense.eq.' fr')then
                freen=freen+1
                ubound(ix)=big
                lbound(ix)=-big
                in=in+1
              else if(sense.eq.' pl')then
                ubound(ix)=big
                in=in+1
              else
                w6=w6+1
                if(w6.le.wignore)then
                  write(buff,'(1x,a,a,a)')
     x            'warning : wrong boundtype :',sense,' ignored'
                  call lpmprnt(buff)
                endif
              endif
            else
             w7=w7+1
             if(w7.le.wignore)then
               write(buff,'(1x,a,a,a)')
     x         'warning : wrong colname in the bouns:',name1,' ignored'
               call lpmprnt(buff)
             endif
            endif
          endif
          goto 55
        endif
        w6=w6-wignore
        if(w6.gt.0)then
          write(buff,'(1x,a,i8)')'bound type warnings ignored:',w6
          call lpmprnt(buff)
        endif
        w7=w7-wignore
        if(w7.gt.0)then
          write(buff,'(1x,a,i8)')'column name warnings ignored:',w7
          call lpmprnt(buff)
        endif
        w9=w9-wignore
        if(w9.gt.0)then
          write(buff,'(1x,a,i8)')'bound limit warnings ignored:',w9
          call lpmprnt(buff)
        endif
        if(in.le.0)then
          write(buff,'(1x,a)')'warning : no bound value found'
          call lpmprnt(buff)
        else
          write(buff,11)bouname,in
          call lpmprnt(buff)
        endif
      endif
      w8=w8-wignore
      if(w8.gt.0)then
        write(buff,'(1x,a,i8)')'numeric format warnings ignored:',w8
        call lpmprnt(buff)
      endif

c check for range=0 and rhs > bigbound

      do 105 i=1,m
        if((rowtyp(i).gt.1).and.(abs(range(i)).le.tzer))rowtyp(i)=1
        if((rowtyp(i).gt.0).and.(rhs(i).ge.bigbou))then
          w0=w0+1
          if(w0.le.wignore)then
            call lpbtoc(rownam(i),rownam(i+maxm),name0)
            write(buff,'(1x,a,1p,d12.5,a,a)')
     x      'warning : too big rhs value :',rhs(i),' row ignored:',name0
            call lpmprnt(buff)
          endif
          rowtyp(i)=0
        endif
 105  continue
      w0=w0-wignore
      if(w0.gt.0)then
        write(buff,'(1x,a,i8)')'rhs limit warnings ignored:',w0
        call lpmprnt(buff)
      endif

c check for bounds consistance, select the longest column

      lcol=-1
      ix=0
      do 106 i=1,n
        if(colpnt(i+1)-colpnt(i).gt.lcol)then
          lcol=colpnt(i+1)-colpnt(i)
          icol=i
        endif
        if(ubound(i).lt.lbound(i))then
          call lpbtoc(colnam(i),colnam(i+maxn),c1)
          write(buff,2)c1
          call lpmprnt(buff)
          ix=1
        endif
 106  continue
      if(ix.gt.0)then
        stop
      endif

c endata, introduce slack variables if requested

      nn=n
      if (inslck.gt.0)then
        do i=1,m
          if(rowtyp(i).gt.1)then
            if(n.ge.maxn)then
              write(buff,'(1x,a,i8)')
     x        'to many columns. current maximum :',maxn
              call lpmprnt(buff)
              n=n+m-i+1
              iok=2
              goto 999
            endif
            n=n+1
            if(colpnt(n).ge.maxnz)then
              write(buff,'(1x,a,i8)')
     x        'to many nonzeros. current maximum :',maxnz
              call lpmprnt(buff)
              iok=3
              n=n+m-i+1
              goto 999
            endif
            colpnt(n+1)=colpnt(n)+1
            rowidx(colpnt(n))=i
            if(rowtyp(i).eq.2)then
              nonzeros(colpnt(n))=-1.0d+00
            else
              nonzeros(colpnt(n))=+1.0d+00
            endif
            lbound(n)=0.0d+00
            obj(n)=0.0d+00
            ubound(n)=range(i)
            range(i)=0.0d+00
            rowtyp(i)=1
          endif
        enddo
      endif
      nz=colpnt(n+1)-1

c  write statistics

      call lpbtoc(colnam(icol),colnam(icol+maxn),c1)
      write(buff,'(1x)')
      call lpmprnt(buff)
      write(buff,12)'number of rows       :',m
      call lpmprnt(buff)
      write(buff,12)'number of columns    :',n
      call lpmprnt(buff)
      write(buff,12)'free variables       :',freen
      call lpmprnt(buff)
      write(buff,12)'number of nonzeros   :',nz
      call lpmprnt(buff)
      write(buff,13)'length of long. col. :',lcol,c1
      call lpmprnt(buff)
      write(buff,'(1x)')
      call lpmprnt(buff)

c row and column names are written to a scratch file

      open(sfile,status='scratch',form='unformatted',err=955)
      write(sfile)(rownam(i),i=1,m)
      write(sfile)(rownam(i+maxm),i=1,m)
      write(sfile)(colnam(i),i=1,nn)
      write(sfile)(colnam(i+maxn),i=1,nn)
      goto 999

c check the number of the rows

 925  read(fin,'(a4,a8)',err=945)sen4,name0
      if(sen4.ne.'colu')then
        m=m+1
        goto 925
      endif
      name0='        '

c check the number of the columns

 935  read(fin,'(a4,a8)',err=945)sen4,name1
      if(sen4.ne.'    ')then
        if(inslck.gt.0)n=n+m
        goto 999
      endif
      if(name0.ne.name1)then
        n=n+1
        name0=name1
      endif
      goto 935

c file error

 955  write(buff,'(1x,a)')'file creation error. execution halted.'
      call lpmprnt(buff)
      stop

c wrong mps file

 945  write(buff,'(1x,a)')'error in reading the mps file.'
      call lpmprnt(buff)
      write(buff,'(1x,a,a)')'check the existence of the records :',
     x 'name, rows, columns, rhs, endata'
      call lpmprnt(buff)
      stop
 999  close(fin)

      end subroutine lpmpsinput

c ===========================================================================

      subroutine lpfreadnam(
     x fin)
      integer*4 fin

      integer*4    star
      character*80 st
      character    sta(80)
      equivalence  (st,sta(1))
      character*99 buff

c ---------------------------------------------------------------------------

  10  read(fin,'(a80)')st
      star=1
  15  if(sta(star).eq.'*')goto 20
      star=star+1
      if(star.le.80)goto 15
  20  if(star.lt.5)goto 10
      if(st(1:4).ne.'name')then
        write(buff,'(1x,a)')'fatal : name  record not found'
        call lpmprnt(buff)
        stop
      endif
      write(buff,'(1x,a,a)')'name :',st(8:(star-1))
      call lpmprnt(buff)

      end subroutine lpfreadnam

c ===========================================================================

      subroutine lpfread(
     x fin,sense,name0,name1,val1,name2,val2,w8,wignore)

      character*8 name0,name1,name2
      character*3 sense
      real*8      val1,val2
      integer*4   fin,w8,wignore

      integer*4    star
      character*64 st,blk
      character    sta(80)
      equivalence  (st,sta(1))
      character*99 buff

c --------------------------------------------------------------------------

      blk=
     x'                                                                '
  10  star=1
      st=blk
      read(fin,'(a64)',err=5)st
      goto 15
   5  write(buff,'(1x,a)')'error in reading the mps file.'
      call lpmprnt(buff)
      write(buff,'(1x,a,a)')'check the existence of the records :',
     x 'name, rows, columns, rhs, endata'
      call lpmprnt(buff)
      stop
  15  if(sta(star).eq.'*')goto 20
      star=star+1
      if(star.eq. 6)star=13
      if(star.eq.16)star=23
      if(star.eq.41)star=48
      if(star.le.64)goto 15
  20  if((star.lt.4).or.(st(1:(star-1)).eq.blk(1:(star-1))))goto 10
      sense(1:3)=st(1:3)
      if(star.lt.6)then
         name0='        '
         name1='        '
         name2='        '
         val1=0.0d+0
         val2=0.0d+0
         goto 99
      endif
      name0(1:8)=st(5:12)
      if(star.lt.16)then
         name1='        '
         name2='        '
         val1=0.0d+0
         val2=0.0d+0
         goto 99
      endif
      name1(1:8)=st(15:22)
      if(star.lt.37)then
         name2='        '
         val1=0.0d+0
         val2=0.0d+0
         goto 99
      endif
      read(st(25:36),'(1p,e12.5)',err=30)val1
      goto 25
  30  w8=w8+1
      if(w8.le.wignore)then
        write(buff,'(1x,a,a)')
     x  'warning : invalid numeric format :',st(25:36)
        call lpmprnt(buff)
      endif
      val1=0.0d+0
  25  if(star.lt.41)then
         name2='        '
         val2=0.0d+0
         goto 99
      endif
      name2(1:8)=st(40:47)
      if(star.lt.62)then
         val2=0.0d+0
         goto 99
      endif
      read(st(50:61),'(1p,e12.5)',err=40)val2
      goto 35
  40  w8=w8+1
      if(w8.le.wignore)then
        write(buff,'(1x,a,a)')
     x  'warning : invalid numeric format :',st(50:61)
        call lpmprnt(buff)
      endif
      val2=0.0d+0
  35  continue
  99  return
      end subroutine lpfread

c ===========================================================================

      subroutine lpfndnam(hdr,index,name,names,n,ix)
      integer*4 n,hdr(n),index(n),names(2*n),ix,a,b
      character*8 name
      call lpctob(a,b,name,n,ix)
      ix=hdr(ix)
  10  if(ix.ne.0)then
        if((names(ix).eq.a).and.(names(n+ix).eq.b))goto 99
        ix=index(ix)
        goto 10
      endif
  99  return
      end subroutine lpfndnam

c ===========================================================================

      subroutine lpbtoc(a,b,c)
      integer*4 a,b
      character*8 c
      integer*4  xx(2)
      character*8 xy
      equivalence (xx,xy)
      xx(1)=a
      xx(2)=b
      c=xy

      end subroutine lpbtoc

c ============================================================================

      subroutine lpctob(a,b,c,hbl,hsval)
      integer*4 a,b
      integer*4   hbl,hsval
      character*8 c
      integer*4  xx(2)
      character*8 xy
      equivalence (xx,xy)
      xy=c
      a=xx(1)
      b=xx(2)
      hsval=mod(abs(262144*ichar(c(1:1))+4096*ichar(c(2:2))+
     x 64*ichar(c(3:3))+ichar(c(4:4))+
     x 262144*ichar(c(5:5))+4096*ichar(c(6:6))+64*ichar(c(7:7))+
     x ichar(c(8:8))),hbl)+1

      end subroutine lpctob

c ============================================================================
c ============================================================================

      subroutine lpmpsout(m,n,nz,fout,sfile,code,opt,
     x rownam,colnam,range,lbound,ubound,rhs,
     x rowtyp,xs,colsta,rowsta,rowval,colpnt,rowidx,nonzeros,
     x colpre,rowpre,iter,corect,outlev,minmax,big,objnam,
     x tottim,factim,dv,dv2,dv3,costfunc,mtjh2)

      integer*4 m,n,nz,rowtyp(m),colsta(n),rowsta(m),colpnt(n+1),
     x rowidx(nz),rowpre(m),colpre(n),factim,tottim,
     x fout,code,iter,corect,outlev,sfile,rownam(m*2),colnam(n*2)
      real*8 rhs(m),range(m),lbound(n),ubound(n),opt,xs(n),big,
     x rowval(m),nonzeros(nz),costfunc(n)
      real*8 dv(m),dv2(m),dv3(m)

      character*8  objnam,c1
      integer*4    minmax
      character*6  str4
      integer*4    i,j,pnt1,pnt2,mtjh2
      real*8       sol,lbig

c --------------------------------------------------------------------------

   1  format(1x,'objective function : ',a8,'  value  : ',d19.12,
     x ' (maximize)')
   2  format(1x,'objective function : ',a8,'  value  : ',d19.12,
     x ' (minimize)')
   3  format(1x,'iterations    : ',i8,'  time    : ',f12.2,' sec')
   4  format(1x,'corrections   : ',i8,'  average : ',f12.2)
   7  format(1x,'total solution time    =',f12.2,' sec')
   8  format(1x)
   9  format(1x,'-------b-p-m-p-d---interior-point-lp-optimizer---',
     x 'version-2.12----------')
  10  format(1x,'---c-o-l-u-m-n-s----r-e-p-o-r-t---')
  11  format(1x,'  name          value       status')
  12  format(1x,'----------------------------------')
  14  format(1x,'------r-o-w-s----r-e-p-o-r-t------')


  18  format(1x,'execution stopped.')
  19  format(1x,'optimal solution found')
  20  format(1x,'problem is dual infeasibile (or badly scaled).')
  21  format(1x,'problem is primal infeasibile (or badly scaled).')
  22  format(1x,'termination code :',i3)
  23  format(1x,'general memory limit')

c read column and rownames

      rewind(sfile)
      read(sfile)(rownam(i),i=1,m)
      read(sfile)(rownam(i+m),i=1,m)
      read(sfile)(colnam(i),i=1,n)
      read(sfile)(colnam(i+n),i=1,n)
      close(sfile)
      lbig=0.9d+0*big

c write statistics

      write(fout,9)
      write(fout,8)
      write(fout,22)code
      if(code.eq.-2)then
        write(fout,23)
        goto 999
      endif
      if(code.le.1)then
         write(fout,18)
      else if(code.eq.2)then
         write(fout,19)
      else if(code.eq.3)then
         write(fout,20)
      else if(code.eq.4)then
         write(fout,21)
      endif

      if(minmax.eq.-1)then
        write(fout,1)objnam,-opt
      else
        write(fout,2)objnam,opt
      endif
      write(fout,8)
      write(fout,3)iter,(0.01d+0*dble(factim))
      if(iter.eq.0)iter=1
      write(fout,4)corect,(dble(corect)/dble(iter))
      write(fout,7)(0.01d+0*dble(tottim))
      if(outlev.eq.0)goto 99

c columns report

      write(mtjh2,'(1h%,13x7hmce id.,6x14hcoff. for cost,
     *                    9x11hprobability)')
      write(fout,8)
      write(fout,10)
      write(fout,11)
      write(fout,12)
      do 60 i=1,n
        str4='      '
        if(colsta(i).eq.-2)str4=' fixed'
        if(colsta(i).eq.-3)then
          if(colpre(i).eq.0)then
             str4='    fx'
          else
             str4='rowsng'
          endif
        endif
        if((ubound(i).gt.lbig).and.(lbound(i).lt.-lbig))str4='  free'
        if(colsta(i).eq.  1)str4=' basic'
        if(colsta(i).eq. -4)str4='colsng'
        if(colsta(i).eq. -5)str4='rowact'
        if(colsta(i).eq. -6)str4='chepdu'
        if(colsta(i).eq. -7)str4='duchek'
        if(colsta(i).eq. -8)str4='bndchk'
        if(colsta(i).lt.-12)str4='coldbl'
        if(colsta(i).eq.-10)str4='aggreg'
        call lpbtoc(colnam(i),colnam(i+n),c1)
        write(fout,'(1x,a8,1x,d18.11,1x,a6)')c1,xs(i),str4
        write(mtjh2,'(1h ,i20, 1p2e20.10)')
     *      i,costfunc(i),xs(i)
  60  continue
      write(mtjh2,'(1h ,''  '')')

c calculating row values

      do i=1,m
        rowval(i)=0.0d+0
      enddo
      do i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        sol=xs(i)
        do j=pnt1,pnt2
          rowval(rowidx(j))=rowval(rowidx(j))+nonzeros(j)*sol
        enddo
      enddo

c rows report

      write(fout,8)
      write(fout,14)
      write(fout,11)
      write(fout,12)
      do 90 i=1,m
        str4='      '
        if(rowsta(i).eq.-2)str4='  free'
        if(rowsta(i).eq.-3)then
          if(rowpre(i).eq.0)then
             str4='     n'
          else
             str4='rowsng'
          endif
        endif
        if(rowsta(i).eq.  1)str4=' basic'
        if(rowsta(i).eq. -4)str4='colsng'
        if(rowsta(i).eq. -5)str4='rowact'
        if(rowsta(i).eq. -6)str4='chepdu'
        if(rowsta(i).eq. -7)str4='duchek'
        if(rowsta(i).eq. -8)str4='bndchk'
        if(rowsta(i).lt.-12)str4='coldbl'
        if(rowsta(i).eq.-10)str4='aggreg'
        call lpbtoc(rownam(i),rownam(i+m),c1)
        write(fout,'(1x,a8,1x,d18.11,1x,a6)')c1,rowval(i),str4
  90  continue

      do 91 i=1,m
        write(fout,'(1x,'' dual'',i10,1p3e15.5)') i,dv(i),dv2(i),dv3(i)
  91  continue

      goto 99

  99  write(fout,12)
 999  return
      end subroutine lpmpsout

c ===========================================================================
c supernodal left looking, primer supernode loop (cache),
c supernode update with indirect addressing
c relative pivot tolerance
c =============================================================================

      subroutine lpnfactor(ecolpnt,
     x vcstat,rowidx,pivots,count,
     x nonzeros,diag,err,updat,mut,index,dropn,slktyp,
     x snhead,fpnt,invperm,nodtyp,dv,odiag)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 err,mut(mn),dropn,ecolpnt(mn),vcstat(mn),
     x rowidx(cfree),pivots(mn),count(mn),index(mn),slktyp(m)
      integer*4 snhead(mn),fpnt(mn),invperm(mn),nodtyp(mn)
      real*8 nonzeros(cfree),diag(mn),updat(mn),dv(m),odiag(mn)

      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c -----------------------------------------------------------------------------
      integer*4 i,j,k,o,p,pnt1,pnt2,ppnt1,ppnt2,col,kprew,
     x prewnode,ppnode,rb,w1
      real*8 s,diap,diam
      character*99 buff
c------------------------------------------------------------------------------
      err=0
      w1=0

c  initialization

      do 10 i=1,mn
        mut(i)=0
        index(i)=0
        updat(i)=0.0
        fpnt(i)=ecolpnt(i)
  10  continue
      ppnode=0
      prewnode=0
      i=0

c  loop for pivot columns

 100  i=i+1
      if(i.gt.pivotn)goto 60
      col=pivots(i)

c  step vcstat if relaxed

      if(vcstat(col).le.-2)then
        call lpcolremv(i,col,mut,index,fpnt,count,pivots,invperm,
     x  snhead,nodtyp,rowidx,nonzeros,ppnode,prewnode)
        diag(col)=0.0
        i=i-1
        if((ppnode.gt.0).and.(prewnode.eq.i))goto 110
        goto 100
      endif

      ppnt1=ecolpnt(col)
      ppnt2=count(col)
      if(ppnt1.le.nz)then
        diag(col)=1.0d00/diag(col)
        goto 180
      endif
      kprew=index(col)

c  compute

      diap=diag(col)
      diam=0.0d+0
 130  if(kprew)129,150,131

c standard transformation

 131  k=mut(kprew)
      pnt1=fpnt(kprew)
      pnt2=count(kprew)
      if(pnt1.lt.pnt2)then
        o=rowidx(pnt1+1)
        mut(kprew)=index(o)
        index(o)=kprew
      endif
      pnt1=pnt1+1
      fpnt(kprew)=pnt1
      s=-nonzeros(pnt1-1)*diag(kprew)
      if(kprew.le.n)then
        diap=diap+s*nonzeros(pnt1-1)
      else
        diam=diam+s*nonzeros(pnt1-1)
      endif
      do 170 o=pnt1,pnt2
        updat(rowidx(o))=updat(rowidx(o))+s*nonzeros(o)
 170  continue
      kprew=k
      goto 130

c supernodal transformation

 129  kprew=-kprew
      k=mut(kprew)
      p=invperm(kprew)
      pnt1=fpnt(kprew)+1
      if(pnt1.le.count(kprew))then
        o=rowidx(pnt1)
        mut(kprew)=index(o)
        index(o)=-kprew
      endif
      if(kprew.le.n)then
        call lpcspnd(p,snhead(p),diag,nonzeros,
     x  fpnt,count,pivots,updat,diap,rowidx(pnt1))
      else
        call lpcspnd(p,snhead(p),diag,nonzeros,
     x  fpnt,count,pivots,updat,diam,rowidx(pnt1))
      endif
      kprew=k
      goto 130

c  pack a column, and free the working array

 150  do k=ppnt1,ppnt2
        nonzeros(k)=updat(rowidx(k))
        updat(rowidx(k))=0
      enddo

c set up diag

      if((ppnode.le.0).or.(prewnode.ne.snhead(i)))then
        diap=diap+diam
        diam=max(trabs,abs(diam*trabs))
        if(abs(diap).lt.diam)then
          call lprngchk(rowidx,nonzeros,ecolpnt(col),count(col),
     x    vcstat,rb,diag,slktyp,dropn,col,dv,diap,w1,odiag(col))
          if(rb.ne.0)err=1
          diag(col)=diap
          if(vcstat(col).le.-2)goto 100
        else
          diag(col)=1.0d00/diap
        endif
      else
        diag(col)=diam
        updat(col)=diap
      endif

c transformation in (primer) supernode

 110  if(prewnode.eq.i)then
        if(ppnode.gt.0)then
          do j=ppnode+1,i
            o=j-1
            p=pivots(j)
            call lpcspnode(ppnode,o,diag,nonzeros,fpnt,count,pivots,
     x      nonzeros(ecolpnt(p)),diag(p))
            diam=max(trabs,abs(diag(p)*trabs))
            diag(p)=diag(p)+updat(p)
            if(abs(diag(p)).lt.diam)then
              call lprngchk(rowidx,nonzeros,ecolpnt(p),count(p),
     x        vcstat,rb,diag,slktyp,dropn,p,dv,diag(p),w1,odiag(p))
              if(rb.ne.0)err=1
            else
              diag(p)=1.0d00/diag(p)
            endif
          enddo
        endif
        ppnode=0
      endif

c update the linked list

 180  if(snhead(i).eq.0)then
        ppnode=0
        if(ppnt1.le.ppnt2)then
          j=rowidx(ppnt1)
          mut(col)=index(j)
          index(j)=col
        endif
        prewnode=0
      else
        if(prewnode.ne.snhead(i))then
          prewnode=snhead(i)
          if(nodtyp(i).gt.0)then
            ppnode=i
          else
            ppnode=-i
          endif
          if(ecolpnt(pivots(prewnode)).le.count(pivots(prewnode)))then
            j=rowidx(ecolpnt(pivots(prewnode)))
            mut(col)=index(j)
            index(j)=-col
          endif
        endif
      endif

c  end of the main loop

      goto 100

c  end of mfactor

  60  if(w1.gt.0)then
        write(buff,'(1x,a,i6)')'total warnings of row dependencies:',w1
        call lpmprnt(buff)
      endif

      end subroutine lpnfactor

c =============================================================================

      subroutine lpcolremv(i,col,mut,index,fpnt,count,pivots,invperm,
     x snhead,nodtyp,rowidx,nonzeros,ppnode,prewnode)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 i,col,mut(mn),index(mn),fpnt(mn),count(mn),pivots(mn),
     x invperm(mn),snhead(mn),nodtyp(mn),rowidx(cfree),ppnode,
     x prewnode
      real*8 nonzeros(cfree)

      integer*4 j,jj,k,l,o,p,pnt1

        jj=index(col)
 195    if(jj.eq.0)goto 103
        if(jj.lt.0)then
          j=-jj
        else
          j=jj
        endif
        k=mut(j)
        pnt1=fpnt(j)
        call lpmove(pnt1,count(j),rowidx,nonzeros)
        if(pnt1.le.count(j))then
          o=rowidx(pnt1)
          mut(j)=index(o)
          index(o)=jj
        endif
        if(jj.lt.0)then
          p=invperm(j)
          l=snhead(p)
          do o=p+1,l
          call lpmove(fpnt(pivots(o)),count(pivots(o)),rowidx,nonzeros)
          enddo
        endif
        jj=k
        goto 195

c step in the primer supernode

 103  if((ppnode.gt.0).and.(prewnode.eq.snhead(i)))then
        l=i-1
        do o=ppnode,l
          pnt1=fpnt(pivots(o))
 104      if(pnt1.le.count(pivots(o)))then
            if(rowidx(pnt1).eq.col)then
              call lpmove(pnt1,count(pivots(o)),rowidx,nonzeros)
              pnt1=count(pivots(o))
            endif
            pnt1=pnt1+1
            goto 104
          endif
        enddo
      endif

c make changes

      pivotn=pivotn-1
      do j=i,pivotn
        pivots(j)=pivots(j+1)
        snhead(j)=snhead(j+1)
        nodtyp(j)=nodtyp(j+1)
      enddo
      do j=1,pivotn
        if(snhead(j).ge.i)snhead(j)=snhead(j)-1
        invperm(pivots(j))=j
      enddo
      if(prewnode.ge.i)prewnode=prewnode-1

      end subroutine lpcolremv

c =============================================================================

      subroutine lpmove(pnt1,pnt2,rowidx,nonzeros)
      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4 pnt1,pnt2,rowidx(cfree),i,j
      real*8    nonzeros(cfree),s
      if(pnt1.le.pnt2)then
        j=rowidx(pnt1)
        s=nonzeros(pnt1)
        pnt2=pnt2-1
        do i=pnt1,pnt2
          nonzeros(i)=nonzeros(i+1)
          rowidx(i)=rowidx(i+1)
        enddo
        rowidx(pnt2+1)=j
        nonzeros(pnt2+1)=s
      endif

      end subroutine lpmove

c =============================================================================

c  callable interface

c  standard form: ax-s=b    u>=x,s>=l

c  remarks:
c          eq  rows    0  >= s >=   0
c          gt  rows  +inf >= s >=   0
c          lt  rows    0  >= s >= -inf
c          fr  rows  +inf >= s >= -inf

c  input:   obj           objective function (to be minimize)       (n)
c           rhs           right-hand side                           (m)
c           lbound        lower bounds                              (m+n)
c           ubound        upper bounds                              (m+n)
c           colpnt        pointer to the columns                    (n+1)
c           rowidx        row indices                               (nz)
c           nonzeros      nonzero values                            (nz)
c           big           practical +inf

c  output: code           termination code
c          xs             primal values
c          dv             dual values
c          dspr           dual resuduals

c input arrays will be destroyed !

c ===========================================================================

      subroutine lpsolver(
     x obj,rhs,lbound,ubound,diag,odiag,xs,dxs,dxsn,up,dspr,ddspr,
     x ddsprn,dsup,ddsup,ddsupn,dv,ddv,ddvn,prinf,upinf,duinf,scale,
     x nonzeros,
     x vartyp,slktyp,colpnt,ecolpnt,count,vcstat,pivots,invprm,
     x snhead,nodtyp,inta1,prehis,rowidx,rindex,
     x code,opt,iter,corect,fixn,dropn,fnzmax,fnzmin,addobj,
     x bigbou,big,ft)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpinitv/ prmin,upmax,dumin,stamet,safmet,premet,regul
      real*8        prmin,upmax,dumin
      integer*4     stamet,safmet,premet,regul

      integer*4 fixn,dropn,code,iter,corect,fnzmin,fnzmax,ft
      real*8  addobj,opt,big,
     x obj(n),rhs(m),lbound(mn),ubound(mn),scale(mn),diag(mn),odiag(mn),
     x xs(mn),dxs(mn),dxsn(mn),up(mn),dspr(mn),ddspr(mn),ddsprn(mn),
     x dsup(mn),ddsup(mn),ddsupn(mn),dv(m),ddv(m),ddvn(m),
     x nonzeros(cfree),prinf(m),upinf(mn),duinf(mn),bigbou
      integer*4 vartyp(n),slktyp(m),colpnt(n1),ecolpnt(mn),
     x count(mn),vcstat(mn),pivots(mn),invprm(mn),snhead(mn),
     x nodtyp(mn),inta1(mn),prehis(mn),rowidx(cfree),rindex(rfree)

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
      common/lpascal/ objnor,rhsnor,scdiff,scpass,scalmet
      real*8        objnor,rhsnor,scdiff
      integer*4     scpass,scalmet
c ---------------------------------------------------------------------------
      integer*4 i,j,k,active,pnt1,pnt2,prelen,freen
      real*8 scobj,scrhs,sol,lbig
      character*99 buff
c ---------------------------------------------------------------------------

c inicializalas

      if(cfree.le.(nz+1)*2)then
        write(buff,'(1x,a)')'not enough memory, realmem < nz !'
        call lpmprnt(buff)
        code=-2
        goto 50
      endif
      if(rfree.le.nz)then
        write(buff,'(1x,a)')'not enough memory, intmem < nz !'
        call lpmprnt(buff)
        code=-2
        goto 50
      endif
      iter=0
      corect=0
      prelen=0
      fnzmin=cfree
      fnzmax=-1
      scobj=1.0d+0
      scrhs=1.0d+0
      code=0
      lbig=0.9d+0*big
      if(bigbou.gt.lbig)then
        lbig=bigbou
        big=lbig/0.9d+0
      endif
      do i=1,mn
        scale(i)=1.0d+0
      enddo

c remove fix variables and free rows

      do i=1,n
        vartyp(i)=0
        if(abs(ubound(i)-lbound(i)).le.tplus*(abs(lbound(i)+1.0d0)))then
          vartyp(i)= 1
          vcstat(i)=-2-1
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            rhs(rowidx(j))=rhs(rowidx(j))-ubound(i)*nonzeros(j)
          enddo
          addobj=addobj+obj(i)*lbound(i)
        else
          vcstat(i)=0
        endif
      enddo
      do i=1,m
        slktyp(i)=0
        j=i+n
        if((ubound(j).gt.lbig).and.(lbound(j).lt.-lbig))then
          vcstat(j)=-2-1
        else
          vcstat(j)=0
        endif
      enddo

c   p r e s o l v e r

      call lptimer(k)
      if(premet.gt.0)then
        write(buff,'(1x)')
        call lpmprnt(buff)
        write(buff,'(1x,a)')'process: presolv'
        call lpmprnt(buff)
        call lppresol(colpnt,rowidx,nonzeros,rindex,nonzeros(nz+1),
     x  snhead,snhead(n1),nodtyp,nodtyp(n1),vcstat,vcstat(n1),
     x  ecolpnt,count,ecolpnt(n1),count(n1),
     x  vartyp,dxsn(n1),dxs(n1),diag(n1),odiag(n1),
     x  ubound,lbound,ubound(n1),lbound(n1),rhs,obj,prehis,prelen,
     x  addobj,big,pivots,invprm,dv,ddv,dxsn,dxs,diag,odiag,premet,code)
        write(buff,'(1x,a)')'presolv done...'
        call lpmprnt(buff)
        if(code.ne.0)goto 45
      endif

c remove lower bounds

      call lpstndrd(ubound,lbound,rhs,obj,nonzeros,
     x vartyp,slktyp,vcstat,colpnt,rowidx,addobj,tplus,tzer,lbig,big)

c scaling before aggregator

      i=iand(scalmet,255)
      j=iand(scpass,255)
      if(i.gt.0)call lpmscale(colpnt,rowidx,nonzeros,obj,rhs,ubound,
     x vcstat,scale,upinf,i,j,scdiff,ddsup,dxsn,dxs,snhead)

c aggregator

      if(premet.gt.127)then
        write(buff,'(1x)')
        call lpmprnt(buff)
        write(buff,'(1x,a)')'process: aggregator'
        call lpmprnt(buff)
        call lpaggreg(colpnt,rowidx,nonzeros,rindex,
     x  vcstat,vcstat(n1),ecolpnt,count,ecolpnt(n1),count(n1),
     x  rhs,obj,prehis,prelen,pivots,vartyp,slktyp,invprm,snhead,
     x  nodtyp,inta1,inta1(n1),dv,addobj,premet,code)
        write(buff,'(1x,a)')'aggregator done...'
        call lpmprnt(buff)
        if(code.ne.0)goto 55
      endif

c scaling after aggregator

      i=scalmet/256
      j=scpass/256
      if(i.gt.0)call lpmscale(colpnt,rowidx,nonzeros,obj,rhs,
     x ubound,vcstat,scale,upinf,i,j,scdiff,ddsup,dxsn,dxs,snhead)

      call lptimer(j)
      write(buff,'(1x)')
      call lpmprnt(buff)
      write(buff,'(1x,a,f8.2,a)')
     x 'time for presolv, scaling and aggregator: ',0.01*(j-k),' sec.'
      call lpmprnt(buff)

c cleaning

      do i=1,mn
        xs(i)=0.0d+0
        dspr(i)=0.0d+0
        dsup(i)=0.0d+0
        up(i)=0.0d+0
      enddo
      do i=1,m
        dv(i)=0.0d+0
      enddo

c is the problem solved ?

      fixn=0
      dropn=0
      freen=0
      do i=1,n
        if(vcstat(i).le.-2)then
          fixn=fixn+1
        else if(vartyp(i).eq.0) then
          freen=freen+1
        endif
      enddo
      do i=1,m
        if(vcstat(i+n).le.-2)dropn=dropn+1
      enddo
      active=mn-fixn-dropn
      if(active.eq.0)code=2
      if(code.gt.0)then
        opt=addobj
        write(buff,'(1x,a)')'problem is solved by the pre-solver'
        call lpmprnt(buff)
        if(code.gt.0)goto 55
        goto 50
      endif

c presolve statistics

      if(premet.gt.0)then
        i=0
        j=0
        do k=1,n
          if(vcstat(k).gt.-2)then
            i=i+count(k)-ecolpnt(k)+1
            if(j.lt.count(k)-ecolpnt(k)+1)j=count(k)-ecolpnt(k)+1
          endif
        enddo
        write(buff,'(1x,a22,i8)')'number of rows       :',(m-dropn)
        call lpmprnt(buff)
        write(buff,'(1x,a22,i8)')'number of columns    :',(n-fixn)
        call lpmprnt(buff)
        write(buff,'(1x,a22,i8)')'free variables       :',freen
        call lpmprnt(buff)
        write(buff,'(1x,a22,i8)')'no. of nonzeros      :',i
        call lpmprnt(buff)
        write(buff,'(1x,a22,i8)')'longest column count :',j
        call lpmprnt(buff)
      endif

c incrase rowidx by n

      j=colpnt(1)
      k=colpnt(n+1)-1
      do i=j,k
        rowidx(i)=rowidx(i)+n
      enddo
      active=mn-fixn-dropn

c normalize obj and rhs

      if(objnor.gt.tzer)then
        call lpscalobj(obj,scobj,vcstat,objnor)
      endif
      if(rhsnor.gt.tzer)then
        call lpscalrhs(rhs,scrhs,vcstat,rhsnor,ubound,xs,up)
      endif

c calling phas12

      sol=scobj*scrhs
      i=mn+mn
      call lptimer(k)
      call lpphas12(
     x obj,rhs,ubound,diag,odiag,xs,dxs,dxsn,up,dspr,ddspr,
     x ddsprn,dsup,ddsup,ddsupn,dv,ddv,ddvn,nonzeros,prinf,upinf,duinf,
     x vartyp,slktyp,colpnt,ecolpnt,count,vcstat,pivots,invprm,
     x snhead,nodtyp,inta1,rowidx,rindex,
     x dxs,dxsn,ddspr,ddsprn,ddsup,ddsupn,
     x code,opt,iter,corect,fixn,dropn,active,fnzmax,fnzmin,addobj,
     x sol,ft,i)
      call lptimer(j)
      write(buff,'(1x,a,f11.2,a)')'solver time ',0.01*(j-k),' sec.'
      call lpmprnt(buff)

c decrease rowidx by n

      j=colpnt(1)
      k=colpnt(n+1)-1
      do i=j,k
        rowidx(i)=rowidx(i)-n
      enddo

c rescaling

  55  do i=1,m
        rhs(i)=rhs(i)*scrhs*scale(i+n)
        ubound(i+n)=ubound(i+n)*scrhs*scale(i+n)
        xs(i+n)=xs(i+n)*scrhs*scale(i+n)
        up(i+n)=up(i+n)*scrhs*scale(i+n)
        dv(i)=dv(i)*scobj/scale(i+n)
        dspr(i+n)=dspr(i+n)/scale(i+n)*scobj
        dsup(i+n)=dsup(i+n)/scale(i+n)*scobj
      enddo

      do i=1,n
        obj(i)=obj(i)*scobj*scale(i)
        ubound(i)=ubound(i)*scrhs/scale(i)
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do j=pnt1,pnt2
          nonzeros(j)=nonzeros(j)*scale(i)*scale(rowidx(j)+n)
        enddo

        xs(i)=xs(i)/scale(i)*scrhs
        up(i)=up(i)/scale(i)*scrhs
        dspr(i)=dspr(i)*scale(i)*scobj
        dsup(i)=dsup(i)*scale(i)*scobj
      enddo

c postprocessing

  45  call lppstsol(colpnt,rowidx,nonzeros,vcstat,vcstat(n1),
     x vartyp,slktyp,ubound,lbound,ubound(n1),lbound(n1),rhs,obj,xs,
     x inta1,ddvn,prehis,prelen,big)

  50  return
      end subroutine lpsolver

c ===========================================================================

      subroutine lpstndrd(ubound,lbound,rhs,obj,nonzeros,
     x vartyp,slktyp,vcstat,colpnt,rowidx,addobj,tplus,tzer,lbig,big)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 vartyp(n),slktyp(m),vcstat(mn),colpnt(n1),rowidx(nz)
      real*8 ubound(mn),lbound(mn),rhs(m),obj(n),nonzeros(nz),
     x addobj,tplus,tzer,lbig,big

      integer*4 i,j,k,pnt1,pnt2

c generate standard form, row modification

      k=0
      do 150 i=1,m
        j=i+n
        if(vcstat(j).gt.-2)then
          if(abs(ubound(j)-lbound(j)).le.tplus*(abs(lbound(j))+1d0))then
            slktyp(i)=0
            ubound(j)=0.0d+00
            rhs(i)=rhs(i)+lbound(j)
            goto 150
          endif
ccc          if((ubound(j).gt.lbig).and.(lbound(j).lt.-lbig))then
ccc            vcstat(j)=-2
ccc            slktyp(i)=0
ccc            goto 150
ccc          endif
          if(lbound(j).lt.-lbig)then
            slktyp(i)=2
            lbound(j)=-ubound(j)
            ubound(j)=big
            rhs(i)=-rhs(i)
            k=k+1
          else
            slktyp(i)=1
          endif
          rhs(i)=rhs(i)+lbound(j)
          ubound(j)=ubound(j)-lbound(j)
          if(ubound(j).lt.lbig)slktyp(i)=-slktyp(i)
        else
          slktyp(i)=0
        endif
 150  continue

c negate reverse rows

      if(k.gt.0)then
        do i=1,n
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if(abs(slktyp(rowidx(j))).ge.2)nonzeros(j)=-nonzeros(j)
          enddo
        enddo
      endif

c column modification

      do 155 i=1,n
        if(vcstat(i).gt.-2)then
ccc          if(abs(ubound(i)-lbound(i)).le.tplus*(abs(lbound(i))+1d0))then
ccc            vcstat(i)=-2
ccc            vartyp(i)= 1
ccc            do j=colpnt(i),colpnt(i+1)-1
ccc              rhs(rowidx(j))=rhs(rowidx(j))-nonzeros(j)*lbound(i)
ccc            enddo
ccc            addobj=addobj+obj(i)*lbound(i)
ccc            goto 155
ccc          endif
          if((ubound(i).gt.lbig).and.(lbound(i).lt.-lbig))then
            vartyp(i)=0
            goto 155
          endif
          if(lbound(i).lt.-lbig)then
            vartyp(i)=2
            lbound(i)=-ubound(i)
            ubound(i)=big
            obj(i)=-obj(i)
            do j=colpnt(i),colpnt(i+1)-1
              nonzeros(j)=-nonzeros(j)
            enddo
          else
            vartyp(i)=1
          endif
          if(abs(lbound(i)).gt.tzer)then
            if(ubound(i).lt.lbig)ubound(i)=ubound(i)-lbound(i)
            do j=colpnt(i),colpnt(i+1)-1
              rhs(rowidx(j))=rhs(rowidx(j))-nonzeros(j)*lbound(i)
            enddo
            addobj=addobj+obj(i)*lbound(i)
          endif
          if(ubound(i).lt.lbig)vartyp(i)=-vartyp(i)
        endif
 155  continue

      end subroutine lpstndrd

c ===========================================================================
c modifying the primal and dual variables
c ===========================================================================

      subroutine lppdmodi(xs,dspr,vcstat,
     x vartyp,slktyp,gap,pobj,dobj,prinf,duinf,upinf,
     x colpnt,rowidx,rownz,pinf,uinf,dinf)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpcompl/ climit,ccorr
      real*8        climit,ccorr

      integer*4 vcstat(mn),vartyp(n),slktyp(m),colpnt(n1),rowidx(nz)
      real*8 xs(mn),dspr(mn),gap,pobj,dobj,
     x prinf(m),upinf(mn),duinf(mn),rownz(nz),pinf,uinf,dinf

      integer*4 i,j,k,prm,dum,upm,pnt1,pnt2
      real*8 sp,sd,sol,s

c --------------------------------------------------------------------------

      prm=0
      dum=0
      upm=0
      sd=gap
      sp=abs(pobj-dobj)/(abs(pobj)+1.0d0)
      sd=sd*ccorr
      if(sd.gt.climit)sd=climit
      do i=1,mn
        if(vcstat(i).gt.-2)then
          if(i.le.n)then
            j=vartyp(i)
          else
            j=slktyp(i-n)
          endif
          if(j.ne.0)then
            sp=xs(i)*dspr(i)
            if(sp.lt.sd)then
              if(xs(i).gt.dspr(i))then
                sol=sd/xs(i)
                duinf(i)=duinf(i)+dspr(i)-sol
                dspr(i)=sol
                dum=dum+1
              else
                sol=sd/dspr(i)
                s=xs(i)-sol
                xs(i)=sol
                if(j.lt.0)then
                  upinf(i)=upinf(i)+s
                  upm=upm+1
                endif
                if(i.le.n)then
                  pnt1=colpnt(i)
                  pnt2=colpnt(i+1)-1
                  do k=pnt1,pnt2
                    prinf(rowidx(k)-n)=prinf(rowidx(k)-n)+s*rownz(k)
                  enddo
                else
                  prinf(i-n)=prinf(i-n)-s
                endif
                prm=prm+1
              endif
            endif
ccc
ccc it's totally wrong! do not modify upper bounds !
ccc
ccc              if(j.lt.0)then
ccc                sp=up(i)*dsup(i)
ccc                if(sp.lt.sd)then
ccc                  if(up(i).gt.dsup(i))then
ccc                    sol=sd/up(i)
ccc                    duinf(i)=duinf(i)-dsup(i)+sol
ccc                    dsup(i)=sol
ccc                    dum=dum+1
ccc                  else
ccc                    sol=sd/dsup(i)
ccc                    upinf(i)=upinf(i)+up(i)-sol
ccc                    up(i)=sol
ccc                    upm=upm+1
ccc                  endif
ccc                endif
ccc              endif
            endif
        endif
      enddo

c correct infeas. norm

      if(prm.gt.0)then
        pinf=0.0d+0
        do i=1,m
          if(vcstat(i+n).gt.-2)then
            if(abs(prinf(i)).gt.pinf)pinf=abs(prinf(i))
          else
            prinf(i)=0.0d+0
         endif
       enddo
      endif
      if(upm.gt.0)then
        uinf=0.0d+0
        do i=1,mn
          if(vcstat(i).gt.-2)then
            if(abs(upinf(i)).gt.uinf)uinf=abs(upinf(i))
          else
            upinf(i)=0.0d+0
          endif
        enddo
      endif
      if(dum.gt.0)then
        dinf=0.0d+0
        do i=1,mn
          if(vcstat(i).gt.-2)then
            if(abs(duinf(i)).gt.dinf)dinf=abs(duinf(i))
          else
            duinf(i)=0.0d+0
          endif
        enddo
      endif

      end subroutine lppdmodi

c ===========================================================================

c  prelev:  1 :  rowsng
c           2 :  colsng
c           4 :  rowact
c           8 :  chepdu
c          16 :  duchek
c          32 :  bndchk
c          64 :  splchk
c         128 :  freagr
c         256 :  sparse
c         512 :  xduchk

c ========================================================================

      subroutine lppresol(colpnt,colidx,colnzs,rowidx,rownzs,
     x collst,rowlst,colmrk,rowmrk,colsta,rowsta,
     x colbeg,colend,rowbeg,rowend,
     x vartyp,pmaxr,pminr,pmbig,ppbig,
     x upperb,lowerb,upslck,loslck,rhs,obj,prehis,prelen,
     x addobj,big,list,mrk,
     x dulo,duup,dmaxc,dminc,dmbig,dpbig,prelev,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 colpnt(n1),colidx(nz),rowidx(nz),
     x collst(n),rowlst(m),pmbig(m),ppbig(m),
     x colbeg(n),colend(n),rowbeg(m),rowend(m),
     x colmrk(n),rowmrk(m),colsta(n),rowsta(m),
     x list(mn),mrk(mn),prehis(mn),prelen,prelev,code,
     x dpbig(n),dmbig(n),vartyp(n)
      real*8    colnzs(nz),rownzs(nz),pmaxr(m),pminr(m),addobj,
     x upperb(n),lowerb(n),upslck(m),loslck(m),rhs(m),obj(n),
     x dulo(m),duup(m),dmaxc(n),dminc(n),big

      integer*4 i,j,k,p,o,pnt1,pnt2,pass,cnum,procn,rnum,coln,rown
      real*8    sol,up,lo,tfeas,zero,lbig,bigbou,dbigbo
      integer*4 dusrch,bndsrc,bndchg
      character*99 buff

c initialize : clean up the matrix and set-up row-wise structure

      tfeas  = 1.0d-08
      zero   = 1.0d-15
      dusrch =  10
      bndsrc =   5
      bndchg =   6
      bigbou = 1.0d+5
      dbigbo = 1.0d+5

      lbig = big*0.9d+0
      pass=0
      rown=0
      coln=0
      cnum=0
      rnum=0
      do i=1,mn
        mrk(i)=-1
      enddo
      do i=1,m
        pmaxr(i)=0.0d+0
        pminr(i)=0.0d+0
        pmbig(i)=0
        ppbig(i)=0
        rowend(i)=0
        if(rowsta(i).gt.-2)then
          rown=rown+1
          rowlst(rown)=i
          rowmrk(i)=0
        endif
      enddo
      do i=1,n
        dmaxc(i)=0.0d+0
        dminc(i)=0.0d+0
        dmbig(i)=0
        dpbig(i)=0
        if(colsta(i).gt.-2)then
          coln=coln+1
          collst(coln)=i
          colmrk(i)=0
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          p=pnt2
          do j=pnt2,pnt1,-1
           if((rowsta(colidx(j)).le.-2).or.(abs(colnzs(j)).lt.zero))then
             o=colidx(j)
             sol=colnzs(j)
             colidx(j)=colidx(p)
             colnzs(j)=colnzs(p)
             colidx(p)=o
             colnzs(p)=sol
             p=p-1
           else
             rowend(colidx(j))=rowend(colidx(j))+1
           endif
          enddo
          colbeg(i)=pnt1
          colend(i)=p
        endif
      enddo
      pnt1=1
      do j=1,rown
        i=rowlst(j)
        rowbeg(i)=pnt1
        pnt1=pnt1+rowend(i)
        rowend(i)=rowbeg(i)-1
      enddo
      do k=1,coln
        i=collst(k)
        pnt1=colbeg(i)
        pnt2=colend(i)
        do j=pnt1,pnt2
          rowend(colidx(j))=rowend(colidx(j))+1
          rowidx(rowend(colidx(j)))=i
          rownzs(rowend(colidx(j)))=colnzs(j)
        enddo
      enddo

c initialize the minimum and maximum row activity

      sol=0.9d+0*big
      o=1
      do j=1,coln
        i=collst(j)
        pnt1=colbeg(i)
        pnt2=colend(i)
        up=upperb(i)
        lo=lowerb(i)
        call lpchgmxm(pnt1,pnt2,up,lo,colidx,colnzs,
     x  ppbig,pmaxr,pmbig,pminr,sol,o,m)
      enddo

c start presolve sequence: step 1 : row singletons

  10  procn=1
      call lpsetlst(n,m,nz,rown,rowlst,rowmrk,coln,collst,colmrk,
     x procn,rowsta,colsta,rowbeg,rowend,cnum,list,mrk,pass,
     x colbeg,colend,colidx)
      if(coln+rown.eq.0)goto 50
      if((iand(prelev,1).gt.0).and.(cnum.gt.0))then
        call lprowsng(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  coln,collst,colmrk,rown,rowlst,rowmrk,
     x  cnum,list,mrk,procn,
     x  ppbig,pmaxr,pmbig,pminr,
     x  lbig,tfeas,zero,code)
        if(code.gt.0)goto 100
      endif

c step 2 : column singletons

      procn=2
      call lpsetlst(m,n,nz,coln,collst,colmrk,rown,rowlst,rowmrk,
     x procn,colsta,rowsta,colbeg,colend,cnum,list,mrk,pass,
     x rowbeg,rowend,rowidx)
      if(coln+rown.eq.0)goto 50
      if((iand(prelev,2).gt.0).and.(cnum.gt.0))then
        call lpcolsng(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  coln,collst,colmrk,
     x  cnum,list,mrk,procn,
     x  ppbig,pmaxr,pmbig,pminr,
     x  lbig,tfeas,zero,code)
        if(code.gt.0)goto 100
      endif

c step 3 : row activity check

      procn=3
      call lpsetlst(n,m,nz,rown,rowlst,rowmrk,coln,collst,colmrk,
     x procn,rowsta,colsta,rowbeg,rowend,cnum,list,mrk,pass,
     x colbeg,colend,colidx)
      if(coln+rown.eq.0)goto 50
      if((iand(prelev,4).gt.0).and.(cnum.gt.0))then
        call lprowact(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  coln,collst,colmrk,rown,rowlst,rowmrk,
     x  cnum,list,mrk,procn,
     x  ppbig,pmaxr,pmbig,pminr,
     x  lbig,tfeas,code)
        if(code.gt.0)goto 100
      endif

c step 4 : cheap dual test

      procn=4
      call lpsetlst(m,n,nz,coln,collst,colmrk,rown,rowlst,rowmrk,
     x procn,colsta,rowsta,colbeg,colend,cnum,list,mrk,pass,
     x rowbeg,rowend,rowidx)
      if(coln+rown.eq.0)goto 50
      if((iand(prelev,8).gt.0).and.(cnum.gt.0))then
        call lpchepdu(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  coln,collst,colmrk,rown,rowlst,rowmrk,
     x  cnum,list,mrk,procn,
     x  ppbig,pmaxr,pmbig,pminr,
     x  lbig,zero,code)
        if(code.gt.0)goto 100
      endif

c step 5 : usual dual test

      procn=5
      call lpsetlst(n,m,nz,rown,rowlst,rowmrk,coln,collst,colmrk,
     x procn,rowsta,colsta,rowbeg,rowend,rnum,list(n+1),mrk(n+1),pass,
     x colbeg,colend,colidx)

c remove zero entries at the first loop from the main list

      if (pass.eq.5)then
        k=1
   5    if(k.le.coln)then
          if(colmrk(collst(k)).eq.0)then
            colmrk(collst(k))=-1
            collst(k)=collst(coln)
            coln=coln-1
          else
            k=k+1
          endif
          goto 5
        endif
        k=1
  20    if(k.le.rown)then
          if(rowmrk(rowlst(k)).eq.0)then
            rowmrk(rowlst(k))=-1
            rowlst(k)=rowlst(rown)
            rown=rown-1
          else
            k=k+1
          endif
          goto 20
        endif
      endif

      if((iand(prelev,16).gt.0).and.(cnum+rnum.gt.0))then
        call lpduchek(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  coln,collst,colmrk,rown,rowlst,rowmrk,
     x  cnum,list,mrk,rnum,list(n+1),mrk(n+1),procn,
     x  ppbig,pmaxr,pmbig,pminr,
     x  dulo,duup,dmaxc,dminc,dpbig,dmbig,
     x  big,lbig,tfeas,zero,dbigbo,dusrch,code,prelev)
        if(code.gt.0)goto 100
      endif
      goto 10

c bound check

  50  procn=6
      if(iand(prelev,32).gt.0)then
        call lpbndchk(n,m,mn,nz,
     x  colbeg,colend,colidx,colnzs,
     x  rowbeg,rowend,rowidx,rownzs,
     x  upperb,lowerb,upslck,loslck,
     x  rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x  cnum,list,mrk,procn,dmaxc,dminc,
     x  ppbig,pmaxr,pmbig,pminr,dpbig,dmbig,
     x  big,lbig,tfeas,bndsrc,bndchg,bigbou,code)
        if(code.gt.0)goto 100
      endif

c finding splitted free variables

      procn=7
      if(iand(prelev,64).gt.0)then
       call lpcoldbl(n,m,mn,nz,colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,upperb,lowerb,obj,colsta,
     x prelen,prehis,procn,list,dmaxc,vartyp,big,lbig,tfeas,zero)
        if(code.gt.0)goto 100
      endif
      goto 999

c infeasibility detected

 100  if(code.eq.3)then
        write(buff,'(1x,a)')'dual infeasibility detected in presolve'
      else
        write(buff,'(1x,a)')'primal infeasibility detected in presolve'
      endif
      call lpmprnt(buff)
      if (procn.eq.1)then
        write(buff,'(1x,a)')'presolve process: row singleton check'
      else if (procn.eq.2)then
        write(buff,'(1x,a)')'presolve process: column singleton check'
      else if (procn.eq.3)then
        write(buff,'(1x,a)')'presolve process: row activity check'
      else if (procn.eq.4)then
        write(buff,'(1x,a)')'presolve process: cheap dual check'
      else if (procn.eq.5)then
        write(buff,'(1x,a)')'presolve process: dual check'
      else if (procn.eq.6)then
        write(buff,'(1x,a)')'presolve process: bound check'
      else if (procn.eq.7)then
        write(buff,'(1x,a)')'presolve process: splitcol check'
      endif
      call lpmprnt(buff)

  999 return
      end subroutine lppresol

c ============================================================================

      subroutine lpchgmxm(pnt1,pnt2,upper,lower,idx,nonzrs,
     x pbig,maxr,mbig,minr,lbig,dir,siz)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

c this subroutine changes/updates the minimum/maximum row activity
c values

      integer*4 siz,pnt1,pnt2,idx(nz),pbig(siz),mbig(siz),dir
      real*8    upper,lower,nonzrs(nz),maxr(siz),minr(siz),lbig

      integer*4 j,k
      real*8 s

      do j=pnt1,pnt2
        k=idx(j)
        s=nonzrs(j)
        if(s.gt.0d+0)then
          if(upper.ge.lbig)then
            pbig(k)=pbig(k)+dir
          else
            maxr(k)=maxr(k)+upper*s*dble(dir)
          endif
          if(lower.le.-lbig)then
            mbig(k)=mbig(k)+dir
          else
            minr(k)=minr(k)+lower*s*dble(dir)
          endif
        else
          if(upper.ge.lbig)then
            mbig(k)=mbig(k)+dir
          else
            minr(k)=minr(k)+upper*s*dble(dir)
          endif
          if(lower.le.-lbig)then
            pbig(k)=pbig(k)+dir
          else
            maxr(k)=maxr(k)+lower*s*dble(dir)
          endif
        endif
      enddo

      end subroutine lpchgmxm

c ============================================================================

      subroutine lpmodmxm(nz,pnt1,pnt2,oldb,newb,rowidx,nonzeros,
     x pbig,maxr,mbig,minr,lbig,dir,siz)

c this subroutine modifies the row (column) activity values
c from an old bound (oldb) to a new one (newb)
c dir= 1 update on upper bound
c dir=-1 update on lower bound

      integer*4 nz,siz,pnt1,pnt2,rowidx(nz),pbig(siz),mbig(siz),dir
      real*8    oldb,newb,nonzeros(nz),maxr(siz),minr(siz),lbig

      integer*4 f,j,k
      real*8 s,diff

      f=0
      diff=newb-oldb
      if(abs(oldb).gt.lbig)then
        diff=newb
        f=1
        if(oldb.gt.0.0d+0)then
          dir=1
        else
          dir=-1
        endif
        do j=pnt1,pnt2
          k=rowidx(j)
          if((nonzeros(j)*dble(dir)).gt.0.0d+0)then
            pbig(k)=pbig(k)-1
          else
            mbig(k)=mbig(k)-1
          endif
        enddo
      endif
      if(abs(newb).gt.lbig)then
        diff=-oldb
        f=f+2
        if(newb.gt.0)then
          dir=1
        else
          dir=-1
        endif
        do j=pnt1,pnt2
          k=rowidx(j)
          if((nonzeros(j)*dble(dir)).gt.0.0)then
            pbig(k)=pbig(k)+1
          else
            mbig(k)=mbig(k)+1
          endif
        enddo
      endif
      if(f.lt.3)then
        do j=pnt1,pnt2
          k=rowidx(j)
          s=nonzeros(j)
          if(s.gt.0.0d+0)then
            if(dir.eq.1)then
              maxr(k)=maxr(k)+diff*s
            else
              minr(k)=minr(k)+diff*s
            endif
          else
            if(dir.eq.1)then
              minr(k)=minr(k)+diff*s
            else
              maxr(k)=maxr(k)+diff*s
            endif
          endif
        enddo
      endif

      end subroutine lpmodmxm

c ============================================================================

      subroutine lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x colbeg,colend,rowbeg,rowend,rhs,pivot,traf)

c this subroutine removes a column from the row-wise representation
c and updates the right-hand side, if parameter traf is set

      integer*4 m,n,nz,col,colidx(nz),rowidx(nz),
     x colbeg(n),colend(n),rowbeg(m),rowend(m)
      real*8    rhs(m),pivot,colnzs(nz),rownzs(nz)
      logical   traf

      integer*4 i,j,k,pnt1,pnt2
      real*8    sol

      do i=colbeg(col),colend(col)
        j=colidx(i)
        pnt1=rowbeg(j)
        pnt2=rowend(j)-1
        do k=pnt1,pnt2
          if(rowidx(k).eq.col)then
            sol=rownzs(k)
            rowidx(k)=rowidx(pnt2+1)
            rownzs(k)=rownzs(pnt2+1)
            rowidx(pnt2+1)=col
            rownzs(pnt2+1)=sol
            goto 10
          endif
        enddo
  10    rowend(j)=pnt2
      enddo
      if(traf)then
        do i=colbeg(col),colend(col)
          rhs(colidx(i))=rhs(colidx(i))-pivot*colnzs(i)
        enddo
      endif

      end subroutine lpremove

c =============================================================================

      subroutine lpsetlst(m,n,nz,coln,collst,colmrk,rown,rowlst,rowmrk,
     x procn,colsta,rowsta,colbeg,colend,cnum,list,mrk,pass,
     x rowbeg,rowend,rowidx)

c this subroutine deletes entries from the main search list
c and set-up the local search list for the presolv subprocesses.

      integer*4 m,n,nz,coln,collst(n),colmrk(n),procn,colsta(n),
     x cnum,list(n),mrk(n),pass,colbeg(n),colend(n),
     x rown,rowlst(m),rowmrk(m),rowsta(m),rowbeg(m),rowend(m),
     x rowidx(nz)

      integer*4 i,j,k,p1,p2

      pass=pass+1
      k=1
      cnum=0
  10  if(k.le.coln)then
        i=collst(k)
        if((colsta(i).le.-2).or.(colmrk(i).eq.procn))then
          collst(k)=collst(coln)
          colmrk(i)=-procn
          coln=coln-1
        else
          k=k+1
          if((procn.le.2).and.(colbeg(i).ne.colend(i)))goto 10
          cnum=cnum+1
          list(cnum)=i
          mrk(i)=pass
        endif
        goto 10
      endif

      k=1
  20  if(k.le.rown)then
        i=rowlst(k)
        if((rowsta(i).le.-2).or.(rowmrk(i).eq.procn))then
          rowlst(k)=rowlst(rown)
          rowmrk(i)=-procn
          rown=rown-1
        else
          k=k+1
        endif
        goto 20
      endif

c extend lists

      k=1
      do while (k.le.rown)
        p1=rowbeg(rowlst(k))
        p2=rowend(rowlst(k))
        do i=p1,p2
          j=rowidx(i)
          if((mrk(j).lt.0).and.
     x    ((procn.gt.2).or.(colbeg(j).eq.colend(j))))then
            mrk(j)=procn
            cnum=cnum+1
            list(cnum)=j
          endif
        enddo
        k=k+1
      enddo

      end subroutine lpsetlst

c ==========================================================================
c this is a postsolv procedure

c ========================================================================

      subroutine lppstsol(colpnt,colidx,colnzs,colsta,rowsta,
     x vartyp,slktyp,upb,lob,ups,los,rhs,obj,xs,
     x status,rowval,prehis,prelen,big)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 colpnt(n1),colidx(nz),colsta(n),rowsta(m),
     x prehis(mn),prelen,vartyp(n),slktyp(m),status(mn)
      real*8    colnzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),rowval(m),xs(n),big

      integer*4 i,j,k,l,p,pnt1,pnt2,row,col
      real*8    sol,lo1,lo2,up1,up2,lbig,sol1,sol2,s

      lbig=0.9d+0*big
      do i=1,mn
        if(i.le.n)then
          j=colsta(i)
        else
          j=rowsta(i-n)
        endif
        if(j.eq.-3)then
          status(i)=0
        else
          status(i)=prelen+1
        endif
      enddo

      do i=1,prelen
        status(prehis(i))=i
      enddo

      do i=1,m
        rowval(i)=0.0d+0
        if(abs(slktyp(i)).eq.2)rhs(i)=-rhs(i)
      enddo

      do i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do j=pnt1,pnt2
          if(abs(slktyp(colidx(j))).eq.2)then
            colnzs(j)=-colnzs(j)
          endif
        enddo
        if((status(i).gt.prelen).or.(status(i).eq.0))then
          if(vartyp(i).ne.0)then
            if(upb(i).lt.lbig)upb(i)=upb(i)+lob(i)
            xs(i)=xs(i)+lob(i)
            do j=pnt1,pnt2
              rhs(colidx(j))=rhs(colidx(j))+colnzs(j)*lob(i)
            enddo
          endif
          if(abs(vartyp(i)).eq.2)then
            obj(i)=-obj(i)
            upb(i)=-lob(i)
            lob(i)=-big
            xs(i)=-xs(i)
            do j=pnt1,pnt2
              colnzs(j)=-colnzs(j)
            enddo
          endif
          do j=pnt1,pnt2
            rowval(colidx(j))=rowval(colidx(j))+xs(i)*colnzs(j)
          enddo
        endif
      enddo

      i=prelen
      do while(i.ge.1)
        j=prehis(i)
        if(j.le.n)then
           k=-colsta(j)-2
          if((k.eq.1).or.(k.eq.3).or.(k.eq.5).or.(k.eq.6))then
            sol=lob(j)
            xs(j)=sol
          else if((k.eq.2).or.(k.eq.8))then
            row=prehis(i+1)-n
            l=colpnt(j)
            do while(l.lt.colpnt(j+1))
              if(colidx(l).eq.row)then
                sol=colnzs(l)
                l=colpnt(j+1)
              endif
              l=l+1
            enddo
            sol=(rhs(row)-rowval(row))/sol
            xs(j)=sol
          else if(k.eq.4)then
            k=0
            sol1=lob(j)
            sol2=upb(j)
            p=i+1
            do while ((p.le.prelen).and.(prehis(p).gt.n).and.
     x        (-rowsta(prehis(p)-n)-2.eq.4))
              row=prehis(p)-n
              l=colpnt(j)
              do while(l.lt.colpnt(j+1))
                if(colidx(l).eq.row)then
                  sol=colnzs(l)
                  l=colpnt(j+1)
                endif
                l=l+1
              enddo
              if(los(row).gt.-lbig)then
                s=(rhs(row)-rowval(row)+los(row))/sol
                if((sol.gt.0.0d+0).and.(s.gt.sol1))then
                  k=1
                  sol1=s
                endif
                if((sol.lt.0.0d+0).and.(s.lt.sol2))then
                  k=2
                  sol2=s
                endif
              endif
              if(ups(row).lt.lbig)then
                s=(rhs(row)-rowval(row)+ups(row))/sol
                if((sol.gt.0.0d+0).and.(s.lt.sol2))then
                  k=2
                  sol2=s
                endif
                if((sol.lt.0.0d+0).and.(s.gt.sol1))then
                  k=1
                  sol1=s
                endif
              endif
              p=p+1
            enddo
            if(k.eq.1)sol=sol1
            if(k.eq.2)sol=sol2
            if(k.eq.0)then
              sol=sol1
              if(sol.lt.-lbig)sol=sol2
              if(sol.gt.lbig)sol=0.0d+0
            endif
            xs(j)=sol
          else if(k.gt.17)then
            col=k-17
            if((vartyp(j).eq.4).or.(vartyp(j).eq.12))then
              lo2=-big
              lo1=lob(j)
            else
              lo2=lob(j)
              if((lo2.gt.-lbig).and.(lob(col).gt.-lbig))then
                lo1=lob(col)-lo2
              else
                lo1=-big
              endif
            endif
            if((vartyp(j).eq.8).or.(vartyp(j).eq.12))then
              up2=big
              up1=upb(j)
            else
              up2=upb(j)
              if((up2.lt.lbig).and.(upb(col).lt.lbig))then
                up1=upb(col)-up2
              else
                up1=big
              endif
            endif
            lob(col)=lo1
            upb(col)=up1
            sol=0.0d+0
            if(sol.lt.lo2)sol=lo2
            if(sol.gt.up2)sol=up2
            if(xs(col)-sol.lt.lo1)sol=xs(col)-lo1
            if(xs(col)-sol.gt.up1)sol=xs(col)-up1
            xs(j)=sol*obj(j)
            xs(col)=xs(col)-sol
            sol=0.0d+0
          endif
          l=colpnt(j)
          do while(l.lt.colpnt(j+1))
            row=colidx(l)
            if(status(row+n).gt.status(j))then
              rhs(row)=rhs(row)+colnzs(l)*sol
            else
              rowval(row)=rowval(row)+colnzs(l)*sol
            endif
            l=l+1
          enddo
        endif
        i=i-1
      enddo


      end subroutine lppstsol

c ============================================================================
c ===========================================================================

c ===========================================================================
c  super dense oszlopok 'multiply' kezelessel

c ===========================================================================

      subroutine lpffactor(pntc,crow,colpnt,rowidx,
     x mark,pivcols,ccol,nonz,diag,
     x cpermf,cpermb,rpermf,rpermb,pntr,cfill,rfill,
     x cpnt,cnext,cprew,rindex,workr,
     x fixn,dropn,fnzmax,fnzmin,active,oper,actual,slktyp,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      integer*4 rowidx(cfree),rindex(rfree),colpnt(n1),
     x pivcols(mn),cpermf(mn),cpermb(mn),rpermf(mn),rpermb(mn),
     x ccol(mn),crow(mn),pntc(mn),pntr(mn),mark(mn),cfill(mn),
     x cpnt(mn),cnext(mn),cprew(mn),slktyp(m),rfill(mn),fixn,
     x dropn,fnzmax,fnzmin,active,col,dcols,code
      real*8 nonz(cfree),diag(mn),workr(mn),actual(mn),oper
      character*99 buff

c ---------------------------------------------------------------------------

c     cpermf       oszloplista elore lancolasa, fejmutato cfirst
c     cpermb       oszloplista hatra lancolasa, fejmutato clast
c     rpermf       sorlista    elore lancolase, fejmutato rfirst
c     rpermb       sorlista    hatra lancolasa, fejmutato rlast
c     ccol         oszlopszamlalok
c     crow         sorszamlalok (vcstat)
c     pntc         oszlopmutatok
c     pntr         sormutatok
c     mark         eliminacios integer segedtomb
c     workr        eliminacios real    segedtomb
c     cfill        a sorfolytonos tarolas update-elesehez segedtomb
c     rfill        a sorfolytonos tarolas update-elesehez segedtomb
c     cpnt         szamlalok szerinti listak fejmutatoja
c     cnext        szamlalok szerinti elore-lancolt lista
c     cprew        szamlalok szerinti hatra-lancolt lista

c --------------------------------------------------------------------------
      integer*4 pnt,pnt1,pnt2,i,j,k,l,o,p,endmem,ccfree,rcfree,pmode,
     x rfirst,rlast,cfirst,clast,pcol,pcnt,ppnt1,ppnt2,fill,
     x prewcol,ii,pass,minm,w1,wignore,method
      real*8    pivot,ss,tltmp1,tltmp2
c---------------------------------------------------------------------------

   1  format(' not enough memory in the row    file ')
   2  format(' not enough memory in the column file ')
   3  format(' row    realaxed  :',i6,'  diag :',1p,d12.5,'  type :',i3)
   4  format(' column dropped   :',i6,'  diag :',1p,d12.5)
   6  format(' nonzeros         :',i12)
   7  format(' operations       :',0p,f13.0)
   8  format(' superdense cols. :',i12)

c move elements in the dropped rows to the end of the columns

      code=0
      if((order.gt.2.5).and.(order.lt.3.5))then
        method=1
        write(buff,'(a)')' minimum local fill-in heuristic'
      else
        method=0
        write(buff,'(a)')' minimum degree heuristic'
      endif
      call lpmprnt(buff)
      wignore=10
      w1=0
      pass=2
      minm=-m-1
      if(dropn.gt.0)then
        do 15 i=1,n
          if(crow(i).le.-2)goto 15
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          p=pnt2
          do 16 j=pnt2,pnt1,-1
            if(crow(rowidx(j)).gt.-2)goto 16
            o=rowidx(j)
            pivot=nonz(j)
            rowidx(j)=rowidx(p)
            rowidx(p)=o
            nonz(j)=nonz(p)
            nonz(p)=pivot
            p=p-1
  16      continue
  15    continue
      endif

c initialization

      endmem=cfree
      pivotn=0
      pnt=nz+1
      cfirst=0
      clast =0
      pmode =0
      do 11 i=1,mn
        pivcols(i)=0
        ccol(i)=0
        if(crow(i).gt.-2)then
          crow(i)=0
        else
          if(minm.ge.crow(i))minm=crow(i)-1
        endif
        mark(i)=0
  11  continue

c set up the permut lists and compute crow

      dcols=0
      do 10 i=1,mn
        if(crow(i).le.-2)goto 10
        if(i.le.n)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          o=0
          do while((pnt1+o.le.pnt2).and.(crow(rowidx(pnt1+o)).gt.-2))
            o=o+1
          enddo
          if(o.ge.supdens)then
            pass=1
            crow(i)=minm
            dcols=dcols+1
            goto 10
          endif
          pnt2=pnt1+o-1
          do j=pnt1,pnt2
            crow(rowidx(j))=crow(rowidx(j))+1
          enddo
          pntc(i)=pnt1
          ccol(i)=o
        else
          pntc(i)=pnt
          ccol(i)=0
          pnt=pnt+1
        endif
        if(cfirst.eq.0)then
           cfirst=i
        else
           cpermf(clast)=i
        endif
        cpermb(i)=clast
        clast=i
  10  continue
      cpermf(clast)=0
      ccfree=cfree-pnt
      if(ccfree.lt.mn)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif
      write(buff,8)dcols
      call lpmprnt(buff)
      if(pass.eq.1)then
        tltmp1=tpiv2
        tltmp2=tabs
        tabs=tpiv2
      endif

c create pointers to rindex

 500  do i=1,mn
        cpnt(i) =0
        cnext(i)=0
        cprew(i)=0
        workr(i)=0.0d+0
      enddo
      pnt=1
      i=cfirst
      rfirst=0
      rlast=0
  25  if(i.gt.0)then
        if(rfirst.eq.0)then
          rfirst=i
        else
          rpermf(rlast)=i
        endif
        rpermb(i)=rlast
        rlast=i
        pntr(i)=pnt
        rfill(i)=pnt
        pnt=pnt+crow(i)

c initialize the counter lists

        j=crow(i)+ccol(i)+1
        if(j.gt.0)then
          o=cpnt(j)
          cnext(i)=o
          cpnt(j)=i
          if(o.ne.0)cprew(o)=i
        endif
        cprew(i)=0
        i=cpermf(i)
        goto 25
      endif
      rcfree=rfree-pnt
      if(rcfree.lt.mn)then
        write(buff,1)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif

c create the row file : symbolical transps the matrix

      i=cfirst
  26  if(i.gt.0)then
        pnt1=pntc(i)
        pnt2=pnt1+ccol(i)-1
        do 27 j=pnt1,pnt2
          k=rowidx(j)
          if(crow(k).le.-2)goto 27
          rindex(rfill(k))=i
          rfill(k)=rfill(k)+1
  27    continue
        i=cpermf(i)
        goto 26
      endif
      rpermf(rlast)=0
      pcol=0

c loop for pivots

  50  call lpfndpiv(cpnt,cnext,pntc,ccol,crow,rowidx,nonz,
     x diag,pcol,pivot,pmode,method,workr,mark,rindex,pntr)
      if (pcol.eq.0)goto 900
      pivot=1.0d+0/pivot
      diag(pcol)=pivot
      ccfree=endmem-pntc(clast)-ccol(clast)

c compress column file

      if(ccfree.lt.mn)then
        call lpccomprs(mn,cfree,ccfree,endmem,nz,
     x  pntc,ccol,cfirst,cpermf,rowidx,nonz,code)
        if(code.lt.0)goto 999
      endif

c remove pcol from the cpermf lists

      prewcol=cpermb(pcol)
      o=cpermf(pcol)
      if(prewcol.ne.0)then
        cpermf(prewcol)=o
      else
        cfirst=o
      endif
      if(o.eq.0)then
        clast=prewcol
      else
        cpermb(o)=prewcol
      endif

c remove pcol from the rpermf lists

      prewcol=rpermb(pcol)
      o=rpermf(pcol)
      if(prewcol.ne.0)then
        rpermf(prewcol)=o
      else
        rfirst=o
      endif
      if(o.eq.0)then
        rlast=prewcol
      else
        rpermb(o)=prewcol
      endif

c administration

      pivotn=pivotn+1
      pivcols(pivotn)=pcol
      pcnt=ccol(pcol)+crow(pcol)

c remove pcol from the counter lists

      o=cnext(pcol)
      ii=cprew(pcol)
      if(ii.eq.0)then
        cpnt(pcnt+1)=o
      else
        cnext(ii)=o
      endif
      if(o.ne.0)cprew(o)=ii
      pnt1=pntc(pcol)
      pnt2=pnt1+ccol(pcol)-1
      if(pnt1.gt.nz)then
        ppnt1=endmem-pcnt
        ppnt2=ppnt1+pcnt-1
        endmem=endmem-pcnt
        ccfree=ccfree-pcnt
        pnt=ppnt1
        do 60 j=pnt1,pnt2
          o=rowidx(j)
          mark(o)=1
          workr(o)=nonz(j)
          rowidx(pnt)=o
          pnt=pnt+1

c remove pcol from the row file

          rfill(o)=-1
          cfill(o)=ccol(o)
          l=pntr(o)
          p=l+crow(o)-2
          do 55 k=l,p
            if(rindex(k).eq.pcol)then
              rindex(k)=rindex(p+1)
              goto 60
            endif
  55      continue
  60    continue
        pntc(pcol)=ppnt1

c create pivot column from the row file

        pnt1=pntr(pcol)
        pnt2=pnt1+crow(pcol)-1
        do 70 i=pnt1,pnt2
          o=rindex(i)
          l=pntc(o)
          p=l+ccol(o)-1

c move the original column

          if(l.le.nz)then
            if(ccfree.lt.mn)then
              call lpccomprs(mn,cfree,ccfree,endmem,nz,
     x        pntc,ccol,cfirst,cpermf,rowidx,nonz,code)
              if(code.lt.0)goto 999
              l=pntc(o)
              p=l+ccol(o)-1
            endif
            ccfree=ccfree-ccol(o)
            j=pntc(clast)+ccol(clast)
            if(j.le.nz)j=nz+1
            pntc(o)=j
            do 72 k=l,p
              nonz(j)=nonz(k)
              rowidx(j)=rowidx(k)
              j=j+1
  72        continue
            l=pntc(o)
            p=j-1

c update the cpermf lists

            prewcol=cpermb(o)
            k=cpermf(o)
            if(prewcol.ne.0)then
              cpermf(prewcol)=k
            else
              if(k.ne.0)then
                cfirst=k
              else
                goto 93
              endif
            endif
            if(k.eq.0)then
              clast=prewcol
            else
              cpermb(k)=prewcol
            endif
            cpermf(clast)=o
            cpermb(o)=clast
            cpermf(o)=0
            clast=o
          endif
  93      continue   ! rlt

c find element and move in the column o

          cfill(o)=ccol(o)-1
          rfill(o)= 0
          do 75 k=l,p
            if(rowidx(k).eq.pcol)then
              mark(o)=1
              rowidx(pnt)=o
              pnt=pnt+1
              workr(o)=nonz(k)
              rowidx(k)=rowidx(p)
              nonz(k)=nonz(p)
              goto 70
            endif
  75      continue
  70    continue
      else
        ppnt1=pnt1
        ppnt2=pnt2
        do 65 j=pnt1,pnt2
          o=rowidx(j)
          mark(o)=1
          workr(o)=nonz(j)

c remove pcol from the row file

          rfill(o)=-1
          cfill(o)=ccol(o)
          l=pntr(o)
          p=l+crow(o)-2
          do 67 k=l,p
            if(rindex(k).eq.pcol)then
              rindex(k)=rindex(p+1)
              goto 65
            endif
  67      continue
  65    continue
      endif
      ccol(pcol)=pcnt

c remove columns from the counter lists

      do 77 j=ppnt1,ppnt2
        i=rowidx(j)
        o=cnext(i)
        ii=cprew(i)
        if(ii.eq.0)then
          cpnt(crow(i)+ccol(i)+1)=o
        else
          cnext(ii)=o
        endif
        if(o.ne.0)cprew(o)=ii
  77  continue

c sort pivot column, set-up workr

      if(ppnt1.lt.ppnt2)call lphpsort((ppnt2-ppnt1+1),rowidx(ppnt1))
      do p=ppnt1,ppnt2
        nonz(p)=workr(rowidx(p))
        workr(rowidx(p))=workr(rowidx(p))*pivot
      enddo

c elimination loop

      do 80 p=ppnt1,ppnt2
        i=rowidx(p)
        ss=nonz(p)

c transforme diag and delete element from mark

        diag(i)=diag(i)-ss*workr(i)
        mark(i)=0
        pcnt=pcnt-1

c transformation on the column i

        fill=pcnt
        pnt1=pntc(i)
        pnt2=pnt1+cfill(i)-1
        do 90 k=pnt1,pnt2
           o=rowidx(k)
           if(mark(o).ne.0)then
             nonz(k)=nonz(k)-ss*workr(o)
             fill=fill-1
             mark(o)=0
           endif
  90    continue

c compute the free space

        ii=cpermf(i)
        if(ii.eq.0)then
          k=endmem-pnt2-1
        else
          k=pntc(ii)-pnt2-1
        endif

c move column to the end of the column file

        if(fill.gt.k)then
          if (ccfree.lt.mn)then
            call lpccomprs(mn,cfree,ccfree,endmem,nz,
     x      pntc,ccol,cfirst,cpermf,rowidx,nonz,code)
            if(code.lt.0)goto 999
            pnt1=pntc(i)
            pnt2=pnt1+cfill(i)-1
          endif
          if(i.ne.clast)then
            l=pntc(clast)+ccol(clast)
            pntc(i)=l
            do 95 k=pnt1,pnt2
              rowidx(l)=rowidx(k)
              nonz(l)=nonz(k)
              l=l+1
  95        continue
            pnt1=pntc(i)
            pnt2=l-1
            prewcol=cpermb(i)
            if(prewcol.eq.0)then
              cfirst=ii
            else
              cpermf(prewcol)=ii
            endif
            cpermb(ii)=prewcol
            cpermf(clast)=i
            cpermb(i)=clast
            clast=i
            cpermf(clast)=0
          endif
        endif

c create fill in

        do 97 k=p+1,ppnt2
          o=rowidx(k)
          if(mark(o).eq.0)then
            mark(o)=1
          else
            pnt2=pnt2+1
            nonz(pnt2)=-ss*workr(o)
            rowidx(pnt2)=o
            rfill(o)=rfill(o)+1
          endif
   97   continue
        pnt2=pnt2+1
        ccol(i)=pnt2-pnt1
        if(i.eq.clast)then
          ccfree=endmem-pnt2-1
        endif
  80  continue

c make space for fills in the row file

      do 100 j=ppnt1,ppnt2
        i=rowidx(j)

c update the counter lists

        fill=ccol(i)+crow(i)+rfill(i)+1
        o=cpnt(fill)
        cnext(i)=o
        cpnt(fill)=i
        if(o.ne.0)cprew(o)=i
        cprew(i)=0
        pnt2=pntr(i)+crow(i)-1

c compute the free space

        ii=rpermf(i)
        if(ii.eq.0)then
          k=rfree-pnt2-1
        else
          k=pntr(ii)-pnt2-1
        endif

c move row to the end of the row file

        if(k.lt.rfill(i))then
          if(rcfree.lt.mn)then
            call lprcomprs(mn,rfree,
     x      rcfree,pntr,crow,rfirst,rpermf,rindex,code)
            if(code.lt.0)goto 999
          endif
          if(ii.ne.0)then
            pnt1=pntr(i)
            pnt2=pnt1+crow(i)-1
            pnt=pntr(rlast)+crow(rlast)
            pntr(i)=pnt
            do 110 l=pnt1,pnt2
              rindex(pnt)=rindex(l)
              pnt=pnt+1
 110        continue

c update the rperm lists

            prewcol=rpermb(i)
            if(prewcol.eq.0)then
              rfirst=ii
            else
              rpermf(prewcol)=ii
            endif
            rpermb(ii)=prewcol
            rpermf(rlast)=i
            rpermb(i)=rlast
            rlast=i
            rpermf(rlast)=0
          endif
        endif
        crow(i)=crow(i)+rfill(i)
        if(i.eq.rlast)rcfree=rfree-crow(i)-pntr(i)
 100  continue

c make pointers to the end of the filled rows

      do 120 j=ppnt1,ppnt2
        rfill(rowidx(j))=pntr(rowidx(j))+crow(rowidx(j))-1
 120  continue

c generate fill in the row file

      do 130 j=ppnt1,ppnt2
        o=rowidx(j)
        pnt1=pntc(o)+cfill(o)
        pnt2=pntc(o)+ccol(o)-1
        do 140 k=pnt1,pnt2
          rindex(rfill(rowidx(k)))=o
          rfill(rowidx(k))=rfill(rowidx(k))-1
 140    continue
 130  continue

c end of the pivot loop

      goto 50

c compute the 'superdense' columns, enter in pass=2

 900  if(pass.eq.1)then
        pass=pass+1
        tpiv2=tltmp1
        tabs=tltmp2
        pmode=1
        call lpccomprs(mn,cfree,ccfree,endmem,nz,
     x  pntc,ccol,cfirst,cpermf,rowidx,nonz,code)
        if(code.lt.0)goto 999
        call lpexcols(rowidx,nonz,rpermf,rpermb,crow,
     x  pntc,ccol,pivcols,cpermf,cpermb,workr,colpnt,diag,
     x  cfirst,clast,endmem,ccfree,minm,code)
        if(code.lt.0)goto 999
        goto 500
      endif

c rank check

      if(pivotn.lt.mn-fixn-dropn)then
        i=cfirst
 910    if (i.gt.0)then
          crow(i)=-2
          if(i.le.n)then
            w1=w1+1
            if(w1.le.wignore)then
              write(buff,4)i,diag(i)
              call lpmprnt(buff)
            endif
            fixn=fixn+1
          else
            w1=w1+1
            if(w1.le.wignore)then
              write(buff,3)(i-n),diag(i),slktyp(i-n)
              call lpmprnt(buff)
            endif
            actual(i)=-1
            dropn=dropn+1
          endif
          i=cpermf(i)
          goto 910
        endif
        active=mn-pivotn
        w1=w1-wignore
        if(w1.gt.0)then
          write(buff,'(1x,a,i5)')'warnings ignored:',w1
          call lpmprnt(buff)
        endif
      endif

c repermut

      do 955 i=1,mn
        mark(i)=mn+1
        pntr(i)=0
 955  continue
      do 915 i=1,pivotn
        mark(pivcols(i))=i
 915  continue
      fill=0
      oper=0.0d+0
      do 920 i=1,mn
        if(crow(i).le.-2)goto 920
        pnt1=pntc(i)
        if(pnt1.le.nz)goto 920
        if(ccol(i).gt.0)then
          pnt2=pnt1+ccol(i)-1
          do j=pnt1,pnt2
            workr(rowidx(j))=nonz(j)
          enddo
          call lphpsrt(ccol(i),mn,rowidx(pnt1),mark)
          do j=pnt1,pnt2
            nonz(j)=workr(rowidx(j))
          enddo
        endif
        fill=fill+ccol(i)
        oper=oper+dble(ccol(i)*ccol(i)+ccol(i))/2.0d+0
 920  continue
      do 950 i=1,n
        if(crow(i).le.-2)goto 950
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        k=pnt2-pnt1+1
        if(k.gt.0)then
          do j=pnt1,pnt2
            workr(rowidx(j))=nonz(j)
          enddo
          call lphpsrt(k,mn,rowidx(pnt1),mark)
          do j=pnt1,pnt2
            nonz(j)=workr(rowidx(j))
          enddo
        endif
        if(pntc(i).lt.nz)then
          ccol(i)=k
          fill=fill+ccol(i)
          oper=oper+dble(ccol(i)*ccol(i)+ccol(i))/2.0d+0
        endif
 950  continue

c create the counter inta1 for the minor iterations

      do 960 i=1,pivotn
        col=pivcols(pivotn-i+1)
        if(ccol(col).eq.0)goto 960
        pntr(col)=pntr(rowidx(pntc(col)))+ccol(col)
 960  continue

c modify ccol   ( counter ->> pointer )

      do 970 i=1,pivotn
        col=pivcols(i)
        ccol(col)=pntc(col)+ccol(col)-1
 970  continue

c end of ffactor

      if(fnzmin.gt.fill)fnzmin=fill
      if(fnzmax.lt.fill)fnzmax=fill
      write(buff,6)fill
      call lpmprnt(buff)
      write(buff,7)oper
      call lpmprnt(buff)
      if(method.eq.1)tfind=-tfind
 999  return
      end subroutine lpffactor

c ===========================================================================

      subroutine lpccomprs(mn,cfree,ccfree,endmem,nz,
     x pnt,count,cfirst,cpermf,rowidx,nonz,code)
      integer*4 mn,cfree,ccfree,endmem,nz,pnt(mn),rowidx(cfree),
     x count(mn),cpermf(mn),cfirst,code
      real*8 nonz(cfree)

      integer*4 i,j,pnt1,pnt2,pnt0
      character*99 buff
c ---------------------------------------------------------------------------
   2  format(' not enough memory detected in subroutine ccompress')
      pnt0=nz+1
      i=cfirst
  40  if(i.le.0)goto 30
        pnt1=pnt(i)
        if(pnt1.lt.pnt0)goto 10
        if(pnt1.eq.pnt0)then
          pnt0=pnt0+count(i)
          goto 10
        endif
        pnt(i)=pnt0
        pnt2=pnt1+count(i)-1
        do 20 j=pnt1,pnt2
          rowidx(pnt0)=rowidx(j)
          nonz(pnt0)=nonz(j)
          pnt0=pnt0+1
  20    continue
  10    i=cpermf(i)
      goto 40
  30  ccfree=endmem-pnt0-1
      if(ccfree.lt.mn)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
      endif

      end subroutine lpccomprs

c ===========================================================================

      subroutine lprcomprs(mn,rfree,rcfree,pnt,count,rfirst,
     x rpermf,rindex,code)
      integer*4 mn,rfree,rcfree,pnt(mn),count(mn),rfirst,rpermf(mn),
     x rindex(rfree),code

      integer*4 i,j,ppnt,pnt1,pnt2
      character*99 buff

c ---------------------------------------------------------------------------

   2  format(' not enough memory detected in subroutine rcompress')
      ppnt=1
      i=rfirst
   5  if(i.eq.0)goto 20
      pnt1=pnt(i)
      if(ppnt.eq.pnt1)then
         ppnt=ppnt+count(i)
         goto 15
      endif
      pnt2=pnt1+count(i)-1
      pnt(i)=ppnt
      do 10 j=pnt1,pnt2
        rindex(ppnt)=rindex(j)
        ppnt=ppnt+1
  10  continue
  15  i=rpermf(i)
      goto 5
  20  rcfree=rfree-ppnt
      if(rcfree.lt.mn)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
      endif

      end subroutine lprcomprs

c ==========================================================================

      subroutine lpexcols(rowidx,nonz,rpermf,rpermb,crow,
     x pntc,ccol,pivcols,cpermf,cpermb,workr,colpnt,diag,
     x cfirst,clast,endmem,ccfree,minm,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 rowidx(cfree),rpermf(mn),rpermb(mn),crow(mn),
     x pntc(mn),ccol(mn),pivcols(mn),cpermf(mn),cpermb(mn),colpnt(n1),
     x cfirst,clast,endmem,ccfree,minm,code
      real*8 nonz(cfree),workr(mn),diag(mn)

      integer*4 i,j,k,l,o,prewcol,pnt1,pnt2,ppnt1,ppnt2
      real*8 ss
      character*99 buff

c ----------------------------------------------------------------------------

   2  format(' not enough memory in the column file ')

      do i=1,mn
        rpermf(i)=0
        rpermb(i)=0
        if(crow(i).gt.-2)crow(i)=0
      enddo
      do i=1,pivotn
        crow(pivcols(i))=-1
      enddo
      prewcol=0
      do 200 i=1,n
        if(crow(i).ne.minm)goto 200
        if(prewcol.eq.0)prewcol=i
        ppnt1=colpnt(i)
        ppnt2=colpnt(i+1)-1

c update column's permut list

        if(clast.ne.0)then
          pnt1=pntc(clast)+ccol(clast)
          cpermf(clast)=i
        else
          cfirst=i
          pnt1=0
        endif
        cpermb(i)=clast
        cpermf(i)=0
        clast=i
        if(pnt1.lt.nz)pnt1=nz+1
        pntc(i)=pnt1
        pnt2=pnt1

c repack the original column

        do 202 j=ppnt1,ppnt2
          k=rowidx(j)
          if(crow(k).gt.-2)then
            workr(k)=nonz(j)
            rpermf(k)=1
            if(crow(k).eq.-1)then
              rpermb(k)=rpermb(k)+1
            endif
            rowidx(pnt2)=k
            pnt2=pnt2+1
          endif
 202    continue

c ftran on the column

        do j=1,pivotn
          o=pivcols(j)
          if(rpermf(o).gt.0)then
            ppnt1=pntc(o)
            ppnt2=ppnt1+ccol(o)-1
            ss=-workr(o)*diag(o)
            diag(i)=diag(i)+ss*workr(o)
            do k=ppnt1,ppnt2
              l=rowidx(k)
              if(rpermf(l).eq.0)then
                workr(l)=nonz(k)*ss
                rowidx(pnt2)=l
                pnt2=pnt2+1
                rpermf(l)=1
                if((crow(l).eq.-1).or.(l.lt.i))then
                  rpermb(l)=rpermb(l)+1
                endif
              else
                workr(l)=workr(l)+nonz(k)*ss
              endif
            enddo
          endif
        enddo

c augftr with the prewious columns

        j=prewcol
 215    if(j.ne.i)then
          ppnt1=pntc(j)
          ppnt2=ppnt1+ccol(j)-1
          do k=ppnt1,ppnt2
            l=rowidx(k)
            if((crow(l).eq.-1).and.(rpermf(l).gt.0))then
              if(rpermf(j).eq.0)then
                workr(j)=-workr(l)*nonz(k)*diag(l)
                rowidx(pnt2)=j
                pnt2=pnt2+1
                rpermf(j)=1
                rpermb(j)=rpermb(j)+1
              else
                workr(j)=workr(j)-workr(l)*nonz(k)*diag(l)
              endif
            endif
          enddo
          j=cpermf(j)
          goto 215
        endif
        ccol(i)=pnt2-pnt1

c pack the column

        pnt2=pnt2-1
        do j=pnt1,pnt2
          o=rowidx(j)
          nonz(j)=workr(o)
          rpermf(o)=0
        enddo
        ccfree=endmem-pntc(clast)-ccol(clast)
        if(ccfree.lt.mn)then
          write(buff,2)
          call lpmprnt(buff)
          code=-2
          goto 999
        endif
        crow(i)=0
 200  continue

c make space in the old factors

      o=0
      do i=1,pivotn
        j=pivcols(i)
        o=o+rpermb(j)
      enddo
      ppnt1=endmem-o
      if(ccfree.le.o)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif
      endmem=ppnt1
      ccfree=ccfree-o
      do i=pivotn,1,-1
        k=pivcols(i)
        pnt1=pntc(k)
        if(pnt1.gt.nz)then
          pnt2=pnt1+ccol(k)-1
          pntc(k)=ppnt1
          do j=pnt1,pnt2
            rowidx(ppnt1)=rowidx(j)
            nonz(ppnt1)=nonz(j)
            ppnt1=ppnt1+1
          enddo
          ppnt1=ppnt1+rpermb(k)
        endif
      enddo

c make space in the active submatrix

      o=0
      i=cfirst
 220  if(i.ne.0)then
        o=o+rpermb(i)
        i=cpermf(i)
        goto 220
      endif
      if(ccfree.le.o)then
        write(buff,2)
        call lpmprnt(buff)
        code=-2
        goto 999
      endif
      ccfree=ccfree-o
      ppnt1=pntc(clast)+ccol(clast)+o
      i=clast
  230 if(i.ne.0)then
        pnt1=pntc(i)
        if(pnt1.gt.nz)then
          pnt2=pnt1+ccol(i)-1
          ppnt1=ppnt1-rpermb(i)
          do j=pnt2,pnt1,-1
            rowidx(ppnt1)=rowidx(j)
            nonz(ppnt1)=nonz(j)
            ppnt1=ppnt1-1
          enddo
          pntc(i)=ppnt1+1
        endif
        i=cpermb(i)
        goto 230
      endif

c store the dense columns in the final positions

      i=prewcol
 250  if(i.gt.0)then
        pnt1=pntc(i)
        pnt2=pnt1+ccol(i)-1
        ppnt1=pnt1
        do j=pnt1,pnt2
          o=rowidx(j)
          if((crow(o).eq.-1).or.(o.lt.i))then
            k=pntc(o)+ccol(o)
            nonz(k)=nonz(j)
            rowidx(k)=i
            ccol(o)=ccol(o)+1
          else
            nonz(ppnt1)=nonz(j)
            rowidx(ppnt1)=rowidx(j)
            ppnt1=ppnt1+1
          endif
        enddo
        ccol(i)=ppnt1-pnt1
        i=cpermf(i)
        goto 250
      endif

c compute crow

      do i=1,mn
        if(crow(i).gt.-2)crow(i)=0
      enddo
      i=cfirst
  280 if(i.gt.0)then
        pnt1=pntc(i)
        pnt2=pnt1+ccol(i)-1
        do j=pnt1,pnt2
          crow(rowidx(j))=crow(rowidx(j))+1
        enddo
        i=cpermf(i)
        goto 280
      endif
999   return
      end subroutine lpexcols

c =============================================================================
c =============================================================================

      subroutine lprngchk(rowidx,nonzeros,pnt1,pnt2,
     x  vcstat,rb,diag,slktyp,dropn,col,dv,dia,w1,odia)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      integer*4 pnt1,pnt2,rowidx(cfree),vcstat(mn),rb,
     x slktyp(m),dropn,col
      real*8    nonzeros(cfree),diag(mn),dv(m),dia,odia

      integer*4 i,j,w1,wignore
      character*99 buff

c --------------------------------------------------------------------------

      wignore=5
      rb=0
      if(col.le.n)then
        if(diag(col).lt.0)then
          dia=-1.0d+12
          odia=odia+1.0d+00/dia
        else
          dia=+1.0d+12
          odia=odia+1.0d+00/dia
        endif
      else
        dia=0.0d+0

c check for modification columns

        do 10 i=pnt1,pnt2
          j=rowidx(i)
          if((vcstat(j).le.-2).or.(j.gt.n))goto 10
          if(abs(nonzeros(i)).lt.tzer)goto 10
ccc             dia=+1.0+10
ccc             odia=odia+1.0d+00/dia
          rb=1
          vcstat(col)=-1
          goto 20
 10     continue

c dependent row, relax only if the dual variable is zero !

        if(abs(dv(col-n)).lt.tzer)then
          vcstat(col)=-2
          dropn=dropn+1
          w1=w1+1
          if(w1.le.wignore)then
            write(buff,'(1x,a,i5,a,i6)')
     x      'warning :  row dropped ',col-n,'  type:',slktyp(col-n)
            call lpmprnt(buff)
          endif
        endif
      endif
 20   return
      end subroutine lprngchk

c ==========================================================================
c ===========================================================================

      subroutine lprowact(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x coln,collst,colmrk,rown,rowlst,rowmrk,
     x cnum,list,mrk,procn,
     x ppbig,pmaxr,pmbig,pminr,
     x lbig,tfeas,code)

c this subroutine removes singleton rows and may fixes variables

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,list(m),mrk(m),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x coln,rown,collst(n),rowlst(n),colmrk(n),rowmrk(m),
     x ppbig(m),pmbig(m),code

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),addobj,lbig,tfeas

      integer*4 i,j,k,l,row,col,dir,setdir,p,p1,p2,red,crem,rrem
      real*8 upper,lower,pivot,eps
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      rrem=0
      crem=0
  10  if(cnum.ge.1)then
        row=list(1)
        mrk(row)=-1
        list(1)=list(cnum)
        cnum=cnum-1

        if(ppbig(row).le.0)then
          upper=pmaxr(row)-rhs(row)
        else
          upper=lbig
        endif
        if(pmbig(row).le.0)then
          lower=pminr(row)-rhs(row)
        else
          lower=-lbig
        endif

c check feasibility

        eps=abs(rhs(row)+1.0d+0)*tfeas
        if((lower-ups(row).gt.eps) .or.
     x     (los(row)-upper.gt.eps))then
          cnum=-row-n
          code=4
          goto 100
        endif

c check redundancy

        setdir=0
        red=0
        if((los(row)-lower.lt.eps) .and.
     x     (upper-ups(row).lt.eps))then
          red=1
        endif
        if(ups(row)-lower.lt.eps)then
          red=1
          setdir=-1
        else if(upper-los(row).lt.eps)then
          red=1
          setdir=1
        endif



        if(red.gt.0)then
          prelen=prelen+1
          prehis(prelen)=row+n
          rowsta(row)=-2-procn
          traf=.false.
          call lpremove(n,m,nz,row,rowidx,rownzs,colidx,colnzs,
     x    rowbeg,rowend,colbeg,colend,obj,pivot,traf)
          rrem=rrem+1
          if(setdir.eq.0)then
            j=rowbeg(row)
            k=rowend(row)
            do i=j,k
              l=rowidx(i)
              if(colmrk(l).lt.0)then
                coln=coln+1
                collst(coln)=l
              endif
              colmrk(l)=procn
            enddo
          else
            dir=-1
            traf=.true.
            j=rowbeg(row)
            k=rowend(row)
            do i=j,k
              col=rowidx(i)
              if(rownzs(i)*dble(setdir).gt.0.0d+0)then
                pivot=upb(col)
              else
                pivot=lob(col)
              endif
              p1=colbeg(col)
              p2=colend(col)
              call lpchgmxm(p1,p2,upb(col),lob(col),colidx,colnzs,
     x        ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
              addobj=addobj+pivot*obj(col)
              lob(col)=pivot
              upb(col)=pivot
              prelen=prelen+1
              prehis(prelen)=col
              colsta(col)=-2-procn
              call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x        colbeg,colend,rowbeg,rowend,rhs,pivot,traf)
              crem=crem+1
              p1=colbeg(col)
              p2=colend(col)
              do p=p1,p2
                l=colidx(p)
                if(rowmrk(l).lt.0)then
                  rown=rown+1
                  rowlst(rown)=l
                endif
                rowmrk(l)=procn
                if(mrk(l).lt.0)then
                  mrk(l)=procn
                  cnum=cnum+1
                  list(cnum)=l
                endif
              enddo
            enddo
          endif
        endif
        goto 10
      endif

 100  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'rowact:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif

      end subroutine lprowact

c ===========================================================================
c ===========================================================================

      subroutine lprowsng(n,m,mn,nz,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x upb,lob,ups,los,
     x rhs,obj,addobj,colsta,rowsta,prelen,prehis,
     x coln,collst,colmrk,rown,rowlst,rowmrk,
     x cnum,list,mrk,procn,
     x ppbig,pmaxr,pmbig,pminr,
     x lbig,tfeas,tzer,code)

c this subroutine removes singleton rows and may fixes variables

      integer*4 n,m,mn,nz,colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),cnum,list(m),mrk(m),
     x colsta(n),rowsta(m),prehis(mn),procn,prelen,
     x coln,rown,collst(n),rowlst(n),colmrk(n),rowmrk(m),
     x ppbig(m),pmbig(m),code

      real*8 colnzs(nz),rownzs(nz),upb(n),lob(n),ups(m),los(m),
     x rhs(m),obj(n),pmaxr(m),pminr(m),addobj,lbig,tfeas,tzer

      integer*4 i,l,row,col,dir,crem,rrem
      real*8 ub,lb,upper,lower,sol,pivot
      logical traf
      character*99 buff

c ---------------------------------------------------------------------------

      rrem=0
      crem=0
  10  if(cnum.ge.1)then
        row=list(1)
        mrk(row)=-1
        list(1)=list(cnum)
        cnum=cnum-1
        if(rowbeg(row).eq.rowend(row))then

c remove singleton row

          col=rowidx(rowbeg(row))
          pivot=rownzs(rowbeg(row))
          traf=.false.
          call lpremove(n,m,nz,row,rowidx,rownzs,colidx,colnzs,
     x    rowbeg,rowend,colbeg,colend,obj,lower,traf)
          rrem=rrem+1
          prelen=prelen+1
          prehis(prelen)=row+n
          rowsta(row)=-2-procn

c calculate new bounds (ub,lb)

          if(ups(row).lt.lbig)then
            ub=rhs(row)+ups(row)
          else
            ub=ups(row)
          endif
          if(los(row).gt.-lbig)then
            lb=rhs(row)+los(row)
          else
            lb=los(row)
          endif
          if(pivot.gt.0)then
            if(ub.lt.lbig)ub=ub/pivot
            if(lb.gt.-lbig)lb=lb/pivot
          else
            if(ub.lt.lbig)then
              sol=ub/pivot
            else
              sol=-ub
            endif
            if(lb.gt.-lbig)then
              ub=lb/pivot
            else
              ub=-lb
            endif
            lb=sol
          endif

c update

          upper=upb(col)
          lower=lob(col)
          dir=-1
          call lpchgmxm(colbeg(col),colend(col),upper,lower,colidx,
     x    colnzs,ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
          if(lb.gt.lower)lower=lb
          if(ub.lt.upper)upper=ub

c check primal feasibility

          if((lower-upper).gt.((abs(lower)+1.0d+0)*tfeas))then
            cnum=-col
            code=4
            goto 100
          endif

c check for fix variable

          if((upper-lower).lt.((abs(lower)+1.0d+0)*tzer))then
            prelen=prelen+1
            prehis(prelen)=col
            colsta(col)=-2-procn
            traf=.true.
            call lpremove(m,n,nz,col,colidx,colnzs,rowidx,rownzs,
     x      colbeg,colend,rowbeg,rowend,rhs,lower,traf)
            crem=crem+1
            addobj=addobj+obj(col)*lower
            do i=colbeg(col),colend(col)
              l=colidx(i)
              if((mrk(l).lt.0).and.(rowbeg(l).eq.rowend(l)))then
                mrk(l)=procn
                cnum=cnum+1
                list(cnum)=l
              endif
            enddo
          else

c update bounds

            dir=1
            call lpchgmxm(colbeg(col),colend(col),upper,lower,colidx,
     x      colnzs,ppbig,pmaxr,pmbig,pminr,lbig,dir,m)
          endif
          lob(col)=lower
          upb(col)=upper


c update search lists

          do i=colbeg(col),colend(col)
            l=colidx(i)
            if(rowmrk(l).lt.0)then
              rown=rown+1
              rowlst(rown)=l
            endif
            rowmrk(l)=procn
          enddo
          if(colsta(col).gt.-2)then
            if (colmrk(col).lt.0)then
              coln=coln+1
              collst(coln)=col
            endif
            colmrk(col)=procn
          endif
        endif
        goto 10
      endif

 100  if(rrem+crem.gt.0)then
        write(buff,'(1x,a,i5,a,i5,a)')
     x  'rowsng:',crem,' columns,',rrem,' rows removed'
        call lpmprnt(buff)
      endif

      end subroutine lprowsng

c ===========================================================================
c ===========================================================================

      subroutine lpmscale(colpnt,rowidx,nonzeros,
     x obj,rhs,ubound,vcstat,scale,scalen,scalmet,scpass,scdiff,
     x ddsup,ddsupn,dxs,snhead)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 colpnt(n1),rowidx(nz),vcstat(mn),
     x scalmet,scpass,snhead(mn)
      real*8    nonzeros(cfree),obj(n),rhs(m),ubound(mn),scale(mn),
     x scalen(mn),scdiff,ddsup(mn),ddsupn(mn),dxs(mn)

      integer*4 i
      character*99 buff

      write(buff,'(1x)')
      call lpmprnt(buff)
      write(buff,'(1x,a)')'process: scaling'
      call lpmprnt(buff)

      do i=1,mn
        scalen(i)=1.0d+0
      enddo

      if((scalmet.eq.2).or.(scalmet.eq.4))then
        call lpscale1(ubound,nonzeros,colpnt,obj,scalen,vcstat,
     x  rowidx,rhs,ddsup,scpass,scdiff,snhead,nonzeros(nz+1))
      endif
      if((scalmet.eq.3).or.(scalmet.eq.5))then
        call lpscale2(ubound,nonzeros,colpnt,obj,scalen,vcstat,
     x  rowidx,rhs,scpass,scdiff,ddsup,ddsupn,dxs,snhead)
      endif
      if((scalmet.gt.0).and.(scalmet.le.3))then
        call lpsccol2(ubound,nonzeros,colpnt,obj,scalen,
     x  vcstat,rowidx)
        call lpscrow2(rhs,ubound,nonzeros,rowidx,colpnt,ddsup,
     x  scalen,vcstat)
      endif

      do i=1,mn
        scale(i)=scale(i)*scalen(i)
      enddo

      write(buff,'(1x,a)')'scaling done...'
      call lpmprnt(buff)

      end subroutine lpmscale

c ============================================================================

      subroutine lpscale1(bounds,rownzs,colpnt,obj,scale,
     x vcstat,rowidx,rhs,work1,scpass,scdif,veclen,
     x lognz)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      real*8 bounds(mn),rownzs(cfree),obj(n),scale(mn),
     x  rhs(m),work1(mn),scdif,lognz(nz)
      integer*4 rowidx(cfree),colpnt(n1),vcstat(mn),scpass,veclen(mn)

      real*8 defic,odefic
      integer*4 pass,i,j,pnt1,pnt2,nonz
      character*99 buff

      pass=0
      nonz=0
      defic= 1.0d+0
      odefic=0.0d+0
      do i=1,mn
        veclen(i)=0
      enddo
      do i=1,n
        if(vcstat(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if((abs(rownzs(j)).gt.tzer).and.
     x      (vcstat(rowidx(j)+n).gt.-2))then
              lognz(j)=log(abs(rownzs(j)))
              veclen(i)=veclen(i)+1
              veclen(rowidx(j)+n)=veclen(rowidx(j)+n)+1
              nonz=nonz+1
              odefic=odefic+abs(lognz(j))
            else
              lognz(j)=0.0d+0
            endif
          enddo
        endif
      enddo
      do i=1,mn
        if(veclen(i).eq.0)veclen(i)=1
        scale(i)=0.0d+0
      enddo
      if(nonz.eq.0)goto 999
      odefic=exp(odefic/dble(nonz))
      if(odefic.le.scdif)goto 999
  10  write(buff,'(1x,a,i2,a,1p,d12.5)')'pass',pass,'. average def.',
     &                                  odefic
      call lpmprnt(buff)
      call lpsccol1(colpnt,scale,
     x vcstat,rowidx,veclen,lognz)
      pass=pass+1
      call lpscrow1(rowidx,colpnt,work1,scale,vcstat,defic,veclen,lognz)
      defic=exp(defic/dble(nonz))
      if(defic.le.scdif)goto 999
      if(pass.ge.scpass)goto 999
      if(odefic.le.defic)goto 999
      odefic=defic
      goto 10
 999  write(buff,'(1x,a,i2,a,1p,d12.5)')'pass',pass,'. average def.',
     &                                  defic
      call lpmprnt(buff)

c scaling

      do i=1,mn
        scale(i)=exp(scale(i))
      enddo
      do i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do j=pnt1,pnt2
          rownzs(j)=rownzs(j)/scale(i)/scale(rowidx(j)+n)
        enddo
        obj(i)=obj(i)/scale(i)
        bounds(i)=bounds(i)*scale(i)
      enddo
      do i=1,m
        rhs(i)=rhs(i)/scale(i+n)
        bounds(i+n)=bounds(i+n)/scale(i+n)
      enddo

      end subroutine lpscale1

c ============================================================================

      subroutine lpscrow1(rowidx,colpnt,
     x maxi,scale,excld,ss,veclen,lognz)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 lognz(nz),maxi(mn),scale(mn),ss
      integer*4 rowidx(cfree),colpnt(n1),excld(mn),veclen(mn)
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c ---------------------------------------------------------------------------
      integer*4 i,j,pnt1,pnt2
      real*8 sol
c ---------------------------------------------------------------------------
      ss=0
      do i=1,m
        maxi(i)=0.0d+0
      enddo
      do i=1,n
        if(excld(i).gt.-2)then
          sol=scale(i)
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if(excld(rowidx(j)+n).gt.-2)then
              maxi(rowidx(j))=maxi(rowidx(j))+lognz(j)-sol
              ss=ss+abs(lognz(j)-sol-scale(rowidx(j)+n))
            endif
          enddo
        endif
      enddo
      do i=1,m
        scale(n+i)=maxi(i)/veclen(i+n)
      enddo

      end subroutine lpscrow1

c ===========================================================================

      subroutine lpsccol1(colpnt,scale,
     x excld,rowidx,veclen,lognz)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 scale(mn),lognz(nz)
      integer*4 colpnt(n1),excld(mn),rowidx(cfree),veclen(mn)
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c ---------------------------------------------------------------------------
      integer*4 i,j,pnt1,pnt2
      real*8 ma
c ---------------------------------------------------------------------------
      do i=1,n
        ma=0.0d+0
        if(excld(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            ma=ma+lognz(j)-scale(rowidx(j)+n)
          enddo
          scale(i)=ma/veclen(i)
        endif
      enddo

      end subroutine lpsccol1

c ===========================================================================

      subroutine lpscrow2(rhs,bounds,rownzs,rowidx,
     x colpnt,maxi,scale,excld)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      real*8 rownzs(cfree),bounds(mn),rhs(m),maxi(m),scale(mn)
      integer*4 rowidx(cfree),colpnt(n1),excld(mn)
c ---------------------------------------------------------------------------
      integer*4 i,j,pnt1,pnt2,k
      real*8 sol
c ---------------------------------------------------------------------------
      do i=1,m
        maxi(i)=0
      enddo
      do i=1,n
        if(excld(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            k=rowidx(j)
            sol=abs(rownzs(j))
            if (maxi(k).lt.sol)maxi(k)=sol
          enddo
        endif
      enddo
      do i=1,m
        if(maxi(i).le.tzer)maxi(i)=1.0d+0
        scale(n+i)=maxi(i)*scale(n+i)
        rhs(i)=rhs(i)/maxi(i)
        bounds(i+n)=bounds(i+n)/maxi(i)
      enddo
      do i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do j=pnt1,pnt2
          k=rowidx(j)
          rownzs(j)=rownzs(j)/maxi(k)
        enddo
      enddo

      end subroutine lpscrow2

c ===========================================================================


      subroutine lpsccol2(bounds,rownzs,colpnt,obj,scale,
     x excld,rowidx)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 rownzs(cfree),bounds(mn),obj(n),scale(mn)
      integer*4 colpnt(n1),excld(mn),rowidx(cfree)
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer
c ---------------------------------------------------------------------------
      integer*4 i,j,pnt1,pnt2
      real*8 sol,ma
c ---------------------------------------------------------------------------
      do i=1,n
        if(excld(i).gt.-2)then
          ma=0
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if(excld(rowidx(j)+n).gt.-2)then
              sol=abs(rownzs(j))
              if (ma.lt.sol)ma=sol
            endif
          enddo
          if (ma.le.tzer)ma=1.0d+0
          scale(i)=ma*scale(i)
          do j=pnt1,pnt2
            rownzs(j)=rownzs(j)/ma
          enddo
          obj(i)=obj(i)/ma
          bounds(i)=bounds(i)*ma
        endif
      enddo

      end subroutine lpsccol2

c ===========================================================================

      subroutine lpscalobj(obj,scobj,excld,objnor)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 obj(n),scobj,objnor
      integer*4 excld(n),i
      character*99 buff
c ---------------------------------------------------------------------------
      scobj=0.0d+0
      do i=1,n
        if(excld(i).gt.-2)then
          if (abs(obj(i)).gt.scobj)scobj=abs(obj(i))
        endif
      enddo
      scobj=scobj/objnor
      if(scobj.lt.1.0d-08)scobj=1.0d-08
      write(buff,'(1x,a,1p,d8.1)')'obj. scaled ',scobj
      call lpmprnt(buff)
      do i=1,n
        obj(i)=obj(i)/scobj
      enddo

      end subroutine lpscalobj

c ===========================================================================

      subroutine lpscalrhs(rhs,scrhs,excld,rhsnor,bounds,xs,up )

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8 rhs(m),scrhs,rhsnor,bounds(mn),xs(mn),up(mn)
      integer*4 excld(mn),i
      character*99 buff
c ---------------------------------------------------------------------------
      scrhs=0.0d+0
      do i=1,m
        if(excld(i+n).gt.-2)then
          if(abs(rhs(i)).gt.scrhs)scrhs=abs(rhs(i))
        endif
      enddo
      scrhs=scrhs/rhsnor
      if(scrhs.lt.1.0d-08)scrhs=1.0d-08
      write(buff,'(1x,a,1p,d8.1)')'rhs. scaled ',scrhs
      call lpmprnt(buff)
      do i=1,m
        rhs(i)=rhs(i)/scrhs
      enddo
      do i=1,mn
        bounds(i)=bounds(i)/scrhs
        xs(i)=xs(i)/scrhs
        up(i)=up(i)/scrhs
      enddo

      end subroutine lpscalrhs

c ============================================================================
c curtis-reid scaling algorithm
c ============================================================================

      subroutine lpscale2(bounds,rownzs,colpnt,obj,sc,
     x  vcstat,rowidx,rhs,scpass,scdif,scm1,rk,logsum,count)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpnumer/ tplus,tzer
      real*8        tplus,tzer

      real*8 bounds(mn),rownzs(cfree),obj(n),sc(mn),
     x  rhs(m),scdif,scm1(mn),rk(mn),logsum(mn)
      integer*4 rowidx(cfree),colpnt(n1),vcstat(mn),scpass,count(mn)

      integer*4 i,j,in,pnt1,pnt2,pass
      real*8 logdef,s,qk,qkm1,ek,ekm1,ekm2,sk,skm1
      character*99 buff

      pass=0
      do i=1,mn
       count(i)=0
       logsum(i)=0.0d+0
      enddo
      logdef=0.0d+0
      in=0
      do i=1,n
        if(vcstat(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            if(vcstat(rowidx(j)+n).gt.-2)then
              if(abs(rownzs(j)).gt.tzer)then
                s=log(abs(rownzs(j)))
                count(rowidx(j)+n)=count(rowidx(j)+n)+1
                count(i)=count(i)+1
                logsum(i)=logsum(i)+s
                logsum(rowidx(j)+n)=logsum(rowidx(j)+n)+s
                logdef=logdef+s*s
                in=in+1
              endif
            endif
          enddo
        endif
      enddo
      do i=1,mn
       if((vcstat(i).le.-2).or.(count(i).eq.0))count(i)=1
      enddo
      logdef=sqrt(logdef)/dble(in)
      logdef=exp(logdef)
      write(buff,'(1x,a,i2,a,1p,d12.5)')'pass',pass,'. average def.',
     &                                  logdef
      call lpmprnt(buff)
      if(logdef.le.scdif)then
        do i=1,mn
          sc(i)=1.0d+0
        enddo
        goto 999
      endif

c initialize

      do i=1,m
        sc(i+n)=logsum(i+n)/count(i+n)
        rk(i+n)=0
      enddo
      sk=0
      do i=1,n
        if(vcstat(i).gt.-2)then
          s=logsum(i)
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            s=s-logsum(rowidx(j)+n)/count(rowidx(j)+n)
          enddo
        else
          s=0
        endif
        rk(i)=s
        sk=sk+s*s/count(i)
        sc(i)=0.0d+0
      enddo
      do i=1,mn
        scm1(i)=sc(i)
      enddo
      ekm1=0
      ek=0
      qk=1.0d+0

c curtis-reid scaling

  10  pass=pass+1
        do i=1,m
          rk(i+n)=ek*rk(i+n)
        enddo
        do i=1,n
          if(vcstat(i).gt.-2)then
            pnt1=colpnt(i)
            pnt2=colpnt(i+1)-1
            s=rk(i)/count(i)
            do j=pnt1,pnt2
              if(vcstat(rowidx(j)+n).gt.-2)
     x        rk(rowidx(j)+n)=rk(rowidx(j)+n)+s
            enddo
          endif
        enddo
        skm1=sk
        sk=0.0d+0
        do i=1,m
          rk(i+n)=-rk(i+n)/qk
          sk=sk+rk(i+n)*rk(i+n)/count(i+n)
        enddo
        ekm2=ekm1
        ekm1=ek
        ek=qk*sk/skm1
        qkm1=qk
        qk=1-ek
        if(pass.gt.scpass)goto 20

c update column-scale factors

        do i=1,n
          if(vcstat(i).gt.-2)then
            s=sc(i)
            sc(i)=s+(rk(i)/count(i)+ekm1*ekm2*(s-scm1(i)))/qk/qkm1
            scm1(i)=s
          endif
        enddo

c even pass

        do i=1,n
          if(vcstat(i).gt.-2)then
            s=ek*rk(i)
            pnt1=colpnt(i)
            pnt2=colpnt(i+1)-1
            do j=pnt1,pnt2
              if(vcstat(rowidx(j)+n).gt.-2)
     x        s=s+rk(rowidx(j)+n)/count(rowidx(j)+n)
            enddo
            s=-s/qk
          else
            s=0
          endif
          rk(i)=s
        enddo
        skm1=sk
        sk=0.0d+0
        do i=1,n
          sk=sk+rk(i)*rk(i)/count(i)
        enddo
        ekm2=ekm1
        ekm1=ek
        ek=qk*sk/skm1
        qkm1=qk
        qk=1-ek

c update row-scale factors

        do i=1,m
          j=i+n
          if(vcstat(j).gt.-2)then
            s=sc(j)
            sc(j)=s+(rk(j)/count(j)+ekm1*ekm2*(s-scm1(j)))/qk/qkm1
            scm1(j)=s
          endif
        enddo
      goto 10

c syncronize column factors

  20  do i=1,n
        if(vcstat(i).gt.-2)then
          sc(i)=sc(i)+(rk(i)/count(i)+ekm1*ekm2*(sc(i)-scm1(i)))/qkm1
        endif
      enddo

c scaling

      logdef=0
      do i=1,mn
        if(vcstat(i).gt.-2)then
          sc(i)=exp(sc(i))
        else
          sc(i)=1.0d+0
        endif
      enddo
      do i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do j=pnt1,pnt2
          rownzs(j)=rownzs(j)/sc(i)/sc(rowidx(j)+n)
          if((vcstat(rowidx(j)+n).gt.-2).and.
     x      (abs(rownzs(j)).gt.tzer))then
            s=log(abs(rownzs(j)))
            logdef=logdef+s*s
          endif
        enddo
        obj(i)=obj(i)/sc(i)
        bounds(i)=bounds(i)*sc(i)
      enddo
      do i=1,m
        rhs(i)=rhs(i)/sc(i+n)
        bounds(i+n)=bounds(i+n)/sc(i+n)
      enddo
      logdef=sqrt(logdef)/dble(in)
      logdef=exp(logdef)
      pass=pass-1
      write(buff,'(1x,a,i2,a,1p,d12.5)')'pass',pass,'. average def.',
     &                                  logdef
      call lpmprnt(buff)
 999  return
      end subroutine lpscale2

c ============================================================================
c ===========================================================================

      subroutine lpstlamb(colpnt,vcstat,rowidx,cnt,fixn,dropn,p)

      implicit   none

      integer*4 colpnt(n1),vcstat(mn),rowidx(nz),cnt(mn),
     x          fixn,dropn,p

      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      real*8         tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens
      common/lpfactor/ tpiv1,tpiv2,tabs,trabs,lam,tfind,order,supdens

      real*8         maxdense,densgap
      integer*4      setlam,denslen
      common/lpsetden/ maxdense,densgap,setlam,denslen

      integer*4 i,j,pnt1,pnt2,cn,lcn,lcd,ndn,z,maxcn
      real*8    la
      character*99 buff

c ---------------------------------------------------------------------------


      write(buff,'(1x)')
      call lpmprnt(buff)
      do i=1,m
        cnt(i)=0
      enddo
      if((m-dropn).ge.(n-fixn))then
        cnt(1)=m-dropn-n+fixn
      endif
      maxcn=0
      do i=1,n
        if(vcstat(i).gt.-2)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          cn=0
          do j=pnt1,pnt2
            if(vcstat(rowidx(j)).gt.-2)cn=cn+1
          enddo
          if(cn.gt.0)cnt(cn)=cnt(cn)+1
          vcstat(i)=cn
          if(maxcn.lt.cn)maxcn=cn
        endif
      enddo
      if(setlam.lt.0)goto 70

      cn =maxcn
      lcd=maxcn
      lcn=maxcn
      z=0
      pnt1=(n-fixn+m-dropn)*maxdense
      pnt2=0
      if((m-dropn).ge.1.5*(n-fixn))then
        maxdense=1.0
      endif
      if((m-dropn).ge.2.5*(n-fixn))then
        lcn=1
        lcd=2
        goto 60
      endif

      do while ((pnt2.le.pnt1).and.(cn.gt.0))
        if(cnt(cn).eq.0)then
          z=z+1
        else
          if(z.gt.0)then
            if((densgap*cn*cn).le.(cn+z+1)*(cn+z+1))then
              lcd=cn+z+1
              lcn=cn
              ndn=pnt2
            endif
            z=0
          endif
          pnt2=pnt2+cnt(cn)
        endif
        cn=cn-1
      enddo

  60  write(buff,'(1x,a,i6)')'largest sparse column length :',lcn
      call lpmprnt(buff)
      if((maxcn.le.denslen).or.(lcn.eq.maxcn))then
        write(buff,'(1x,a)')'problem has no dense columns'
        call lpmprnt(buff)
        lcn=maxcn
      else
        write(buff,'(1x,a,i6)')'smallest dense column length :',lcd
        call lpmprnt(buff)
        write(buff,'(1x,a,i6)')'number of dense columns      :',ndn
        call lpmprnt(buff)
      endif
      la=lcn+0.5
      la=la/m
      write(buff,'(1x,a,f7.4)')'computed density parameter   : ',la
      call lpmprnt(buff)
      if(la.gt.lam)then
        lam=la
      else
        write(buff,'(1x,a,f7.4)') 'parameter reset to value    : ',lam
        call lpmprnt(buff)
      endif
  70  lam=lam*m
      p=1
      if((lam.ge.maxcn).and.(setlam.le.0))p=2
      if(supdens.le.lam)supdens=lam

      write(buff,'(1x)')
      call lpmprnt(buff)

      end subroutine lpstlamb

c ===========================================================================

c numerically more stable version

c ===========================================================================

       subroutine  lpsparser(n,n1,m,nz,colpnt,
     x colbeg,colend,colidx,colnzs,
     x rowbeg,rowend,rowidx,rownzs,
     x colsta,rowsta,rhs,slktyp,
     x mark,rflag,tplus,tzer,redtol,reltol,abstol)

      integer*4 n,n1,m,nz,colpnt(n1),colbeg(n),colend(n),colidx(nz),
     x rowbeg(m),rowend(m),rowidx(nz),colsta(n),rowsta(m),
     x rflag(m),mark(n),slktyp(m)
      real*8 colnzs(nz),rownzs(nz),rhs(m),
     x tplus,tzer,redtol,reltol,abstol

      integer*4 i,j,k,pnt1,pnt2,rpnt1,rpnt2,row,col,prow,pcol,
     x pnt,ppnt1,ppnt2,elim,total,totaln,iw,rowlen
      real*8 pivot,nval,tol
      character*99 buff

c ---------------------------------------------------------------------------

      total=0
      totaln=0
      tol=1.0d+0/reltol
      do i=1,m
        if(rowsta(i).gt.-2)then
          rflag(i)=0
          totaln=totaln+rowend(i)-rowbeg(i)+1
        else
          rflag(i)=2
        endif
      enddo
      do i=1,n
        mark(i)=0
      enddo

 100  elim=0
      do 20 row=1,m
        if((rflag(row).lt.2).and.(slktyp(row).eq.0))then
          iw=rflag(row)
          pnt1=rowbeg(row)
          pnt2=rowend(row)
          rowlen=pnt2-pnt1

c select the shortest column

          col=0
          k=m+1
          do j=pnt1,pnt2
            i=rowidx(j)
            mark(i)=j
            if(colend(i)-colbeg(i).lt.k)then
              col=i
              k=colend(i)-colbeg(i)
            endif
          enddo
          if(col.eq.0)then
            rflag(row)=1
            goto 20
          endif

c scan the selected column

          ppnt1=colbeg(col)
          ppnt2=colend(col)
          do 30 i=ppnt1,ppnt2
            prow=colidx(i)
            rpnt1=rowbeg(prow)
            rpnt2=rowend(prow)
            if((rowlen.gt.rpnt2-rpnt1).or.(iw+rflag(prow).ge.2).or.
     x      (row.eq.prow))goto 30
            k=-1
            do pnt=rpnt1,rpnt2
              if(mark(rowidx(pnt)).gt.0)k=k+1
            enddo
            if(k.ne.rowlen)goto 30

c select pivot

            pcol=0
            pivot=tol
            do pnt=rpnt1,rpnt2
              if(mark(rowidx(pnt)).gt.0)then
                if(abs(rownzs(mark(rowidx(pnt)))).gt.abstol)then
                  nval=-rownzs(pnt)/rownzs(mark(rowidx(pnt)))
                  if(abs(nval).lt.abs(pivot))then
                    pivot=nval
                    pcol=rowidx(pnt)
                  endif
                endif
              endif
            enddo
            if(pcol.eq.0)goto 20

c transformation

            rflag(prow)=0
            rhs(prow)=rhs(prow)+pivot*rhs(row)
            do pnt=rpnt1,rpnt2
              if(mark(rowidx(pnt)).gt.0)then
                nval=rownzs(pnt)+pivot*rownzs(mark(rowidx(pnt)))
                if(abs(nval).lt.tplus*(abs(rownzs(pnt))))nval=0.0d+0
                rownzs(pnt)=nval
              endif
            enddo
            do while (rpnt1.le.rpnt2)
              if(abs(rownzs(rpnt1)).lt.tzer)then
                k=rowidx(rpnt1)
                rownzs(rpnt1)=rownzs(rpnt2)
                rowidx(rpnt1)=rowidx(rpnt2)
                rownzs(rpnt2)=0.0d+0
                rowidx(rpnt2)=k
                rpnt2=rpnt2-1
                elim=elim+1
              else
                rpnt1=rpnt1+1
              endif
            enddo
            rowend(prow)=rpnt2
  30      continue
          do j=pnt1,pnt2
            mark(rowidx(j))=0
          enddo
          rflag(row)=1
        endif
  20  continue
      total=total+elim
      totaln=totaln-elim
      if(dble(elim)/(dble(totaln)+1.0d+0).gt.redtol)goto 100

c making modification in the column file

      if(total.gt.0)then
        do i=1,n
          mark(i)=colbeg(i)-1
        enddo
        do i=1,m
          if(rowsta(i).gt.-2)then
            pnt1=rowbeg(i)
            pnt2=rowend(i)
            do j=pnt1,pnt2
              col=rowidx(j)
              mark(col)=mark(col)+1
              colidx(mark(col))=i
              colnzs(mark(col))=rownzs(j)
            enddo
          endif
        enddo
        pnt=colpnt(1)
        do i=1,n
          iw=pnt
          if(colsta(i).gt.-2)then
            pnt1=colbeg(i)
            pnt2=mark(i)
            colbeg(i)=pnt
            do j=pnt1,pnt2
              colnzs(pnt)=colnzs(j)
              colidx(pnt)=colidx(j)
              pnt=pnt+1
            enddo
            pnt1=colend(i)+1
            colend(i)=pnt-1
          else
            pnt1=colpnt(i)
          endif
          pnt2=colpnt(i+1)-1
          do j=pnt1,pnt2
            colnzs(pnt)=colnzs(j)
            colidx(pnt)=colidx(j)
            pnt=pnt+1
          enddo
          colpnt(i)=iw
        enddo
        colpnt(n+1)=pnt
      endif

      write(buff,'(1x,i5,a)')total,' nonzeros eliminated'
      call lpmprnt(buff)
      return
      end subroutine  lpsparser

c ===========================================================================
c 6 way loop unrolling

c ============================================================================

      subroutine lpcspnode(firstc,lastc,diag,nonzeros,
     x fpnt,count,pivots,knz,dia)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 firstc,lastc,fpnt(mn),count(mn),pivots(mn)
      real*8 diag(mn),nonzeros(cfree),knz(mn),dia

      integer*4 pnt11,pnt12,pnt13,pnt14,pnt15,pnt16,
     x col1,col2,col3,col4,col5,col6,frs,j,pnt2
      real*8    s1,s2,s3,s4,s5,s6

c compute

      frs=firstc

  99  if(lastc-2-frs) 98,30,97
  98  if(lastc-frs) 999,10,20
  97  if(lastc-4-frs) 40,50,60



  60  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      col5=pivots(frs+4)
      col6=pivots(frs+5)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt15=fpnt(col5)
      pnt16=fpnt(col6)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      fpnt(col5)=pnt15+1
      fpnt(col6)=pnt16+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      s5=-nonzeros(pnt15)*diag(col5)
      s6=-nonzeros(pnt16)*diag(col6)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4+
     x    nonzeros(pnt15)*s5+nonzeros(pnt16)*s6
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4+
     x  nonzeros(pnt15+j)*s5+nonzeros(pnt16+j)*s6
      enddo
      frs=frs+6
      goto 99



  50  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      col5=pivots(frs+4)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt15=fpnt(col5)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      fpnt(col5)=pnt15+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      s5=-nonzeros(pnt15)*diag(col5)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4+
     x    nonzeros(pnt15)*s5
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4+
     x  nonzeros(pnt15+j)*s5
      enddo
      goto 999



  40  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4
      enddo
      goto 999



  30  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3
      enddo
      goto 999



  20  col1=pivots(frs)
      col2=pivots(frs+1)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2
      enddo
      goto 999



  10  col1=pivots(frs)
      pnt11=fpnt(col1)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      s1=-nonzeros(pnt11)*diag(col1)
      dia=dia+
     x    nonzeros(pnt11)*s1
      do j=1,pnt2
        knz(j)=knz(j)+
     x  nonzeros(pnt11+j)*s1
      enddo

 999  return
      end subroutine lpcspnode

c ==========================================================================
c 6 way loop unrolling

c ============================================================================

      subroutine lpcspnd(firstc,lastc,diag,nonzeros,
     x fpnt,count,pivots,knz,dia,index)

      common/dims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 firstc,lastc,fpnt(mn),count(mn),pivots(mn),index(mn)
      real*8 diag(mn),nonzeros(cfree),knz(mn),dia

      integer*4 pnt11,pnt12,pnt13,pnt14,pnt15,pnt16,
     x col1,col2,col3,col4,col5,col6,frs,j,pnt2
      real*8    s1,s2,s3,s4,s5,s6

c compute

      frs=firstc

  99  if(lastc-2-frs) 98,30,97
  98  if(lastc-frs) 999,10,20
  97  if(lastc-4-frs) 40,50,60



  60  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      col5=pivots(frs+4)
      col6=pivots(frs+5)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt15=fpnt(col5)
      pnt16=fpnt(col6)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      fpnt(col5)=pnt15+1
      fpnt(col6)=pnt16+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      s5=-nonzeros(pnt15)*diag(col5)
      s6=-nonzeros(pnt16)*diag(col6)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4+
     x    nonzeros(pnt15)*s5+nonzeros(pnt16)*s6
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4+
     x  nonzeros(pnt15+j)*s5+nonzeros(pnt16+j)*s6
      enddo
      frs=frs+6
      goto 99



  50  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      col5=pivots(frs+4)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt15=fpnt(col5)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      fpnt(col5)=pnt15+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      s5=-nonzeros(pnt15)*diag(col5)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4+
     x    nonzeros(pnt15)*s5
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4+
     x  nonzeros(pnt15+j)*s5
      enddo
      goto 999



  40  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      col4=pivots(frs+3)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt14=fpnt(col4)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      fpnt(col4)=pnt14+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      s4=-nonzeros(pnt14)*diag(col4)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3+nonzeros(pnt14)*s4
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3+nonzeros(pnt14+j)*s4
      enddo
      goto 999



  30  col1=pivots(frs)
      col2=pivots(frs+1)
      col3=pivots(frs+2)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt13=fpnt(col3)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      fpnt(col3)=pnt13+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      s3=-nonzeros(pnt13)*diag(col3)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2+
     x    nonzeros(pnt13)*s3
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2+
     x  nonzeros(pnt13+j)*s3
      enddo
      goto 999



  20  col1=pivots(frs)
      col2=pivots(frs+1)
      pnt11=fpnt(col1)
      pnt12=fpnt(col2)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      fpnt(col2)=pnt12+1
      s1=-nonzeros(pnt11)*diag(col1)
      s2=-nonzeros(pnt12)*diag(col2)
      dia=dia+
     x    nonzeros(pnt11)*s1+nonzeros(pnt12)*s2
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1+nonzeros(pnt12+j)*s2
      enddo
      goto 999



  10  col1=pivots(frs)
      pnt11=fpnt(col1)
      pnt2=count(col1)-pnt11
      fpnt(col1)=pnt11+1
      s1=-nonzeros(pnt11)*diag(col1)
      dia=dia+
     x    nonzeros(pnt11)*s1
      do j=1,pnt2
        knz(index(j))=knz(index(j))+
     x  nonzeros(pnt11+j)*s1
      enddo

 999  return
      end subroutine lpcspnd

c ==========================================================================
c ===========================================================================

      subroutine lpsupnode(ecolpnt,count,rowidx,vcstat,pivots,
     x snhead,invperm,nodtyp)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lpsprnod/     psupn,ssupn,maxsnz
      integer*4          psupn,ssupn,maxsnz

      integer*4 ecolpnt(mn),count(mn),rowidx(cfree),vcstat(mn),
     x pivots(mn),snhead(mn),invperm(mn),nodtyp(mn)

      integer*4 i,j,k,l,i1,i2,ppnt1,ppnt2,pnt1,pnt2,pcol,col,snmode
      integer*4 sn1,sn2,ss1,ss2,supnz
      character*99 buff

   1  format(1x,'supernodes       :',i12,'   ',i12)
   2  format(1x,'supernodal cols. :',i12,'   ',i12)
   3  format(1x,'dense window     :',i12)

      do i=1,mn
        snhead(i)=0
        invperm(i)=0
        nodtyp(mn)=0
      enddo
      do i=1,pivotn
        invperm(pivots(i))=i
      enddo
      sn1=0
      sn2=0
      ss1=0
      ss2=0
      pnt1=1
      pnt2=0
      i=0
  10  i=i+1
      if(i.le.pivotn)then
        pcol=pivots(i)
        if(vcstat(pcol).gt.-2)then
          j=0
          ppnt1=ecolpnt(pcol)
          ppnt2=count(pcol)
          k=i+1
          snmode=1
          supnz=pnt2-pnt1+1
  20      if((k.le.pivotn).and.(ppnt1.le.ppnt2))then
            col=pivots(k)
            pnt1=ecolpnt(col)
            pnt2=count(col)
            supnz=supnz+pnt2-pnt1+1
            if(((ppnt2-ppnt1-pnt2+pnt1).eq.1).and.(supnz.lt.maxsnz))then
              if(col.ne.rowidx(ppnt1))goto 30
              i2=ppnt1+1
              i1=pnt1
  40          if(i1.le.pnt2)then
                if(rowidx(i1).ne.rowidx(i2))goto 30
                i1=i1+1
                i2=i2+1
                goto 40
              endif
              k=k+1
              ppnt1=ppnt1+1
              goto 20
            endif
          endif
  30      if(k.eq.i+1)then
            snmode=-1
            supnz=pnt2-pnt1+1
  25        if((k.le.pivotn).and.(ppnt1.le.ppnt2))then
              col=pivots(k)
              pnt1=ecolpnt(col)
              pnt2=count(col)
              supnz=supnz+pnt2-pnt1+1
              if((ppnt2-ppnt1.eq.pnt2-pnt1).and.(supnz.le.maxsnz))then
                i2=ppnt1
                i1=pnt1
  45            if(i1.le.pnt2)then
                  if(rowidx(i1).ne.rowidx(i2))goto 35
                  i1=i1+1
                  i2=i2+1
                  goto 45
                endif
                k=k+1
                goto 25
              endif
            endif
          endif
  35      if(snmode.eq.1)then
            denwin=k-i
            if((k-i).lt.psupn)goto 10
            sn1=sn1+1
            ss1=ss1+(k-i)
            j=sn1
          else
            if((k-i).lt.ssupn)goto 10
            sn2=sn2+1
            ss2=ss2+(k-i)
            j=-sn2
          endif
          do l=i,k-1
            snhead(l)=j
            nodtyp(l)=j
          enddo
          i=k-1
        endif
        goto 10
      endif
      write(buff,1)sn1,sn2
      call lpmprnt(buff)
      write(buff,2)ss1,ss2
      call lpmprnt(buff)
      write(buff,3)denwin
      call lpmprnt(buff)
      k=0
      do i=pivotn,1,-1
        if(snhead(i).ne.0)then
          if(k.ne.snhead(i))then
            j=i
            k=snhead(i)
          endif
          snhead(i)=j
        else
          k=0
        endif
      enddo

      end subroutine lpsupnode

c ============================================================================
c  update supernode partitions after  column fixing
c (only in the sparse part of the constraint matrix)
c =============================================================================

      subroutine lpsupupd(pivots,invperm,snhead,nodtyp,vcstat,
     x ecolpnt)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 pivots(mn),invperm(mn),snhead(mn),nodtyp(mn),
     x ecolpnt(mn),vcstat(mn)

      integer*4 i,j,k

c make changes : compress pivots,nodetyp,snhead

      i=1
      j=0
  10  if(i.le.pivotn)then
        k=pivots(i)
        if((ecolpnt(k).gt.nz).or.(vcstat(k).gt.-2))then
          j=j+1
          pivots(j)=pivots(i)
          snhead(j)=snhead(i)
          nodtyp(j)=nodtyp(i)
        endif
        invperm(i)=j
        i=i+1
        goto 10
      endif
      pivotn=j

c change snhead

      do j=1,pivotn
        if(snhead(j).gt.0)snhead(j)=invperm(snhead(j))
      enddo

c create new invperm

      do j=1,pivotn
        invperm(pivots(j))=j
      enddo

      end subroutine lpsupupd

c =============================================================================
c ==========================================================================

      subroutine lpsymfact (pivots,rowidx,ecolpnt,count,
     x vcstat,list,next,work,mark,fill,code)


      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 pivots(mn),ecolpnt(mn),count(mn),vcstat(mn),
     x list(mn),next(mn),work(mn),mark(mn),rowidx(cfree),code

      integer*4 i,ii,j,k,l,pnt1,pnt2,fnz,kprew,fill
      character*99 buff

c --------------------------------------------------------------------------

      fnz=nz+1

      do 10 i=1,mn
        list(i)=0
        next(i)=0
        mark(i)=0
        work(i)=mn+1
  10  continue
      do 15 i=1,pivotn
        work(pivots(i))=i
  15  continue
      do 20 i=1,n
        if(vcstat(i).le.-2)goto 20
        j=rowidx(ecolpnt(i))
        next(i)=list(j)
        list(j)=i
  20  continue

      do 50 ii=1,pivotn
        i=pivots(ii)
        mark(i)=1
        if(i.le.n)goto 50
        l=fnz
        ecolpnt(i)=fnz
        kprew=list(i)
  60    if(kprew.eq.0)goto 70
        pnt1=ecolpnt(kprew)
        pnt2=count(kprew)
        if(fnz.ge.cfree-m)then
          write(buff,'(1x,a)')'not enough memory'
          call lpmprnt(buff)
          code=-2
          goto 999
        endif
        do j=pnt1,pnt2
          k=rowidx(j)
          if(mark(k).eq.0)then
            mark(k)=1
            rowidx(fnz)=k
            fnz=fnz+1
          endif
        enddo
        kprew=next(kprew)
        goto 60

  70    do j=l,fnz-1
          mark(rowidx(j))=0
        enddo
        count(i)=fnz-1
        k=fnz-l
        if(k.gt.0)then
          call lphpsrt(k,mn,rowidx(l),work)
          j=rowidx(l)
          next(i)=list(j)
          list(j)=i
        endif
  50  continue
      fill=fnz-nz-1
 999  return
      end subroutine lpsymfact

c ===========================================================================

      subroutine lptransps(n,m,nz,colpnt,rowidx,colnz,
     x           rowpnt,colindex,rownz,perm)

      integer*4 n,m,nz,colpnt(n+1),rowidx(nz),rowpnt(m+1),
     x          colindex(nz),perm(n)
      real*8    colnz(nz),rownz(nz)


      integer*4 i,j,k,pnt1,pnt2,ii

c ---------------------------------------------------------------------------

      do 10 i=1,m+1
        rowpnt(i)=0
  10  continue
      do 20 i=1,n
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do 30 j=pnt1,pnt2
          k=rowidx(j)
          rowpnt(k)=rowpnt(k)+1
  30    continue
  20  continue

      j=rowpnt(1)
      k=1
      do 40 i=1,m
        rowpnt(i)=k
        k=k+j
        j=rowpnt(i+1)
  40  continue

      do 50 ii=1,n
        i=perm(ii)
        pnt1=colpnt(i)
        pnt2=colpnt(i+1)-1
        do 60 j=pnt1,pnt2
          k=rowidx(j)
          colindex(rowpnt(k))=i
          rownz(rowpnt(k))=colnz(j)
          rowpnt(k)=rowpnt(k)+1
  60    continue
  50  continue

      do 70 i=1,m
        rowpnt(m-i+2)=rowpnt(m-i+1)
  70  continue
      rowpnt(1)=1

      end subroutine lptransps

c =========================================================================

      subroutine lphpsrt(n,mn,iarr,index)

      integer*4 n,mn,iarr(n),index(mn)

      integer*4 i,j,l,ir,rra

c ---------------------------------------------------------------------------

      l=n/2+1
      ir=n
  10  if(l.gt.1)then
         l=l-1
         rra=iarr(l)
      else
        rra=iarr(ir)
        iarr(ir)=iarr(1)
        ir=ir-1
        if(ir.le.1)then
          iarr(1)=rra
          goto 999
        endif
      endif
      i=l
      j=l+l
  20  if(j-ir)40,50,60
  40  if(index(iarr(j)).lt.index(iarr(j+1)))j=j+1
  50  if(index(rra).lt.index(iarr(j)))then
        iarr(i)=iarr(j)
        i=j
        j=j+j
      else
        j=ir+1
      endif
      goto 20
  60  iarr(i)=rra
      goto 10
 999  if(n.gt.0)then
        if(index(iarr(n)).gt.mn)then
          n=n-1
          goto 999
        endif
      endif

      end subroutine lphpsrt

c ===========================================================================
c ==========================================================================

      subroutine lpnewsmf(colpnt,pivots,rowidx,cnonz,ecolpnt,count,
     x vcstat,invprm,snhead,nodtyp,mark,workr,list,prew,next,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      common/lplogprt/ loglog,lfile
      integer*4      loglog,lfile

      integer*4 pivots(mn),ecolpnt(mn),count(mn),vcstat(mn),
     x invprm(mn),snhead(mn),nodtyp(mn),mark(mn),rowidx(cfree),
     x colpnt(n1),list(mn),prew(mn),next(mn),code
      real*8 cnonz(nz),workr(mn)

      integer*4 i,ii,j,k,l,o,pnt1,pnt2,fnz,kprew
      character*99 buff

c --------------------------------------------------------------------------

      fnz=nz+1

c restructuring the ordering

      k=0
      l=mn+1
      do i=1,pivotn
        j=pivots(i)
        if(vcstat(j).gt.-1)then
          k=k+1
          invprm(k)=j
        else if(vcstat(j).eq.-1)then
          l=l-1
          invprm(l)=j
        endif
      enddo

      write(buff,'(1x,a,i5,a)')
     x 'instable pivot(s), correcting',(mn-l+1),' pivot position(s)'
      call lpmprnt(buff)

      do i=1,k
        pivots(i)=invprm(i)
      enddo
      pivotn=k
      do i=l,mn
        pivotn=pivotn+1
        pivots(pivotn)=invprm(i)
      enddo

c reorder the matrix

      do 10 i=1,mn
        invprm(i)=0
        snhead(i)=0
        mark(i)=0
        nodtyp(i)=mn+1
        next(i)=0
        prew(i)=0
        list(i)=0
  10  continue
      do i=1,pivotn
        nodtyp(pivots(i))=i
      enddo
      do ii=1,pivotn
        i=pivots(ii)
        if(i.le.n)then
          pnt1=colpnt(i)
          pnt2=colpnt(i+1)-1
          k=pnt2-pnt1+1
          if(k.gt.0)then
            do j=pnt1,pnt2
              workr(rowidx(j))=cnonz(j)
            enddo
            call lphpsrt(k,mn,rowidx(pnt1),nodtyp)
            do j=pnt1,pnt2
              cnonz(j)=workr(rowidx(j))
            enddo
          endif
          ecolpnt(i)=pnt1
  15      if((pnt1.le.pnt2).and.(vcstat(rowidx(pnt2)).le.-2))then
            pnt2=pnt2-1
            goto 15
          endif
          count(i)=pnt2
          if(pnt1.le.pnt2)then
            j=rowidx(pnt1)
            o=list(j)
            next(i)=o
            list(j)=i
            if(o.ne.0)prew(o)=i
            prew(i)=0
          endif
        endif
      enddo

      do 50 ii=1,pivotn
        i=pivots(ii)
        mark(i)=1
        if(i.le.n)then

c remove i from the secondary list

          if(ecolpnt(i).le.count(i))then
            k=next(i)
            l=prew(i)
            if(k.gt.0)then
              prew(k)=l
            endif
            if(l.gt.0)then
              next(l)=k
            else
              list(rowidx(ecolpnt(i)))=k
            endif
          endif

c simple column of a

          if(invprm(i).eq.0)then
            l=ecolpnt(i)
            k=count(i)-l+1
            goto 72
          endif

c transformed column of a

          pnt1=ecolpnt(i)
          pnt2=count(i)
          l=fnz
          ecolpnt(i)=fnz
          if(fnz.ge.cfree-mn)then
            write(buff,'(1x,a)')'not enough memory'
            call lpmprnt(buff)
            code=-1
            goto 999
          endif
          do j=pnt1,pnt2
            mark(rowidx(j))=1
            rowidx(fnz)=rowidx(j)
            fnz=fnz+1
          enddo
          goto 59
        endif

c create nonzero pattern

        l=fnz
        ecolpnt(i)=fnz
        if(fnz.ge.cfree-mn)then
          write(buff,'(1x,a)')'not enough memory'
          call lpmprnt(buff)
          code=-2
          goto 999
        endif
        kprew=list(i)
  25    if(kprew.eq.0)goto 59
        k=next(kprew)
        mark(kprew)=1
        rowidx(fnz)=kprew
        fnz=fnz+1
        pnt1=ecolpnt(kprew)+1
        pnt2=count(kprew)
        ecolpnt(kprew)=pnt1
        if(pnt1.le.pnt2)then
          j=rowidx(pnt1)
          o=list(j)
          next(kprew)=o
          list(j)=kprew
          if(o.ne.0)prew(o)=kprew
          prew(kprew)=0
        endif
        kprew=k
        goto 25

c build new column structure

  59    kprew=invprm(i)
  60    if(kprew.eq.0)goto 70
        pnt1=ecolpnt(kprew)
        pnt2=count(kprew)
        do j=pnt1,pnt2
          k=rowidx(j)
          if(mark(k).eq.0)then
            mark(k)=1
            rowidx(fnz)=k
            fnz=fnz+1
          endif
        enddo
        kprew=snhead(kprew)
        goto 60

c linking invperms, free working arrays

  70    do j=l,fnz-1
          mark(rowidx(j))=0
        enddo
        count(i)=fnz-1
        k=fnz-l
        if(k.gt.1)call lphpsrt(k,mn,rowidx(l),nodtyp)
  72    if(k.gt.0)then
          j=rowidx(l)
          snhead(i)=invprm(j)
          invprm(j)=i
        endif
  50  continue

c end of creation of nonzero pattern, set up new supernode partitions

      k=loglog
      loglog=0
      call lpsupnode(ecolpnt,count,rowidx,vcstat,pivots,snhead,
     x  invprm,nodtyp)
      loglog=k
 999  return

      end subroutine lpnewsmf

c =========================================================================
c ===========================================================================

      subroutine lpsymmfo(inta1,pivots,ecolpnt,vcstat,
     x colpnt,rowidx,rowpnt,colindex,perm,invperm,
     x count,inta2,inta3,inta4,inta5,inta6,inta7,inta8,inta9,
     x inta10,inta11,nonzeros,l,oper,tfind,inta12,code)

      common/lpdims/ n,n1,m,mn,nz,cfree,pivotn,denwin,rfree
      integer*4    n,n1,m,mn,nz,cfree,pivotn,denwin,rfree

      integer*4 inta1(mn),ecolpnt(mn),pivots(mn),vcstat(mn),
     x colpnt(n1),rowidx(cfree),rowpnt(mn),colindex(rfree),
     x perm(mn),invperm(mn),count(mn),inta2(mn),inta3(mn),inta4(mn),
     x inta5(mn),inta6(mn),inta7(mn),inta8(mn),inta9(mn),inta10(mn),
     x inta11(mn),tfind,inta12(mn),l,code

      real*8   nonzeros(cfree),oper

      integer*4 i,j,k,t1,tt1,t2,p1,p2,pnt,pnt1,pnt2,aatnz
      character*99 buff

c ---------------------------------------------------------------------------

    1 format(1x,'building aat                 time:',f9.2,' sec')
    2 format(1x,'building ordering    list    time:',f9.2,' sec')
    4 format(1x,'symbolic factorisation       time:',f9.2,' sec')
    5 format(1x,'total symbolic phase         time:',f9.2,' sec')
    6 format(1x,'sub-diagonal nonzeros in aat     :',i9)
    7 format(1x,'sub-diagonal nonzeros in l       :',i9)
    8 format(1x,'nonzeros         :',i12)
    9 format(1x,'operations       :',f13.0)
   10 format(1x,'minimum local fill-in ordering with power:',i3)
   11 format(1x,'minimum degree ordering (power=0)')
   12 format(1x,'without ordering')

      call lptimer (tt1)
      if(tfind.lt.0)then
        write(buff,12)
      else if(tfind.eq.0)then
        write(buff,11)
      else
        write(buff,10)tfind
      endif
      oper=0.0d+0
      call lpmprnt(buff)
      do i=1,nz
        rowidx(i)=rowidx(i)-n
      enddo
      if(rfree.lt.nz)then
        write(buff,'(1x,a)')'not enough integer memory'
        call lpmprnt(buff)
        code=-2
        goto 999
      endif
      if(cfree.lt.2*nz)then
        write(buff,'(1x,a)')'not enough real memory'
        call lpmprnt(buff)
        code=-2
        goto 999
      endif

c if no ordering...

      if(tfind.lt.0)then
        t2=tt1
        do i=1,m
          perm(i)=i
        enddo
        goto 50
      endif

c otherwise...

      do i=1,n
        inta2(i)=i
      enddo
      call lptransps(n,m,nz,colpnt,rowidx,nonzeros,
     x rowpnt,colindex,nonzeros(nz+1),inta2)
      k=1
      l=m
      do i=1,m
        pivots(i)=0
        if(vcstat(i+n).le.-2)then
          invperm(l)=i
          l=l-1
        else
          invperm(k)=i
          k=k+1
        endif
      enddo
      call lptransps(m,n,nz,rowpnt,colindex,nonzeros(nz+1),
     x colpnt,rowidx,nonzeros,invperm)
      do i=1,n
        p1=colpnt(i)
        if(vcstat(i).le.-2)then
          p2=colpnt(i)-1
        else
          p2=colpnt(i+1)-1
  19      if((p1.le.p2).and.(vcstat(rowidx(p2)+n).le.-2))then
            p2=p2-1
            goto 19
          endif
        endif
        perm(i)=p1
        invperm(i)=p2
      enddo

      pnt=nz+1
      do i=1,m
        if(pnt+mn.gt.cfree)then
          write(buff,'(1x,a)')'not enough real memory'
          call lpmprnt(buff)
          code=-2
          goto 999
        endif
        pivots(i)=1
        if(vcstat(i+n).gt.-2)then
          ecolpnt(i)=pnt
          pnt1=rowpnt(i)
          pnt2=rowpnt(i+1)-1
          do j=pnt1,pnt2
            k=colindex(j)
            if(vcstat(k).gt.-2)then
              p1=perm(k)
              p2=invperm(k)
              perm(k)=perm(k)+1
              do l=p1,p2
                if(pivots(rowidx(l)).eq.0)then
                  pivots(rowidx(l))=1
                  rowidx(pnt)=rowidx(l)
                  pnt=pnt+1
                endif
              enddo
            endif
          enddo
          count(i)=pnt-ecolpnt(i)
          do j=ecolpnt(i),pnt-1
            pivots(rowidx(j))=0
          enddo
        endif
      enddo
      aatnz=pnt-nz-1


      call lptimer (t2)
      write(buff,1)dble(t2-tt1)/100.0d+0
      call lpmprnt(buff)

c call lpminimum fill-in ordering

      call lpgenmfo(m,mn,nz,cfree,rfree,pivotn,
     x ecolpnt,count,perm,rowpnt,vcstat(n+1),rowidx,
     x invperm,inta1,inta2,inta3,inta4,inta5,inta6,inta7,
     x inta8,inta9,inta10,inta11,colindex,tfind,inta12,pivots,code)
      if(code.lt.0)goto 999


 50   call lptimer(t1)
      write(buff,2)dble(t1-t2)/100.0d+0
      call lpmprnt(buff)

      pivotn=0
      do 30 i=1,n
        ecolpnt(i)=colpnt(i)
        count(i)=colpnt(i+1)-1
        inta2(i)=i
        if(vcstat(i).le.-2)goto 30
        pivotn=pivotn+1
        pivots(pivotn)=i
  30  continue

      call lptransps(n,m,nz,colpnt,rowidx,nonzeros,
     x rowpnt,colindex,nonzeros(nz+1),inta2)

      k=1
      l=m
      do 40 i=1,m
        j=perm(i)
        if(vcstat(j+n).le.-2)then
          invperm(l)=j
          l=l-1
        else
          pivotn=pivotn+1
          pivots(pivotn)=j+n
          invperm(k)=j
          k=k+1
        endif
  40  continue

      call lptransps(m,n,nz,rowpnt,colindex,nonzeros(nz+1),
     x colpnt,rowidx,nonzeros,invperm)

      do 20 i=1,nz
        rowidx(i)=rowidx(i)+n
  20  continue

      do i=1,n
        if(vcstat(i).gt.-2)then
          k=ecolpnt(i)
          l=count(i)
  35      if((l.ge.k).and.(vcstat(rowidx(l)).le.-2))then
            l=l-1
            goto 35
          endif
          count(i)=l
        endif
      enddo

      call lpsymfact(pivots,rowidx,ecolpnt,count,vcstat,
     x perm,invperm,inta2,inta1,l,code)
      if(code.lt.0)goto 999
      call lptimer(t2)
      write(buff,4)dble(t2-t1)/100.0d+0
      call lpmprnt(buff)
      if(tfind.ge.0)then
        write(buff,6)aatnz
        call lpmprnt(buff)
      endif
      write(buff,7)l
      call lpmprnt(buff)

      do 55 i=1,mn
        inta1(i)=0
  55  continue
      l=0
      do 60 i=1,pivotn
        j=pivots(pivotn-i+1)
        k=count(j)-ecolpnt(j)+1
        if(k.eq.0)goto 60
        l=l+k
        inta1(j)=inta1(rowidx(ecolpnt(j)))+k
        oper=oper+(dble(k)*dble(k)+dble(k))/2.0d+0
  60  continue
      call lptimer(t1)
      write(buff,5)dble(t2-tt1)/100.0d+0
      call lpmprnt(buff)
      write(buff,8)l
      call lpmprnt(buff)
      write(buff,9)oper
      call lpmprnt(buff)

 999  return
      end subroutine lpsymmfo

c ===========================================================================
      subroutine lptimer(wctime)

      implicit   none

c =======================================================================
c  return time in milliseconds
c =======================================================================

c      use dfport

      integer (kind=4) :: wctime

c      integer*4 ihr,imin,isec,i100
c =======================================================================
c      call gettim(ihr,imin,isec,i100)

      wctime = 99999
c      wctime=i100+100*(isec+60*(imin+60*ihr))

      end
