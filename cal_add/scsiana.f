c*******************************************************************
      subroutine scsiana(not,nind,nset,isys,bvec,rvec,pf)
c*******************************************************************
      implicit   none

      integer*4 not,nind,nset,isys
      real*8 bvec(*),rvec(*),pf
c
      real*8 btlo,btup,pf1,pf2
      integer*4 i
c
      if(nind.eq.1) then
c       ----- only one independent component -----
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''-- only one independent component'',
     *                  '' analytical solution is obtained --'')')
        btlo=-bvec(1)
        btup =1.0e+10
        do i=2,nset
          if(rvec(i).gt.0.0d0) then
            if(-bvec(i).gt.btlo) btlo=-bvec(i)
          else
            if(bvec(i).lt.btup) btup=bvec(i)
          endif
        enddo
        if(btlo.ge.btup) then
          write(not,'(1h ,''-- infeasible domain --'')')
          pf=0.0d0
        elseif(btlo.lt.-0.9999e+10) then
          call dnorm(btup,pf)
        else
          call dnorm(btup,pf1)
          call dnorm(btlo,pf2)
          pf=pf1-pf2
        endif
        if(isys.eq.1) then
          pf=1-pf
        endif
      elseif(nset.eq.2) then
c       ----- two independent component -----
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''-- only two independent component'',
     *                  '' analytical solution is obtained --'')')
        call bpfij(pf,-bvec(1),-bvec(2),rvec(2))
        if(isys.eq.1) then
          pf=1-pf
        endif
      endif
      write(not,'(1h ,''  pf...............'',1pe15.5)') pf

      end subroutine scsiana
