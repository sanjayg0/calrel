c*****************************************************************************
      subroutine joint(tf,par,bnd,tp,ids,ib,igt,lgp,izx,
     *                 ipa,mc,x1,y1,z1,igx,sg,ex,igfx,ixz,
     *                 beta,alph,gtor,fltf,xdes,ydes,iboun,icent)
c*****************************************************************************
      implicit   none

      include 'o8para.h'
      real*8        xdat
      common/o8xdat/xdat(nx)
      real          optite
      common/o8itin/optite

      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'nlpflag.h'
      include   'simu.h'

      real    (kind=8),allocatable :: alp(:)
      real    (kind=8),allocatable :: gam(:)
      real    (kind=8),allocatable :: dgx(:)
      real    (kind=8),allocatable :: gdgx(:)
      real    (kind=8),allocatable :: alphnew(:,:)
      real    (kind=8),allocatable :: btnew(:)
      real    (kind=8),allocatable :: g0(:)
      real    (kind=8),allocatable :: xjoint(:)
      real    (kind=8),allocatable :: yjoint(:)
      real    (kind=8),allocatable :: gdes(:)
      integer (kind=4),allocatable :: ifunc1(:,:)
      integer (kind=4),allocatable :: ifunc2(:,:)
      integer (kind=4),allocatable :: iflag1(:)
      integer (kind=4),allocatable :: iactive(:)
      integer (kind=4),allocatable :: iflag2(:)
      integer (kind=4),allocatable :: mcnew(:,:)
      integer (kind=4),allocatable :: ncomp(:)
      integer (kind=4),allocatable :: mc2(:,:)
      integer (kind=4),allocatable :: igfxnew(:,:)
      integer (kind=4),allocatable :: ialpha(:,:)
      integer (kind=4),allocatable :: icomps(:,:)
      integer (kind=4),allocatable :: iset(:)
      integer (kind=4),allocatable :: ifail(:,:)
      logical         ,allocatable :: fltfnew(:)

      integer (kind=4) :: maxset,isys,ind,iboun,icent,nset
      parameter         ( maxset=1000 )

      integer (kind=4) :: ids(nrx),ib(nrx),ipa(4,nrx),izx(nrx)
      integer (kind=4) :: lgp(3,nig),igt(nig)
      integer (kind=4) :: mc(ntl,2),igfx(ngf,3),igx(ngfc),ixz(*)
      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z1(nry),bnd(2,nrx)
      real    (kind=8) :: par(4,nrx),tp(ntp),sg(nrx),ex(nrx)
      real    (kind=8) :: beta(ngf),alph(nry,ngf),gtor(ngf)
      real    (kind=8) :: xdes(nrx,ngf),ydes(nry,ngf),tolalpha
      logical fltf(ngf)

      integer (kind=4) :: i,ncount,mcount,ncset,j,l,iiff
      integer (kind=4) :: nactive,k,kfin,nncomp, ntlnew, ngfnew
      integer (kind=4) :: iiff1,iiff2,m,iiff22,iiff11,ij
      real    (kind=8) :: ggg,sdgy
      real    (kind=8) :: gg00,gggg,btdum

c     integer*4 if1(4),if2(4),iff,jfunc
c     real*8 ggg1,gg11,ggg2,gg22,coef


c     ===== checking =====

      neval=0
      if(icl.ne.3) then
      write(not,'(1h ,'' join is for parallel or general problems '')')
      stop
      endif

      allocate(ialpha(nry,ngfc))
      allocate(iactive(ngfc))
      allocate(ncomp(maxset))
      allocate(ifunc1(ngfc,maxset))
      allocate(ifunc2(ngfc,maxset))
      allocate(iflag1(ngfc))
      allocate(iflag2(ngfc))
      allocate(g0(ngfc))
      allocate(mcnew(maxset,2))
      allocate(alp(nry))
      allocate(gam(nry))
      allocate(gdgx(nrx))
      allocate(dgx(nrx))
      allocate(alphnew(nry,1000))
      allocate(btnew(maxset))
      allocate(xjoint(nrx))
      allocate(yjoint(nry))
      allocate(gdes(ngfc))
      allocate(icomps(2,ngfc))

c     ===== preparation for linearized joint design point ====

      tolalpha=1.0d0/dfloat(nry)*1.0d-3
      do i=1,ngfc
        if(.not.fltf(i)) then
        write(not,'(1h ,'' fatal error in making center points '')')
        write(not,'(1h ,'' form has not been done for lsf'',i5)')i
        stop
        endif
        do j=1,nry
          if(dabs(alph(j,i)).lt.tolalpha) then
            ialpha(j,i)=1
          else
            ialpha(j,i)=0
          endif
        enddo
      enddo

c     ===== find cut set and joint desing point =====

      call joincut(not,ntl,mc,ncset,ncomp,
     *            ngfc,ifunc1,ifunc2,ngf,igfx,fltf)

c     ===== check joint cut set is empty or not =====
      allocate(iset(1000))
      allocate(ifail(ncset,ncset))

      do i=1,ncset
      do j=1,ncset
        ifail(i,j)=0
      enddo
      enddo
      do i=1,ncset-1
      do j=i+1,ncset
        call scmkbim(not,ngfc,ncomp(i),ifunc1(1,i),
     *                        ncomp(j),ifunc1(1,j)
     *                        ,nset,iset,ifail(i,j))
      enddo
      enddo

c     ----- evaluate mean value -----

      do i=1,nry
        y1(i)=0.0d0
      enddo
      write(not,'(1h ,'' mean of function '')')
      call cytox(x1,y1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      do i=1,ngf
        iiff=igfx(i,1)
        call ugfun(g0(i),x1,tp,iabs(iiff))
        zero(i)=dabs((1.0d-3)*g0(i))
        write(not,'(1h ,i10,2e15.5)') igfx(i,2),g0(i),zero(i)
      enddo


      ncount=0
      mcount=0
      do i=1,ncset
        nncomp=ncomp(i)
        write(not,'(1h ,''  '')')
        write(not,'(1h ,'' cutset........ '',i5)')i
        write(not,'(1h ,'' num of comp... '',i5)')nncomp

c       ----- for linearized joint design point -----

        if(icent.lt.0) then
        do ij=1,nncomp
          icomps(1,ij)=ifunc1(ij,i)
          icomps(2,ij)=ifunc2(ij,i)
        enddo
        call optwol(not,nry,1,nncomp,ngfc,ialpha,icomps,
     *              alph,beta,ncomp,xdat,btdum,alp)
        else
        iswnlp=0    !  inequatlity constraint !
        call donlp2(nry,nncomp,ifunc1(1,i),ifunc2(1,i),ids,ib,ipa,
     *              izx,ixz,lgp,igt,z1,y1,bnd,tf,par,sg,ex,tp,
     *              alp,gam,dgx,gdgx)
        kfin=int(optite)+11
        if(kfin.ne.11.and.kfin.ne.12) write(not,1001) i
c       ----- find active constraint -----
        write(not,'(1h ,'' joint design point found '',i10)')kfin
        endif

        write(not,'(1h ,1p5e15.5)') (xdat(j),j=1,nry)
        call cytox(xjoint,xdat,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *             ib,izx)
c       ----- joint design point in standard normal -----
        do j=1,nry
          yjoint(j)=xdat(j)
        enddo
        do j=1,nncomp
          iflag1(j)=0
          iflag2(j)=0
        enddo
        nactive=0
        do j=1,nncomp
          call ugfun(ggg,xjoint,tp,iabs(ifunc2(j,i)))
          gdes(j)=ggg
          gg00=zero(iabs(ifunc1(j,i)))
           write(not,'(1h ,'' gg00'',2e15.5)') gg00
          if(icent.lt.0) then
            iiff1=ifunc1(j,i)
            iiff2=ifunc2(j,i)
            ggg=beta(iabs(iiff1))
            do ij=1,nry
              ggg=ggg-yjoint(ij)*alph(ij,iabs(iiff1))
            enddo
          endif
          write(not,'(1h ,''active check comp'',i10,'' gval'',
     *      1pe15.5)')
     *         ifunc2(j,i),ggg
          if(dabs(ggg).le.gg00) then
            nactive=nactive+1
            iactive(nactive)=j
            iflag1(j)=1
            iflag2(j)=1
            ncount=ncount+1
            mcount=mcount+1
            mcnew(mcount,1)=ncount
            mcnew(mcount,2)=ncount
c           ----- make beta and gradient -----
            call joinalbt(nry,xjoint,yjoint,tp,ex,sg,dgx,gam,alp,tf,par,
     *                    bnd,igt,lgp,ids,ipa,izx,ixz,sdgy,gdgx,gdes(j),
     *      ifunc1(j,i),ifunc2(j,i),alphnew(1,ncount),btnew(ncount))
            write(not,'(1h ,'' == active component == '')')
            write(not,'(1h ,'' alpha vector '')')
            write(not,'(1h ,1p5e15.5)') (alphnew(k,ncount),k=1,nry)
            write(not,'(1h ,'' beta value....'',1pe15.5)')btnew(ncount)
          endif
        enddo

        write(not,'(1h ,'' total num of active comps....'',i10)')nactive
        if(nactive.eq.0) write(not,1002) i
c       ---- active & non active intersection -----
c        if(nactive.ne.0.and.nactive.ne.nncomp) then
        if(nactive.ne.nncomp) then
          write(not,'(1h ,'' inactive constraints'')')
          do k=1,nncomp
            if(iflag1(k).eq.1) goto 100
            ncount=ncount+1
            mcount=mcount+1
            mcnew(mcount,1)=ncount
            mcnew(mcount,2)=ncount
            iiff1=ifunc1(k,i)
            iiff2=ifunc2(k,i)
            do l=1,nrx
              x1(l)=xdes(l,iabs(iiff1))
            enddo
            do l=1,nry
              y1(l)=ydes(l,iabs(iiff1))
            enddo
            do m=1,nncomp
              if(m.ne.k) then
                iiff22=ifunc2(m,i)
                iiff11=ifunc1(m,i)
                call ugfun(gggg,x1,tp,iabs(iiff22))
                if(iiff22.gt.0) then
                  if(gggg.gt.zero(iabs(iiff11))) goto 50
                else
                  if(gggg.lt.-zero(iabs(iiff11))) goto 50
                endif
              endif
             enddo
c            ----- design point is in the failure domain -----
             btnew(ncount)=beta(iabs(iiff1))
             do l=1,nry
               alphnew(l,ncount)=alph(l,iabs(iiff1))
             enddo
             goto 100
   50        continue
c            ----- design point is not in the failure domain -----
             call joinalbt(nry,xjoint,yjoint,tp,ex,sg,dgx,gam,alp,tf,par
     *                    ,bnd,igt,lgp,ids,ipa,izx,ixz,sdgy,gdgx,gdes(k)
     *                ,iiff1,iiff2,alphnew(1,ncount),btnew(ncount))
  100       continue
          enddo
        endif

c          do j=1,nactive
c            jfunc=iactive(j)
c            if1(1)=ifunc1(jfunc,i)
c            if1(2)=-if1(1)
c            if2(1)=ifunc2(jfunc,i)
c            if2(2)=-if2(1)
c            do k=1,nncomp
c              if(iflag1(k).eq.1) goto 100
c              if1(3)=ifunc1(k,i)
c              if1(4)=-if1(3)
c              if2(3)=ifunc2(k,i)
c              if2(4)=-if2(3)
c              iswnlp=1
c              write(not,'(1h ,'' activ'',i10,'' and inactive'',i10)')
c     *            if2(1),if2(3)
c              write(not,'(1h ,'' if1..........'',4i10)')(if1(l),l=1,4)
c              write(not,'(1h ,'' if2..........'',4i10)')(if2(l),l=1,4)
c              call donlp2(nry,4,if1,if2,ids,ib,ipa,izx,ixz,
c     *                  lgp,igt,z1,y1,bnd,tf,par,sg,ex,tp,
c     *                  alp,gam,dgx,gdgx)
c              kfin=int(optite)+11
c              write(not,'(1h ,''  '')')
c              write(not,'(1h ,'' intersection point found '',i10)') kfin
c              write(not,'(1h ,1p5e15.5)') (xdat(l),l=1,nry)
c              call cytox(x1,xdat,z1,tf,ex,sg,par,bnd,
c     *                  igt,lgp,ids,ipa,ib,izx)
cc              if(kfin.ne.11.and.kfin.ne.12) then
c                call ugfun(ggg1,x1,tp,iabs(if2(1)))
c                call ugfun(ggg2,x1,tp,iabs(if2(3)))
c                write(not,'(1h ,'' check g for active   '',e15.5)')ggg1
c                write(not,'(1h ,'' check g for inactive '',e15.5)')ggg2
c                gg11=zero(iabs(if1(1)))
c                gg22=zero(iabs(if1(3)))
c                write(not,'(1h ,'' gg11                 '',e15.5)')gg11
c                write(not,'(1h ,'' gg22                 '',e15.5)')gg22
c                if(dabs(ggg1).gt.gtor(iabs(if1(1)))*1.0d1.or.
c     *            dabs(ggg2).gt.gtor(iabs(if1(3)))*1.0d1) goto 100
c                if(dabs(ggg1).gt.gg11.or.dabs(ggg2).gt.gg22) goto 100
c              endif
c              iflag2(k)=1
c              ncount=ncount+1
c              mcount=mcount+1
c              mcnew(mcount,1)=ncount
c              mcnew(mcount,2)=ncount
c              call joinalbt(nry,x1,y1,tp,ex,sg,dgx,gam,alp,tf,par,
c     *                      bnd,igt,lgp,ids,ipa,izx,ixz,sdgy,gdgx,
c     *               if1(1),if2(1),alphnew(1,ncount),btnew(ncount))
c              write(not,'(1h ,'' alpha vector for'',i10)') if2(1)
c              write(not,'(1h ,1p5e15.5)') (alphnew(l,ncount),l=1,nry)
c              write(not,'(1h ,'' beta value...'',1pe15.5)')btnew(ncount)
c              ncount=ncount+1
c              mcount=mcount+1
c              mcnew(mcount,1)=ncount
c              mcnew(mcount,2)=ncount
c              call joinalbt(nry,x1,y1,tp,ex,sg,dgx,gam,alp,tf,par,
c     *                      bnd,igt,lgp,ids,ipa,izx,ixz,sdgy,gdgx,
c     *               if1(3),if2(3),alphnew(1,ncount),btnew(ncount))
c              write(not,'(1h ,'' alpha vector for'',i10)') if2(3)
c              write(not,'(1h ,1p5e15.5)') (alphnew(l,ncount),l=1,nry)
c              write(not,'(1h ,'' beta value...'',1pe15.5)')btnew(ncount)
c  100       continue
c            enddo
c          enddo

c         ---- non used components -----

c          write(not,'(1h ,'' check non used components '')')
c          do j=1,nncomp
c            if(iflag2(j).eq.0) then
c              write(not,'(1h ,'' comp'',i10,'' is not used'')')
c     *        ifunc2(j,i)
c              iff=ifunc1(j,i)
c              ncount=ncount+1
c              mcount=mcount+1
c              mcnew(mcount,1)=ncount
c              mcnew(mcount,2)=ncount
c              coef=1.0d0
c              if(iff.lt.0) coef=-1.0d0
c              do k=1,nry
c                alphnew(k,ncount)=coef*alph(k,iabs(iff))
c              enddo
c              btnew(ncount)=coef*beta(iabs(iff))
c              write(not,'(1h ,'' alpha vector for'',i10)') ifunc2(j,i)
c              write(not,'(1h ,1p5e15.5)') (alphnew(l,ncount),l=1,nry)
c              write(not,'(1h ,'' beta value...'',1pe15.5)')btnew(ncount)
c            endif
c          enddo
c        endif

        write(not,'(1h ,'' ====== cut set is done ======'')')
        mcount=mcount+1
        mcnew(mcount,1)=0
        mcnew(mcount,2)=0
      enddo

      allocate(mc2(mcount,2))
      do i=1,mcount
        mc2(i,1)=mcnew(i,1)
        mc2(i,2)=mcnew(i,2)
      enddo
c     ----- check all data -----
      call joinprset(not,mcount,ncount,mc2,nry,alphnew,btnew)

c     ----- caluculate reliability -----

      ngfnew=ncount
      ntlnew=mcount
      allocate(igfxnew(ngfnew,3))
      allocate(fltfnew(ngfnew))
      do i=1,ngfnew
        igfxnew(i,1)=i
        igfxnew(i,2)=i
        igfxnew(i,3)=i
        fltfnew(i)=.true.
      enddo
      isys=-3
      ind=0
      call scis(not,ngfnew,nry,nsm,npr,ntlnew,isys,nin,ind,iboun,
     *          cov,stp,mc2,y1,igfxnew,btnew,alphnew,fltfnew,ifail)

      write(not,'(1h ,'' number of g-func eval....'',i10)')neval

      deallocate(mc2)
      deallocate(igfxnew)
      deallocate(fltfnew)
      deallocate(ialpha)
      deallocate(iactive)
      deallocate(ncomp)
      deallocate(ifunc1)
      deallocate(ifunc2)
      deallocate(iflag1)
      deallocate(iflag2)
      deallocate(g0)
      deallocate(mcnew)
      deallocate(alp)
      deallocate(gam)
      deallocate(gdgx)
      deallocate(dgx)
      deallocate(alphnew)
      deallocate(btnew)
      deallocate(xjoint)
      deallocate(yjoint)
      deallocate(gdes)
      deallocate(icomps)
      deallocate(iset)
      deallocate(ifail)

 1001 format(' !! warning in joint !!',/
     *       ' joint design point may not be found or ',/
     *       ' not be an optimal one for cut set ',i5)
 1002 format(' !! Warning in joint !!',/
     *       ' No joint desing point for this cust set',i5)

      end subroutine joint
