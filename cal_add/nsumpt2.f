cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine nsumpt(tp,x,mc,igx,idg)
c
c     functions:
c       determine whether the simulated point in monte carlo simulation
c       is in the failure region or not.
c
c     input arguments:
c       tp   : deterministic parameters.
c       x    : simulated point in the x space.
c       mc   : aray of minimun cut sets.
c
c     output arguments:
c       idg  : index to show the trial point is in the failure region or not.
c              =1, the point is in the failure region.
c              =0, the point is not in the failure region.
c
c     calls: gfun.
c
c     called by: nmont.
c
c     last version: july 30, 1987 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine nsumpt2
     *     (tp,x,mc,igx,igfx,idg,mcst,mcend,gfun,gset,igset)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     modified nsumpt
c     to keep limit-state function value

      implicit   none

      include   'cuts.h'
      include   'grad.h'
      include   'prob.h'

      real*8 x(*),tp(*)
      integer*4 mc(ntl,2),igx(ngfc),igfx(ngf,3),idg,mcst,mcend
      integer*4 igset(*)
      real*8 gfun(*),gset(*)
c
      integer*4 ig,ig4,imm,i,ic
      real*8 gx,gmmax
c
      flgf=.true.
      flgr=.false.
      do 10 ig=1,ngfc
c     ----- check each function -----
      ig4=igfx(ig,3)
      call ugfun(gx,x,tp,ig4)
      gfun(ig)=gx
      if(gx.le.0.0d0) then
        igx(ig)=0
      else
        igx(ig)=1
      endif
 10   continue
      idg=1
      imm=1
      gmmax=0.0d0
      do 100 i=mcst,mcend
      if(mc(i,1).ne.0) then
          ic=mc(i,2)
        if(mc(i,1).lt.0) then
          imm=imm*igx(ic)
          if(igx(ic).eq.0) gmmax=gmmax+gfun(ic)**2
        else
          imm=imm*(1-igx(ic))
          if(igx(ic).eq.1) gmmax=gmmax+gfun(ic)**2
        endif
      else
        igset(i)=imm
        idg=idg*(1-imm)
        imm=1
        gset(i)=gmmax
        gmmax=0.0d0
      endif
 100  continue
      idg=1-idg

c     idg=1 fail
c     idg=0 safe

      end subroutine nsumpt2
