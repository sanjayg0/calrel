c**********************************************
      subroutine scfiro(not,nset,ncomp,iset,nry,alpha)
c**********************************************
c
c    find non sigular combination of alpha
c    until nry found
c
c**********************************************
      implicit   none

      integer*4,allocatable::iflag(:),icomp(:)
      integer*4 not,nset,iset(*),nry
      real*8 alpha(nry,*),prod
c
      integer*4 i,ncomp,j,ncomp2
      real*8 cdot
      allocate(iflag(nset))
      allocate(icomp(nset))
      do i=1,nset
        iflag(i)=0
      enddo
c
      ncomp=1
      icomp(ncomp)=iset(1)
      iflag(1)=1
c
      do i=1,nset
        if(icomp(i).eq.0) then
            do j=1,ncomp
            prod=cdot(alpha(1,icomp(j)),alpha(1,iset(i)),nry,nry,nry)
            if(dabs(prod).gt.0.99999d0) goto 100
          enddo
          ncomp=ncomp+1
          icomp(ncomp)=iset(i)
          iflag(i)=1
          if(ncomp.eq.nry) goto 200
        endif
  100   continue
      enddo
c
      if(ncomp.lt.nry) then
      print *, 'fatal error in scis'
      print *, 'number of nonsigular components is less than'
      print *, 'number of variables'
      stop
      endif
c
      ncomp2=ncomp
  200 continue
      do i=1,nset
        if(iflag(i).eq.0) then
          ncomp2=ncomp2
          icomp(ncomp2)=iset(i)
        endif
      enddo
      do i=1,nset
        iset(i)=icomp(i)
      enddo
c
      if(not.ne.0) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' *** subroutine scfiro *** '')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,'' number of comp find.........'',i5)') ncomp
      write(not,'(1h ,'' comp........................'',10i5)')
     *     (iset(i),i=1,ncomp)
      write(not,'(1h ,'' rest of comp '')')
      write(not,'(1h ,'' comp........................'',10i5)')
     *     (iset(i),i=ncomp+1,nset)
      endif
      deallocate(iflag)
      deallocate(icomp)

      end subroutine scfiro
