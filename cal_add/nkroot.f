c***************************************************************
      subroutine nkroot(ngf,ntl,nry,ngfc,icl,igfx,mcst,mcend,mc,
     *                  beta,alph,xwrk,ywrk,igwrk,root)
c***************************************************************
c
c     make beta limit to be analyzed
c
c**************************************************************
      implicit   none

      integer*4 ngf,ntl,nry,ngfc
      real*8 root,beta(ngf),alph(nry,ngf)
      integer*4 icl,igfx(ngf,3),mcst,mcend,mc(ntl,2),igwrk(ngf)
      real*8 xwrk(nry),ywrk(nry)
c
      integer*4 i,i2,i1,ik,ik2,j,jk,jk2,jj,jj2,imm,idg,ii,ij
      integer*4 ic,ier
      real*8 root1,rho,cdot,a,b,c,e,sx,tmp,sy,pch
c
      root=30.d0
c
      if(icl.le.2) then
c       ---- for seriese sys or comp porb. -----
        do 95 i=1,ngfc
          i2=igfx(i,2)
          if(abs(beta(i2)).le.root) root=beta(i2)
  95    continue
        if(dabs(root).le.2.d0) root=2.d0
      else
c       ----- for general system -----
        root1=0.d0
        do 96 i=mcst,mcend
          if(mc(i,1).ne.0) then
            i1=i+1
            ik=mc(i,2)
            ik2=igfx(ik,2)
            do 196 j=i1,mcend
              if(mc(j,1).eq.0) go to 96
              jk=mc(j,2)
              jk2=igfx(jk,2)
              rho=cdot(alph(1,ik2),alph(1,jk2),1,1,nry)
c             -----compute the threshold for the root.
              if(dabs(rho).eq.1.0d0) go to 100
              a=1.d0-rho*rho
              c=beta(ik2)-rho*beta(jk2)
              b=c/dsqrt(a)
              e=beta(jk2)*beta(jk2)+b*b
              e=dsqrt(e)
              sx=0.d0
              do 97 jj=1,nry
                xwrk(jj)=alph(jj,ik2)-alph(jj,jk2)*rho
                sx=sx+xwrk(jj)*xwrk(jj)
  97          continue
              sx=dsqrt(sx)
              a=b/sx
              do 98 jj=1,nry
                ywrk(jj)=beta(jk2)*alph(jj,jk2)+a*xwrk(jj)
  98          continue
              go to 103
100           if(rho.eq.1.0d0) then
                tmp=(beta(ik2)+beta(jk2))/2
              else
                tmp=(beta(ik2)-beta(jk2))/2
              endif
              do 101,jj=1,nry
                ywrk(jj)=tmp*alph(jj,ik2)
101           continue
              e=max(dabs(beta(ik2)),dabs(beta(jk2)))
c.....check this point is in the failure domain or not.
103           do 1098 jj=1,ngfc
                sy=0.d0
                jj2=igfx(jj,2)
                igwrk(jj)=1
                if(jj.eq.ik.or.jj.eq.jk) then
                  igwrk(jj)=0
                else
                  do 99 ii=1,nry
  99              sy=sy+ywrk(ii)*alph(ii,jj2)
                  if(sy.ge.beta(jj2)) igwrk(jj)=0
                endif
 1098         continue
              imm=1
              idg=1
              do 1100 ij=mcst,mcend
                if(mc(ij,1).ne.0) then
                  ic=mc(ij,2)
                  if(mc(ij,1).lt.0) then
                    imm=imm*igwrk(ic)
                  else
                    imm=imm*(1-igwrk(ic))
                  endif
                else
                  idg=idg*(1-imm)
                  imm=1
                endif
 1100         continue
c.....the root is in the failure domain.
              if(idg.eq.0.and.e.ge.root1) root1=e
 196        continue
          else
            if(root1.le.root) root=root1
            root1=0.d0
          endif
   96   continue
      endif
c
      call dnorm(-root,pch)
      if(icl.le.2) then
        pch=0.0001d0*pch
      else
        pch=0.0001d0*pch
      endif
      pch=1.d0-pch
      call dnormi(root,pch,ier)

      end subroutine nkroot
