c********************************************************************
c     equality constrained recursive quadratic programming with
c     multiple inactivation and superlinearly convergent projected
c     bfgs-update (version 12/93 spellucci )
c********************************************************************
      subroutine o8opti(ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd,
     *                  gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
c      subroutine o8opti
      implicit   none

      include 'o8comm.h'
      include 'o8cons.h'
      include 'o8qpdu.h'
      include 'o8fint.h'
      integer*4 ifunc1(*),ifunc2(*),ids(*),ipa(4,*)
      integer*4 izx(*),lgp(3,*),igt(*),ixz(*),ib(*)
      real*8 zzz(*),bnd(2,*),tf(*),par(4,*),sg(*),ex(*),tp(*),yyy(*)
      real*8 gam(*),dgx(*),gdgx(*),alp(*)
c*******  local variables
      integer l,l0,i,j,k,csssig,csirup,csreg,cschgx
      integer csmdph
      double precision delsig,delx,sum,term
      double precision umin,term1,scfh,unorm
      double precision del1
      double precision qtx(nx)
      double precision yy(nx),yx(nx),trvec(nx)
      integer iumin,rank0,nr0,csdifx,clwold
      integer delist(0:nresm),nrbas,bindba(nresm)
      double precision eps,delold,uminsc,fac,slackn,tauqp0
      double precision o8sc1,o8sc3,o8vecn
      external o8sc1,o8sc3,o8vecn
      logical nperm,qpnew,etaini,viobnd
      save
c initialization
c save starting point for later printing only
      do i=1,n
        d(i)=zero
        d0(i)=zero
      enddo
      itstep=0
      alist(0)=nh
      delist(0)=0
      violis(0)=0
      upsi=zero
      psi=zero
      psi0=zero
      sig0=zero
      d0norm=one
      unorm=one
c*** in order to have cosphi well defined for itstep=1
      dnorm=one
      del=del0
c*** count successive regularization steps
      csreg=0
c*** count successive small changes in x
      cschgx=0
c*** count small differences of fx
      csdifx=0
c*** count irregular quasi-newton-updates
      csirup=0
c*** count successive small stepsizes
      csssig=0
c*** count successive small differences of penalty-function
      csmdph=0
      matsc=one
c  formerly tauqp=tp2 is worse
      tauqp=one
      nperm=.false.
      ident=.false.
      etaini=.false.
      if ( n .gt. 100 .or. nres .gt. 100 ) te3=.false.
      do  i = 1,n
        perm(i)=i
        perm1(i)=i
      enddo
      do i = 1,nh
        bind0(i)=1
        bind(i)=1
        alist(i)=i
      enddo
      if ( analyt ) then
        eps=min(epsx,sqrt(epsmac))
      else
        eps=epsdif
        if ( epsx .lt. epsdif**2 ) epsx=epsdif**2
      endif
      eps=max(epsmac*tp3,min(tm3,eps))
c**** calling for external function evaluation necessary only if
c     corr=.true.
c
c******* function and gradient values, from xtr=x*xsc
      do i =1 ,nres
        if ( i .le. nh ) then
          cfuerr(i)=.false.
          call esh(i,x,res(i))
          if ( cfuerr(i) ) then
            if ( .not. silent ) call o8msg(23)
            optite=-10
            return
          endif
          term=abs(res(i))
          if ( .not. gconst(i) ) then
c******* we assume that the gradient can be evaluated whenever
c******* the function can
            call esgradh(i,x,yy)
            val(i)=.true.
            do  j=1,n
              gres(j,i)=yy(j)
            enddo
          endif
        else
          bind(i)=0
          cfuerr(i)=.false.
          call esg(i-nh,x,res(i),ifunc1,ifunc2,yyy,zzz,tf,tp,ex,sg,par,
     *               bnd,igt,lgp,ids,ipa,ib,izx)
c          call esg(i-nh,x,res(i))
          if ( cfuerr(i) ) then
            if ( .not. silent ) call o8msg(23)
            optite=-10
            return
          endif
          term=-min(zero,res(i))
          if ( res(i) .le. delmin ) then
            bind(i)=1
            alist(0)=alist(0)+1
            alist(alist(0))=i
            if ( .not. gconst(i) ) then
              val(i)=.true.
c****** we assume that the gradient can be evaluated
c       whenever the function can
              call esgradg(i-nh,x,yy,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,
     *        sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
c              call esgradg(i-nh,x,yy)
              do j =1,n
                gres(j,i)=yy(j)
              enddo
            endif
          endif
        endif
        upsi=upsi+term
        psi=psi+term*w(i)
        if ( val(i) .and. .not. gconst(i) )
     f    gresn(i)=max(one,o8vecn(1,n,yy))
      enddo
  100 continue
c****************************************************************
c     obtaining a point feasible within tau0 first
c****************************************************************
      if ( upsi .ge. tau0 ) then
        scf=zero
        phase=-1
      else
        ffuerr=.false.
        call esf(x,fx)
        if ( ffuerr ) then
          if ( .not. silent ) call o8msg(22)
          optite=-9
          return
        endif
        if ( .not. val(0) ) then
c*** we assume that the gradient evaluation can be done
c*** whenever the function has been evaluated
          call esgradf(x,gradf)
          val(0)=.true.
        endif
        scf=one
        phase=0
        fxst=fx
        psist=psi
        upsist=upsi
        do j=1,nres
          resst(j)=res(j)
        enddo
        eta=zero
      endif
c*******************************************************************
  200 continue
c*******************************************************************
c     main iteration loop: getting a better x
c*******************************************************************
      if ( .not. ident ) then
        itstep=itstep+1
        if ( itstep .gt. iterma ) then
          optite=-nint(three)
          itstep=iterma
          b2n=accinf(itstep,8)
          b2n0=accinf(itstep,7)
          return
        endif
        qpnew=.false.
        qpterm=0
        delold=del
        del=zero
        b2n0=-one
        b2n=-one
        singul=.false.
        nperm=.false.
        do i=1,n
          nperm=nperm .or. (perm(i) .ne. perm1(i))
          perm(i)=perm1(i)
        enddo
        do i=1,nres
          diag(i)=zero
        enddo
      endif
c*******************************
c     current valid row permutation for qr-decomposition of
c     matrix of binding gradients in order to obtain continuity
c     of the qr-decomposition  is given by perm
c*******************************
      nr=alist(0)
      nrbas=nr
      do i=1,nres
        bindba(i)=bind(i)
      enddo
      do j=1,32
        accinf(itstep,j)=zero
      enddo
      gfn=o8vecn(1,n,gradf)
c***** compute new weight of objective function if useful
      if ( nres .gt. 0 .and. phase .ge. 0 .and. .not. ident
     f    .and. itstep .gt. 1 .and.
     f    ( (accinf(itstep-1,10) .eq. -1. .and.  scf0 .eq. one )
     f    .or. accinf(itstep-1,10) .eq. 1. )   ) then
c***   try rescaling the objective function
        term=zero
        do i=1,nres
          if ( gunit(1,i) .ne. 1 ) term=max(term,gresn(i))
        enddo
        scfh=term/max(one/scfmax,gfn)
        if ( scfh .lt. one/scfmax ) scfh=one/scfmax
        if ( scfh .gt. scfmax ) scfh=scfmax
        if ( (fxst-fx)*scfh+scfh/scf*(psist-psi) .ge.
     f     scfh/scf*eta*clow .and. lastch .le. itstep-4
     f     .and. scfh .lt. tm1*scf .or. scfh .gt. tp1*scf )then
c*** rescale the objective function if this seems promising and the
c*** change is significant
          clow=clow+1
          term=scfh/scf
          psi=psi*term
          psist=psist*term
          do i=1,nres
            u(i)=u(i)*term
          enddo
          unorm=unorm*term
          scf=scfh
          lastch=itstep
          term=sqrt(term)
          do  i=1,n
            diag0(i)=term*diag0(i)
            do  j=1,n
              a(j,i)=a(j,i)*term
            enddo
          enddo
          matsc=matsc*term
          if ( .not. silent ) call o8msg(2)
        endif
      endif
      accinf(itstep,1)=itstep
      accinf(itstep,2)=fx
      accinf(itstep,3)=scf
      accinf(itstep,4)=psi
      accinf(itstep,5)=upsi
      if ( .not. silent ) call o8info(1)
c
c***  begin solver
c*******************************************************************
c     qr-decomposition of matrix of binding gradients
c*******************************************************************
      if ( nr .ge. 1 ) then
        call o8dec(1,nr)
      else
        rank=0
      endif
      call o8left(a,gradf,yy,term,n)
      do i=1,n
        qgf(i)=yy(perm(i))
      enddo
      call o8ht(1,0,1,rank,n,qr,betaq,qgf,trvec)
      do i=1,n
        qgf(i)=trvec(i)
      enddo
      if ( rank .ne. nr .and. .not. silent ) call o8msg(1)
c*******************************************************************
c     compute del as function of x (forcing infeasibility and
c     the projected gradient to zero)
c*******************************************************************
      b2n0=o8vecn(rank+1,n,qgf)
      sum=zero
      do i=1,nres
        if ( i.le.nh ) then
          sum=sum+abs(res(i))/gresn(i)
        else
          sum=sum-min(zero,res(i))/gresn(i)
        endif
      enddo
      if ( itstep .gt. 1 .and. accinf(itstep-1,8) .ge. zero
     f     .and. .not. etaini
     f     .and. accinf(itstep-1,18) .ge. 0  ) then
        etaini=.true.
        eta=(accinf(itstep-1,8)/max(one,gfn)+sum
     f       +min(one,slackn)+min(one,abs(uminsc)))/min(30*n,iterma)
        level=eta
      endif
      delx=delmin
      term=scf*(fx0-fx)+psi0-psi
      if ( term .gt. zero .and. scf .ne. zero )
     f     delx=max(delx,exp(p7*p7*log(term)))
      if ( scf .eq. zero ) delx=min(del0*tm4,max(delx,upsi*tm2))
      delsig=delmin
c**** del should be large enough to include constraints hit in
c**** step before . violis comes from unimin
      do i=1,violis(0)
        j=violis(i)
        delsig=max(delsig,
     f    res(j)/gresn(j)/delfac(j)*(one+tm1))
      enddo
      del=min(del0,max(min(delsig,five*delx),delx))
      if ( violis(0) .eq. 0 ) del=min(del,del01)
c************************************************************
c     if phase=2 don't loose a binding constraint
c     phase=2 implies delist(0)=0
c************************************************************
      if ( phase .eq. 2 .and. violis(0) .eq. 0 ) then
        do i=nh+1,nres
          if ( bind0(i) .eq. 1 )
     f    del=min(del01,max(del,abs(res(i))/gresn(i)))
        enddo
      endif
c*** reduce del by a fixed factor ( tm2) if useful,
c    that is if delete-list
c*** in the previous step was not empty
      term=del
      do i=1,delist(0)
        j=delist(i)
        term1=res(j)/gresn(j)*(one-tm2)/delfac(j)
        if ( term1 .ge. del*tm2 ) term=min(term,term1)
      enddo
      del=term
c***  if delta becomes too large, we may loose complementary slackness
      if ( itstep .gt. 1 .and. .not. ident .and. scf .ne. zero ) then
        term=zero
        do i=nh+1,nres
          term=term+
     f    max(zero,res(i)/gresn(i)-delmin)
     f    *max(zero,u(i)-smallw)/gresn(i)
        enddo
        if ( term .gt. zero ) then
          do i=nh+1,nres
            if ( u(i) .gt. smallw .and. res(i)/gresn(i) .gt. delmin )
     f      then
              del=max(delmin,
     f          min(del,res(i)/gresn(i)*(one-tm2)/delfac(i)))
            endif
          enddo
        endif
      endif
c***  if the current step was singular and not successful,
c***  try a greater del
c***  the same, if qpterm in the last step did signal trouble, but
c***  stepsize selection was nevertheless successful
      if ( itstep .gt. 1
     f     .and. accinf(itstep-1,30) .lt. 0.
     f   ) del=min(tp1*delold,del0)
c*************************************************************
c     include nearly binding inequality constraints
c*************************************************************
      do i=nh+1,nres
        term=res(i)/gresn(i)
        if ( bind(i) .eq. 0 .and. term .le. del*delfac(i) ) then
c  it may be useful to include  constraints>0  if  near its boundary
c  but avoid it, if the constraint was in the old delete-list
            bind(i)=1
            alist(0)=alist(0)+1
            alist(alist(0))=i
            if ( .not. val(i) ) then
              val(i)=.true.
              call esgradg(i-nh,x,yy,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,
     *        sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
c              call esgradg(i-nh,x,yy)
              do j=1,n
                gres(j,i)=yy(j)
              enddo
              gresn(i)=max(one,o8vecn(1,n,yy))
            endif
        endif
      enddo
      rank0=rank
      nr0=nr
      nr=alist(0)
      call o8dec(nr0+1,nr)
      if ( rank .ne. nr .and. .not. silent ) call o8msg(3)
      call o8ht(1,0,rank0+1,rank,n,qr,betaq,qgf,trvec)
      do i=1,n
        qgf(i)=trvec(i)
      enddo
      do i=1,n
        yy(i)=-qgf(i)*scf
      enddo
      do i=1,nres
        yu(i)=zero
      enddo
c************************************************************
c     first computation of lagrangian multipliers u
c************************************************************
      call o8sol(1,rank,yy,yu)
      umin=zero
      unorm=zero
      do i=1,nres
        u(i)=zero
      enddo
      iumin=0
      uminsc=zero
      do i=1,rank
        unorm=max(unorm,abs(yu(i)))
        k=alist(colno(i))
        u(k)=-yu(i)
        if ( k .gt. nh ) then
          if ( -yu(i)/gresn(k) .lt. uminsc ) then
            iumin=k
            uminsc=-yu(i)/gresn(k)
          endif
        endif
      enddo
      if ( scf .ne. zero ) then
        do i=1,n
          yx(i)=scf*gradf(i)
          do j=1,nres
            yx(i)=yx(i)-gres(i,j)*u(j)
          enddo
        enddo
        b2n=o8vecn(1,n,yx)/scf
      else
        b2n=-one
      endif
      if ( .not. silent ) call o8info(3)
c********** compute new delta
      del1=del
      if ( b2n .ge. zero  )
     f del1=max(del,
     f   tm1*min(del0,
     f   exp(p7*log( abs(b2n)/(gfn+one) + max(zero,-uminsc) + sum))  ))
c**** exclude constraints which were candidates for inactivating
c     in the previous step
c**** if useful
      do i=1,delist(0)
        j=delist(i)
        term1=res(j)/gresn(j)*(one-tm2)/delfac(j)
        if ( term1 .ge. del1*tm2 ) del1=max(delmin,min(del1,term1))
      enddo
      slackn=zero
      do i=nh+1,nres
        slackn=slackn+
     f  max(zero,res(i)/gresn(i)-delmin)*max(zero,u(i)-smallw)/gresn(i)
      enddo
      if ( upsi .le. delmin .and. b2n .le. epsx*(gfn+one)
     f     .and. b2n .ne. -one
     f     .and. uminsc .ge. -smallw .and.
     f     slackn .le. delmin*smallw*nres ) then
c sufficient accuracy in kuhn-tucker conditions
        optite=nint(zero)
        return
      endif
c******** include additional constraints if necessary
      l0=alist(0)
      do i=1,nres
        term=res(i)/gresn(i)/delfac(i)
        if ( term .gt. del .and. term .le. del1
     f      .and. bind(i) .eq. 0) then
          bind(i)=1
          alist(0)=alist(0)+1
          alist(alist(0))=i
          if ( .not. val(i) ) then
            val(i)=.true.
c            call esgradg(i-nh,x,yy)
            call esgradg(i-nh,x,yy,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,
     *      sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
            do j=1,n
              gres(j,i)=yy(j)
            enddo
            gresn(i)=max(one,o8vecn(1,n,yy))
          endif
        endif
      enddo
      del=del1
      accinf(itstep,6)=del
      accinf(itstep,7)=b2n0
      accinf(itstep,9)=alist(0)
      accinf(itstep,10)=-1.
      nr=alist(0)
      if ( l0 .ne. nr ) then
        rank0=rank
        call o8dec(l0+1,nr)
        call o8ht(1,0,rank0+1,rank,n,qr,betaq,qgf,trvec)
        do i=1,n
          qgf(i)=trvec(i)
        enddo
      endif
      if ( .not. silent ) call o8info(2)
      if ( rank .ne. nr ) then
        if ( .not. silent ) call o8msg(4)
        goto 400
      endif
c************************************************************
c     second solution for multipliers, rank may have changed !
c************************************************************
      do i=1,n
        yy(i)=-qgf(i)*scf
      enddo
      do i=1,nres
        yu(i)=zero
      enddo
      call o8sol(1,rank,yy,yu)
c*** remember the column interchanges in qr!
c*** yu(i) corresponds to u(alist(colno(i))
      umin=zero
      unorm=zero
      do i=1,nres
        u(i)=zero
      enddo
      iumin=0
      uminsc=zero
      do i=1,rank
        unorm=max(unorm,abs(yu(i)))
        k=alist(colno(i))
        u(k)=-yu(i)
        if ( k .gt. nh ) then
          umin=min(umin,-yu(i))
          if ( -yu(i)/gresn(k) .lt. uminsc ) then
            iumin=k
            uminsc=-yu(i)/gresn(k)
          endif
        endif
      enddo
      if ( scf .ne. zero ) then
        do i=1,n
          yx(i)=scf*gradf(i)
          do j=1,nres
            yx(i)=yx(i)-gres(i,j)*u(j)
          enddo
        enddo
        b2n=o8vecn(1,n,yx)/scf
      endif
      accinf(itstep,8)=b2n
      accinf(itstep,11)=umin
      call o8shms
      if ( .not. silent ) call o8info(4)
      delist(0)=0
      if ( phase .ge. 0 .and. b2n .ne. -one ) then
        if ( abs(uminsc) .ge. max(smallw,abs(b2n)/(gfn+one)*c1d)) then
          do i=nh+1,nr
            k=alist(colno(i))
            if ( -yu(i)/gresn(k) .le. -smallw
     f            ) then
              delist(0)=delist(0)+1
              delist(delist(0))=k
            endif
          enddo
        endif
      endif
c*** the new delist doesn't influence the current d but only the
c*** computation
c*** of the next del
c***************************************************************
      eqres=.true.
      do i =1,nres
        eqres=eqres .and. ( bind(i) .eq. bind0(i) )
      enddo
c
c
c*****  compute condition number estimators of diag-r and diag of
c*****  cholesky-decomposition of b
      if ( nr .gt. 1 ) then
        term=zero
        term1=one
        do i=1,nr
          term=max(term,abs(diag(i)))
          term1=min(term1,abs(diag(i)))
        enddo
        accinf(itstep,13)=term/term1
      elseif ( nr .eq. 1 ) then
        accinf(itstep,13)=1.
      else
        accinf(itstep,13)=-1.
      endif
      term=abs(a(1,1))
      term1=abs(a(1,1))
      i=2
      do while ( i .le. n )
          term=max(term,abs(a(i,i)))
          term1=min(term1,abs(a(i,i)))
          i=i+1
      enddo
      accinf(itstep,14)=(term/term1)**2
      if ( .not. silent ) call o8info(5)
c*** since a represents the cholesky-factor, this square
      slackn=zero
      do i=nh+1,nres
        slackn=slackn+
     f  max(zero,res(i)/gresn(i)-delmin)*max(zero,u(i)-smallw)/gresn(i)
      enddo
      if (  umin .ge. -smallw .and.
     f      slackn .le. delmin*smallw*nres  .and.
     f      upsi .le. nres*delmin .and. upsi0 .le. nres*delmin
     f      .and. abs(fx-fx0) .le. eps*(abs(fx)+one) .and.
     f      b2n .ne. -one .and.
     f      b2n .le. tp2*epsx*(gfn+one)    ) then
        csdifx=csdifx+1
      else
        csdifx=0
      endif
      if ( phase .ge. 0 .and.
     f     (accinf(itstep,14) .gt. tp3 .or. .not. analyt ) .and.
     f    csdifx .gt. n     ) then
          optite=nint(four)
c****     to avoid possible slow convergence with singular
c****     projected hessian or inaccurate numerical gradients
          return
      endif
c
c
c****** compute damping factor for tangential component if upsi>tau0/2
      scf0=one
      if ( phase .ge. 0 .and. upsi .gt. tau0*p5 )
     f   scf0=max(one/scfmax,
     f        (two*(tau0-upsi)/tau0)*upsi*tm1/max(one,gfn) )/scf
      accinf(itstep,15)=scf0
c
c
c************************************************************
c   compute tangential component
c************************************************************
      do i=nr+1,n
        qtx(i)=yy(i)*scf0
      enddo
c
c
c**** qtx(nr+1),..,qtx(n) is s2
c************************************************************
c   compute right hand side and vertical component
c   use damping for inactivation direction if very large
c   no indirect inactivation if infeasibility large and
c   we are not almost stationary on the current manifold
c************************************************************
      fac=one
      if ( -umin*c1d .gt. b2n+upsi .and. b2n .ne. -one ) fac=c1d
      if ( upsi .gt. tau0*p5 ) fac=zero
      do i=1,nr
        k=alist(colno(i))
        term=res(k)
        if ( k .gt. nh .and. -yu(i) .lt. zero
     f       .and. term .gt. zero ) term=-term
        if ( k .gt. nh .and. -yu(i) .lt. zero )
     f       term=term-yu(i)*fac
        yx(i)=-term
      enddo
      call o8solt(1,nr,yx,qtx)
c**** qtx is transformed  direction of descent for phi
      call o8ht(-1,0,1,nr,n,qr,betaq,qtx,yx)
      do i=1,n
        qtx(perm(i))=yx(i)
      enddo
c**** solve l(transp)*d=qtx, l = a = cholesky-factor of b
      call o8rght(a,qtx,d,term,n)
c**** end solver
c***** compute new penalty weights : regular case
      clwold=clow
      if ( phase .ge. 0  ) call o8sce
      if ( clow .gt. clwold ) then
c*** tau_qp depends on the (new) weights
        term=w(1)
        do i=1,nres
          term=max(term,w(i))
        enddo
        tauqp=max(one,min(tauqp,term))
      endif
c
c
c
c**** compute parameter phase and stopping criterion
      if ( uminsc .lt. -smallw ) phase=min(1,phase)
      if ( .not. eqres ) phase=min(0,phase)
      if ( eqres .and. upsi .lt. tau0 ) phase=max(1,phase)
c*** rescale and project d if appropriate
      call o8cutd
c*** compute the directional derivative dirder
      call o8dird
c*** terminate if correction is small
      if ( dnorm .le. epsx*(xnorm+epsx) .and. upsi .le. delmin
     f     .and. b2n .ne. -one
     f     .and. uminsc .ge. -smallw .and. b2n .le. epsx*(gfn+one) )
     f     then
        optite=nint(one)
        return
      endif
  350 continue
c***** reenter from the singular case: dirder has been computed already
      accinf(itstep,16)=xnorm
      accinf(itstep,17)=dnorm
      accinf(itstep,18)=phase
c***** compute stepsize
      cfincr=icf
c****
c****
c**** if no descent direction is obtained, check whether restarting
c**** the method
c**** might help
      if ( dirder .ge. zero  ) then
c**** no direction of descent
        if ( .not. silent ) call o8msg(11)
        stptrm=-two
        sig=zero
        goto 360
      endif
c**** if directional derivative correct but very small, terminate
c**** since no further progress might be possible
      if (-dirder.le.epsmac*tp2*(scf*abs(fx)+psi+one))then
        if ( upsi .gt. delmin*nres ) then
          optite = -nint(one)
          stptrm = -one
        else
          optite = nint(two)
          stptrm = one
        endif
        sig = zero
        goto 360
      endif
c*** phase = 2 : we may hope to obtain superlinear convergence
c*** switch to maratos-correction is on then
c*** return to phase=1 if first order correction large
      if ( phase .ge. 1
     f .and. dnorm .le. smalld*(xnorm+smalld) .and. scf0 .eq. one .and.
     f     uminsc .ge. -smallw  .and. .not. singul) phase=2
      if ( phase .eq. 2 .and. dnorm .gt. (xnorm+smalld) )
c*** return to phase 1 since correction large again
     f     phase=1
c
c
      call o8smax
c*** stmaxl is the maximal stepsize such that point on projected
c*** ray changes with sigma, but sigla at most
      do i=1,n
        dd(i)=zero
        x1(i)=x(i)+d(i)
      enddo
c**** correction added . error message from rsi-france
      viobnd=.false.
      do i=1,n
        if ( (llow(i) .and. x1(i) .lt. ug(i)-taubnd) .or.
     *    (lup(i) .and. x1(i) .gt. og(i)+taubnd) ) viobnd=.true.
      enddo
c**** end correction
c**** compute second order correction of infeasibility if useful
      if ( phase .eq. 2 .and. dnorm .gt. xnorm*sqrt(epsmac)
     f  .and. (.not. singul)  .and. (.not. viobnd) ) then
c       if ( bloc ) call user_eval(x1,-1)
c***** only function values, from xtr=xsc*x1
        do i=1,alist(0)
          yx(i)=zero
          if ( i.le. nh .and. .not. gconst(alist(i)) ) then
            cfuerr(i)=.false.
            call esh(i,x1,yx(i))
            if ( cfuerr(i) ) goto 355
          else
            if ( .not. gconst(alist(i)) ) then
              cfuerr(alist(i))=.false.
*              call esg(alist(i)-nh,x1,yx(i))
               call esg(alist(i)-nh,x1,yx(i),ifunc1,ifunc2,yyy,zzz,tf,
     *         tp,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
              if ( cfuerr(alist(i)) ) goto 355
            endif
          endif
          yx(i)=-yx(i)
        enddo
        do i=1,alist(0)
          yy(i)=yx(colno(i))
        enddo
        call o8solt(1,nr,yy,dd)
        do i=nr+1,n
          dd(i)=zero
        enddo
        call o8ht(-1,0,1,nr,n,qr,betaq,dd,yx)
        do i=1,n
          dd(perm(i))=yx(i)
        enddo
        call o8rght(a,dd,dd,term,n)
        if ( sqrt(term) .gt. p5*dnorm ) then
c*** second order correction almost as large as first order one:
c*** not useful
          do i=1,n
            dd(i)=zero
          enddo
          if ( .not. silent ) call o8msg(6)
        endif
      endif
  355 continue
      if ( .not. silent ) call o8info(7)
c
c
      sig=min(one,stmaxl)
      call o8unim(sig,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,sg,par,bnd,
     *           gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
c
c
  360 continue
      cfincr=icf-cfincr
      if ( .not. silent ) call o8info(10)
c***** count successive small steps
      term=scf*(fx0-fx)+psi0-psi
      if ( abs(term) .le. epsphi*(scf*abs(fx)+psi) ) then
       csmdph=csmdph+1
      else
       csmdph=0
      endif
c**** csmdph counts contiguous small differences of penalty
c**** function phi
      if ( csmdph .gt. numsm ) then
        optite=nint(seven)
        return
      endif
      if ( sig .le. five*tm2 ) then
        if ( sig0 .le. five*tm2 ) csssig=csssig+1
      else
        csssig=0
      endif
c***
c***  csssig counts the number of successive small sig's
c***
      accinf(itstep,21)=sig
      accinf(itstep,22)=cfincr
      accinf(itstep,23)=dirder
      accinf(itstep,24)=dscal
      accinf(itstep,25)=cosphi
      accinf(itstep,26)=violis(0)
      if ( sig .eq. zero .and. stptrm .eq. one .and.
     f     optite .eq. two ) then
c******* no further significant progress possible
         if ( .not. silent ) call o8info(17)
         return
      endif
      if ( stptrm .eq. one .and. sig .le. tm4 .and.
     f     accinf(itstep,13) .gt. tp4 .and. .not. singul
     f    .and. nres .gt. 0) then
c*** try a regularized step, hopefully this will give a better d
c    and larger sig
           if ( accinf(itstep,14) .gt. tp4 ) call o8inim
           ident=.true.
           singul=.true.
           goto 400
      endif
      if ( stptrm .lt. zero ) then
c******* stepsize selection failed
        if (  .not. ident )
     f    then
c******* try restart with a = identity scaled
          if ( .not. silent ) call o8msg(7)
          ident=.true.
          delist(0)=0
          violis(0)=0
          csreg=0
          csssig=0
          csirup=0
          call o8inim
          alist(0)=nrbas
          do i=1,nres
            bind(i)=bindba(i)
          enddo
          if ( upsi .ge. tau0 ) then
            goto 100
          else
            goto 200
          endif
        endif
        if ( .not. singul .and. ident .and.
     f     accinf(itstep,13) .gt. tp4 .and. nres .gt. 0 ) then
c**** try the full sqp-direction
c**** this may be the third try for this point
          singul=.true.
          ident=.true.
          goto 400
        endif
        if ( stptrm .eq. -two ) then
          optite=-nint(four)
          return
        endif
        if ( sig .eq. zero .and. optite .eq. -one ) return
c***** unidimensional search unsuccessfully terminated
        optite=-nint(two)
        return
      endif
c*****
      if ( singul .and. itstep .gt. n .and. abs(fx-fx0) .le.
     f     eps*(abs(fx)+one) .and. phase .ge. 0
     f     .and. upsi .le. nres*delmin  .and. upsi0 .le. nres*delmin
     f     .and. slackn .le. delmin*smallw*nres
     f     .and. infeas .le. upsi
     f     .and. .not. ident ) then
c****** since multipliers may be incorrect for infeas .ne. zero be
c****** careful. if multipliers are important check in solchk
        optite=nint(four)
c****** avoid slow progress in case of singular constraints ****
        return
      endif
c****** relaxed termination criteria in the singular case
      if ( singul .and. upsi .le. delmin*nres .and. upsi0 .le.
     f     delmin*nres
     f   .and.  b2n .ne. -one
     f   .and.  b2n .le. (gfn+one)*epsx*tp2 .and. phase .ge. 0
     f   .and. slackn .le. delmin*smallw*nres
     f   .and. infeas .le. upsi ) then
c****** since multipliers may be incorrect for infeas .ne. zero
c****** be careful. if multipliers are important check in solchk
c******
        optite=nint(three)
        return
      endif
      k=0
      do i=1,n
        if ( abs(difx(i)) .ge. epsx*(abs(x(i))+tm2)) k=1
      enddo
      if ( k .eq. 0  ) then
        cschgx=cschgx+1
      else
        cschgx=0
      endif
      if ( cschgx .gt. nreset .and. singul ) then
c**** very slow progress in x in the singular case. terminate
        optite=nint(five)
        return
      endif
c*********** new value of x has been accepted
      xnorm=o8vecn(1,n,x)
      ident=.false.
      call o8egph(gphi0)
      do i=0,nres
        if ( .not. gconst(i) )  val(i)=.false.
      enddo
c
c      if ( bloc ) call user_eval(x,2)
c     evaluate gradients only, since function values are already
c     valid from unidimensional minimization
c     argument is xtr=xsc*x
      if ( phase .ge. 0 .and. .not. gconst(0) ) then
        val(0)=.true.
        call esgradf(x,gradf)
      endif
      do i=1,alist(0)
        l=alist(i)
        if ( .not. val(l) ) then
          val(l)=.true.
          if ( l .le. nh ) then
            call esgradh(l,x,yx)
          else
c            call esgradg(l-nh,x,yx)
            call esgradg(i-nh,x,yx,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,
     *      sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
          endif
          do j=1,n
            gres(j,l)=yx(j)
          enddo
          gresn(l)=max(one,o8vecn(1,n,yx))
        endif
      enddo
      call o8egph(gphi1)
      do i=1,n
        yx(i)=x(i)-x0(i)
        yy(i)=gphi1(i)-gphi0(i)
      enddo
c**** since a represents the cholesky-factor, this sqrt
      term=sqrt(o8vecn(1,n,yy)/o8vecn(1,n,yx))
      if ( term .ne. zero .and. phase .ge. 0 )
     f      matsc=max(one/scfmax,min(scfmax,term/2))
c**** current scaling of identity in case of restart
c
      alist(0)=0
      do i=1,nres
        u0(i)=u(i)
        bind0(i)=bind(i)
        res0(i)=res(i)
        if ( i.le. nh ) then
          bind(i)=1
        else
          bind(i)=0
          if ( res(i)/gresn(i) .le. delmin ) bind(i)=1
          if ( .not. val(i) .and. bind(i) .eq. 1 ) then
            val(i)=.true.
c            call esgradg(i-nh,x,yx)
            call esgradg(i-nh,x,yx,ifunc1,ifunc2,yyy,zzz,ex,tf,tp,
     *      sg,par,bnd,gam,dgx,gdgx,alp,igt,lgp,ids,ipa,ib,izx,ixz)
            do j=1,n
              gres(j,i)=yx(j)
            enddo
            gresn(i)=max(one,o8vecn(1,n,yx))
          endif
        endif
        if ( bind(i) .eq. 1 ) then
          alist(0)=alist(0)+1
          alist(alist(0))=i
        endif
      enddo
c**** define evaluation sequence for constraints:
c**** nonbinding inequalities in ascending order of value
c**** before  the binding ones (commented here,
c**** too expensive for ng large)
csort      k=0
csort      do i=1,nres
csort        if ( bind(i) .eq. 0 ) then
csort          k=k+1
csort          sort(k)=i
csort          work(k)=res(i)
csort        endif
csort      enddo
csort      do i=1,alist(0)
csort        k=k+1
csort        sort(k)=alist(i)
csort        work(k)=res(alist(i))
csort      enddo
csort      do i=1,nres-alist(0)
csort        term=work(i)
csort        j=i
csort        do k=i+1,nres-alist(0)
csort          if ( work(k) .lt. term )  then
csort            term=work(k)
csort            j=k
csort          endif
csort        enddo
csort        k=sort(i)
csort        sort(i)=sort(j)
csort        sort(j)=k
csort        term=work(i)
csort        work(i)=work(j)
csort        work(j)=term
csort      enddo
c**** bind now corresponds to the state of the new point
c**** but there may be gradients evaluated at the new point
c**** not yet put to bind
c****
c**** update the unprojected quasi-newton-matrix anyway
c****
      if ( scf .ne. zero ) then
       if ( csirup .gt. nreset .or. csssig .gt. nreset
     f      .or. csreg  .gt. nreset
     f     ) then
         csreg=0
         csssig=0
         csirup=0
         call o8inim
       else
         call o8bfgs
c*** for projected update:   if ( .not. silent ) call o8info(13)
c*** for pantoja&mayne update:
         if ( .not. silent ) call o8info(14)
       endif
      endif
c****
c**** proceed
c****
      if(accinf(itstep,27) .eq. one ) then
        if( itstep .gt. 1 .and.
     f    accinf(itstep-1,29).ne.zero.and.accinf(itstep,29).ne.zero)
     f  then
c*** c ount s uccessive ir regular up dates
          csirup=csirup+1
        else
          csirup=0
        endif
      endif
c**** accinf(itstep,27) =1     update  pantoja&mayne
c****                   =0     noupdate
c****                   =-1    restart
c****                   =2     normal bfgs (nr=0)
c****                   =3     powell's modified bfgs (nr=0)
c**** accinf(itstep,28) =      modification term tk/ den2/den1  resp.
c**** accinf(itstep,29) =      modification factor th/xsik resp.
c**** csirup counts the number of successive irregular updating steps
      if ( phase .eq. -1 ) then
        goto 100
      else
        goto 200
      endif
  400 continue
      singul=.true.
      phase=min(phase,0)
      accinf(itstep,10)=one
c************ try to compute a descent direction using
c************ an extended quadratic program with
c************ individual slack variable for any constraint
c****** compute damping factor for tangential component if upsi>tau0/2
c****** by rescaling f if possible
      scf0=one
      if ( phase .ge. 0 .and. upsi .gt. tau0*p5 ) then
        scfh=max(one/scfmax,
     f      min(scfmax,
     f          (two*(tau0-upsi)/tau0)*upsi*tau/max(one,gfn) ) )
        if ( (fxst-fx)*scfh+scfh/scf*(psist-psi) .ge.
     f     scfh/scf*eta*clow .and. lastch .le. itstep-4
     f     .and. scfh .lt. tm1*scf .or. scfh .gt. tp1*scf )then
c*** rescale the objective function if this seems promising and the
c*** change is significant
          clow=clow+1
          term=scfh/scf
          scf0=term
          psi=psi*term
          psist=psist*term
          do i=1,nres
            u(i)=u(i)*term
          enddo
          unorm=unorm*term
          scf=scfh
          lastch=itstep
          accinf(itstep,15)=scf
          term=sqrt(term)
          do  i=1,n
            diag0(i)=term*diag0(i)
            do  j=1,n
              a(j,i)=a(j,i)*term
            enddo
          enddo
          matsc=matsc*term
          if ( .not. silent ) call o8msg(2)
        endif
      endif
c***  slack is upsi at most
      accinf(itstep,32)=upsi
      if ( .not. silent ) call o8info(15)
c***
c***
      accinf(itstep,13)=-one
      term=abs(a(1,1))
      term1=term
      i=2
      do while ( i .le. n )
          term=max(term,abs(a(i,i)))
          term1=min(term1,abs(a(i,i)))
          i=i+1
      enddo
      accinf(itstep,14)=(term/term1)**2
      if ( .not. silent ) call o8info(5)
      clwold=clow
c*** save for restart
      tauqp0=tauqp
      do i=1,nres
        u(i)=zero
      enddo
      do i=1,n
        dd(i)=zero
      enddo
c
c
      call o8qpdu
      if ( dnorm .eq. zero .and. qpterm .eq. 1
     f    .and. optite .eq. three  ) return
      if ( dnorm .le. epsx*(min(xnorm,one)+epsx) .and.
     f     qpterm .lt. 0  )
     f  then
c*** may be it failed because of illconditioning
        if ( upsi .ge. nres*delmin
     f      .and. qpnew ) then
c*** restarting the method has been done already: game is over
          optite=-nint(one)
          if ( .not. silent ) call o8info(18)
          return
        endif
        if ( qpnew ) then
          optite=nint(qpterm-five)
          if ( .not. silent ) call o8info(18)
          return
        endif
c*** try a=id
          qpnew=.true.
          do i=1,nres
            w(i)=one
          enddo
          lastch=itstep
          call o8inim
          ident=.true.
c formerly tauqp=tauqp0
          tauqp=one
          alist(0)=nrbas
          do i=1,nres
            bind(i)=bindba(i)
          enddo
          if ( scf .eq. zero ) then
            goto 100
          else
            goto 200
          endif
      endif
      accinf(itstep,11)=zero
      delist(0)=0
      umin=zero
c*** b2n is defined also internally in o8qpdu
      call o8shms
      if ( (qpterm .ge. 0 .or. qpterm .eq. -3)
     f         .and. scf .ne. zero ) then
        unorm=abs(u(1))
        do i=2,nres
          unorm=max(unorm,abs(u(i)))
        enddo
        do i=1,n
          yx(i)=scf*gradf(i)
          do j=1,nres
            yx(i)=yx(i)-gres(i,j)*u(j)
          enddo
        enddo
        b2n=o8vecn(1,n,yx)/scf
      else
        b2n=-one
c*** signals "undefined" here
      endif
      if ( .not. silent ) call o8info(2)
      if ( .not. silent ) call o8info(16)
c*** to avoid termination
      if ( b2n .eq. -one ) b2n=epsmac/tolmac
      if ( qpterm .ge. 0
     f   .and. dnorm .le. tm2*epsx*(epsx+min(one,xnorm)))
     f   then
           if ( upsi .le. nres*delmin ) then
             optite=nint(six)
           else
             optite=-nint(five)
           endif
           return
      endif
c***
c***
c**** check whether qpsolver terminated unsuccessfully
      if ( qpterm .lt. 0 ) then
c*** we have a unfeasible  solution for qp. try it
c*** but don't complain if it fails
          if ( .not. silent ) call o8msg(10)
      endif
      if ( clow .gt. clwold ) then
        term=one
        do i=1,nres
          term=max(term,w(i))
        enddo
        tauqp=max(one,term)
      endif
      if ( tauqp .gt. taufac**3*tauqp0 )tauqp=taufac**3*tauqp0
c*** no change of tauqp otherwise
      b2n0=b2n
      umin=zero
      uminsc=zero
      if ( qpterm .ge. 1 ) then
        slackn=zero
        do i=nh+1,nres
          slackn=slackn+
     f max(zero,res(i)/gresn(i)-delmin)*max(zero,u(i)-smallw)/gresn(i)
        enddo
      else
c*** slack is undefined, since multipliers are undefined.
c*** use this value to prevent premature termination
        slackn=one
      endif
      goto 350
      end subroutine o8opti
