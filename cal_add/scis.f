c***********************************************************************
c      subroutine scis(mc,y1,igfx,btdes,alphar,fltf,ind,iboun)
      subroutine scis(not,ngf,nry,nsm,npr,ntl,icl,nin,ind,iboun,cov,stp,
     *                mc,y1,igfx,btdes,alphar,fltf,iifail)
c***********************************************************************
      implicit   none

      integer*4 maxset
      parameter(maxset=1000)
      real*8,allocatable::alpha(:,:),ptable(:,:),amat(:,:),stable(:,:)
      real*8,allocatable::avec(:),bvec(:),ro(:,:),rrr(:),rvec(:)
      real*8,allocatable::stpsav(:),cum(:),cum2(:)
      integer*4,allocatable::icset(:,:),nset(:),iicset(:),next(:)
      integer*4,allocatable::iord(:),itst(:)
c
      integer*4 ngf,nry,nsm,npr,ntl,nin,not,icl,iifail(*)
      real*8 cov,stp
      integer*4 mc(ntl,2),igfx(ngf,3)
      logical fltf(ngf)
      real*8 y1(nry),alphar(nry,ngf),btdes(ngf)
      integer*4 ind
c
      real*8 pf,btg,pl3,pu3,yl3,yu3,pff,cvl,cvu,cvar,rdum1,rdum2
      real*8 rdum,dummy(1)
      integer*4 i,j,iopt,isys,ncset,iflag,ier,iset,jset,nnset
      integer*4 min,max,ifail,nind,idum,ntotal,nn
      integer*4 maxcvli,maxcvlj,maxcvui,maxcvuj,ii,jj,iboun
c
      allocate(iicset(maxset))
      allocate(icset(maxset,maxset))
      allocate(ptable(maxset,maxset))
      allocate(stable(maxset,maxset))
      allocate(next(maxset))
      allocate(nset(maxset))
      allocate(alpha(nry,ngf))
      iopt = 1
c
c     ===== preparation for scis =====
c
      if(ind.eq.0) then
c
        allocate(avec(ngf))
        allocate(bvec(ngf))
        allocate(ro(ngf,ngf))
        allocate(rrr(ngf))
        allocate(amat(ngf,ngf))
        allocate(iord(ngf))
        allocate(rvec(maxset))
c
        do i=1,ngf
        do j=1,nry
          alpha(j,i)=alphar(j,i)
        enddo
        enddo
c
c       construct cut set data
c
        call sciscut(not,ngf,igfx,iabs(icl),ntl,mc,
     *               isys,ncset,nset,maxset,icset,fltf)
c
        if(isys.eq.1.or.isys.eq.2) then
          call scprep(not,nset(1),icset(1,1),nry,alpha,ngf,ro,igfx(1,1),
     *                isys,btdes,bvec,nind,iord,ngf,amat,rvec)
          if(nind.eq.1.or.nset(1).eq.2) then
            call scsiana(not,nind,nset(1),isys,bvec,rvec,pf)
          else
c           ----- general -----
c           3. scis simulation
            iflag=0
            call sciseng(nset(1),ngf,not,npr,nsm,iopt,iflag,
     *                 isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                 nind,ngf,amat,0,idum,rdum,rdum1,rdum2)
          endif
          pff=1.0d0-pf
          call dnormi(btg,pff,ier)
          write(not,'(1h ,''  '')')
          write(not,'(1h ,''----- system reliability -----'')')
          write(not,'(1h ,''probability of failure ........'',1pe15.5)')
     *                            pf
          write(not,'(1h ,''generalized reliability index..'',1pe15.5)')
     *    btg
c          deallocate(amat)
        else
c         general system need to build probability table
          ntotal=ncset*(ncset+1)/2
          allocate(itst(ntotal))
          allocate(stpsav(ntotal))
          allocate(cum(ntotal))
          allocate(cum2(ntotal))
          nn=0
          do i=1,ncset
            iset=i
            do j=i,ncset
              nn=nn+1
              jset=j
              if(iset.eq.jset) then
                write(not,'(1h ,''  '')')
                write(not,'(1h ,''<< cutset'',i5,''>>'')')i
                call scprep(not,nset(iset),icset(1,iset),nry,alpha,ngf,
     *                      ro,igfx(1,1),isys,btdes,bvec,nind,iord,ngf,
     *                      amat,rvec)
                if(nind.eq.1.or.nset(iset).eq.2) then
                  call scsiana(not,nind,nset(iset),isys,bvec,rvec,pf)
                  cvar=0.0d0
                else
                  call sciseng(nset(iset),ngf,not,npr,nsm,iopt,0,
     *                     isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                     nind,ngf,amat,
     *                     0,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                endif
                ptable(i,j)=pf
                stable(i,j)=(pf*cvar)**2
              else
                write(not,'(1h ,''  '')')
                write(not,'(1h ,''<< cutset of'',2i5,
     *                          ''>>'')')i,j
                if(icl.eq.-3) then
                  ifail=iifail((j-1)*ncset+i)
                endif
                if(ifail.eq.0) then
                call scmkbim(0,ngf,nset(iset),icset(1,iset),
     *             nset(jset),icset(1,jset),nnset,iicset,ifail)
                endif
                if(ifail.eq.1) then
                  write(not,'(1h ,''  '')')
                  write(not,'(1h ,'' -- infeasible cut set'',
     *                            '' porb=0.0 --'')')
                  ptable(i,j)=0.0d0
                  stable(i,j)=0.0d0
                else
                  call scprep(not,nnset,iicset,nry,alpha,ngf,ro,
     *            igfx(1,1),isys,btdes,bvec,nind,iord,ngf,amat,rvec)
                  if(nind.eq.1.or.nnset.eq.2) then
c                    call scsiana(not,nind,nset(iset),isys,bvec,rvec,pf)
                    call scsiana(not,nind,nnset,isys,bvec,rvec,pf)
                    cvar=0.0d0
                  else
                    call sciseng(nnset,ngf,not,npr,nsm,iopt,0,
     *                     isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                     nind,ngf,amat,
     *                     0,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                  endif
                  ptable(i,j)=pf
                  stable(i,j)=(pf*cvar)**2
                endif
              endif
            enddo
          enddo
c
          do i=2,ncset
            do j=1,i-1
              ptable(i,j)=ptable(j,i)
              stable(i,j)=stable(j,i)
            enddo
          enddo
c
c         ----- order information -----
  900     continue
          write(not,'(1h ,''  '')')
          write(not,'(1h ,''<<probability table>>'')')
          write(not,'(1h ,5x,15i15)') (i,i=1,ncset)
          do i=1,ncset
          write(not,'(1h ,i5,1p15e15.5)') i,(ptable(i,j),j=1,ncset)
          enddo
          write(not,'(1h ,''  '')')
          write(not,'(1h ,''<<variance table>>'')')
          write(not,'(1h ,5x,15i15)') (i,i=1,ncset)
          do i=1,ncset
          write(not,'(1h ,i5,1p15e15.5)') i,(stable(i,j),j=1,ncset)
          enddo
          call scbsort(0,ncset,maxset,ptable,min,max,next)
          call sciboun(ncset,maxset,ptable,stable,
     *                 min,next,pl3,pu3,cvl,cvu,
     *                 maxcvli,maxcvlj,maxcvui,maxcvuj)
          if(pl3.gt.1.d0) pl3=1.d0
          if(pu3.gt.1.d0) pu3=1.d0
          call dnormi(yl3,pl3,ier)
          call dnormi(yu3,pu3,ier)
          write(not,1060) pl3,pu3,-yl3,-yu3,cvl,cvu
c
          if(iboun.ne.0) then
            if(cvl.gt.cov.or.cvu.gt.cov) then
              if(cvl.gt.cov) then
                ii=maxcvli
                jj=maxcvlj
                if(ii.gt.jj) then
                  jj=maxcvli
                  ii=maxcvlj
                endif
                nn=(ii-1)*ii/2+jj
                if(ii.eq.jj) then
                  write(not,'(1h ,''  '')')
                  write(not,'(1h ,''<< cutset'',i5,''>>'')')i
                  call scprep(not,nset(ii),icset(1,ii),nry,alpha,ngf,ro,
     *                    igfx(1,1),isys,btdes,bvec,nind,iord,ngf,
     *                    amat,rvec)
                  call sciseng(nset(iset),ngf,not,npr,nsm,iopt,0,
     *                    isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                     nind,ngf,amat,
     *                     1,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                else
                  write(not,'(1h ,''  '')')
                  write(not,'(1h ,''<< cutset of'',2i5,
     *            ''>>'')')ii,jj
                  call scmkbim(0,ngf,nset(ii),icset(1,ii),
     *               nset(jj),icset(1,jj),nnset,iicset,ifail)
                  call scprep(not,nnset,iicset,nry,alpha,ngf,ro,
     *               igfx(1,1),isys,btdes,bvec,nind,iord,ngf,amat,rvec)
                  call sciseng(nnset,ngf,not,npr,nsm,iopt,0,
     *               isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *               nind,ngf,amat,
     *               1,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                endif
                ptable(ii,jj)=pf
                ptable(jj,ii)=pf
                stable(ii,jj)=(pf*cvar)**2
                stable(jj,ii)=(pf*cvar)**2
              endif
              if(cvu.gt.cov) then
                if(maxcvui.ne.maxcvli.or.maxcvuj.ne.maxcvlj) then
                  ii=maxcvui
                  jj=maxcvuj
                  if(ii.gt.jj) then
                    jj=maxcvui
                    ii=maxcvuj
                  endif
                  nn=(ii-1)*ii/2+jj
                  if(ii.eq.jj) then
                    write(not,'(1h ,''  '')')
                    write(not,'(1h ,''<< cutset'',i5,''>>'')')i
                    call scprep(not,nset(ii),icset(1,ii),nry,alpha,ngf,
     *                     ro,igfx(1,1),isys,btdes,bvec,nind,iord,ngf,
     *                     amat,rvec)
                    call sciseng(nset(iset),ngf,not,npr,nsm,iopt,0,
     *                     isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                     nind,ngf,amat,
     *                     1,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                  else
                    write(not,'(1h ,''  '')')
                    write(not,'(1h ,''<< cutset of'',2i5,
     *              ''>>'')')ii,jj
                    call scmkbim(0,ngf,nset(ii),icset(1,ii),
     *                 nset(jj),icset(1,jj),nnset,iicset,ifail)
                    call scprep(not,nnset,iicset,nry,alpha,ngf,ro,
     *               igfx(1,1),isys,btdes,bvec,nind,iord,ngf,amat,rvec)
                    call sciseng(nnset,ngf,not,npr,nsm,iopt,0,
     *                 isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *                 nind,ngf,amat,
     *                 1,itst(nn),stpsav(nn),cum(nn),cum2(nn))
                  endif
                  ptable(ii,jj)=pf
                  ptable(jj,ii)=pf
                  stable(ii,jj)=(pf*cvar)**2
                  stable(jj,ii)=(pf*cvar)**2
                endif
              endif
              goto 900
            endif
          endif
          deallocate(itst)
          deallocate(stpsav)
          deallocate(cum)
          deallocate(cum2)
        endif
        deallocate(avec)
        deallocate(bvec)
        deallocate(ro)
        deallocate(rrr)
        deallocate(amat)
        deallocate(iord)
        deallocate(rvec)
c
      elseif(ind.lt.0) then
c       ----- user specified sample problems -----
        nnset=-ind
        allocate(avec(nnset))
        allocate(bvec(nnset))
        allocate(rrr(nnset))
        allocate(ro(nnset,nnset))
        do i=1,nnset
        read(nin,*) avec(i),bvec(i)
        enddo
        do i=1,nnset
          ro(i,i)=1.0d0
        enddo
        do i=2,nnset
        read(nin,*) (ro(i,j),j=1,i-1)
        enddo
        do i=1,nnset
        do j=i+1,nnset
          ro(i,j)=ro(j,i)
        enddo
        enddo
        iflag=1
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''---- user specified cubic ---- '')')
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''----- covariance matrixc ----'')')
        do i=1,nnset
        write(not,'(1h ,1p20e15.5)') (ro(i,j),j=1,nnset)
        enddo
        write(not,'(1h ,'' '')')
        write(not,'(1h ,''----- avec value(lower bounds) -----'')')
        write(not,'(1h ,1p20e15.5)') (avec(i),i=1,nnset)
        write(not,'(1h ,'' '')')
        write(not,'(1h ,''----- bvec value(upper bounds) -----'')')
        write(not,'(1h ,1p20e15.5)') (bvec(i),i=1,nnset)
        nind=nnset
        call sciseng(nnset,nnset,not,npr,nsm,iopt,1,
     *               isys,cov,ro,stp,y1,rrr,avec,bvec,pf,cvar,
     *               nind,0,dummy,0,idum,rdum,rdum1,rdum2)
        write(not,'(1h ,''  '')')
        write(not,'(1h ,''probability......................'',1pe15.5)')
     *  pf
        deallocate(avec)
        deallocate(bvec)
        deallocate(rrr)
        deallocate(ro)
      endif
c
      deallocate(alpha)
      deallocate(iicset)
      deallocate(icset)
      deallocate(nset)
      deallocate(next)
      deallocate(ptable)
      deallocate(stable)
      return
 1060 format(/
     *' bimodal bounds:',21x,'   lower bound',2x,'    upper bound'/
     *'   probability ........................',1pe12.5,'  ,  ',1pe12.5/
     *'   generalized reliability index.......',1pe12.5,'  ,  ',1pe12.5/
     *'   coefficient of variation............',1pe12.5,'  ,  ',1pe12.5/
     *)
      end subroutine scis
