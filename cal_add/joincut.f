c**************************************************************************
      subroutine joincut(not,ntl,mc,ncset,ncomp,
     *                   ndfunc,ifunc1,ifunc2,ngf,igfx,fltf)
c**************************************************************************
      implicit   none

      integer (kind=4) :: not,ntl,mc(ntl,*),ncset,ncomp(*),ngf
      integer (kind=4) :: ndfunc,ifunc1(ndfunc,*),ifunc2(ndfunc,2)
      integer (kind=4) :: igfx(ngf,3)
      logical fltf(*)

      integer (kind=4) :: iset,icset,i,iim,isq,j

      iset=1
      icset=0
      do i=1,ntl
        iim=mc(i,1)
        if(iim.ne.0) then
          icset=icset+1
          isq=iabs(mc(i,2))
          isq=igfx(isq,2)
          if(.not.fltf(isq)) then
          write(not,'(1h ,'' fatal error in join '')')
          write(not,'(1h ,'' form is not performed'',
     *               '' for l.s.f. '',i5)') igfx(isq,1)
          stop
          endif
          if(iim.gt.0) then
            ifunc1(icset,iset)=isq
            ifunc2(icset,iset)=igfx(isq,1)
          else
            ifunc1(icset,iset)=-isq
            ifunc2(icset,iset)=-igfx(isq,1)
          endif
        else
          ncomp(iset)=icset !  number of components in this cut set
          icset=0
          iset=iset+1
        endif
      enddo
      ncset=iset-1

c     ===== print out =====

      if(ncset.eq.1) then
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== parallel system problem ===== '')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''number of components.................'',i5)')
     *     ncomp(1)
      write(not,'(1h ,''components...........................'',10i5)')
     *   (ifunc2(j,1),j=1,ncomp(1))
      else
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''===== general system problem ===== '')')
      write(not,'(1h ,'' '')')
      write(not,'(1h ,''number of cut sets...................'',i5)')
     * ncset
      do i=1,ncset
      write(not,'(1h ,''----- cutset'',i5,''  -----'')') i
      write(not,'(1h ,''number of components.................'',i5)')
     *     ncomp(i)
      write(not,'(1h ,''components...........................'',10i5)')
     *     (ifunc2(j,i),j=1,ncomp(i))
      enddo
      endif

      end subroutine joincut
