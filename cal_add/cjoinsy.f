      subroutine cjoinsy

      implicit   none

      include   'blkrel1.h'
      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'line.h'
      include   'prob.h'
      include   'simu.h'

      integer (kind=4) :: iboun, icent
      integer (kind=4) ::  n1, n2, n3, n4, n5, n6, n7, n8, n9,n10, nc,nr
      integer (kind=4) :: n11,n12,n13,    n15,n16,n17,n18,n19,n20
      integer (kind=4) :: n21,n22,n23,n24,n25,n26

      save

      nsm = 1000000
      call freei('m',nsm,1)
      call gseed(stp)
      call freer('p',stp,1)
      igf = 0
      npr = max0(nsm/20,1)
      call freei('r',npr,1)
      cov = 0.05d0
      call freer('v',cov,1)
      iboun = 0                  !
      call freei('n',iboun,1)  ! input data flag
      icent = 1
      call freei('t',icent,1)  ! input data flag

      call locate('tf  ',n1,nr,nc)
      call locate('par ',n2,nr,nc)
      call locate('bnd ',n3,nr,nc)
      call locate('tp  ',n4,nr,nc)
      call locate('ids ',n5,nr,nc)
      call locate('ib  ',n6,nr,nc)
      call locate('igt ',n7,nr,nc)
      call locate('lgp ',n8,nr,nc)
      call locate('izx ',n9,nr,nc)
      call locate('ipa ',n10,nr,nc)
      call locate('mc  ',n11,nr,nc)
      call define('x1  ',n12,nrx,1)
      call define('y1  ',n13,nry,1)
      call define('z1  ',n15,1,nry)
      call define('igx ',n16,1,ngfc)
      call locate('sg  ',n17,nr,nc)
      call locate('ex  ',n18,nr,nc)
      call locate('igfx',n19,nr,nc)
      call locate('ixz ',n20,nr,nc)
      call locate('beta',n21,nr,nc)
      call locate('alph',n22,nr,nc)
      call locate('gtor',n23,nr,nc)
      call locate('fltf',n24,nr,nc)
      call locate('xlin',n25,nr,nc)
      call locate('yo  ',n26,nr,nc)

      call joint(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     *           ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),
     *           ia(n20),ia(n21),ia(n22),ia(n23),ia(n24),ia(n25),
     *            ia(n26),iboun,icent)

      end subroutine cjoinsy
