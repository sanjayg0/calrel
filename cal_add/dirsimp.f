cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dirs0(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,mc,x1,y1,
c    *             yy,z1,sg,ex,rr,y2,id,rot,gv,ir,beta)

c     functions:
c       compute the failure probability by directional simulation method
c       and using true failure surface.

c     inpute arguments:
c       tf   : transformation matrix from z to y, y = tf * z.
c       bnd  : lower and upper bounds of x.
c       par  : distribution parameter of x.
c       tp   : deterministic parameters.
c       ids : distribution number of x.
c       ib   : codes of distribution bounds of x.
c       mc   : aray of minimun cut sets.
c       ist  : index for the option of restart of simulation.
c       nsm  : number of simulations.
c       cov  : criteria of coefficient of variation.
c       ncs  : number of minimum cut sets.
c       ntl  : total number of elements in the minimum cut sets.
c       igt  : group type
c       lgp  : location of the first element of a group in y.
c       izx  : a vector associated with z indicating the no. of the
c              corresponding x.
c       ipa  : #'s of the associated rv of distribution parameters.
c              n-th element  =  0 means parameter n is constant.

c    working aray:
c      x,y,z,ex,sg

c    calls: mdris, cytox, dsolv0, dscum0.

c    called by : cdir0.

c    last version: dec. 24, 1989 by h.-z. lin.
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dirsimp(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,mc,x1,y1,
     *            z1,sg,ex,rr,y2,id,rot,gv,ir,beta,alph,fltf,gtor,igfx,
     *            xdes,ydes,ixz,qqq,ndesign,isampl)

      implicit   none

      include   'bond.h'
      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      real    (kind=8),allocatable :: ycent(:,:)
      real    (kind=8),allocatable :: sgcent(:)
      real    (kind=8),allocatable :: alcent(:,:)
      real    (kind=8),allocatable :: alpha(:,:)
      real    (kind=8),allocatable :: btcent(:)
      real    (kind=8),allocatable :: weight(:)
      real    (kind=8),allocatable :: wlim(:)
      real    (kind=8),allocatable :: y0(:)
      logical          :: fltf(ngf)
      real    (kind=8) :: random

      integer (kind=4) :: ndesign
      real    (kind=8) :: qqq
      integer (kind=4) :: ids(nrx),ib(nrx),mc(ntl,2),ixz(*)
      integer (kind=4) :: ipa(4,nrx),izx(nry),lgp(3,nig),igt(nig)
      integer (kind=4) :: id(ngfc+2),ir(ngfc),igfx(ngf,3)
      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z1(nry),bnd(2,nrx)
      real    (kind=8) :: par(4,nrx),tp(*),rr(3,ngfc)
      real    (kind=8) :: sg(nrx),ex(nrx),y2(nry)
      real    (kind=8) :: rot(ngfc+2),gv(ngf),beta(ngf)
      real    (kind=8) :: alph(nry,ngf),gtor(ngf)

      integer (kind=4) :: isampl
      real    (kind=8) :: xdes(nrx,ngf),ydes(nry,ngf)

      integer (kind=4) :: i,ii,ij,ic,i1,i2,i4, imm,iopt, iw
      integer (kind=4) :: iexact, ics,icent, id0,idg1, ier
      integer (kind=4) :: j,j4,jfunc
      integer (kind=4) :: kpl
      integer (kind=4) :: m, m1,mm, mcend,mcst
      integer (kind=4) :: n, n1,nc, neval0, nexact
      real    (kind=8) :: amag, axp
      real    (kind=8) :: btg, bunbo
      real    (kind=8) :: cs, cum,cvar, coef
      real    (kind=8) :: dev, dist
      real    (kind=8) :: exact
      real    (kind=8) :: p2,pf,pff
      real    (kind=8) :: r,r1, rdev, rn,rnm, root, rrr
      real    (kind=8) :: sp,sy, xp,xp1, yyy

      real    (kind=8) :: cdot

      save

      allocate(ycent(nry,ngf))
      allocate(sgcent(ngf))
      allocate(weight(ngf))
      allocate(wlim(ngf))
      allocate(btcent(ngf))
      allocate(alcent(nry,ngf))
      allocate(alpha(nry,ngf))
      allocate(y0(nry))

c     ----- set x as their mean  in case they are deterministic -----


      neval = 0

      do 144 iw = 1,nrx
 144  x1(iw) = ex(iw)

      rn = real(nry)
      mcst = 1
      mcend = ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend = 2*ngfc
      endif
      if(icl.eq.1) then
        call finim(igfx,igf)
        mcst = 1
        mcend = 2
        mc(1,1) = igf
        mc(1,2) = 1
        mc(2,1) = 0
        mc(2,2) = 0
      endif

      if(ist.ne.0) then
        read(ns0,rec = 4,err=10) cum,xp1,root,id0,i1
        id(1) = id0
        n1 = i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif

      kpl = 0
      do 409 i = 1,ngfc
        i2 = igfx(i,2)
        i4 = igfx(i,3)
        if(.not.fltf(i2)) then
          root = 8.
          kpl = 1
          gtor(i2) = 0.0001
        endif
  409 continue

      if(kpl.eq.0) call droot(beta,alph,mc,y1,z1,ir,root,gv,igfx,
     *                1,mcst,mcend)
c      root = 8.0

      rdev = 1.0d0
      dist = 0.0d0
      call ncent(6,icl,ntl,mc,nrx,nry,nig,ngf,ntf,ngfc,ids,
     *           ipa,izx,ixz,ib,lgp,igt,
     *           xdes,beta,z1,bnd,tf,tp,par,sg,ex,ydes,alpha,
     *           weight,wlim,rdev,dist,
     *           nc,ycent,alcent,btcent,sgcent,qqq,ndesign,
     *           alph,fltf,igfx(1,1),isampl)

      exact = 0.0d0
      iexact = 0
      nexact = 0
      i1 = 0
      cum = 0.d0
      xp1 = 0.d0
      n1 = 0

c.... check the origin is in the failure domain or not.
      do 45 j = 1,nry
 45   y1(j) = 0.d0
      call cytox(x1,y1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      do 46 j = 1,ngfc
        j4 = igfx(j,3)
        call ugfun(gv(j),x1,tp,j4)
        if(gv(j).lt.0) then
          ir(j) = 0
        else
          ir(j) = 1
        endif
 46   continue
      imm = 1
      idg1 = 1
      do 1100 i = mcst,mcend
        if(mc(i,1).ne.0) then
          ic = mc(i,2)
          if(mc(i,1).lt.0) then
          imm = imm*ir(ic)
          else
          imm = imm*(1-ir(ic))
          endif
        else
          ics = 1-imm
          idg1 = idg1*ics
          imm = 1
        endif
 1100 continue
      idg1 = 1-idg1
      if(idg1.eq.1) then
      id(1) = 1
      else
      id(1) = 0
      endif

   30 n = npr
      n1 = n+n1
      i1 = i1+1
      xp = 0.d0
      write(not,2010)

c.... perform monte carlo simulation
      iopt  =  1
      do 50 j = i1,nsm
        axp = 0.d0
        sy = 0.d0
c---------- select the design point ----------------
        call gguw(stp,1,iopt,rrr)
        iopt  =  0
        if(rrr.le.qqq) then
          icent = 0
        else
          do jfunc = 1,ngfc-1
            if(rrr.le.wlim(jfunc)) goto 17
          enddo
          icent = ngfc
          goto 27
   17    icent = jfunc
   27    continue
        endif
c---------- select the design point ----------------

c-------- generate n-1 dimensional values -----
        do 160 m = 1,nry
          call gguw(stp,1,iopt,random)
          iopt  =  0
          r  =  random
          call dnormi(y1(m),r,ier)
  160   continue
        if(icent.ne.0) then
          amag = cdot(y1,alcent(1,icent),nry,nry,nry)
          do i = 1,nry
            y0(i) = y1(i)-amag*alcent(i,icent)
          enddo
          call dnorm(-btcent(icent),pf)
          call gguw(stp,1,iopt,random)
          iopt = 0
          r = 1.0d0-(1.0d0-random)*pf
          call dnormi(yyy,r,ier)
          do i = 1,nry
            y1(i) = y0(i)+yyy*alpha(i,icent)
          enddo
        endif

c-------- detarmine n th value -----

        do 70 m = 1,nry
          sy = sy+y1(m)*y1(m)
   70     continue
        sy = dsqrt(sy)
        do 80 m = 1,nry
   80     y1(m) = y1(m)/sy

c        make coeff

        bunbo = qqq
        do jfunc = 1,nc
          call dnorm(-btcent(jfunc),pf)
          amag = cdot(y1,alcent(1,jfunc),nry,nry,nry)
          if(amag.gt.0.0d0) then
            amag = (btcent(jfunc)/amag)**2
            call dchis(amag,rn,p2,ier)
            bunbo = bunbo+weight(jfunc)*(1.0d0-p2)/pf
          endif
        enddo
        coef = 1.0d0/bunbo

c.....use performance function to determine r
c      do 190 kj = 1,2
c      if(kj.eq.2) then
c      do 85 m = 1,nry
c 85   y1(m) = -y1(m)
c      endif
        call dsolv0(tp,y1,x1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     *            izx,rr,y2,gv,ir,root,gtor,igfx)
c.....count total number of roots.
      m1 = 1
      rot(1) = 0.d0
      do 90 i = 1,ngfc
        if(ir(i).ne.0) then
        do 94 ij = 1,ir(i)
          m1 = m1+1
          rot(m1) = rr(ij,i)
 94     continue
        endif
 90   continue
c.....arrange m1 roots in orderings.
      if(m1.le.1) then
        if(id(1).eq.1) axp = axp+1.d0
        go to 190
      endif
      mm = m1-1
 100  do 110 ii = 1,mm
        if(abs(rot(ii)).gt.abs(rot(ii+1))) then
          r1 = rot(ii)
          rot(ii) = rot(ii+1)
          rot(ii+1) = r1
        endif
 110  continue
      if(mm.gt.1) then
        mm = mm-1
        go to 100
      endif

c.....determine the ranges of the roots in the failure sets.

      if(icl.le.2) then
        if(id(1).eq.0) then
          cs = rot(2)*rot(2)
          call dchis(cs,rn,p2,ier)
          axp = axp+1.d0-p2
        else
          cs = rot(m1)*rot(m1)
          call dchis(cs,rn,p2,ier)
          axp = axp+p2
        endif
        go to 190
      endif
c       ----- general system -----
      rot(m1+1) = root
      call dscum0(y1,y2,x1,z1,tf,tp,ex,sg,par,bnd,igt,lgp,ids,
     *     ipa,ib,izx,rot,id,m1,mc,xp,ir,igfx,mcst,mcend)
      axp = axp+xp
 190  continue
c      axp = axp/2.d0
c      cum = cum+axp
      cum = cum+axp*coef
c      xp1 = xp1+axp*axp
      xp1 = xp1+axp*axp*coef*coef
      if(iexact.ne.0) then
        nexact = nexact+1
        exact = exact+cum/real(j)
      endif
        if(j.ge.n1) then
          rnm = real(j)
          pf = cum/rnm
c         sp = cum-2.d0*pf*cum+rnm*pf*pf
c          sp = xp1-2.d0*pf*cum+rnm*pf*pf
c          rnm = rnm*(rnm-1.d0)
          sp = xp1-cum*cum/rnm
          sp = sp/(rnm*(rnm-1))
          pff = 1.d0-pf
          if(sp.gt.0.d0) then
            if(iexact.ne.0) goto 60
c             dev = dsqrt(sp/rnm)
            dev = dsqrt(sp)
            cvar = dev/pf
            call dnormi(btg,pff,ier)
            write(not,2020) j,pf,btg,cvar
            if(cvar.le.cov) then
              iexact = 1
              neval0 = neval
              n1 = n1+npr
            goto 50
            endif
          else
            write(not,2030) j
          endif
          n1 = n+n1
        endif
   50 continue
 60   id0 = id(1)

      write(ns0,rec = 4) cum,xp1,root,id0,min(j,nsm)

c      write(not,'(1h ,'' exact = '',e15.5)') exact/float(nexact)
c      write(not,'(1h ,'' neval = '',i15)') neval0

c---  output formats

 2010 format('     trials',8x,'pf-mean',5x,'betag-mean',4x,
     *       'coef of var')
 2020 format(i11,1p,e15.5,e15.5,e15.5)
 2030 format(i12,5x,'  -----',9x,'  ------',9x,'   ------')

      deallocate(ycent)
      deallocate(sgcent)
      deallocate(weight)
      deallocate(wlim)
      deallocate(btcent)
      deallocate(alcent)
      deallocate(alpha)
      deallocate(y0)

c      nc = 1
c      do i = 1,nry
c      ycent(i,1) = 0.0d0
c      enddo
c      do i = 1,5
c      ycent(i,1) = 1.281551d0
c      enddo
c      do i = 1,10
c      ycent(i,1) = 0.478274d0
c      enddo
c      ysum = 0.0d0
c      do i = 1,nry
c        ysum = ysum+ycent(i,1)**2
c      enddo
c      ysum = dsqrt(ysum)
c      do i = 1,nry
c        alcent(i,1) = ycent(i,1)/ysum
c      enddo
c      btcent(1) = ysum

c      wlim(1) = 1.0d0
c      weight(1) = 1.0d0

      end subroutine dirsimp
