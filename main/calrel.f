      program calrel

!--------------------------------------------------------------------------
!           U n i v e r s i t y   o f   C a l i f o r n i a
!                 Department  of  Civil  Engineering

!                          C  A  L  R  E  L
!                       CAL-RELiability program
!                            Developed by
!               P.-L. Liu, H.-Z. Lin and A. Der Kiureghian

!                      Last Revision: July 2017
!-----------------------------------------------------------------------
!   Arrays  Dimension  Definition
!     xn       nrx     names of X.
!     ex       nrx     mean vector of X.
!     sg       nrx     standard deviations of X.
!     par      nrx     distribution parameters of X.
!     bnd      nrx     lower and upper bound of X.
!     tp       nrx     deterministic parameters in performance function.
!     x0       nrx     initial values of X.
!     tf       nrx     the transformation matrix from Z to Y, i.e. Y = tf * Z.
!     igt      nrx     group types.
!     lgp      nrx     location of the first element of a group in Y.
!     ids      nrx     distribution numbers of X.
!     ipa      nrx     #'s of the associated rv of distribution parameters.
!                      n-th element of ipa = 0 means parameter n is constant.
!     ib       nrx     code of distribution bounds of X.
!                      = 0 , no bounds
!                      = 1 , bounded from below.
!                      = 2 , bounded from above.
!                      = 3 , both sides bounded.
!     izx      nrx     indicators for Z to X.
!     ixz      nrx     indicators for X to Z.
!     beta     nrx     reliability indices for all failure modes.
!     xlin     nrx     design points for all failure modes.
!     alph     nrx     unit normal in Y space for all failure modes.
!     gamma    nrx     unit normal in Z space for all failure modes.
!     det      nrx     normalized sg*d(beta)/d(ex)
!     eta      nrx     normalized sg*d(beta)/d(sg)
!     x        nrx     basic random variables in the original space.
!     y        nrx     basic variables in the standard space.
!     a        nrx     unit normal vector in the Y space.
!     gam      nrx     unit normal vector in the Z space.
!     cur    2*ngr*(nry-1) curvatures for each semiparobolic intersection.
!     sRt    nry*nry*ngf   rotational transf. matrix between Y and Y' space.
!     rhom   ngf*ngf   modal correlation matrix.
!     prob   ngf*ngf   bounds of joint modal failure probabilities.
!                      upper triangle - upperbounds.
!                      lower triangle - lowerbounds.
!                      diagonal - individual mode probabilities:
!     prbr   ngf*ngf   joint modal failure probability by num. integration.

!-----------------------------------------------------------------------
!   Parameter  Definition
!     icl      problem type.
!              icl = 1, component.
!              icl = 2, series system.
!     igr      flag for gradient computation.
!              igr = 0, gradient computed by finite difference.
!              igr = 1, gradient formulas provided by user.
!     ngf      number of transition function.
!     nig      number of independent groups of basic variables.
!     nrx      total number of basic variables.
!     ntp      number of deterministic parameters in transition
!              functions.
!     ncs      number of minimum cut sets.
!     ntl      total number of components in all minimum cut sets.
!     ini      initialization flag, default = 0.
!              ini = 0, Start from mean point.
!              ini = 1, Start point specified by user.
!     ist      restart flag, default = 0.
!              ist = 0, Analyze a new problem.
!              ist = 1, Continue an unconverged problem.
!     igf      no. of transition function to be analyzed, default
!              = 1. igf applies to component analysis only.
!     npr      FORM print interval, default = 0.
!              npr < 0, no first order results are printed.
!              npr = 0, output the final step of first order results.
!              npr > 0, output the results of every npr steps.
!     rho      the threshold used in PNET method, default = 0.6.
!     isc      type of sensitivity analysis required.
!              isc = 1, component sensitivity analysis.
!              isc = 2, system sensitivity analysis (applies only
!                       to series system and only after BOUN has been
!                       executed.)
!     isv      type of parameters to do analysis, default = 0.
!              isv = 1, sensitivity analysis on distribution parameters.
!              isv = 2, sensitivity analysis on deterministic parameters.
!              isv = 0, sensitivity analysis on both distribution and
!                       deterministic parameters.
!     iso      type of second order analysis scheme to be used,
!              iso = 1, point-fitting method.
!              iso = 2, curvature-fitting method.
!              iso = 0, both point-fitting and curvature-fitting methods.
!     ifs      type of analysis, default = 2 if SORM has been executed.
!              ifs = 1, first-order approximation.
!              ifs = 2, second-order approximation.
!     ist      restart flag, default = 0.
!              ist = 0, perform a new directional simulation.
!              ist = 1, continue the previous simulation.
!     nsm      total number of trials, default = 10000.
!     npr      print interval for simulation results, default = nsm/50.
!     stp      starting point in the simulation routine, default = time().
!     cov      covariance threshold for simulation, default = 0.05.
!     icl      problem type, default = 1.
!              icl = 1, component.
!              icl = 2, series system.
!              icl = 3, general system.
!     igr      flag for gradient computation; default = 0.
!              igr = 0, gradient computed by finite difference scheme.
!              igr = 1, gradient formulas provided by user.
!     iop      type of optimization scheme used
!              iop = 1, HL-RF method
!              iop = 2, modified HL-RF method
!              iop = 3, gradient projection method
!     ni1      maximum number of iteration cycles; default = 100;
!              maximum = 100.
!     ni2      maximum steps in line search; default = 4.
!     tol      convergence tolerance; default = 0.001; minimum = 0.001.
!     op1      step size reduction factor in line search.
!              iop = 1, default = 1.0.
!              iop = 2 or 3, default = 0.5.
!     op2      optimization parameter
!              iop = 2, parameter c in descent function; default = 10.
!              iop = 3, convergence tolerance for line search;
!     op3      optimization parameter
!              iop = 3, Maximum step size in line search; default = 4.
!     igt      group type.
!              igt = 1, all variables in the group are independent.
!              igt = 2, variables are described by their marginal
!                       distributions and correlation matrix.
!              igt = 3, variables are described by conditional
!                       distributions and/or marginal distributions.
!-----------------------------------------------------------------------
      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'chck.h'
      include   'crel.h'
      include   'cuts.h'
      include   'dbsy.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'grad.h'
      include   'line.h'
      include   'optm.h'
      include   'para.h'
      include   'prob.h'
      include   'simu.h'

      character (len=7) :: tim(3)
      integer  (kind=4) :: time(3), i,itst, kk
      real*4 tarry(2),runtime

!     save

!---  Set available memory and initialize parameters

      call usize
      itst   = 0
      ifeap  = 0
      ifeapo = 0
      ifres  = 0
      do i = 1,5
        flc(i) = .true.
      end do ! i
      runtime = 0.0e0

!---  Activate license

      call ulicense(0)

!---  Open files.

      call copfil

!---  Start execution

      call cexect

!---  Stop execution

      call etime(tarry, runtime)
      if(runtime.gt.1.0e0) then
        tim(1)  = 'hours'
        time(1) = int(runtime/60/60)
        tim(2)  = 'minutes'
        time(2) = int((runtime-time(1)*60*60)/60)
        tim(3)  = 'seconds'
        time(3) = int(runtime-time(1)*60*60-time(2)*60)
        if(time(1).ne.0) then
          kk = 1
        else
          if(time(2).ne.0)then
            kk = 2
          else
            kk = 3
          endif
        endif

        write(*,2001) ' Running Time=',(time(i),tim(i),i=kk,3)
      else
        write(*,2002) ' Running Time=',runtime,' seconds'
      endif

 2001 format(a,3(1x,i2,1x,a))
 2002 format(a,f8.2,1x,a)

!---  Stop execution and close all files

      call cstop

      end
