      subroutine ugfun(g,x,tp,ig)

      implicit   none

      include   'counts.h'

      real    (kind=8) :: x(*),tp(*)

      real    (kind=8) :: g
      integer (kind=4) :: ig

      save

      if(ig.ge.7.and.ig.le.9) go to 70

      if(ig.ge.11.and.ig.le.20) go to 80

      go to(10,20,20,40,50,60) ig
      write(*,*)' UGFUN: UNDEFINED IG =',ig
      return

c---- Examples 1 and 19

  10  continue  
      g = x(1)**tp(1)-x(2)*tp(2)+tp(3)
      return

c---- Example 2 and Example 2.1

  20  g = tp(1) - x(2)/1000.d0/x(3) - (x(1)/200.d0/x(3))**2
      return

c---- Example 4

  40  g =tp(1) - x(2)/1000.d0/x(3) - (x(1)/200.d0/x(3))**2-x(5)/1000.d0
     *    /x(6)-(x(4)/200.d0/x(6))**2
      return

c---- Example 5

  50  g=x(3)-x(4)
      return

c---- Example 6

  60  g=(x(2)-x(4))/dsqrt(x(1)+x(3))-tp(1)
      return

c---  Examples 7 ,7.1, 7.2, 9., 11, 14, 15, 16, 17

  70  go to (71,72,73) ig-6
      return

   71 g = x(1)+x(2)+x(4)+x(5)-x(6)*tp(1)
      return
   72 g = x(1)+2.*x(3)+2.*x(4)+x(5)-x(6)*tp(1)-x(7)*tp(1)
      return
   73 g = x(2)+ 2.*x(3)+x(4)-x(7)*tp(1)
      return

c---  Example 8 

  80  go to (81,82,83,84,85,86,87,88,89,90) ig-10
      return

   81 g= x(1)-4.d0*x(3)-4.d0*x(4)
      return
   82 g= x(1)-2.d0*x(3)-7.d0*x(4)
      return
   83 g= x(2)-0.2d0*x(3)-0.7d0*x(4)
      return
   84 g=2.5d0*x(1)-10.d0*x(3)-5.d0*x(4)
      return
   85 g=x(1)-10.d0*x(4)
      return
   86 g=x(2)-0.05d0*x(1)-0.5d0*x(4)
      return
   87 g=2.d0*x(1)-5.d0*x(3)-10.d0*x(4)
      return
   88 g=-x(1)+10.d0*x(2)
      return
   89 g=x(1)-10.d0*x(3)-20.d0*x(4)
      return
   90 g=-x(1)+10.d0*x(4)
      return

      end

      subroutine udgx(dgx,x,tp,ig)

      implicit   none

      integer (kind=4) :: ig
      real    (kind=8) :: x(*),dgx(*),tp(*)

      if(ig.ge.7.and.ig.le.9) go to 70
      if(ig.ge.11.and.ig.le.19) go to 80
      if(ig.ge.21.and.ig.le.26) go to 100
      if(ig.ge.27.and.ig.le.29) go to 110
      if(ig.ge.30.and.ig.le.33) go to 130

      go to(10,20,20,40,50,60) ig
      return

c---- Example 1

c  10  continue
c      pai    = 4.0d0*datan2(1.0d0,1.0d0)
c      dgx(1) = pai*pai*x(3)*tp(1)*tp(1)/(12.0d0*(1.0d0-x(2)*x(2)))
c      dgx(2) = pai*pai*x(1)*x(3)*tp(1)*tp(1)*2.0d0*x(2)
c      dgx(2) = dgx(2)/(12.0d0*(1.0d0-x(2)*x(2)))
c      dgx(3) = pai*pai*x(1)*tp(1)*tp(1)/(12.0d0*(1.0d0-x(2)*x(2)))

  10  dgx(1) =  tp(1)*x(1)**(tp(1)-1.d0)
      dgx(2) = -tp(2)
      return

c---- Example 2 and Example 2.1

  20  return

c---- Example 4

  40  return

c---- Example 5

  50  dgx(1) =  0.d0
      dgx(2) =  0.d0
      dgx(3) =  1.d0
      dgx(4) = -1.d0
      return

c---- Example 6

  60  return

c---- Examples 7, 7.1, 7.2, 14

  70  go to (71,72,73) ig-6
   71 return
   72 return
   73 return

c---- Example 8

  80  return

c---- Example 10

 100  return

c---- Example 12

 110  return

c---- Example 13, 181

 130  return

      end

      subroutine udd(x,par,sg,ids,cdf,pdf,bnd,ib)

      implicit   none

      real    (kind=8) :: sg,cdf,pdf
      integer (kind=4) :: ids, ib
      real    (kind=8) :: x(*),par(4),bnd(2)

      real    (kind=8) :: ak,ak1,alambda,alx
      real    (kind=8) :: gamk
      integer (kind=4) :: ier
      real    (kind=8) :: p,pp 
      real    (kind=8) :: xt,xx

      save

      go to(10,20,30,40,50,60) ids-50
      return

c---  Example 5

  10  if (x(4) .lt. 0.) then
        cdf = 0.1d-14
        pdf = 0.1d-14
      else
        cdf = 1.d0-exp(-par(1)*x(4)**2)
        pdf = 2.d0*par(1)*x(4)*exp(-par(1)*x(4)**2)
      end if
      bnd(1) = 0.d0
      ib     = 1
      sg     = 21.d0
      return

c---  Example 6

   20 alambda = par(1)
      ak = par(2)
      sg = alambda/ak/sqrt(ak-1.d0)
      if(x(1).le.0.0d0) then
        pdf = 0.d0
        cdf = 0.d0
        return
      endif
      ak1  = ak + 1.d0
      gamk = gamma(ak)
      alx  = alambda/x(1)
      pdf  = alx**ak1*exp(-alx)/alambda/gamk
      xt   = 2.d0*alx
      pp   = 2.d0*ak
      call dchis(xt,pp,p,ier)
      cdf    = 1.d0-p
      ib     = 1
      bnd(1) = 0.d0
      return

   30 sg = 1.d0/sqrt(par(3)*par(2))
      xx = (x(2)-par(1))/sg
      call dnorm(xx,cdf)
      pdf = 0.39894228d0/sg*exp(-0.5d0*xx*xx)
      ib  = 0
      return

   40 alambda = par(1)
      ak = par(2)
      sg = alambda/ak/sqrt(ak-1.d0)
      if(x(3).le.0.) then
        pdf = 0.d0
        cdf = 0.d0
        return
      endif
      ak1  = ak + 1.d0
      gamk = gamma(ak)
      alx  = alambda/x(3)
      pdf  = alx**ak1*exp(-alx)/alambda/gamk
      xt   = 2.d0*alx
      pp   = 2.d0*ak
      call dchis(xt,pp,p,ier)
      cdf    = 1.d0-p
      ib     = 1
      bnd(1) = 0.d0
      return

   50 sg = 1.d0/sqrt(par(3)*par(2))
      xx = (x(4) - par(1))/sg
      call dnorm(xx,cdf)
      pdf = 0.39894228d0/sg*exp(-0.5d0*xx*xx)
      ib  = 0
      return

c---  Example 13, 18

   60 if (x(4) .lt. 0.) then
          cdf = 0.1d-14
          pdf = 0.1d-14
      else
          cdf = 1.d0 - exp(-par(1)*x(4)**2)
          pdf = 2.d0*par(1)*x(4)*exp(-par(1)*x(4)**2)
      end if
      bnd(1) = 0.d0
      ib = 1
      sg = 21.d0
      return

      end

      subroutine usize

      implicit   none

      integer         mtot,np,ia
      common /blkrel/ mtot,np,ia(1000000)

      include   'blkchr.h'

      integer (kind=4) :: i

      save

      mtot = 1000000
      do i = 1,mtot
        ia(i) = 0
      enddo
      mchar = 5000

      end  
