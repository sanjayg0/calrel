      double precision upsi,upsi0,upsi1,upsist,psi,psi0,
     &    psi1,psist,psimin,
     &    phi,phi0,phi1,phimin,fx,fx0,fx1,
     &    fxst,fmin,b2n,b2n0,xnorm,x0norm,sig0,dscal,dnorm,d0norm
      double precision sig,sigmin,dirder,cosphi,upsim
      double precision x,x0,x1,xmin,d,d0,dd,difx,resmin
      common/o8xdat/x(nx),x0(nx),x1(nx),xmin(nx),resmin(nresm),
     &             d(nx),d0(nx),dd(nx),difx(nx),
     &             xnorm,x0norm,dnorm,d0norm,sig,sig0,sigmin,dscal,
     &             upsi,upsi0,upsi1,upsist,upsim,
     &             psi,psi0,psi1,psist,psimin,
     &             phi,phi0,phi1,phimin,
     &             fx,fx0,fx1,fxst,fmin,
     &             b2n,b2n0,dirder,cosphi
