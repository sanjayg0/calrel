      double precision zero,one,two,three,four,five,six,seven,
     &    twom2,twop4,twop11,
     &    onep3,onep5,p2,p4,p5,p7,p8,p9,c45,tm12,
     &    tm10,tm9,tm8,tm7,tm6,tm5,tm4,tm3,tm2,tm1,tp1,tp2,tp3,tp4
      parameter (zero=0.d0,one=1.d0,two=2.d0,three=3.d0,four=4.d0,
     &    five=5.d0,six=6.d0,seven=7.d0,
     &    twom2=.25d0,twop4=16.d0,twop11=2048.d0,onep3=1.3d0,
     &    onep5=1.5d0,p2=.2d0,p4=.4d0,p5=.5d0,p7=.7d0,p8=.8d0,p9=.9d0,
     &    c45=45.d0,tm12=1.d-12,
     &    tm10=1.d-10,tm9=1.d-9,tm8=1.d-8,tm7=1.d-7,tm6=1.d-6,tm5=1.d-5,
     &    tm4=1.d-4,tm3=1.d-3,tm2=1.d-2,tm1=1.d-1,tp1=1.d1,tp2=1.d2,
     &    tp3=1.d3,tp4=1.d4)
