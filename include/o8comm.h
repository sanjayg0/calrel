      include 'o8para.h'
      real  runtim
      real  optite
      double precision accinf
      integer itstep,phase
      common/o8itin/optite,itstep,phase,runtim,
     &      accinf(0:maxit,32)
      include 'o8xdat.h'
      double precision gradf,gfn,qgf,gphi0,gphi1,gres,gresn
      common/o8grd/gradf(nx),gfn,qgf(nx),gres(nx,nresm),
     &        gresn(nresm),gphi0(nx),gphi1(nx)
c**
      include 'o8rdat.h'
      logical val,gconst,llow,lup
      integer gunit
      common/o8gri/val(0:nresm),gconst(0:nresm),gunit(3,0:nresm),
     &             llow(nx),lup(nx)
      logical intakt,inx,std,te0,te1,te2,te3,singul
      logical ident,eqres,silent,analyt,cold
      common/o8stpa/intakt,inx,std,te0,te1,te2,te3,singul,
     &        ident,eqres,silent,analyt,cold
      double precision a,scalm,scalm2,diag0,matsc
      common/o8qn/a(nx,nx),diag0(nx),
     &           scalm,scalm2,matsc
      integer violis,alist,bind,bind0,sort
      common/o8resi/bind(nresm),bind0(nresm),violis(0:nstep*nresm),
     &               alist(0:nresm),sort(nresm)
      double precision u,u0,w,w1,res,res0,res1,resst,scf,scf0,
     &                yu,slack,infeas,work
      common/o8resd/res(nresm),res0(nresm),res1(nresm),resst(nresm),
     &            u(nresm),u0(nresm),w(nresm),w1(nresm),work(0:nresm),
     &            yu(nresm),slack(nresm),scf,scf0,infeas
      integer n,nr,nres,nh,ng
      common/o8dim/n,nh,ng,nr,nres
      double precision epsmac,tolmac,deldif
      common/o8mpar/epsmac,tolmac,deldif
      integer iterma,ifill1
      double precision del,del0,del01,delmin,tau0,tau,ny
      double precision smalld,smallw,rho,rho1,eta,epsx,epsphi,c1d,
     &                 scfmax,updmy0,tauqp,taufac,taumax
      common/o8par/del0,del01,del,delmin,tau0,tau,
     &             smalld,smallw,rho,rho1,eta,ny,epsx,epsphi,
     &             c1d,scfmax,tauqp,taufac,taumax,updmy0,
     &             iterma,ifill1
      double precision alpha,beta,theta,sigsm,sigla,delta,stptrm
      double precision delta1,stmaxl
      common/o8step/alpha,beta,theta,sigsm,sigla,delta,stptrm,
     &              delta1,stmaxl
      integer icf,icgf,cfincr,cres,cgres
      common/o8cnt/icf,icgf,cfincr,cres(nresm),cgres(nresm)
      logical ffuerr,cfuerr(nresm)
      common/o8err/ffuerr,cfuerr
      double precision level
      integer clow,lastdw,lastup,lastch
      common/o8wei/level,clow,lastdw,lastup,lastch
      character*40 name
      common/o8id/name
      double precision epsdif
      common/o8der/epsdif
      integer prou,meu
      common/o8io/prou,meu
      double precision ug(nx),og(nx),delfac(nresm)
      common/o8bd/ug,og,delfac
      integer nreset,numsm
      common/o8rst/nreset,numsm
      double precision xst(nx)
      common/o8stv/xst
      intrinsic sqrt,exp,log,max,min,abs

