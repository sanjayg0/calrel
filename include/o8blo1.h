      include 'o8para.h'
      include 'o8xblo.h'
      logical val,gconst
      integer gunit
      common/o8gri/val(0:nresm),gconst(0:nresm),gunit(3,0:nresm)
      integer n,nr,nres,nh,ng
      common/o8dim/n,nh,ng,nr,nres
      double precision epsmac,tolmac,deldif
      common/o8mpar/epsmac,tolmac,deldif
      integer iterma,ifill
      double precision del,del0,del01,delmin,tau0,tau,ny
      double precision smalld,smallw,rho,rho1,eta,epsx,epsphi,c1d,
     &                 scfmax,wfac
      common/o8par/del0,del01,del,delmin,tau0,tau,
     &             smalld,smallw,rho,rho1,eta,ny,epsx,epsphi,
     &             c1d,scfmax,wfac,iterma,ifill
      character*40 name
      common/o8id/name
      integer icf,icgf,cfincr,cres,cgres
      common/o8cnt/
     &    icf,icgf,cfincr,cres(nresm),cgres(nresm)
      logical ffuerr,cfuerr(nresm)
      common/o8err/ffuerr,cfuerr
      integer nreset,numsm
      common/o8rst/nreset,numsm
      double precision epsdif
      common/o8der/epsdif
      integer prou,meu
      common/o8io/prou,meu
      data epsdif/1.d-8/,analyt/.true./,nreset/10/
      data prou/10/,meu/20/
