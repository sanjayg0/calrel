cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cinver(tf,n,m)
c
c   functions:
c      compute transformation matrix by inverting l.
c
c   input arguments:
c      tf : l, the transformation matrix from y to z, i.e. z  =  tf * y.
c      n : dimension of x.
c      m : dimension of tf in the calling routine.
c
c   output arguments:
c      tf : the transformation matrix from z to y, i.e. y  =  tf * z.
c
c   calls: none.
c
c   called by: input.
c
c   last revision: sept. 18 1986 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cinver(tf,n,m)

      implicit   none

      integer    n,m
      real*8     tf(m,m)

      integer    i,j,k
      real*8     tij

c---  compute tf by inverting l.

      do j = 1,n
        tf(j,j) = 1.d0/tf(j,j)
      end do ! j
      do j = 1,n
        do i = j+1,n
        tij = 0.d0
        do k = j,i-1
          tij = tij+tf(i,k)*tf(k,j)
        end do ! k
        tf(i,j) = -tij*tf(i,i)
        end do ! i
      end do ! j
      do j = 1,n-1
        do i = j+1,n
          tf(j,i) = 0.d0
        end do ! i
      end do ! j

      end subroutine cinver
