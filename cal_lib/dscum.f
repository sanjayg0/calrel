ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dscum(m1,rot,a1,a2,bt,ig,id,igfx,xp,mcst,mcend,idx)
c
c     functions:
c       compute the failure probability for each directional simulation
c       for the second order system reliability.
c
c     input arguments:
c       kj   : =1, for positive direction of this simulation line.
c              =2, for negative direction of this simulation line.
c       m1   : total no. of roots in this direction.
c       rot  : roots of the intersections of simulated line and 1st order
c              failure surfaces or 2nd order surfaces.
c       a1   : coef. of 2nd order terms in the 2nd order failure surfaces.
c       a2   : coef. of 1st order terms in the 2nd order failure surfaces.
c       bt   : reliability indexes. also are the const. terms of 2nd order
c              surfaces.(for the 1st order surfaces, a1=0 ).
c       ngf   : no. of failure modes.
c       rn   : total no. of variables, ie, no. of degree of freedoms in the
c              chi-sqare distribution.
c       mc   : aray of minimun cut sets.
c       ncs  : no of minimun cut sets.
c       ntl  : toal number of elements in all minimun cut sets  + ncs.
c
c     output arguments:
c       xp   : failure probability for this directional vector.
c
c     calls: dchis.
c
c     called by: dirs2.
c
c     last version: dec. 26, 1989 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dscum(m1,rot,a1,a2,bt,ig,id,igfx,xp,mc,mcst,mcend,
     *                 idx)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      integer (kind=4) :: m1, mcst, mcend, idx
      real    (kind=8) :: xp 
      real    (kind=8) :: rot(ngfc+2),a1(ngfc),a2(ngfc),bt(ngf)
      integer (kind=4) :: id(ngfc+2),mc(ntl,2),igfx(ngf,3),ig(ngfc)

      integer (kind=4) :: i,ii,ic,ics,idg,imm
      integer (kind=4) :: l,l2
      real    (kind=8) :: gx
      real    (kind=8) :: r,rn

c.....check whether the roots are in the failure region or not.

      rn=real(nry)
      xp=0.d0
      do 120 ii=2,m1
      r=(rot(ii)+rot(ii+1))/2.d0
      do 1101 l=1,ngfc
      l2=igfx(l,2)
      if(idx.eq.1) then
      gx=a2(l)*r+bt(l2)
      else
      gx=a1(l)*r*r+a2(l)*r+bt(l2)
      endif
      if(gx.le.1e-10) then
      ig(l)=0
      else
      ig(l)=1
      endif
 1101 continue
      imm=1
      idg=1
      do 100 i=mcst,mcend
      if(mc(i,1).ne.0) then
      ic=abs(mc(i,2))
      if(mc(i,1).lt.0) then
      imm=imm*ig(ic)
      else
      imm=imm*(1-ig(ic))
      endif
      else
      ics=1-imm
      idg=idg*ics
      imm=1
      endif
  100 continue
      idg=1-idg
      if(idg.eq.1) then
      id(ii)=1
      else
      id(ii)=0
      endif
 120  continue
c     print *,'id=',(id(ka),ka=1,m1)
c.....compute the failure probability for this directinal v vector.
      call dpf(m1,id,rot,xp)

      end subroutine dscum
