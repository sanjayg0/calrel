c     *********************************************************************
      subroutine flinsc(x,y,a,d,yt,z,tf,ex,sg,par,bnd,tp,igt,lgp,ids,
     *                 ipa,ib,izx,gx,sdgy,ig,qf4,dqf4,q)

      implicit   none

      include   'grad.h'
      include   'optm.h'
      include   'para.h'
      include   'prob.h'

      real    (kind=8) :: qf4,dqf4
      external         :: qf4,dqf4

      logical          :: fl1,fl2
      integer (kind=4) :: ig 
      real    (kind=8) :: gx,sdgy
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry),yt(nry),z(nrx),tf(ntf)
      real    (kind=8) :: ex(nrx),sg(nrx),par(4,nrx),bnd(2,nrx),tp(*)
      integer (kind=4) :: igt(nig),lgp(3,nig),ids(nrx),ipa(4,nrx)
      integer (kind=4) :: ib(nrx),izx(nry)

      integer (kind=4) :: i,ii
      real    (kind=8) :: alpham, dq0
      real    (kind=8) :: q,q0,q1,q2,qm
      real    (kind=8) :: s,s0,s1,s2,sl
      real    (kind=8) :: v0

      real    (kind=8) :: cdot
      save

      flgf=.true.
      flgr=.false.
      go to (100,200,300),iline
c---  line search by armijo's rule.
c---  compute initial values
100   q0=qf4(y,gx,nry)
      dq0=dqf4(y,d,a,gx,sdgy,nry)
      fl1=.false.
      fl2=.false.
      alpha=1.d0
      v0=v
      ii=0
      sl=dsqrt(cdot(d,d,1,1,nry))
      if(sl.gt.op3) alpha=alpha*op3/sl
      alpham=alpha
      qm=1.d38
c---  find a new y along the search direction.
  110 do 120 i=1,nry
  120 yt(i) = y(i) + alpha*d(i)
      v=v0+alpha*uv
c---  compute new x and new unit normal vector.
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
c---  compute new value of the descent function
      q=qf4(yt,gx,nry)
      if(qm.gt.q) then
        qm=q
        alpham=alpha
      endif
c---  if alpha satisfies armijo's rule, convergence is achieved.
      if(q.le.q0+0.2*dq0*alpha) then
         fl1=.true.
         if(fl2) go to 330
         fl2=.false.
         alpha=alpha/op1
      else
         fl2=.true.
         alpha=alpha*op1
         if(fl1) go to 330
         fl1=.false.
      endif
      ii=ii+1
      if(ii.le.ni2) go to 110
c     call cerror(22,0,0.,' ')
c---  line search by quadratic fitting
c---  compute initial values
200   s0=0.
      q0=qf4(y,gx,nry)
      dq0=dqf4(y,d,a,gx,sdgy,nry)
      v0=v
c---  find a new y along the search direction.
      s1=1.d0
      sl=dsqrt(cdot(d,d,1,1,nry))
      if(sl.gt.op3) s1=s1*op3/sl
      do 210 i=1,nry
  210 yt(i) = y(i) + s1*d(i)
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      v=v0+s1*uv
      q1=qf4(yt,gx,nry)
      alpham=s1
      qm=q1
c---  find a second new y along the search direction.
      s2=s1*op1
      if(q1.lt.q0+0.2*dq0) s2=s1/op1
      do 220 i=1,nry
  220 yt(i) = y(i) + s2*d(i)
      v=v0+s2*uv
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      q2=qf4(yt,gx,nry)
      if(qm.gt.q2) then
        qm=q2
        alpham=s2
      endif
c---  find subsequent y's by quadratic fitting
      ii=3
  230 s=(s1*s1-s2*s2)*q0+(s2*s2-s0*s0)*q1+(s0*s0-s1*s1)*q2
      s=s/2./((s1-s2)*q0+(s2-s0)*q1+(s0-s1)*q2)
      do 240 i=1,nry
  240 yt(i) = y(i) + s*d(i)
      v=v0+s*uv
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      q=qf4(yt,gx,nry)
      if(qm.gt.q) then
        qm=q
        alpham=s
      endif
c---  perform goldstein test
      if(q.le.q0+0.2*dq0*s.and.q.gt.q0+0.8*dq0*s) go to 330
      s1=s2
      s2=s
      q1=q2
      q2=q
      ii=ii+1
      if(ii.le.ni2) go to 230
c     call cerror(22,0,0.,' ')
c---  schittkowski's method
300   alpha=1.d0
      q0=qf4(y,gx,nry)
      dq0=dqf4(y,d,a,gx,sdgy,nry)
      sl=dsqrt(cdot(d,d,1,1,nry))
      if(sl.gt.op3) alpha=alpha*op3/sl
      v0=v
      ii=0
      qm=1.d38
      alpham=alpha
  310 do 320 i=1,nry
  320 yt(i)=y(i)+alpha*d(i)
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      v=v0+alpha*uv
      q=qf4(yt,gx,nry)
      if(qm.gt.q) then
        qm=q
        alpham=alpha
      endif
      if(q.le.q0+0.2*alpha*dq0) go to 330
      alpha=alpha*op1
      ii=ii+1
      if(ii.le.ni2) go to 310
  330 do 340 i=1,nry
  340 y(i)=y(i)+alpham*d(i)
      q=qm
      alpha=alpham
      v=v0+alpha*uv

      end subroutine flinsc
