c     ********************************************************************
c     subroutine fcosine1
c     ********************************************************************
      subroutine fcosine1(cosa,y,nry,icont,ylin)

      implicit   none

      integer (kind=4) :: nry,icont
      real    (kind=8) :: y(nry),cosa(nry,nry-1),ylin(nry)
 
      integer (kind=4) :: i,j,k
      real    (kind=8) :: ysum 

      real    (kind=8) :: cdot

      ysum = cdot(y,ylin,1,1,nry)/cdot(ylin,ylin,1,1,nry)
      do i = 1,nry
        cosa(i,icont) = y(i)-ysum*ylin(i)
      end do ! i
      ysum = sqrt(cdot(cosa(1,icont),cosa(1,icont),1,1,nry))
      do i = 1,nry
        cosa(i,icont) = cosa(i,icont)/ysum
      end do ! i
      do k = 1,icont-1
        ysum = cdot(cosa(1,icont),cosa(1,k),1,1,nry)
        do j = 1,nry
          cosa(j,icont) = cosa(j,icont)-ysum*cosa(j,k)
        end do ! j
        ysum = cdot(cosa(1,icont),cosa(1,icont),1,1,nry)
        ysum = sqrt(ysum)
        do j = 1,nry
          cosa(j,icont) = cosa(j,icont)/ysum
        end do ! j
      end do ! k

      end subroutine fcosine1
