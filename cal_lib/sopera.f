cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sopera(c,rt,a,y01,z,x,tf,ids,par,tp,bt,bnd,sg,ex,ib,
c    *                  igt,lgp,ipa,izx,cur,ig,x0,inp)
c
c   functions:
c     compute diagonal curvatures for the point fitting method.
c
c   input arguments:
c     a   : unit normal vector in the y space.
c     x0  : design point in the x space.
c     tf  : the transformation matrix from z to y, y = tf * z.
c     ids: distribution number of x.
c     par : distribution parameters of x.
c     tp  : deterministic parameters.
c     bt  : reliability index.
c     bnd : lower and upper bounds of x.
c     sg  : standard deviation of x.
c     ex  : means of x.
c     igt : group types.
c     lgp : location of the first element of a group in y.
c     izx : a vector associated with z indicating the no. of the corresponding x
c     ipa : #'s of the associated rv of distribution parameters.
c           n-th element  =  0 means parameter n is a constant.
c     ib  : code of distribution bounds of x.
c
c   output arguments:
c     c   : diagonal curvatures in the y space.
c     cur : curvatures of semiporobolic curves in each coordinate plane.
c     rt  : rotational transformation matrix, y' = rt * y.
c
c   calls: sgs, secant.
c
c   called by: sopf.
c
c   last revision: aug. 20,1987 by h.-z. lin.
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sopera(c,rt,a,y01,z,x,tf,ids,par,tp,bt,bnd,sg,ex,ib,
     *                  igt,lgp,ipa,izx,cur,ig,x0,tor,inp)

      implicit   none

      include   'file.h'
      include   'prob.h'

      integer (kind=4) :: ig, inp
      real    (kind=8) :: bt, gou, tor
      integer (kind=4) :: lgp(3,nig),ipa(4,nrx),izx(nry),igt(nig)
      integer (kind=4) :: ids(nrx),ib(nrx)
      real    (kind=8) :: c(nry),rt(nry,nry),a(nry)
      real    (kind=8) :: y01(nry),z(nry),x(nrx),x0(nrx)
      real    (kind=8) :: tf(ntf),par(4,nrx),tp(*),bnd(2,nrx)
      real    (kind=8) :: sg(nrx),ex(nrx),cur(2,nry-1)

      integer (kind=4) :: ii
      integer (kind=4) :: j
      integer (kind=4) :: k,kj,kk
      integer (kind=4) :: nn
      real    (kind=8) :: bb,beta
      real    (kind=8) :: d,dab
      real    (kind=8) :: gam, gx1
      real    (kind=8) :: w, ww
      real    (kind=8) :: xk
      real    (kind=8) :: y1,yi1, yn,yn1
      save

      nn = nry-1
c     nsec = 4
c.....check the direction cosine at the design point.
      do k = 1,nry
        z(k) = a(k)
      end do ! k
      kj = 0
      do k = nry,1,-1
        if(dabs(z(k)).le.0.1e-7) then
          kj = kj+1
        else
          go to 149
        endif
      end do ! k
 149  continue
      call sgs(rt,z,nry,y01,kj)
      write(not,2010)
      dab = dabs(bt)
      if(dab.gt.3.0) then
        bb = 3.0d0
        xk = 3.0d0/bt
      else if(dab.gt.1.) then
        bb = bt
        xk = 1.0
      else
        bb = 1.d0
        if(bt.lt.0.) bb = -1.d0
      endif
      do j = 1,nry-1
        beta = bb
        kk = 0
   50   y1 = beta
        do ii = 1,nrx
          x(ii) = x0(ii)
        end do ! ii
        call secant(bt,rt,y1,yn,y01,j,z,x,tf,ids,par,tp,bnd,sg,ex,ib,
     *              igt,lgp,ipa,izx,tor,xk,gam,ig,inp,gou)
        ww = dsqrt(gam)
        if(kk-1) 1119,80,80
 1119   d = ww
        yi1 = y1
        yn1 = yn
        gx1 = gou
          cur(1,j) = (yn-bt)/y1/y1
c         cur(1,j) = (gam-1.d0)/2.d0/bt
        beta = -bb
        kk = 1
        go to 50
   80   w = 2.d0*ww*d/(ww+d)
          cur(2,j) = (yn-bt)/y1/y1
c         cur(2,j) = (gam-1.d0)/2.d0/bt
        if(dab.le.0.1e-9) then
          c(j) = (cur(1,j)+cur(2,j))/2.d0
        else
          c(j) = (w*w-1.d0)/(2.d0*bt)
        endif
        kk = 0
        write(not,2050) j,yi1,yn1,gx1,2.*cur(1,j),y1,yn,gou,2.*cur(2,j)
      end do ! j

c---  output formats

 2010 format(' Coordinates and  ave. main curvatures of fitting ',
     &'points in rotated space'/
     &' axis',2x,3hu'i,4x,3hu'n,4x,'G(u)',7x,4ha'+i,9x,3hu'i,4x,3hu'n,
     &  4x,'G(u)',7x,4ha'-i)
 2050 format(i5,f6.3,1x,f6.3,1p,1e11.3,1p,1e11.3,3x,0p,f6.3,1x,f6.3,
     &  1p,1e11.3,1p,1e11.3)

      end
