cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fnewy3(x,y,a,d,yt,z,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
c                        ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig)
c
c   functions:
c      compute new x and y using gradient projection method.
c
c   input arguments:
c      x : current x.
c      y : current y.
c      a : unit normal vector in the y space.
c      d : direction of search.
c      yt : temporary y.
c      gam : dg/dz.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : means of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      sdgy : |dg/dy|.
c      iter : iteration number.
c      gx : g(x).
c      ig : failure mode number.
c
c   output arguments:
c      x : new x.
c      y : new y.
c      z : new z.
c      bt : new reliability index.
c
c   calls: ugfun, cdot, cytox.
c
c   called by: form.
c
c   last revision: july 27 1987 by p.-l. liu.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnewy3(x,y,a,d,yt,z,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *                ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,gdgx)

      implicit   none

      include   'flag.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'

      integer (kind=4) :: iter,ig
      real    (kind=8) :: sdgy,bt,gx
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig),izx(nry)
      integer (kind=4) :: ixz(nrx),ipa(4,nrx),gdgx(nrx)
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry),yt(nry),z(nry),gam(nry)
      real    (kind=8) :: tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),tp(*)

      integer (kind=4) :: i,ii,iii,k
      real    (kind=8) :: alfa, bt1, gx0
      real    (kind=8) :: s0,s1,ss
      real    (kind=8) :: ya

      real    (kind=8) :: cdot 
      save

      flgf=.true.
      flgr=.false.
      if(igr.ne.0) flgr=.true.
c
c---  compute new y using gradient projection method.
c
c----------------------------------------------------------------------
c     perform line search.
c----------------------------------------------------------------------
c---  move along gradient projection direction
c
      if(iter.eq.2) bt=100.d0
      alfa=1.d0
      ya=cdot(y,a,1,1,nry)
      do 30 i=1,nry
 30   d(i)=y(i)-ya*a(i)
      iii=0
 40   do 50 i=1,nry
 50   yt(i)=y(i)-alfa*d(i)
c
c--- if y is not on the failure surface, do newton-type correction.
c
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx0,x,tp,ig)
      call fdirct(x,yt,tp,ex,sg,z,gam,a,tf,par,bnd,igt,lgp,
     *                   ids,ipa,izx,ixz,sdgy,gdgx,ig,0)
      if(dabs(gx0).le.op2) go to 100
      ii=1
      s0=0.
 60   s1=gx0/sdgy+s0
      if(dabs(s1).gt.op3) then
         k=int(s1/op3) ! WIP: truncate not round with nint
         s1=s1-op3*k
      end if
      ss=s1-s0
      do 70 i=1,nry
 70   yt(i)=yt(i)+ss*a(i)
      call cytox(x,yt,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      call fdirct(x,yt,tp,ex,sg,z,gam,a,tf,par,bnd,igt,lgp,ids,
     *            ipa,izx,ixz,sdgy,gdgx,ig,0)
      if(dabs(gx).le.op2) go to 100
      s0=s1
      gx0=gx
      ii=ii+1
      if(ii.le.ni2) go to 60
c
c---  check if the new beta is less than the old one.
c---  if new beta does not decrease, repeat line search.
c
 100  alfa=alfa*op1
      bt1=dsqrt(cdot(yt,yt,1,1,nry))
      iii=iii+1
      if(dabs(bt1).gt.dabs(bt).and.iii.lt.ni2) go to 40
      do 110 i=1,nry
 110  y(i)=yt(i)
c
c---  update beta.
c
      ss=cdot(y,a,1,1,nry)
      bt=bt1
      if(ss.lt.0.d0) bt=-bt

      end subroutine fnewy3
