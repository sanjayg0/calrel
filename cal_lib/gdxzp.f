!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine gdxzp(x,dxzp,par,ids,izx,ex,sg,nge)
!
!   functions:
!      compute d(dx/dz)/d(par), which are used to transform dg/dx to dg/dy.
!
!   input arguments:
!      x : basic random variables in the original space.
!      par : distribution parameters of x.
!      ids : distribution numbers of x.
!      izx : indicators for z to x.
!      nge : number of z in the group.
!
!   output arguments:
!      dxzp : d(dx/dz)/d(par).
!
!   calls: udd, mdgam, dbeta, dnormi.
!
!   called by: gdapar.
!
!   last revision: dec. 10, 1987 by h.-z. lin
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gdxzp(x,dxzp,par,ids,izx,ex,sg,nge)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: nge
      real    (kind=8) :: x(nrx),dxzp(6,nge)
      integer (kind=4) :: ids(nrx),izx(nge)
      real    (kind=8) :: par(4,nrx),ex(nrx),sg(nrx)

      integer (kind=4) :: i,id, k,kf, ier
      real    (kind=8) :: a1,a2,b1,b2, ba
      real    (kind=8) :: d,ds1,ds2, du1,du2, dx, det
      real    (kind=8) :: e,e2, faiz
      real    (kind=8) :: g1,g2, gm1,gm2, gp1,gp2
      real    (kind=8) :: p1,p2, pp1,pp2, pp,pr1, prob
      real    (kind=8) :: rm,rp, r, xx,xt,xt1, z,z1

      save

      do 100 k = 1,nge
      i = izx(k)
      id = ids(i)
      pp1 = par(1,i)
      pp2 = par(2,i)
      xx = x(i)
      do 108 kf = 1,4
 108  dxzp(kf,k) = 0.d0

!---  switch on type of distribution

      if(id.ge.50) go to 51
      go to (1,2,3,4,5,6,51,100,100,100,11,12,13,14),id
!--   normal
    1 dxzp(2,k) = 1.d0
      dxzp(4,k) = 1.d0
      go to 100
!--   lognormal
    2 dxzp(3,k) = pp2*xx
      dxzp(4,k) = xx*(1.d0+pp2*pp2)
      e = sg(i)/ex(i)
      e2 = e*e
      d = 1.d0+e2
      a1 = 1.d0/ex(i)*(1.d0+e2/d)
      a2 = -e2/d/pp2/ex(i)
      dxzp(1,k) = dxzp(3,k)*a1+dxzp(4,k)*a2
      b1 = -e/d/ex(i)
      b2 = -b1/pp2
      dxzp(2,k) = dxzp(3,k)*b1+dxzp(4,k)*b2
      go to 100
!--   gamma
   3  call gdiff(x,par,ids,dxzp(3,k),izx(k),i,2)
      x(i) = xx
      d = sg(i)*sg(i)
      a2 = 2.d0*ex(i)/d
      dxzp(1,k) = dxzp(3,k)/d+dxzp(4,k)*a2
      dxzp(2,k) = (-dxzp(3,k)-dxzp(4,k)*ex(i))*a2/sg(i)
      go to 100
!--   exponential
    4 xt = exp(-pp1*(xx-pp2))
      prob = 1.0-xt
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      call dnorm(-z,pr1)
      dxzp(3,k) = -faiz/pr1/pp1/pp1
      dxzp(2,k) = -dxzp(3,k)/sg(i)/sg(i)
      go to 100
!--   rayleigh
    5 dx = (xx-pp2)/pp1
      xt = exp(-dx*dx/2.)
      prob = 1.0-xt
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      call dnorm(-z,pr1)
      a1 = -2.d0*log(pr1)
      dxzp(3,k) = faiz/pr1/sqrt(a1)
      dxzp(2,k) = dxzp(3,k)*1.526399746d0
      go to 100
!--   uniform
    6 ba = pp2-pp1
      prob = (xx-pp1)/ba
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      dxzp(3,k) = -faiz
      dxzp(4,k) = faiz
      dxzp(2,k) = faiz*3.464101615d0
      go to 100
!--   beta
!   7 go to 51
!--   type i largest
   11 xt = -pp2*(xx-pp1)
      xt = -exp(xt)
      prob = exp(xt)
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      call dnorm(z,pr1)
      dxzp(4,k) = faiz/pp2/pp2/pr1/(log(pr1))
      dxzp(2,k) = -dxzp(4,k)*1.28254983d0/sg(i)/sg(i)
      go to 100
!--   type i smallest
   12 xt = pp2*(xx-pp1)
      xt = -exp(xt)
      pp = exp(xt)
      prob = 1.d0-pp
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      call dnorm(-z,pr1)
      dxzp(4,k) = faiz/pp2/pp2/pr1/log(pr1)
      dxzp(2,k) = -dxzp(4,k)*1.28254983d0/sg(i)/sg(i)
      go to 100
!--   type ii largest
   13 xt = pp1/xx
      xt1 = xt**pp2
      prob = exp(-xt1)
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      pr1 = 1.d0-prob
      p1 = (pp2+1.d0)/pp2
      p2 = -log(pr1)
      dxzp(3,k) = faiz/pp2/(p2**p1)
      dxzp(4,k) = pp1*dxzp(3,k)*(-1.d0/p1+log(p2)/pp2/pp2)
      r = 1.d0/pp2
      g1 = gamma(1.d0-r)
      g2 = gamma(1.d0-r*2)
      du1 = g1
      ds1 = sqrt(g2-g1*g1)
      rp = 1.d0/(pp2+0.001d0)
      rm = 1.d0/(pp2-0.001d0)
      gp1 = gamma(1.d0-rp)
      gm1 = gamma(1.d0-rm)
      gp2 = gamma(1.d0-rp*2.)
      gm2 = gamma(1.d0-rm*2.)
      du2 = (gp1-gm1)*pp1/0.002d0
      ds2 = (sqrt(gp2-gp1*gp1)-sqrt(gm2-gm1*gm1))*pp1/0.002d0
      det = du1*ds2-du2*ds1
      dxzp(1,k) = (ds2*dxzp(3,k)-ds1*dxzp(4,k))/det
      dxzp(2,k) = (-du2*dxzp(3,k)+du1*dxzp(4,k))/det
      go to 100
!-- type iii smallest
  14  xt = xx/pp1
      xt1 = xt**pp2
      prob = 1.0-exp(-xt1)
      call dnormi(z,prob,ier)
      z1 = -0.5d0*z*z
      faiz = exp(z1)/2.506628275d0
      pr1 = 1.d0-prob
      p1 = (pp2-1.d0)/pp2
      p2 = -log(pr1)
      dxzp(3,k) = faiz/pr1/pp2/p2**p1
      dxzp(4,k) = dxzp(3,k)*pp1*(-1.d0/pp2-log(p2)/pp2/pp2)
      r = 1.d0/pp2
      g1 = gamma(1.d0+r)
      g2 = gamma(1.d0+r*2)
      du1 = g1
      ds1 = sqrt(g2-g1*g1)
      rp = 1.d0/(pp2+0.001d0)
      rm = 1.d0/(pp2-0.001d0)
      gp1 = gamma(1.d0+rp)
      gm1 = gamma(1.d0+rm)
      gp2 = gamma(1.d0+rp*2.)
      gm2 = gamma(1.d0+rm*2.)
      du2 = (gp1-gm1)*pp1/0.002d0
      ds2 = (sqrt(gp2-gp1*gp1)-sqrt(gm2-gm1*gm1))*pp1/0.002d00
      det = du1*ds2-du2*ds1
      dxzp(1,k) = (ds2*dxzp(3,k)-ds1*dxzp(4,k))/det
      dxzp(2,k) = (-du2*dxzp(3,k)+du1*dxzp(4,k))/det
      go to 100
!--   beta or user-defined distribution
  51  call gdiff(x,par,ids,dxzp(1,k),izx(k),i,4)
      x(i) = xx
  100 continue

      end subroutine gdxzp
