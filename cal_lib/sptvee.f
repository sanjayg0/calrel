cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sptvee(n,c,bt,pb)

c   functions:
c     use Tvedt's exact integral formula to calculate failure probability.
c     this program is used in the case of principal curvatures.

c   input arguments:
c     n   : nrx-1
c     c   : principal curvatures.
c     bt  : reliability index.

c   output argument:
c     pb  : failure probability.

c   calls: snewg,snewg0,snewg1

c   called by: socf, sopf.

c   last revision: nov. 3, 1989, by h.-z. lin.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sptvee(n,c,bt,pb)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: bt,pb
      real    (kind=8) :: c(n)

      integer (kind=4) :: ii, indx
      integer (kind=4) :: kj,km,km1, ll, nn
      real    (kind=8) :: b, cri, c1
      real    (kind=8) :: err
      real    (kind=8) :: rr,rg1,rg2,rg3, rg,rgg,rgl,rgu, s,s1
      real    (kind=8) :: ul,ul1, u1,u2,u3, us,uu, xh

      real    (kind=8) ::  sfee

c     arrange the curvatures in ordering and count the number of negative
c     curvatures..
      km1=5
      xh=0.5
      indx=1
      nn=n-1
      ll=0
      km=km1
  100 do 110 ii=1,nn
      if(c(ii).gt.c(ii+1)) then
      c1=c(ii)
      c(ii)=c(ii+1)
      c(ii+1)=c1
      endif
  110 continue
      if(nn.gt.1) then
      nn=nn-1
      go to 100
      endif
      do 200 ii=1,n
      if(c(ii).lt.0.) then
      ll=ii
      else
      go to 210
      endif
  200 continue
  210 continue
      if(ll.eq.0.and.c(n).eq.0.) then
c     first order result
      call dnorm(-bt,pb)
      return
      endif
c     set the range of the saddle point.
  190 uu=-0.1e-5
      if(indx.eq.1) go to 720
      if(ll.eq.n) then
      ul=-1000.d0
      else
        if(dabs(c(ll+1)).le.0.1e-7) then
        ul=-1000.d0
        else
        ul=0.5d0/c(ll+1)+0.1e-5
        endif
        if(dabs(c(n)).le.0.1e-7) then
        ul1=-1000.d0
        else
        ul1=0.5d0/c(n)+0.1e-5
        endif
      if(ul.lt.ul1) ul=ul1
      endif
      go to 740
 720  if(c(1)*c(n).le.0..or.ll.eq.n) then
c---  there are (or all are) negative main curvatures
        if(dabs(c(1)).le.0.1e-8) then
        ul=-1000.d0
        else
        ul=0.5d0/c(1)+0.1e-5
        endif
        if(dabs(c(ll)).le.0.1e-8) then
        ul1=-1000.d0
        else
        ul1=0.5d0/c(ll)+0.1e-5
        endif
      if(ul.lt.ul1) ul=ul1
      else if(ll.eq.0) then
      ul=-1000.d0
      endif

c     use secant method to determine the saddle point.
 740  u1=uu
      u2=ul
      call snewg(u1,rg1,bt,c,n)
      rgu=rg1
      call snewg(u2,rg2,bt,c,n)
      rgl=rg2
  301 u3=u1-rg1*(u2-u1)/(rg2-rg1)
      if(u3.gt.uu.or.u3.lt.ul) u3=(uu+ul)/2.
c     print *,'u3=',u3
      call snewg(u3,rg3,bt,c,n)
c     print *,'rg3=',rg3
      if(rg3*rgu.gt.0.) then
      uu=u3
      rgu=rg3
      else
      ul=u3
      rgl=rg3
      endif
      if(dabs(rg3).le.0.1e-7) then
      us=u3
      call snewg1(us,rg,c,n)
c     print *,'rg=',rg
      b=2.0d0/dabs(rg)
      b=dsqrt(b)
c     print *,'b=',b,'us=',us
      if(indx.eq.1.and.b.lt.0.5) then
      bt=-bt
      do kj=1,n
        c(kj)=-c(kj)
      end do ! kj
      indx=-1
      go to 190
      endif
      b=xh*b
      else
      u1=u2
      u2=u3
      rg1=rg2
      rg2=rg3
      go to 301
      endif
      s1=real(km)
  49  s=b*s1
      if(s.le.1.) then
      rr=2.d0
      else
      rr=1.d0/s
      endif
      call snewg0(s,us,bt,c,n,rgg)
      err=rgg
      call dnorm(-bt,cri)
      cri=cri*0.00001
      if(err.ge.cri) then
      s1=s1+4.d0
      go to 49
      endif
      km=int(s1)

c     use trapezoid rule to estimate the integration ( 12 intervals).
c     set the step length equal 1.
c     print *,'np=',km
      pb=sfee(b,us,bt,c,n,km)
      if(indx.eq.-1) then
      pb=1.d0-pb
      bt=-bt
      do kj=1,n
        c(kj)=-c(kj)
      end do ! kj
      endif

      end subroutine sptvee
