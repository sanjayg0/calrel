      subroutine crest

      implicit   none

      include   'blkrel1.h'
      include   'blkchr.h'
      include   'bond.h'
      include   'cuts.h'
      include   'dbsy.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'fsav.h'
      include   'optm.h'
      include   'prob.h'
      include   'simu.h'

      logical          :: flg
      integer (kind=4) :: i, ic
      integer (kind=4) :: lrec, nps
      real    (kind=8) :: r

      save

      inquire(file='rel.nco',exist=flg)
      if(flg) then
        open(nco,file='rel.nco',form='unformatted',status='unknown')
        rewind(nco)
        read(nco,end=10,err=10) icl,igr,igf,ngf,nig,nrx,ntp,nry,ntf,
     *       ncs,ntl,imc,ngfc,ntge,numa,next,idir,ip,iop,ni1,ni2,tol,
     *       op1,op2,op3,nsm,nps,cov,flc,itt,gx,ig0,iter,des0
        read(nco,end=10,err=10) nd
        close(nco)
      else
        write(*,*) 'FILE: rel.nco does not exist'
        stop
      end if
      inquire(file='rel.nia',exist=flg)
      if(flg) then
        open(nia,file='rel.nia',form='unformatted',status='unknown')
        rewind(nia)
        read(nia,end=10,err=10) mtot,np,(ia(i),i=1,next),(ia(i),
     *                          i=idir,mtot)
        close(nia)
      else
        write(*,*) 'FILE: rel.nia does not exist'
        stop
      end if

!     Portability: unit is mechine dependent.

      inquire(file='rel.ns0',exist=flg)
      if(flg) then
        inquire(iolength=lrec) (r,ic=1,max(nrx,nry) + 6)
        open(ns0,file='rel.ns0',form='unformatted',status='unknown',
     &       access='direct',recl=lrec)
      else
        write(*,*) 'FILE: rel.ns0 does not exist'
        stop
      end if
!
      inquire(file='feap.res',exist=flg)
      if(flg) call prest

      return

!     Error

   10 call cerror(41,0,0.,' ')

      end subroutine crest
