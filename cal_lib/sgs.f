ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sgs(rt,a,n,t,kj)

c   functions:
c     use modified gram-schmidth orthogonal process to generated an orthogonal
c     transformation matrix rt, y =  rt * y'. the last column of rt is a, unit
c     normal vector of design point in the y space.

c   input arguments:
c     a   : unit direction cosine vector of design point in the y space.
c     n   :  = nrx, number of variables.
c     kj  : index to show the kj-th element of vector a is not equal to zero
c           and kj+1-th to nrx-th elements are equal to zero.

c   output argument:
c     rt  : rotational transformation matrix , y  =  rt * y'

c   called by: socf, sopera.

c     last revision: july 29, 1987 by h.-z. lin.

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sgs(rt,a,n,t,kj)

      implicit   none

      integer (kind=4) :: n, kj
      real    (kind=8) :: rt(n,n),a(n),t(n)

      integer (kind=4) :: i,j,k

      real    (kind=8) :: cdot 
c-----initialize process
c     read *, thet

      do 1 j = 1,n
      do 1 i = 1,n
      rt(j,i) = 0.d0
    1 rt(j,j) = 1.d0
      do 2 k = 1,n
    2 rt(k,n-kj) = a(k)

c----- modified gram-schmidth orthogonal process

      do 10,i = n,1,-1
        t(i) = dsqrt(cdot(rt(1,i),rt(1,i),1,1,n))
        do 20,j = 1,n
          rt(j,i) = rt(j,i)/t(i)
20      continue
        do 30,j = i-1,1,-1
          t(j) = cdot(rt(1,i),rt(1,j),1,1,n)
          do 40,k = 1,n
            rt(k,j) = rt(k,j)-rt(k,i)*t(j)
40        continue
30      continue
10    continue
      if(kj.ne.0) then
        do 390 i = 1,n
          t(i) = rt(i,n-kj)
          rt(i,n-kj) = rt(i,n)
          rt(i,n) = t(i)
  390   continue
      endif

      end subroutine sgs
