ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine snewgs(ur,rg,bt,c,n)
c
c     function: determinate the saddle point for the case of semi
c               principal curvature.
c
c     last version: aug. 1990 by h.z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine snewgs(ur,rg,bt,c,n)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: ur,rg,bt
      real    (kind=8) :: c(2,n)

      integer (kind=4) :: i
      real    (kind=8) :: u2, ak1,ak2,akb,aku 

      rg=0.d0
      u2=2.d0*ur
      do i=1,n
        ak1=1.d0-u2*c(1,i)
        ak2=1.d0-u2*c(2,i)
        akb=1.d0/dsqrt(ak1)+1.d0/dsqrt(ak2)
        aku=c(1,i)/(ak1**1.5d0)+c(2,i)/(ak2**1.5d0)
        rg=rg+aku/akb
      end do ! i
      rg=rg+ur+bt-1.d0/ur

      end subroutine snewgs
