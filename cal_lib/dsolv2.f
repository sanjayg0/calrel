ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dsolv2(rt,bt,y,yy,cur,rr,ind,a1,a2,igfx,root)
c
c     functions:
c       solve the roots of the intersection of second order failure
c        surface and simulated vectors.(for point fitting method)
c
c     input arguments:
c       ngf  : number of performance functions.
c       rt   : rotational transformation matrices, y=rt*y'.
c       bt   : reliability indexes.
c       y    : simulated vector.
c       cur  : curvatures for each semiparabolic intersection.
c       nry  : no. of variables in y space.
c       root : threshold of root in the root's finding procedure in each
c              direction.
c
c     output arguments:
c       rr   : roots of intersection of 2nd order surface and simulated
c              vector.
c       ind  : index of how many roots for each mode, (1-4 roots)
c       a1   : coef. of 2nd order terms in the 2nd order surface.
c       a2   : coef. of 1st order terms in the 2nd order surface.
c
c     called by: dirs2
c
c     last version: july 30, 1987 by h.-z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dsolv2(rt,bt,y,yy,cur,rr,ind,a1,a2,igfx,root)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      real    (kind=8) :: root
      integer (kind=4) :: ind(ngfc,2),igfx(ngf,3)
      real    (kind=8) :: rt(nry,nry,ngf),y(nry),yy(nry),bt(ngf)
      real    (kind=8) :: rr(4,ngfc),a1(ngfc,2),a2(ngfc,2)
      real    (kind=8) :: cur(2,nry-1,ngf)

      integer (kind=4) :: i,j,k, kk,kj,k2,ll
      real    (kind=8) :: a,at
      real    (kind=8) :: ro

      do k=1,ngfc
        k2=igfx(k,2)
c       print *,'k=',k,'k2=',k2
c       call mprint(cur(1,1,k2),2,nry-1,1)
c       print *,'beta(',k2,')=',bt(k2)
        do i=1,nry
          yy(i)=0.d0
        end do ! i
        do i=1,nry
          do j=1,nry
            yy(i)=rt(j,i,k2)*y(j)+yy(i)
          end do ! j
        end do ! i
c       print *,'yy=',(yy(ka),ka=1,3)
        ll=0
        do kj=1,2
          if(kj.eq.2) then
            do i=1,nry
              yy(i)=-yy(i)
            end do ! i
          endif
          a=0.d0
          if(bt(k2).gt.0) then
            do i=1,nry-1
              if(yy(i).gt.0.d0) then
                at=yy(i)*yy(i)*cur(1,i,k2)
                a=a+at
              else
                at=yy(i)*yy(i)*cur(2,i,k2)
                a=a+at
              endif
            end do ! i
c           print *,'at+=',at
          else
            do i=1,nry-1
              if(yy(i).gt.0.d0) then
                at=yy(i)*yy(i)*cur(2,i,k2)
                a=a+at
              else
                at=yy(i)*yy(i)*cur(1,i,k2)
                a=a+at
              endif
            end do ! i
c         print *,'at-=',at
          endif
          kk=1+ll
          if(a.eq.0.d0) then
            ind(k,kj)=1
            rr(kk,k)=bt(k2)/yy(nry)
c           print *,'rr1(',kk,k,')=',rr(kk,k)
            if(rr(kk,k).ge.root.or.rr(kk,k).le.0.) ind(k,kj)=0
            go to 990
          endif
          ro=yy(nry)*yy(nry)-4.d0*a*bt(k2)
          if(ro.lt.0.d0) then
            ind(k,kj)=0
            go to 990
          else
            if(ro.le.0.1e-12) then
              ind(k,kj)=1
              rr(kk,k)=yy(nry)/2.d0/a
c             print *,'rr2(',kk,k,')=',rr(kk,k)
              if(rr(kk,k).ge.root.or.rr(kk,k).le.0.) ind(k,kj)=0
              go to 990
            else
              ind(k,kj)=2
            endif
          endif
c.....    check ind(k,kj)=2
          ro=dsqrt(ro)
          rr(kk,k)=(yy(nry)+ro)/2.d0/a
          rr(kk+1,k)=(yy(nry)-ro)/2.d0/a
c         print *,'rr(',kk,k,')=',rr(kk,k),rr(kk+1,k)
          if(rr(kk,k).ge.root.or.rr(kk,k).le.0.) then
            if(rr(kk+1,k).ge.root.or.rr(kk+1,k).le.0.) then
              ind(k,kj)=0
              go to 990
            else
              ind(k,kj)=1
              rr(kk,k)=rr(kk+1,k)
              go to 990
            endif
          else
            if(rr(kk+1,k).ge.root.or.rr(kk+1,k).le.0.) then
              ind(k,kj)=1
              go to 990
            else
              ind(k,kj)=2
            endif
          endif
 990      continue
          a1(k,kj)=a
          a2(k,kj)=-yy(nry)
          ll=2
c         print *,'a1,a2(',k,kj,')=',a1(k,kj),a2(k,kj)
        end do ! kj
      end do ! k

      end subroutine dsolv2
