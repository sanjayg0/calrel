cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine cztoxu(pr,x,ex,sg,par,bnd,id,ipa,ib,k,igt)
c
c     function:
c             use secant method to find the root in
c             f(x) = phi(z). when phi(z) is greater than 0.8 or less
c             than 0.2, the root finding procedure is performed in
c             logaritm space.
c
c     input arguments:
c      pr : the value of phi(z) for k-th random variable.
c      sg : standard deviation of k-th random variable.
c      x : basic random variables.
c      z : standard normal variate.
c      par : distribution parameter of x.
c      bnd : lower and upper bound of x.
c      ib : code of distribution bound of x.
c            =  0 , no bounds
c            =  1 , bounded from below.
c            =  2 , bounded from above.
c            =  3 , both sides bounded.
c
c     output arguments:
c       x : new x.
c
c     calls: cboun1, cboun2, cbound. cparvl, udd
c
c     called by: cztox.
c
c     last version: june 18, 1989 by hong zong lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cztoxu(pr,x,ex,sg,par,bnd,id,ipa,ib,k,igt)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: id,ib,k,igt
      real    (kind=8) :: pr, ex,sg
      integer (kind=4) :: ipa(4)
      real    (kind=8) :: x(nrx),bnd(2),par(4)

      integer (kind=4) :: ii,ibis,indx
      real    (kind=8) :: ff,fn,fp, f1,f2,f3,f4
      real    (kind=8) :: pd,pr1, r
      real    (kind=8) :: x1,x2,x3, xn,xp 
      save

c     print *,'id = ',id,'pr=',pr,'ib=',ib
c     print *,'x(',k,') = ',x(k)
      r = 1.e-10
      pr1 = pr
      ibis = 0
      indx = 0
      if(pr.le.0.2) then
c       if(ib.eq.0.or.ib.eq.2) then
        pr1 = dlog(pr)
        indx = 1
c       endif
      else if(pr.ge.0.8) then
        pr1 = dlog(1.d0-pr)
        indx = 2
      endif
c---- to find the bounds of x.
      x2 = x(k)
      f1 = 0.d0
      ii = 0
c     print *,'bnd = ',bnd(1),bnd(2)
c     print *,'indx = ',indx,'pr1=',pr1
      call cbound(x2,bnd,ib)
   10 x(k) = x2
      if(igt.ge.3) call cparvl(x,ex,sg,par,bnd,id,ipa,1)
      call udd(x,par,sg,id,f2,pd,bnd,ib)
c     print *,'bnd = ',bnd(1),bnd(2)
c     print *,'prob = ',f2,'sg=',sg
        if(indx.eq.2) then
        f3 = 1.d0-f2
        else
        f3 = f2
        endif
      f4 = f2
      ii = ii+1
      if(ii.gt.100) then
         f2 = f4-pr
         call cerror(8,id,f2,' ')
         return
      end if
      if(ii.eq.1.and.f3.le.1.e-13) then
c       print *,'x2 = ',x2,'sg=',sg
        call cboun2(x2,bnd,0,sg,pr,f2)
c       print *,'f3 = ',f3,'x2=',x2
        go to 10
      endif
      if(f3.le.1.e-15) then
         call cboun2(x2,bnd,ib,sg,pr,f2)
c        print *,'f3 = ',f3,'x2=',x2
         go to 10
      endif
      if(indx.ne.0) f3 = dlog(f3)
      f2 = f3-pr1
c     print *,'f2 = ',f2
      if(dabs(f2).le.r) return
      if(f1*f2.lt.0.d0) go to 20
      f1 = f2
      x1 = x2
      if(indx.ne.2) go to 40
      if(f2.gt.0.) then
        x2 = x1+sg
      else
        x2 = x1-sg
      endif
      go to 50
  40   if(f2.gt.0.) then
       x2 = x1-sg
       else
       x2 = x1+sg
       endif
  50  call cboun1(x2,bnd,ib,sg,pr,f4)
      go to 10
c---  use secant method to find the root of fx(x) = pr
   20 continue
      if(f1.lt.0.d0) then
        fn = f1
        xn = x1
        fp = f2
        xp = x2
      else
        fn = f2
        xn = x2
        fp = f1
        xp = x1
      endif
c     print *,'x1 = ',x1,'x2=',x2,'f1=',f1,'f2=',f2
c     print *,'indx = ',indx,'pr1=',pr1
   21 ff = f2-f1
      if(dabs(ff).gt.1.e-12) then
        x3 = x2-f2*(x2-x1)/ff
      else
        ibis = 1
        go to 990
      endif
c     print *,'x = ',x1,x2,x3
c     print *,'f2-f1 = ',f2-f1,'x2-x1=',x2-x1
      x1 = x2
      f1 = f2
      x2 = x3
      x(k) = x2
      if(igt.ge.3) call cparvl(x,ex,sg,par,bnd,id,ipa,1)
      call udd(x,par,sg,id,f2,pd,bnd,ib)
      if(f2.le.0.0d0.or.f2.ge.1.0d0) go to 990
      if(indx.eq.1) then
      f2 = dlog(f2)
      else if(indx.eq.2) then
      f2 = dlog(1.d0-f2)
      endif
      f2 = f2-pr1
      if(f2.lt.0.d0) then
         if(f2.gt.fn) then
         fn = f2
         xn = x(k)
         else
         go to 990
         endif
      else
         if(f2.lt.fp) then
         fp = f2
         xp = x(k)
         else
         go to 990
         endif
      endif
c     print *,'x(',k,') = ',x(k),'f2=',f2
      if(dabs(f2).le.r) then
c     print *,'x(',k,') = ',x(k),'f2=',f2
c     print *,'ii = ',ii
      return
      endif
      ii = ii+1
      if(ii.gt.100) then
         call cerror(8,id,f2,' ')
         return
      end if
      go to 21
c     use bisection method
 990  x3 = (xn+xp)/2.d0
      x(k) = x3
      if(igt.ge.3) call cparvl(x,ex,sg,par,bnd,id,ipa,1)
      call udd(x,par,sg,id,f2,pd,bnd,ib)
      if(indx.eq.1) then
      f2 = dlog(f2)
      else if(indx.eq.2) then
      f2 = dlog(1.d0-f2)
      endif
      f2 = f2-pr1
      if(f2.lt.0.d0) then
         if(f2.gt.fn) then
         fn = f2
         xn = x(k)
         else
      print *,'warning: cdf of id=',id,'is not monotonically increased.'
         return
         endif
      else
         if(f2.lt.fp) then
         fp = f2
         xp = x(k)
         else
      print *,'warning: cdf of id=',id,'is not monotonically increased.'
         return
         endif
      endif
c     print *,'x(',k,') = ',x(k),'f2=',f2
      if(dabs(f2).le.r) then
c     print *,'x(',k,') = ',x(k),'f2=',f2
c     print *,'ii = ',ii
      return
      endif
      ii = ii+1
      if(ii.gt.100) then
         call cerror(8,id,f2,' ')
         return
      end if
      if(ibis.eq.0) then
      f1 = fn
      x1 = xn
      f2 = fp
      x2 = xp
c     print *,'x1 = ',x1,'f1=',f1
c     print *,'x2 = ',x2,'f2=',f2
      go to 21
      endif
      go to 990

      end subroutine cztoxu
