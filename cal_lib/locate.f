cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine locate(name,na,nr,nc)
c
c   functions:
c      locate and return properties of array.
c
c   input arguments:
c      name : name of the array.
c      nr : number of rows.
c      nc : number of columns.
c
c   output arguments:
c      na : initial location of array.
c
c   calls: icon, cstop, ifind.
c
c   called by: cfosm, csosm, cboun, csens, cdir1, cdir2, cmont.
c
c   last revision: by wilson
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine locate(name,na,nr,nc)

      include   'blkrel1.h'
      include   'dbsy.h'
      include   'file.h'

      character*1 name

      dimension name(4),iname(4)

      save

c-----locate array ----------------------------------
      na = 0
      call icon(name,iname)
      i = ifind(iname,0)
      if(i.ne.0) go to 800
      call cerror(9,i,r,name)
      call cstop
c-----return array properties -----------------------
  800 na = ia(i+7)
      nr = ia(i+4)
      nc = ia(i+5)
      np = ia(i+6)

      end
