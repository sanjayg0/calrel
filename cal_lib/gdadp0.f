      subroutine gdadp0(dadp,t,dgx,x,y,tp,ex,sg,tf,par,bnd,igt,lgp,
     *                  ids,ipa,izx,ixz,dxz,ig,k)

      implicit   none

      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: ig, k 
      integer (kind=4) :: ipa(4,nrx),lgp(3,nig)
      integer (kind=4) :: igt(nig),izx(nry),ixz(nrx),ids(nrx)
      real    (kind=8) :: x(nrx),y(nry),tp(*),ex(nrx),sg(nrx),t(6,nrx)
      real    (kind=8) :: tf(ntf),par(4,nrx),dadp(nry,4)
      real    (kind=8) :: bnd(2,nrx),dgx(nrx),dxz(nrx)

      integer (kind=4) :: i,j, j1
      real    (kind=8) :: xx,dxx 
      save

      xx=x(k)
      if(dabs(xx).le.0.1e-6) then
      dxx=0.001d0
      else
      dxx=xx*0.001d0
      endif
      x(k)=xx+dxx
      if(igr.eq.0) then
        call cdgx(x,sg,dgx,tp,ig,ids,0)
      else
        call udgx(x,dgx,tp,ig)
      endif
      do 12 j=1,nrx
  12  t(3,j)=(dgx(j)-t(6,j))/dxx
      do 10 j=1,nry
      j1=izx(j)
   10 dgx(j)=t(3,j1)
      call fdirct(x,y,tp,ex,sg,dxz,dgx,dadp(1,1),tf,par,bnd,
     *           igt,lgp,ids,ipa,izx,ixz,1.,t(1,2),1,1)
      do 208 i=1,nry
 208  dadp(i,3)=dadp(i,1)
      x(k)=xx

      end subroutine gdadp0
