      subroutine csopf

      implicit   none

      include   'blkrel1.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: itg,inp
      integer (kind=4) :: nr , nc
      integer (kind=4) :: n1 , n2, n3, n4, n5, n6, n7, n8, n9
      integer (kind=4) :: n10,n11,n12,n13,n14,n15,n16,n17,n18,n19
      integer (kind=4) :: n20,n21,n22,n23,n24,n25,n26

      save

      itg=2
      inp=4
      call freei('G',itg,1)
      call freei('F',igf,1)
      call freei('P',inp,1)
      write(not,1000) itg,inp
      call locate('tf  ',n1,nr,nc)
      call locate('sg  ',n2,nr,nc)
      call locate('ex  ',n3,nr,nc)
      call locate('par ',n4,nr,nc)
      call locate('bnd ',n5,nr,nc)
      call locate('ib  ',n6,nr,nc)
      call locate('tp  ',n7,nr,nc)
      call locate('beta',n8,nr,nc)
      call locate('x   ',n9,nr,nc)
      call locate('ids ',n10,nr,nc)
      call locate('alph',n11,nr,nc)
      call lodefr('srt ',n12,nry,nry*ngf)
      call lodefr('cur ',n13,2,nry*ngf-ngf)
      call define('y01 ',n14,1,nry)
      call define('z   ',n15,1,nry)
      call define('c   ',n16,1,nry)
      call locate('igt ',n17,nr,nc)
      call locate('lgp ',n18,nr,nc)
      call locate('ipa ',n19,nr,nc)
      call locate('izx ',n20,nr,nc)
      call locate('fltf',n21,nr,nc)
      call lodefr('btg1',n22,1,ngf)
      call locate('xlin',n23,nr,nc)
      call locate('gtor',n24,nr,nc)
      call locate('fltp',n25,nr,nc)
      call locate('igfx',n26,nr,nc)

      call sopf(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     &           ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),
     &  ia(n14),ia(n15),ia(n16),ia(n17),ia(n18),ia(n19),ia(n20),
     &  ia(n21),ia(n22),ia(n23),ia(n24),ia(n25),ia(n26),itg,inp)

 1000 format(//' >>>> SECOND-ORDER RELIABILITY ANALYSIS --',
     & ' POINT FITTING <<<<'//
     & ' Type of integration scheme used ...................itg=',i5/
     & '   itg=1 ...........................improved Breitung formula'/
     & '   itg=2 ...........................improved Breitung formula'/
     & '         ............................& Tvedt''s exact integral'/
     & ' Max. number of iterations for each fitting point ..inp=',i5)

      call delete('y01 ')
      call delete('z   ')
      call delete('c   ')

      end subroutine csopf
