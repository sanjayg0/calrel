      subroutine fgam(tf,dxz,nges,nge)

      implicit   none

      integer (kind=4) :: nges,nge
      real    (kind=8) :: tf(nge,nge),dxz(nges)

      integer (kind=4) :: i,j,k,k0
      real    (kind=8) :: xj
c.. compute the inverse gacobian, ie dx/dz
c..   for the diagonal terms
      do 10 i=nges+1,nge
 10   tf(i,i)=1.d0/tf(i,i)
c..   for the offdiagonal terms, stored them in the upper triangul first
      if(nges.eq.0) go to 200
      do 15 i=nges,1,-1
 15   tf(i,nges+1)=-tf(nges+1,i)*dxz(i)*tf(nges+1,nges+1)
 200  if(nges+2.gt.nge) return
      do 20 j=nges+2,nge
      tf(j-1,j)=-tf(j,j-1)*tf(j-1,j-1)*tf(j,j)
      do 30 i=j-2,1,-1
      xj=0.d0
      if(i.le.nges) then
      xj=tf(j,i)*dxz(i)
      k0=nges+1
      else
      xj=tf(j,i)*tf(i,i)
      k0=i+1
      endif
      do 40 k=k0,j-1
 40   xj=xj+tf(j,k)*tf(i,k)
      tf(i,j)=-xj*tf(j,j)
 30   continue
 20   continue

      end subroutine fgam
