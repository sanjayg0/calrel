      subroutine dchisi (x,df,p,ier)

      implicit   none

      integer (kind=4) :: ier
      real    (kind=8) :: x,df,p

      integer (kind=4) :: ic, ist, isw, ncnt
      real    (kind=8) :: css
      real    (kind=8) :: dff, d, dx
      real    (kind=8) :: fcs, fxl, fxr
      real    (kind=8) :: p1
      real    (kind=8) :: xl,xp,xr

      integer (kind=4) :: itmax,nct,nsig
      real    (kind=8) :: eps, epsp

      data             eps/.0001d0/,itmax/50/,nct/20/,nsig/5/

c                           first executable statement

      ic = 0
      ier = 0
c                           estimate starting x
      call dnormi (xp,p,ier)
      if (ier .ne. 0) go to 80
      dff = .2222222d0
      dff = dff/df
      d = dsqrt(dff)
      x  = df*(1. -dff + xp*d)**3
c                           is the case asymptotically normal
      if (df .ge. 40.) go to 9005
c                           find bounds (in x) which enclose p
      ncnt = 0
      ist = 0
      isw = 0
      dx = x * .125d0
    5 if (x) 10,15,20
   10 x = 0.0
      dx = -dx
      go to 20
   15 dx = .1d0
   20 call dchis (x,df,p1,ier)
      dx = dx + dx
      if (ier .ne. 0) go to 85
      css = x
      ncnt = ncnt + 1
      if (ncnt .gt. nct) go to 90
      if (p1 - p) 25,9005,30
   25 x = x + dx
      isw = 1
      if (ist .eq. 0) go to 5
      go to 35
   30 x = x - dx
      ist = 1
      if (isw .eq. 0) go to 5
      xr = css
      xl = x
      go to 40
   35 xl = css
      xr = x
c                           prepare for iteration to find x
   40 epsp = 10.**(-nsig)
      if (xl .lt. 0.) xl = 0.0
      call dchis (xl,df,p1,ier)
      if (ier .ne. 0) go to 85
      fxl = p1 - p
      call dchis (xr,df,p1,ier)
      if (ier .ne. 0) go to 85
      fxr = p1-p
      if (fxl*fxr .ne. 0.0) go to 45
      x = xr
      if (fxl .eq. 0.0) x = xl
      go to 9005
   45 if (df .le. 2. .or. p .gt. .98) go to 50
c                           regula falsi method
      x = xl + fxl*(xr-xl)/(fxl-fxr)
      go to 55
c                           bisection method
   50 x = (xl+xr) * .5
   55 call dchis (x,df,p1,ier)
      if (ier .ne. 0) go to 85
      fcs = p1-p
      if (dabs(fcs) .gt. eps) go to 60
      go to 9005
   60 if (fcs * fxl .gt. 0.0) go to 65
      xr = x
      fxr = fcs
      go to 70
   65 xl = x
      fxl = fcs
   70 if (xr-xl .gt. epsp*dabs(xr)) go to 75
      go to 9005
   75 ic = ic+1
      if (ic .le. itmax) go to 45
      ier = 132
      go to 9000
c                           error returned from dnormi
   80 ier = 130
      go to 9000
c                           error returned from dchis
   85 ier = 131
      go to 9000
   90 ier = 129
 9000 continue
      call uertst (ier,'dchisi ')
 9005 return

      end subroutine dchisi
