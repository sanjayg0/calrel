      subroutine csens

      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4) :: isc,isv

      save

      isc=2
      call freei('c',isc,1)
      if(icl.ne.2) isc=1
      igf=0
      call freei('f',igf,1)
      if(icl.eq.1.and.igf.eq.0) igf=1
      isv=0
      call freei('v',isv,1)
      if(isc.eq.1) then
        write(not,1000)
c       if(icl.eq.1.and.igf.gt.0) write(not,1010) igf
        write(not,1020) isv
        call csenc(isv)
        else if(isc.eq.2) then
        write(not,1030)
        write(not,1020) isv
        call csent(isv)
      else
        write(not,1040)
        write(not,1020) isv
        call csenc(isv)
        call csent(isv)
        endif

 1000 format(//' >>>> SENSITIVITY ANALYSIS AT COMPONENT LEVEL <<<<'/)
c1010 format(' Limit-state function to be analyzed......igf=',i7)
 1020 format(
     & ' Type of parameters for sensitivity analysis'/
     & ' ..........................................isv=',i7/
     & '   isv=1 ......................distribution parameters'/
     & '   isv=2 ...................limit-state fcn parameters'/
     & '   isv=0 ..distribution and limit-state fcn parameters')
 1030 format(//' >>>> Sensitivity Analysis for Series System <<<<'/)
 1040 format(//
     &' >>>> SENSITIVITY ANALYSIS AT COMPONENT AND SYSTEM LEVELS <<<<'/)

      end subroutine csens
