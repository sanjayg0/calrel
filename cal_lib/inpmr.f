      subroutine inpmr(igfx,fltf,fltc,fltp,ims,im0)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: ims,im0
      integer (kind=4) :: igfx(ngf,3)
      logical          :: fltf(ngf),fltc(ngf),fltp(ngf)

      integer (kind=4) :: i,k,km, ng1
      save

      if(im0.eq.0.and.ims.eq.0) return
      if(ims.eq.1) then
        do 459 i=1,ngf
        fltc(i)=.false.
        fltp(i)=.false.
  459   fltf(i)=.false.
      endif
c.... for the case that cuts is skipped and the problem is a series system.
      if(im0.eq.0.and.icl.eq.2.and.imc.eq.0) then
        do 185 km=1,ngf
        igfx(km,1)=km
        igfx(km,2)=km
        igfx(km,3)=km
  185   continue
        ngfc=ngf
        imc=ngf
        return
      endif
c...  for the other case.
      if(ims.eq.1) then
c---  initialize fltf,fltc,fltp
        do 159 i=1,ngf
  159   igfx(i,1)=0
        if(icl.eq.1) then
          igfx(1,2)=1
          imc=1
          ngfc=1
          return
        endif
        do 129 i=1,ngfc
        igfx(i,1)=igfx(i,3)
 129    igfx(i,2)=i
        imc=ngfc
      else
c---  for icl.eq.1
        if(icl.eq.1) then
          ngfc=1
          return
        endif
c---  for the case that no change in cuts.
        if(im0.eq.0) return
c---    for the case that only cuts is reused.
        do 355 i=1,ngfc
          ng1=0
          do 360 k=1,imc
          if(igfx(k,1).eq.igfx(i,3)) then
            igfx(i,2)=k
            ng1=1
          endif
  360     continue
          if(ng1.eq.0) then
            imc=imc+1
            igfx(imc,1)=igfx(i,3)
            igfx(i,2)=imc
          endif
  355   continue
      endif

      end subroutine inpmr
