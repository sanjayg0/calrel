cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine form(xn,ex,sg,par,x0,bnd,tp,tf,igt,lgp,ids,ipa,ib,izx,
c                      ixz,fltf,beta,xlin,alpha,gamma,det,eta,sdgy,x,y,a,
c                      gam,t1,t2,t3,npr,ini,ist)

c   functions:
c      control the flow of solution procedures to compute first order
c      failure probability and relability index.

c   input arguments:
c      xn : names of x.
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      x0 : initial values of x.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      npr : output interval.
c            npr < 0 , no component output.
c            npr = 0 , output final steps.
c            npr > 0 , output at every npr steps.
c      ini : initialization flag.
c            ini = 0 , initialize at mean point.
c            ini >< 0, initialize at user-provided point.
c      ist : restart flag.
c            ist = 1 , restart problem.
c      igf : for icl=1, perform form for performance function igf.

c   output arguments:
c      beta : reliability indices for all failure modes.
c      xlin : design points for all failure modes.
c      alpha : unit normal in y space for all failure modes.
c      gamma : unit normal in z space for all failure modes.
c      adgx  : dgx in x space for all failure modes
c      det : normalized sg*d(beta)/d(ex)
c      eta : normalized sg*d(beta)/d(sg)
c      x : basic random variables in the original space.
c      y : basic variables in the standard space.
c      a : unit normal vector in the y space.
c      gam : unit normal vector in the z space.
c      t1 : temporary array.
c      t2 : temporary array.
c      t3 : temporary array.

c   calls: fdirct, fxtoy, fnewy1, fnewy2, fnewy3, fcheck, fsenvd, foutput.

c   called by: cform.

c  last version: dec. 24, 1990 by hong-zong lin

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine form(xn,ex,sg,par,x0,bnd,tp,tf,igt,lgp,ids,ipa,ib,
     *          izx,ixz,fltf,beta,xlin,alpha,gamma,det,eta,sdgy,x,y,
     *          a,gam,t1,t2,t3,gtor,igfx,yo,ao,yoo,
     *          yooo,yoooo,bbeta,curv,ylin,a1,y1,istep,npr,ini,ist,
     *          ifragil,fstart,fend,finc)

      implicit   none

      include   'blkrel1.h'
      include   'bsocf.h'
      include   'crel.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'fsav.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'
      include   'test.h'

!     integer (kind=4) :: l 
!     common /insa/       l(1)

      real (kind=8),allocatable :: tpc(:)
      real (kind=8),allocatable :: tpch(:)
      real (kind=8),allocatable :: pfch(:,:)
      real (kind=8),allocatable :: btch(:,:)

      character (len=4) :: xn(nrx)
      logical           :: fltf(ngf)

      integer (kind=4) :: npr,ini,ist,ifragil

      integer (kind=4) :: ids(nrx),ipa(4,nrx),igfx(ngf,3),ib(nrx)
      integer (kind=4) :: istep(nry-1,ngf)
      integer (kind=4) :: igt(nig),lgp(3,nig),izx(nry),ixz(nrx)

      real    (kind=8) :: fstart,fend,finc
      real    (kind=8) :: tf(*),x(nrx),x0(nrx),a(nry)
      real    (kind=8) :: y(nry),t1(nry),t2(nry),t3(nrx),gam(nry),tp(*)
      real    (kind=8) :: ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),gtor(ngf)
      real    (kind=8) :: xlin(nrx,ngf),alpha(nry,ngf),gamma(nry,ngf)
      real    (kind=8) :: det(nry,ngf),eta(nry,ngf),beta(ngf),sdgy(ngf)
      real    (kind=8) :: ylin(nry,ngf)

      real    (kind=8) :: yo(nry,ngf),ao(nry,ngf)
      real    (kind=8) :: yoo(nry,ngf),yooo(nry,ngf),yoooo(nry,ngf)
      real    (kind=8) :: bbeta(nry,ngf)
      real    (kind=8) :: curv(nry-1,ngf),y1(nry,ngf),a1(nry,ngf)

      integer (kind=4) :: i, ix,iy, igbs,igk,iijj,ik,ig2,ig4,ifra 
      integer (kind=4) :: idchange, icont
      integer (kind=4) :: j 
      integer (kind=4) :: k ,ki
      integer (kind=4) :: nbk,nck,ncount,ncur , ngdg
      integer (kind=4) :: ngrd,nhe1,nhe2,nnge,notf,nsdx,ntpara 

      real    (kind=8) :: bt,beff,bdp,bbe
      real    (kind=8) :: palfa,pf
      real    (kind=8) :: sbbeta,sdgy2,sepsb,sumb,sydp
      real    (kind=8) :: tchange

      save

      allocate(tpc(ntp))

      do i=1,ntp
        tpc(i)=tp(i)
      enddo
      if(ifragil.ne.0) then
c     ----- read fragility data -----
        idchange=ifragil
        ntpara=int((fend-fstart+finc*0.1d0)/finc)+1
      else
        ntpara=1
      endif

      allocate(tpch(ntpara))
      allocate(pfch(ntpara,ngfc))
      allocate(btch(ntpara,ngfc))

c     ***********************************************************************

c---  define b (approx. hessian) and q (gradient) of lagrangian
      if(iop.eq.4.or.iop.eq.7) then
        call lodefr('hes1',nhe1,nry+1,nry+1)
        call lodefr('hes2',nhe2,nry+1,nry+1)
        call lodefr('grdl',ngrd,1,nry)
      endif
c---  solution phase.
        call lodefr('gdgx',ngdg,1,nrx)
      if(icl.eq.1) call finim(igfx,igf)

      do ifra=1,ntpara

      do igk=ig0,ngfc
       ig2=igfx(igk,2)
       fltf(ig2)=.false.
      enddo
      addk=0
      if(ifragil.ne.0) then
        tchange=fstart+(ifra-1)*finc
        tpc(idchange)=tchange
        tpch(ifra)=tchange
      endif

      if(ist.ne.0) go to 399
      ig0=1
c---  initialize x.
 399  if(icl.ne.1) then
        if(ist.ne.0) go to 499
        nnge=0
        do 990 ki=1,nig
          if(igt(ki).le.2) go to 990
          if(ki.lt.nig) then
            nnge=nnge+(lgp(1,ki+1)-lgp(1,ki))*(lgp(1,ki+1)-lgp(1,ki))
          else
            nnge=nnge+(nry-lgp(1,ki)+1)*(nry-lgp(1,ki)+1)
          endif
 990    continue
        if(nnge.ne.0) then
          ntge=nnge
        else
          ntge=1
        endif
 499    call lodefr('sdxz',nsdx,nrx,3*ngf)
        call lodefr('stf ',notf,ntge,ngf)
      endif
      do igk=ig0,ngfc
        igbs=0
        ncount=0
        nbk=0
        ig2=igfx(igk,2)
        if(fltf(ig2)) go to 105
        fltf(ig2)=.true.
        ig4=igfx(igk,3)
        write(not,2000) ig4
        nck=0
        if(ist.ne.0) then
          bt = beta(ig2)
          sdgy2 = sdgy(ig2)
          go to 50
        endif
        do k=1,nry-1
          istep(k,ig2)=1
        end do ! k

c..{    iteration step: 0
        iter=1
        flgf=.true.
        flgr=.false.
        if(igr.ne.0) flgr=.true.
c       initialize x
        if(ini.lt.0) then
          do j=1,nrx
            x(j)=xlin(j,ig2)
          end do ! j
        else if(ini.eq.0) then
          do j=1,nrx
            x(j)=ex(j)
            if(ids(j).gt.50) x(j)=x0(j)
          end do ! j
        else
          do j=1,nrx
            x(j)=x0(j)
          end do ! j
        endif

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c       initialize y and beta.
        call cxtoy(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     1             izx,bt)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c       compute dg/dy and unit normal vectors in y space.
        call ugfun(gx,x,tpc,ig4)
        call fdirct(x,y,tpc,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,ids,
     1              ipa,izx,ixz,sdgy2,ia(ngdg),ig4,0)
c..}    end of iteration step: 0

  50    if(igbs.eq.0) then
          call foutput(ixz,xn,x,y,a,gam,t2,t3,ids,iter,
     &                 bt,npr,nck,gx,1,pf)
        endif

c..{    iteration step: 1
c       compute new x and y

        iter=iter+1

c       store y and a (new sorm)
        do ik=1,nry
          yoooo(ik,ig2)=yooo(ik,ig2)
          yooo(ik,ig2)=yoo(ik,ig2)
          yoo(ik,ig2)=yo(ik,ig2)
          yo(ik,ig2)=y(ik)
          ao(ik,ig2)=a(ik)
        end do ! ik
        if (nbk.eq.1) then
          call fnew1n(x,y,t1,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     *                izx,sdgy2,bt,gx,bbeta(1,ig2))
          go to 60
        endif
c       end of store y and a (new sorm)

        go to (51,52,53,54,55,58), iop

c       lh-rf method
  51    call fnewy1(x,y,t1,a,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,
     1              izx,sdgy2,bt,gx)
        go to 60
c       modified lh-rf method
  52    call fnewy2(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tpc,igt,lgp,
     1              ids,ipa,ib,izx,ixz,sdgy2,bt,iter,gx,ig4,
     2              ia(ngdg),des0)
        go to 60
c       gradient projection method
  53    call fnewy3(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tpc,igt,lgp,
     1              ids,ipa,ib,izx,ixz,sdgy2,bt,iter,gx,ig4,ia(ngdg))
        go to 60

   54   call fnewy4(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tpc,ia(nhe1),
     1          ia(nhe2),ia(ngrd),igt,lgp,ids,ipa,ib,izx,ixz,
     2          sdgy2,bt,iter,gx,ig4,nry+1,ia(ngdg))
        go to 60
c       improved hl-rf method
   55   call fnewy5(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tpc,igt,lgp,
     *       ids,ipa,ib,izx,ixz,sdgy2,bt,iter,gx,ig4,ia(ngdg))
        go to 60
c       polak-he algorithm
   58   call fnewy8(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tpc,igt,lgp,
     *       ids,ipa,ib,izx,ixz,sdgy2,bt,iter,gx,ig4,ia(ngdg))
        go to 60
c  56   call fnewy6(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
c    *       ids,ipa,ib,izx,ixz,sdgy2,bt,iter,gx,ig4
c    *       ,ia(ngdg))
c       go to 60
c  57   call fnewy7(x,y,a,t1,t2,t3,gam,tf,ex,sg,par,bnd,tp,ia(nhe1),
c    *             ia(nhe2),ia(ngrd),igt,lgp,ids,ipa,ib,izx,ixz,
c    *              sdgy2,bt,iter,gx,ig4,nry+1,ia(ngdg))

c---    compute dg/dy, unit normal vectors in y space.

   60   flgf=.true.
        flgr=.false.
        if(igr.ne.0) flgr=.true.
        if(iop.eq.1.or.nbk.eq.1) then
          call ugfun(gx,x,tpc,ig4)
          call fdirct(x,y,tpc,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,
     *                ids,ipa,izx,ixz,sdgy2,ia(ngdg),ig4,0)
        endif
c    ************   compute curvature  *******************************
        icont=1
        call fcurvat(yo(1,ig2),ao(1,ig2),y,a,curv(icont,ig2),
     *               ig4,tpc,ncur)
c       write (not,5001)                                                   ***
c       write (not,5002) iter,curv(icont,ig2)
c5002   format (24x,i4,5x,f15.10)                                          ***
c    **************** speed up convergence *****************************
        if (nbk.eq.1.and.iter.eq.2) then
          sepsb=0.d0
          do i=1,nry
            sepsb=sepsb+(y(i)-ylin(i,ig2))**2
          end do ! i
          sepsb=dsqrt(sepsb)
        endif
        if (nbk.eq.1.and.iter.eq.3) then
          sydp=0.d0
          bbe=0.d0
          do i=1,nry
            bbe=bbe+bbeta(i,ig2)*alpha(i,ig2)
            sydp=sydp+(ylin(i,ig2)-y(i))**2
          end do ! i
          sydp=dsqrt(sydp)
          palfa=sydp/sepsb
          sbbeta=0.d0
          do i=1,nry
            bbeta(i,ig2)=(epa/palfa*bbe-(epa/palfa-1.d0)*bdp)*
     *                    alpha(i,ig2)
            y(i)=ylin(i,ig2)-epc
            sbbeta=sbbeta+bbeta(i,ig2)*bbeta(i,ig2)
          end do ! i
          sbbeta=dsqrt(sbbeta)
          bbe=(epa/palfa*bbe-(epa/palfa-1.d0)*bdp)
c         write (not,3054)
c3054     format (/,'  value of b (translation of origin) ' )
c         write (not,*) bbe
          call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(gx,x,tpc,ig4)
          call fdirct(x,y,tpc,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,
     *                ids,ipa,izx,ixz,sdgy2,ia(ngdg),ig4,0)
          nbk=1
        endif
        if (nbk.eq.1.and.iter.gt.3) then
          bbe=0.d0
          do i=1,nry
            bbe=bbe+bbeta(i,ig2)*alpha(i,ig2)
          end do ! i
          beff=bdp-bbe
c         write (not,8329) beff*dabs(curv(icont,ig2))
c8329     format ( ' value of beff*cur = ',f20.10)
          bbe=bdp-epa/dabs(curv(icont,ig2))
          do i=1,nry
            bbeta(i,ig2)=bbe*alpha(i,ig2)
          end do ! i
          
        endif

c---  check convergence.

        if (nbk.eq.1) then
          call fchekn(y,yo(1,ig2),a,gx,iter,nck,ig4)
          goto 3621
        endif
        call fcheck(y,a,gx,iter,nck,ig4)
 3621   if(nck.eq.0) go to 50
        if(igbs.eq.1) go to 105
        gtor(ig2)=dabs(gx)
c---------------------------------------------------------------------
c       end of iteration
c--------------------------------------------------------------------

c---  save dx/dz and dy/dz for series system.
        if(ngf.gt.1 .and. icl.ne.1)
     1    call fsave(ia(notf),ia(nsdx),tf,t3,igt,lgp,ig2,0,ia(ngdg),y)
c---  compute the normalized sensitivity measure.
        if(nck.eq.-1) go to 70
        if(ifeap.ne.0) call psavx
        call fsenvd(a,tf,x,y,t1,ex,sg,par,tpc,det(1,ig2),eta(1,ig2),
     *               t3,igt,lgp,ids,izx,sdgy2,ig2,ig4)
c---  keep the results of this performance function.
   70   do ix=1,nrx
          xlin(ix,ig2)=x(ix)
        end do ! ix
        beta(ig2)=bt
        sdgy(ig2)=sdgy2
        do iy=1,nry
          alpha(iy,ig2)=a(iy)
          ylin(iy,ig2)=y(iy)
          gamma(iy,ig2)=gam(iy)
        end do ! iy

c---    output final results.
        if(nck.eq.1.and.npr.ge.0) then
          call foutput(ixz,xn,x,y,a,gam,det(1,ig2),eta(1,ig2),ids,
     *                 iter,bt,npr,nck,gx,2,pf)
          pfch(ifra,igk)=pf
          btch(ifra,igk)=bt
        else if(nck.eq.-1) then
          ig0=igk
          fltf(ig2)=.false.

          if(ifragil.ne.0) then
            do iijj=ig0,ngfc
              ig4=igfx(iijj,3)
              write(not,'(1h ,''                     '')')
              write(not,'(1h ,''===== fragility for limit state'',i5,
     *                    '' ====='')') ig4
              write(not,'(1h ,2x3hseq,10x5hparam,13x2hpf,11x4hbeta)')
              do i=1,ifra-1
                write(not,'(1h ,i5,1p3e15.5)')i,tpch(i),
     *           pfch(i,iijj),btch(i,iijj)
              enddo
            enddo
          endif

          deallocate(tpc)
          deallocate(tpch)
          deallocate(pfch)
          deallocate(btch)
          call cstop
        end if
c       run =etime(tarray)
c       print *,'run time for form =',run
c       do 999 i=1,npf
c         if(ipf(i,3).ne.0) then
c           write(not,9981) i,ipf(i,1),ipf(i,2)
c9981         format('ipf=',i4,3x'n=',i2,'   p=',i2)
c         endif
c999    continue
 2000   format(/' Limit-state function ',i5)
        bdp=bt
        if (iop.eq.1.or.iop.eq.3.or.iop.eq.4.or.
     &                  iop.eq.5.or.iop.eq.6) then
          nbk=0
          goto 105
        endif
c       write (not,6570) ncount
c6570   format ( '  number of hl-rf step with alfa=1   ',i5)
        if (nbk.eq.1) goto 105
c       ****************  detect case beta*k1 greater than 1 **************
        if (ncount.le.3) then
          bdp=beta(ig2)
c         write (not,6571)
c6571       format ( '  beta*k1 greater than 1 ')
          nbk=1
          iter=1
          do i=1,nry
            y(i)=y(i)-epc
          end do ! i
          sumb=0.d0
          do k=1,nry
            sumb=sumb+alpha(k,ig2)*(ylin(k,ig2)-y(k))
          end do ! k
          do j=1,nry
            y(j)=y(j)+sumb*alpha(j,ig2)
          end do ! j
          call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(gx,x,tpc,ig4)
          call fdirct(x,y,tpc,ex,sg,t3,gam,a,tf,par,bnd,igt,lgp,
     *                ids,ipa,izx,ixz,sdgy2,ia(ngdg),ig4,0)
          nck=0
          igbs=1
          goto 50
        else
          nbk=0
        endif
  105   do ik=1,nry
          y1(ik,ig2)=y(ik)
          a1(ik,ig2)=a(ik)
        end do ! ik
        istep(icont,ig2)=iter
      end do ! igk

      enddo

      if(ifragil.ne.0) then
        do igk=ig0,ngfc
          ig4=igfx(igk,3)
          write(not,'(1h ,''                     '')')
          write(not,'(1h ,''===== fragility for limit state'',i5,
     *                 '' ====='')') ig4
          write(not,'(1h ,2x3hseq,10x5hparam,13x2hpf,11x4hbeta)')
          do i=1,ntpara
            write(not,'(1h ,i5,1p3e15.5)')i,tpch(i),
     *       pfch(i,igk),btch(i,igk)
          enddo
        enddo
      endif
c     call delete('gdgx')
      if(iop.eq.4.or.iop.eq.7) then
      call delete('hes1')
      call delete('hes2')
      call delete('grdl')
      endif
      deallocate(tpc)
      deallocate(tpch)
      deallocate(pfch)
      deallocate(btch)

      end subroutine form
