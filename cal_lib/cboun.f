      subroutine cboun

      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer    n1,n2,n3,n4,n5,n6,n7,n8,n9, nr,nc

      save

      call freei('t',ibt,1)
      write(not,1000) ibt
      call locate('beta',n1,nr,nc)
      call locate('alph',n2,nr,nc)
      call lodefr('rhom',n3,ngf,ngf)
      call lodefr('prob',n4,ngf,ngf)
      call define('prbr',n5,ngfc,ngfc)
      call lodefi('next',n6,1,ngf)
      call defini('ibx ',n7,ngfc-1,2)
      call defini('ilub',n8,ngfc-1,ngfc-1)
      call locate('igfx',n9,nr,nc)
      call bounds(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5)
     *          ,ia(n6),ia(n7),ia(n8),ia(n9))
      itt = 1

 1000 format(//' >>>> FIRST-ORDER BOUNDS FOR SERIES SYSTEM <<<<'//
     & ' Type of bounds ..........................ibt=',i7,/
     & '   ibt=1 .............................unimodal bounds'/
     & '   ibt=2 .....................relaxed bimodal  bounds'/
     & '   ibt=3 .............................bimodal  bounds'/
     & '   ibt=0 ............................all of the above')

      end subroutine cboun
