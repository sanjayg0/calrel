!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine fdirct(x,y,tp,ex,sg,dgx,gam,a,tf,par,bnd,igt,lgp,ids,ipa,
!                        izx,ixz,sdgy,ig)

!   functions:
!      compute dg/dy, unit normal vectors in y and z space, and |dg/dy|.

!   input arguments:
!      x : basic random variables in the original space.
!      y : basic variables in the standard space.
!      tp : deterministic parameters in performance function.
!      ex : mean values of x.
!      sg : standard deviations of x.
!      tf : the transformation matrix from z to y, i.e. y = tf * z.
!      par : distribution parameters of x.
!      igt : group types.
!      lgp : location of the first element of a group in y.
!      ids : distribution numbers of x.
!      ipa : #'s of the associated rv of distribution parameters.
!             n-th element of ipa = 0 means parameter n is constant.
!      izx : indicators for z to x.
!      ixz : indicators for x to z.
!      ig : performance function number.

!   output arguments:
!      dgx : dg/dx.
!      gam : unit normal vector in the z space.
!      a : unit normal vector in the y space.
!      sdgy : |dg/dy|.

!   calls: cdgx, udgx, cdxz, cdyx, cdot.

!   called by: form, fnewy2.

!   last revision: june 22 1989 by h.-z. lin.

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fdirct(x,y,tp,ex,sg,dgx,gam,a,tf,par,bnd,igt,lgp,
     *                  ids,ipa,izx,ixz,sdgy,gdgx,ig,indx)

      implicit   none

      include   'flag.h'
      include   'prob.h'

!     real tarray(2)

      integer (kind=4) :: ig,indx
      real    (kind=8) :: sdgy
      integer (kind=4) :: ipa(4,nrx),igt(nig),izx(nry),ixz(nrx)
      integer (kind=4) :: ids(nrx)
      integer (kind=4) :: lgp(3,nig)
      real    (kind=8) :: x(nrx),y(nry),tp(*),ex(nrx),sg(nrx),dgx(nrx)
      real    (kind=8) :: gam(nry),a(nry),tf(ntf),par(4,nrx),gdgx(nrx)
      real    (kind=8) :: bnd(2,nrx)

      integer (kind=4) :: i,ii,is0
      integer (kind=4) :: j,jj,j0,j1,j1s, jend
      integer (kind=4) :: kk
      integer (kind=4) :: nn,nn1, ng0,nge,nges
      real    (kind=8) :: ysum,sdgz
      real    (kind=8) :: xsg

      real    (kind=8) :: cdot 

      save

      if(indx.eq.1) go to 990
!---  compute dg/dx.

      if(igr.eq.0) then
        call cdgx(x,sg,gdgx,tp,ig,ids,0)
      else
        call udgx(gdgx,x,tp,ig)
      endif
      do i = 1,nry
        j      = izx(i)
        gam(i) = gdgx(j)
      end do ! i

!---  loop over groups

 990  nn=1
      do 100 ii=1,nig
        j0=lgp(1,ii)
        if(ii.lt.nig) then
          j1=lgp(1,ii+1)-1
        else
          j1=nry
        endif
        nge=j1-j0+1
        if(nge.le.0) go to 100
        nn1=nn-1+nge*nge

!---  type 1 groups

        if(igt(ii).eq.1) then
!....   compute dx/dz.
          if(indx.ne.1) call cdxz(x,dgx(j0),par,ids,izx(j0),nge)
!....   compute dg/dy dg/dx * dx/dy. dg/dx=gam(.); dx/dy=dgx(.); a(.) = dg/dy.
          do i = j0,j1
            gam(i) = gam(i)*dgx(i)
            a(i)   = gam(i)
          end do ! i

!---  type 2 groups

        else if(igt(ii).eq.2) then
!---  compute dx/dz.
          if(indx.ne.1) call cdxz(x,dgx(j0),par,ids,izx(j0),nge)
!....   compute dg/dz = dg/dx * dx/dz. gam(.)=dg/dx; dx/dz=dgx(.).
          do i = j0,j1
            gam(i) = gam(i)*dgx(i)
          end do ! i
!---  compute dg/dy = dg/dz * dz/dy (=inv tf)
          a(j1)=gam(j1)/tf(nn1)
          do j = j1-1,j0,-1
            a(j)=gam(j)
            jj=nn+(j-j0)*nge+j-j0
            a(j)=(a(j)-cdot(tf(jj+1),a(j+1),1,1,j1-j))/tf(jj)
          end do ! j

!---  type 3 groups

        else if(igt(ii).eq.3) then
!---  for the marginal distributed subgroup:
!---  compute dx/dz.
          j1s=lgp(2,ii)
          nges=j1s-j0+1
          if(nges.le.0) go to 360
          if(indx.ne.1) call cdxz(x,dgx(j0),par,ids,izx(j0),nges)
          if(j1.le.j1s) then
!---  compute dg/dz = dg/dx * dx/dz.
            do i = j0,j1s
              gam(i) = gam(i)*dgx(i)
            end do ! i
            go to 370
          endif
!---  for the conditionally distributed subgroup:
!---  compute dy/dx and store in tf.
  360     if(j1.le.j1s) go to 150
          if(indx.ne.1) then
            call cdyx(x,y(j0),ex,sg,par,bnd,tf(nn),ids,ipa,
     *               izx(j0),ixz,nges,nge,j0,igt(ii),0)
            call fgam(tf(nn),dgx(j0),nges,nge)
          endif
!---  compute dg/dz
          do j=j0,j1
            if(j.lt.j0+nges) then
!     ---- for marginally distributed sub group -----
              gam(j)=gam(j)*dgx(j)
              jj=nn+nges*nge+j-j0
!       ------ original ------
!             gam(j)=gam(j)+cdot(tf(jj),gam(j0+nges),nge,1,nge-nges)
!       ------ original ------
!       ------ modified by k.fujimura 02/04/2003 ------
              do kk=1,nge-nges
                gam(j)=gam(j)+gam(j0+nges+kk-1)*tf(jj+(kk-1)*nge)
              enddo
!       ------ modified by k.fujimura 02/04/2003 ------
            else
              jj=nn+(j-j0)*nge+j-j0
!       ------ original ------
!             gam(j)=cdot(tf(jj),gam(j),nge,1,j1-j+1)
!       ------ original ------
!       ------ modified by k.fujimura 02/04/2003 ------
              ysum=0.0d0
              do kk=1,nge-j+1
                ysum=ysum+tf(jj+(kk-1)*nge)*gam(j+kk-1)
              enddo
              gam(j)=ysum
!       ------ modified by k.fujimura 02/04/2003 ------
            endif
          end do ! j
!---  compute dg/dy = dg/dz * (inv tf) or dg/dy = dg/dx * (inv dy/dx).
  370     if(j1.gt.j1s) then
            do j=j1s+1,j1
              a(j)=gam(j)
            end do ! j
            if(nges.le.0) go to 399
            jend=j1s
            ng0=nges
          else
            jend=j1
            ng0=nge
          endif
          jj=nn+(jend-j0)*nge-1
          a(jend)=gam(jend)/tf(jj+ng0)
          if(jend-1.lt.j0) go to 386
          do j=jend-1,j0,-1
            a(j)=gam(j)
            jj=nn+(j-j0)*nge+j-j0
            a(j)=(a(j)-cdot(tf(jj+1),a(j+1),1,1,ng0-j))/tf(jj)
          end do ! j
 386      continue
 399      if(j1.le.j1s.or.indx.eq.1) go to 150
!---     compute gamma
          if(nges.ne.0) then
            do i=j0,j1s
              gam(i)=gdgx(izx(i))*dgx(i)
            end do ! i
            is0=j1s+1
          else
            gam(j0)=gdgx(izx(j0))*tf(nn)
            is0=j0+1
          endif
          do i=is0,j1
            jj=nn+(i-j0)*nge
            xsg=cdot(tf(jj),tf(jj),1,1,i-j0+1)
            gam(i)=gdgx(izx(i))*dsqrt(xsg)
          end do ! i
        endif

  150   nn=nn1+1
  100 continue

!---  normalize -dg/dy, and -dg/dz.

      if(indx.eq.1) return
      sdgz = dsqrt(cdot(gam,gam,1,1,nry))
      sdgy = dsqrt(cdot(a,a,1,1,nry))
!     a(.) = - (dg/dy)/|dg/dy|
      do i=1,nry
        gam(i) = -gam(i)/sdgz
        a(i)   = -a(i)/sdgy
      end do ! i

      end subroutine fdirct
