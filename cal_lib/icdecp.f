cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine icdecp(a,m,n,mm)
c
c   functions:
c      perform cholesky decomposition, a=ll'.
c
c   input arguments:
c      a : symmetric banded matrix.
c      m : band width of a.
c      n : dimension of a.
c      mm  : declared row number of a in the calling routine.
c
c   output arguments:
c      a : lower triangle - l, upper triangle - l'.
c
c   calls: cerror.
c
c   called by: input.
c
c   last revision: sept. 18 1986 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine icdecp(a,m,n,mm)

      implicit   none

      integer (kind=4) :: m,n,mm
      real    (kind=8) :: a(1,1)
!WIP  a is a(1,1) crazy coding?

      integer (kind=4) :: i,ia,i1, ier
      integer (kind=4) :: j,ja,jm
      integer (kind=4) :: k
      real    (kind=8) :: aij

c---  perform choleskey decompution.

      ja=1
      do j=1,n
        jm=max0(j-m,1)
        ia=(jm-1)*mm+1
        do i=jm,j
          i1=i-1
          aij=a(i,ja)
          do k=jm,i1
            aij=aij-a(k,ia)*a(k,ja)
          end do ! k
          if(j.ne.i) go to 25
          if(aij) 60,60,20
   20     a(i,ja)=dsqrt(aij)
   25     a(i,ja)=aij/a(i,ia)
          ia=ia+mm
        end do ! i
        ja=ja+mm
      end do ! j

c---  transpose u to get l

      ja=1
      do 50 j=2,n
      ja=ja+mm
      ia=1
      do 50 i=1,j-1
      a(j,ia)=a(i,ja)
   50 ia=ia+mm
      return

c---  print the negative diagonal.

   60 ier=3
      call cerror(13,j,aij,' ')

      end subroutine icdecp
