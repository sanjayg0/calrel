cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cxtoy(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,izx,bt)
c
c   functions:
c      transform x to y, and compute reliability index.
c
c   input arguments:
c      x : basic variables in the original space.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : mean values of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      izx : no. of the corresponding x associated with each z.
c
c   output arguments:
c      y : basic variables in the standard space.
c      z : basic variables in the correlated normal space.
c      bt : reliability index.
c
c   calls: cdot, cparvl, cxtoz.
c
c   called by: form.
c
c   last revision: june 15 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cxtoy(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *              izx,bt)

      implicit   none

      include   'prob.h'

      real    (kind=8) :: x(nrx),y(nry),z(nry),tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx), bnd(2,nrx)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),izx(nry),lgp(3,nig)
      integer (kind=4) :: igt(nig)

      integer (kind=4) :: i ,ii
      integer (kind=4) :: j0,j1, jj
      integer (kind=4) :: k
      integer (kind=4) :: nge,nges, nn
      real    (kind=8) :: bt

      real    (kind=8) :: cdot

      save
c                                         !by adk, 5/3/01
c---  initialize the y vector             !by adk, 5/3/01
c                                         !by adk, 5/3/01
      do ii=1,nry                        !by adk, 5/3/01
        y(ii) = 0.0d0                        !by adk, 5/3/01
      end do ! ii
c
c---  loop over groups
c
      nn=1
      do 100 i=1,nig
      j0=lgp(1,i)
      if(i.eq.nig) then
        j1 = nry
      else
        j1 = lgp(1,i+1)-1
      endif
      nge=j1-j0+1
      if(nge.le.0) go to 100

c---  for type 1 groups, transform y -> x directly.
      if(igt(i).eq.1) then
        do ii=j0,j1
          k=izx(ii)
          call cxtoz(x,y(ii),par(1,k),sg(k),ids(k),k)
        end do ! ii

c---  for type 2 groups, first transform x -> z, then transform z -> y.
      else if(igt(i).eq.2) then
        do ii = j0,j1
          k=izx(ii)
          call cxtoz(x,z(ii),par(1,k),sg(k),ids(k),k)
          do jj = j0,j1
            y(ii) = y(ii)+tf(nn+ii-j0+(jj-j0)*nge)*z(jj)       !by adk, 5/3/01
          end do ! jj
c         y(ii) = cdot(tf(nn+ii-j0),z(j0),nge,1,nge) !commented by adk, 5/3/01
        end do ! ii

c---  for type 3 groups, compute dist. parameters, then transform y -> x.
      else if(igt(i).eq.3) then
        if(lgp(2,i).lt.j0) go to 40
        nges=lgp(2,i)-j0+1
        do ii=j0,lgp(2,i)
          k=izx(ii)
          call cxtoz(x,z(ii),par(1,k),sg(k),ids(k),k)
          do jj=j0,j1
            y(ii) = y(ii)+tf(nn+ii-j0+(jj-j0)*nge)*z(jj) !by k.fujimura, 2/4/03
          end do ! jj
c          y(ii)=cdot(tf(nn+ii-j0),z(j0),nge,1,nges)  !commented by k.fujimura,
        end do ! ii
   40   if(lgp(2,i)+1.gt.j1) go to 165
        do ii=lgp(2,i)+1,j1
          k=izx(ii)
          call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),
     *           ids(k),ipa(1,k),1)
          call cxtoz(x,y(ii),par(1,k),sg(k),ids(k),k)
        end do ! ii

c---  for type 4 groups, compute dist. parameters, then transform y -> x.
c     else
c     j1s=lgp(2,i)
c     j0s=lgp(3,i)
c     nges=j1-j1s+1
c     ngec=j0s-j0+1
c     j1s0=(j1s-j0)*nge
c       if(j1s.le.j0) go to 140
c       do 150 ii=j0,j1s-1
c       k=izx(ii)
c     call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
c 150   call cxtoz(x,y(ii),par(1,k),sg(k),ids(k),k)
c 140   if(j0s.lt.j1s) go to 160
c       nngs=j0s-j1s+1
c       do 180 ii=j1s,j0s
c       k=izx(ii)
c      call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
c       call cxtoz(x,z(ii),par(1,k),sg(k),ids(k),k)
c 180   y(ii)=cdot(tf(nn+j1s0+ii-j0),z(j1s-j0+1),nge,1,nngs)
c 160   if(j1.le.j0s) go to 165
c       do 130 ii=j0s+1,j1
c       k=izx(ii)
c       call cxtoz(x,z(ii),par(1,k),sg(k),ids(k),k)
c 130   y(ii)=cdot(tf(nn+j1s0+ii-j0),z(j1s-j0+1),nge,1,nges)
      endif
  165 nn=nn+nge*nge
  100 continue
c
c---  compute the initial beta
c
      bt=dsqrt(cdot(y,y,1,1,nry))

      end subroutine cxtoy
