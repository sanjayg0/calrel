cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine secant(bt,rt,y1,yn,y01,ii,z,x,tf,ids,par,tp,bnd,sg,
c    *                  ex,ib,igt,lgp,ipa,izx,tor,xk,gam,ig)
c
c   functions:
c     use secant algorithm to find root for the point fitting method.
c
c   input arguments:
c     bt  : reliability index.
c     rt  : rotational transformation matrix, y  = rt * y'.
c     y1  : value of y'(+,- i),  = k*bt.
c     ii  : value of coordinate cross section. ex:(ii,nrx).
c     x   : design point in the x space.
c     tf  : the transformation matrix from z to y, y  = tf * z.
c     ids: distribution number of x.
c     par : distribution parameters of x.
c     tp  : deterministic parameters.
c     bnd : lower and upper bounds of x.
c     sg  : standard deviations of x.
c     ex  : means of x
c     ib  : code of distribution bounds of x.
c     tor : tolerence for secant routine.
c     xk  : value used to determine y1, ie, y1 = xk*bt.
c     ds  : parameters in full distribution function.
c     igt : group types.
c     lgp : location of first element of a group in y.
c     izx : a vector associated with z indicating the no. of corresponding x
c     ipa : #'s of the associated rv of distribution parameters.
c           n-th element  =  0 means parameter n is a constant.
c
c   output arguments:
c     yn  : solution of secant method. ie, (y1,yn) is the fitting point
c           in this semiparobolic.
c     gam :  = 1 + 2*bt*a, here a is curvature in this semiparobolic.
c
c     calls:  cytox.
c
c     called by: sopera.
c
c     last revision: july 20, 1987 by h.-z. lin.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine secant(bt,rt,y1,yn,y01,ii,z,x,tf,ids,par,tp,bnd,sg,
     *                  ex,ib,igt,lgp,ipa,izx,tor,xk,gam,ig,inp,gou)

      implicit   none

      include   'grad.h'
      include   'prob.h'

      integer (kind=4) :: ii, ig, inp
      real    (kind=8) :: bt, gam,gou, tor, xk, y1,yn
      integer (kind=4) :: ids(nrx),igt(nig)
      integer (kind=4) :: lgp(3,nig),ipa(4,nrx),izx(nry)
      real    (kind=8) :: rt(nry,nry),y01(nry),z(nry),x(nrx),tf(ntf)
      real    (kind=8) :: bnd(2,nrx),sg(nrx),ib(nrx),par(4,nrx),tp(*)
      real    (kind=8) :: ex(nrx)

      integer (kind=4) :: i,j, n, ind, index
      real    (kind=8) :: pa, pa2,pai1,pai2
      real    (kind=8) :: x1,x2,x3, xxk
      real    (kind=8) :: y1s,yb,yy1,yy2
      save

      flgf = .true.
      flgr = .false.
      pa = acos(0.d0)
      pa2 = pa/9.d0
      yb = dsqrt(y1*y1+bt*bt)
      y1s = dabs(y1)
      pai1 = acos(y1s/yb)
      y1s = y1/y1s
      xxk = xk*xk
      ind = 1
      yn = bt
      do 151 i = 1,nry
        y01(i) = rt(i,ii)*y1+rt(i,nry)*yn
 151  continue
      call cytox(x,y01,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(yy1,x,tp,ig)
      gou = yy1
c     if(dabs(yy1).le.tor) go to 88
c.....check surface : downward or upward to the origin in order to set
c     the secand point.
      if(yy1.gt.0.d0) go to 70
          if(bt.ge.0.d0) then
          index = 0
          x1 = yn
             if(bt.le.1.d0) then
             index = 1
             pai2 = pai1-pa2
             x1 = pai1
             yn = yb*sin(pai2)
             y1 = y1s*yb*cos(pai2)
             x2 = pai2
             else
             yn = bt*(1.d0-xxk/2.d0)
             x2 = yn
             endif
          else
          index = 1
          x1 = -pai1
          pai2 = -(pai1+pa2)
          yn = yb*sin(pai2)
          y1 = y1s*yb*cos(pai2)
          x2 = pai2
c     print *,'y1 = ',y1,'yn=',yn
          endif
   77 do 20 i = 1,nry
   20 y01(i) = rt(i,ii)*y1+rt(i,nry)*yn
      call cytox(x,y01,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(yy2,x,tp,ig)
c     print *,'yy2 = ',yy2
      n = 2
          if(dabs(yy2).le.tor) then
c       print *,'yy2-yy1 = ',yy2-yy1
        x3 = x1-yy1*(x2-x1)/(yy2-yy1)
              if(index.eq.0) then
            yn = x3
              else
            yn = yb*sin(x3)
            y1 = y1s*yb*cos(x3)
              endif
        gou = yy2
        go to 88
          endif
   22 x3 = x1-yy1*(x2-x1)/(yy2-yy1)
c     print *,'x3 = ',x3
  345     if(index.eq.0) then
        yn = x3
          else
        yn = yb*sin(x3)
        y1 = y1s*yb*cos(x3)
c       print *,'y1 = ',y1,'yn=',yn
          endif
      x1 = x2
      x2 = x3
      do 30 j = 1,nry
   30 y01(j) = rt(j,ii)*y1+rt(j,nry)*yn
      call cytox(x,y01,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      yy1 = yy2
      call ugfun(yy2,x,tp,ig)
c     print *,'yy2 = ',yy2
      n = n+1
          if(dabs(yy2).le.tor.or.n.gt.inp) then
        x3 = x1-yy1*(x2-x1)/(yy2-yy1)
              if(index.eq.0) then
            yn = x3
              else
            yn = yb*sin(x3)
            y1 = y1s*yb*cos(x3)
c       print *,'n = ',n,'y1=',y1,'yn=',yn
              endif
        gou = yy2
        go to 88
          end if
      go to 22
c-----if the surface is upward.
   70     if(bt.ge.0.d0) then
        index = 1
        x1 = pai1
        pai2 = pai1+pa2
        yn = yb*sin(pai2)
        y1 = y1s*yb*cos(pai2)
        x2 = pai2
          else
        index = 0
        x1 = yn
          if(-bt.le.1.0) then
          index = 1
          x1 = -pai1
          pai2 = -pai1+pa2
          yn = yb*sin(pai2)
          y1 = y1s*yb*cos(pai2)
          x2 = pai2
          else
          yn = bt-bt*xxk/2.d0
          x2 = yn
          endif
        endif
      go to 77
   88 continue
c-----compute the gam = 1 + 2*bt*a, a=curvature.
        gam = 1.d0+2.d0*bt*(yn-bt)/(y1*y1)
          if(ind.eq.1.and.gam.le.0.00001.and.dabs(yy2).gt.0.0001) then
        tor = 0.0001
        ind = 2
        go to 345
          endif
c     ikm = ikm+n
c     print *,'ikm = ',ikm

      end subroutine secant
