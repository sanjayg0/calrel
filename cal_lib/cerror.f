cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cerror(ier,n,r,cc)
c
c   functions:
c      print error messages.
c
c   input arguments:
c      ier : error code.
c      n,r : numbers required in error messages.
c      cc : character string.
c
c   called by: main, cbdecp, fcheck, spthree, cztoxu.
c
c   last revision: july 29 1987 by p.-l. liu.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cerror(ier,n,r,cc)

      implicit   none

      include   'blkrel1.h'
      include   'file.h'

      character  cc*80
      integer    ier, n
      real*8     r

      save

      if(ier.gt.40) go to 140
      if(ier.gt.20) go to 120
      if(ier.gt.10) go to 110

      go to (1,2,3,4,5,6,7,8,9),ier

c---  subroutine defin

    1 write(not,1010) cc,n,mtot
 1010 format(/' ERROR 1: Memory exceeded - array=',4a1,
     *       /'          Storage required  =',i7,
     *       /'          Storage available =',i7)
      return

c---  subroutine cexect

    2 write(not,1020) cc(1:4)
 1020 format(' WARNING 2: Command ',a4,' not available')
      return

c---  subroutine cexect
c---  input problem size.

    3 write(not,1030)
 1030 format(' ERROR 3: Problem size undefined')
      call cstop
      return

c---  subroutine cexect
c---  input problem data.

    4 write(not,1040)
 1040 format(' ERROR 4: Problem data not available.')
      call cstop
      return

c---  subroutine cexect
c---  perform 1st order first.

    5 write(not,1050)
 1050 format(' ERROR 5: 1st order results not available')
      call cstop
      return

c---  subroutine cexect
c---  skip the 'bound' command.

    6 write(not,1060)
 1060 format(' WARNING 6: 1st order probability bounds only apply to',
     *       ' series systems.')
      return

c---  subroutine cexect
c---  perform 2nd order first.

    7 write(not,1070)
 1070 format(' WARNING 7: 2nd order results not available')
      call cstop
      return

c---  subroutine cztoxu
c---  iteration no. exceeds nit1 when solving f(x)=z.

    8 write(not,1080) n,r
 1080 format(' WARNING 8: fx(x) = z is not solved within 100 steps'/,
     *       '            ndis=',i3,',   fx(x) - z = ',1pe14.7)
      return

c---  subroutine locate

    9 write(not,1090) cc(1:4)
 1090 format(' ERROR 9: Array "',a4,'" not previously defined.')
      call cstop
      return

c---  subroutine lodefi, lodefr

      write(not,1100)
 1100 format(' ERROR 10: Problem size changed.')
      call cstop
      return

c---  subroutine input

  110 go to (11,12,13,14,15,16,17),ier-10

   11 write(not,1110) cc(1:4)
 1110 format(' WARNING 11: Data type ',a4,' not available')
      call cstop
      return

c---  subroutine infree

   12 write(not,1120)
 1120 format(' ERROR 12: End of data file')
      call cstop
      return

c---  subroutine icbdcp
c---  check correlation matrix in input file.

   13 write(not,1130) n,n,r
 1130 format(' ERROR 13: Correlation matrix not positive definite'/
     *       ' ro(',i3,',',i3,')=',e13.6/)
      call cstop
      return

c---  subroutine inpro

   14 write(not,1140)
 1140 format(' ERROR 14: Correlation matrix input error')
      call cstop
      return

c---  subroutine inline

   15 write(not,1150) cc
 1150 format(' ERROR 15: Input error, last input :'/a)
      call cstop
      return

c---  subroutine input

   16 write(not,1160) n
 1160 format(' ERROR 16: Variable',i5,' is undefined')
      call cstop
      return

c---  subroutine cexect

   17 write(not,1170) n
 1170 format(' ERROR 17: Total number of basic variables =',i5)
      call cstop
      return

c---  subroutine fcheck

c---  change maximum iteration number and restart.

  120 go to (21,22),ier-20

   21 write(not,1210) n
 1210 format(' ERROR 21: Convergence not achieved in ni1 steps',
     *       ' for failure mode',i5)
c     call cstop
      return

c---  subroutine fnewy

   22 write(not,1220)
 1220 format(' WARNING 22: Convergence not achieved in line search',
     *       ' in ni2 steps')
      return

c---  subroutine mont, dir1, dir2, dirs, crest

  140 continue
      write(not,1410)
 1410 format(' WARNING 41: No previous run')

c---  subroutine pthree
c---  Tvedt's formulas cannot be used.

c   5 write(not,1050)
c1050 format(' ERROR 5: For some principle curvatures 1+(bt+1)*c < 0'/
c    *       '          Tvedt''s formulas cannot be used')

      end subroutine cerror
