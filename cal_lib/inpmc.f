      subroutine inpmc(mc,igfx)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      save

      integer (kind=4) :: mc(ntl,2),igfx(ngf)

      integer (kind=4) :: i,j,k,l, ii,ik, i0
      integer (kind=4) :: mm, ncheck, ng0
      integer (kind=4) :: r1
!     real    (kind=8) :: r1
!WIP  It was implicitly real*8 before

      ngfc = 1
      do k = 2,ngf
        igfx(k)=0
      end do ! k

      mc(1:ntl,1) = 0
      read(nin,*,err=99) (mc(i,1),i=1,ntl)
      goto 98

!     Read error

   99 continue
      write(not,'(1h '' !! fatal error           !!'')')
      write(not,'(1h '' !! wrong input for cutset!!'')')
      write(not,'(1h '' !! check the data        !!'')')
      stop

   98 continue
      ncheck=0
      do i=1,ntl
        if(mc(i,1).eq.0) ncheck=ncheck+1
      end do ! i

      if(ncheck.ne.ncs) then
        write(not,'(1h '' !! fatal error                         !!'')')
        write(not,'(1h '' !! wrong input for cutset              !!'')')
        write(not,'(1h '' !! number of zeros must be equal to ncs!!'')')
        write(not,'(1h '' !! check the data                      !!'')')
        stop
      endif
      write(not,2030)
      igfx(1)=abs(mc(1,1))
      j=1
      if(icl.eq.3) then

!---  for general system.

        do i0 = 1,ncs
          do k = j,ntl
            if(mc(k,1).eq.0) go to 132
            ng0 = 0
            ik  = abs(mc(k,1))
            do i = 1,ngfc
              if(igfx(i).eq.ik) then
                ng0 = 1
                go to 131
              endif
            end do ! i
            ngfc       = ngfc+1
            igfx(ngfc) = ik
  131       continue
          end do ! k
  132     write(not,2031) i0,(mc(l,1),l=j,k-1)
          j = k + 1
        end do ! i0

!---  for series system.

      else
        do k=1,ntl-1,2
          ng0=0
          ik=abs(mc(k,1))
          do i=1,ngfc
            if(igfx(i).eq.ik) then
              ng0 = 1
              go to 25
            endif
          end do ! i
          ngfc       = ngfc+1
          igfx(ngfc) = ik
  25      mc(k+1,1)  = 0
          mc(k+1,2)  = 0
        end do ! k
        write(not,2032) (mc(l,1),l=1,ntl-1,2)
      endif

c.... arrange igfc in orderings.

      if(ngfc.le.1) return

      mm=ngfc-1
   80 do ii=1,mm
        if(igfx(ii).gt.igfx(ii+1)) then
          r1=igfx(ii)
          igfx(ii)=igfx(ii+1)
          igfx(ii+1)=r1
        endif
      end do ! ii
      if(mm.gt.1) then
        mm=mm-1
        go to 80
      endif

!...  assign mc in ordering of igfx

      j=1
      do i0=1,ncs
        do k=j,ntl
            if(mc(k,1).eq.0) then
              mc(k,2)=0
              go to 440
            else
              ik=abs(mc(k,1))
              do i=1,ngfc
                if(igfx(i).eq.ik) then
                  mc(k,2)=i
                  go to 480
                endif
              end do ! i
            endif
  480      continue
         end do ! k
  440    j=k+1
       end do ! i0

!     Formats

 2030 format(/' cut sets:')
 2031 format('   cut set',i3,':',(10i5))
 2032 format(' components of series system:',/,(10i5))

      end subroutine inpmc
