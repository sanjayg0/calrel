cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cparvl(x,ex,sg,par,bnd,ids,ipa,indx)
c
c   functions:
c      assign values to the distribution parameters of a variable.
c
c   input arguments:
c      x : current value of the basic random variables.
c      ipa : #'s of the associated rv of distribution parameters
c             n-th element.eq.0 means parameter n = par(n).
c      ids : distribution number.
c      indx: indx=0, first time to call cparvl
c
c   output arguments:
c      ex : current mean of x.
c      sg : current standard deviation of x.
c      par : values of the distribution parameters.
c
c   called by: input, fxtoy, cytox.
c
c   last revision: dec. 1 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cparvl(x,ex,sg,par,bnd,ids,ipa,indx)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: ids, indx 
      integer (kind=4) :: ipa(4)
      real    (kind=8) :: ex,sg
      real    (kind=8) :: x(nrx),par(4),bnd(2)

      if(ipa(1).ne.0.or.ipa(2).ne.0.or.ipa(3).ne.0.or.ipa(4).ne.0)
     *go to 33
      if(indx.eq.0) call cparam(ex,sg,par,ids)
      return
 33   if(ipa(3).ne.0) par(3)=x(iabs(ipa(3)))
      if(ipa(4).ne.0) par(4)=x(iabs(ipa(4)))
      if(iabs(ids).le.50) then
        if(ipa(1).lt.0) then
          par(1)=x(-ipa(1))
        else if(ipa(1).gt.0) then
          ex=x(ipa(1))
        endif
        if(ipa(2).lt.0) then
          par(2)=x(-ipa(2))
        else if(ipa(2).gt.0) then
          sg=x(ipa(2))
        endif
      else
        if(ipa(1).ne.0) par(1)=x(iabs(ipa(1)))
        if(ipa(2).ne.0) par(2)=x(iabs(ipa(2)))
        go to 66
      endif
      if(ipa(1).lt.0.or.ipa(2).lt.0.or.ipa(3).lt.0.or.ipa(4).lt.0) then
        call cparam(ex,sg,par,-iabs(ids))
      else
      call cparam(ex,sg,par,ids)
      endif
c     update the bounds
  66  if(ids.gt.50) return
        if(ids.eq.4.or.ids.eq.5) then
        bnd(1)=par(2)
      else if(ids.eq.6) then
        bnd(1)=par(1)
        bnd(2)=par(2)
      else if(ids.eq.7) then
        bnd(1)=par(3)
        if(par(3).eq.0.and.par(4).eq.0.) par(4)=1.d0
        bnd(2)=par(4)
      end if

      end subroutine cparvl
