cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cexect

c   functions:
c      control the execution of macro commands.

c   calls: cerror, cinpu, cform, csorm, cboun, csens, cdirs, cmont.

c   called by: main.

c   last revision: july 31 1987 by p.-l. liu.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cexect

      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'flcg.h'
      include   'line.h'
      include   'optm.h'
      include   'prob.h'

      logical       userflag, uname
      common /ulic/ userflag, uname

      character  comd(15)*4
      logical    compar

      integer    i,ic, kk,kk1,kk2, lrec
      real*8     r

      save

      data comd/'calr','data','form','boun','sens','sorm',
     &          'dirs','mont','rest','pnet','exit','scis',
     &          'lpbo','join','sysa'/

c---  Output title

      call title

c---  Check license

      if(.not.userflag) then
        write(*,*) ' Not licensed to use CALREL'
        stop
      endif

c---  Read solution commands

   10 call freetm
      do ic=1,14
        if(compar(line,comd(ic))) then
          go to 30
        endif
      end do ! ic
      call cerror(2,ic,r,line)
      go to 10
   30 continue

      go to (110,120,130,140,150,160,170,180,190,200,210,220,230,240),ic

      write(*,*) ' IC =',ic

c---  input control information.

  110 if(.not.flc(1)) then
        do i = 1,5
          flc(i) = .true.
        end do ! i
      endif
      ngf = 1
      call freei('F',ngf,1)
      nig = 1
      call freei('G',nig,1)
      nrx = 0
      call freei('X',nrx,1)
      if(nrx.le.0) call cerror(17,nrx,0.d0,' ')
      ntp = 0
      call freei('P',ntp,1)
      write(not,2010) ngf,nig,nrx,ntp
      flc(1) = .false.
      call clear
      call defini('fltf',kk,1,ngf)
      call defini('fltc',kk1,1,ngf)
      call defini('fltp',kk2,1,ngf)
      call cdeflt(ia(kk),ia(kk1),ia(kk2))
      go to 10

c---  input problem data.

  120 if(flc(1)) call cerror(3,0,0.d0,' ')
      call cinpu
      flc(2) = .false.

c     portability: unit is mechine dependent: Use inquire to length

      inquire(iolength=lrec) (r,i=1,max(nrx,nry) + 6)
      open(ns0,file='rel.ns0',form='unformatted',status='unknown',
     &     access='direct',recl=lrec)
      go to 10

c---  perform 1st order analysis.

  130 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      call cform
      flc(3) = .false.
      itt = 0
      gmin = 1
      gmax = 1
      go to 10

c---  compute 1st order probability bounds.

  140 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      if(flc(3)) call cerror(5,0,0.d0,' ')
      if(icl.ne.2.and.icl.ne.4) then
         call cerror(6,0,0.d0,' ')
      else
        call cboun
      endif
      flc(4)=.false.
      go to 10

c---  perform 1st order sensitivity analysis.

  150 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      if(flc(3)) call cerror(5,0,0.d0,' ')
      call csens
      go to 10

c---  perform 2nd order analysis.

  160 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      if(flc(3)) call cerror(5,0,0.d0,' ')
      call csorm
      flc(5)=.false.
      go to 10

c---  perform directional simulation.

  170 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      call cdirs
      go to 10

c---  perform monte carlo simulation.

  180 if(flc(1)) call cerror(3,0,0.d0,' ')
      if(flc(2)) call cerror(4,0,0.d0,' ')
      call cmont
      go to 10

c---  restart the previous solution.

  190 call crest
      imc=1
      go to 10

c---  do pnet analysis

  200 call cpnet
      go to 10

c---  do scis simulation

  220 call cscis
      go to 10

c---  do lp bounds

  230 call clpboun
      go to 10

c---  do lp system analysis with joint design point

  240 call cjoinsy
      go to 10

c---  exit the program.

  210 return

c---  output formats.

 2010 format(//' >>>> NEW PROBLEM <<<<'//
     &  ' Number of limit-state functions..........ngf=',i7/
     &  ' Number of independent variable groups ...nig=',i7/
     &  ' Total number of random variables ........nrx=',i7/
     &  ' Number of limit-state parameters ........ntp=',i7)

      end subroutine cexect
