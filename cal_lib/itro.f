cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine itro(ro,n1,n2,r1,r2,u1,s1,u2,s2)
c
c   functions:
c      transform the correlation coefficient of two x's to z space.
c
c   input arguments:
c      ro : correlation coefficient of x1 and x2.
c      n1,n2 : distribution numbers of x1 and x2.
c      r1,r2 : correlation coefficients of x1 and x2.
c      u1,s1 : extra dist. parameters (required if x1 is beta dist.)
c      u2,s2 : extra dist. parameters (required if x2 is beta dist.)
c
c   output arguments:
c      ro : correlation coefficient of z1 and z2.
c
c   calls: none.
c
c   called by: itraro.
c
c   last revision: sept. 18 1986 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine itro(ro,n1,n2,r1,r2,u1,s1,u2,s2)

      implicit   none

      integer (kind=4) :: n1,n2
      real    (kind=8) :: ro, r1,r2, u1,u2, s1,s2

      integer (kind=4) :: nn1,nn2
      real    (kind=8) :: o,oo,oq
      real    (kind=8) :: p,pp,pq,pr
      real    (kind=8) :: q,qq 
      real    (kind=8) :: r,rc,rr,rs
      real    (kind=8) :: s,sc,sp,sq,ss, sgl,sgl1,sgl2
      real    (kind=8) :: u,uc,up,uq,us,uu
      real    (kind=8) :: x,xp,xq,xs,xu,xx

c---  if either of x's distribution is user-defined, return.

      if(n1.gt.50.or.n2.gt.50) return
      x=ro
      if(n1.le.n2) then
         nn1=n1
         nn2=n2
         p=r1
         q=r2
      else
         nn1=n2
         nn2=n1
         p=r2
         q=r1
         u=u1
         s=s1
         u1=u2
         s1=s2
         u2=u
         s2=s
      end if
      xx=x*x
      pp=p*p
      qq=q*q
      xp=x*p
      xq=x*q
      pq=p*q
      go to (100,200,300,400,500,600,700,1,1,1,1100,1200,1300,1400),nn1
  100 go to (1,102,103,104,105,106,107,1,1,1,111,112,113,114),nn2
c---  normal, lognormal.
  102 sgl=dsqrt(dlog(1.d0+qq))
      ro=q/sgl*x
      go to 2000
c---  normal, gamma.
  103 ro=(1.001-0.007*q+0.118*qq)*x
      go to 2000
c---  normal, exponential.
  104 ro=1.107*x
      go to 2000
c---  normal, rayleigh.
  105 ro=1.014*x
      go to 2000
c---  normal, uniform.
  106 ro=1.023*x
      go to 2000
c---  normal, beta.
  107 ro=1.026+0.001*x-0.178*u2+0.268*s2-0.001*xx
     *   +0.178*u2*u2-0.679*s2*s2-0.003*x*s2
      ro=ro*x
      go to 2000
c---  normal, type 1 largest.
  111 ro=1.031*x
      go to 2000
c---  normal, type 1 smallest.
  112 ro=1.031*x
      go to 2000
c---  normal, type 2 largest.
  113 ro=(1.030+0.238*q+0.364*qq)*x
      go to 2000
c---  normal, type 3 smallest.
  114 ro=(1.031-0.195*q+0.328*qq)*x
      go to 2000
  200 go to (202,203,204,205,206,207,1,1,1,211,212,213,214),nn2-1
c---  lognormal, lognormal.
  202 sgl1=dsqrt(dlog(1.d0+pp))
      sgl2=dsqrt(dlog(1.d0+qq))
      ro=dlog(1.d0+x*pq)/sgl1/sgl2
      go to 2000
c---  lognormal, gamma.
  203 ro=1.001+0.033*x+0.004*p-0.016*q+0.002*xx+0.223*pp+0.130*qq
      ro=(ro-0.104*xp+0.029*pq-0.119*xq)*x
      go to 2000
c---  lognormal, exponential.
  204 ro=(1.098+0.003*x+0.019*p+0.025*xx+0.303*pp-0.437*xp)*x
      go to 2000
c---  lognormal, rayleigh.
  205 ro=(1.011+0.001*x+0.014*p+0.004*xx+0.231*pp-0.130*xp)*x
      go to 2000
c---  lognormal, uniform.
  206 ro=(1.019+0.014*p+0.010*xx+0.249*pp)*x
      go to 2000
c---  lognormal, beta.
  207 uu=u2*u2
      ss=s2*s2
      xu=x*u2
      xs=x*s2
      us=u2*s2
      up=u2*p
      sp=s2*p
      ro=0.979 + 0.053*x + 0.181*u2 + 0.293*s2 + 0.002*p
     *    - 0.004*xx - 0.181*uu + 5.796*ss + 0.277*pp - 0.107*xu
     *    - 0.619*xs - 0.190*xp - 3.976*us - 0.097*up + 0.133*sp
     *    -14.402*ss*s2 - 0.069*pp*p
     *    + 0.031*x*xs + 0.015*x*xp
     *    + 3.976*uu*s2 + 0.097*uu*p - 0.430*ss*p
     *    + 0.113*sp*p + 1.239*x*us + 0.380*x*up
      ro=ro*x
      go to 2000
c---  lognormal, type 1 largest.
  211 ro=(1.029+0.001*x+0.014*p+0.004*xx+0.233*pp-0.197*xp)*x
      go to 2000
c---  lognormal, type 1 smallest.
  212 ro=(1.029-0.001*x+0.014*p+0.004*xx+0.233*pp+0.197*xp)*x
      go to 2000
c---  lognormal, type 2 largest.
  213 ro=1.026+0.082*x-0.019*p+0.222*q+0.018*xx+0.288*pp+0.379*qq
      ro=(ro-0.441*xp+0.126*pq-0.277*xq)*x
      go to 2000
c---  lognormal, type3 smallest.
  214 ro=1.031+0.052*x+0.011*p-0.210*q+0.002*xx+0.220*pp+0.350*qq
      ro=(ro+0.005*xp+0.009*pq-0.174*xq)*x
      go to 2000
  300 go to (303,304,305,306,307,1,1,1,311,312,313,314),nn2-2
c---  gamma, gamma.
  303 ro=1.002+0.022*x-0.012*(p+q)+0.001*xx+0.125*(pp+qq)
      ro=(ro-0.077*(xp+xq)+0.014*pq)*x
      go to 2000
c---  gamma, exponential.
  304 ro=(1.104+0.003*x-0.008*p+0.014*xx+0.173*pp-0.296*xp)*x
      go to 2000
c---  gamma, rayleigh.
  305 ro=(1.014+0.001*x-0.007*p+0.002*xx+0.126*pp-0.090*xp)*x
      go to 2000
c---  gamma, uniform.
  306 ro=(1.023+0.000*x-0.007*p+0.002*xx+0.127*pp-0.000*xp)*x
      go to 2000
c---  gamma, beta.
  307 uu=u2*u2
      ss=s2*s2
      xu=x*u2
      xs=x*s2
      us=u2*s2
      up=u2*p
      sp=s2*p
      if(x.gt.0.) then
      ro=0.931+0.050*x+0.366*u2+0.549*s2+0.181*p
     *  -0.055*xx-0.515*uu+4.804*ss-0.484*pp-0.064*xu
     *  -0.637*xs+0.032*xp-4.059*us-0.319*up-0.211*sp
     *  +0.052*xx*x+0.227*uu*u2-10.220*ss*s2+0.559*pp*p-0.042*x*xu
     *  +0.223*x*xs-0.172*x*xp+0.028*x*uu+0.695*x*ss+0.126*x*pp
     *  +3.845*uu*s2+0.019*uu*p-1.244*us*s2+0.008*up*p-2.075*ss*p
     *  +0.167*sp*p+0.666*x*us+0.386*x*up-0.517*x*sp+2.125*us*p
      else
      ro=1.025+0.050*x-0.029*u2+0.047*s2-0.136*p
     *  +0.069*xx+0.178*uu+6.281*ss+0.548*pp-0.027*xu
     *  -0.686*xs+0.046*xp-3.513*us+0.231*up+0.299*sp
     *  +0.063*xx*x-0.226*uu*u2-17.507*ss*s2-0.366*pp*p+0.051*x*xu
     *  -0.246*x*xs+0.186*x*xp-0.001*x*uu+0.984*x*ss+0.121*x*pp
     *  +3.700*uu*s2+0.081*uu*p+1.356*us*s2+0.002*up*p+1.654*ss*p
     *  -0.135*sp*p+0.619*x*us+0.410*x*up-0.686*x*sp-2.205*us*p
      end if
      ro=ro*x
      go to 2000
c---  gamma, type 1 largest.
  311 ro=(1.031+0.001*x-0.007*p+0.003*xx+0.131*pp-0.132*xp)*x
      go to 2000
c---  gamma, type 1 smallest.
  312 ro=(1.031-0.001*x-0.007*p+0.003*xx+0.131*pp+0.132*xp)*x
      go to 2000
c---  gamma, type 2 largest.
  313 ro=1.029+0.056*x-0.030*p+0.225*q+0.012*xx+0.174*pp+0.379*qq
      ro=(ro-0.313*xp+0.075*pq-0.182*xq)*x
      go to 2000
c---  gamma, type 3 smallest.
  314 ro=1.032+0.034*x-0.007*p-0.202*q+0.000*xx+0.121*pp+0.339*qq
      ro=(ro-0.006*xp+0.003*pq-0.111*xq)*x
      go to 2000
  400 go to (404,405,406,407,1,1,1,411,412,413,414),nn2-3
c---  exponential, exponential.
  404 ro=(1.229-0.367*x+0.153*xx)*x
      go to 2000
c---  exponential, rayleigh.
  405 ro=(1.123-0.100*x+0.021*xx)*x
      go to 2000
c---  exponential, uniform.
  406 ro=(1.133+0.029*xx)*x
      go to 2000
c---  exponential, beta.
  407 uu=u2*u2
      ss=s2*s2
      xu=x*u2
      xs=x*s2
      us=u2*s2
      ro=1.082-0.004*x+0.204*u2+0.432*s2-0.001*xx
     *  - 0.204*uu+7.728*ss+0.008*xu-1.699*xs-5.338*us
     *  -19.741*ss*s2+0.135*x*xs+5.338*uu*s2+3.397*x*us
      ro=ro*x
      go to 2000
c---  exponential, type 1 largest.
  411 ro=(1.142-0.154*x+0.031*xx)*x
      go to 2000
c---  exponential, type 1 smallest.
  412 ro=(1.142+0.154*x+0.031*xx)*x
      go to 2000
c---  exponential, type 2 largest.
  413 ro=(1.109-0.152*x+0.361*q+0.130*xx+0.455*qq-0.728*xq)*x
      go to 2000
c---  exponential, type 3 smallest
  414 ro=(1.147+0.145*x-0.271*q+0.010*xx+0.459*qq-0.467*xq)*x
      go to 2000
  500 go to (505,506,507,1,1,1,511,512,513,514),nn2-4
c---  rayleigh, rayleigh.
  505 ro=(1.028-0.029*x)*x
      go to 2000
c---  rayleigh, uniform.
  506 ro=(1.038-0.008*xx)*x
      go to 2000
c---  rayleigh, beta.
  507 ro=1.037-0.042*x-0.182*u2+0.369*s2-0.001*xx
     *     +0.182*u2*u2-1.150*s2*s2+0.084*x*u2
      ro=ro*x
      go to 2000
c---  rayleigh, type 1 largest.
  511 ro=(1.046-0.045*x+0.006*xx)*x
      go to 2000
c---  rayleigh, type 1 smallest.
  512 ro=(1.046+0.045*x+0.006*xx)*x
      go to 2000
c---  rayleigh, type 2 largest.
  513 ro=(1.036-0.038*x+0.266*q+0.028*xx+0.383*qq-0.229*xq)*x
      go to 2000
c---  rayleigh, type 3 smallest.
  514 ro=(1.047+0.042*x-0.212*q+0.353*qq-0.136*xq)*x
      go to 2000
  600 go to (606,607,1,1,1,611,612,613,614),nn2-5
c---  uniform, uniform.
  606 ro=(1.047-0.047*xx)*x
      go to 2000
c---  uniform, beta.
  607 ro=1.040+0.015*x-0.176*u2+0.432*s2-0.008*xx
     *   +0.176*u2*u2-1.286*s2*s2-0.137*x*s2
      ro=ro*x
      go to 2000
c---  uniform, type 1 largest.
  611 ro=(1.055+0.015*xx)*x
      go to 2000
c---  uniform, type 1 smallest.
  612 ro=(1.055+0.015*xx)*x
      go to 2000
c---  uniform, type 2 largest.
  613 ro=(1.033+0.305*q+0.074*xx+0.405*qq)*x
      go to 2000
c---  uniform, type 3 smallest.
  614 ro=(1.061-0.237*q-0.005*xx+0.379*qq)*x
      go to 2000
  700 go to (707,1,1,1,711,712,713,714),nn2-6
c---  beta, beta.
  707 o=u1
      p=s1
      q=u2
      r=s2
      u=o+q
      s=p+r
      oo=o*o
      pp=p*p
      qq=q*q
      rr=r*r
      oq=o*q
      pr=p*r
      us=oo+qq
      ss=pp+rr
      uc=oo*o+qq*q
      sc=pp*p+rr*r
      if(ro.gt.0) then
      ro=1.030-0.050*x-0.056*u+0.094*s+0.009*xx-0.084*us+2.583*ss
     *  +0.100*x*u+0.010*x*s-0.485*u*s+0.270*oq-0.688*pr-0.011*xx*x
     *  +0.024*uc-10.786*sc+0.013*xx*u-0.035*xx*s+0.001*x*us-0.069*x*ss
     *  +1.174*us*s+0.004*oq*u+0.227*ss*u+2.783*pr*s+0.058*x*s*u
     *  -0.260*x*oq-0.352*x*pr-1.609*oq*s+0.194*pr*u
      else
      ro=0.939-0.023*x+0.147*u+0.668*s+0.035*xx-0.008*us+3.146*ss
     *  +0.103*x*u-0.126*x*s-1.866*u*s-0.268*oq-0.304*pr+0.011*xx*x
     *  -0.024*uc-10.836*sc-0.013*xx*u-0.035*xx*s-0.001*x*us+0.069*x*ss
     *  +1.175*us*s-0.005*oq*u-0.270*ss*u+2.781*pr*s+0.058*x*u*s
     *  -0.259*x*oq+0.352*x*pr+1.608*oq*s-0.189*pr*u
      end if
      ro=ro*x
      go to 2000
c---  beta, type 1 largest.
  711 ro=1.055-0.066*x-0.194*u1+0.391*s1+0.003*xx
     *   +0.194*u1*u1-1.134*s1*s1+0.130*x*u1+0.003*x*s1
      ro=ro*x
c---  beta, type 1 smallest.
      go to 2000
  712 ro=1.055+0.066*x-0.194*u1+0.391*s1+0.003*xx
     *   +0.194*u1*u1-1.134*s1*s1-0.130*x*u1-0.003*x*s1
      ro=ro*x
      go to 2000
c---  beta, type 2 largest.
  713 uu=u1*u1
      ss=s1*s1
      xu=x*u1
      xs=x*s1
      us=u1*s1
      uq=u1*q
      sq=s1*q
      ro=1.005 + 0.091*x + 0.285*u1+ 0.260*s1+ 0.199*q
     *  - 0.023*xx - 0.285*uu + 8.180*ss + 0.543*qq - 0.181*xu
     *  - 1.744*xs - 0.336*xq - 5.450*us - 0.265*uq + 0.514*sq
     *  -19.661*ss*s1- 0.178*qq*q
     *  + 0.244*x*xs + 0.066*xx*q - 0.001*x*ss
     *  + 5.450*uu*s1+ 0.265*uu*q - 0.986*ss*q
     *  + 0.133*sq*q + 3.488*x*us + 0.671*x*uq
      ro=ro*x
      go to 2000
c---  beta, type 3 smallest.
  714 ro=1.054+0.002*x-0.176*u1+0.366*s1-0.201*q
     *   -0.002*xx+0.176*u1*u1-1.098*s1*s1+0.340*qq
     *   -0.004*x*u1-0.029*s1*q
      ro=ro*x
      go to 2000
 1100 go to (1111,1112,1113,1114),nn2-10
c---  type 1 largest, type 1 largest.
 1111 ro=(1.064-0.069*x+0.005*xx)*x
      go to 2000
c---  type 1 largest, type 1 smallest.
 1112 ro=(1.064+0.069*x+0.005*xx)*x
      go to 2000
c---  type 1 largest, type 2 largest.
 1113 ro=(1.056-0.060*x+0.263*q+0.020*xx+0.383*qq-0.332*xq)*x
      go to 2000
c---  type 1 largest, type 3 smallest.
 1114 ro=(1.064+0.065*x-0.210*q+0.003*xx+0.356*qq-0.211*xq)*x
      go to 2000
 1200 go to (1212,1213,1214),nn2-11
c---  type 1 smallest, type 1 smallest.
 1212 ro=(1.064-0.069*x+0.005*xx)*x
      go to 2000
c---  type 1 smallest, type 2 largest.
 1213 ro=(1.056+0.060*x+0.263*q+0.020*xx+0.383*qq+0.332*xq)*x
      go to 2000
c---  type 1 smallest, type 3 smallest.
 1214 ro=(1.064-0.065*x-0.210*q+0.003*xx+0.356*qq+0.211*xq)*x
      go to 2000
 1300 go to (1313,1314),nn2-12
c---  type 2 largest, type 2 largest.
 1313 rs=pp+qq
      rc=pp*p+qq*q
      r=p+q
      ro=1.086+0.054*x+0.104*r-0.055*xx+0.662*rs-0.570*x*r+0.203*pq
      ro=(ro-0.020*xx*x-0.218*rc-0.371*x*rs+0.257*xx*r+0.141*pq*r)*x
      go to 2000
c---  type 2 largest, type 3 smallest.
 1314 ro=1.065+0.146*x+0.241*p-0.259*q+0.013*xx+0.372*pp+0.435*qq
      ro=(ro+0.005*xp+0.034*pq-0.481*xq)*x
      go to 2000
 1400 go to (1414),nn2-13
c---  type 3 smallest, type 3 smallest.
 1414 ro=1.063-0.004*x-0.200*(p+q)-0.001*xx+0.337*(pp+qq)
      ro=(ro+0.007*(xp+xq)-0.007*pq)*x
 2000 if(ro.gt.1.0d0) ro=0.999999999
      if(ro.lt.-1.d0) ro=-0.999999999
    1 return
      end
