cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fnewy2(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
c                        ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig)

c   functions:
c      compute new x and y using modified rf method.
c      the descent function des = [y-(y*a)*a]^2 + op2*gx^2.

c   input arguments:
c      x : current x.
c      y : current y.
c      a : unit normal vector in the y space.
c      d : direction of search.
c      yt : temporary y.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : means of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      sdgy : |dg/dy|.
c      iter : iteration number.
c      gx : g(x).
c      ig : failure mode number.

c   output arguments:
c      x : new x.
c      y : new y.
c      dxz : dx/dz.
c      gam : unit normal vector in the z space.
c      bt : new reliability index.

c   calls: ugfun, cdot, cytox, fdirct.

c   called by: form.

c   last revision: dec. 20 1990 by h.-z. lin.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnewy2(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *            ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,gdgx,des0)

      implicit   none

      include   'flag.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'

      integer (kind=4) :: iter,ig
      real    (kind=8) :: sdgy,bt,gx,des0
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry),yt(nry),dxz(nrx),gam(nry)
      real    (kind=8) :: tf(ntf),ex(nrx),sg(nrx),gdgx(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),tp(*)
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig)
      integer (kind=4) :: izx(nry),ixz(nrx),ipa(4,nrx)

      integer (kind=4) :: i,iii
      real    (kind=8) :: alpha, cpara
      real    (kind=8) :: des,dy
      real    (kind=8) :: s
      real    (kind=8) :: t, term1,term2 

      real    (kind=8) :: cdot
      save


c---  if iter = 2, compute the initial value of the descent function:
c---  des = [y-(y*a)*a]^2 + op2*gx^2.

      flgf=.true.
      if(igr.ne.0) flgr=.true.
      if(iter.gt.2) go to 40
      t=cdot(y,a,1,1,nry)
      term1 = 0.0d0
      do 28 i=1,nry
      dy=y(i)-t*a(i)
   28 term1=term1+dy*dy
      term2=gx*gx
      if(int(op2).eq.10) then
        cpara = dmax1(tol*tol,term1)/dmax1(tol*tol,term2)
      else
        cpara = op2
      endif
      des0=term1+cpara*gx*gx

c---  use hl-rf method to determine direction of search.

   40 s=gx/sdgy+cdot(y,a,1,1,nry)
      do 50 i=1,nry
   50 d(i)=s*a(i)-y(i)
      alpha=1.
c     ncount=ncount+1
      iii=0
c----------------------------------------------------------------------
c     perform line search.
c----------------------------------------------------------------------
c---  find a new y along the search direction.

   60 do 70 i=1,nry
   70 yt(i) = y(i) + alpha*d(i)

c---  compute new x and new unit normal vector.

      call cytox(x,yt,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      call fdirct(x,yt,tp,ex,sg,dxz,gam,a,tf,par,bnd,igt,lgp,ids,
     *            ipa,izx,ixz,sdgy,gdgx,ig,0)

c---  compute new des, i.e. new value of the descent function.

      t=cdot(yt,a,1,1,nry)
      des=cpara*gx*gx
      do 80 i=1,nry
      dy=yt(i)-t*a(i)
   80 des=des+dy*dy
      iii=iii+1

c---  if des < des0 or sub-iteration number exceeds ni2, return.
c---  if des > des0, repeat line search procedures.

      if(des.lt.des0.or.iii.gt.ni2) go to 90
      alpha=alpha*op1
      go to 60
   90 bt=dsqrt(cdot(yt,yt,1,1,nry))
      if(t.lt.0.d0) bt=-bt
      do 100 i=1,nry
  100 y(i)=yt(i)
      des0=des

      end subroutine fnewy2
