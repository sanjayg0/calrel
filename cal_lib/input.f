cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine input(xn,ex,sg,par,x0,bnd,tp,igt,lgp,ids,ipa,ib,
c                       izx,ixz,tf,nds,igx,nes,les,fltf,igfx,nls)
c
c   functions:
c      read and print input data.
c
c   output arguments:
c      xn : names of x.
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      x0 : initial values of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      ro : correlation matrix of x.
c      igt : group types.
c      lgp : initial positions of groups in y array.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa  =  0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c            =  0 , no bounds
c            =  1 , bounded from below.
c            =  2 , bounded from above.
c            =  3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      mc : minimum cut sets.
c      tf : transformation matrix.
c      nes : starting and ending addresses of transition-function
c            element stress list in array les.
c      les : list of element stresses/strains used in transition
c            functions.
c      fltf : flags for form analysis.
c              =  .true. , form has been done for the transition function.
c              =  .false. , form hasn't been done for the transition function.
c
c   calls: cerror, infree, inline, cparam, cparvl, define, defini, inpro,
c          delete, chgdim.
c
c   called by: cinpu.
c
c   revision: jun. 12 1990 by h.-z. lin.
c   last revision: march 1991 by y. zhang
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine input(xn,ex,sg,par,x0,bnd,tp,igt,lgp,ids,ipa,ib,izx,
     *                ixz,tf,nds,igx,nes,les,fltf,fltc,fltp,igfx,nls)
c    *                ixz,tf,nds,igx,nes,les,fltf,igfx,nls)

      implicit   none

      include   'blkrel1.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'line.h'
      include   'mdat4.h'
      include   'optm.h'

      character (len=4) :: icom,coms(8),xn(nrx)
      character (len=80):: title
      character (len=60):: out

      logical           :: compar,fltf(ngf),fltc(ngf),fltp(ngf)
c     logical           :: compar,fltf(ngf)

      integer (kind=4) :: nls
      real    (kind=8) :: ex(nrx),sg(nrx),par(4,nrx),x0(nrx)
      real    (kind=8) :: bnd(2,nrx),tp(*)
      real    (kind=8) :: tf(*), p(4),rin(9)
      integer (kind=4) :: igt(nig),lgp(3,nig),ids(nrx),ipa(4,nrx)
      integer (kind=4) :: ib(nrx),igx(nrx),nds(nrx)
      integer (kind=4) :: izx(nry),ixz(nrx)
      integer (kind=4) :: ibound(14),ip(4),iin(9)
      integer (kind=4) :: nes(2,ngf),les(*),igfx(ngf,3)

      integer (kind=4) :: i, i1, ic,ig, im0,ims, iids, itime
      integer (kind=4) :: j,jj
      integer (kind=4) :: k,kk
      integer (kind=4) :: mst
      integer (kind=4) :: nc,nn,nr,nv,n1,n2x
      integer (kind=4) :: ngc,ngd,nge,ngm,ngn,ngx, ntol, nline
      real    (kind=8) :: r, xx0
      save

      data ibound /0,1,1,1,1,3,3,0,0,0,0,0,1,1/
      data coms/'titl','flag','cuts','opti','stat','para','tran','end '/
      data itime  /0/

c---  read the first 4 characters and switch to desired route.

      ims = 0
      im0 = 0

   10 read(nin,'(a4)') icom
      do ic = 1,8
        if(compar(icom,coms(ic))) go to 30
      end do ! ic
      call cerror(11,ic,r,icom)
      go to 10

   30 go to (110,120,130,140,150,160,170,180),ic

c---  input title.

  110 call infree(1,iin,rin)
      nline = iin(1)
      do i = 1,nline
        read(nin,'(a)') title
        write(not,2010) title
      end do ! i
      go to 10

c---  input control data.

  120 call infree(2,iin,rin)
      icl = iin(1)
      igr = iin(2)
      write(not,2020) icl,igr
      go to 10

c---  input minimum cut sets.

  130 call infree(2,iin,rin)
      ncs = iin(1)
      ntl = iin(2)
      write(not,2015) ncs,ntl
      ntl = iin(2)+ncs
      call locate('mc  ',mst,nr,nc)
      call inpmc(ia(mst),igfx(1,3))
      im0 = 1
      go to 10

c---  input optimization parameters.

  140 call infree(9,iin,rin)
      iop   = iin(1)
      ni1   = iin(2)
      ni2   = iin(3)
      tol   = rin(4)
      op1   = rin(5)
      op2   = rin(6)
      op3   = rin(7)
      iline = iin(8)
      nidt  = iin(9)
      if (nidt.eq.2) then
        ntol = nidt
      else        ! rlt bug fix
        ntol = 0
      endif
      if(iop.lt.1.or.iop.gt.8) iop = 1
      if(tol.lt.0.d0) tol = -1*tol
      if(ni1.lt.0.0d0) ni1 = -1*ni1
      if(ni2.lt.0.0d0) ni2 = -1*ni2
      if(iline.le.0) iline = 1
      if(iop.eq.1) then
        if(op1.le.0.0d0 .or. op1.ge.1.d0) op1 = 1.
      else if(iop.eq.2) then
        if(op1.le.0.0d0 .or. op1.ge.1.d0) op1 = 0.5d0
        if(op2.le.0.0d0) op2 = 10.d0
        op3 = 0.0d0
      else if(iop.eq.3) then
        if(op1.le.0.0d0 .or. op1.ge.1.d0) op1 = 0.5d0
        if(op2.eq.0.0d0) op2 = 1.d-8
        if(op2.lt.0.0d0) op2 = dabs(op2)
        if(op3.le.0.0d0) op3 = 4.d0
      else if(iop.eq.4) then
        if(op1.le.0.0d0 .or. op1.ge.1.d0) op1 = 0.5d0
        if(op2.eq.0.d0) op2 = tol
        if(op2.lt.0.d0) op2 = dabs(op2)
        if(op3.le.0.d0) op3 = 4.d0
      else if(iop.eq.5) then
        if(op1.le.0.0d0 .or. op1.ge.1.0d0) op1 = 0.5d0
        if(op2.le.1.0d0) op2 = 1.1d0
        if(op3.le.0.d0 .or. op3.ge.1.0d0) op3 = 0.1d0
      else if(iop.eq.6) then
        if(op1.le.0.0d0 .or. op1.ge.1.0d0) op1 = 0.5d0
        if(op2.le.1.0d0) op2 = 0.5d0
      end if
      write(not,2040) iop,ni1,ni2,tol,op1,op2,op3
      if(iop.eq.4) write(not,2045) iline
      if(ntol.eq.2) write(not,2047) ntol
      go to 10

c---  read statistics of the basic variables x.

  150 lgp(1,1) = 1
      lgp(2,1) = 0
      lgp(3,1) = 0
      if(itime.eq.0) write(not,2050)
      itime = 1
      nn = 1
      i1 = 1
      do i = 1,nig
        call infree(4,iin,rin)
        igt(i) = iin(1)
        nge = iin(2)
        if(igt(i).eq.3) then
          ngm = iin(3)
          ngd = nge
        else if(igt(i).eq.4) then
          ngm = iin(4)
          ngc = iin(3)
          ngd = nge-ngm
        endif
        ngx = nge
        ngn = ngm
        write(not,2051) i,igt(i)
        do j = 1,ngx
          call inline(p,xx0,igt(i),iids,ip,xn,nv)
          igx(j) = nv
          do jj = 1,4
            par(jj,nv) = p(jj)
            if(ip(jj).eq.0) go to 152
            do kk = 1,j-1
              if(abs(ip(jj)).eq.igx(kk)) go to 152
            end do ! kk
            call cerror(16,ip(jj),0.d0,' ')
  152       ipa(jj,nv) = ip(jj)
          end do ! jj
          x0(nv) = xx0
          ids(nv) = iabs(iids)
          nds(j) = iids
          if(ids(nv).eq.0) then
            ixz(nv) = 0
            nge = nge-1
            if(igt(i).eq.3 .and. j.le.ngn) ngm = ngm-1
            if(igt(i).eq.4 .and. j.gt.ngd) ngm = ngm-1
          else
            ixz(nv) = i1
            izx(i1) = nv
            i1 = i1+1
          endif
          ex(nv) = p(1)
          sg(nv) = p(2)
          if(igt(i).lt.3) then
            call cparam(ex(nv),sg(nv),par(1,nv),iids)
          else
            call cparvl(x0,ex(nv),sg(nv),par(1,nv),bnd(1,nv),
     *               iids,ipa(1,nv),0)
          endif
          if(ids(nv).le.50) then
            ib(nv) = ibound(ids(nv))
            if(ids(nv).eq.4.or.ids(nv).eq.5) then
              bnd(1,nv) = par(2,nv)
            else if(ids(nv).eq.6) then
              bnd(1,nv) = par(1,nv)
              bnd(2,nv) = par(2,nv)
            else if(ids(nv).eq.7) then
              bnd(1,nv) = par(3,nv)
              if(par(3,nv).eq.0.d0 .and. par(4,nv).eq.0.d0 ) then
                par(4,nv) = 1.d0
              endif
              bnd(2,nv) = par(4,nv)
            end if
          end if
          if(igt(i).lt.3) then
            if(ids(nv).eq.7) then
              write(not,2052) xn(nv),ids(nv),ex(nv),sg(nv),
     *                       (par(k,nv),k = 1,4),x0(nv)
            else if(ids(nv).le.50) then
              write(not,2053) xn(nv),ids(nv),ex(nv),sg(nv),
     *                       (par(k,nv),k = 1,2),x0(nv)
            else
              write(not,2054) xn(nv),ids(nv),sg(nv),(par(k,nv),k = 1,4),
     *                        x0(nv)
            end if
          else
            out = ' '
            if(ids(nv).le.50) then
              write(out(1:10),2056) ex(nv)
              write(out(11:20),2056) sg(nv)
              do k = 1,2
                if(ip(k).gt.0) then
                  write(out(k*10-9:k*10),2055) ip(k)
                else if(ip(k).lt.0) then
                  write(out(k*10+11:k*10+20),2055) -ip(k)
                else
                  write(out(k*10+11:k*10+20),2056) par(k,nv)
                endif
              end do ! k
            else
              do k = 1,2
                if(ip(k).ne.0) then
                  write(out(k*10+11:k*10+20),2055) iabs(ip(k))
                else
                  write(out(k*10+11:k*10+20),2056) par(k,nv)
                endif
              end do ! k
            endif
            if(ids(nv).gt.50.or.ids(nv).eq.7) then
              do k = 3,4
                if(ip(k).ne.0) then
                  write(out(k*10+11:k*10+20),2055) iabs(ip(k))
                else
                  write(out(k*10+11:k*10+20),2056) par(k,nv)
                endif
              end do ! k
            endif
            write(not,2057) xn(nv),ids(nv),out,x0(nv)
          endif
        end do ! j
        if(i.lt.nig) lgp(1,i+1) = lgp(1,i)+nge
        if(igt(i).ne.4) then
          lgp(2,i) = lgp(1,i)+ngm-1
        else
          lgp(2,i) = lgp(1,i)+nge-ngm
          lgp(3,i) = lgp(1,i)+ngc-1
        endif

c---  read correlation matrix of x if x's are correlated.

        if(igt(i).eq.1) go to 157
        if(igt(i).ge.3) ngx = ngn
        if(ngx.ne.0) then
          call define('rox ',n2x,ngx,ngx)
          call inpro(ex,sg,par,tf(nn),nds,ids,
     *               izx(lgp(1,i)),ngx,nge,igt(i),igx,ia(n2x))
          call delete('rox ')
        endif
  157   nn = nn+nge*nge
      end do ! i
      ntf = nn-1
      nry = i1-1
      ims = 1
      go to 10

c---  input transition function parameters.

  160 if(ntp.eq.0) go to 10
      read(nin,*) (tp(i),i = 1,ntp)
      write(not,2060)
      do i = 1,ntp
        write(not,2061) i,tp(i)
      end do ! i
      ims = 1
      go to 10

c---  input transition function information.

  170 call infree(1,iin,rin)
      nesc = iin(1)
  171 call infree(2,iin,rin)
      ig = iin(1)
      if(ig.eq.0) go to 10
      n1 = nls+1
      nls = nls+iin(2)
      nele = max0(nele,iin(2))
      read(nin,*) (les(i),i = n1,nls)
      nes(1,ig) = n1
      nes(2,ig) = nls
      go to 171

c---  stop input.

  180 continue
      call inpmr(igfx,fltf,fltc,fltp,ims,im0)
      return

c---  output format.

 2010 format(1x,a)
 2020 format(
     & ' Type of system ..........................icl=',i7/
     & '   icl = 1 ...................................component'/
     & '   icl = 2 ...............................series system'/
     & '   icl = 3 ..............................general system'/
     & ' Flag for gradient computation ...........igr=',i7/
     & '   igr = 0 ...........................finite difference'/
     & '   igr = 1 ...................formulas provided by user')
 2040 format(/
     & ' Optimization scheme used ................iop=',i7/
     & '   iop = 1 ................................HL-RF method'/
     & '   iop = 2 .......................modified HL-RF method'/
     & '   iop = 3 ..................gradient projection method'/
     & '   iop = 4 .................sequential quadratic method'/
     & '   iop = 5 .......................improved HL-RF method'/
     & '   iop = 6 .............................Polak-He method'/
c    & '   iop = 5 .................augmented lagrangiam method'/
c    & '   iop = 6 .....................combined penalty method'/
     & ' Maximum number of iteration cycles ......ni1=',i7/
     & ' Maximum steps in line search ............ni2=',i7/
     & ' Convergence tolerance ...............tol=',1p,1e11.3/
     & ' Optimization parameter 1 ............op1=',1p,1e11.3/
     & ' Optimization parameter 2 ............op2=',1p,1e11.3/
     & ' Optimization parameter 3 ............op3=',1p,1e11.3)
2045  format(
     & ' Line search method used  ..............iline=',i7/
     & '   iline = 1..............................armijo method'/
     & '   iline = 2...................quadratic fitting method'/
     & '   iline = 3........................schittkowski method')
2047  format(
     & ' Convergence criterion employed .........ntol=',i7/
     & '   ntol = 0.............for convex limit state function'/
     & '   ntol = 2..........for nonconvex limit state function')

 2050 format(/' Statistical data of basic varibles:'/
     & ' Available probability distributions:'/
     & '   determinitic .............ids = 0'/
     & '   normal ...................ids = 1'/
     & '   lognormal ................ids = 2'/
     & '   gamma ....................ids = 3'/
     & '   shifted exponential ......ids = 4'/
     & '   shifted rayleigh .........ids = 5'/
     & '   uniform ..................ids = 6'/
     & '   beta .....................ids = 7'/
     & '   type i largest value .....ids =11'/
     & '   type i smallest value ....ids =12'/
     & '   type ii largest value ....ids =13'/
     & '   weibull ..................ids =14'/
     & '   user defined .............ids >50')
 2051 format(/' Group no.:',i5,10x,'Group type:',i5/
     & ' var   ids   mean    st. dev.   param1    param2    param3',
     & '    param4   init. pt')
 2052 format(1x,a4,i4,1p,7e10.2)
 2053 format(1x,a4,i4,1p,4e10.2,20x,e10.2)
 2054 format(1x,a4,i4,10x,1p,6e10.2)
 2055 format(i10)
 2056 format(2e10.2)
 2057 format(1x,a4,i4,a,1pe10.2)
 2060 format(/' Deterministic parameters in limit-state function:')
 2061 format('   tp (',i3,') = ',1p,1e11.3)
 2015 format(
     & ' Number of cut sets (cs) .................ncs=',i7/
     & ' Total number of components in all cs.....ntl=',i7)

      end subroutine input
