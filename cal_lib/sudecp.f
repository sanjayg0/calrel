      subroutine sudecp(a,n,ma,mb,mm)

      implicit   none 

      integer (kind=4) :: n,mm,ma,mb
      real    (kind=8) :: a(mm,1)
c
      integer (kind=4) :: i,j,k
      integer (kind=4) :: i1,ima,imb, j1,jma,jmb
      real    (kind=8) :: aij,aji
      real    (kind=8) :: error

c-----decomposition a=lu
c
      do 50 j=1,n
      j1=j-1
      jmb=max(j-mb,1)
      if(jmb.gt.j1) go to 25
      do 20 i=jmb,j1
      i1=i-1
      ima=max(i-ma,jmb)
      aij=0.d0
      if(ima.gt.i1) go to 20
      do 10 k=ima,i1
   10 aij=aij+a(i,k)*a(k,j)
   20 a(i,j)=(a(i,j)-aij)/a(i,i)
c
   25 jma=max(j-ma,1)
      do 40 i=jma,j
      i1=i-1
      imb=max(i-mb,jma)
      aji=0.d0
      if(imb.gt.i1) go to 40
      do 30 k=imb,i1
   30 aji=aji+a(j,k)*a(k,i)
   40 a(j,i)=a(j,i)-aji
      if(a(j,j)) 50,55,50
   50 continue
      return
c
   55 error=2.d0

      end subroutine sudecp
