      subroutine dbetai (x,a,b,p,ier)

      implicit   none

      integer (kind=4) :: ier
      real    (kind=8) :: x,a,b,p

      integer (kind=4) :: itmax
      real    (kind=8) :: eps, sig, zero, smexe

      integer (kind=4) :: ic, nc
      real    (kind=8) :: xl,xr,xrmxl, xc,xt, fn,fxl,fxr, fcs
      real    (kind=8) :: aa, afn, bb, c
      real    (kind=8) :: p1, q0,qx
      real    (kind=8) :: temp, dtemp
      real    (kind=8) :: zi,zz

      data             eps/.0001d0/,sig/1.d-5/,itmax/30/,zero/0.0d0/
      data             smexe/-170.0d0/

c                           first executable statement

      ier = 0
      xl = dmin1(a,b)
      if (xl.le.0.0d0) go to 70
      if (xl.le.1.0d0) go to 20
      xr = dmax1(a,b)
      if (10.0*xl.le.xr) go to 20
      ic = 0
      xl = 0.0
      xr = 1.0
      fxl = -p
      fxr = 1.0-p
      if (fxl*fxr.gt.zero) go to 65
c                           bisection method
    5 x = (xl+xr)*.5
      call dbeta (x,a,b,p1,ier)
      if (ier.ne.0) go to 20
      fcs = p1-p
      if (fcs*fxl.gt.zero) go to 10
      xr = x
      fxr = fcs
      go to 15
   10 xl = x
      fxl = fcs
   15 xrmxl = xr-xl
      if (xrmxl.le.sig.and.dabs(fcs).le.eps) go to 9005
      ic = ic+1
      if (ic.le.itmax) go to 5
c                           error returned from dbeta
c                           use newtons method for skewed cases
   20 if (p.le.0.0.or.p.ge.1.0) go to 65
      if (p.gt..5) go to 25
      aa = a
      bb = b
      q0 = dlog(p)
      go to 30
   25 q0 = dlog(1.0-p)
      aa = b
      bb = a
   30 xt = aa/(aa+bb)
      dtemp = log_gamma(aa+bb)-log_gamma(aa)-log_gamma(bb)
      dtemp = dtemp-(aa+bb)*dlog(aa+bb)+(aa-.5)*dlog(aa)+(bb-.5)
     1*dlog(bb)
      dtemp = dtemp+.5*dlog(bb/aa)+aa*dlog(1.0+bb/aa)+bb*dlog(1.+aa/bb)
      do 45 nc=1,100
       temp = dlog(15.0+aa+bb)
       fn = 0.7*temp*temp+dmax1(xt*(aa+bb)-aa,0.0d0)
       temp = aa+fn+fn
       afn = dint(fn)+1.0
       c = 1.0-(aa+bb)*xt/temp
       zi = 2.0/(c+dsqrt(c*c-4.0*fn*(fn-bb)*xt/(temp*temp)))
   35       afn = afn-1.0
       if (afn.lt..5) go to 40
       temp = aa+afn+afn
       zi = (temp-2.0)*(temp-1.0-afn*(afn-bb)*xt*zi/temp)
       temp = aa+afn-1.0
       zi = 1.0/(1.0-temp*(temp+bb)*xt/zi)
       go to 35
   40       zz = zi
       temp = dlog(xt)
       if (temp.le.smexe) go to 50
       qx = dtemp+aa*temp+bb*dlog(1.-xt)+dlog(zz)
       xc = (q0-qx)*(1.-xt)*zz/aa
       xc = dmax1(xc,-.99d0)
       temp = .5/xt-.5
       xc = dmin1(xc,temp)
       xt = xt*(1.+xc)
       if (dabs(xc).lt.sig) go to 55
   45 continue
      ier = 131
      go to 9000
   50 xt = 0.0
   55 if (p.gt..5) go to 60
      x = xt
      go to 9005
   60 x = 1.0-xt
      go to 9005
   65 ier = 129
      go to 9000
   70 ier = 130
 9000 continue
      call uertst (ier,'dbetai')
 9005 return

      end subroutine dbetai
