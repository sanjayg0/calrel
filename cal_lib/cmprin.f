cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cmprin(a,nr,nc,nrr,index)
c
c   functions:
c      print a nr*nc matrix.
c
c   input arguments:
c      a : a nr*nc matrix.
c      nr : number of rows to be printed.
c      nc : number of columns to be printed.
c      nrr : row number declared in the calling routine.
c      index : format code.
c              eq. 0, format : (10f7.2).
c              ne. 0, format : (8e14.6).
c
c   called by: cform, input.
c
c   last revision: jan.8 1990 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cmprin(igx,a,nr,nc,nrr,index,ity)

      implicit   none

      include   'file.h'

      integer (kind=4) :: nr,nc,nrr,index,ity
      real    (kind=8) :: a(nrr,nc)
      integer (kind=4) :: igx(nc)

      integer (kind=4) :: i,j,k,n, jh
      save


c---  print a in (10f7.2) format.
c
      if(index.eq.0) then
      do 10 j=1,nc,10
      jh=j+9
      if(jh.gt.nc)jh=nc
      if(ity.eq.0) then
      write(not,30)(igx(n),n=j,jh)
      else
      write(not,30)(n,n=j,jh)
      endif
      do 10 i=1,nr
      if(ity.eq.0) then
      write(not,40)igx(i),(a(i,k),k=j,jh)
      else
      write(not,40)i,(a(i,k),k=j,jh)
      endif
   10 continue
c
c---  print a in (7e11.3) format.
c
      else
      do 20 j=1,nc,7
      jh=j+6
      if(jh.gt.nc)jh=nc
      if(ity.eq.0) then
      write(not,50) (igx(n),n=j,jh)
      else
      write(not,50) (n,n=j,jh)
      endif
      do 20 i=1,nr
      if(ity.eq.0) then
      write(not,60)igx(i),(a(i,k),k=j,jh)
      else
      write(not,60)i,(a(i,k),k=j,jh)
      endif
   20 continue
      endif
   30 format(2x,10i7)
   40 format(i5,10f7.2)
   50 format(i9,6i11)
   60 format(i3,1p,7e11.3)

      end subroutine cmprin
