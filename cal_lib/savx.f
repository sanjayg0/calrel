      subroutine savx(x,u,ndm,ndf,numnp)

      implicit   none

      integer (kind=4) :: ndm,ndf,numnp
      real    (kind=8) :: x(ndm,numnp),u(ndf,numnp)

      integer (kind=4) :: i,j

      write(21,'('' NEW PROBLEM'')')

      do i = 1,numnp
        write(21,20) i,(x(j,i),j=1,ndm),(u(j,i),j=1,ndf),
     &                 (x(j,i)+u(j,i),j=1,ndm)
      end do ! i

   20 format(i5,1p,6e12.4)

      end
