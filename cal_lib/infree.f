cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine infree(nit,ii,rr)
c
c   functions:
c      read free-format input data, at most 20 items per line.
c
c   input arguments:
c      nit : number of input items.
c
c   output arguments:
c      ii : integer input data.
c      rr : real input data.
c
c   calls: isep, cerror.
c
c   called by: input.
c
c   last revision: july 29 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine infree(nit,ii,rr)

      implicit   none

      include   'file.h'

      character (len=80) ::  a
      character (len= 1) ::  sep

      integer (kind=4) :: nit
      integer (kind=4) :: ii(nit)
      real    (kind=8) :: rr(nit)

      logical          :: fs
      integer (kind=4) :: i, is,it, ie

      save

      sep=','
      do i=1,nit
        ii(i)=0
        rr(i)=0.d0
      end do ! i
      read(nin,'(a)',end=60) a
      is=1
      do it=1,nit
        call isep(a,is,ie,sep)
        if(is.gt.ie) go to 40
        fs=.true.
        do i=is,ie
          if(a(i:i).eq.'.') fs=.false.
        end do ! i
        read(a(is:ie),'(f80.0)') rr(it)
        if(fs) read(a(is:ie),'(i80)') ii(it)
   40   is=max0(is,ie)+1
        if(is.gt.80) return
      end do ! it
      return

   60 call cerror(12,0,0.,' ')

      end subroutine infree
