c   routine name   - gguw
c
c-----------------------------------------------------------------------
c
c   computer            - dec11/single
c
c   latest revision     - june 1, 1981
c
c   purpose             - uniform (0,1) random number generator with
c                           shuffling
c
c   usage               - call gguw (dseed,nr,iopt,r)
c
c   arguments    dseed  - input/output double precision variable
c                           assigned an integer value in the
c                           exclusive range (1.d0, 2147483647.d0).
c                           dseed is replaced by a new value to be
c                           used in a subsequent call.
c                nr     - input.  number of deviates to be generated
c                           on this call.
c                iopt   - input.  option switch.
c                           on initial entry, iopt must be one.
c                           on subsequent calls, iopt must be zero.
c                r      - output vector of length nr containing the
c                           shuffled floating point (0,1) deviates.
c
c   precision/hardware  - single/all
c
c   reqd. routines      - none
c
c-----------------------------------------------------------------------
      subroutine gguw   (dseed,nr,iopt,r)
c                                  specifications for arguments
      implicit   none

      integer (kind=4) :: nr,iopt
      real    (kind=8) :: r(nr)
      real    (kind=8) :: dseed
c                                  specifications for local variables
      integer (kind=4) :: i,j
      real    (kind=8) :: wk(128)
      real    (kind=8) :: d2p31m,d2p31

      real    (kind=8) :: x
!     real    (kind=8) :: dmod
c                                  d2p31m=(2**31) - 1
c                                  d2p31 =(2**31)(or an adjusted value)
      data               wk/128*0.0d0/
      data               d2p31m/2147483647.d0/
      data               d2p31 /2147483648.d0/
c     data               d2p31 /2147483711.d0/
c                                  initial entry - fill table
c                                  first executable statement
      if (iopt .ne. 1)   go to 10
      do 5  i = 1,128
!        dseed= dmod( 16807.d0*dseed,d2p31m )
         dseed= mod( 16807.d0*dseed,d2p31m )
    5 wk(i)   = dseed / d2p31
   10 continue
      do 15 i = 1,nr
c                                  generate a new uniform (0,1) deviate
c                                  16807 = (7**5)
!        dseed= dmod(16807.d0*dseed,d2p31m)
         dseed= mod(16807.d0*dseed,d2p31m)
c                                  now shuffle
!        j    = dmod(dseed,128.d0)+1.d0
         j    = int(mod(dseed,128.d0))+1 ! WIP truncate not round
         x    = dseed / d2p31
         r(i) = wk(j)
         wk(j)= x
   15 continue

      end subroutine gguw
