ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine fnewy5(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
c                        ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig)
c
c   functions:
c      compute new x and y using improved rf method.
c      the descent function des = y*y/2 + op2*|gx| .
c      use armijo line search method.
c
c   input arguments:
c      x : current x.
c      y : current y.
c      a : unit normal vector in the y space.
c      d : direction of search.
c      yt : temporary y.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      ex : means of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      tp : deterministic parameters in performance function.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      sdgy : |dg/dy|.
c      iter : iteration number.
c      gx : g(x).
c      ig : failure mode number.
c
c   output arguments:
c      x : new x.
c      y : new y.
c      dxz : dx/dz.
c      gam : unit normal vector in the z space.
c      bt : new reliability index.
c
c   calls: ugfun, cdot, cytox, fdirct.
c
c   called by: form.
c
c   last revision: april 6, 1995 by c.-c. li
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine fnewy5(x,y,a,d,yt,dxz,gam,tf,ex,sg,par,bnd,tp,igt,lgp,
     *            ids,ipa,ib,izx,ixz,sdgy,bt,iter,gx,ig,gdgx)
      implicit   none

      include   'flag.h'
      include   'grad.h'
      include   'prob.h'
      include   'optm.h'

      integer (kind=4) :: iter,ig
      real    (kind=8) :: sdgy,bt,gx
      integer (kind=4) :: ids(nrx),ib(nrx),igt(nig),lgp(3,nig)
      integer (kind=4) :: izx(nry),ixz(nrx),ipa(4,nrx)
      real    (kind=8) :: x(nrx),y(nry)
      real    (kind=8) :: a(nry),d(nry),yt(nry),dxz(nrx),gam(nry)
      real    (kind=8) :: tf(ntf),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx),tp(*)
      real    (kind=8) :: gdgx(nrx)

      integer (kind=4) :: i,iii
      real    (kind=8) :: a1,a2,a3,alpha
      real    (kind=8) :: bound
      real    (kind=8) :: cpara
      real    (kind=8) :: des,des1,des2
      real    (kind=8) :: g0,gsign 
      real    (kind=8) :: s
      real    (kind=8) :: t,tmp1,tmp2,tmp3 

      real    (kind=8) :: cdot

      save

c---  des = y*y/2 + op2*|gx|.

      flgf=.true.
      flgr=.false.
      if(igr.ne.0) flgr=.true.
c     call cytox(x,y,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
c     call ugfun(gx,x,tp,ig)
c     call fdirct(x,y,tp,ex,sg,dxz,gam,a,tf,par,bnd,igt,lgp,ids,
c    &            ipa,izx,ixz,sdgy,gdgx,ig,0)
      tmp1=cdot(y,y,1,1,nry)
      if(dabs(sdgy).eq.0.0d0) then
        print *,'warning norm of gradg is zero'
        stop
      endif
      s=gx/sdgy+cdot(y,a,1,1,nry)
      do i = 1,nry
         d(i) = s*a(i) - y(i)
      end do ! i
      if(dabs(gx).gt.1d-3) then
        cpara = s**2/dabs(gx)*0.5d0
        cpara = dabs(op2)*max(dsqrt(tmp1)/dabs(sdgy),cpara)
       else
        cpara=dabs(op2)*dsqrt(tmp1)/dabs(sdgy)
      endif
c     find a*<grad(m),d>
      gsign=dsign(1.0d0,gx)
      if(gx.eq.0.0d0) gsign=0.0d0
      bound=cdot(a,d,1,1,nry)
      bound=cdot(y,d,1,1,nry)-cpara*gsign*sdgy*cdot(a,d,1,1,nry)
      bound=dabs(op3)*bound
      if(bound.gt.0.0d0) then
        print *,'warning bound is greater than zero in fnewy5'
     &    , bound
      endif
      g0    = gx
      alpha = 1.0d0
      a3    = cdot(y,a,1,1,nry)
c     ncount=ncount+1
c----------------------------------------------------------------------
c     perform armijo line search.
c----------------------------------------------------------------------
c---  find a new y along the search direction.

      tmp2=cdot(d,d,1,1,nry)
      tmp3=cdot(d,y,1,1,nry)
      do 60,iii = 1,ni2
         a1 = (1.0d0-alpha)
         a2 = alpha*s
         do i=1,nry
           yt(i) = (y(i) + alpha*s*a(i)) - alpha*y(i)
         end do ! i

c---     compute new x and gx

         call cytox(x,yt,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
         call ugfun(gx,x,tp,ig)
c        call cxtoy(x,yt,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,izx,bt)
c
c---     compute new des, i.e. new value of the descent function.
c
         des1=(tmp3+alpha*tmp2/2)*alpha
         des2=cpara*(dabs(gx)-dabs(g0))
         des=des1+des2
c        if(iii.gt.10) then
c        print *,'iteration ',iii,gx,g0
c        endif
c
c---     if des - des0 <= bound*b^k  exits
c---     if des-des0 > bound*b^k repeat line search procedures.
c
         if(des .le. bound*alpha) go to 90
         alpha = alpha*dabs(op1)
60    continue
      alpha=alpha/dabs(op1)
      print *,'Warning: Number of iteration in Armijo line search',
     &        ' exceeds ni2 =',ni2
c     print '(a,i4,3e15.8)', 'length',iter,dsqrt(tmp1),dsqrt(tmp2),
c    &                    tmp3/dsqrt(tmp1*tmp2)
      stop
90    do i = 1,nry
         y(i)=yt(i)
      end do ! i
c     print *,'step in fnewy5',iii
c     if(int(iter/1000)*1000.eq.iter) then
c     print '(a,i4,3e15.8)', 'length',iii,dsqrt(tmp1),dsqrt(tmp2),
c    &                    tmp3/dsqrt(tmp1*tmp2)
c     endif
      call cytox(x,y,dxz,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call ugfun(gx,x,tp,ig)
      call fdirct(x,y,tp,ex,sg,dxz,gam,a,tf,par,bnd,igt,lgp,ids,
     &            ipa,izx,ixz,sdgy,gdgx,ig,0)
      tmp1=cdot(y,y,1,1,nry)
      bt=dsqrt(tmp1)
      t=cdot(a,y,1,1,nry)
      if(t.lt.0.d0) bt=-bt

      end subroutine fnewy5
