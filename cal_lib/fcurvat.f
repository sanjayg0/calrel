!     ********************************************************************
!     subroutine fcurvat
!
!     written by mario de stefano      august-october 1990
!     cur = curvature
!     ********************************************************************
      subroutine fcurvat(yo,ao,y,a,cur,ig4,tp,ncur)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: ig4,ncur 
      real    (kind=8) :: cur
      real    (kind=8) :: yo(nry),ao(nry),y(nry),a(nry),tp(*)

      integer (kind=4) :: i,j
      real    (kind=8) :: ang
      real    (kind=8) :: dl,dl2,dus,du
      real    (kind=8) :: sum, usum

      ncur = 0
      dl  = 0.d0
      dl2 = 0.d0
      dus = 0.d0
      du  = 0.d0
      do i = 1,nry
        du = yo(i)-y(i)
        dus = du*du
        dl2 = dl2+dus
      end do ! i
      dl = sqrt(dl2)
      usum = 0.d0
      do j = 1,nry
        usum = usum+ao(j)*a(j)
      end do ! j
      if (abs(usum).ge.1.d0) then
        ncur = 1
        return
      else
        ncur = 0
      endif
      ang = acos(usum)
      cur = ang/dl
      usum = 0.d0
      do i = 1,nry
        usum = usum+ao(i)*(y(i)-yo(i))
      end do ! i
      if (usum.lt.0.d0) cur = -abs(cur)

      end subroutine fcurvat
