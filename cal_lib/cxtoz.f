ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cxtoz(x,z,p,sg,ids,k)

c   functions:
c      transform x to a standard normal variate.

c   input arguments:
c      x : basic random variables.
c      ids : distribution number of x.
c      p : distribution parameters of x.
c      k : no. of the z to be calculated.

c   output arguments:
c      z : the standard normal variate to be calculated.
c      sg : standard deviation of x.

c   calls: dbeta, dnormi, udd.

c   called by: cxtoy.

c   last revision: july 27 1987 by p.-l. liu.

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cxtoz(x,z,p,sg,ids,k)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: ids, k
      real    (kind=8) :: z, sg 
      real    (kind=8) :: x(nrx),p(4),bnd(2)

      integer (kind=4) :: ib, ier
      real    (kind=8) :: pd, pp, prob
      real    (kind=8) :: ba, dx, xx, xt,xt1

      save

      xx = x(k)

c---  compute z or fx(x).

      if(ids.ge.51) go to 51
      go to (1,2,3,4,5,6,7,70,70,70,11,12,13,14),ids
      write(*,*) ' CXTOZ: DEFAULT TO NORMAL??'
c---  normal
    1 z = (xx-p(1))/p(2)
      return
c---  lognormal
    2 z = (log(xx)-p(1))/p(2)
      return
c---  gamma
    3 xt = p(1)*xx
      call mdgam(xt,p(2),prob,ier)
      go to 60
c---  exponential
    4 xt = exp(-p(1)*(xx-p(2)))
      prob = 1.0-xt
      go to 60
c---  rayleigh
    5 dx = (xx-p(2))/p(1)
      xt = exp(-dx*dx/2.)
      prob = 1.0-xt
      go to 60
c---  uniform
    6 ba = p(2)-p(1)
      prob = (xx-p(1))/ba
      go to 60
c---  beta
    7 ba = p(4)-p(3)
      xt = (xx-p(3))/ba
      call dbeta(xt,p(1),p(2),prob,ier)
      go to 60
c---  student
c   8 call mstud(xx,p(1),p(2),p(3),prob)
c     go to 60
c---  type i largest
   11 xt = -p(2)*(xx-p(1))
      xt = -exp(xt)
      prob = exp(xt)
      go to 60
c---  type i smallest
   12 xt = p(2)*(xx-p(1))
      xt = -exp(xt)
      pp = exp(xt)
      prob = 1.d0-pp
      go to 60
c---  type ii largest
   13 xt = p(1)/xx
      xt1 = xt**p(2)
      prob = exp(-xt1)
      go to 60
c---  type iii smallest
   14 xt = xx/p(1)
      xt1 = xt**p(2)
      prob = 1.0-exp(-xt1)
      go to 60
c---  user-defined distribution
   51 call udd(x,p,sg,ids,prob,pd,bnd,ib)

c---  compute z such that phi(z) = fx(x)

c     print *,'prob = ',prob
   60 call dnormi(z,prob,ier)
c     print *,'z = ',z
   70 ier = 1

      end subroutine cxtoz
