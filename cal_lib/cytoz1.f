cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c      subroutine cytoz1(tf,y,z,nge,nges,ngec,ind)
c
c   functions:
c      transform y in a type 4 group to z.
c
c   input arguments:
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      y : basic variables in the standard space.
c      nge : number of variables in this group.
c      nges : number of variables to be transformed.
c
c   output arguments:
c      z : basic variables in the correlated normal space.
c
c   called by: cytox.
c
c   last revision: june 16 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cytoz1(tf,y,z,nge,nges,ngec,ind)

      implicit   none

      integer (kind=4) :: nge, nges, ngec, ind
      real    (kind=8) :: tf(nge,nge),y(nge),z(nge)

      integer (kind=4) :: i,j, j0, j1s
      integer (kind=4) :: n1,  n2

      j0=nge-nges
      j1s=j0+1
      if(ind.eq.1) then
      z(j1s)=y(j1s)/tf(j1s,j1s)
      n1=j1s+1
      n2=ngec
      else
      n1=ngec+1
      n2=nge
      endif
      if(nges.eq.1) return
      if(n1.lt.n2) return
      do 20 i=n1,n2
      z(i)=y(i)
      do 10 j=j1s,i-1
   10 z(i)=z(i)-tf(i,j)*z(j)
   20 z(i)=z(i)/tf(i,i)
      return
      end subroutine cytoz1
