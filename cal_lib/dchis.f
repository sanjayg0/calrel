      subroutine dchis (cs,df,p,ier)

      implicit   none
!                           specifications for arguments

      integer (kind=4) :: ier
      real    (kind=8) :: cs,df,p

!                           specifications for local variables

      integer (kind=4) :: i
      real    (kind=8) :: pt2
      real    (kind=8) :: a,z,dgam,eps,w,w1,b,z1,half,one,thrten,thrd
      real    (kind=8) :: func
      real    (kind=8) :: rinfm,x

      data             eps/1.0d-6/,half/5.d-1/,thrten/13.d0/,one/1.d0/
      data             thrd/.3333333333333333d0/
      data             pt2/.2222222d0/
      data             rinfm/-1.7d+38/

      func(w,a,z) = w*exp(a*log(z)-z)

!                           first executable statement
!                           test for invalid input values

      if (df.ge.0.5d0 .and. df.le.2.d5 .and. cs.ge.0.0d0) go to 5
      ier = 129
      p = rinfm
      go to 9000
    5 ier = 0
!                           set p = 0. if cs is less than or
!                           equal to 10.**(-12)
      if (cs .gt. 1.d-12) go to 15
   10 p = 0.0d0
      go to 9005
   15 if(df.le.66.d0) go to 20
!                           use normal distribution approximation
!                           for large degrees of freedom
      if(cs.lt.2.0) go to 10
      x = ((cs/df)**thrd-(one-pt2/df))/sqrt(pt2/df)
      if (x .gt. 5.0d0) go to 50
      if (x .lt. -18.8055d0) go to 55
      call dnorm (x,p)
      go to 9005
!                           initialization for calculation using
!                           incomplete gamma function
   20 if (cs .gt. 200.d0) go to 50
      a = half*df
      z = half*cs
      dgam  =  gamma(a)
      w = max(half*a,thrten)
      if (z .ge. w) go to 35
      if (df .gt. 25.d0 .and. cs .lt. 2.d0) go to 10
!                           calculate using equation no. 6.5.29
      w = one/(dgam*a)
      w1 = w
       do 25 i = 1,50
       b = i
       w1 = w1*z/(a+b)
       if (w1 .le. eps*w) go to 30
       w = w+w1
   25       continue
   30 p = func(w,a,z)
      go to 9005
!                           calculate using equation no. 6.5.32
   35 z1 = one/z
      b = a-one
      w1 = b*z1
      w = one+w1
       do 40 i = 2,50
       b = b-one
       w1 = w1*b*z1
       if (w1 .le. eps*w) go to 45
       w = w+w1
   40       continue
   45 w = z1*func(w,a,z)
      p = one-w/dgam
      go to 9005
   50 p = 1.0d0
      go to 9005
!                           warning error - underflow would have
!                           occurred
   55 p = 0.0
      ier = 34
 9000 continue
      call uertst (ier,'dchis  ')
 9005 return

      end subroutine dchis
