cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine itraro(ex,sg,par,ro,ids,izx,nge,nges)
c
c   functions:
c      transform the correlation matrix from x space to z space.
c
c   input arguments:
c      ex : mean vector of x.
c      sg : standard deviations of x.
c      par : distribution parameters of x.
c      ro : correlation matrix of x.
c      ids : distribution numbers of x.
c      izx : correspondence indicator for z -> x.
c      nge : dimension of ro.
c      nges : size of marginally distributed subgroup.
c
c   output arguments:
c      ro : correlation matrix of z.
c
c   calls: itro.
c
c   called by: input.
c
c   last revision: july 24 1987 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine itraro(ex,sg,par,ro,ids,izx,nge,nges)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: nge,nges
      real    (kind=8) :: ex(nrx),sg(nrx),par(4,nrx),ro(nge,nge)
      integer (kind=4) :: ids(nrx),izx(nge)

      integer (kind=4) :: i,j,k,l
      real    (kind=8) :: ba, cvi,cvj
      real    (kind=8) :: r
      real    (kind=8) :: u1,u2, s1,s2
      save
c
c---  compute the coefficient of variations of x.
c---  for beta distributions, two extra parameters are computed.
c
      do 200 i=1,nges-1
      k=izx(i)
      if(ex(k).eq.0.0d0) then
         cvi=0.0d0
      else
         cvi=sg(k)/ex(k)
      end if
      if(ids(k).eq.7) then
         ba=par(4,k)-par(3,k)
         u1=(ex(k)-par(3,k))/ba
         s1=sg(k)/ba
      end if
      do 100 j=i+1,nges
      l=izx(j)
      if(ro(i,j).eq.0.0d0) go to 100
      if(ex(l).eq.0.) then
         cvj=0.0d0
      else
         cvj=sg(l)/ex(l)
      end if
      if(ids(l).eq.7) then
         ba=par(4,l)-par(3,l)
         u2=(ex(l)-par(3,l))/ba
         s2=sg(l)/ba
      end if
      r=ro(i,j)
c
c---  perform transformation for each pair of x's.
      call itro(r,ids(k),ids(l),cvi,cvj,u1,s1,u2,s2)
      ro(i,j)=r
      ro(j,i)=r
  100 continue
      ro(i,i)=1.d0
  200 continue

      end subroutine itraro
