      subroutine pstop

      implicit   none

      include   'bdata.h'
      include   'cdata.h'
      include   'conval.h'
      include   'crel.h'
      include   'ddata.h'
      include   'file.h'
      include   'jjcf.h'
      include   'mdat4.h'

!     OLD FEAP COMMON BLOCK NAMES

      real    (kind=8) :: dm 
      integer (kind=4) ::    n,ma,mct,iel,nel
      common  /eldata/    dm,n,ma,mct,iel,nel
     
      real    (kind=8) ::  eerror,eproj,efem
      common  /errind/     eerror,eproj,efem

      logical      :: fl    ,pfr,folw
      common  /fdata/ fl(11),pfr,folw

      integer (kind=4) :: nh1,nh2
      common  /hdata/     nh1,nh2

      character        :: lct*4
      integer (kind=4) ::          jct     ,ll
      common  /jdata/     lct(100),jct(100),ll

      integer (kind=4) :: l,lv,lvs   ,lve
      common  /ldata/     l,lv,lvs(9),lve(9)

      integer (kind=4) :: n11a,n11b,ia      ,n14,n15
      common  /mdat2/     n11a,n11b,ia(2,26),n14,n15

      integer (kind=4) :: n16,n17,n18,n19,n20
      common  /mdat3/     n16,n17,n18,n19,n20
      integer (kind=4) :: n21,n22,n23,n24,n25,n26,n27,n28
      common  /mdat3/     n21,n22,n23,n24,n25,n26,n27,n28

      integer (kind=4) :: nn,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9
      common  /mdata/     nn,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9
      integer (kind=4) :: n10,n11,n12,n13,m1
      common  /mdata/     n10,n11,n12,n13,m1

      integer (kind=4) :: na,nal,nau,nc,nl,nm,nv,nw
      common /ndata/      na,nal,nau,nc,nl,nm,nv,nw

      logical      :: pfl
      common /pcent/  pfl

      integer (kind=4) :: iclear,idev,iopl
      common /pdata2/     iclear,idev,iopl

      logical          :: plfl
      common /pdata3/     plfl

      integer (kind=4) :: kmax
      common /plong/      kmax

      integer (kind=4) :: np
      common /plstrs/     np

      logical          :: prt
      common /print/      prt

      real    (kind=8) ::  prop,a
      integer (kind=4) ::              iexp    ,ik    ,npld
      common /prlod/      prop,a(6,10),iexp(10),ik(10),npld

      integer (kind=4) :: max
      common /psize/      max

      real    (kind=8) :: tol,rnmax,shift
      common /rdata/      tol,rnmax,shift

      integer (kind=4) :: ndf,ndm,nen1,nst
      common /sdata/      ndf,ndm,nen1,nst

      integer (kind=4) :: nums,iels      ,inods    ,ivals
      logical          ::                                     fol
      common /sldata/     nums,iels(4,26),inods(26),ivals(26),fol(26)

      integer (kind=4) :: istv
      common /strnum/     istv

      integer (kind=4) :: md,mv,mf
      common /subdt/      md,mv,mf

      real    (kind=8) :: time,dt,c1,c2,c3,c4,c5
      common /tdata/      time,dt,c1,c2,c3,c4,c5

      integer (kind=4) :: ifile,jfile
!     N.B. Just defined not set anywhere

      close(ifile)
      close(jfile)
      open(jfile,file='feap.res',form='unformatted',status='unknown')
      rewind(jfile)
      write(jfile) o,head,numnp,numel,nummat,nen,neq,ipr,vvv,ifeap,
     *             theta,nrk,nrc,nrm,nrt,nop,dm,n,ma,mct,iel,nel,
     *             eerror,eproj,efem,fl,pfr,nh1,nh2,lct,jct,ll
      write(jfile) nmnc,ngnc,ntdc,nmnf,ngnf,ntdf,ntdm,nrvm,nrvf,nrvc,
     *             nrvb,l,lv,lvs,lve,nes0,les0,nsts,ndeb,ndeu,nesc,
     *             nele,ndrb,ndxc,n11a,n11b,ia,
     *             n14,n15,n16,n17,n18,n19,n20,n21,n22,n23,n24,n25,n26,
     *             n27,n28,nn,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
      write(jfile) n13,m1,na,nal,nau,nc,nl,nm,nv,nw,pfl,iclear,idev,
     *             iopl,plfl,kmax,np,prt,prop,a,iexp,ik,
     *             npld,max,tol,rnmax,shift,ndf,ndm,nen1,nst,
     *             nums,iels,inods,ivals,fol,istv,
     *             md,mv,mf,time,dt,c1,c2,c3,c4,c5
      close(jfile)

      end
