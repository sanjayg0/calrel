cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cytox(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
c
c   functions:
c      transform y to x.
c
c   input arguments:
c      y : basic variables in the standard space.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      par : distribution parameters of x.
c      bnd : lower and upper bound of x.
c      ids : distribution numbers of x.
c      ib : code of distribution bounds of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      izx : a vector associated with z indicating the no. of the
c            corresponding x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element = 0 means parameter n is a constant.
c
c   output arguments:
c      x : basic variables in the original space.
c      z : current z.
c      ex : current mean of x.
c      sg : current standard deviation of x.
c
c   calls: cytoz, cztox.
c
c   called by: fnewy1, fnewy2, fnewy3.
c
c   last revision: june 15 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cytox(x,y,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)

      implicit   none

      include   'prob.h'

      real    (kind=8) :: x(nrx),y(nry),z(nry),tf(*),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx)
      integer (kind=4) :: igt(nig),lgp(3,nig)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),ib(nrx),izx(nry)

      integer (kind=4) :: i,ii, j0,j1, k
      integer (kind=4) :: nn, nge, nges

      save
c
c---  loop over groups
c
      nn=1
      do 60 i=1,nig
        j0=lgp(1,i)
        j1=lgp(1,i+1)-1
        if(i.eq.nig) j1=nry
        nge=j1-j0+1
        if(nge.le.0) go to 60
c---  for type 1 groups, transform y -> x directly.
        if(igt(i).eq.1) then
          do 10 ii=j0,j1
          k=izx(ii)
          call cztox(y(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
     *               ipa(1,k),ib(k),k,igt(i))
 10       continue
c---  for type 2 groups, first transform y -> z, then transform z -> x.
        else if(igt(i).eq.2) then
          call cytoz(tf(nn),y(j0),z(j0),nge,nge)
          do 20 ii=j0,j1
          k=izx(ii)
          call cztox(z(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
     *               ipa(1,k),ib(k),k,igt(i))
  20      continue
c---  for type 3 groups, compute dist. parameters, then transform y -> x
        else if(igt(i).eq.3) then
          nges=lgp(2,i)-j0+1
          if(lgp(2,i).lt.j0) go to 40
          call cytoz(tf(nn),y(j0),z(j0),nge,nges)
          do 30 ii=j0,lgp(2,i)
          k=izx(ii)
          call cztox(z(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
     *               ipa(1,k),ib(k),k,igt(i))
   30     continue
   40     if(lgp(2,i)+1.gt.j1) go to 165
          do 50 ii=lgp(2,i)+1,j1
          k=izx(ii)
          call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),
     *           ids(k),ipa(1,k),1)
          call cztox(y(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
     *               ipa(1,k),ib(k),k,igt(i))
  50      continue
c---  for type 4 groups, compute dist. parameters, then transform y -> x
c       else
c         j1s=lgp(2,i)
c         j0s=lgp(3,i)
c         nges=j1-j1s+1
c         ngec=j0s-j0+1
c         if(j1s.le.j0) go to 145
c         do 130 ii=j0,j1s-1
c         k=izx(ii)
c         call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k)
c    *             ,ids(k),ipa(1,k),1)
c 130    call cztox(y(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
c    *               ipa(1,k),ib(k),k,igt(i))
c 145    if(j0s.lt.j1s) go to 155
c         call cytoz1(tf(nn),y(j0),z(j0),nge,nges,ngec,1)
c         do 140 ii=j1s,j0s
c         k=izx(ii)
c         call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),
c    *        ids(k),ipa(1,k),1)
c 140    call cztox(z(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
c    *               ipa(1,k),ib(k),k,igt(i))
c 155    if(j1.le.j0s) go to 165
c         call cytoz1(tf(nn),y(j0),z(j0),nge,nges,ngec,2)
c----    for marginal distribution
c         do 150 ii=j0s+1,nge
c         k=izx(ii)
c 150    call cztox(z(ii),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
c    *               ipa(1,k),ib(k),k,igt(i))
        endif
  165  nn=nn+nge*nge
   60 continue

      end subroutine cytox
