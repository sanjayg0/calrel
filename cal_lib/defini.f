cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine defini(name,na,nr,nc)
c
c   functions:
c      define and reserve storage for an integer array.
c
c   input arguments:
c      name : name of the array.
c      nr : number of rows.
c      nc : number of columns.
c
c   output arguments:
c      na : initial location of array.
c
c   calls: defin.
c
c   called by: cinpu, cfosm, csosm, cboun, cdir1, cdir2, csens, cmont.
c
c   last revision: by wilson
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine defini(name,na,nr,nc)

      implicit   none

      include   'blkrel1.h'

      character (len=1) :: name(4)
      integer (kind=4) :: na,nr,nc

      np = 1
      call defin(name,na,nr,nc)

      end subroutine defini
