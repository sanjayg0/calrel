cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine sopf(tf,sg,ex,par,bnd,ib,tp,beta,x,ids,alph,srt
c    *              ,cur,y01,z,c,igt,lgp,ipa,izx,fltf,btg1,xlin,itg)
c
c   functions:
c     compute diagonal curvatures and second order failure prob. by the
c     point fitting method in the system reliability problem.
c
c   input arguments:
c     tf  : the transformation matrix from z to y, y=tf * z.
c     sg  : standard deviations of x.
c     bnd : lower and upper bound of x.
c     ib  : code of distribution bounds of x.
c     par : distribution parameters of x.
c     tp  : deterministic parameters.
c     beta: reliability indexes of all modes.
c     xlin : design points in the x space.
c     ids: distribution number of x.
c     alph:unit normal vectors in t4e y space.
c     igt : group types.
c     lgp : location of the first element of a group in y.
c     izx : a vector associated with z indicating the no. of the
c           corresponding x
c     ipa : #'s of the associated rv of distribution parameters.
c           n-th element = 0 means parameter n is a constant.
c
c   optput arguments:
c     cur : curvatures for each semiparobolic intersection.
c     srt : rotational transformation matrix between y and y' space.
c
c   calls: sopera,sphbre,sptved,dnormi.
c
c   called by: main.
c
c   last revision: aug. 20, 1987 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine sopf(tf,sg,ex,par,bnd,ib,tp,beta,x,ids,alph,srt,
     *           cur,y01,z,c,igt,lgp,ipa,izx,fltf,btg1,xlin,gtor,
     *           fltp,igfx,itg,inp)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

c     real tarray(2)
      logical          :: fltf(ngf),fltp(ngf)

      integer (kind=4) :: itg, inp
      integer (kind=4) :: ib(nrx),ids(nrx)
      integer (kind=4) :: igt(nig),lgp(3,nig),ipa(4,nrx)
      integer (kind=4) :: izx(nry),igfx(ngf,3)
      real    (kind=8) :: tf(ntf),sg(nrx),ex(nrx),par(4,nrx),bnd(2,nrx)
      real    (kind=8) :: tp(*),beta(ngf),x(nrx),xlin(nrx,ngf)
      real    (kind=8) :: alph(nry,ngf),srt(nry,nry*ngf),y01(nry),z(nry)
      real    (kind=8) :: c(nry),cur(2,nry*ngf-ngf)
      real    (kind=8) :: btg1(ngf),gtor(ngf)

      integer (kind=4) :: i0, ier, ig2,ig4, igk
      integer (kind=4) :: nn
      real    (kind=8) :: bta1,bta2
      real    (kind=8) :: pf, pf1,pf2,pf3,pf4 
      save

c-----compute the diagonal curvature matrix at design point
      nn=nry-1
      if(icl.eq.1) call finim(igfx,igf)
      do 30 igk=1,ngfc
      ig2=igfx(igk,2)
      i0=ig2-1
      if(.not.fltf(ig2)) call cerror(5,0,0.,' ')
            if(fltp(ig2)) go to 30
      fltp(ig2)=.true.
      ig4=igfx(igk,3)
      write(not,2010) ig4
      write(not,2020)
      if(abs(beta(ig2)).gt.8.) then
        call dnorm(-beta(ig2),pf)
        write(not,2030)
        if(beta(ig2).le.-1.5) then
        write(not,2045) beta(ig2),beta(ig2),1.d0-pf,1.d0-pf
        else
        write(not,2040) beta(ig2),beta(ig2),pf,pf
        endif
        go to 20
      endif
      call sopera(c,srt(1,nry*i0+1),alph(1,ig2),y01,z,x,tf,
     *          ids,par,tp,beta(ig2),bnd,sg,ex,ib,igt,lgp,ipa,izx,
     *          cur(1,nn*i0+1),ig4,xlin(1,ig2),gtor(ig2),inp)
c-----print result
      write(not,2030)
      call sphbre(nn,c,beta(ig2),pf1)
      pf3=1.d0-pf1
      call dnormi(bta1,pf3,ier)
      if(itg.ne.2) go to 10
      call sptves(nn,cur(1,nn*i0+1),beta(ig2),pf2)
      pf4=1.d0-pf2
      call dnormi(bta2,pf4,ier)
      if(bta1.le.-1.5.or.bta2.le.-1.5) then
      write(not,2045) bta1,bta2,pf3,pf4
      else
      write(not,2040) bta1,bta2,pf1,pf2
      endif
      go to 20
   10 if(bta1.le.-1.5) then
      write(not,2055) bta1,pf3
      else
      write(not,2050) bta1,pf1
      endif
   20 write(not,2020)
      btg1(ig2)=bta1
   30 continue

c-----output formats

 2010 format(/' Limit-state function',i5)
 2020 format(1x,77('-'))
 2030 format(/39x,'Improved Breitung',6x,7hTvedt's,' EI')
 2040 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     */,' probability                     Pf2 =',1pe14.7,6x,1pe14.7)
 2045 format(' Generalized reliability index betag =',5x,f9.6,11x,f9.6,
     */,' probability                     Pf2 =',' 1- ',1pe14.7,2x,
     *' 1- ',1pe14.7)
 2050 format(' Generalized reliability index betag =',5x,f9.6,13x,'----'
     */,' probability                     Pf2 =',1pe14.7,13x,'----')
 2055 format(' Generalized reliability index betag =',5x,f9.6,13x,'----'
     */,' probability                     Pf2 =',' 1- ',1pe14.7,13x,
     *'----')
c     run=etime(tarray)

      end subroutine sopf
