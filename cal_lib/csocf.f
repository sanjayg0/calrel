      subroutine csocf(iso)

      implicit   none

      include   'blkrel1.h'
      include   'bsocf.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4) :: iso, ist, itg,itm, nr,nc, npk
      integer (kind=4) :: n1 , n2, n3, n4, n5, n6, n7, n8, n9
      integer (kind=4) :: n10,n11,n12,n13,n14,n15,n16,n17,n19
      integer (kind=4) :: n20,n21,n22,n23,n24,n25,n26,n27,n28,n29
      integer (kind=4) :: n30,n31,n32,n33,n34,n35,n36,n37,n38,n39
      integer (kind=4) :: n40,n41,n42,n43,n44,n45,n46,n47,n48,n49
      integer (kind=4) :: n50,n54
      real    (kind=8) :: see

      save

      itg=1
      call freei('G',itg,1)
      call freei('F',igf,1)
      itm=0
      call freei('M',itm,1)
      call locate('tf  ',n1,nr,nc)
      call locate('alph',n3,nr,nc)
      call locate('sg  ',n4,nr,nc)
      call locate('ex  ',n5,nr,nc)
      call locate('par ',n6,nr,nc)
      call locate('bnd ',n7,nr,nc)
      call locate('ib  ',n8,nr,nc)
      call locate('tp  ',n9,nr,nc)
      call locate('ids ',n10,nr,nc)
      call lodefr('t1  ',n12,1,nry)
      call lodefr('t2  ',n13,1,nry)
      call lodefr('t3  ',n14,1,nry)
      call locate('beta',n19,nr,nc)
      call locate('igt ',n21,nr,nc)
      call locate('lgp ',n22,nr,nc)
      call locate('izx ',n23,nr,nc)
      call locate('ipa ',n24,nr,nc)
      call locate('x   ',n26,nr,nc)
      call locate('igfx',n28,nr,nc)
      if(itm.eq.0) then
      write(not,1000) itg
      call locate('xlin',n2,nr,nc)
      call lodefr('srt ',n11,nry,nry*ngf)
      call lodefr('c   ',n15,1,nry-1)
      call define('ddg ',n16,nry-1,nry-1)
      call define('u   ',n17,nry-1,nry-1)
      call locate('sdgy',n20,nr,nc)
      call locate('fltf',n25,nr,nc)
      call locate('fltc',n27,nr,nc)
      call socf(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *          ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),ia(n13),ia(n14)
     *   ,ia(n15),ia(n16),ia(n17),ia(n19),ia(n20),ia(n21),ia(n22),
     *   ia(n23),ia(n24),ia(n25),ia(n26),ia(n27),ia(n28),iso,itg)
      call delete('ddg ')
      call delete('u   ')
      call delete('c   ')
      else
      call lodefr('curv',n29,nry-1,ngf)
      call locate('ixz ',n30,nr,nc)
      call lodefr('y   ',n31,1,nry)
      call lodefr('a   ',n32,1,nry)
      call lodefr('gam ',n33,1,nry)
      call lodefr('yo  ',n34,nry,ngf)
      call lodefr('ao  ',n35,nry,ngf)
      call lodefr('cosa',n36,nry,(nry-1)*ngf)
      call lodefr('ysta',n37,nry,(nry-1)*ngf)
      call lodefr('yoo0',n38,nry,ngf)
      call lodefr('yoo1',n39,nry,ngf)
      call lodefr('yoo2',n40,nry,ngf)
      call lodefr('yses',n41,1,nry)
      call lodefr('ases',n42,1,nry)
      call lodefr('xses',n43,1,nry)
      call lodefr('bbet',n44,nry,ngf)
      call lodefi('iste',n45,nry-1,ngf)
      call lodefr('ylin',n46,nry,ngf)
      call lodefr('gdgx',n47,nrx,1)
      call lodefi('igss',n48,ngf,1)
      call lodefr('aa1 ',n49,nry,ngf)
      call lodefr('yy1 ',n50,nry,ngf)
      call lodefr('sed ',n54,1,ngf)
      npk=nry-1
      call freei('k',npk,1)
      epa=0.10d0
      call freer('a',epa,1)
      epb=0.05d0
      call freer('b',epb,1)
      epc=0.5d0
      call freer('c',epc,1)
      ist=0
      call freei('t',ist,1)
      see=12345
      call freer('e',see,1)
      write(not,1005) npk,epa,epb,epc,ist
c      print *,
c     *'seed for random number for starting points.......see=',see
      if(itm.eq.2) then
      call eisocf(ia(n1),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),ia(n8),
     *     ia(n9),ia(n10),ia(n12),ia(n13),ia(n14),ia(n29),ia(n19),
     *     ia(n21),ia(n22),ia(n23),ia(n24),ia(n26),ia(n28),ia(n30),
     *     ia(n31),ia(n32),ia(n33),ia(n34),ia(n35),ia(n36),ia(n37),
     *     ia(n38),ia(n39),ia(n40),ia(n41),ia(n42),ia(n43),ia(n44),
     *     ia(n45),ia(n46),ia(n47),ia(n48),ia(n49),ia(n50),
     *     ia(n54),ist,npk,see)
      else
      call sgsocf(ia(n1),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),ia(n8),
     *     ia(n9),ia(n10),ia(n12),ia(n13),ia(n14),ia(n29),ia(n19),
     *     ia(n21),ia(n22),ia(n23),ia(n24),ia(n26),ia(n28),ia(n30),
     *     ia(n31),ia(n32),ia(n33),ia(n34),ia(n35),ia(n36),ia(n37),
     *     ia(n38),ia(n39),ia(n40),ia(n41),ia(n42),ia(n43),ia(n44),
     *     ia(n45),ia(n46),ia(n47),ia(n48),ia(n49),ia(n50),
     *     ia(n54),ist,npk,see)
      endif
      call delete('y   ')
      call delete('a   ')
      call delete('gam ')
      call delete('yses')
      call delete('ases')
      call delete('xses')
      endif
      call delete('t1  ')
      call delete('t2  ')
      call delete('t3  ')

 1000 format(//
     & ' >>>> SECOND-ORDER RELIABILITY ANALYSIS  --',
     & ' CURVATURE FITTING <<<<'//
     & ' Type of integration scheme used ..................itg=',i5/
     & '   itg=1 ..........................improved Breitung formula'/
     & '   itg=2 ..........................improved Breitung formula'/
     & '         ...........................& Tvedt''s exact integral')

 1005 format(//
     & ' >>> SECOND-ORDER RELIABILITY ANALYSIS --',
     & ' GRADIENT BASED CURVATURE FITTING <<<'//
     & ' Number of principal curvatures to be found........npk=',i5/
     & ' Convergence parameter.............................epa=',f8.4/
     & ' Min |beta * kappa|................................epb=',f8.4/
     & ' Perturbation length parameter.....................epc=',f8.4/
     & ' Restart flag .....................................ist=',i5/
     & '   ist=0 ..............................analyze a new problem'/
     & '   ist=1 .......................continue a previous analysis')

      end subroutine csocf
