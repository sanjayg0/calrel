      subroutine crtox(x,pr,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)

      implicit   none

      include   'prob.h'

      real    (kind=8) :: x(nrx),pr(nry),ex(nrx),sg(nrx)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx)
      integer (kind=4) :: igt(nig),lgp(3,nig)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),ib(nrx),izx(nry)

      integer (kind=4) :: i, ii, ier, j0,j1, k, nge
      real    (kind=8) :: df,dl,dl1
      real    (kind=8) :: pr1
      real    (kind=8) :: rk
      real    (kind=8) :: z,zk,zz

!---  loop over groups
!
      do 60 i = 1,nig
        j0 = lgp(1,i)
        j1 = lgp(1,i+1)-1
        if(i.eq.nig) j1 = nry
        nge = j1-j0+1
        if(nge.le.0) go to 60
          do 10 ii = j0,j1
          k = izx(ii)
          if(ids(k).eq.0) go to 10
      if(ids(k).ge.51) go to 51
      go to (1,2,3,4,5,6,7,51,51,51,11,12,13,14),ids(k)
!---  normal
   1  call dnormi(z,pr(ii),ier)
      x(k) = par(2,k)*z+par(1,k)
      go to 10
!---  lognormal
   2  call dnormi(z,pr(ii),ier)
      x(k) = exp(par(2,k)*z+par(1,k))
      go to 100
!---  gamma
    3 df = par(2,k)*2.0d0
      call dchisi(x(k),df,pr(ii),ier)
      x(k) = x(k)/2.d0/par(1,k)
      go to 100
!---  exponential
    4 pr1 = 1.d0-pr(ii)
      x(k) = -log(pr1)/par(1,k)+par(2,k)
      go to 100
!---  rayleigh
    5 pr1 = 1.d0-pr(ii)
      zz = -2.d0*log(pr1)
      x(k) = par(1,k)*sqrt(zz)+par(2,k)
      go to 100
!---  uniform
    6 x(k) = par(1,k)+(par(2,k)-par(1,k))*pr(ii)
      go to 100
!---  beta
    7 call dbetai(x(k),par(1,k),par(2,k),pr(ii),ier)
      x(k) = (par(4,k)-par(3,k))*x(k)+par(3,k)
      go to 100
!---  type 1 largest
   11 if(pr(ii).lt.0.99996832878669) then
          dl = -log(pr(ii))
          x(k) = par(1,k)-log(dl)/par(2,k)
      else
          pr1 = 1.d0-pr(ii)
          dl = pr1-pr1*pr1/2.d0+pr1**3/3.d0
          x(k) = par(1,k)-log(dl)/par(2,k)
      end if
      go to 100
!---  type 1 smallest
   12 rk = 1.d0-0.99996832878669
      if(pr(ii).gt.rk) then
          pr1 = 1.d0-pr(ii)
          dl1 = -log(pr1)
          x(k) = par(1,k)+log(dl1)/par(2,k)
      else
          dl1 = pr(ii)-pr(ii)*pr(ii)/2.d0+pr(ii)**3/3.d0
          x(k) = par(1,k)+log(dl1)/par(2,k)
      end if
      go to 100
!---  type 2 largest
   13 if(pr(ii).lt.0.99996832878669) then
          dl = -log(pr(ii))
      else
          pr1 = 1.d0-pr(ii)
          dl = pr1-pr1*pr1/2.d0+pr1**3/3.d0
      end if
      zk = 1.d0/par(2,k)
      x(k) = par(1,k)/dl**zk
      go to 100
!---  type 3 smallest
   14 pr1 = 1.d0-pr(ii)
      zk = 1.d0/par(2,k)
      x(k) = par(1,k)*(-log(pr1))**zk
      go to 100
!---  user-defined
   51 call cztoxu(pr(k),x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),
     *            ipa(1,k),ib(k),k,igt(i))
!---  check if x falls outside its bounds.
  100 if(ib(k).ne.0) call cbound(x(k),bnd(1,k),ib(k))
   10 continue
   60 continue

      end subroutine crtox
