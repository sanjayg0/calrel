c---------------------------------------------- freer
      subroutine freer(ch,data,num)

      implicit   none

      include    'line.h'

      character (len=1) :: blk,ch, ic,mul,div,add,sub,eqs,e

      integer (kind=4) :: num
      real    (kind=8) :: data(10)

      integer (kind=4) :: i,j,jj,k, nn
      real    (kind=8) :: xx
 
      save

      data blk/' '/,mul/'*'/,div/'/'/,add/'+'/,
     *     sub/'-'/,e/'e'/,eqs/'='/

c     Make sure CH is upper case

      i = ichar(ch)
      if(i.ge.97) then
        ic = char(i - 32)
      else
        ic = ch
      endif

c-----find real string ------------------------------

      i = 0
      if(ic.eq.blk) go to 250
      do i=1,llen
        if((line(i).eq.ic).and.(line(i+1).eq.eqs)) go to 250
      end do ! i
      return

c---- extract real data -----------------------------

  250 do j=1,num
        data(j) = 0.0d0
      end do ! j

      do j=1,num
        jj = 0
  270   if(i.gt.llen) go to 300
        call rddata(i,xx,nn)
        if(jj.ne.0) go to 275
        data(j) = xx
        go to 290

c-----  arithmetric statement -------------------------

  275   if(jj.eq.1) data(j)=data(j)*xx
        if(jj.eq.2) data(j)=data(j)/xx
        if(jj.eq.3) data(j)=data(j)+xx
        if(jj.eq.4) data(j)=data(j)-xx
        if(jj.ne.5) go to 290

c-----  exponential data ------------------------------

        jj = int(xx) ! WIP truncate not round with nint
        if(jj.eq.0) go to 290
        do k=1,jj
          if(xx.lt.0.0) data(j) = data(j)/10.d0
          if(xx.gt.0.0) data(j) = data(j)*10.d0
        end do ! k

c-----  set type of statement -------------------------

  290   jj = 0
        if(line(i).eq.mul) jj = 1
        if(line(i).eq.div) jj = 2
        if(line(i).eq.add) jj = 3
        if(line(i).eq.sub) jj = 4
        if(line(i).eq.e)   jj = 5
        if(line(i+1).eq.eqs) jj = 0
        if(jj.ne.0) go to 270
        if(nn.gt.9) return
  300   continue
      end do ! j

c----------------------------------------------------

      end
