ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dirs2(beta,rt,cur,mc,x,y,yy,rr,a1,a2,rot,
c    *                 ind,id,ig,btg1,alph)
c
c     functions:
c       compute the failure probability by the directional simulation
c       (simulate a line) method for the second order system reliability
c       problem.(for point fitting method)
c
c     input arguments:
c       beta : reliability indexes of all modes.
c       rt   : rotational transformation matrices. y=rt*y'.
c       cur  : curvatures for each semiparabolic intersection.
c       mc   : elements of minimun cut sets.
c       nry   : no. of variables.
c       ngf   : no. of performance functions.
c       nsm  : no. of directional simulation.
c       ntl : total number of elements in the minimun cut sets + ncs.
c       cov : criteria on the coefficient of variance.
c
c     working aray:
c       x,y,yy,rr,a1,a2,rot,stg,cstg,ind,id,ig,idd,istg
c
c     calls: dnormi,cmprin,dsolv2,dscum.
c
c     called by: main.
c
c     last version :  dec. 2, 1987, by h. -z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dirs2(beta,rt,cur,mc,x,y,yy,rr,a1,a2,rot,
     *                 ind,id,ig,btg1,alph,igfx)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      real    (kind=8) :: random
      integer (kind=4) :: ind(ngfc,2)
      integer (kind=4) :: ig(ngfc),mc(ntl,2)
      integer (kind=4) :: id(ngfc+2),igfx(ngf,3)
      real    (kind=8) :: x(nry),y(nry),beta(ngf)
      real    (kind=8) :: rt(nry,nry*ngf),yy(nry)
      real    (kind=8) :: cur(2,nry*ngf-ngf),rr(4,ngfc),a1(ngfc,2)
      real    (kind=8) :: a2(ngfc,2),rot(ngfc+2),btg1(ngf)
      real    (kind=8) :: alph(nry,ngf)

      integer (kind=4) :: i,ii,i1,ic,ics,imm,iopt,id0,idg, ier
      integer (kind=4) :: j
      integer (kind=4) :: kj,kk
      integer (kind=4) :: l,l2,ll
      integer (kind=4) :: m,m1,mm, mcend,mcst
      integer (kind=4) :: n,n1
      real    (kind=8) :: axp, btg
      real    (kind=8) :: cum,cvar
      real    (kind=8) :: dev
      real    (kind=8) :: gx
      real    (kind=8) :: pf,pff
      real    (kind=8) :: r,r1,rn,rnm, root
      real    (kind=8) :: sp,sy
      real    (kind=8) :: xp,xp1
      save

      rn=real(nry)
      mcst=1
      mcend=ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend=2*ngfc
      endif
      if(icl.eq.1) then
        call finim(igfx,igf)
        mcst=1
        mcend=2
        mc(1,1)=igf
        mc(1,2)=1
        mc(2,1)=0
        mc(2,2)=0
      endif
      if(ist.ne.0) then
        read(ns0,rec=3,err=10) x,cum,xp1,root,id0,i1
        id(1)=id0
        n1=i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif
      call droot(beta,alph,mc,x,y,ig,root,btg1,igfx,2,mcst,mcend)
      n1=0
      i1=0
      r1=0.d0
      xp1=0.d0
      cum=0.0d0
c.....check the origin is in the failure domain or not.
      do 1101 l=1,ngfc
      l2=igfx(l,2)
      gx=beta(l2)
      if(gx.le.1e-10) then
      ig(l)=0
      else
      ig(l)=1
      endif
 1101 continue
      imm=1
      idg=1
      do 1105 i=mcst,mcend
      if(mc(i,1).ne.0) then
      ic=mc(i,2)
      if(mc(i,1).lt.0) then
      imm=imm*ig(ic)
      else
      imm=imm*(1-ig(ic))
      endif
      else
      ics=1-imm
      idg=idg*ics
      imm=1
      endif
 1105 continue
      idg=1-idg
      if(idg.eq.1) then
      id(1)=1
      else
      id(1)=0
      endif
   30 continue
      n=npr
      n1=n1+n
      i1=i1+1
      write(not,2010)
      xp=0.d0
c
c     perform monte carlo simulation to simulate a normalized direction.
      iopt = 1
      do 140 j=i1,nsm
        axp=0.d0
        sy=0.d0
c     generate the random variables.
        do 60 m=1,nry
          call gguw(stp,1,iopt,random)
          iopt = 0
          r = random
          call dnormi(y(m),r,ier)
   60   continue
        do 70 m=1,nry
          sy=sy+y(m)*y(m)
   70     continue
        sy=dsqrt(sy)
        do 80 m=1,nry
   80     y(m)=y(m)/sy
c.....use performance function to determine r
        call dsolv2(rt,beta,y,yy,cur,rr,ind,a1,a2,igfx,root)
c.....count total number of roots.
        ll=0
        do 130 kj=1,2
          kk=1+ll
          m1=1
          rot(1)=0.d0
          do 90 ii=1,ngfc
            if(ind(ii,kj).eq.0) go to 90
            if(ind(ii,kj).eq.1) then
              m1=m1+1
              rot(m1)=rr(kk,ii)
            else
              m1=m1+2
              rot(m1-1)=rr(kk+1,ii)
              rot(m1)=rr(kk,ii)
            endif
   90     continue
          ll=2
c.....arrange m1 roots in orderings.
          if(m1.le.1) then
          if(id(1).eq.1) axp=axp+1.d0
          go to 130
          endif
          mm=m1-1
 100      do 110 ii=1,mm
            if(rot(ii).gt.rot(ii+1)) then
              r1=rot(ii)
              rot(ii)=rot(ii+1)
              rot(ii+1)=r1
            endif
 110      continue
          if(mm.gt.1) then
            mm=mm-1
            go to 100
          endif
c.
c.....determine the ranges of the roots in the failure sets.
c       if(icl.le.2) then
c         if(id(1).eq.0) then
c         cs=rot(2)*rot(2)
c         call dchis(cs,rn,p2,ier)
c         axp=axp+1.d0-p2
c         else
c         cs=rot(m1)*rot(m1)
c         call dchis(cs,rn,p2,ier)
c         axp=axp+p2
c         endif
c       go to 130
c       endif
          rot(m1+1)=root
c         print *,'rot=',(rot(ka),ka=1,m1)
c.....all the roots are positive.
          call dscum(m1,rot,a1(1,kj),a2(1,kj),beta,ig,
     *    id,igfx,xp,mc,mcst,mcend,2)
c         print *,'xp=',xp
          axp=axp+xp
 130    continue
        axp=axp/2.d0
        cum=cum+axp
        xp1=xp1+axp*axp
        if(j.ge.n1) then
          rnm=real(j)
          pf=cum/rnm
          sp=xp1-2.d0*pf*cum+rnm*pf*pf
          rnm=rnm*(rnm-1.d0)
          pff=1.d0-pf
          if(sp.gt..1e-12) then
            dev=dsqrt(sp/rnm)
            cvar=dev/pf
            call dnormi(btg,pff,ier)
            write(not,2020)j,pf,btg,cvar
            if(cvar.le.cov) go to 150
          else
            write(not,2030)j
          endif
          n1=n+n1
        endif
  140 continue
 150  id0=id(1)

      write(ns0,rec=3) x,cum,xp1,root,id0,min(j,nsm)

c---  output formats

 2010 format(/' trials',7x,'pf-mean ',6x,'betag-mean ',4x,
     *       'coef of var of pf')
 2020 format(i7,1p,3x,e14.7,3x,e14.7,3x,e14.7)
 2030 format(i7,3x,'    ----',9x,'    ----',9x,'    ----')

      end subroutine dirs2
