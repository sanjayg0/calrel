      subroutine gdgyp(t,tf1,tf2,dgx,dgyp,nge,nges,rp,j0,ix,kk)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: nge,nges, j0, ix,kk 
      real    (kind=8) :: rp
      real    (kind=8) :: tf1(nge,nge),tf2(nge,nge),dgx(nge)
      real    (kind=8) :: dgyp(nry,4,nrx),t(6,nrx)

      integer (kind=4) :: i,j, i0, j01
      real    (kind=8) :: xg

      do 10 i=nges+1,nge
      do 10 j=1,i
 10   tf1(j,i)=(tf1(j,i)-tf2(j,i))/rp
c     print *,'final tf1='
c     call mprint(tf1,nge,nge,1)
      j01=j0-1
c--   compute dg/dx * dx/dz/dpar
      do 120 j=1,nge
      if(j.le.nges) then
      xg=t(5,j01+j)*dgx(j)
      i0=nges+1
      else
      i0=j
      xg=0.d0
      endif
      do 130 i=i0,nge
 130  xg=xg+t(5,j01+i)*tf1(j,i)
 120  dgyp(j01+j,kk,ix)=xg
c--   compute dg/dx * dx/dz/dpar *dz/dy
      if(nges.eq.0) return
      do 220 i=1,nges
      xg=0.d0
      do 240 j=i,nges
 240  xg=xg+dgyp(j01+j,kk,ix)*tf1(j,i)
 220  dgyp(j01+i,kk,ix)=xg
c999  print *,'dgyp',(dgyp(j0+ka,kk,ix),ka=0,nge-1)

      end subroutine gdgyp
