      subroutine csorm

      implicit   none

      include   'blkrel1.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'test.h'

      integer (kind=4) :: iso

c     real tarray(2)

      save

      addk = 1
      iso = 1
      call freei('O',iso,1)
      if(iso.eq.1) then
        call csopf
      else if(iso.eq.2) then
        call csocf(iso)
c     call etime(tarray, run)
c     print *,'run time after socf = ',run
      else
        call csopf
        call csocf(iso)
      endif
      addk = 0

      end subroutine csorm
