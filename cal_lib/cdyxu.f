cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cdyxu(x,ex,sg,par,bnd,dyx,ids,ipa,izx,nge,i,igt,n0)
c
c   functions:
c      compute dy/dx for variables with user-defined distributions by
c      central difference.
c
c   input arguments:
c      x : basic random variables in the original space.
c      ex : mean values of x.
c      sg : standard deviations of x.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c            n-th element of ipa  =  0 means parameter n is constant.
c      izx : indicators for z to x.
c      nge : number of z's in the group.
c
c   output arguments:
c      dyx : dx/dy.
c
c   calls: udd, dnormi.
c
c   called by: cdyx.
c
c   last revision: dec 1 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdyxu(x,ex,sg,par,bnd,dyx,ids,ipa,izx,nge,
     *            i,igt,n0)

      implicit   none

      include   'prob.h'

      integer    nge, i,igt,n0
      real*8     x(nrx),ex(nrx),sg(nrx),par(4,nrx),dyx(nge,nge)
      real*8     bnd(2,nrx)
      integer    ids(nrx),izx(nge),ipa(4,nrx)

      integer    j,j1, k,l, ib, ier
      real*8     d, hm,hp
      real*8     p,phi,pd,py, r, tps, xl

      save

      data tps/2.506628275d0/,r/0.001d0/

c---- compute dy/dx.

      k = izx(i)
      if(igt.ge.3) then
        call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
      endif
      call udd(x,par(1,k),sg(k),ids(k),p,pd,bnd(1,k),ib)
      call dnormi(py,p,ier)
      phi = exp(-py*py/2.0d0)
      if(igt.eq.4.and.i.gt.n0) then
        j1 = n0
      else
        dyx(i,i) = pd*tps/phi
        j1 = i-1
      endif
      do j = 1,j1
        l = izx(j)
        d = sg(l)*r
        xl = x(l)
        x(l) = xl+d
        if(igt.ge.3) then
          call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
        endif
        call udd(x,par(1,k),sg(k),ids(k),hp,pd,bnd(1,k),ib)
        x(l) = xl-d
        if(igt.ge.3) then
          call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
        endif
        call udd(x,par(1,k),sg(k),ids(k),hm,pd,bnd(1,k),ib)
        dyx(i,j) = tps*(hp-hm)/(phi*d*2.d0)
        x(l) = xl
        if(igt.ge.3) then
          call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
        endif
      end do ! j

      if(igt.eq.4) return

      do j = i+1,nge
        dyx(i,j) = 0.d0
      end do ! j

      end subroutine cdyxu
