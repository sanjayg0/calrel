      subroutine cpnet

      implicit   none

      include   'blkrel1.h'
      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4) :: nr,nc, n1,n2,n3,n4,n5,n6,n7,n8
      real    (kind=8) :: rho0

      save

      rho0=0.6d0
      call freer('o',rho0,1)
      if(rho0.le.0.d0.or.rho0.gt.1.d0) rho0=0.6d0
      call locate('beta',n1,nr,nc)
      call locate('alph',n2,nr,nc)
      call lodefr('rhom',n3,ngf,ngf)
      call lodefr('prob',n4,ngf,ngf)
      call lodefi('next',n5,1,ngf)
      call define('irep',n6,1,ngfc)
      call define('id  ',n7,1,ngfc)
      call locate('igfx',n8,nr,nc)
      write(not,1000) rho0
      call pnet(ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),ia(n7),
     *          ia(n8),rho0)
      call delete('irep')
      call delete('id  ')
      itt=1
 1000 format(//' >>>> pnet approximation <<<<'//
     *' Threshold for coef. of correlation .rho0=',1p,e11.3)

      end subroutine cpnet
