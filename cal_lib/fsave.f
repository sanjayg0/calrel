      subroutine fsave(stf,sdxz,tf,dxz,igt,lgp,ig,indx,dgx,y)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      integer (kind=4) :: ig,indx
      integer (kind=4) :: lgp(3,nig),igt(nig)
      real    (kind=8) :: stf(ntge,ngf),sdxz(nrx,3*ngf),tf(ntf),dxz(nrx)
      real    (kind=8) :: dgx(nrx),y(nry)

      integer (kind=4) :: k,ki 
      integer (kind=4) :: nn,nn0,ng,nge,nge0,ng3
      nn=0
      nn0=0
      do 90 ki=1,nig
        if(ki.lt.nig) then
        nge0=lgp(1,ki+1)-lgp(1,ki)
        else
        nge0=nry-lgp(1,ki)+1
        endif
        nge=nge0*nge0
      if(igt(ki).le.2) go to 95
        if(indx.eq.0) then
        do 10 k=1,nge
 10     stf(nn0+k,ig)=tf(nn+k)
        else
        do 18 k=1,nge
 18     tf(nn+k)=stf(nn0+k,ig)
        endif
        nn0=nn0+nge
 95   nn=nn+nge
 90   continue
      if(indx.ne.0) return
      ng=ngf+ig
      ng3=ng+ngf
      do 20 k=1,nrx
      sdxz(k,ng)=dgx(k)
 20   sdxz(k,ig)=dxz(k)
      do 30 k=1,nry
 30   sdxz(k,ng3)=y(k)

      end subroutine fsave
