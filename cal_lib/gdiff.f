      subroutine gdiff(x,par,ids,dxzp,izx,i,np)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: izx,i,np
      real    (kind=8) :: x(nrx),dxz(2),dxzp(np),par(4,nrx)
      integer (kind=4) :: ids(nrx)

      integer (kind=4) :: j, k2, indx
      real    (kind=8) :: pp, r
      save

      do 71 j=1,np
      pp=par(j,i)
      if(dabs(pp).le.0.1e-8) then
      r=0.001d0
      else
      r=0.001d0*pp
      endif
      indx=0
      do 75 k2=1,2
      if(indx.eq.0) then
      par(j,i)=pp+r
      indx=1
      else
      par(j,i)=pp-r
      endif
 75   call cdxz(x,dxz(k2),par,ids,izx,1)
      par(j,i)=pp
 71   dxzp(j)=(dxz(1)-dxz(2))/2.d0/r

      end subroutine gdiff
