      subroutine cinpu

      implicit   none

      include   'blkchr.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'mdat4.h'
      include   'optm.h'
      include   'prob.h'

      include   'blkrel1.h'

      integer    n0,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13
      integer    n15,n16,n17,n18,n19,n20,n21, nr,nc
      integer    nls

      save

      data nls /0/

      call lodefh('xn  ',n0,4,nrx)
      call lodefr('ex  ',n1,1,nrx)
      call lodefr('sg  ',n2,1,nrx)
      call lodefr('par ',n3,4,nrx)
      call lodefr('x0  ',n4,1,nrx)
      call lodefr('bnd ',n5,2,nrx)
      call lodefr('tp  ',n6,1,ntp)
      call lodefi('igt ',n7,1,nig)
      call lodefi('lgp ',n8,3,nig)
      call lodefi('ids ',n9,1,nrx)
      call lodefi('ipa ',n10,4,nrx)
      call lodefi('ib  ',n11,1,nrx)
      call lodefi('izx ',n12,1,nrx)
      call lodefi('ixz ',n13,1,nrx)
      call lodefr('tf  ',n15,nrx,nrx)
      call defini('nds ',n16,1,nrx)
      call defini('igx ',n17,1,nrx)
      call locate('nes ',nes0,nr,nc)
      call lodefi('les ',les0,1,nls)
      call locate('fltf',n18,nr,nc)
      call locate('fltc',n19,nr,nc)
      call locate('fltp',n20,nr,nc)
      call lodefi('igfx',n21,3,ngf)

c.... les can only be input once.

      write(not,1000)
      if(ntf.le.1) ntf = 1

      call input(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     &           ia(n7),ia(n8),ia(n9),ia(n10),ia(n11),ia(n12),
     &           ia(n13),ia(n15),ia(n16),ia(n17),ia(nes0),
     &           ia(les0),ia(n18),ia(n19),ia(n20),ia(n21),nls)
c    &           ia(n18),ia(n21),nls)
      call chgdim('izx ',1,nry)
      call chgdim('tf  ',1,ntf)
      call chgdim('les ',1,nls)
      call locate('les ',les0,nr,nc)
      call delete('nds ')
      call delete('igx ')

 1000 format(//' >>>> INPUT DATA <<<<'/)

      end subroutine cinpu
