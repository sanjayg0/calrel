cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine scurvt(ddg,x,z,tp,rt,y1,y0,tf,ids,par,bt,bnd,sg,ib,
c    *                 sdgy,ex,ig,igt,lgp,izx,ipa)
c
c   functions:
c     compute curvature matrix at design point in the rotated standard space
c     using finite difference algorithm for the curvature fitting method.
c
c   input arguments:
c     x0  : design point in the original space.
c     tp  : deterministic parameters in performance function.
c     rt  : rotational transformation matrix, y = rt * y'
c     tf  : transformation matrix from z to y, y = tf * z.
c     ids: distribution number in x.
c     par : distribution parameters of x.
c     bt  : reliability index.
c     bnd : lower and upper bounds of x.
c     sg  : standard deviation of x.
c     ib  : code of distribution bounds of x.
c     sdgy: |dg/dy|
c
c   output arguments:
c     ddg : curvature matrix in y space at design point.
c
c   calls :sdiff,cmprin
c
c   called by: socf.
c
c   last revision: may 3, 1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine scurvt(ddg,x0,z,tp,rt,y1,y0,tf,ids,par,bt,bnd,sg,ib,
     *                 sdgy,ex,ig,igt,lgp,izx,ipa,x)

      implicit   none

      include   'file.h'
      include   'prob.h'

      integer (kind=4) :: ig
      real    (kind=8) :: bt, sdgy
      integer (kind=4) :: ids(nrx),ib(nrx),ipa(4,nrx)
      integer (kind=4) :: izx(nry),lgp(3,nig),igt(nig)
      real    (kind=8) :: ddg(nry-1,nry-1),x0(nrx),z(nry),tp(*)
      real    (kind=8) :: rt(nry,nry),x(nrx)
      real    (kind=8) :: y1(nry),y0(nry),tf(ntf),par(4,nrx)
      real    (kind=8) :: bnd(2,nrx),sg(nrx),ex(nrx)

      integer (kind=4) :: i, ii, j, indx
      real    (kind=8) :: dd
      real    (kind=8) :: f1,f2,f3,f4
      real    (kind=8) :: h2,hh,hi,hj
      real    (kind=8) :: r

      real    (kind=8) :: sdiff
      save

c     write(not,932)
c932  format(/,' ------------ rotation matrix rt ---------------',/)
c     call cmprin(ib,rt,nry,nry,nry,1,1)
c
c.... calculate the reduced curvature matrix from rotated space
c
      indx = 0
      r = 0.001d0
 999  do 30 i = 1,nry
   30 y1(i) = 0.d0
      y1(nry) = bt
      do 40 i = 1,nry-1
      do 40 j = 1,i
      hi  =  r
      if (i.eq.j) then
         h2 = hi*2.d0
         hh = h2*h2
         y1(i) = h2
         do 21 ii = 1,nrx
 21      x(ii) = x0(ii)
         f1 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         y1(i) = -h2
         do 22 ii = 1,nrx
 22      x(ii) = x0(ii)
         f3 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         y1(i) = 0.d0
         do 23 ii = 1,nrx
 23      x(ii) = x0(ii)
         f2 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         ddg(i,j) = (f1-f2-f2+f3)/hh
      else
         hj  =  r
         hh = hi*hj*4.d0
         y1(i) = hi
         y1(j) = hj
         do 24 ii = 1,nrx
 24      x(ii) = x0(ii)
         f1 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         y1(j) = -hj
         do 25 ii = 1,nrx
 25      x(ii) = x0(ii)
         f3 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         y1(i) = -hi
         y1(j) = hj
         do 26 ii = 1,nrx
 26      x(ii) = x0(ii)
         f2 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         y1(j) = -hj
         do 27 ii = 1,nrx
 27      x(ii) = x0(ii)
         f4 = sdiff(x,z,tp,rt,y1,y0,tf,ids,par,bnd,sg,ex,ib,ig,igt,lgp,
     *              izx,ipa)
         ddg(i,j) = (f1-f2-f3+f4)/hh
         y1(i) = 0.d0
         y1(j) = 0.d0
       endif
   40 continue
c-----compute length of gradient vector of g
c
      dd = dabs(sdgy)*2.d0
c-----compute the reduced second derivatives matrix in the rotated space
      do 130 i = 1,nry-1
      do 130 j = 1,i
      ddg(i,j) = ddg(i,j)/dd
      if(dabs(ddg(i,j)).ge.0.5.and.indx.eq.0) then
      write(not,399)
 399  format('WARNING: There is a noise problem. The step length in',/,
     *      '          finite difference routine has been increased',
     *      ' to 10*old')
      r = r*10.d0
      indx = 1
      go to 999
      endif
      ddg(j,i) = ddg(i,j)
  130 continue

      end subroutine scurvt
