cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cdxpar(x,z,ex,sg,par,ids,s,k)
c
c   functions:
c      compute d(x)/d(par) by fixing z.
c
c   input arguments:
c      x : basic random variable in the original space.
c      z : random variable obtained from phi(z) = fx(x).
c      ex : mean of x.
c      sg : standard deviation of x.
c      par : distribution parameters of x.
c      ids : distribution number of x.
c
c   output arguments:
c      s : [ dx/d(ex), dx/d(sg), dx/d(par1), ...,dx/d(par4) ].
c
c   calls: gamma, udd.
c
c   called by: fsenvd, fsenvd.
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdxpar(x,z,ex,sg,par,ids,s,k,bnd,ib,ipa,igt)

c     last revision: april 22, 1988 by h. z. lin.

      implicit   none

      include   'prob.h'

      real*8     z,ex,sg
      integer    ids,ib,igt
      real*8     x(nrx),par(4),s(6),bnd(2)
      integer    ipa(4)

      integer    i, ier, k
      real*8     a1,a2, b1,b2,ba, d,det,ds1,ds2,du1,du2
      real*8     e,e2, g1,g2,gm1,gm2, gp1,gp2, p,p1,pp,pp1,pp2
      real*8     r,rm,rp,rpp, sg2, x1,x2,xt,xt1,xt2, xx, zk

      save

      if(ids.eq.0) then
        do i = 1,6
          s(i) = 0.0d0
        end do ! i
        return
      endif
      s(5) = 0.d0
      s(6) = 0.d0
      xx   = x(k)

      if(ids.ge.51) go to 510

      go to (10,20,30,40,50,60,510,100,100,100,110,120,130,140),ids

  10  s(2) = z
      s(4) = s(2)
      return

   20 s(3) =  xx
      s(4) =  z*xx
      e    =  sg/ex
      e2   =  e*e
      d    =  1.d0+e2
      a1   =  1.d0/ex*(1.d0+e2/d)
      a2   = -e2/d/par(2)/ex
      s(1) =  s(3)*a1+s(4)*a2
      b1   = -e/d/ex
      b2   = -b1/par(2)
      s(2) =  s(3)*b1+s(4)*b2
      return

  30  call dnorm(z,p)
      x(k) = xx
      pp   = 2.d0*par(2)
      call dchisi(xt,pp,p,ier)
      s(3) = -xt/(2.d0*par(1)*par(1))
      r    =  0.001d0*par(2)
      pp1  =  2.d0*(par(2)+r)
      pp2  =  2.d0*(par(2) - r)
      call dchisi(xt1,pp1,p,ier)
      call dchisi(xt2,pp2,p,ier)
      s(4) = (xt1 - xt2)/4.d0/(par(1)*r)
      sg2  =  sg*sg
      s(1) = (s(3)+s(4)*2.d0*ex)/sg2
      s(2) = -2.d0*ex*(s(3)+s(4)*ex)/(sg*sg2)
      return

   40 call dnorm(-z,p)
      s(3) =  log(p)/par(1)/par(1)
      s(4) =  1.d0
      s(1) =  1.d0
      s(2) = -s(3)/(sg*sg) - 1.d0
      return

   50 call dnorm(-z,p)
      p1   = 2.d0*log(p)
      s(3) = sqrt(-p1)
      s(4) = 1.d0
      s(1) = 1.d0
      s(2) = s(3)*1.526399746d0 - 1.91305838d0
      return

   60 ba   = par(2) - par(1)
      call dnorm(z,p)
      s(3) = 1.d0 - p
      s(4) = p
      s(1) = 1.d0
      s(2) = 1.732050808d0*(2.d0*p - 1.d0)
      return

  100 return

  110 call dnorm(z,p)
      p1   = -log(p)
      p1   =  log(p1)
      s(3) =  1.d0
      s(4) =  p1/par(2)/par(2)
      s(1) =  s(3)
      s(2) =  0.450040993d0*s(3) - 1.28254983d0*s(4)/(sg*sg)
      return

  120 call dnorm(-z,p)
      p1   = -log(p)
      p1   =  log(p1)
      s(3) =  1.d0
      s(4) =  p1
      s(1) =  s(3)
      s(2) =  0.450040993d0*s(3) - 1.28254983d0*s(4)/(sg*sg)
      return

  130 call dnorm(z,p)
      p1   = -log(p)
      zk   = -1.d0/par(2)
      s(3) = p1**zk
      s(4) = par(1)*zk*p1**(zk - 1.)
      r    = 1.d0/par(2)
      g1   = gamma(1.d0 - r)
      g2   = gamma(1.d0 - r*2)
      du1  = g1
      ds1  = dsqrt(g2 - g1*g1)
      rp   = 1.d0/(par(2) + 0.001d0)
      rm   = 1.d0/(par(2) - 0.001d0)
      gp1  = gamma(1.d0 - rp)
      gm1  = gamma(1.d0 - rm)
      gp2  = gamma(1.d0 - rp*2.d0)
      gm2  = gamma(1.d0 - rm*2.d0)
      du2  = (gp1 - gm1)*par(1)/0.002d0
      ds2  = (sqrt(gp2 - gp1*gp1) - sqrt(gm2 - gm1*gm1))*par(1)/0.002d0
      det  =  du1*ds2  - du2*ds1
      s(1) = (ds2*s(3) - ds1*s(4))/det
      s(2) = (-du2*s(3) + du1*s(4))/det
      return

  140 call dnorm( - z,p)
      p1   = log(p)
      p1   = 1.d0 - p1
      zk   = 1.d0/par(2)
      s(3) = p1**zk
      s(4) = par(1)*zk*p1**(zk - 1.)
      r    = 1.d0/par(2)
      g1   = gamma(1.d0+r)
      g2   = gamma(1.d0+r*2.d0)
      du1  = g1
      ds1  = dsqrt(g2 - g1*g1)
      rp   = 1.d0/(par(2) + 0.001d0)
      rm   = 1.d0/(par(2) - 0.001d0)
      gp1  = gamma(1.d0 + r p)
      gm1  = gamma(1.d0 + rm)
      gp2  = gamma(1.d0 + rp*2.d0)
      gm2  = gamma(1.d0 + rm*2.d0)
      du2  = (gp1 - gm1)*par(1)/0.002d0
      ds2  = (sqrt(gp2 - gp1*gp1) - sqrt(gm2 - gm1*gm1))*par(1)/0.002d00
      det  = du1*ds2 - du2*ds1
      s(1) = (ds2*s(3) - ds1*s(4))/det
      s(2) = ( - du2*s(3)+du1*s(4))/det
      return

  510 r = 0.001d0
      call dnorm(z,p)
      xx = x(k)
      do i = 1,4
        pp = par(i)
        if(pp.eq.0.0d0) then
          rpp = r
        else
          rpp = r*pp
        endif
        par(i) = pp+rpp
        call cztoxu(p,x,ex,sg,par,bnd,ids,ipa,ib,k,igt)
        x1     = x(k)
        par(i) = pp - rpp
        call cztoxu(p,x,ex,sg,par,bnd,ids,ipa,ib,k,igt)
        x2     = x(k)
        par(i) = pp
        x(k)   = xx
        s(2+i) = (x1 - x2)/(rpp*2.d0)
      end do ! i
      s(1) = 0.d0
      s(2) = 0.d0

      end subroutine cdxpar
