cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cztox(z,x,ex,sg,p,bnd,id,ip,ib,k,igt)
c
c   functions:
c      transform the standard normal variate z to x.
c
c   input arguments:
c      x : basic random variables.
c      z : standard normal variate.
c      p : distribution parameter of x.
c      bnd : lower and upper bound of x.
c      ib : code of distribution bound of x.
c           = 0 , no bounds
c           = 1 , bounded from below.
c           = 2 , bounded from above.
c           = 3 , both sides bounded.
c      id : distribution number of x.
c
c   output arguments:
c      x : new x.
c
c   calls: dnorm, dchisi, cztoxu.
c
c   called by: cytox.
c
c   last revision: sept. 18 1986 by p.-l. liu.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cztox(z,x,ex,sg,p,bnd,id,ip,ib,k,igt)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: id,ib,k,igt
      real    (kind=8) :: z, ex,sg
      integer (kind=4) :: ip(4)
      real    (kind=8) :: x(nrx),p(4),bnd(2)

      integer (kind=4) :: ier
      real    (kind=8) :: df,dl,dl1
      real    (kind=8) :: pr, pr1
      real    (kind=8) :: zk, zz
      save

      if(id.eq.0) return
      if(id.ge.51) go to 51
      go to (1,2,3,4,5,6,7,51,51,51,11,12,13,14),id
c---  normal
    1 x(k) = p(2)*z+p(1)
      return
c     go to 100
c---  lognormal
    2 x(k) = exp(p(2)*z+p(1))
      go to 100
c---  gamma
    3 call dnorm(z,pr)
      df = p(2)*2.0d0
      call dchisi(x(k),df,pr,ier)
      x(k) = x(k)/2.d0/p(1)
      go to 100
c---  exponential
    4 call dnorm(-z,pr1)
      x(k) = -log(pr1)/p(1)+p(2)
      go to 100
c---  rayleigh
    5 call dnorm(-z,pr1)
      zz = -2.d0*log(pr1)
      x(k) = p(1)*sqrt(zz)+p(2)
      go to 100
c---  uniform
    6 call dnorm(z,pr)
      x(k) = p(1)+(p(2)-p(1))*pr
      go to 100
c---  beta
    7 call dnorm(z,pr)
      call dbetai(x(k),p(1),p(2),pr,ier)
      x(k) = (p(4)-p(3))*x(k)+p(3)
      go to 100
c---  student
c   8 call mstudi(z,p(1),p(2),p(3),x(k))
c     go to 100
c---  type 1 largest
   11 if(z.lt.4.d0) then
          call dnorm(z,pr)
          dl = -log(pr)
          x(k) = p(1)-log(dl)/p(2)
      else
          call dnorm(-z,pr1)
          dl = pr1-pr1*pr1/2.d0+pr1**3/3.d0
          x(k) = p(1)-log(dl)/p(2)
      end if
      go to 100
c---  type 1 smallest
   12 if(z.gt.-4.d0) then
          call dnorm(-z,pr1)
          dl1 = -log(pr1)
          x(k) = p(1)+log(dl1)/p(2)
      else
          call dnorm(z,pr)
          dl1 = pr-pr*pr/2.d0+pr**3/3.d0
          x(k) = p(1)+log(dl1)/p(2)
      end if
      go to 100
c---  type 2 largest
   13 if(z.lt.4.d0) then
          call dnorm(z,pr)
          dl = -log(pr)
      else
          call dnorm(-z,pr1)
          dl = pr1-pr1*pr1/2.d0+pr1**3/3.d0
      end if
      zk = 1.d0/p(2)
      x(k) = p(1)/dl**zk
      go to 100
c---  type 3 smallest
   14 call dnorm(-z,pr1)
      zk = 1.d0/p(2)
      x(k) = p(1)*(-log(pr1))**zk
      go to 100
c---  user-defined
   51 call dnorm(z,pr)
c     print *,'z=',z,'pr=',pr
      call cztoxu(pr,x,ex,sg,p,bnd,id,ip,ib,k,igt)
c---  check if x falls outside its bounds.
  100 if(ib.ne.0) call cbound(x(k),bnd,ib)

      end subroutine cztox
