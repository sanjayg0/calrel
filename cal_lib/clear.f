cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine clear
c
c   functions:
c      delete all arrays in storage.
c
c   called by: cexect.
c
c   last revision: aug. 14 1987 by pei-ling liu
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine clear

      implicit   none

      include   'blkrel1.h'
      include   'dbsy.h'

      save

      next = 1
      idir = mtot
      numa = 0

      end subroutine clear
