c    *********************************************************************
c    subroutine project
c    *********************************************************************
c    cosa = cosines of the last step vector
c    ystar  =  design points coordinates
c    icont  =  curvature computed
c
c     written by mario de stefano    august-october 1990
c     *********************************************************************
      subroutine project (y,cosa,ystar,nry,icont)

      implicit   none

      integer (kind=4) :: nry,icont
      real    (kind=8) :: y(nry),cosa(nry,nry-1),ystar(nry,nry-1)

      integer (kind=4) :: i,j,k

      real    (kind=8) :: cdot, ysum

      ysum = cdot(y,ystar(1,1),1,1,nry)/cdot(ystar(1,1),
     &                                       ystar(1,1),1,1,nry)

      do i = 1,nry
        y(i) = y(i)+(1.0d0-ysum)*ystar(i,1)
      end do ! i
      do i = 1,icont-1
        ysum = 0.d0
        do k = 1,nry
          ysum = ysum+cosa(k,i)*(ystar(k,i)-y(k))
        end do ! k
        do j = 1,nry
          y(j) = y(j)+ysum*cosa(j,i)
        end do ! j
      end do ! i

      end subroutine project
