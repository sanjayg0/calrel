!     *********************************************************************
!     subroutine fchekn
!     check convergence for subsequent prinipal axis determination
!
!     last version: dec. 27,1990 by hong-zong lin
!
!     **********************************************************************
      subroutine fchekn(y,yo,a,gx,iter,nck,ig)

      implicit   none

      include   'chck.h'
      include   'optm.h'
      include   'prob.h'

      integer (kind=4) :: iter,nck,ig
      real    (kind=8) :: gx
      real    (kind=8) :: y(nry),a(nry),yo(nry)

      real    (kind=8) :: tm,tt,ya

      real    (kind=8) :: cdot

      save

!---  check first-order optimization condition for subsequent curvatures
      if(iop.eq.6) ya=cc
      tm = abs(gx)
      tt = cdot(y,y,1,1,nry)
      tt = abs(1.0d0 - abs(cdot(y,a,1,1,nry)/sqrt(tt)))
c     do 10 i=1,nry
c      tt=tt+(y(i)-yo(i))**2
c10   continue
c     tt=dsqrt(tt)
      tm = max(tm,tt)
c     if(tm.lt.tt) tm=tt
      if(tm.le.tol) then
         nck=1
         return
      end if

c---  check iterations number

      if(iter.gt.ni1) then
         call cerror(21,ig,0.,' ')
         nck=-1
      end if

      end subroutine fchekn
