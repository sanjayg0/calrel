      subroutine csenc(isv)

      implicit   none

      include   'blkchr.h'
      include   'blkrel1.h'
      include   'bond.h'
      include   'file.h'
      include   'flag.h'
      include   'line.h'
      include   'prob.h'

      integer (kind=4) :: isv
      integer (kind=4) :: n0,n1,n2,n3,n4,n5,n6,n7,n8,n9,n10, nr,nc

      save

      call locate('xn  ',n0,nr,nc)
      call locate('xlin',n1,nr,nc)
      call locate('beta',n2,nr,nc)
      call locate('sdgy',n3,nr,nc)
      call locate('tp  ',n4,nr,nc)
      call define('$$$$',n5,6,nrx)
      call locate('ids ',n6,nr,nc)
      call locate('ipa ',n7,nr,nc)
      call locate('fltf',n8,nr,nc)
      call locate('ixz ',n9,nr,nc)
      call locate('igfx',n10,nr,nc)
      call gsenc(nd(1),ia(n1),ia(n2),ia(n3),ia(n4),ia(n5),ia(n6),
     *           ia(n7),ia(n8),ia(n9),ia(n10),isv)
      call delete('$$$$')

      end subroutine csenc
