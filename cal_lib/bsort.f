!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine bsort(b,next,bmin,bmax)
!
!   functions:
!     sort vector b in order of increasing values.
!
!   input arguments:
!     b : vector to be sorted.
!
!   output arguments:
!     bmin : a pointer such that b(min) is the smallest element of b.
!     bmax : a pointer such that b(max) is the largest element of b.
!     next : an array indicating the element which is next to this
!            element in magnitude.
!
!   output arguments:
!
!   calls: none.
!
!   called by: bounds.
!
!   last revision: jan. 9 1990 by h.-z. lin.
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine bsort(b,next,bmin,bmax,igfx)

      implicit   none

      include   'cuts.h'
      include   'prob.h'

      real    (kind=8) :: b(ngf)
      integer (kind=4) :: next(ngfc),igfx(ngf,3)
      integer (kind=4) :: bmin,bmax

      integer (kind=4) :: i,j, jp, ki

      save

      if(b(igfx(1,2)).gt.b(igfx(2,2))) then
        bmin    = 2
        bmax    = 1
        next(2) = 1
        next(1) = 0
      else
        bmin    = 1
        bmax    = 2
        next(1) = 2
        next(2) = 0
      endif

      if(ngfc.eq.2) return

      do i = 3,ngfc
        ki = igfx(i,2)
        if(b(ki).le.b(igfx(bmin,2))) then
          next(i) = bmin
          bmin    = i
        elseif(b(ki).ge.b(igfx(bmax,2))) then
          next(bmax) = i
          next(i)    = 0
          bmax       = i
        else
          jp = bmin
  35      j  = next(jp)
          if(b(ki).le.b(igfx(j,2)))go to 45
          jp = j
          go to 35

  45      next(jp) = i
          next(i)  = j
        endif

      end do ! i

      end
