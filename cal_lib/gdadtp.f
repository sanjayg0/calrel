cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine gdadtp(x,y,tp,ex,sg,a,sbt,dgx,gam1,tf,par,bnd,igt,lgp,ids,
c    *                 ipa,izx,ixz,dgx1,dadd,ig)
c
c   functions:
c      compute d(dg/dy)/d(par), where par is the parameters in the transition
c      function.
c
c   input arguments:
c      x : basic random variables in the original space.
c      y : basic variables in the standard space.
c      tp : deterministic parameters in performance function.
c      ex : mean values of x.
c      sg : standard deviations of x.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      par : distribution parameters of x.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      ig : performance function number.
c
c   output arguments:
c      dadd: d(dg/dy)/d(par).
c
c   calls: cdgx, udgx, cdot.
c
c   called by: gsens.
c
c   last revision: june 28,1989 by h.-z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gdadtp(x,y,tp,ex,sg,a,sbt,tf,par,bnd,igt,lgp,
     *                  ids,ipa,izx,ixz,t,dadd,dxz,ig)

      implicit   none

      include   'flag.h'
      include   'prob.h'

      integer (kind=4) :: ig 
      real    (kind=8) :: sbt
      integer (kind=4) :: ipa(4,nrx),lgp(3,nig),igt(nig),izx(nry)
      integer (kind=4) :: ids(nrx),ixz(nrx)
      real    (kind=8) :: x(nrx),y(nry),tp(*),ex(nrx),sg(nrx),t(nrx,6)
      real    (kind=8) :: a(nry),tf(ntf),par(4,nrx),dadd(nry,ntp)
      real    (kind=8) :: dxz(nrx),bnd(2,nrx)

      integer (kind=4) :: j,j1, k
      real    (kind=8) :: abc, dtpp, rr, tpp
      save

c
c---  compute d(dg/dx)/d(tp).
c
      do 15 k=1,ntp
      tpp=tp(k)
      if(dabs(tpp).le.0.1e-6) then
      dtpp=0.001d0
      else
      dtpp=tpp*0.001d0
      endif
      tp(k)=tpp+dtpp
      if(igr.eq.0) then
        call cdgx(x,sg,t(1,1),tp,ig,ids,0)
      else
        call udgx(x,t(1,1),tp,ig)
      endif
      tp(k)=tpp-dtpp
      if(igr.eq.0) then
        call cdgx(x,sg,t(1,2),tp,ig,ids,0)
      else
        call udgx(x,t(1,2),tp,ig)
      endif
      rr=2.d0*dtpp
      do 12 j=1,nrx
  12  t(j,3)=(t(j,1)-t(j,2))/rr
      do 10 j=1,nry
      j1=izx(j)
   10 t(j,1)=t(j1,3)
      call fdirct(x,y,tp,ex,sg,dxz,t(1,1),dadd(1,k),tf,par,bnd,
     *           igt,lgp,ids,ipa,izx,ixz,1.,t(1,2),1,1)
c     compute d(alpha)/d(par)
      abc=0.d0
      do 91 j=1,nry
 91   abc=abc+dadd(j,k)*a(j)
      do 92 j=1,nry
 92   dadd(j,k)=(-dadd(j,k)+abc*a(j))/sbt
      tp(k)=tpp
   15 continue

      end subroutine gdadtp
