!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      subroutine eisocf(ex,sg,par,bnd,tp,tf,igt,lgp,ids,ipa,ib,izx,
!                      ixz,beta,alpha,x,y,a,gam,t1,t2,t3,ist)
!
!   functions:
!      control the flow of solution procedures to compute second order
!      failure probability and relability index.
!
!   input arguments:
!      ex : mean vector of x.
!      sg : standard deviations of x.
!      par : distribution parameters of x.
!      bnd : lower and upper bound of x.
!      tp : deterministic parameters in performance function.
!      tf : the transformation matrix from z to y, i.e. y  =  tf * z.
!      igt : group types.
!      lgp : location of the first element of a group in y.
!      ids : distribution numbers of x.
!      ipa : #'s of the associated rv of distribution parameters.
!             n-th element of ipa  =  0 means parameter n is constant.
!      ib : code of distribution bounds of x.
!            =  0 , no bounds
!            =  1 , bounded from below.
!            =  2 , bounded from above.
!            =  3 , both sides bounded.
!      izx : indicators for z to x.
!      ixz : indicators for x to z.
!      ist : restart flag.
!            ist  =  1 , restart problem.
!      igf : for icl = 1, perform form for performance function igf.
!
!   output arguments:
!      beta : reliability indices for all failure modes.
!      alpha : unit normal in y space for all failure modes.
!      adgx  : dgx in x space for all failure modes
!      x : basic random variables in the original space.
!      y : basic variables in the standard space.
!      a : unit normal vector in the y space.
!      gam : unit normal vector in the z space.
!      t1 : temporary array.
!      t2 : temporary array.
!      t3 : temporary array.
!
!   calls: fytox, project, fcosine1
!
!   called by: csocf
!
!   apr. 27, 1996 by chun-ching li
!
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine eisocf(tf,alpha,sg,ex,par,bnd,ib,tp,ids,t1,t2,t3,
     &           curv,beta,igt,lgp,izx,ipa,x,igfx,ixz,y,a,gam,yo,ao,
     &           cosa,ystar,yoo,yooo,yoooo,yses,ases,xses,
     &           bbeta,istep,ylin,gdgx,igss,a1,y1,sed,
     &           ist,kreq,see)
!     subroutine eisocf(tf,alpha,sg,ex,par,bnd,ib,tp,ids,t1,t2,t3,
!    *          curv,beta,igt,lgp,izx,ipa,x,igfx,ixz,y,a,gam,yo,ao,
!    *          cosa,ystar,yoo,bbeta,istep,ylin,gdgx,igss,a1,y1,sed,
!    *          ist,kreq,see)

      implicit   none

      include   'blkrel1.h'
      include   'bsocf.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'fsav.h'
      include   'optm.h'
      include   'prob.h'

      integer (kind=4) :: ist,kreq
      real    (kind=8) :: see 
      real    (kind=8) :: tf(*),x(nrx),a(nry)
      real    (kind=8) :: y(nry),t1(nry),t2(nry),t3(nrx),gam(nry),tp(*)
      real    (kind=8) :: ex(nrx),sg(nrx)
      integer (kind=4) :: ids(nrx),ipa(4,nrx),igfx(ngf,3)
      real    (kind=8) :: par(4,nrx),bnd(2,nrx)
      integer (kind=4) :: ib(nrx)
      real    (kind=8) :: alpha(nry,ngf),gdgx(nrx)
      real    (kind=8) :: beta(ngf),ylin(nry,ngf)
      integer (kind=4) :: igss(ngf)
      integer (kind=4) :: igt(nig),lgp(3,nig),izx(nry),ixz(nrx)
      real    (kind=8) :: yo(nry,ngf),ao(nry,ngf),cosa(nry,nry-1,ngf)
      real    (kind=8) :: ystar(nry,nry-1,ngf),sed(ngf)
      real    (kind=8) :: yoo(nry,ngf),yooo(nry,ngf),yoooo(nry,ngf)
      real    (kind=8) :: yses(nry),a1(nry,ngf),y1(nry,ngf)
      real    (kind=8) :: ases(nry),xses(nrx),bbeta(nry,ngf)
      real    (kind=8) :: curv(nry-1,ngf)
      integer (kind=4) :: istep(nry-1,ngf)

      integer (kind=4) :: i,ier, ig2,ig4,igk, ik, icont
      integer (kind=4) :: jj
      integer (kind=4) :: kk
      integer (kind=4) :: nbk,nck,nses 
      real    (kind=8) :: addk
      real    (kind=8) :: bdp, bta1,bta2
      real    (kind=8) :: curvold 
      real    (kind=8) :: err 
      real    (kind=8) :: pf1,pf2,pf3,pf4
      real    (kind=8) :: ysum, seed, sdgy,sdgyo,sdgyu
      real    (kind=8) :: ymax,ysign

      real    (kind=8) :: cdot

      addk = 0
!---  define b (approx. hessian) and q (gradient) of lagrangian
!---  solution phase.
      if(ist.eq.0) then
      do 1100 i = 1,ngf
 1100 sed(i) = see
      endif
      do 100 igk = 1,ngfc
      icont = 1
!     ncount = 0
      nbk = 0
      ig4 = igfx(igk,3)
      ig2 = igfx(igk,2)
      if(ist.ne.0) then
      if(ig4.ne.igf) go to 100
      icont = igss(ig2)
      endif
      seed = sed(ig2)
      iter = istep(icont,ig2)
      do 2015 ik = 1,nry
      y(ik) = y1(ik,ig2)
 2015 a(ik) = a1(ik,ig2)
      bdp = beta(ig2)
      write(not,2000) ig4
      write(not,2010)
      write (not,9438)
      icont = 0
!---------------------------------------------------------------------

!     **********************************************************************
!     **********************************************************************
!     computation of  curvatures
!     **********************************************************************
!     **********************************************************************

 4010 nck = 0
      nses = 0
!     *************** copy y design to y star ******************************
      if (icont.ge.kreq) then
      istep(kreq,ig2) = iter
      do 6346 ik = 1,igss(ig2)
 6346 write (not,9428)ik,curv(ik,ig2),istep(ik,ig2)
      if(ist.ne.0) write(not,9427)
 9427 format(2x,32('-'))
      do 6347 ik = igss(ig2)+1,kreq
 6347 write (not,9428)ik,curv(ik,ig2),istep(ik,ig2)
 9428 format(i5,3x,1p,e11.3,8x,i5)
      igss(ig2) = icont
      do 5727 i = 1,kreq
 5727 gdgx(i) = curv(i,ig2)/2.d0
      write(not,2030)
      call sphbre(kreq,gdgx,bdp,pf1)
      pf3 = 1.d0-pf1
      call dnormi(bta1,pf3,ier)
      call sptvee(kreq,gdgx,bdp,pf2)
      write(not,2030)
      pf4 = 1.d0-pf2
      call dnormi(bta2,pf4,ier)
      if(bta1.le.-1.5.or.bta2.le.-1.5) then
      write(not,2045) bta1,bta2,pf3,pf4
      else
      write(not,2040) bta1,bta2,pf1,pf2
      endif
      go to 105
      endif
      icont = icont+1
      do 4025 i = 1,nry
 4025 ystar(i,icont,ig2) = ylin(i,ig2)
      call cytox(x,ystar(1,1,ig2),t1,tf,ex,sg,par,bnd,igt,lgp,ids,
     *        ipa,ib,izx)
      call fdirct(x,ystar(1,1,ig2),tp,ex,sg,t3,gam,a,tf,par,
     *  bnd,igt,lgp,ids,ipa,izx,ixz,sdgyu,gdgx,ig4,0)
      iter = 0
      curvold = 10.0d0
      call fstart(y,ylin(1,ig2),epc,cosa(1,1,ig2),ystar(1,1,ig2),
     *            nry,icont,alpha(1,ig2),see)
      do 7001,jj = 1,nry
         yo(jj,ig2) = (y(jj)-ystar(jj,1,ig2))/epc
7001  continue
7000  do 6999,jj = 1,nry
!        y(jj) = ylin(jj,ig2)-yo(jj,ig2)*epc
         y(jj) = ylin(jj,ig2)
6999  continue
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call fdirct(x,y,tp,ex,sg,t3,gam,ao(1,ig2),tf,par,bnd,igt,
     *     lgp,ids,ipa,izx,ixz,sdgyo,gdgx,ig4,0)
      do 6998,jj = 1,nry
!        y(jj) = ylin(jj,ig2)+yo(jj,ig2)*epc/2
         y(jj) = ylin(jj,ig2)+yo(jj,ig2)*epc
6998  continue
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call fdirct(x,y,tp,ex,sg,t3,gam,a,tf,par,bnd,igt,
     *     lgp,ids,ipa,izx,ixz,sdgy,gdgx,ig4,0)
      do 7002,jj = 1,nry
         yoo(jj,ig2) = (ao(jj,ig2)*sdgyo-a(jj)*sdgy)/epc/sdgyu
7002  continue
      ysum = cdot(yoo(1,ig2),yoo(1,ig2),1,1,nry)
      if(ysum.le.1.0d-16) then
        curv(icont,ig2) = 0.0d0
        go to 9000
      endif
      call project1(yoo(1,ig2),cosa(1,1,ig2),ystar(1,1,ig2),nry,icont)
      ysum = cdot(yoo(1,ig2),yoo(1,ig2),1,1,nry)
      do 7015,jj = 1,nry
         yoo(jj,ig2) = yoo(jj,ig2)/sqrt(ysum)
7015  continue
      do 8999,jj = 1,nry
!        y(jj) = ylin(jj,ig2)-yoo(jj,ig2)*epc
         y(jj) = ylin(jj,ig2)
8999  continue
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call fdirct(x,y,tp,ex,sg,t3,gam,ao(1,ig2),tf,par,bnd,igt,
     *     lgp,ids,ipa,izx,ixz,sdgyo,gdgx,ig4,0)
      do 8998,jj = 1,nry
!        y(jj) = ylin(jj,ig2)+yoo(jj,ig2)*epc/2
         y(jj) = ylin(jj,ig2)+yoo(jj,ig2)*epc
8998  continue
      call cytox(x,y,t1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
      call fdirct(x,y,tp,ex,sg,t3,gam,a,tf,par,bnd,igt,
     *     lgp,ids,ipa,izx,ixz,sdgy,gdgx,ig4,0)
      do 8002,jj = 1,nry
         yooo(jj,ig2) = (ao(jj,ig2)*sdgyo-a(jj)*sdgy)/epc/sdgyu
8002  continue
      curv(icont,ig2) = abs(cdot(yooo(1,ig2),yo(1,ig2),1,
     &   1,nry))
      call  project1(yooo(1,ig2),cosa(1,1,ig2),
     &      ystar(1,1,ig2),nry,icont)
!     curv(icont,ig2) = abs(cdot(yooo(1,ig2),yo(1,ig2),1,
!    &   1,nry))
      ysign = 0.0d0
      ymax = 0.0d0
      do 8003,jj = 1,nry
         if(abs(yo(jj,ig2)).gt.ymax) then
           ysign = yoo(jj,ig2)/yo(jj,ig2)
           ymax = abs(yo(jj,ig2))
         endif
 8003 continue
      curv(icont,ig2) = curv(icont,ig2)*sign(1.0d0,ysign)
!     ysum = sqrt(sqrt(cdot(yooo(1,ig2),yooo(1,ig2),1,1,nry)))
      do 8050,jj = 1,nry
         yoo(jj,ig2) = yoo(jj,ig2)*curv(icont,ig2)+yooo(jj,ig2)
         y(jj) = yooo(jj,ig2)
 8050  continue
      call  project1(y,cosa(1,1,ig2),ystar(1,1,ig2),nry,icont)
      ysum = cdot(y,y,1,1,nry)
      do 7005,jj = 1,nry
         yo(jj,ig2) = y(jj)/sqrt(ysum)
 7005 continue
      do 7006,jj = 1,nry
         y(jj) = ystar(jj,icont,ig2)+epc*yo(jj,ig2)
 7006 continue
      if(curvold.ne.0.0d0) then
          err = abs((curv(icont,ig2)-curvold)/curvold)
         else
          err = abs(curv(icont,ig2)-curvold)
      endif
      iter = iter+1
      if(iter.gt.ni1) then
        print *,'warning iteration number in eigenproblem exceeds',
     &        iter
        stop
      endif
      if(err.gt.epa) then
        curvold = curv(icont,ig2)
!       if(int(iter/100)*100.eq.iter) then
!       write(*,'(a,i3,2x,f15.9,2x,i5)') 'curvature  ',icont,
!    &     curv(icont,ig2),iter
!       endif
        go to 7000
      endif
      call fcosine1(cosa(1,1,ig2),yoo(1,ig2),nry,
     &     icont,ystar(1,icont,ig2))
!       write(*,'(a,i3,2x,f15.9,2x,i5,f15.9)') 'curvature  ',icont,
!    &     curv(icont,ig2),iter
9000  istep(icont,ig2) = iter

!     ******* detect case beta*cur less than epb   ************************
      if(bdp*abs(curv(icont,ig2)).lt.epb) then
      istep(icont,ig2) = iter
      do 1846 i = 1,igss(ig2)
 1846 write (not,9428) i,curv(i,ig2),istep(i,ig2)
      if(ist.ne.0) write(not,9427)
      do 1845 i = igss(ig2)+1,icont
 1845 write (not,9428) i,curv(i,ig2),istep(i,ig2)
      igss(ig2) = icont
      do 5725 i = 1,icont
 5725 gdgx(i) = curv(i,ig2)/2.d0
      write(not,2030)
      call sphbre(icont,gdgx,bdp,pf1)
      pf3 = 1.d0-pf1
      call dnormi(bta1,pf3,ier)
      call sptvee(icont,gdgx,bdp,pf2)
      pf4 = 1.d0-pf2
      call dnormi(bta2,pf4,ier)
      if(bta1.le.-1.5.or.bta2.le.-1.5) then
      write(not,2045) bta1,bta2,pf3,pf4
      else
      write(not,2040) bta1,bta2,pf1,pf2
      endif
      go to 105
      endif
      go to 4010
105   beta(ig2) = bdp
      do 554 kk = 1,nry
      y1(kk,ig2) = y(kk)
 554  a1(kk,ig2) = a(kk)
      sed(ig2) = seed
  100 continue

 9438 format (/,2x,' Principal curvature        # steps ' )
 2000 format(/' Limit-state function ',i5)
 2010 format(1x,77('-'))
 2030 format(/39x,'Improved Breitung',6x,7hTvedt's,' EI')
 2040 format(' Generalized reliability index betag  = ',
     *       5x,f9.6,11x,f9.6,
     *     /,' probability                     Pf2 =',
     *       1pe14.7,6x,1pe14.7)
 2045 format(' Generalized reliability index betag  = ',
     *       5x,f9.6,11x,f9.6,
     *     /,' probability                     Pf2 =',' 1- ',
     *       1pe14.7,6x,' 1- ',1pe14.7)
      write(not,2010)

      end subroutine eisocf
