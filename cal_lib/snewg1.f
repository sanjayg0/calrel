ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine snewg1(u,rg1,c,n)
c
c     function: compute the scaling factor.
c
c     last version: nov. 1989, by h.-z. lin
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine snewg1(u,rg1,c,n)

      implicit   none

      integer (kind=4) :: n
      real    (kind=8) :: u,rg1
      real    (kind=8) :: c(n)

      integer (kind=4) :: i
      real    (kind=8) :: u2, ac
      rg1=0.d0
      u2=u*2.d0
      do i=1,n
        ac=c(i)/(1.d0-u2*c(i))
      rg1=rg1+ac*ac
      end do ! i
      rg1=2.d0*rg1+1.d0+1.d0/u/u

      end subroutine snewg1
