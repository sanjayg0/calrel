!cacccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine nmont(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,mc,x1,y,yy,z,
!                      igx,ex,sg,beta)
!
!     functions:
!       compute the failure probability by monte carlo simulation method
!       and using true failure surface. this subroutine is for use in pc.
!
!     inpute arguments:
!       tf   : transformation matrix from z to y, y = tf * z.
!       bnd  : lower and upper bounds of x.
!       par  : distribution parameter of x.
!       tp   : deterministic parameters.
!       ids  : distribution number of x.
!       ib   : codes of distribution bounds of x.
!       mc   : aray of minimun cut sets.
!       ist  : index for the option of restart of simulation.
!       nsm  : number of simulations.
!       cov  : criteria of coefficient of variation.
!       ncs  : number of minimum cut sets.
!       ntl  : total number of elements in the minimum cut sets.
!       igt  : group type
!       lgp  : location of the first element of a group in y.
!       izx  : a vector associated with z indicating the no. of the
!              corresponding x.
!       ipa  : #'s of the associated rv of distribution parameters.
!              n-th element  =  0 means parameter n is constant.
!       ex   : mean of each variable.
!       sg   : standard deviation of each variable.
!
!    working aray:
!      x1,y1,z,igx
!
!    calls: mdris, cytox, nsumpt.
!
!    last version: dec. 2, 1987 by h.-z. lin.
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine nmont(tf,par,bnd,tp,ids,ib,igt,lgp,izx,ipa,
     &                 mc,x1,y1,z,igx,sg,ex,igfx,beta)

      implicit   none

      include   'counts.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'


      integer (kind=4) :: ids(nrx),ib(nrx),igx(ngfc),mc(ntl,2)
      integer (kind=4) :: ipa(4,nrx),izx(nry),lgp(3,nig),igt(nig)
      integer (kind=4) :: igfx(ngf,3)
      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z(nry),bnd(2,nrx)
      real    (kind=8) :: par(4,nrx),tp(*)
      real    (kind=8) :: sg(nrx),ex(nrx)

      integer (kind=4) :: i, i1, idg, ier
      integer (kind=4) :: iexact, igtp, inkk, iopt, itst
      integer (kind=4) :: jjj
      integer (kind=4) :: kip
      integer (kind=4) :: m,mcst,mcend
      integer (kind=4) :: n,n1,nexact
      real    (kind=8) :: beta, btg
      real    (kind=8) :: cum,cum2, cvar, dev
      real    (kind=8) :: exact
      real    (kind=8) :: pf,pff
      real    (kind=8) :: r, rbt, rn,rnm
      real    (kind=8) :: sbet, sp
      real    (kind=8) :: xpp

      neval = 0
      sbet  = 0.0d0    ! rlt  Set to avoid undefined values

!     check whether all subgroups are type 1.

      igtp = 0
      do i = 1,nig
        if(igt(i).ne.1) igtp = 1
      end do ! i
      inkk = 0
      do i = 1,nrx
        if(ids(i).gt.50) then
          if(ib(i).eq.0) x1(i) = 0.0d0
          if(ib(i).eq.1) x1(i) = bnd(1,i)+sg(i)
          if(ib(i).eq.2) x1(i) = bnd(2,i)-sg(i)
          if(ib(i).eq.3) x1(i) = (bnd(1,i)+bnd(2,i))*0.5d0
         else
          x1(i) = ex(i)
        endif
      end do ! i
      if(beta.gt.0.1e-9) then
        write(not,398) beta
 398  format(
     *'  Radius of beta sphere ................rad = ',1p,1e11.3)
        sbet = beta*beta
        inkk = 1
      endif
      mcst = 1
      mcend = ntl
      if(icl.eq.2) then
        if(mcend.ne.2*ngfc) mcend = 2*ngfc
      endif
      if(icl.eq.1) then
        call finim(igfx,igf)
        mcst    = 1
        mcend   = 2
        mc(1,1) = igf
        mc(1,2) = 1
        mc(2,1) = 0
        mc(2,2) = 0
      endif
      if(ist.ne.0) then
        read(ns0,rec = 1,err=10) cum,sbet,i1
        n1 = i1
        go to 30
   10   call cerror(41,0,0.,' ')
      endif
      i1     = 0
      n1     = 0
      exact  = 0.0d0
      iexact = 0
      nexact = 0
      cum    = 0.0d0
      cum2   = 0.0d0
   30 n  = npr
      n1 = n+n1
      i1 = i1+1
      write(not,2010)

!.... perform monte carlo simulation

      iopt =  1
      call gguw(stp,1,iopt,r)
      iopt = 0
      do i = 1,100000
        call gguw(stp,1,iopt,r)
      enddo
      do jjj = i1,nsm
        itst = jjj

!       ----- all are s.i. variables ------

        if(igtp.eq.0) then
          do m = 1,nry
            call gguw(stp,1,iopt,rn)
            iopt  = 0
            y1(m) = rn
          end do ! m
          call crtox(x1,y1,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          go to 499
        endif

!       ----- all are s.i. variables ------

        do m = 1,nry
          call gguw(stp,1,iopt,rn)
          iopt = 0
          r    = rn
          call dnormi(y1(m),r,ier)
        end do ! m
        if(inkk.eq.1) then
          rbt = 0.d0
          do kip = 1,nry
            rbt = rbt+y1(kip)*y1(kip)
          end do ! kip
          if(rbt.le.sbet) go to 55
        endif
        call cytox(x1,y1,z,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)

!       -----

  499   call nsumpt(tp,x1,mc,igx,igfx,idg,mcst,mcend)
        if(idg.eq.1) then
          xpp = 1.d0
        else
          xpp = 0.d0
        endif
        cum  = cum+xpp
        cum2 = cum2+xpp*xpp
        if(iexact.ne.0) then
          nexact = nexact+1
          exact  = exact+cum/real(jjj)
        endif
 55     if(jjj.lt.n1) go to 50
        rnm = real(jjj)
        pf  = cum/rnm
        sp  = cum2-cum*cum/rnm
        sp  = sp/(rnm*(rnm-1))

        pff = 1.d0-pf
        if(sp.gt.0.d0) then
          if(iexact.ne.0) goto 60
          dev  = dsqrt(sp)
          cvar = dev/pf
          call dnormi(btg,pff,ier)
          write(not,2020) jjj,pf,btg,cvar
          if(cvar.le.cov) go to 60
        else
          write(not,2030) jjj
        endif
        n1 = n+n1
  50    continue
      end do ! jjj

  60  continue

      write(ns0,rec=1) cum,sbet,min(jjj,nsm)

!.... output formats

 2010 format('      trials',7x,'pf-mean ',6x,'betag-mean ',4x,
     1       'coef of var of pf')
 2020 format(i12,1p,3x,e14.7,3x,e14.7,3x,e14.7)
 2030 format(i12,5x,'  -----',9x,'  ------',9x,'   ------')

      end subroutine nmont
