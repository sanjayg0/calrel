cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine lodefh(name,na,nr,nc)
c
c   functions:
c      locate and return properties of an integer array if it exists,
c      otherwise define and reserve storage for this array.
c
c   input arguments:
c      name : name of the array.
c      nr : number of rows.
c      nc : number of columns.
c
c   output arguments:
c      na : initial location of array.
c
c   calls: icon, cstop, ifind.
c
c   called by: cinpu, cfosm, csosm, cboun, csens, cdirs, cmont.
c
c   last revision: aug. 14 1987 by pei-ling liu
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine lodefh(name,na,nr,nc)

      implicit   none

      include   'blkrel1.h'

      character (len=1) :: name(4)
      integer (kind=4) :: iname(4)
      integer (kind=4) :: na,nr,nc

      integer (kind=4) :: i, ifind

      save
c-----locate array ----------------------------------
      na = 0
      call icon(name,iname)
      i = ifind(iname,0)
      if(i.ne.0) go to 800
      np=3
      call defin(name,na,nr,nc)
      return
c-----return array properties -----------------------
  800 na = ia(i+7)
c     nr = ia(i+4)
c     nc = ia(i+5)
c     np = ia(i+6)
      if(nr.ne.ia(i+4).or.nc.ne.ia(i+5)) call chgdim(name,nr,nc)

      end
