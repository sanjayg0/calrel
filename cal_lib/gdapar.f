cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine gdapar(x,y,tp,ex,sg,dgx,gam,a,tf,par,igt,lgp,ids,ipa,
c                        izx,ixz,sdgy)
c
c   functions:
c      compute d(dg/dy)/d(par).
c
c   input arguments:
c      x : basic random variables in the original space.
c      y : basic variables in the standard space.
c      tp : deterministic parameters in performance function.
c      ex : mean values of x.
c      sg : standard deviations of x.
c      tf : the transformation matrix from z to y, i.e. y = tf * z.
c      par : distribution parameters of x.
c      igt : group types.
c      lgp : location of the first element of a group in y.
c      ids : distribution numbers of x.
c      ipa : #'s of the associated rv of distribution parameters.
c             n-th element of ipa = 0 means parameter n is constant.
c      izx : indicators for z to x.
c      ixz : indicators for x to z.
c      ig : performance function number.
c   output arguments:
c      dgx : dg/dx.
c      gam : unit normal vector in the z space.
c      a : unit normal vector in the y space.
c      sdgy : |dg/dy|.
c
c   calls: udgx, cdot.
c
c   called by: gsens
c
c   last revision: dec. 5, 1987 by hong zong lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gdapar(x,y,ex,sg,a,sbt,dgx,t,tf0,par,igt,lgp,ids,
     *                  ipa,izx,ixz,dadp,tf1,bnd,dxz,ig)

      implicit   none

      include   'prob.h'

      integer (kind=4) :: ig
      real    (kind=8) :: sbt 
      integer (kind=4) :: ipa(4,nrx),lgp(3,nig),igt(nig),izx(nry)
      integer (kind=4) :: ids(nrx),ixz(nrx)
      real    (kind=8) :: x(nrx),y(nry),ex(nrx),sg(nrx),dgx(nrx),a(nry)
      real    (kind=8) :: t(6,nrx),tf0(*),par(4,nrx)
      real    (kind=8) :: dadp(nry,4,nrx),tf1(ntf),dxz(nrx),bnd(2,nrx)

      integer (kind=4) :: i,j,k, ii,iz, j0,j1,j1s
      integer (kind=4) :: nge,nges,n1,nn,nn0,nn1
      real    (kind=8) :: abc,tp
      save
c
      do 90 j=1,nrx
      do 90 i=1,4
      do 91 k=1,nry
  91  dadp(k,i,j)=0.d0
 90   t(i,j)=0.
c     print *,'dgx=',(dgx(kp),kp=1,nrx)
c     compute dg/dx
      do 10 i=1,nry
      j=izx(i)
   10 t(5,i)=dgx(j)
        do 390 j=1,nrx
  390   t(6,j)=dgx(j)
c.....compute dg/dy/dpar for non-random variables.
        if(nrx.eq.nry) go to 105
        do 202 i=1,nrx
        if(ids(i).ne.0) go to 202
        call gdadp0(dadp(1,1,i),t,dgx,x,y,tp,ex,sg,tf1,par,bnd,igt,
     *       lgp,ids,ipa,izx,ixz,dxz,ig,i)
 202    continue

c
c---  loop over groups
c
 105  nn=1
      nn0=0
      do 100 ii=1,nig
      j0=lgp(1,ii)
      if(ii.eq.nig) then
      j1=nry
      else
      j1=lgp(1,ii+1)-1
      endif
      nge=j1-j0+1
      if(nge.le.0) go to 100
      nn1=nn-1+nge*nge
c
c---  type 1 groups
c
      if(igt(ii).eq.1) then
c---  compute d(dx/dz)/d(par).
        call gdxzp(x,t(1,j0),par,ids,izx(j0),ex,sg,nge)
c     compute d(dg/dy)/d(par) = dg/dx * d(dx/dz)/d(par)
        do 20 i=j0,j1
        iz=izx(i)
        do 25 k=1,4
        dadp(i,k,iz)=t(5,i)*t(k,i)
  25    continue
   20   continue
c
c---  type 2 groups
c
      else if(igt(ii).eq.2) then
c---  compute d(dx/dz)/d(par).
        call gdxzp(x,t(1,j0),par,ids,izx(j0),ex,sg,nge)
c---  compute  dg/dx * d(dx/dz)/d(par).
      do 200 i=j0,j1
      do 235 k=1,4
      t(k,i)=t(5,i)*t(k,i)
  235 continue
  200 continue
c---  compute  dg/dy=dg/dx * d(dx/dz)/d(par) * dz/dy (=inv tf)
      call gdgyp1(dadp,t(1,j0),tf1(nn),izx(j0),nge,nge,j0,0)
c
c---  type 3 groups
c
      else
        j1s=lgp(2,ii)
        nges=j1s-j0+1
      n1=nn-1
      do 299 i=1,nge*nge
 299  tf1(n1+i)=tf0(nn0+i)
c     print *,'dxz=',(dxz(ka),ka=j0,j0+nges-1)
c     print *,'tf1'
c     call mprint(tf1,nge,nge,1)
c --- compute inverse tf
      if(nges.gt.1)  call cinver(tf1,nges,nge)
c     if(nges.eq.0) then
c     print *,'tf'
c     call mprint(tf1(nn),nge,nge,1)
c     endif
      do 395 i=j0+nges,j1
      k=izx(i)
 395  call cparvl(x,ex(k),sg(k),par(1,k),bnd(1,k),ids(k),ipa(1,k),1)
      call gcom1(t,x,y,ex,sg,dgx,tf0(nn0+1),tf1(nn),par,bnd,ids,ipa,
     *      igt,izx,ixz,dadp,dxz,nge,nges,j0,j1,ii)
        nn0=nn0+nge*nge
        endif
      nn=nn1+1
  100 continue
      do 110 i=1,nrx
 110  dgx(i)=t(6,i)
c     compute d(alpha)/d(par)
      do 900 i=1,nrx
      do 910 j=1,4
      abc=0.d0
      do 912 k=1,nry
  912 abc=abc+a(k)*dadp(k,j,i)
      do 913 k=1,nry
  913 dadp(k,j,i)=(-dadp(k,j,i)+abc*a(k))/sbt
  910 continue
  900 continue
c     jkp=1
c     if(jkp.eq.1) stop

      end subroutine gdapar
