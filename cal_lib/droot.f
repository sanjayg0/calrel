      subroutine droot(beta,alph,mc,x,y,ig,root,btg1,igfx,indx
     *                ,mcst,mcend)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'
      include   'simu.h'

      integer (kind=4) :: indx,mcst,mcend
      real    (kind=8) :: x(nry),y(nry),beta(ngf),alph(nry,ngf)
      integer (kind=4) :: ig(ngfc),mc(ntl,2),igfx(ngf,3)
      real    (kind=8) :: btg1(ngf)

      integer (kind=4) :: i,ii,ij,ik,ik2,i1,i2, imm, ic,idg,ier
      integer (kind=4) :: j,jj,jj2,jk,jk2
      real    (kind=8) :: a,b,c,e
      real    (kind=8) :: pch
      real    (kind=8) :: rho, rn, root,root1
      real    (kind=8) :: sx,sy
      real    (kind=8) :: tmp

      real    (kind=8) :: cdot 

      save

      rn=real(nry)
      root=30.d0
c
      if(icl.le.2) then
c       ---- for seriese sys or comp porb. -----
        if(indx.eq.1) then
c         ----- minimum beta is root -----
          do 95 i=1,ngfc
            i2=igfx(i,2)
            if(abs(beta(i2)).le.root) root=beta(i2)
  95      continue
        else
          do 195 i=1,ngfc
            i2=igfx(i,2)
            if(abs(btg1(i2)).le.root) root=btg1(i2)
 195      continue
        endif
        if(dabs(root).le.2.d0) root=2.d0
      else
c       ----- for general system -----
        root1=0.d0
        do 96 i=mcst,mcend
          if(mc(i,1).ne.0) then
            i1=i+1
            ik=mc(i,2)
            ik2=igfx(ik,2)
            do 196 j=i1,mcend
              if(mc(j,1).eq.0) go to 96
              jk=mc(j,2)
              jk2=igfx(jk,2)
              rho=cdot(alph(1,ik2),alph(1,jk2),1,1,nry)
c             -----compute the threshold for the root.
              if(dabs(rho).eq.1.0d0) go to 100
              a=1.d0-rho*rho
              c=beta(ik2)-rho*beta(jk2)
              b=c/dsqrt(a)
              e=beta(jk2)*beta(jk2)+b*b
              e=dsqrt(e)
              sx=0.d0
              do 97 jj=1,nry
                x(jj)=alph(jj,ik2)-alph(jj,jk2)*rho
                sx=sx+x(jj)*x(jj)
  97          continue
              sx=dsqrt(sx)
              a=b/sx
              do 98 jj=1,nry
                y(jj)=beta(jk2)*alph(jj,jk2)+a*x(jj)
  98          continue
              go to 103
100           if(rho.eq.1.0d0) then
                tmp=(beta(ik2)+beta(jk2))/2
              else
                tmp=(beta(ik2)-beta(jk2))/2
              endif
              do 101,jj=1,nry
                y(jj)=tmp*alph(jj,ik2)
101           continue
              e=max(dabs(beta(ik2)),dabs(beta(jk2)))
c.....check this point is in the failure domain or not.
103           do 1098 jj=1,ngfc
                sy=0.d0
                jj2=igfx(jj,2)
                ig(jj)=1
                if(jj.eq.ik.or.jj.eq.jk) then
                  ig(jj)=0
                else
                  do 99 ii=1,nry
  99              sy=sy+y(ii)*alph(ii,jj2)
                  if(sy.ge.beta(jj2)) ig(jj)=0
                endif
 1098         continue
              imm=1
              idg=1
              do 1100 ij=mcst,mcend
                if(mc(ij,1).ne.0) then
                  ic=mc(ij,2)
                  if(mc(ij,1).lt.0) then
                    imm=imm*ig(ic)
                  else
                    imm=imm*(1-ig(ic))
                  endif
                else
                  idg=idg*(1-imm)
                  imm=1
                endif
 1100         continue
c.....the root is in the failure domain.
              if(idg.eq.0.and.e.ge.root1) root1=e
 196        continue
          else
            if(root1.le.root) root=root1
            root1=0.d0
          endif
   96   continue
      endif
c
      call dnorm(-root,pch)
      if(icl.le.2) then
        pch=0.0005d0*pch
      else
        pch=0.0001d0*pch
      endif
      pch=1.d0-pch
      call dchisi(root,rn,pch,ier)
      root =dsqrt(root)
c     write(not,398) root
c398  format(
c    *' Threshold for roots ................root=',1p,1e11.3)

      end subroutine droot
