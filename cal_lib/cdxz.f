cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cdxz(x,dxz,par,ids,izx,nge)
c
c   functions:
c      compute dx/dz, which are used to transform dg/dx to dg/dy.
c
c   input arguments:
c      x : basic random variables in the original space.
c      par : distribution parameters of x.
c      ids : distribution numbers of x.
c      izx : indicators for z to x.
c      nge : number of z in the group.
c
c   output arguments:
c      dxz : dx/dz.
c
c   calls: udd, mdgam, dbeta, dnormi.
c
c   called by: fdirct.
c
c   last revision: aug. 3 1990 by h. z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdxz(x,dxz,par,ids,izx,nge)

      implicit   none

      include   'prob.h'

      integer    nge
      real*8     x(nrx),dxz(nge), par(4,nrx)
      integer    ids(nrx),izx(nge)

      integer    i,k,ib,id, ier
      real*8     ap1,ap2, ba,bet, c, dx, fai
      real*8     prob, pd,pp,pp1,pp2
      real*8     sg, xx, xt,xt1,xtu, y,y1
      real*8     bnd(2)

      save

      do k = 1,nge
        i   = izx(k)
        id  = ids(i)
        pp1 = par(1,i)
        pp2 = par(2,i)
        xx  = x(i)

c---    switch on type of distribution

        if(id.ge.51) go to 51

        go to (1,2,3,4,5,6,7,100,100,100,11,12,13,14),id
        write(*,*) 'ID =',id
        return

c--     normal

    1   dxz(k) = pp2
        go to 100

c--     lognormal

    2   dxz(k) = pp2*xx
        go to 100

c--     gamma

    3   xt = pp1*xx
        c  = pp2-1.d0
        pd = pp1*xt**c*exp(-xt)/gamma(pp2)
        call mdgam(xt,pp2,prob,ier)
        go to 60

c--     exponential

    4   xt   = exp(-pp1*(xx-pp2))
        prob = 1.0-xt
        pd   = pp1*xt
        go to 60

c--     rayleigh

    5   dx   = (xx-pp2)/pp1
        xt   = exp(-dx*dx/2.)
        prob = 1.0-xt
        pd   = dx*xt/pp1
        go to 60

c--     uniform

    6   ba   = pp2-pp1
        prob = (xx-pp1)/ba
        pd   = 1.0d0/ba
        go to 60

c--     beta

    7   ba  = par(4,i)-par(3,i)
        xt  = (xx-par(3,i))/ba
        call dbeta(xt,pp1,pp2,prob,ier)
        bet = gamma(pp1)*gamma(pp2)/gamma(pp1+pp2)
        xtu = (par(4,i)-xx)/ba
        ap1 = xt**(pp1-1.)
        ap2 = xtu**pp2
        pd  = ap1/(par(4,i)-xx)*ap2/bet
        go to 60

c--     type i largest

   11   xt   = -pp2*(xx-pp1)
        xt   = -exp(xt)
        prob =  exp(xt)
        pd   = -prob*pp2*xt
        go to 60

c--     type i smallest

   12   xt   =  pp2*(xx-pp1)
        xt   = -exp(xt)
        pp   =  exp(xt)
        prob =  1.d0-pp
        pd   = -pp*pp2*xt
        go to 60

c--     type ii largest

   13   xt   = pp1/xx
        xt1  = xt**pp2
        prob = exp(-xt1)
        pd   = pp2/pp1*xt1*xt*prob
        go to 60

c--   type iii smallest

   14   xt   = xx/pp1
        xt1  = xt**pp2
        prob = 1.0-exp(-xt1)
        pd   = pp2/pp1*xt1/xt*(1.0-prob)
        go to 60

c--     user-defined distribution

   51   call udd(x,par(1,i),sg,id,prob,pd,bnd,ib)

   60   call dnormi(y,prob,ier)
        y1     = -0.5d0*y*y
        fai    =  exp(y1)/2.506628275d0
        dxz(k) =  fai/pd

  100   continue

      end do ! k

      end subroutine cdxz
