!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine gpfij(pij,bti,btj,blij,bpti,bptj)
!
!   functions:
!     use gaussian quadrature formula to calculate the integration of d(normal
!     densitiy)/d(par).
!
!   input arguments:
!     bti : reliability index of transition state i.
!     btj : reliability index of transition state j.
!     blij :cos vij.
!     bpti : d(beta(i))/d(par).
!     bptj : d(beta(j))/d(par).
!
!   output:
!     pij : failure probability pij.
!
!
!   last revision: dec. 5, 1987 by h.-z. lin.
!
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gpfij(pij,bti,btj,blij,bpti,bptj)

      implicit   none

!-----gauss quadrature method .
!     use 1 10 points gauss quadrature

      real    (kind=8) :: pij,bti,btj,blij,bpti,bptj

      integer (kind=4) :: i,k
      real    (kind=8) :: pai2, r0,r1,r2,r3,r4
      real    (kind=8) :: s2,s3,s4,s5,s6, z
      real    (kind=8) :: x(10),w(10)
!
!------ x(k) : abscissas of  quadrature
!
      data (x(k),k=1,10) /
     1 -0.97390652851717172 ,
     2 -0.86506336668898451 ,
     3 -0.67940956829902440 ,
     4 -0.43339539412924719 ,
     5 -0.14887433898163121 ,
     6  0.14887433898163121 ,
     7  0.43339539412924719 ,
     8  0.67940956829902440 ,
     9  0.86506336668898451 ,
     *  0.97390652851717172 /
!
!----- w(k) : weights  quadrature
!
      data (w(k),k=1,10) /
     1   0.06667134430868813759  ,
     2   0.14945134915058059314  ,
     3   0.21908636251598204399  ,
     4   0.26926671930999635509  ,
     5   0.29552422471475287017  ,
     6   0.29552422471475287017  ,
     7   0.26926671930999635509  ,
     8   0.21908636251598204399  ,
     9   0.14945134915058059314  ,
     *   0.06667134430868813759  /
      data pai2 /0.1591549430919/
!
!     pai = dacos(-1.d0)
      r0 = bti*bti+btj*btj
      r1 = blij/2.d0
      r2 = bti*btj*2.d0
      r3 = bti*bpti+btj*bptj
      r4 = btj*bpti+bti*bptj
      pij = 0.d0
      do i = 1,10
        z = (x(i)+1.d0)*r1
        s2 = 1.d0-z*z
!       s3 = -1.d0/pai/2.d0*sqrt(s2*s2*s2)
        s3 = -pai2/sqrt(s2*s2*s2)
        s4 = r2*z
        s5 = -(r0-s4)/s2/2.d0
        s6 = s3*exp(s5)
        s6 = s6*(r3-z*r4)
        pij = pij+s6*w(i)
      end do ! i
!     call dnorm(-bti,pfi)
!     call dnorm(-btj,pfj)
!     pij = pij*blij/2.d0+pfi*pfj
      pij = pij*blij/2.d0

      end subroutine gpfij
