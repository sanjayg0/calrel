      subroutine freei(ch,idata,num)

      implicit   none

      include   'file.h'
      include   'line.h'

      character (len=1) :: ch, ic,blk,comma,i0,eqs,neg,colin,lne

      integer (kind=4) :: num 
      integer (kind=4) :: idata(80)

      integer (kind=4) :: i,j, isign, nn

      save

      data      blk   /' '/
      data      comma /','/
      data      i0    /'0'/
      data      eqs   /'='/
      data      neg   /'-'/
      data      colin /':'/

c     Make sure CH is upper case

      i = ichar(ch)
      if(i.ge.97) then
        ic = char(i - 32)
      else
        ic = ch
      endif
      
c-----find integer string ---------------------------

      i = 0
      if(ic.eq.blk) go to 200
      do i = 1,llen
        if((line(i).eq.ic).and.(line(i+1).eq.eqs)) go to 200
      end do ! i
      return

c-----zero integer string ---------------------------

  200 do j=1,num
        idata(j)=0
      end do ! j
      if(line(i+1).eq.eqs) i=i+1
      do j=1,num
        isign = 1

c-----  skip blanks between integers ------------------

  215   if(line(i+1).ne.blk) go to 220
        i = i + 1
        if(i.gt.llen) go to 900
        go to 215
  220   i = i + 1
        if(i.gt.llen) go to 230

c-----  check for sign --------------------------------

        lne = line(i)
        if(lne.ne.neg) go to 225
        isign = -1
        go to 220

c-----  extract integer -------------------------------

  225   if(lne.eq.blk) go to 230
        if(lne.eq.comma) go to 230
        if(lne.eq.colin) go to 230
        nn = ichar( lne ) - ichar( i0 )
        if((nn.lt.0).or.(nn.gt.9)) go to 900
        idata(j) = 10*idata(j) + nn
        go to 220
c-----  set sign ---- --------------------------------
  230   idata(j) = idata(j)*isign
      end do ! j

  900 continue

      end
