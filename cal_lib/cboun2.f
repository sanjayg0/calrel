cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cboun2(x,bnd,ib,sg)
c
c   input arguments:
c      x : x computed by inverse transformation from y.
c      ib : code of distribution bounds of x.
c            =  0 , no bounds
c            =  1 , bounded from below.
c            =  2 , bounded from above.
c            =  3 , both sides bounded.
c      bnd : lower and upper bound of x.
c
c   output arguments:
c      x : updated x.
c
c   called by: cztox.
c
c   last revision: may 8, 1989 by h. z. lin.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cboun2(x2,bnd,ib,sg,pr,f2)

      implicit   none

      integer    ib
      real*8     x2, sg,pr,f2
      real*8     bnd(2)

      go to (60,30,40,50) ib + 1

   30 x2 = bnd(1) + (x2 + sg)*0.5d0
      return
   40 x2 = bnd(2) + (x2 - sg)*0.5d0
      return
   50 if(abs(x2 - bnd(1)).lt.abs(x2 - bnd(2))) then
        x2 = bnd(1) + (x2 + sg)*0.5d0
      else
        x2 = bnd(2) + (x2 - sg)*0.5d0
      end if
      return
  60  if(pr.gt.f2) then
      x2 = x2 + sg
      else
      x2 = x2 - sg
      endif

      end subroutine cboun2
