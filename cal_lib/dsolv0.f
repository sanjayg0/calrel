ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     subroutine dsolv0(tp,y1,x1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
c    *                 ib,izx,rr,y2,gv,ir,root,gtor)
c
c     functions:
c       solve the roots of the intersection of second order failure
c        surface and simulated vectors.(for point fitting method)
c
c     input arguments:
c       ngf  : number of performance functions.
c       rt   : rotational transformation matrices, y = rt*y'.
c       bt   : reliability indexes.
c       y    : simulated vector.
c       cur  : curvatures for each semiparabolic intersection.
c       nry   : no. of variables in y space.
c
c     output arguments:
c       rr   : roots of intersection of 2nd order surface and simulated
c              vector.
c       ind  : index of how many roots for each mode, (1-4 roots)
c       a1   : coef. of 2nd order terms in the 2nd order surface.
c       a2   : coef. of 1st order terms in the 2nd order surface.

      subroutine dsolv0(tp,y1,x1,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,
     *                 ib,izx,rr,y2,gv,ir,root,gtor,igfx)

      implicit   none

      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      real    (kind=8) :: root
      real    (kind=8) :: tf(ntf),x1(nrx),y1(nry),z1(nry),bnd(2,nrx)
      real    (kind=8) :: par(4,nrx),tp(*),sg(nrx),ex(nrx)
      integer (kind=4) :: ids(nrx),ib(nrx)
      real    (kind=8) :: rr(3,ngfc),y2(nry)
      integer (kind=4) :: ipa(4,nrx),izx(nry),lgp(3,nig),igt(nig)
      real    (kind=8) :: gv(ngf),gtor(ngf)
      integer (kind=4) :: ir(ngfc),igfx(ngf,3)

      integer (kind=4) :: i,idn,idx1,idx2,ikh,irr
      integer (kind=4) :: k,k2,k4,kp
      integer (kind=4) :: ll
      integer (kind=4) :: nro
      real    (kind=8) :: g1,g2,g3, gb, gg2
      real    (kind=8) :: roo,r1,r2,r3,r4, rb,rh, rr2

      roo = root/3.d0
      nro = int(roo)
      do 301 k = 1,ngfc
        k2 = igfx(k,2)
        k4 = igfx(k,3)
        irr = 0
        kp = 1
        g1 = gv(k)
        if(dabs(g1).le.gtor(k2)) then
          irr = irr+1
          rr(irr,k) = 0.d0
          idn = 0
        else
          idn = 1
          r1 = 0.d0
          if(g1.lt.0.d0) then
            idx1 = 1
          else
            idx1 = 0
          endif
        endif
        ikh = 0
        do 350 ll = 1,nro+2
          r2 = roo*real(ll)
          if(r2.lt.root) go to 23
          if(ikh.eq.0) then
            r2 = root
            ikh = 1
          else
            go to 360
          endif
 23       do i = 1,nry
            y2(i) = y1(i)*r2
          end do ! i
c         print *,'r2 = ',r2
          call cytox(x1,y2,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          call ugfun(g2,x1,tp,k4)
c         print *,'g2 = ',g2
          gg2 = g2
          rr2 = r2
          if(dabs(g2).le.gtor(k2)) then
            irr = irr+1
            rr(irr,k) = r2
            g1 = g2
            r1 = r2
            idn = 0
            go to 350
c           ---- goto to find next root lager than r2
          endif
          if(g2.lt.0.d0) then
            idx2 = 1
          else
            idx2 = 0
          endif
          if(idn.eq.0) then
            g1 = g2
            r1 = r2
            idx1 = idx2
            idn = 1
            go to 350
          endif
          if(idx1.eq.idx2) then
            g1 = g2
            r1 = r2
            go to 350
          endif
          kp = 2
          rb = r1
          gb = g1
          rh = r2
 201      continue
          r3 = r1-g1*(r2-r1)/(g2-g1)
          if(r3.le.0..or.r3.ge.root) then
            r3 = (rb+rh)*0.5d0
          endif
          do 20 i = 1,nry
 20       y2(i) = y1(i)*r3
          call cytox(x1,y2,z1,tf,ex,sg,par,bnd,igt,lgp,ids,ipa,ib,izx)
          kp = kp+1
          call ugfun(g3,x1,tp,k4)
          if(g3*gb.lt.0.) then
            if(rh.gt.r3) rh = r3
          else
            if(rb.lt.r3) rb = r3
          endif
          if(dabs(g3).le.gtor(k2).or.kp.ge.100) then
            irr = irr+1
            r4 = r2-g2*(r3-r2)/(g3-g2)
            if(kp.ge.100.and.(r4.le.rb.or.r4.ge.rh)) r4 = (rb+rh)*0.5d0
c            if(r4.le.rb.or.r4.ge.rh) r4 = (rb+rh)/2.d0
            rr(irr,k) = r4
            g1 = gg2
            r1 = rr2
            idx1 = idx2
            idn = 1
            if(kp.ge.100) write(not,309)
            goto 360
c            if(icl.le.2) go to 360
c            go to 350
          endif
          g1 = g2
          r1 = r2
          r2 = r3
          g2 = g3
          go to 201
 350      continue
 360    ir(k) = irr
 301  continue

 309  format('  warning: root is not solved within 100 steps')

      end subroutine dsolv0
