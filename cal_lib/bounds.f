!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     subroutine bounds(beta,alph,rhom,prob,prbr,pl1,pl2,
!    &                  pl3,next,ibx,ilub)

!   functions:
!     compute unimodal and bimodal bounds of system failure probability.

!   input arguments:
!     beta : reliability indices of all failure modes.
!     alph : unit normal vectors at the design points in the y space.
!     ibt  :  = 1, unibomal bound only.
!             = 2, relax bimodal bound only.
!             = 3, bimodal bound only.
!             = 0, unimodal, relax bimodal and bimodal bounds.

!   output arguments:
!     rhom : modal correlation matrix.
!     prob : bounds on joint modal failure probabilities.
!         upper triangle - upperbounds stored in .
!         lower triangle - lowerbounds stored in .
!         diagonal - individual mode probabilities:
!     prbr: joint modal failure probabilities calculated by numerical
!            integration.
!     pl1, pu1 : unimodal bounds of system failure probability.
!     pl2, pu2 : bimodal bounds of system failure probability.
!     pl3, pu3 : bimodal bounds of system failure probability using true pfij.

!   calls: bsort, dnorm, bpfij.

!   called by: cboun

!   last revision: jan. 8, 1990 by hong zong lin

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine bounds(beta,alph,rhom,prob,prbr,next,ibx,ilub,igfx)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'flag.h'
      include   'prob.h'

      real    (kind=8) ::  beta(ngf),alph(nry,ngf),rhom(ngfc,ngfc)
      real    (kind=8) ::  prbr(ngfc,ngfc),prob(ngfc,ngfc)
      integer (kind=4) ::  next(ngfc),ibx(ngfc-1,2)
      integer (kind=4) ::  ilub(ngfc-1,ngfc-1),igfx(ngf,3)

      integer (kind=4) :: i, id, ii, im1,ip1
      integer (kind=4) :: j, jj
      integer (kind=4) :: ki,kj
      real    (kind=8) :: bt,btij
      real    (kind=8) :: pp,pij, p1,p2, pl1,pl2,pl3, pu1n, pf,px
      real    (kind=8) :: r

      real    (kind=8) :: cdot  ! External function

!     save

!---  index failure modes in order of decreasing failure probability

      id = 0
      if(icl.ne.2) return
      do jj = 1,2
        do ii = 1,ngfc-1
          ibx(ii,jj) = 0
        end do ! ii
      end do ! jj
      do ii = 1,ngfc-1
        do jj = 1,ii
          ilub(ii,jj) = 0
        end do ! jj
      end do ! ii

      if(itt.eq.1) go to 30

      call bsort(beta,next,gmin,gmax,igfx)

!---  compute modal correlation coefficient matrix

      do i = 1,ngfc
        ki = igfx(i,2)
        bt = -beta(ki)
        call dnorm(bt,pf)
        prob(i,i) = pf
      end do ! i


      do i = 1,ngfc-1
        ki = igfx(i,2)
        ip1 = i+1
        rhom(i,i) = 1.d0
        do j = ip1,ngfc
          kj = igfx(j,2)
          rhom(i,j) = cdot(alph(1,ki),alph(1,kj),1,1,nry)
          if(rhom(i,j).lt.0.0d0) id = 1
          rhom(j,i) = rhom(i,j)
        end do ! j
      end do ! i
      rhom(ngfc,ngfc) = 1.0d0

!---  switch to required routine

   30 if(ibt.eq.0) go to 100

      go to(100,200,300) ibt

!---  compute unimodal bounds

  100 pl1 = prob(gmin,gmin)
      pp = 1.d0
      do i = 1,ngfc
        pp = pp*(1.d0 - prob(i,i))
      end do ! i
      pu1  = 1.d0 - pp
      pu1n = 0.d0
      do i = 1,ngfc
        pu1n = pu1n + prob(i,i)
      end do ! i
      if(id.eq.1) pu1 = pu1n
      if(ibt.eq.1) go to 999

!---  compute relaxed bimodal bounds

  200 if(itt.eq.1) go to 240

!---  compute bounds on joint modal failure probabilities
!---  upperbounds stored in upper triangle of prob(i,j)
!---  lowerbounds stored in lower triangle of prob(i,j)
!---  individual mode probabilities are stored along the diagonal

      do i = 1,ngfc-1
        ki  = igfx(i,2)
        ip1 = i+1
        do j = ip1,ngfc
          kj = igfx(j,2)
          r  = dsqrt(1.d0 - rhom(i,j)*rhom(i,j))
          if(r.le.0.1e-12) then
            p1   = 0.5d0*prob(j,j)
            p2   = 0.5d0*prob(i,i)
          else
            btij = -(beta(ki)-beta(kj)*rhom(i,j))/r
            call dnorm(btij,p1)
            p1 = p1*prob(j,j)
            btij = -(beta(kj)-beta(ki)*rhom(i,j))/r
            call dnorm(btij,p2)
            p2   = p2*prob(i,i)
          endif

          if(rhom(i,j).ge.0.d0) then

            prob(i,j) = p1+p2
            if(rhom(i,j).le.0.1e-13)prob(i,j) = p1
            prob(j,i)   = p1
            ilub(j-1,i) = 2
            if(p2.gt.p1) then
              prob(j,i)   = p2
              ilub(j-1,i) = 1
            endif

          else
            prob(i,j) = p1
            if(p2.lt.p1) prob(i,j) = p2
            prob(j,i) = 0.d0
          endif

        end do ! j
      end do ! i

!---   compute relax bimodal bounds on system failure probability
!---        ****   l-o-w-e-r  b-o-u-n-d    ****

  240 ii = gmin
      pl2 = prob(ii,ii)
      do i = 2,ngfc
        ii  = next(ii)
        im1 = i-1
        pp  = 0.d0
        jj  = gmin
        do j = 1,im1
          px = prob(jj,ii)
          if(jj.gt.ii) px = prob(ii,jj)
          pp = pp+px
          jj = next(jj)
        end do ! j
        pp = prob(ii,ii) - pp
        if(pp.lt.0.d0) pp = 0.d0
        pl2 = pl2 + pp
      end do ! i

!---      ****   u-p-p-e-r   b-o-u-n-d    ****

      ii  = gmin
      pu2 = prob(ii,ii)
      do i = 2,ngfc
        ii = next(ii)
        pp = 0.d0
        im1 = i-1
        jj = gmin
        do j = 1,im1
          px = prob(ii,jj)
          if(ii.lt.jj) px = prob(jj,ii)
          if(px.gt.pp) then
            pp = px
            ibx(im1,1) = jj
          endif
          jj = next(jj)
        end do ! j
        pu2 = pu2+prob(ii,ii)-pp
      end do ! i

      if(ibt.eq.2) go to 999

!---  compute joint-modal probabilities by numerical integration.

  300 do i = 1,ngfc
        prbr(i,i) = prob(i,i)
      end do ! i

      do i = 1,ngfc-1
        ki = igfx(i,2)
        ip1 = i+1
        do j = ip1,ngfc
          kj = igfx(j,2)
          call bpfij(pij,beta(ki),beta(kj),rhom(i,j))
          prbr(i,j) = pij
          prbr(j,i) = pij
        end do ! j
      end do ! i

!---  compute  bimodal bounds on system failure probability
!---        ****   l-o-w-e-r  b-o-u-n-d    ****

      ii = gmin
      pl3 = prbr(ii,ii)
      do i = 2,ngfc
        ii = next(ii)
        im1 = i - 1
        pp  = 0.d0
        jj  = gmin
        do j = 1,im1
          px = prbr(jj,ii)
          pp = pp + px
          jj = next(jj)
        end do ! j
        pp = prbr(ii,ii) - pp
        if(pp.lt.0.d0) pp = 0.d0
        pl3 = pl3 + pp
      end do ! i

!---        ****   u-p-p-e-r   b-o-u-n-d    ****

      ii  = gmin
      pu3 = prbr(ii,ii)
      do i = 2,ngfc
        ii  = next(ii)
        pp  = 0.d0
        im1 = i-1
        jj  = gmin
        do j = 1,im1
          px = prbr(ii,jj)
          if(px.gt.pp) then
            pp         = px
            ibx(im1,2) = jj
          endif
          jj = next(jj)
        end do ! j
        pu3 = pu3 + prbr(ii,ii) - pp
      end do ! i

  999 call bsyspr(rhom,prob,prbr,pl1,pl2,pl3,igfx(1,3))

      end subroutine bounds
