cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      function ifind(iname,lun)
c
c   functions:
c      find array location.
c
c   input arguments:
c      iname : integer data corresponding to 'name'.
c      lun : incore/outcore flag.
c
c   called by: chgdim, delete, locate.
c
c   last revision: by wilson
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      integer function ifind(iname,lun)

      implicit    none

      include   'blkrel1.h'
      include   'dbsy.h'

      integer  (kind=4) :: iname(4), lun
      character (len=1) :: ch1,ch2,ch3,ch4

      integer (kind=4) :: i,n
      save

      i = idir
      do n = 1,numa
        if(lun.ne.ia(i+9)) go to 100
        write(ch1,'(a1)') ia(i)
        write(ch2,'(a1)') ia(i+1)
        write(ch3,'(a1)') ia(i+2)
        write(ch4,'(a1)') ia(i+3)
        if (iname(1).ne.ia(i  )) go to 100
        if (iname(2).ne.ia(i+1)) go to 100
        if (iname(3).ne.ia(i+2)) go to 100
        if (iname(4).eq.ia(i+3)) go to 200
  100   i = i + 10
      end do ! n
      i = 0
  200 ifind = i

      end function ifind
