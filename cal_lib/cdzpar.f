cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      subroutine cdzpar(x,z,ex,sg,par,ids,s,k)
c
c   functions:
c      compute d(z)/d(par).
c
c   input arguments:
c      x : basic random variable in the original space.
c      z : random variable obtained from phi(z) = fx(x).
c      ex : mean of x.
c      sg : standard deviation of x.
c      par : distribution parameters of x.
c      ids : distribution number of x.
c
c   output arguments:
c      s : [ dz/d(ex), dz/d(sg), dz/d(par1), ...,dz/d(par4) ].
c
c   calls: gamma, dchis, dbeta, udd.
c
c   called by: fsenvd, cdyx.
c
c   last revision: feb 23, 20009 by k.-j. hong
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cdzpar(x,z,ex,sg,par,ids,s,k)

      implicit   none

      include   'prob.h'

      integer    ids,k
      real*8     z,ex,sg
      real*8     x(nrx),par(4),s(6),bnd(2)

      integer    i, ib, ier
      real*8     a,b, ba, d, det, dr,ds1,ds2, dx,dxp,du1,du2, e
      real*8     f,fm,fp, g1,g2, gm,gm1,gm2,gp1,gp2,gq,gqr, gr
      real*8     p,pd,pm,pp,pz,pz2, q,qr,qr1, r,rm,rp,rpp, sg2
      real*8     u,ux,uxk, xl,xt,xx, xl2

      save

      if(ids.eq.0) then
        do i = 1,6
          s(i) = 0.d0
        end do ! i
        return
      endif
      s(5) = 0.d0
      s(6) = 0.d0
      pz = 0.39894228048d0*exp(-z*z*0.5d0)     ! phi(z)
      xx = x(k)

      if(ids.ge.51) go to 510

      go to (10,20,30,40,50,60,70,100,100,100,110,120,130,140),ids

   10 s(1) = -1.d0/sg
      s(2) = -z/sg
      s(3) = s(1)
      s(4) = s(2)
      return

   20 s(3) = -1.d0/par(2)
      s(4) = -z/par(2)
      e = (sg/ex)**2
      d = 1.d0 + e
      f = 1.d0/ex/d/par(2)
      s(1) = (-d-e+e*z/par(2))*f
      s(2) = sg*(1.+s(4))/ex*f
      return

   30 xl = par(1)*xx
      gm   = gamma(par(2))
      s(3) = xx*xl**(par(2)-1)*exp(-xl)/pz/gm
      xl2  = 2.0*xl
      pp   = 2.d0*(par(2) + 0.001d0)
      pm = 2.d0*(par(2) - 0.001d0)
      call dchis(xl2,pp,fp,ier)
      call dchis(xl2,pm,fm,ier)
      s(4) = (fp-fm)/0.002d0/pz
      sg2 = sg*sg
      s(1) = (s(3)+s(4)*2.d0*ex)/sg2
      s(2) = -2.d0*ex*(s(3)+s(4)*ex)/sg/sg2
      return

   40 dx = xx-par(2)
      e = exp(-par(1)*dx)/pz
      s(1) = -par(1)*e
      s(2) = (par(1)-dx/sg/sg)*e
      s(3) = dx*e
      s(4) = s(1)
      return

   50 dx = xx-par(2)
      r = dx/par(1)
      e = exp(-0.5d0*r*r)*r/pz/par(1)   ! fx(x)/phi(z)
      s(1) = -xx/par(1)*e*0.797884561d0  ! revised by kjh feb 23 2000
      s(2) = -xx/par(1)*e*1.526399746d0
      s(3) = -r*e
      s(4) = s(1)
      return

   60 ba = par(2)-par(1)
      s(1) = -1.d0/pz/ba
      e = -s(1)/ba
      s(2) = 1.732050807d0*(par(1)+par(2)-2.d0*xx)*e
      s(3) = (xx-par(2))*e
      s(4) = (par(1)-xx)*e
      return

   70 q = par(1)
      r = par(2)
      a = par(3)
      b = par(4)
      ba = b-a
      gq = gamma(q)
      gr = gamma(r)
      gqr = gamma(q+r)
      qr = q+r
      qr1 = qr+1.d0
      u = (xx-a)/ba
      xt = (b-xx)/ba
      s(5) = -gqr*(u**q)*(xt**r)/pz/gq/gr/(xx-a)
      s(6) = s(5)*(xx-a)/(b-xx)
      p = q+0.001d0
      call dbeta(u,p,r,fp,ier)
      p = q-0.001d0
      call dbeta(u,p,r,fm,ier)
      pz2 = 0.002d0*pz
      s(3) = (fp-fm)/pz2
      p = r+0.001d0
      call dbeta(u,q,p,fp,ier)
      p = r-0.001d0
      call dbeta(u,q,p,fm,ier)
      s(4) = (fp-fm)/pz2
      s(1) = 0.d0
      s(2) = 0.d0
      return

  100 return

  110 dx = xx-par(1)
      dxp = par(2)*dx
      e = exp(-dxp-exp(-dxp))/pz
      s(1) = -par(2)*e
      s(2) = (0.450053207d0*par(2)-1.28254983d0*dx/sg/sg)*e
      s(3) = s(1)
      s(4) = dx*e
      return

  120 dx = xx-par(1)
      dxp = par(2)*dx
      e = exp(dxp-exp(dxp))/pz
      s(1) = -par(2)*e
      s(2) = (-0.450053207d0*par(2)-1.28254983d0*dx/sg/sg)*e
      s(3) = s(1)
      s(4) = dx*e
      return

  130 ux = par(1)/xx
      uxk = ux**par(2)
      e = uxk*exp(-uxk)/pz
      s(3) = -par(2)*e/par(1)
      s(4) = -log(ux)*e
      r = 1.d0/par(2)
      g1 = gamma(1.d0-r)
      g2 = gamma(1.d0-r*2)
      du1 = g1
      ds1 = dsqrt(g2-g1*g1)
      rp = 1.d0/(par(2)+0.001d0)
      rm = 1.d0/(par(2)-0.001d0)
      gp1 = gamma(1.d0-rp)
      gm1 = gamma(1.d0-rm)
      gp2 = gamma(1.d0-rp*2.)
      gm2 = gamma(1.d0-rm*2.)
      du2 = (gp1-gm1)*par(1)/0.002d0
      ds2 = (dsqrt(gp2-gp1*gp1)-dsqrt(gm2-gm1*gm1))*par(1)/0.002d0
      det = du1*ds2-du2*ds1
      s(1) = (ds2*s(3)-ds1*s(4))/det
      s(2) = (-du2*s(3)+du1*s(4))/det
      return

  140 ux = xx/par(1)
      uxk = ux**par(2)
      e = uxk*exp(-uxk)/pz
      s(3) = -par(2)*e/par(1)
      s(4) = log(ux)*e
      r = 1.d0/par(2)
      g1 = gamma(1.d0+r)
      g2 = gamma(1.d0+r*2.)
      du1 = g1
      ds1 = dsqrt(g2-g1*g1)
      dr = 0.001d0*par(2)
      rp = 1.d0/(par(2)+dr)
      rm = 1.d0/(par(2)-dr)
      gp1 = gamma(1.d0+rp)
      gm1 = gamma(1.d0+rm)
      gp2 = gamma(1.d0+rp*2.d0)
      gm2 = gamma(1.d0+rm*2.d0)
      du2 = (gp1-gm1)*par(1)/2.d0/dr
      ds2 = (dsqrt(gp2-gp1*gp1)-dsqrt(gm2-gm1*gm1))*par(1)/2.d0/dr
      det = du1*ds2-du2*ds1
      s(1) = (ds2*s(3)-ds1*s(4))/det
      s(2) = (-du2*s(3)+du1*s(4))/det
      return

  510 r = 0.001d0
      do i = 1,4
        pp = par(i)
        if(pp.eq.0) then
          rpp = r
        else
          rpp = r*pp
        endif
        par(i) = pp+rpp
        call udd(x,par,sg,ids,fp,pd,bnd,ib)
        par(i) = pp-rpp
        call udd(x,par,sg,ids,fm,pd,bnd,ib)
        par(i) = pp
        s(2+i) = (fp-fm)/rpp/2.d0/pz
      end do ! i
      s(1) = 0.d0
      s(2) = 0.d0

      end subroutine cdzpar
