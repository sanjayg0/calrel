c     stp : sum of d(pfi)/d(par), where par is the distribution par.
c     sdp : sum of d(pfi)/d(par), where par is performance fun. par.
c     bdp : d(bti)/d(par) for all the transition functions. here par is the
c           par. in the transition function.
c     ibx : index to identify which entry (entries) of matrix prbr
c           is used in the computation bimodal bound of system failure prob.
c     pu1 : the upper bound of system failure probability in unimodal bound.
c     pu3 : the upper bound of system failure probability in bimodal bound.
c
c     calls:
c            cerror, dnormi, ugfun, gdapar,  gdadtp, gdfijd, gpfij
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine gsens(xn,xlin,beta,sdgy,tp,ex,sg,par,igt,lgp,ipa,
     *izx,ixz,ids,tf,alph,next,prbr,rhom,ibx,t,tj,ddpi,drip,stp,sdp,
     *ti,bdp,dadi,drid,bnd,sdxz,stf,igg,igfx,isv)

      implicit   none

      include   'bond.h'
      include   'cuts.h'
      include   'file.h'
      include   'flag.h'
      include   'prob.h'

      character (len=4): : xn(nrx)

      integer (kind=4) :: i,ii,i0,i1, im,imo, igd,isv, ier, j0,j1, jj
      integer (kind=4) :: k,ki,kj, kii,kjj,ki0,kj0
      real    (kind=8) :: dfido,dfipo, did,dsn
      real    (kind=8) :: p1,p2,pai,pbi,pbj,pbu, pb1,pb2, yu3
      real    (kind=8) :: xlin(nrx,ngf),t(6,nrx),tp(*)
      integer (kind=4) :: ids(nrx),next(ngfc)
      real    (kind=8) :: beta(ngf),sdgy(ngf),alph(nry,ngf)
      real    (kind=8) :: prbr(ngfc,ngfc),rhom(ngfc,ngfc),stf(ntge,ngf)
      integer (kind=4) :: igg(ngfc*2)
      real    (kind=8) :: ddpi(nry,4,nrx*ngfc),drip(4,nrx)
      real    (kind=8) :: stp(4,nrx),sdp(ntp),bdp(ntp,ngfc)
      integer (kind=4) :: ibx(ngfc-1,2)
      real    (kind=8) :: dadi(nry,ntp*ngfc),drid(ntp),par(4,nrx)
      real    (kind=8) :: ti(4,nrx),tj(4,nrx),bnd(2,nrx),tf(ntf),ex(nrx)
      integer (kind=4) :: igt(nig),igfx(ngf,3)
      integer (kind=4) :: ixz(nrx),izx(nry),ipa(4,nrx),lgp(3,nig)
      real    (kind=8) :: sg(nrx),sdxz(nrx,3*ngf)

      save

c
c
c     compute system sensitivity for bimodal bounds
       pai=acos(-1.d0)
        ii=gmin
      do 360 i=2,ngfc
        ii=next(ii)
      kii=igfx(ii,2)
        pbi=exp(-beta(kii)*beta(kii)*0.5d0)/sqrt(2.d0*pai)
        jj=ibx(i-1,2)
        if (jj.eq.0) go to 360
      kjj=igfx(jj,2)
c     compute the system sensitivity w.r.t. distribution parameters
c     for bimodal bounds.
c     compute d(btij)/d(par), d(pfij)/d(par).
c
        pbj=exp(-beta(kjj)*beta(kjj)*0.5d0)/sqrt(2.d0*pai)
        pb1=pbi*prbr(jj,jj)
        pb2=pbj*prbr(ii,ii)
c     compute normal density of ii,jj
      p1=1.d0-rhom(ii,jj)*rhom(ii,jj)
      p2=0.5d0*(beta(kii)*beta(kii)+beta(kjj)*beta(kjj)-2.d0*
     *   rhom(ii,jj)*beta(kii)*beta(kjj))/p1
      dsn=exp(-p2)/sqrt(p1)/pai/2.d0
      if(isv.eq.2) go to 320
      do 399 i1=1,nrx
      do 399 j1=1,4
 399  drip(j1,i1)=0.
c     read d(bti)/d(par) and d(btj)/d(par).
      call ginp(ti,tj,ids,kii,kjj)
c     compute the integration of d(nornal density bti,btj)/d(par).
c     compute d(ro ij)/d(par)
c
      imo=0
      do 920 im=1,2
      if(imo.eq.0) then
      i0=ii
      j0=jj
      imo=1
      else
      i0=jj
      j0=ii
      endif
      ki0=igfx(i0,2)
      kj0=igfx(j0,2)
      igd=(i0-1)*nrx
      if(igg(i0).eq.0) then
      call fsave(stf,sdxz,tf,t(1,1),igt,lgp,ki0,1,t(1,2),t(1,3))
      call gdapar(xlin(1,ki0),sdxz(1,ki0+2*ngf),ex,sg,alph(1,ki0)
     *,sdgy(ki0),sdxz(1,ki0+ngf),t,stf(1,ki0),par,igt,lgp,ids,ipa,
     * izx,ixz,ddpi(1,1,igd+1),tf,bnd,sdxz(1,ki0),igfx(i0,3))
       igg(i0)=1
      endif
      do 290 ki=1,nrx
      do 290 kj=1,4
      do 290 k=1,nry
  290 drip(kj,ki)=drip(kj,ki)+alph(k,kj0)*ddpi(k,kj,ki+igd)
  920 continue
c     compute d(pfij)/d(par)
      do 382 ki=1,nrx
      do 385 kj=1,4
      call gpfij(did,beta(kii),beta(kjj),rhom(ii,jj),
     *           ti(kj,ki),tj(kj,ki))
        dfipo=-pb1*ti(kj,ki)-pb2*tj(kj,ki)+did+dsn*
     *            drip(kj,ki)
        stp(kj,ki)=stp(kj,ki)-dfipo
  385 continue
  382 continue
c     compute da/d(par) for deterministic parameters.
  320 if(ntp.eq.0.or.isv.eq.1) go to 350
      do 330 ki=1,ntp
  330 drid(ki)=0.d0
      imo=0
      do 930 im=1,2
      if(imo.eq.0) then
      i0=ii
      j0=jj
      imo=1
      else
      i0=jj
      j0=ii
      endif
      ki0=igfx(i0,2)
      kj0=igfx(j0,2)
      igd=(i0-1)*ntp
      if(igg(i0+ngfc).eq.0) then
      call fsave(stf,sdxz,tf,t(1,1),igt,lgp,ki0,1,t(1,2),t(1,3))
      call gdadtp(xlin(1,ki0),sdxz(1,ki0+2*ngf),tp,ex,sg,alph(1,ki0)
     *   ,sdgy(ki0),tf,par,bnd,igt,lgp,ids,ipa,izx,ixz,t,dadi(1,igd+1),
     *    sdxz(1,ki0),igfx(i0,3))
      igg(i0+ngfc)=1
      endif
      do 430 ki=1,ntp
      do 430 k=1,nry
  430 drid(ki)=drid(ki)+alph(k,kj0)*dadi(k,ki+igd)
  930 continue
c     compute d(pfij)/d(par)
      do 334 ki=1,ntp
      call gpfij(did,beta(kii),beta(kjj),rhom(ii,jj),
     *bdp(ki,ii),bdp(ki,jj))
      dfido=-pb1*bdp(ki,ii)-pb2*bdp(ki,jj)+did+dsn*drid(ki)
      sdp(ki)=sdp(ki)-dfido
  334 continue
  350 continue
  360 continue
        call dnormi(yu3,pu3,ier)
        pbu=-0.39894228d0*exp(-yu3*yu3*0.5d0)
c     output the result
      write(not,4020)
      call goutp(t,pbu,xn,stp,sdp,ids,ipa,ixz,isv)
 4020 format(///' sensitivity measures based on bimodal upper',
     *    ' bound of pf1',
     *       /,1x,70('-'))

      end subroutine gsens
