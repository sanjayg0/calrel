# N.B.  It is necessary to modify 'makefile.in' before using make.

include $(CALREL)/makefile.in

CLEANDIRS = cal_lib cal_add

calrel: archive
	(cd main; make calrel)
	@@echo "--> CALREL executable made <--"

archive:   
	(cd cal_lib; make archive)
	(cd cal_add; make archive)
	@@echo "--> CALREL Archive updated <--"

install: archive calrel

clean:
	for i in $(CLEANDIRS); do (cd $$i; make clean); done
	if [ -f $(ARCALREL) ]; then rm $(ARCALREL); fi
	@@echo "--> CALREL rcscleaned <--"

