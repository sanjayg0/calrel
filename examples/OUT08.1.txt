======================================================
*                                                    *
*                    CalREL                          *
*                                                    *
* Copyright 1985 University of California, Berkeley  *
*  Department of Civil & Environemtal Engineering    *
*                                                    *
* This version of CalREL is for exclusive use by:    *
*          Students and Faculty at the               *
*       University of California, Berkeley           *
*                                                    *
* This software is protected by copyright laws.      *
* Unauthorized use and distribution are prohibited   *
* by law and will be prosecuted.                     *
*                                                    *
*====================================================*


R U N N I N G     C A L R E L     N O W 


>>>> INPUT DATA <<<<

Example 8.1 --Continue to do analysis of Example 8.                             

optimization scheme used ................iop=      1
  iop=1 ................................HL-RF method
  iop=2 .......................modified HL-RF method
  iop=3 ..................gradient projection method
  iop=4 .................sequential quadratic method
  iop=5 .......................improved HL-RF method
  iop=6 .............................Polak-He method
maximum number of iteration cycles ......ni1=    100
maximum steps in line search ............ni2=      4
convergence tolerance ................tol= 1.000E-04
optimization parameter 1 .............op1= 5.000E-01
optimization parameter 2 .............op2= 0.000E+00
optimization parameter 3 .............op3= 0.000E+00


>>>> FIRST-ORDER RELIABILITY ANALYSIS <<<<

print interval ..........................npr=      0
  npr<0 ..........no first order results are printed
  npr=0 ........print the final step of FORM results
  npr>0 ........print the results of every npr steps
initialization flag .....................ini=      0
  ini=0 .......................start from mean point
  ini=1 ..........start from point specified by user
  ini=-1 ....start from previous linearization point
restart flag ............................ist=      0
  ist=0 .......................analyze a new problem
  ist=1 .............continue an unconverged problem

limit-state function    16
-----------------------------------------------------------------------------
iteration number ..............iter=        24
value of limit-state function..g(x)=   -6.1438E-08
reliability index .............beta=      2.961260
probability ....................Pf1= 1.5319170E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
        1.100E+02     5.817E-01         .1964     .3104    -.3018    -.1215
        1.061E+01    -1.806E+00        -.6097    -.5987     .8544   -1.1282
        8.001E+00     8.074E-01         .2727     .0000     .0000     .0000
        1.022E+01     2.126E+00         .7179     .7384    -.2109    -.8119
-----------------------------------------------------------------------------

limit-state function    17
-----------------------------------------------------------------------------
iteration number ..............iter=        14
value of limit-state function..g(x)=   -6.8209E-07
reliability index .............beta=      2.701293
probability ....................Pf1= 3.4535274E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
        7.775E+01    -1.172E+00        -.4337    -.4696     .5546    -.5826
        1.403E+01     0.000E+00         .0000     .0000     .0000     .0000
        9.993E+00     1.464E+00         .5420     .2908    -.1579    -.1775
        1.055E+01     1.944E+00         .7198     .8336    -.1956    -.8356
-----------------------------------------------------------------------------

limit-state function    18
-----------------------------------------------------------------------------
iteration number ..............iter=         5
value of limit-state function..g(x)=    3.4121E-07
reliability index .............beta=      1.621794
probability ....................Pf1= 5.2423687E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
        1.201E+02     1.024E+00         .6312     .7071    -.6717    -.6414
        1.201E+01    -1.258E+00        -.7756    -.7071     .9898    -.9490
        6.498E+00     0.000E+00         .0000     .0000     .0000     .0000
        4.641E+00     0.000E+00         .0000     .0000     .0000     .0000
-----------------------------------------------------------------------------

limit-state function    19
-----------------------------------------------------------------------------
iteration number ..............iter=         8
value of limit-state function..g(x)=   -1.1483E-07
reliability index .............beta=     -1.879736
probability ................1 - Pf1= 3.0072038E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
        1.332E+02     1.547E+00        -.8227    -.8587     .6133    1.0880
        1.565E+01     0.000E+00         .0000     .0000     .0000     .0000
        5.551E+00    -8.134E-01         .4327     .3055    -.6302     .3699
        3.885E+00    -6.929E-01         .3686     .4116    -.8938     .5570
-----------------------------------------------------------------------------


>>>> SECOND-ORDER RELIABILITY ANALYSIS -- POINT FITTING <<<<

type of integration scheme used ...................itg=    2
  itg=1 ...........................improved Breitung formula
  itg=2 ...........................improved Breitung formula
        ............................& Tvedt's exact integral
max. number of iterations for each fitting point ..inp=    4

limit-state function   11
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.426  1.937  8.489E-03 -1.663E-01   -2.426  2.173  2.931E-03 -8.581E-02
   2 2.426  2.426  1.693E-07 -6.298E-09   -2.426  2.426  1.693E-07 -6.298E-09
   3 2.426  2.083  3.882E-02 -1.165E-01   -2.426  1.959  7.312E-04 -1.586E-01

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.252904            2.268385
probability                     Pf2 = 1.2132606E-02       1.1652878E-02
-----------------------------------------------------------------------------

limit-state function   12
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.281  2.109  2.052E-02 -6.626E-02   -2.281  2.023  6.381E-04 -9.911E-02
   2 2.281  2.281  5.526E-07 -1.915E-08   -2.281  2.281  5.526E-07 -1.915E-08
   3 2.281  1.881  4.638E-03 -1.539E-01   -2.281  2.144  3.099E-03 -5.271E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.167678            2.174324
probability                     Pf2 = 1.5091608E-02       1.4840419E-02
-----------------------------------------------------------------------------

limit-state function   13
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 3.000  3.075  4.107E-05 -1.977E-03   -3.000  3.074  2.314E-05 -2.219E-03
   2 3.000  2.936  2.686E-02 -3.300E-02   -3.000  2.834  1.437E-03 -5.571E-02
   3 3.000  2.380  5.454E-03 -1.566E-01   -3.000  2.945  5.470E-03 -3.090E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.991768            2.997079
probability                     Pf2 = 1.3868334E-03       1.3629003E-03
-----------------------------------------------------------------------------

limit-state function   14
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.786  2.279  8.172E-02 -1.308E-01   -2.786  2.662  3.485E-02 -3.206E-02
   2 2.786  2.786  2.624E-06 -1.112E-08   -2.786  2.786  2.624E-06 -1.112E-08
   3 2.786  2.615  3.141E-01 -4.406E-02   -2.786  2.492  1.315E-02 -7.572E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.700101            2.703659
probability                     Pf2 = 3.4659234E-03       3.4290299E-03
-----------------------------------------------------------------------------

limit-state function   15
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.017  1.925  6.019E-03 -4.522E-02   -2.017  1.885  6.491E-04 -6.520E-02
   2 2.017  2.017  5.277E-08 -2.045E-09   -2.017  2.017  5.277E-08 -2.045E-09
   3 2.017  2.015  2.388E-06 -1.282E-03   -2.017  2.015  1.744E-06 -1.358E-03

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      1.986914            1.987498
probability                     Pf2 = 2.3465960E-02       2.3433635E-02
-----------------------------------------------------------------------------

limit-state function   16
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.961  2.415  4.822E-06 -1.246E-01   -2.961  2.652  1.548E-03 -7.042E-02
   2 2.961  2.503  1.031E-02 -1.044E-01   -2.961  2.267  4.090E-07 -1.583E-01
   3 2.961  2.913  3.493E-05 -1.092E-02   -2.961  2.905  3.694E-06 -1.279E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.803360            2.814911
probability                     Pf2 = 2.5286620E-03       2.4395347E-03
-----------------------------------------------------------------------------

limit-state function   17
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.701  2.441  1.865E-01 -7.145E-02   -2.701  2.315  2.930E-03 -1.058E-01
   2 2.701  2.701  4.138E-07 -4.095E-09   -2.701  2.701  4.138E-07 -4.095E-09
   3 2.701  1.842  1.422E-02 -2.356E-01   -2.701  2.381  4.622E-02 -8.770E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.512339            2.539129
probability                     Pf2 = 5.9966913E-03       5.5564352E-03
-----------------------------------------------------------------------------

limit-state function   18
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 1.622  1.622 -5.053E-08  6.310E-09   -1.622  1.622 -1.679E-08  1.074E-08
   2 1.622  1.622 -3.446E-08  8.528E-09   -1.622  1.622 -3.446E-08  8.528E-09
   3 1.622  1.622 -3.446E-08  8.528E-09   -1.622  1.622 -3.446E-08  8.528E-09

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      1.621794            1.621794
probability                     Pf2 = 5.2423685E-02       5.2423687E-02
-----------------------------------------------------------------------------

limit-state function   19
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1-1.734 -2.015  1.604E-06 -8.967E-02    1.682 -2.059  3.808E-04 -1.266E-01
   2-1.880 -1.880  9.177E-09 -1.697E-09    1.880 -1.880  9.177E-09 -1.697E-09
   3-1.755 -1.997  1.797E-06 -7.582E-02    1.695 -2.048  3.541E-04 -1.173E-01

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =     -1.969805           -1.973454
probability                     Pf2 = 1-  2.4430334E-02   1-  2.4221935E-02
-----------------------------------------------------------------------------


>>>> DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.
    trials        Pf-mean     betag-mean    coef of var
       500    9.59983E-03    2.34163E+00    8.36248E-02
      1000    9.89102E-03    2.33046E+00    5.90968E-02
      1500    9.63518E-03    2.34026E+00    4.89304E-02


>>>> FIRST-ORDER DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.

trials       Pf-mean       betag-mean     coef of var of pf
   500    6.9175316E-03    2.4615174E+00    9.1162304E-02
  1000    7.3227414E-03    2.4410288E+00    6.4951365E-02
  1500    7.1622497E-03    2.4490211E+00    5.3986805E-02
  2000    7.0818175E-03    2.4530862E+00    4.6290764E-02


>>>> SECOND-ORDER DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.

trials       Pf-mean       betag-mean     coef of var of pf
   500    9.5905306E-03    2.3419932E+00    8.1408716E-02
  1000    9.9338268E-03    2.3288379E+00    5.7708397E-02
  1500    9.6823450E-03    2.3384350E+00    4.7934253E-02


>>>> MONTE CARLO SIMULATION <<<<

 print interval ......................... npr=    5000
 number of simulations .................. nsm=  100000
 threshold for coef. of variation .... cov=  .5000E-01
 random seed ......................... stp= 619949855.
     trials       Pf-mean       betag-mean     coef of var of Pf
       5000    5.8000000E-03    2.5240844E+00    1.8517456E-01
      10000    6.9000000E-03    2.4624275E+00    1.1997580E-01
      15000    8.3333333E-03    2.3939797E+00    8.9072231E-02
      20000    8.6500000E-03    2.3802690E+00    7.5700947E-02
      25000    8.6400000E-03    2.3806952E+00    6.7748160E-02
      30000    9.3000000E-03    2.3534521E+00    5.9590389E-02
      35000    9.3714286E-03    2.3506062E+00    5.4957214E-02
      40000    9.4000000E-03    2.3494731E+00    5.1328748E-02
      45000    9.4888889E-03    2.3459671E+00    4.8163780E-02
Stop - Program terminated.
