calrel ngf=3 nig=2 nrx=7 ntp=1
data
title
1
Example 7 -- Series system reliability with depenedet random variables.
flag icl,igr
2 0
CUTS ncs,ntl
3,3
7,0,8,0,9,0
OPTI iop,ni1,ni2,tol,op1,op2,op3
5 40 4 0.001 0.25 0.001 4
statistics igt(i),nge,ngm
2 5
x1    1,14,134.,23.,0.,0.,0.
x2    2,14,134.,23.,0.,0.,0.
x3    3,14,160.,35.,0.,0.,0.
x4    4,14,150.,30.,0.,0.,0.
x5    5,14,150.,30.,0.,0.,0.
0.4
0.2 0.4
0.2 0.2 0.4
0.2 0.2 0.2 0.4
2,2
x6    6,6,65.,20.,0.,0.,0.
x7    7,6,50.,15.,0.,0.,0.
0.4
para
5.
end
form
boun
pnet
snes isc=2
sorm iso=1
dirs ifs=0 nsm=1000 npr=500 cov=0.05 stp=619944257.
dirs ifs=1 nsm=1000 npr=500 cov=0.05 stp=619944257.
dirs ifs=2 nsm=1000 npr=500 cov=0.05 stp=619944257.
mont nsm=4000 npr=2000 cov=0.05 stp=619944257.
exit
