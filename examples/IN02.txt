CALREL nrx=3 nig=1 ntp=1
DATA
TITLE nline title
2
Example 2 -- Component reliability with dependent random variables defined
             by marginal distributions and correlation matrix.
FLAG icl,igr
1 0
OPTIMIZATION iop,ni1,ni2,tol,op1,op2,op3
5,20,10,0.001
STATISTICS igt(i),nge,ngm nv,ids,ex,sg,p3,p4,x0
2 3
x1  1,2,500.,100.
x2  2,2,2000.,400.
x3  3,6,5.,0.5
0.3
0.2 0.2
para
1.0
END
FORM npr=1 igf=2
SENS isc=1 isv=0 igf=2
SORM iso=3 itg=2 igf=2
DIRS igf=2 ifs=0 nsm=5000 npr=500 cov=0.03 stp=621319411.
MONT igf=2 nsm=500000 cov=0.03 npr=2000 stp=621319411.
EXIT
