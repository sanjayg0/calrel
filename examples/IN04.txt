CALREL nrx=6 nig=2 ntp=1
DATA
TITLE nline title
3
Example 4 --  Component reliability with two independent groups of dependent
              random variables defined by marginal distributions and corre-
              lation matrix
FLAG icl,igr
1 0
OPTIMIZATION iop,ni1,ni2,tol,op1,op2,op3
2,20,4,0.001,0.5
STATISTICS igt(i),nge,ngm nv,ids,ex,sg,p3,p4,x0
2 3
x1  1,2,500.,100.
x2  2,2,2000.,400.
x3  3,6,5.,0.5
0.3
0.2 0.2
2 3
x4  4,2,450.,90.
x5  5,2,1800.,360.
x6  6,6,4.5,0.45
0.3
0.2 0.2
para
1.7
END
FORM igf=4
SENS isc=1 isv=0 igf=4
SORM iso=3 itg=2 igf=4
DIRS igf=4 ifs=0 nsm=1000 npr=500 stp=62120774.
MONT igf=4 nsm=50000 npr=1000 stp=62120774.
EXIT
