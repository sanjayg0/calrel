======================================================
*                                                    *
*                    CalREL                          *
*                                                    *
* Copyright 1985 University of California, Berkeley  *
*  Department of Civil & Environemtal Engineering    *
*                                                    *
* This version of CalREL is for exclusive use by:    *
*          Students and Faculty at the               *
*       University of California, Berkeley           *
*                                                    *
* This software is protected by copyright laws.      *
* Unauthorized use and distribution are prohibited   *
* by law and will be prosecuted.                     *
*                                                    *
*====================================================*


R U N N I N G     C A L R E L     N O W 


>>>> NEW PROBLEM <<<<

number of limit-state functions..........ngf=      1
number of independent variable groups ...nig=      1
total number of random variables ........nrx=      2
number of limit-state parameters ........ntp=      3


>>>> INPUT DATA <<<<

Example 1 -- Component reliability with independent random variables.           
type of system ..........................icl=      1
  icl=1 ...................................component
  icl=2 ...............................series system
  icl=3 ..............................general system
flag for gradient computation ...........igr=      1
  igr=0 ...........................finite difference
  igr=1 ...................formulas provided by user

optimization scheme used ................iop=      1
  iop=1 ................................HL-RF method
  iop=2 .......................modified HL-RF method
  iop=3 ..................gradient projection method
  iop=4 .................sequential quadratic method
  iop=5 .......................improved HL-RF method
  iop=6 .............................Polak-He method
maximum number of iteration cycles ......ni1=     20
maximum steps in line search ............ni2=      4
convergence tolerance ................tol= 1.000E-04
optimization parameter 1 .............op1= 1.000E+00
optimization parameter 2 .............op2= 0.000E+00
optimization parameter 3 .............op3= 0.000E+00

statistical data of basic varibles:
available probability distributions:
  determinitic .............ids=0
  normal ...................ids=1
  lognormal ................ids=2
  gamma ....................ids=3
  shifted exponential ......ids=4
  shifted rayleigh .........ids=5
  uniform ..................ids=6
  beta .....................ids=7
  type i largest value .....ids=11
  type i smallest value ....ids=12
  type ii largest value ....ids=13
  weibull ..................ids=14
  user defined .............ids>50

group no.:    1          group type:    1
var   ids   mean    st. dev.   param1    param2    param3    param4   init. pt
x1     6  5.00E+01  2.89E+01  0.00E+00  1.00E+02                      2.35E+01
x2     4  2.00E+01  2.00E+01  5.00E-02  0.00E+00                      2.35E+01

deterministic parameters in limit-state function:
  tp (  1) =  1.000E+00
  tp (  2) =  1.000E+00
  tp (  3) =  0.000E+00
WARNING 2: command      not available


>>>> FIRST-ORDER RELIABILITY ANALYSIS <<<<

print interval ..........................npr=      1
  npr<0 ..........no first order results are printed
  npr=0 ........print the final step of FORM results
  npr>0 ........print the results of every npr steps
initialization flag .....................ini=      1
  ini=0 .......................start from mean point
  ini=1 ..........start from point specified by user
  ini=-1 ....start from previous linearization point
restart flag ............................ist=      0
  ist=0 .......................analyze a new problem
  ist=1 .............continue an unconverged problem

limit-state function     1
-----------------------------------------------
iteration number ..............iter=         1
reliability index .............beta=     .8781
value of limit-state function..g(x)=-1.735E-18
var       linearization point       unit normal
            x             u            alpha
x1      2.352E+01    -7.218E-01        -.8029
x2      2.352E+01     5.001E-01         .5961
-----------------------------------------------
iteration number ..............iter=         2
reliability index .............beta=     .8777
value of limit-state function..g(x)=-6.589E-04
var       linearization point       unit normal
            x             u            alpha
x1      2.405E+01    -7.047E-01        -.8022
x2      2.405E+01     5.231E-01         .5970
-----------------------------------------------------------------------------
iteration number ..............iter=         3
value of limit-state function..g(x)=   -5.9577E-07
reliability index .............beta=       .877644
probability ....................Pf1= 1.9006858E-01
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      2.407E+01    -7.041E-01        -.8022    -.8022     .7438    -.6681
x2      2.407E+01     5.240E-01         .5970     .5970    -.5153    -.1048
-----------------------------------------------------------------------------
WARNING 2: command      not available


>>>> SENSITIVITY ANALYSIS AT COMPONENT LEVEL <<<<

type of parameters for sensitivity analysis
..........................................isv=      0
  isv=1 ......................distribution parameters
  isv=2 ...................limit-state fcn parameters
  isv=0 ..distribution and limit-state fcn parameters

sensitivity with respect to distribution parameters

limit-state function    1
----------------------------------------------------------------------
d(beta)/d(parameter) :
var     mean      std dev     par 1      par 2      par 3      par 4
x1    2.576E-02 -2.314E-02  1.956E-02  6.201E-03                       
x2   -2.576E-02 -5.242E-03  1.240E+01 -2.576E-02                       

d(Pf1)/d(parameter) :
var     mean      std dev     par 1      par 2      par 3      par 4
x1   -6.993E-03  6.282E-03 -5.310E-03 -1.683E-03                       
x2    6.993E-03  1.423E-03 -3.366E+00  6.993E-03                       
----------------------------------------------------------------------

sensitivity with respect to limit-state function parameters

limit-state function    1
----------------------------------------------------------------------
par   d(beta)/d(parameter)     d(Pf1)/d(parameter)
 1          1.973E+00               -5.354E-01
 2         -6.201E-01                1.683E-01
 3          2.576E-02               -6.993E-03
----------------------------------------------------------------------


>>>> SECOND-ORDER RELIABILITY ANALYSIS -- POINT FITTING <<<<

type of integration scheme used ...................itg=    2
  itg=1 ...........................improved Breitung formula
  itg=2 ...........................improved Breitung formula
        ............................& Tvedt's exact integral
max. number of iterations for each fitting point ..inp=    4

limit-state function    1
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 1.033   .839  9.277E-06 -7.251E-02   -1.010   .866  1.732E-10 -2.223E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =       .852718             .853295
probability                     Pf2 = 1.9690791E-01       1.9674788E-01
-----------------------------------------------------------------------------


>>>> SECOND-ORDER RELIABILITY ANALYSIS  -- CURVATURE FITTING <<<<

type of integration scheme used ..................itg=    2
  itg=1 ..........................improved Breitung formula
  itg=2 ..........................improved Breitung formula
        ...........................& Tvedt's exact integral

limit-state function    1
-----------------------------------------------------------------------------
main curvatures in (n-1)x(n-1) space

       1
 1 -4.253E-02
                                      improved Breitung      Tvedt's EI
generalized reliability index betag =       .855571             .855995
probability                     Pf2 = 1.9611764E-01       1.9600028E-01
-----------------------------------------------------------------------------


>>>> MONTE CARLO SIMULATION <<<<

 print interval ......................... npr=    1000
 number of simulations .................. nsm=   50000
 threshold for coef. of variation .... cov=  .5000E-01
 random seed ......................... stp= 621320774.

limit-state function     1
----------------------------------------------------------------------
     trials       Pf-mean       betag-mean     coef of var of Pf
       1000    2.0400000E-01    8.2741831E-01    6.2496933E-02
       2000    1.9650000E-01    8.5418948E-01    4.5227779E-02
Stop - Program terminated.
