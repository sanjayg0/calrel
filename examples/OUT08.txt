======================================================
*                                                    *
*                    CalREL                          *
*                                                    *
* Copyright 1985 University of California, Berkeley  *
*  Department of Civil & Environemtal Engineering    *
*                                                    *
* This version of CalREL is for exclusive use by:    *
*          Students and Faculty at the               *
*       University of California, Berkeley           *
*                                                    *
* This software is protected by copyright laws.      *
* Unauthorized use and distribution are prohibited   *
* by law and will be prosecuted.                     *
*                                                    *
*====================================================*


R U N N I N G     C A L R E L     N O W 


>>>> NEW PROBLEM <<<<

number of limit-state functions..........ngf=      9
number of independent variable groups ...nig=      2
total number of random variables ........nrx=      4
number of limit-state parameters ........ntp=      0


>>>> INPUT DATA <<<<

Example 8 -- General system reliability with dependent random variables.        
type of system ..........................icl=      3
  icl=1 ...................................component
  icl=2 ...............................series system
  icl=3 ..............................general system
flag for gradient computation ...........igr=      0
  igr=0 ...........................finite difference
  igr=1 ...................formulas provided by user
number of cut sets (cs) .................ncs=      7
total number of components in all cs.....ntl=     14

cut sets:
  cut set  1:   11   14
  cut set  2:   11   15
  cut set  3:   11   16
  cut set  4:   12   17
  cut set  5:   12  -15
  cut set  6:   12   18
  cut set  7:   13   19

optimization scheme used ................iop=      5
  iop=1 ................................HL-RF method
  iop=2 .......................modified HL-RF method
  iop=3 ..................gradient projection method
  iop=4 .................sequential quadratic method
  iop=5 .......................improved HL-RF method
  iop=6 .............................Polak-He method
maximum number of iteration cycles ......ni1=    100
maximum steps in line search ............ni2=     10
convergence tolerance ................tol= 1.000E-04
optimization parameter 1 .............op1= 5.000E-01
optimization parameter 2 .............op2= 1.100E+00
optimization parameter 3 .............op3= 1.000E-01

statistical data of basic varibles:
available probability distributions:
  determinitic .............ids=0
  normal ...................ids=1
  lognormal ................ids=2
  gamma ....................ids=3
  shifted exponential ......ids=4
  shifted rayleigh .........ids=5
  uniform ..................ids=6
  beta .....................ids=7
  type i largest value .....ids=11
  type i smallest value ....ids=12
  type ii largest value ....ids=13
  weibull ..................ids=14
  user defined .............ids>50

group no.:    1          group type:    2
var   ids   mean    st. dev.   param1    param2    param3    param4   init. pt
x1     2  1.00E+02  2.00E+01  4.59E+00  1.98E-01                      0.00E+00
x2     2  1.50E+01  3.00E+00  2.69E+00  1.98E-01                      0.00E+00

correlation coefficient matrix in original space:
       1          2
 1  1.000E+00  2.000E-01
 2  2.000E-01  1.000E+00
 end inpro 

group no.:    2          group type:    2
var   ids   mean    st. dev.   param1    param2    param3    param4   init. pt
x3    13  7.00E+00  2.10E+00  6.05E+00  5.19E+00                      0.00E+00
x4    13  5.00E+00  1.50E+00  4.32E+00  5.19E+00                      0.00E+00

correlation coefficient matrix in original space:
       1          2
 1  1.000E+00  3.000E-01
 2  3.000E-01  1.000E+00
 end inpro 


>>>> FIRST-ORDER RELIABILITY ANALYSIS <<<<

print interval ..........................npr=      0
  npr<0 ..........no first order results are printed
  npr=0 ........print the final step of FORM results
  npr>0 ........print the results of every npr steps
initialization flag .....................ini=      0
  ini=0 .......................start from mean point
  ini=1 ..........start from point specified by user
  ini=-1 ....start from previous linearization point
restart flag ............................ist=      0
  ist=0 .......................analyze a new problem
  ist=1 .............continue an unconverged problem

limit-state function    11
-----------------------------------------------------------------------------
iteration number ..............iter=        14
value of limit-state function..g(x)=   -2.0118E-07
reliability index .............beta=      2.425980
probability ....................Pf1= 7.6335616E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      7.808E+01    -1.151E+00        -.4742    -.5175     .6044    -.6272
x2      1.404E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      1.260E+01     2.013E+00         .8300     .7840    -.2618    -.6742
x4      6.918E+00     7.124E-01         .2936     .3427    -.1968    -.1930
-----------------------------------------------------------------------------

limit-state function    12
-----------------------------------------------------------------------------
iteration number ..............iter=         9
value of limit-state function..g(x)=   -5.2386E-07
reliability index .............beta=      2.281104
probability ....................Pf1= 1.1271139E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      8.008E+01    -1.022E+00        -.4482    -.4703     .5599    -.5364
x2      1.412E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      8.567E+00     1.027E+00         .4503     .1647    -.1264    -.0631
x4      8.993E+00     1.762E+00         .7723     .8670    -.3017    -.7744
-----------------------------------------------------------------------------

limit-state function    13
-----------------------------------------------------------------------------
iteration number ..............iter=        10
value of limit-state function..g(x)=   -3.7912E-08
reliability index .............beta=      3.084338
probability ....................Pf1= 1.0200292E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      9.394E+01    -2.164E-01        -.0702     .0000     .0000     .0000
x2      1.191E+01    -1.043E+00        -.3381    -.3566     .4343    -.4277
x3      9.359E+00     1.287E+00         .4172     .1011    -.0658    -.0549
x4      1.434E+01     2.593E+00         .8406     .9288    -.0864   -1.1834
-----------------------------------------------------------------------------

limit-state function    14
-----------------------------------------------------------------------------
iteration number ..............iter=         9
value of limit-state function..g(x)=   -5.2221E-07
reliability index .............beta=      2.786049
probability ....................Pf1= 2.6677440E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      7.974E+01    -1.044E+00        -.3747    -.3876     .4697    -.4564
x2      1.410E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      1.673E+01     2.569E+00         .9221     .9156    -.1621   -1.0493
x4      6.406E+00     2.703E-01         .0970     .1073    -.0760    -.0506
-----------------------------------------------------------------------------

limit-state function    15
-----------------------------------------------------------------------------
iteration number ..............iter=         7
value of limit-state function..g(x)=   -4.4592E-07
reliability index .............beta=      2.017450
probability ....................Pf1= 2.1824272E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      8.315E+01    -8.328E-01        -.4128    -.4128     .5003    -.4173
x2      1.422E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      7.650E+00     6.525E-01         .3234     .0000     .0000     .0000
x4      8.315E+00     1.718E+00         .8515     .9108    -.3922    -.7779
-----------------------------------------------------------------------------

limit-state function    16
-----------------------------------------------------------------------------
iteration number ..............iter=        25
value of limit-state function..g(x)=   -4.1904E-08
reliability index .............beta=      2.961260
probability ....................Pf1= 1.5319170E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      1.100E+02     5.816E-01         .1964     .3104    -.3018    -.1214
x2      1.061E+01    -1.805E+00        -.6096    -.5987     .8544   -1.1281
x3      8.001E+00     8.075E-01         .2727     .0000     .0000     .0000
x4      1.022E+01     2.126E+00         .7179     .7384    -.2109    -.8119
-----------------------------------------------------------------------------

limit-state function    17
-----------------------------------------------------------------------------
iteration number ..............iter=        14
value of limit-state function..g(x)=   -1.4116E-06
reliability index .............beta=      2.701293
probability ....................Pf1= 3.4535272E-03
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      7.775E+01    -1.172E+00        -.4338    -.4696     .5546    -.5826
x2      1.403E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      9.993E+00     1.464E+00         .5420     .2908    -.1579    -.1775
x4      1.055E+01     1.944E+00         .7198     .8336    -.1956    -.8356
-----------------------------------------------------------------------------

limit-state function    18
-----------------------------------------------------------------------------
iteration number ..............iter=         5
value of limit-state function..g(x)=    1.1026E-05
reliability index .............beta=      1.621794
probability ....................Pf1= 5.2423724E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      1.201E+02     1.024E+00         .6312     .7071    -.6717    -.6414
x2      1.201E+01    -1.258E+00        -.7756    -.7071     .9898    -.9490
x3      6.498E+00     0.000E+00         .0000     .0000     .0000     .0000
x4      4.641E+00     0.000E+00         .0000     .0000     .0000     .0000
-----------------------------------------------------------------------------

limit-state function    19
-----------------------------------------------------------------------------
iteration number ..............iter=         8
value of limit-state function..g(x)=   -4.1880E-07
reliability index .............beta=     -1.879736
probability ................1 - Pf1= 3.0072039E-02
var          design point                     sensitivity vectors
           x*            u*            alpha     gamma     delta      eta
x1      1.332E+02     1.547E+00        -.8227    -.8587     .6133    1.0880
x2      1.565E+01     0.000E+00         .0000     .0000     .0000     .0000
x3      5.551E+00    -8.133E-01         .4327     .3055    -.6302     .3699
x4      3.885E+00    -6.929E-01         .3686     .4116    -.8938     .5570
-----------------------------------------------------------------------------


>>>> SECOND-ORDER RELIABILITY ANALYSIS -- POINT FITTING <<<<

type of integration scheme used ...................itg=    2
  itg=1 ...........................improved Breitung formula
  itg=2 ...........................improved Breitung formula
        ............................& Tvedt's exact integral
max. number of iterations for each fitting point ..inp=    4

limit-state function   11
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.426  1.937  8.489E-03 -1.663E-01   -2.426  2.173  2.932E-03 -8.580E-02
   2 2.426  2.426  9.229E-08 -3.434E-09   -2.426  2.426  9.229E-08 -3.434E-09
   3 2.426  2.083  3.882E-02 -1.165E-01   -2.426  1.959  7.311E-04 -1.586E-01

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.252904            2.268385
probability                     Pf2 = 1.2132599E-02       1.1652862E-02
-----------------------------------------------------------------------------

limit-state function   12
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.281  2.109  2.052E-02 -6.625E-02   -2.281  2.023  6.382E-04 -9.912E-02
   2 2.281  2.281  2.005E-07 -6.945E-09   -2.281  2.281  2.005E-07 -6.945E-09
   3 2.281  1.881  4.638E-03 -1.539E-01   -2.281  2.144  3.098E-03 -5.270E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.167678            2.174324
probability                     Pf2 = 1.5091602E-02       1.4840408E-02
-----------------------------------------------------------------------------

limit-state function   13
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 3.000  3.075  4.106E-05 -1.977E-03   -3.000  3.074  2.315E-05 -2.219E-03
   2 3.000  2.936  2.685E-02 -3.300E-02   -3.000  2.834  1.437E-03 -5.571E-02
   3 3.000  2.380  5.454E-03 -1.566E-01   -3.000  2.945  5.470E-03 -3.089E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.991768            2.997079
probability                     Pf2 = 1.3868330E-03       1.3628997E-03
-----------------------------------------------------------------------------

limit-state function   14
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.786  2.279  8.172E-02 -1.308E-01   -2.786  2.662  3.485E-02 -3.205E-02
   2 2.786  2.786  3.018E-07 -1.279E-09   -2.786  2.786  3.018E-07 -1.279E-09
   3 2.786  2.615  3.140E-01 -4.405E-02   -2.786  2.492  1.316E-02 -7.572E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.700100            2.703659
probability                     Pf2 = 3.4659277E-03       3.4290316E-03
-----------------------------------------------------------------------------

limit-state function   15
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.017  1.925  6.021E-03 -4.523E-02   -2.017  1.885  6.490E-04 -6.519E-02
   2 2.017  2.017  1.457E-07 -5.648E-09   -2.017  2.017  1.457E-07 -5.648E-09
   3 2.017  2.015  2.392E-06 -1.283E-03   -2.017  2.015  1.741E-06 -1.357E-03

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      1.986914            1.987498
probability                     Pf2 = 2.3465959E-02       2.3433632E-02
-----------------------------------------------------------------------------

limit-state function   16
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.961  2.415  4.827E-06 -1.246E-01   -2.961  2.653  1.548E-03 -7.041E-02
   2 2.961  2.503  1.032E-02 -1.044E-01   -2.961  2.267  4.094E-07 -1.583E-01
   3 2.961  2.913  3.493E-05 -1.091E-02   -2.961  2.905  3.697E-06 -1.280E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.803361            2.814913
probability                     Pf2 = 2.5286518E-03       2.4395209E-03
-----------------------------------------------------------------------------

limit-state function   17
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 2.701  2.441  1.865E-01 -7.145E-02   -2.701  2.315  2.930E-03 -1.058E-01
   2 2.701  2.701  8.564E-07 -8.476E-09   -2.701  2.701  8.564E-07 -8.476E-09
   3 2.701  1.842  1.423E-02 -2.356E-01   -2.701  2.381  4.622E-02 -8.771E-02

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      2.512342            2.539129
probability                     Pf2 = 5.9966386E-03       5.5564343E-03
-----------------------------------------------------------------------------

limit-state function   18
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1 1.622  1.622 -1.605E-06  2.004E-07   -1.622  1.622 -5.370E-07  3.435E-07
   2 1.622  1.622 -1.099E-06  2.720E-07   -1.622  1.622 -1.099E-06  2.720E-07
   3 1.622  1.622 -1.099E-06  2.720E-07   -1.622  1.622 -1.099E-06  2.720E-07

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =      1.621794            1.621794
probability                     Pf2 = 5.2423680E-02       5.2423683E-02
-----------------------------------------------------------------------------

limit-state function   19
-----------------------------------------------------------------------------
coordinates and  ave. main curvatures of fitting points in rotated space
axis  u'i    u'n    G(u)       a'+i         u'i    u'n    G(u)       a'-i
   1-1.734 -2.015  1.604E-06 -8.967E-02    1.682 -2.059  3.808E-04 -1.266E-01
   2-1.880 -1.880  3.341E-08 -6.178E-09    1.880 -1.880  3.341E-08 -6.178E-09
   3-1.755 -1.997  1.797E-06 -7.582E-02    1.695 -2.048  3.541E-04 -1.173E-01

                                      improved Breitung      Tvedt's EI
generalized reliability index betag =     -1.969805           -1.973452
probability                     Pf2 = 1-  2.4430333E-02   1-  2.4222051E-02
-----------------------------------------------------------------------------


>>>> DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.
    trials        Pf-mean     betag-mean    coef of var
       500    9.59983E-03    2.34163E+00    8.36248E-02
      1000    9.89102E-03    2.33046E+00    5.90968E-02
      1500    9.63518E-03    2.34026E+00    4.89304E-02


>>>> FIRST-ORDER DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.

trials       Pf-mean       betag-mean     coef of var of pf
   500    6.9174181E-03    2.4615233E+00    9.1162212E-02
  1000    7.3226363E-03    2.4410339E+00    6.4951286E-02
  1500    7.1621396E-03    2.4490267E+00    5.3986770E-02
  2000    7.0817122E-03    2.4530915E+00    4.6290683E-02


>>>> SECOND-ORDER DIRECTIONAL SIMULATION <<<<

 print interval ......................... npr=    500
 number of simulationts ................. nsm=   5000
 threshold for coef. of variation .... cov= .5000E-01
 random seed ......................... stp=619949844.

trials       Pf-mean       betag-mean     coef of var of pf
   500    9.5904256E-03    2.3419973E+00    8.1408525E-02
  1000    9.9337343E-03    2.3288414E+00    5.7708272E-02
  1500    9.6822532E-03    2.3384386E+00    4.7934174E-02


>>>> MONTE CARLO SIMULATION <<<<

 print interval ......................... npr=    2000
 number of simulations .................. nsm=  200000
 threshold for coef. of variation .... cov=  .5000E-01
 random seed ......................... stp= 619949855.
     trials       Pf-mean       betag-mean     coef of var of Pf
       2000    6.5000000E-03    2.4837691E+00    2.7651638E-01
       4000    5.2500000E-03    2.5589129E+00    2.1767153E-01
       6000    6.0000000E-03    2.5121442E+00    1.6617976E-01
       8000    6.5000000E-03    2.4837691E+00    1.3823226E-01
      10000    6.9000000E-03    2.4624275E+00    1.1997580E-01
      12000    7.5833333E-03    2.4283745E+00    1.0443460E-01
      14000    8.2857143E-03    2.3960809E+00    9.2465517E-02
      16000    8.5000000E-03    2.3867076E+00    8.5386749E-02
      18000    8.6111111E-03    2.3819288E+00    7.9977576E-02
      20000    8.6500000E-03    2.3802690E+00    7.5700947E-02
      22000    8.4090909E-03    2.3906585E+00    7.3213349E-02
      24000    8.4583333E-03    2.3885138E+00    6.9890237E-02
      26000    8.6538462E-03    2.3801052E+00    6.6378855E-02
      28000    8.8571429E-03    2.3715367E+00    6.3219352E-02
      30000    9.3000000E-03    2.3534521E+00    5.9590389E-02
      32000    9.1250000E-03    2.3605064E+00    5.8253872E-02
      34000    9.4117647E-03    2.3490074E+00    5.5638829E-02
      36000    9.3055556E-03    2.3532301E+00    5.4381789E-02
      38000    9.3157895E-03    2.3528214E+00    5.2901953E-02
      40000    9.4000000E-03    2.3494731E+00    5.1328748E-02
      42000    9.5000000E-03    2.3455309E+00    4.9824846E-02
Stop - Program terminated.
